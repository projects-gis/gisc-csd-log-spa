// Unit Test: app/frameworks/core/eventAggregator.js
define(['knockout', 'app/frameworks/core/eventAggregator', 'knockout-postbox'], function(ko, EventAggregator) {
    describe("EventAggregator", function() {
        it("should know all requires lib...", function() {
            var koRef = ko;
            expect(koRef).not.toBeNull();

            var postboxRef = ko.postbox;
            expect(postboxRef).not.toBeNull();
        });

        it("should work...", function() {
            var eva = new EventAggregator();
            var listener = {
                listen: function(msg) {
                    // just a stub.
                    console.log(msg);
                }
            };

            spyOn(listener, 'listen');

            eva.subscribe("let-s-talk-about-secret", listener.listen);
            eva.publish("let-s-talk-about-secret", "I laugh");
            eva.publish("let-s-talk-about-secret", "I laugh again");

            expect(listener.listen).toHaveBeenCalled();
            expect(listener.listen).toHaveBeenCalledWith("I laugh");
            expect(listener.listen).toHaveBeenCalledWith("I laugh again");

            expect(eva.disposableObjects).not.toBeNull();
            expect(eva.disposableObjects.length).toBe(1);

            eva.destroy();

            expect(eva.disposableObjects.length).toBe(0);
        });
    });
});