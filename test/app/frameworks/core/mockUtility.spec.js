// Unit Test: app/frameworks/core/mockUtility.js
define(['app/frameworks/core/mockUtility'], function(MockUtility) {
    describe("MockUtility class, specialize", function() {
        it("can ensure id is suitable for CRUD mocking", function() {
            var seedLength = 1;
            var mockObj1 = {
                id: null
            };
            seedLength = MockUtility.ensureIdMaxPositiveInteger(seedLength, mockObj1);

            expect(mockObj1.id).toBe(2);
            expect(seedLength).toBe(2);

            var mockObj2 = {
                id: -1
            };
            seedLength = MockUtility.ensureIdMaxPositiveInteger(seedLength, mockObj2);

            expect(mockObj2.id).toBe(3);
            expect(seedLength).toBe(3);
        });
    });
    describe("MockUtility class, in general", function() {
        var testDataSource = [];

        // Setup
        beforeEach(function() {
            // spyOn(myfunc, 'init').andCallThrough();
            testDataSource = [
                { id: 1, altKey: 1, title: "item1", prop1: "some-value", prop2: true, nestProp: { childProp1: "sth", childProp2: 5.8 }},
                { id: 2, altKey: 2, title: "item2", prop1: "some-value2", prop2: false, nestProp: { childProp1: "sth-else", childProp2: 5 }},
                { id: 3, altKey: 3, title: "item3", prop1: "some-value3", prop2: true, nestProp: { childProp1: "sth-else-entirely", childProp2: 5.96796 }}
            ];
        });

        // MockUtility.createEntity
        it("can create entity", function() {
            var newItem = { 
                id: -1, 
                altKey: 1,
                title: "item4", 
                prop1: "some-value4", 
                prop2: true, 
                nestProp: { 
                    childProp1: "sth-again", 
                    childProp2: 5.8 
                }
            };

            MockUtility.createEntity(testDataSource, newItem);

            var expectSize = 4;

            expect(testDataSource.length).toBe(expectSize);
            expect(newItem.id).toBe(expectSize);
        });

        it("can create entity.. with key other than id", function() {
            var newItem = { 
                id: -1, 
                altKey: 1,
                title: "item4", 
                prop1: "some-value4", 
                prop2: true, 
                nestProp: { 
                    childProp1: "sth-again", 
                    childProp2: 5.8 
                }
            };

            MockUtility.createEntity(testDataSource, newItem, "altKey");

            var expectSize = 4;

            expect(testDataSource.length).toBe(expectSize);
            expect(newItem.altKey).toBe(expectSize);
        });

        // MockUtility.updateEntity
        it("can update entity", function() {
            var updateItem = { 
                id: 1, 
                altKey: 1,
                title: "newitem1", 
                prop1: "some-value", 
                prop2: true, 
                nestProp: { 
                    childProp1: "sth-again", 
                    childProp2: 5.8 
                }
            };

            MockUtility.updateEntity(testDataSource, updateItem);

            var expectSize = 3;

            expect(testDataSource.length).toBe(expectSize);
            expect(updateItem.id).toBe(1);

            var targetItem = null;
            testDataSource.forEach(function(element) {
                if(element.id === 1) {
                    targetItem = element;
                }
            }, this);

            expect(targetItem).toBe(updateItem);
        });

        // MockUtility.deleteEntity
        it("can delete entity", function() {

            MockUtility.deleteEntity(testDataSource, 1);

            var expectSize = 2;

            expect(testDataSource.length).toBe(expectSize);
        });

        // MockUtility.findEntity
        it("can find entity", function() {

            var item = MockUtility.findEntity(testDataSource, 1);

            var expectSize = 3;
            var expectObject = testDataSource[0];

            expect(testDataSource.length).toBe(expectSize);
            expect(item).toBe(expectObject);
        });

    });
});