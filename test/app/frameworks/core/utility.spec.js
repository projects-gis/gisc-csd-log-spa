// Unit Test: app/frameworks/core/utility.js
define(['app/frameworks/core/utility'], function(Utility) {

    // Parameter parsing, creation.
    describe("Utility class, about Parameters", function() {
        // Setup
        var testParam1;
        var testParam2;
        beforeEach(function() {
            testParam1 = {
                id: 1,
                superPower: "roll-eye"
            };
            testParam2 = {
                id: 2,
                superPower: "roll-eye",
                title: "i18n:Some_Key"
            };
        });

        it("should extract parameters", function() {
            var actual1 = Utility.extractParam(testParam1.id);
            var actual2 = Utility.extractParam(testParam1.superPower);
            var actual3 = Utility.extractParam(testParam1.somethingDoesNotExisted);
            var actual4 = Utility.extractParam(testParam1.somethingDoesNotExisted, "butINeedIt");

            expect(actual1).toBe(testParam1.id);
            expect(actual2).toBe(testParam1.superPower);
            expect(actual3).toBeNull();
            expect(actual4).toBe("butINeedIt");
        });

        // // Skip because jasmine does not setup with i18nextko
        // // I still cannot find a way to stub i18nextko using jasmine's spy
        // xit("should extract possible i18n parameters", function() {

        //     // spyOn(i18nextko, "t").and.callFake(function(key) {
        //     //     return key;
        //     // });
        //     var i18nextko = {};
        //     i18nextko.t = jasmine.createSpy('t').and.callFake(function(key) {
        //         return key;
        //     });
        //     // i18nextko.t = jasmine.createSpy('t').and.stub();

        //     // var actual1 = Utility.extractPossibleI18NParam(testParam2.id);
        //     // var actual2 = Utility.extractPossibleI18NParam(testParam2.superPower);
        //     // var actual3 = Utility.extractPossibleI18NParam(testParam2.somethingDoesNotExisted);
        //     // var actual4 = Utility.extractPossibleI18NParam(testParam2.somethingDoesNotExisted, "butINeedIt");
        //     var actual5 = Utility.extractPossibleI18NParam(testParam2.title);
        //     // var actual6 = Utility.extractPossibleI18NParam(testParam2.title, "butINeedIt");

        //     // expect(actual1).toBe(testParam2.id);
        //     //expect(actual2).toBe(testParam2.superPower);
        //     // expect(actual3).toBeNull();
        //     // expect(actual4).toBe("butINeedIt");
        //     expect(actual5).toBe("Some_Key");
        //     // expect(actual6).toBe("Some_Key");
        // });

        if ("should create options if certain parameter exist", function() {
            var actual1 = Utility.createOptionIfParamExist("id", testParam1.id);
            var actual2 = Utility.createOptionIfParamExist("id", testParam1.somethingDoesNotExisted);

            expect(actual1).toBe(testParam1.id);
            expect(actual2).toBeNull();
        });
    });

    // Data Structure things...
    describe("Utility class, about Data Structure", function() {
        it("should able to aggregateCollection", function() {
            var struct1 = {
                components: [{
                    group: "gisc-ui-control",
                    items: [{
                        name: "gisc-chrome-shell",
                        path: "components/controls/gisc-chrome/shell/shell"
                    }, {
                        name: "gisc-chrome-crumbtail",
                        path: "components/controls/gisc-chrome/crumbtail/crumbtail"
                    }]
                }, {
                    group: "svg",
                    items: [{
                        name: "svg-nav-block",
                        path: "text!components/svgs/nav-block.html"
                    }, {
                        name: "svg-nav-bar",
                        path: "text!components/svgs/nav-bar.html"
                    }]
                }]
            };

            var allComponents = Utility.aggregateCollection({
                source: struct1.components,
                by: "items"
            });

            expect(allComponents).not.toBeNull();
            expect(allComponents.length).toBe(4);
        });
    });

    // Data Conversion things...
    describe("Utility class, about Data Conversion", function() {
        it("can ensure number with precistion", function() {
            var actual1 = Utility.ensureNumberPrecision(25.01);
            var actual2 = Utility.ensureNumberPrecision(25.01, 5);

            expect(parseFloat(actual1)).toBe(parseFloat(25.01));
            expect(actual1).toBe("25.01");

            expect(actual2).toBe("25.01000");
        });
    });

    // Data Conversion things...
    describe("Utility class, about resolve URL", function() {
        it("can resolve url properly", function() {
            var actual1 = Utility.resolveUrl("/", "~/api/user");
            expect(actual1).toBe("/api/user");

            var actual2 = Utility.resolveUrl("/", "/api/user");
            expect(actual2).toBe("/api/user");

            var actual3 = Utility.resolveUrl("http://localhost", "~/api/user");
            expect(actual3).toBe("http://localhost/api/user");

            var actual4 = Utility.resolveUrl("http://localhost/", "~/api/user");
            expect(actual4).toBe("http://localhost/api/user");

            var actual5 = Utility.resolveUrl("", "~/api/user");
            expect(actual5).toBe("/api/user");

            var actual6 = Utility.resolveUrl(undefined, "~/api/user");
            expect(actual6).toBe("/api/user");

            var actual7 = Utility.resolveUrl(null, "~/api/user");
            expect(actual7).toBe("/api/user");
        });
    });

    // MIME type discovery logic.
    describe("Utility class, about mimetype detection", function() {
        it("should detect capital letter extension.", function() {
            expect(Utility.getMIMEType("JPG")).toBe("image/jpeg");
            expect(Utility.getMIMEType("JpG")).toBe("image/jpeg");
        });

        it("should detect with dot extension.", function() {
            expect(Utility.getMIMEType(".jpg")).toBe("image/jpeg");
            expect(Utility.getMIMEType("......jpg")).toBe("image/jpeg");
        });

        it("should detect binary or unknown mime type.", function() {
            var binaryMimeType = Utility.getMIMEType("bin");
            expect(Utility.getMIMEType()).toBe(binaryMimeType);
            expect(Utility.getMIMEType(null)).toBe(binaryMimeType);
            expect(Utility.getMIMEType("")).toBe(binaryMimeType);
            expect(Utility.getMIMEType("dat")).toBe(binaryMimeType);
            expect(Utility.getMIMEType("unknown")).toBe(binaryMimeType);
        });
    });

    // Data Conversion things...
    describe("Utility class, about date add days", function() {
        it("can add days properly", function() {
            var actual1 = Utility.addDays(new Date("2016-02-01"), 1);
            expect(actual1).toEqual(new Date("2016-02-02"));

            var actual2 = Utility.addDays(new Date("2016-02-01"), 0);
            expect(actual2).toEqual(new Date("2016-02-01"));

            var actual3 = Utility.addDays(new Date("2016-02-01"),-1);
            expect(actual3).toEqual(new Date("2016-01-31"));

            var actual4 = Utility.addDays(new Date("2016-02-01"));
            expect(actual4).toEqual(new Date("2016-02-01"));

            var actual5 = Utility.addDays(new Date("2016-02-01") , null);
            expect(actual5).toEqual(new Date("2016-02-01"));

        });
    });

    // MISC.
    describe("Utility class, about string format", function() {
        it("should format complete parameters by order properly", function() {
            var result = Utility.stringFormat("Hello, {0} {1}.", "John", "Smith");
            expect(result).toBe("Hello, John Smith.");
        });
        it("should format lack of parameters properly", function() {
            var result = Utility.stringFormat("Hello, {0} {1}.", "John");
            expect(result).toBe("Hello, John undefined.");
        });
        it("should format parameterless properly", function() {
            var result = Utility.stringFormat("Hello, {0} {1}.");
            expect(result).toBe("Hello, undefined undefined.");
        });
        it("should format exceeed of parameters properly", function() {
            var result = Utility.stringFormat("Hello, {0} {1}.", "John", "Smith", "MR", "Sir");
            expect(result).toBe("Hello, John Smith.");
        });
    });

    describe("Utility class, about other things...", function() {
        it("should generate uniqueID", function() {
            var generatedId = Utility.getUniqueId();
            expect(generatedId).toMatch("^(\{{0,1}([0-9a-fA-F]){8}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){4}-([0-9a-fA-F]){12}\}{0,1})$");
        });

        it("should generate hash code from string", function() {
            var stringContent1 = "lorem ipsum dollar sit amet";
            var stringContent2 = "{id:1, name:'test'}";

            var hash1 = Utility.getHashCode(stringContent1);
            var hash1Copy = Utility.getHashCode("lorem ipsum dollar sit amet");

            expect(hash1).toBe(hash1Copy);
        });

        it("should generate random string", function() {
            var random1 = Utility.randomString(10);
            var random2 = Utility.randomString(20);

            expect(random1.length).toBe(10);
            expect(random2.length).toBe(20);
        });

        it("should format number string with comma separator", function() {
            var rawNumber = 1000000;
            var formatNumber = Utility.numberFormat(rawNumber);

            expect(formatNumber).toBe("1,000,000");
        });
    });

    // Data Calculation
    describe("Utility class, about Data Calculation...", function() {
        it("should generate array with InfoStatus", function() {
            // Case 1
            var originalArray = [];
            var currentArray = [
                { userId: 1, companyId: 1, companyName: "Company 1" }
            ];
            var generated1 = Utility.generateArrayWithInfoStatus(originalArray, currentArray, "companyId");
            expect(generated1.length).toBe(1);
            expect(generated1[0].infoStatus).toBe(2);

            // Case 2
            originalArray = [
                { id: 1, userId: 1, companyId: 1, companyName: "Company 1", infoStatus: 1 },
                { id: 2, userId: 1, companyId: 2, companyName: "Company 2", infoStatus: 1 }
            ];
            currentArray = [
                { userId: 1, companyId: 3, companyName: "Company 3" }
            ];
            var generated2 = Utility.generateArrayWithInfoStatus(originalArray, currentArray, "companyId");
            expect(generated2.length).toBe(3);

            // Case 3
            originalArray = [
                { id: 1, userId: 1, companyId: 1, companyName: "Company 1", infoStatus: 1 },
                { id: 2, userId: 1, companyId: 2, companyName: "Company 2", infoStatus: 1 }
            ];
            currentArray = originalArray;
            var generated3 = Utility.generateArrayWithInfoStatus(originalArray, currentArray, "companyId");
            expect(generated3.length).toBe(2);
            
        });
    });

    // Data Manipulation
    describe("Utility class, about Data Manipulation...", function() {
        it("should able to apply formatted property to an object", function() {
            var testObj = {
                brand: "Apple",
                model: "IP010"
            };

            Utility.applyFormattedProperty(testObj, "title", "{0} ({1})", "brand", "model");

            expect(testObj.title).not.toBeUndefined();
            expect(testObj.title).toBe("Apple (IP010)");
        });

        it("should able to apply formatted property to an entire collection", function() {
            var testArray = [{
                    brand: "Apple",
                    model: "IP010",
                    year: 1983 
                }, {
                    brand: "Samsung",
                    model: "GX003",
                    year: 2015 
                }, {
                    brand: "Nintendo",
                    model: "NX001",
                    year: 2016 
                }
            ];
            
            Utility.applyFormattedPropertyToCollection(testArray, "title", "{0} ({1}) - {2}", "brand", "model", "year");

            expect(testArray.length).toBe(3);
            expect(testArray[0].title).not.toBeUndefined();
            expect(testArray[0].title).toBe("Apple (IP010) - 1983");
            expect(testArray[1].title).toBe("Samsung (GX003) - 2015");
            expect(testArray[2].title).toBe("Nintendo (NX001) - 2016");
        });

        it("should able to apply formatted property to an object with predicate", function() {
            var testObj1 = {
                brand: "Apple",
                model: "IP010",
                year: null
            };
            var testObj2 = {
                brand: "Samsung",
                model: "GX003",
                year: 2015
            };

            let format = (object) => {
                if((object.year !== undefined) && object.year) {
                    return "{0} ({1}) - {2}";
                }
                return "{0} ({1})";
            };

            Utility.applyFormattedPropertyPredicate(testObj1, "title", format, "brand", "model", "year");
            Utility.applyFormattedPropertyPredicate(testObj2, "title", format, "brand", "model", "year");

            expect(testObj1.title).not.toBeUndefined();
            expect(testObj1.title).toBe("Apple (IP010)");
            expect(testObj2.title).not.toBeUndefined();
            expect(testObj2.title).toBe("Samsung (GX003) - 2015");
        });

        it("should able to apply formatted property to an object with predicate to an entire collection", function() {
            var testArray = [{
                    brand: "Apple",
                    model: "IP010",
                    year: null 
                }, {
                    brand: "Samsung",
                    model: "GX003",
                    year: 2015 
                }, {
                    brand: "Nintendo",
                    model: "NX001"
                }
            ];

            testArray.forEach(function(element) {
                Utility.applyFormattedPropertyPredicate(element, "title", (x) => {
                    if((x.year !== undefined) && x.year) {
                        return "{0} ({1}) - {2}";
                    }
                    return "{0} ({1})";
                }, "brand", "model", "year");

            }, this);

            expect(testArray.length).toBe(3);
            expect(testArray[0].title).not.toBeUndefined();
            expect(testArray[0].title).toBe("Apple (IP010)");
            expect(testArray[1].title).toBe("Samsung (GX003) - 2015");
            expect(testArray[2].title).toBe("Nintendo (NX001)");

        });
    });

});