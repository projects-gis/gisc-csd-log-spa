/**
 * README for all developers
 * ALL components (screen) MUST define its route otherwise the navigation system won't work properly. 
 * 
 * WARNING:
 * 1. portals[i].name MUST match the portal name constant used by Shell.js
 */
var routing = {
    portals: [
        {
            name: "back-office",
            routes: [
                // Select Company
                { url: 'select-company', params: { page: 'bo-company-select', navLevel: 1 } },
                // Home
                { url: 'home', params: { page: 'bo-home', navLevel: 1 } },
                // Subscription and Group
                { url: 'subscription-and-group', params: { page: 'bo-subscription-and-group', navLevel: 1}, permission: 'SubscriptionManagement' },
                // Subscription and Group - Subscription
                { url: 'subscription-and-group/subscription', params: { page: 'bo-subscription-and-group-subscription', navLevel: 2 }, permission: 'SubscriptionManagement' },
                { url: 'subscription-and-group/subscription/create', params: { page: 'bo-subscription-and-group-subscription-manage', mode: 'create',  navLevel: 3 }, permission: 'CreateSubscription' },
                { url: 'subscription-and-group/subscription/update', params: { page: 'bo-subscription-and-group-subscription-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateSubscription' },
                // Subscription and Group - Group
                { url: 'subscription-and-group/group', params: { page: 'bo-subscription-and-group-group', navLevel: 2 }, permission: 'SubscriptionManagement' },
                { url: 'subscription-and-group/group/create', params: { page: 'bo-subscription-and-group-group-manage', mode: 'create', navLevel: 3 }, permission: 'CreateGroup_BackOffice' },
                { url: 'subscription-and-group/group/update', params: { page: 'bo-subscription-and-group-group-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateGroup_BackOffice' },
                // Subscription - Technician Team
                { url: 'subscription-and-group/technician-team', params: { page: 'bo-subscription-and-group-technician-team', navLevel: 2 }, permission: 'SubscriptionManagement' },
                { url: 'subscription-and-group/technician-team/create', params: { page: 'bo-subscription-and-group-technician-team-manage', mode: 'create', navLevel: 3 }, permission: 'CreateTechnicianTeam_BackOffice' },
                { url: 'subscription-and-group/technician-team/update', params: { page: 'bo-subscription-and-group-technician-team-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateTechnicianTeam_BackOffice' },
                { url: 'subscription-and-group/technician-team/update/select-accessible-users', params: { page: 'bo-shared-accessible-users-select', mode: 'technician-update', navLevel: 4 } },
                { url: 'subscription-and-group/technician-team/create/select-accessible-users', params: { page: 'bo-shared-accessible-users-select', mode: 'technician-create', navLevel: 4 } },

                // Company
                { url: 'company', params: { page: 'bo-company', navLevel: 1 }, permission: 'CompanyManagement' },
                { url: 'company/create', params: { page: 'bo-company-manage', mode: 'create', navLevel: 2 }, permission: 'CreateCompany' },
                { url: 'company/create/subscription', params: { page: 'bo-company-manage-subscription', mode: 'company-create', navLevel: 3 }, permission: 'CreateCompany' },
                { url: 'company/create/subscription/add', params: { page: 'bo-company-manage-subscription-manage', mode: 'company-create-subscription-add', navLevel: 4 }, permission: 'CreateCompany' },
                { url: 'company/create/subscription/update', params: { page: 'bo-company-manage-subscription-manage', mode: 'company-create-subscription-update', navLevel: 4 }, permission: 'CreateCompany' },
                { url: 'company/create/contact', params: { page: 'bo-company-manage-contact-manage', mode: 'company-create', navLevel: 3 }, permission: 'CreateCompany' },
                { url: 'company/create/regional', params: { page: 'bo-company-manage-regional-manage', mode: 'company-create', navLevel: 3 }, permission: 'CreateCompany' },
                { url: 'company/create/setting', params: { page: 'bo-company-manage-setting-manage', mode: 'company-create', navLevel: 3 }, permission: 'CreateCompany' },
                { url: 'company/create/mapservices', params: { page: 'bo-company-manage-mapservices-manage', mode: 'company-create', navLevel: 3 }, permission: 'CreateCompany' },
                { url: 'company/update', params: { page: 'bo-company-manage', mode: 'update', navLevel: 2 }, permission: 'UpdateCompany' },
                { url: 'company/update/subscription', params: { page: 'bo-company-manage-subscription', mode: 'company-update', navLevel: 3 }, permission: 'UpdateCompany' },
                { url: 'company/update/subscription/add', params: { page: 'bo-company-manage-subscription-manage', mode: 'company-update-subscription-add', navLevel: 4 }, permission: 'UpdateCompany' },
                { url: 'company/update/subscription/update', params: { page: 'bo-company-manage-subscription-manage', mode: 'company-update-subscription-update', navLevel: 4 }, permission: 'UpdateCompany' },
                { url: 'company/update/contact', params: { page: 'bo-company-manage-contact-manage', mode: 'company-update', navLevel: 3 }, permission: 'UpdateCompany' },
                { url: 'company/update/regional', params: { page: 'bo-company-manage-regional-manage', mode: 'company-update', navLevel: 3 }, permission: 'UpdateCompany' },
                { url: 'company/update/setting', params: { page: 'bo-company-manage-setting-manage', mode: 'company-update', navLevel: 3 }, permission: 'UpdateCompany' },
                { url: 'company/update/mapservices', params: { page: 'bo-company-manage-mapservices-manage', mode: 'company-update', navLevel: 3 }, permission: 'UpdateCompany' },
                { url: 'company/create/pairable-vehicle', params: { page: 'bo-company-manage-pairable-vehicle', navLevel: 3, mode: 'company-create' }, permission: 'CreateCompany' },                
                { url: 'company/update/pairable-vehicle', params: { page: 'bo-company-manage-pairable-vehicle', navLevel: 3,mode: 'company-update'}, permission: 'UpdateCompany' },

                // User
                { url: 'user', params: { page: 'bo-user', navLevel: 1 }, permission: 'UserManagement_BackOffice' },
                { url: 'user/create', params: { page: 'bo-user-manage', mode: 'create', navLevel: 2 }, permission: 'CreateUser_BackOffice' },
                { url: 'user/create/select-accessible-company', params: { page: 'bo-shared-accessible-company-select', mode: 'user-create', navLevel: 4 } },
                { url: 'user/update', params: { page: 'bo-user-manage', mode: 'update', navLevel: 2 }, permission: 'UpdateUser_BackOffice' },
                { url: 'user/update/select-accessible-company', params: { page: 'bo-shared-accessible-company-select', mode: 'user-update', navLevel: 4 } },
                // Asset
                { url: 'asset', params: { page: 'bo-asset', navLevel: 1 }, permission: 'AssetManagement' },
                // Asset - Box Feature
                { url: 'asset/box-feature', params: { page: 'bo-asset-box-feature', navLevel: 2 }, permission: 'AssetManagement' },
                { url: 'asset/box-feature/create', params: { page: 'bo-asset-box-feature-manage', mode: 'create', navLevel: 3 }, permission: 'CreateBoxFeature' },
                { url: 'asset/box-feature/update', params: { page: 'bo-asset-box-feature-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateBoxFeature' },
                // Asset - Box Model
                { url: 'asset/box-model', params: { page: 'bo-asset-box-model', navLevel: 2 } },
                { url: 'asset/box-model/create', params: { page: 'bo-asset-box-model-manage', mode: 'create', navLevel: 3 } },
                { url: 'asset/box-model/update', params: { page: 'bo-asset-box-model-manage', mode: 'update', navLevel: 3 } },
                // Asset - Box Template
                { url: 'asset/box-template', params: { page: 'bo-asset-box-template', navLevel: 2 } },
                { url: 'asset/box-template/create', params: { page: 'bo-asset-box-template-manage', mode: 'create', navLevel: 3 } },
                { url: 'asset/box-template/update', params: { page: 'bo-asset-box-template-manage', mode: 'update', navLevel: 3 } },
                // Asset - Box Stock
                { url: 'asset/box-stock', params: { page: 'bo-asset-box-stock', navLevel: 2 }, permission: 'AssetManagement' },
                { url: 'asset/box-stock/create', params: { page: 'bo-asset-box-stock-manage', mode: 'create', navLevel: 3 }, permission: 'CreateBoxsStock' },
                { url: 'asset/box-stock/update', params: { page: 'bo-asset-box-stock-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateBoxsStock' },
                // Asset - Box
                { url: 'asset/box', params: { page: 'bo-asset-box', navLevel: 2 }, permission: 'AssetManagement' },
                { url: 'asset/box/search', params: { page: 'bo-asset-box-search', navLevel: 3 }, permission: 'AssetManagement' },
                { url: 'asset/box/create', params: { page: 'bo-asset-box-manage', mode: 'create', navLevel: 3 }, permission: 'CreateBox' },
                { url: 'asset/box/update', params: { page: 'bo-asset-box-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateBox' },
                { url: 'asset/box/update/contract', params: { page: 'bo-asset-box-manage-contract', navLevel: 4 }, permission: 'UpdateBox' },
                { url: 'asset/box/update/maintenance', params: { page: 'bo-asset-box-manage-maintenance', navLevel: 4 }, permission: 'UpdateBox' },
                { url: 'asset/box/update/maintenance/add', params: { page: 'bo-asset-box-manage-maintenance-manage', mode: 'create', navLevel: 5 }, permission: 'UpdateBox' },
                { url: 'asset/box/update/maintenance/update', params: { page: 'bo-asset-box-manage-maintenance-manage', mode: 'update', navLevel: 5 }, permission: 'UpdateBox' },
                { url: 'asset/box/import', params: { page: 'bo-asset-box-import', navLevel: 3 }, permissionsAnd: ['CreateBox', 'UpdateBox'] },
                { url: 'asset/box/assign', params: { page: 'bo-asset-box-assign', navLevel: 3 }, permission: 'UpdateBox' },
                { url: 'asset/box/return', params: { page: 'bo-asset-box-return', navLevel: 3 }, permission: 'UpdateBox' },

                // Asset - Vehicle Model
                { url: 'asset/vehicle-model', params: { page: 'bo-asset-vehicle-model', navLevel: 2 } },
                { url: 'asset/vehicle-model/create', params: { page: 'bo-asset-vehicle-model-manage', mode: 'create', navLevel: 3 } },
                { url: 'asset/vehicle-model/update', params: { page: 'bo-asset-vehicle-model-manage', mode: 'update', navLevel: 3 } },
                { url: 'asset/vehicle-model/inverse-feature', params: { page: 'bo-shared-accessible-inverse-features-select', navLevel: 4 } },
                
                
                // MDVR Models
                { url: "asset/mdvr", params: { page: "bo-asset-mdvr-menu", navLevel: 2 } },
                { url: "asset/mdvr/mdvrmodels", params: { page: "bo-asset-mdvr-models", navLevel: 3 } },
                { url: "asset/mdvr/mdvrmodels/create", params: { page: "bo-asset-mdvr-models-manage", navLevel: 4, mode: 'create' } },
                { url: "asset/mdvr/mdvrmodels/update", params: { page: "bo-asset-mdvr-models-manage", navLevel: 4, mode: 'update' } },

                // MDVRs
                { url: "asset/mdvr/mdvr", params: { page: "bo-asset-mdvrs", navLevel: 3 } },
                { url: "asset/mdvr/mdvr/create", params: { page: "bo-asset-mdvrs-manage", navLevel: 4, mode: 'create' } },
                { url: "asset/mdvr/mdvr/managetwo", params: { page: "bo-asset-mdvrs-manage-two", navLevel: 4 } },
                { url: "asset/mdvr/mdvr/update", params: { page: "bo-asset-mdvrs-manage", navLevel: 4, mode: 'update' } },
                { url: "asset/mdvr/mdvr/updateMutiple", params: { page: "bo-asset-mdvrs-update-multiple", navLevel: 4} },
                { url: "asset/mdvr/mdvr/search", params: { page: "bo-asset-mdvrs-search", navLevel: 4} },
                { url: 'asset/mdvrs/import', params: { page: 'bo-asset-mdvrs-import', navLevel: 3 } },

                // MDVR Config
                { url: "asset/mdvr/mdvrconfig", params: { page: "bo-asset-mdvrs-config", navLevel: 3 } },
                { url: "asset/mdvr/mdvrconfig/create", params: { page: "bo-asset-mdvrs-config-manage", navLevel: 4, mode: 'create' } },
                { url: "asset/mdvr/mdvrconfig/update", params: { page: "bo-asset-mdvrs-config-update", navLevel: 4, mode: 'update' } },

                // Asset - DMS Model
                { url: "asset/dms/model", params: { page: "bo-asset-dms-model", navLevel: 3 } },
                { url: "asset/dms/model/create", params: { page: "bo-asset-dms-model-manage", navLevel: 4, mode: 'create' } },
                { url: "asset/dms/model/update", params: { page: "bo-asset-dms-model-manage", navLevel: 4, mode: 'update' } },

                // Asset - DMS
                { url: "asset/dms", params: { page: "bo-asset-dms", navLevel: 2 } },
                { url: "asset/dms/device", params: { page: "bo-asset-dms-device", navLevel: 3 } },
                { url: "asset/dms/create", params: { page: "bo-asset-dms-manage", navLevel: 4, mode: 'create' } },
                { url: "asset/dms/update", params: { page: "bo-asset-dms-manage", navLevel: 4, mode: 'update' } },

                //Asset - DMS Config
                { url: "asset/dms/config", params: { page: "bo-asset-dms-config", navLevel: 3 } },
                { url: "asset/dms/config/create", params: { page: "bo-asset-dms-config-manage", navLevel: 4, mode: 'create' } },
                { url: "asset/dms/config/update", params: { page: "bo-asset-dms-config-manage", navLevel: 4, mode: 'update' } },
                { url: "asset/dms/config/config-key", params: { page: "bo-asset-dms-config-config-key", navLevel: 5 } },

                //Asset - ADAS Model
                { url: "asset/adas/model", params: { page: "bo-asset-adas-model", navLevel: 3 } },
                { url: "asset/adas/model/create", params: { page: "bo-asset-adas-model-manage", navLevel: 4, mode: 'create'} },
                { url: "asset/adas/model/update", params: { page: "bo-asset-adas-model-manage", navLevel: 4, mode: 'update'} },

                 // Asset - ADAS
                { url: "asset/adas", params: { page: "bo-asset-adas", navLevel: 2 } },
                { url: "asset/adas/device", params: { page: "bo-asset-adas-device", navLevel: 3 } },
                { url: "asset/adas/create", params: { page: "bo-asset-adas-manage", navLevel: 4 ,mode: 'create'} },
                { url: "asset/adas/update", params: { page: "bo-asset-adas-manage", navLevel: 4 ,mode: 'update'} },

                // Announcement
                { url: 'announcement', params: { page: 'bo-announcement', navLevel: 1 }, permission: 'Announcement' },
                { url: 'announcement/search', params: { page: 'bo-announcement-search', navLevel: 2 }, permission: 'Announcement' },
                { url: 'announcement/create', params: { page: 'bo-announcement-manage', mode: 'create', navLevel: 2 }, permission: 'CreateAnnouncement' },
                { url: 'announcement/create/message', params: { page: 'bo-announcement-manage-message-manage', navLevel: 3 } },
                { url: 'announcement/create/select-accessible-company', params: { page: 'bo-shared-accessible-company-select', mode: 'announcement-create', navLevel: 4 } },
                { url: 'announcement/update', params: { page: 'bo-announcement-manage', mode: 'update', navLevel: 2 }, permission: 'UpdateAnnouncement' },
                { url: 'announcement/update/message', params: { page: 'bo-announcement-manage-message-manage', navLevel: 3 } },
                { url: 'announcement/update/select-accessible-company', params: { page: 'bo-shared-accessible-company-select', mode: 'announcement-update', navLevel: 4 } },
                // Configuration
                { url: 'configuration', params: { page: 'bo-configuration', navLevel: 1 }, permission: 'Configuration_BackOffice' },
                // Configuration - Translation
                { url: 'configuration/translation', params: { page: 'bo-configuration-translation', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/translation/import', params: { page: 'bo-configuration-translation-import', navLevel: 3 }, permission: 'Translation' },
                { url: 'configuration/translation/export', params: { page: 'bo-configuration-translation-export', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/translation/language', params: { page: 'bo-configuration-translation-language', navLevel: 3 }, permission: 'Translation' },
                { url: 'configuration/translation/language/add', params: { page: 'bo-configuration-translation-language-add', navLevel: 4 }, permission: 'Translation' },
                // Configuration - POI Icon
                { url: 'configuration/poi-icon', params: { page: 'bo-configuration-poi-icon', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/poi-icon/create', params: { page: 'bo-configuration-poi-icon-manage', mode: 'create', navLevel: 3 }, permission: 'POIIconsManagement_BackOffice' },
                { url: 'configuration/poi-icon/update', params: { page: 'bo-configuration-poi-icon-manage', mode: 'update', navLevel: 3 }, permission: 'POIIconsManagement_BackOffice' },
                // Configuration - Vehicle Icon
                { url: 'configuration/vehicle-icon', params: { page: 'bo-configuration-vehicle-icon', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/vehicle-icon/create', params: { page: 'bo-configuration-vehicle-icon-manage', mode: 'create', navLevel: 3 }, permission: 'VehicleIconsManagement' },
                { url: 'configuration/vehicle-icon/create/select-accessible-company', params: { page: 'bo-shared-accessible-company-select', mode: 'vehicle-icon-create', navLevel: 4 } },
                { url: 'configuration/vehicle-icon/update', params: { page: 'bo-configuration-vehicle-icon-manage', mode: 'update', navLevel: 3 }, permission: 'VehicleIconsManagement'},
                { url: 'configuration/vehicle-icon/update/select-accessible-company', params: { page: 'bo-shared-accessible-company-select', mode: 'vehicle-icon-update', navLevel: 4 } },
                // Configuration - System Setting
                { url: 'configuration/system-setting', params: { page: 'bo-configuration-system-setting', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/system-setting/update', params: { page: 'bo-configuration-system-setting-manage', navLevel: 3 }, permission: 'UpdateSettings' },
                // Configuration - Tracking Databases
                { url: 'configuration/tracking-databases', params: { page: 'bo-configuration-tracking-databases', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/tracking-databases/add', params: { page: 'bo-configuration-tracking-databases-manage', mode: 'create', navLevel: 3 },permission: 'TrackingDatabaseManagement' },
                { url: 'configuration/tracking-databases/view', params: { page: 'bo-configuration-tracking-databases-manage', mode: 'update', navLevel: 3 }, permission: 'TrackingDatabaseManagement' },
                // Configuration - Auto-mail
                { url: 'configuration/auto-mail', params: { page: 'bo-configuration-auto-mail', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/sql-template-config', params: { page: 'bo-configuration-sql-template-config', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/sql-template-config/add', params: { page: 'bo-automail-sql-template-manage', mode: 'create', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/sql-template-config/view', params: { page: 'bo-automail-sql-template-manage', mode: 'update', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/mail-config', params: { page: 'bo-configuration-mail-config', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/mail-config/add', params: { page: 'bo-automail-mail-config-manage', mode: 'create', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/mail-config/view', params: { page: 'bo-automail-mail-config-manage', mode: 'update', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/mail-config/view', params: { page: 'bo-automail-mail-config-view', mode: 'view', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/sending-logs', params: { page: 'bo-configuration-sending-log', navLevel: 4 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/auto-mail/sending-logs/export', params: { page: 'bo-configuration-sending-log-manage',mode: 'export', navLevel: 5 }, permission: 'Configuration_BackOffice' },

                // Configuration - MapService
                { url: 'configuration/map-service/list', params: { page: 'bo-configuration-map-service', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/map-service/create', params: { page: 'bo-configuration-map-service-manage', mode: 'create', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/map-service/update', params: { page: 'bo-configuration-map-service-manage', mode: 'update', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                
                // Configuration - nostra-configurations
                { url: 'configuration/nostra-configurations/list', params: { page: 'bo-configuration-nostra-configurations', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/nostra-configurations/update', params: { page: 'bo-configuration-nostra-configurations-manage', navLevel: 3 }, permission: 'Configuration_BackOffice' },

                // Configuration - GIS - Service -configurations
                { url: 'configuration/gisservice-configurations/list', params: { page: 'bo-configuration-gis-service-configurations', navLevel: 2 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/gisservice-configurations/create', params: { page: 'bo-configuration-gis-service-configurations-manage', mode: 'create', navLevel: 3 }, permission: 'Configuration_BackOffice' },
                { url: 'configuration/gisservice-configurations/update', params: { page: 'bo-configuration-gis-service-configurations-manage', mode: 'update', navLevel: 3 }, permission: 'Configuration_BackOffice' },

                // Report
                { url: 'report', params: { page: 'bo-report', navLevel: 1 } },
                // Report - Email Log
                { url: 'report/email-log/email-log', params: { page: 'bo-report-email-log', navLevel: 2 } },
                // Report - Company Expiration
                { url: 'report/company-expiration', params: { page: 'bo-report-company-expiration', navLevel: 2 } },
                { url: 'report/company-expiration/search', params: { page: 'bo-report-company-expiration-search', navLevel: 3 } },
                // Report - Contract Expiration
                { url: 'report/contract-expiration', params: { page: 'bo-report-contract-expiration', navLevel: 2 } },
                { url: 'report/contract-expiration/fleet-service', params: { page: 'bo-report-contract-expiration-fleet-service', navLevel: 3 } },
                { url: 'report/contract-expiration/fleet-service/search', params: { page: 'bo-report-contract-expiration-search', navLevel: 4 } },
                { url: 'report/contract-expiration/mobile-service', params: { page: 'bo-report-contract-expiration-mobile-service', navLevel: 3 } },
                { url: 'report/contract-expiration/mobile-service/search', params: { page: 'bo-report-contract-expiration-search', navLevel: 4 } },
                { url: 'report/contract-expiration/box-maintenance', params: { page: 'bo-report-contract-expiration-box-maintenance', navLevel: 3 } },
                { url: 'report/contract-expiration/box-maintenance/search', params: { page: 'bo-report-contract-expiration-search', navLevel: 4 } },
                // Report - DLT Report
                { url: 'report/dlt-report/search', params: { page: 'bo-report-dlt-report-search', navLevel: 2 } },
                { url: 'report/dlt-report/list', params: { page: 'bo-report-dlt-report-list', navLevel: 3 } },
                // Report - DLT New Installation
                { url: 'report/dlt-new-installation/search', params: { page: 'bo-report-dlt-new-installation-search', navLevel: 2 } },
                { url: 'report/dlt-new-installation/list', params: { page: 'bo-report-dlt-new-installation-list', navLevel: 3 } },
                // Report - DLT Next Year
                { url: 'report/dlt-nextyear/search', params: { page: 'bo-report-dlt-next-year-search', navLevel: 2 } },
                { url: 'report/dlt-nextyear/list', params: { page: 'bo-report-dlt-next-year-list', navLevel: 3 } },
                // Report - Letter
                { url: 'report/letter/search', params: { page: 'bo-report-letter-search', navLevel: 2 } },
                { url: 'report/letter/list', params: { page: 'bo-report-letter-list', navLevel: 3 } },


                //ticket-management
                { url: 'ticket/manage', params: { page: 'bo-ticket-manage', navLevel: 1 } },
                { url: 'ticket/manage/asset', params: { page: 'bo-ticket-manage-asset', navLevel: 3 } },
                { url: 'ticket/manage/asset/vehicle', params: { page: 'bo-ticket-manage-asset-vehicle', navLevel: 3 } },
                { url: 'ticket/manage/asset/find', params: { page: 'bo-ticket-manage-asset-find', navLevel: 2 } },
                { url: 'ticket/manage/asset/create', params: { page: 'bo-ticket-manage-asset-create', navLevel: 3 } },
                { url: 'ticket/manage/asset/update', params: { page: 'bo-ticket-manage-asset-update', navLevel: 3 } },
                { url: 'ticket/manage/asset/history', params: { page: 'bo-ticket-manage-asset-history', navLevel: 3 } },
                { url: 'ticket/manage/asset/details', params: { page: 'bo-ticket-manage-asset-details', navLevel: 3 } },
                { url: 'ticket/manage/detail/close', params: { page: 'bo-ticket-manage-detail-close', navLevel: 4 } },
                { url: 'ticket/manage/asset/batch-action', params: { page: 'bo-ticket-manage-asset-batch-action', navLevel: 3 } },

                { url: 'ticket/dashboard', params: { page: 'bo-ticket-dashboard', navLevel: 2 } },
                { url: 'ticket/manage/edit', params: { page: 'bo-ticket-manage-edit', navLevel: 3 } },
                { url: 'ticket/manage/editappointment', params: { page: 'bo-ticket-manage-edit-appointment', navLevel: 3 } },
                { url: 'ticket/manage/viewappointment', params: { page: 'bo-ticket-manage-view-appointment', navLevel: 3 } },
                { url: 'ticket/manage/sendmail', params: { page: 'bo-ticket-manage-sendmail', navLevel: 3 } },
                { url: 'ticket/manage/asset/maildetails', params: { page: 'bo-ticket-manage-sendmail', mode:'view', navLevel: 3 } },
                { url: 'ticket/manage/create', params: { page: 'bo-ticket-manage-create', navLevel: 3 } },
                { url: 'ticket/manage/fleet-service', params: { page: 'bo-ticket-manage-fleet-service', navLevel: 3 } },
                { url: 'ticket/manage/detail/fleet-service', params: { page: 'bo-ticket-manage-detail-fleet-service', navLevel: 4 } },

                { url: 'ticket/manage/asset/vehicleinfo', params: { page: 'bo-ticket-manage-asset-vehicle-info', navLevel: 3 } },
                { url: 'ticket/manage/asset/detail/vehicleinfo', params: { page: 'bo-ticket-manage-asset-detail-vehicle-info', navLevel: 4 } },
                
                { url: 'ticket/manage/ticket-new', params: { page: 'bo-ticket-manage-ticket-new', navLevel: 3 } },
                { url: 'ticket/manage/ticket-ack', params: { page: 'bo-ticket-manage-ticket-ack', navLevel: 3 } },
                { url: 'ticket/manage/ticket-inprog', params: { page: 'bo-ticket-manage-ticket-inprog', navLevel: 3 } },
                { url: 'ticket/manage/ticket-confirm', params: { page: 'bo-ticket-manage-ticket-confirm', navLevel: 3 } },
                { url: 'ticket/manage/ticket-pending', params: { page: 'bo-ticket-manage-ticket-pending', navLevel: 3 } },
                { url: 'ticket/manage/ticket-follow', params: { page: 'bo-ticket-manage-ticket-follow', navLevel: 3 } },
                { url: 'ticket/manage/ticket-close', params: { page: 'bo-ticket-manage-ticket-close', navLevel: 3 } },
                { url: 'ticket/manage/view-error', params: { page: 'bo-ticket-manage-ticket-view-error', navLevel: 3 } },

                { url: 'ticket/manage/dashboard', params: { page: 'bo-ticket-manage-dashboard', navLevel: 2 } },
                { url: 'ticket/manage/search', params: { page: 'bo-ticket-searchDashboard', navLevel: 3} },
               
                { url: 'ticket/manage/installation', params: { page: 'bo-ticket-manage-installation', navLevel: 2 } },

                { url: 'ticket/manage/appointments', params: { page: 'bo-ticket-manage-appointments', navLevel: 2 } },
                { url: 'ticket/manage/appointments/view', params: { page: 'bo-ticket-manage-appointments-view', navLevel: 3 } },

                { url: 'ticket/manage/mail', params: { page: 'bo-ticket-manage-mail', navLevel: 2 } },
                { url: 'ticket/manage/mail/create',params: { page: 'bo-ticket-manage-mail-manage', mode:'create',navLevel:3 } },
                { url: 'ticket/manage/mail/create',params: { page: 'bo-ticket-manage-mail-manage', mode:'update',navLevel:3 } },


                { url: 'ticket/manage/sim', params: { page: 'bo-ticket-manage-sim', navLevel: 2 } },
                { url: 'ticket/manage/sim/search', params: { page: 'bo-ticket-manage-sim-search', navLevel: 3 } },
                { url: 'ticket/manage/sim/view', params: { page: 'bo-ticket-manage-sim-view', navLevel: 3 } },
                { url: 'ticket/manage/sim/import', params: { page: 'bo-ticket-manage-sim-import', navLevel: 3 } },
                { url: 'ticket/manage/sim/manage/create', params: { page: 'bo-ticket-manage-sim-manage', mode: 'create', navLevel: 3 } },
                { url: 'ticket/manage/sim/manage/update', params: { page: 'bo-ticket-manage-sim-manage', mode: 'update', navLevel: 3 } },

                { url: 'ticket/manage/sim/manage/export-with-criteria', params: { page: 'bo-ticket-manage-export-with-criteria', navLevel: 4 } },

                //Ticket Sim Stock
                { url: 'ticket/manage/sim-stock', params: { page: 'bo-ticket-manage-sim-stock', navLevel: 2 } },
                { url: 'ticket/manage/sim-stock/manage/create', params: { page: 'bo-ticket-manage-sim-stock-manage', mode: 'create', navLevel: 3 } },
                { url: 'ticket/manage/sim-stock/manage/update', params: { page: 'bo-ticket-manage-sim-stock-manage', mode: 'update', navLevel: 3 } },

                //Ticket Operator Package
                { url: 'ticket/manage/operator-package', params: { page: 'bo-ticket-manage-package', navLevel: 2 }, permission: 'AssetManagement' },
                { url: 'ticket/manage/operator-package/create', params: { page: 'bo-ticket-manage-package-manage', mode: 'create', navLevel: 3 }, permission: 'CreateOperatorPackage' },
                { url: 'ticket/manage/operator-package/update', params: { page: 'bo-ticket-manage-package-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateOperatorPackage' },

                // Ticket - Vendors
                { url: 'ticket/vendors', params: { page: 'bo-ticket-manage-vendor', navLevel: 2 } },
                { url: 'ticket/vendors/create', params: { page: 'bo-ticket-manage-vendor-manage', mode: 'create', navLevel: 3 } },
                { url: 'ticket/vendors/update', params: { page: 'bo-ticket-manage-vendor-manage', mode: 'update', navLevel: 3 } },
                { url: 'ticket/vendors/signature/create', params: { page: 'bo-ticket-manage-vendor-manage-signature', mode: 'create', navLevel: 4 } },
                { url: 'ticket/vendors/signature/update', params: { page: 'bo-ticket-manage-vendor-manage-signature', mode: 'update', navLevel: 4 } },

                // Ticket - Transporter
                { url: 'ticket/transporter', params: { page: 'bo-ticket-manage-transporter', navLevel: 2 } },
                { url: 'ticket/transporter/create', params: { page: 'bo-ticket-manage-transporter-manage', mode: 'create', navLevel: 3 } },
                { url: 'ticket/transporter/update', params: { page: 'bo-ticket-manage-transporter-manage', mode: 'update', navLevel: 3 } },
                { url: 'ticket/transporter/vehicle', params: { page: 'bo-ticket-manage-transporter-manage-vehicle', navLevel: 3 } },
                { url: 'ticket/transporter/find-vehicle', params: { page: 'bo-ticket-manage-transporter-manage-find-vehicle', navLevel: 4 } },
                
                //Ticket - Search
                { url: 'ticket/search', params: { page: 'bo-ticket-search', navLevel: 2 } },
                { url: 'ticket/manage/search', params: { page: 'bo-ticket-manage-search', navLevel: 3 } },


                //About
                { url: 'about', params: { page: 'bo-about', navLevel: 1 } },
            ]
        },
        {
            name: "company-admin",
            routes: [
                // Contract
                { url: 'contract', params: { page: 'ca-contract', navLevel: 1 } },
                { url: 'contract/search', params: { page: 'ca-contract-search', navLevel: 2 } },
                { url: 'contract/create', params: { page: 'ca-contract-manage', mode: 'create' , navLevel: 3 } },
                { url: 'contract/view', params: { page: 'ca-contract-view', navLevel: 2 } },
                { url: 'contract/view/update', params: { page: 'ca-contract-manage', mode: 'update', navLevel: 3 } },
                { url: 'contract/view/create-from-copy', params: { page: 'ca-contract-manage', mode: 'createFromCopy', navLevel: 3 } },
                // Business Unit
                { url: 'business-unit-menu', params: { page: 'ca-business-unit-menu', navLevel: 1 }, permission: 'BusinessUnitManagement' },
                { url: 'business-unit', params: { page: 'ca-business-unit', navLevel: 2 }, permission: 'BusinessUnitManagement' },
                { url: 'business-unit/view', params: { page: 'ca-business-unit-view', navLevel: 3 }, permission: 'BusinessUnitManagement' },
                { url: 'business-unit/create', params: { page: 'ca-business-units-hierarchy-manage', mode: 'create', navLevel: 4 }, permission: 'CreateBusinessUnit' },
                { url: 'business-unit/view/update', params: { page: 'ca-business-units-hierarchy-manage', mode: 'update', navLevel: 4 }, permission: 'UpdateBusinessUnit' },
                
                { url: 'business-units-levels/view', params: { page: 'ca-business-units-levels', navLevel: 3 }},
                { url: 'business-units-levels/create', params: { page: 'ca-business-units-levels-manage', mode: 'create', navLevel: 4 }},
                { url: 'business-units-levels/update', params: { page: 'ca-business-units-levels-manage', mode: 'update', navLevel: 4 }},
                
                { url: 'business-unit/create/authorized-persons', params: { page: 'ca-business-units-authorized-persons', navLevel: 5 }, mode: 'create',},
                { url: 'business-unit/view/update/authorized-persons', params: { page: 'ca-business-units-authorized-persons', navLevel: 5 },mode: 'update', },

                // Group
                { url: 'group', params: { page: 'ca-group', navLevel: 1 }, permission: 'GroupManagement' },
                { url: 'group/create', params: { page: 'ca-group-manage', mode: 'create', navLevel: 3 }, permission: 'CreateGroup_Company' },
                { url: 'group/view', params: { page: 'ca-group-view', navLevel: 2 }, permission: 'GroupManagement' },
                { url: 'group/view/update', params: { page: 'ca-group-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateGroup_Company' },
                // User
                { url: 'user', params: { page: 'ca-user', navLevel: 1 }, permission: 'UserManagement_Company' },
                { url: 'user/search', params: { page: 'ca-user-search', navLevel: 2 }, permission: 'UserManagement_Company' },
                { url: 'user/create', params: { page: 'ca-user-manage', mode: 'create', navLevel: 3 }, permission: 'CreateUser_Company' },
                { url: 'user/create/group', params: { page: 'ca-user-manage-group-select', mode: 'ca-user-create', navLevel: 4 } },
                { url: 'user/create/select-accessible-business-unit', params: { page: 'ca-user-manage-accessible-business-unit-select', mode: 'ca-user-create', navLevel: 4 } },
                { url: 'user/view', params: { page: 'ca-user-view', navLevel: 2 }, permission: 'UserManagement_Company' },
                { url: 'user/view/update', params: { page: 'ca-user-manage', mode: 'update', navLevel: 3 }, permission: 'UpdateUser_Company' },
                { url: 'user/view/update/group', params: { page: 'ca-user-manage-group-select', mode: 'ca-user-update', navLevel: 4 } },
                { url: 'user/view/update/select-accessible-business-unit', params: { page: 'ca-user-manage-accessible-business-unit-select', mode: 'ca-user-update', navLevel: 4 } },
                { url: 'user/view/reset-password', params: { page: 'ca-user-reset-password', navLevel: 3 }, permission: 'UserManagement_Company' },
                // Asset
                { url: 'asset', params: { page: 'ca-asset', navLevel: 1 }, permission: 'Asset' },
                // Asset - Box
                { url: 'asset/box', params: { page: 'ca-asset-box', navLevel: 2 }, permission: 'Asset' },
                { url: 'asset/box/search', params: { page: 'ca-asset-box-search', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/box/view', params: { page: 'ca-asset-box-view', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/box/view/contract', params: { page: 'ca-asset-box-view-contract', navLevel: 4 }, permission: 'Asset' },
                { url: 'asset/box/view/maintenance', params: { page: 'ca-asset-box-view-maintenance', navLevel: 4 }, permission: 'Asset' },
                { url: 'asset/box/view/maintenance/add', params: { page: 'ca-asset-box-view-maintenance-manage', mode: 'create', navLevel: 6 }, permission: 'CreateBoxMaintenance' },
                { url: 'asset/box/view/maintenance/view', params: { page: 'ca-asset-box-view-maintenance-view', navLevel: 5 }, permission: 'Asset' },
                { url: 'asset/box/view/maintenance/view/update', params: { page: 'ca-asset-box-view-maintenance-manage', mode: 'update', navLevel: 6 }, permission: 'UpdateBoxMaintenance' },
                // Asset - Vehicle
                { url: 'asset/vehicle', params: { page: 'ca-asset-vehicle', navLevel: 2 }, permission: 'Asset' },
                { url: 'asset/vehicle/search', params: { page: 'ca-asset-vehicle-search', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/vehicle/create', params: { page: 'ca-asset-vehicle-manage', mode: 'create', navLevel: 4 }, permission: 'CreateVehicle' },
                { url: 'asset/vehicle/create/license-detail', params: { page: 'ca-asset-vehicle-manage-license-detail-manage', mode: 'create', navLevel: 5 }, permission: 'CreateVehicle' },
                { url: 'asset/vehicle/create/default-driver', params: { page: 'ca-asset-vehicle-manage-default-driver-manage', mode: 'create', navLevel: 5 }, permission: 'CreateVehicle' },
                { url: 'asset/vehicle/create/alert', params: { page: 'ca-asset-vehicle-manage-alert-manage', mode: 'create', navLevel: 5 }, permission: 'CreateVehicle' },
                { url: 'asset/vehicle/import', params: { page: 'ca-asset-vehicle-import', navLevel: 3 }, permissionsAnd: ['CreateVehicle', 'UpdateVehicle'] },
                { url: 'asset/vehicle/view', params: { page: 'ca-asset-vehicle-view', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/vehicle/view/update', params: { page: 'ca-asset-vehicle-manage', mode: 'update', navLevel: 4 }, permission: 'UpdateVehicle' },
                { url: 'asset/vehicle/view/update/license-detail', params: { page: 'ca-asset-vehicle-manage-license-detail-manage', mode: 'update', navLevel: 5 }, permission: 'UpdateVehicle' },
                { url: 'asset/vehicle/view/update/default-driver', params: { page: 'ca-asset-vehicle-manage-default-driver-manage', mode: 'update', navLevel: 5 }, permission: 'UpdateVehicle' },
                { url: 'asset/vehicle/view/update/alert', params: { page: 'ca-asset-vehicle-manage-alert-manage', mode: 'update', navLevel: 5 }, permission: 'UpdateVehicle' },
                { url: 'asset/vehicle/view/maintenance-plan', params: { page: 'ca-asset-vehicle-view-maintenance-plan', navLevel: 4 }, permission: 'Asset' },
                { url: 'asset/vehicle/view/maintenance-plan/create', params: { page: 'ca-asset-vehicle-view-maintenance-plan-manage', mode: 'create', navLevel: 5 }, permission: 'CreateVehicleMaintenance' },
                { url: 'asset/vehicle/view/maintenance-plan/create-log', params: { page: 'ca-asset-vehicle-view-maintenance-plan-create-log', mode: 'create', navLevel: 5 }, permission: 'CreateVehicleMaintenance' },
                { url: 'asset/vehicle/view/maintenance-plan/view', params: { page: 'ca-asset-vehicle-view-maintenance-plan-view', navLevel: 5 }, permission: 'Asset' },
                { url: 'asset/vehicle/view/maintenance-plan/view-log', params: { page: 'ca-asset-vehicle-view-maintenance-plan-view-log', navLevel: 5 }, permission: 'Asset' },
                { url: 'asset/vehicle/view/maintenance-plan/view-cancel', params: { page: 'ca-asset-vehicle-view-maintenance-plan-view-cancel', navLevel: 5 }, permission: 'Asset' },
                { url: 'asset/vehicle/view/maintenance-plan/view/update', params: { page: 'ca-asset-vehicle-view-maintenance-plan-manage', mode: 'update', navLevel: 6 }, permission: 'UpdateVehicleMaintenance' },
                { url: 'asset/vehicle/view/maintenance-plan/view/complete', params: { page: 'ca-asset-vehicle-view-maintenance-plan-complete', navLevel: 6 }, permission: 'UpdateVehicleMaintenance' },
                { url: 'asset/vehicle/view/maintenance-plan/view/cancel', params: { page: 'ca-asset-vehicle-view-maintenance-plan-cancel', navLevel: 6 }, permission: 'UpdateVehicleMaintenance' },
                { url: 'asset/vehicle/view/odo-meter', params: { page: 'ca-asset-vehicle-view-odo-meter-manage', navLevel: 4 }, permission: 'UpdateVehicle' },
                { url: 'asset/vehicle/view/contract', params: { page: 'ca-asset-vehicle-view-contract', navLevel: 4 }, permission: 'Asset' },
                { url: 'asset/vehicle/dms-detail', params: { page: 'ca-asset-vehicle-dms-detail', navLevel: 4 } },
                { url: 'asset/vehicle/adas-detail', params: { page: 'ca-asset-vehicle-adas-detail', navLevel: 4 } },
                { url: 'asset/vehicle/view/odo-meter-fixing', params: { page: 'ca-asset-vehicle-view-odo-meter-fixing', navLevel: 4 }, permission: 'UpdateVehicle' },
                                
                // Asset - Driver
                { url: 'asset/driver', params: { page: 'ca-asset-driver', navLevel: 2 }, permission: 'Asset' },
                { url: 'asset/driver/search', params: { page: 'ca-asset-driver-search', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/driver/create', params: { page: 'ca-asset-driver-manage', mode: 'create', navLevel: 4 }, permission: 'CreateDriver' },
                { url: 'asset/driver/import', params: { page: 'ca-asset-driver-import', navLevel: 3 }, permissionsAnd: ['CreateDriver', 'UpdateDriver'] },
                { url: 'asset/driver/import-picture', params: { page: 'ca-asset-driver-import-picture', navLevel: 3 }, permission: 'UpdateDriver' },
                { url: 'asset/driver/view', params: { page: 'ca-asset-driver-view', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/driver/view/update', params: { page: 'ca-asset-driver-manage', mode: 'update', navLevel: 4 }, permission: 'UpdateDriver' },
                { url: 'asset/driver/view/user/create', params: { page: 'ca-user-manage', mode: 'create', path: 'driver', navLevel: 4 }, permission: 'CreateUser_Company' },
                { url: 'asset/driver/view/user/create/group', params: { page: 'ca-user-manage-group-select', mode: 'ca-user-create', path: 'driver', navLevel: 5 } },
                { url: 'asset/driver/view/user/create/select-accessible-business-unit', params: { page: 'ca-user-manage-accessible-business-unit-select', mode: 'ca-user-create', path: 'driver', navLevel: 5 } },
                { url: 'asset/driver/driver-card', params: { page: 'ca-asset-driver-card', navLevel: 5 }  },

                // Asset - Fleet Service
                { url: 'asset/fleet-service', params: { page: 'ca-asset-fleet-service', navLevel: 2 }, permission: 'Asset' },
                { url: 'asset/fleet-service/search', params: { page: 'ca-asset-fleet-service-search', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/fleet-service/create', params: { page: 'ca-asset-fleet-service-manage', mode: 'create', navLevel: 4 }, permission: 'CreateFleetService' },
                { url: 'asset/fleet-service/view', params: { page: 'ca-asset-fleet-service-view', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/fleet-service/view/update', params: { page: 'ca-asset-fleet-service-manage', mode: 'update', navLevel: 4 }, permission: 'UpdateFleetService' },
                // Asset - Mobile Service
                { url: 'asset/mobile-service', params: { page: 'ca-asset-mobile-service', navLevel: 2 }, permission: 'Asset' },
                { url: 'asset/mobile-service/search', params: { page: 'ca-asset-mobile-service-search', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/mobile-service/create', params: { page: 'ca-asset-mobile-service-manage', mode: 'create', navLevel: 4 }, permission: 'CreateMobileService' },
                { url: 'asset/mobile-service/view', params: { page: 'ca-asset-mobile-service-view', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/mobile-service/view/update', params: { page: 'ca-asset-mobile-service-manage', mode: 'update', navLevel: 4 }, permission: 'UpdateMobileService'},
                // Asset - SMS Service
                { url: 'asset/sms-service', params: { page: 'ca-asset-sms-service', navLevel: 2 }, permission: 'Asset' },
                { url: 'asset/sms-service/search', params: { page: 'ca-asset-sms-service-search', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/sms-service/create', params: { page: 'ca-asset-sms-service-manage', mode: 'create', navLevel: 4 }, permission: 'CreateSMSService' },
                { url: 'asset/sms-service/view', params: { page: 'ca-asset-sms-service-view', navLevel: 3 }, permission: 'Asset' },
                { url: 'asset/sms-service/view/update', params: { page: 'ca-asset-sms-service-manage', mode: 'update', navLevel: 4 }, permission: 'UpdateSMSService' },

                // Asset - Driver Performance Rule
                { url: 'driver-performance-rule-menu', params: { page: 'ca-driver-performance-rule-menu', navLevel: 1 } },
                { url: 'driver-performance-rule', params: { page: 'ca-driver-performance-rule', navLevel: 2 } },
                { url: 'driver-performance-rule/search', params: { page: 'ca-driver-performance-rule-search', navLevel: 3 } },
                { url: 'driver-performance-rule/create', params: { page: 'ca-driver-performance-rule-manage', mode: 'create', navLevel: 3 } },
                { url: 'driver-performance-rule/view', params: { page: 'ca-driver-performance-rule-view', navLevel: 3 } },
                { url: 'driver-performance-rule/view/update', params: { page: 'ca-driver-performance-rule-manage', mode: 'update', navLevel: 4 } },

                // Asset - Advance Driver Performance Rule
                { url: 'advance-driver-performance-rule', params: { page: 'ca-advance-driver-performance-rule', navLevel: 2 } },
                { url: 'advance-driver-performance-rule/create', params: { page: 'ca-advance-driver-performance-rule-manage', mode: 'create', navLevel: 3 } },
                { url: 'advance-driver-performance-rule/view/update', params: { page: 'ca-advance-driver-performance-rule-manage', mode: 'update', navLevel: 4 } },
                { url: 'advance-driver-performance-rule/view', params: { page: 'ca-advance-driver-performance-rule-view', navLevel: 3 } },
                { url: 'advance-driver-performance-rule/create/select-accessible-businessunit', params: { page: 'ca-advance-driver-performance-rule-accessible-bu-select', mode: 'create', navLevel: 4 } },
                { url: 'advance-driver-performance-rule/view/update/select-accessible-businessunit', params: { page: 'ca-advance-driver-performance-rule-accessible-bu-select', mode: 'update', navLevel: 5 } },
                { url: 'advance-driver-performance-rule/create/speed', params: { page: 'ca-advance-driver-performance-rule-speed', mode: 'create', navLevel: 4 } },
                { url: 'advance-driver-performance-rule/view/update/speed', params: { page: 'ca-advance-driver-performance-rule-speed', mode: 'update', navLevel: 5 } },
                { url: 'advance-driver-performance-rule/create/behavior', params: { page: 'ca-advance-driver-performance-rule-behavior', mode: 'create', navLevel: 4 } },
                { url: 'advance-driver-performance-rule/view/update/behavior', params: { page: 'ca-advance-driver-performance-rule-behavior', mode: 'update', navLevel: 5 } },
                { url: 'advance-driver-performance-rule/create/fuel', params: { page: 'ca-advance-driver-performance-rule-fuel', mode: 'create', navLevel: 4 } },
                { url: 'advance-driver-performance-rule/view/update/fuel', params: { page: 'ca-advance-driver-performance-rule-fuel', mode: 'update', navLevel: 5 } },

                // Trainer
                { url:"asset/trainer", params: { page: "ca-asset-trainer", navLevel: 2 }},
                { url:"asset/trainer/import", params: { page: "ca-asset-trainer-import", navLevel: 3 } },
                { url:"asset/trainer/import-picture", params: { page: "ca-asset-trainer-import-picture", navLevel: 3 } },
                { url:"asset/trainer/search", params: { page: "ca-asset-trainer-search", navLevel: 3 } },
                { url:"asset/trainer/create", params: { page: "ca-asset-trainer-manage", navLevel: 3, mode: 'create' } },
                { url:"asset/trainer/view", params: { page: "ca-asset-trainer-view", navLevel: 3 } },
                { url: "asset/trainer/view/update", params: { page: "ca-asset-trainer-manage", mode: 'update', navLevel: 4 } }, 


                // Configuration
                { url: 'configuration', params: { page: 'ca-configuration', navLevel: 1 }, permission: 'Configuration_Company' },

                // Announcement
                { url: 'configuration/announcement', params: { page: 'ca-configuration-announcement', navLevel: 2 } },
                { url: 'configuration/announcement/search', params: { page: 'ca-configuration-announcement-search', navLevel: 3 } },
                { url: 'configuration/announcement/create', params: { page: 'ca-configuration-announcement-manage', mode: 'create', navLevel: 3 } },
                { url: 'configuration/announcement/update', params: { page: 'ca-configuration-announcement-manage', mode: 'update', navLevel: 4 } },
                { url: 'configuration/announcement/view', params: { page: 'ca-configuration-announcement-view', navLevel: 3 } },
                { url: 'configuration/announcement/message', params: { page: 'ca-configuration-announcement-manage-message-manage' ,navLevel: 5 } },
                { url: 'configuration/announcement/create/select-accessible-businessunit', params: { page: 'ca-configuration-accessible-bu-select', mode: 'create', navLevel: 4 } },
                { url: 'configuration/announcement/update/select-accessible-businessunit', params: { page: 'ca-configuration-accessible-bu-select', mode: 'update', navLevel: 5 } },

                // Configuration - Alert
                { url: 'configuration/alert', params: { page: 'ca-configuration-alert', navLevel: 2 }, permission: 'Configuration_Company' },
                { url: 'configuration/alert/search', params: { page: 'ca-configuration-alert-search', navLevel: 3 }, permission: 'Configuration_Company' },
                { url: 'configuration/alert/create', params: { page: 'ca-configuration-alert-manage', mode: 'create', navLevel: 4 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/select-asset', params: { page: 'ca-configuration-alert-manage-asset-select', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/areasetting-select', params: { page: 'ca-configuration-alert-manage-areasetting-select', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/view', params: { page: 'ca-configuration-alert-view', navLevel: 3 }, permission: 'Configuration_Company' },
                { url: 'configuration/alert/view/update', params: { page: 'ca-configuration-alert-manage', mode: 'update', navLevel: 4 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/view/update/select-asset', params: { page: 'ca-configuration-alert-manage-asset-select', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/areasetting-select', params: { page: 'ca-configuration-alert-manage-areasetting-select', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/datetimesetting-select', params: { page: 'ca-configuration-alert-manage-datetimesetting-select', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/datetimesetting-select', params: { page: 'ca-configuration-alert-manage-datetimesetting-select', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/create-allow', params: { page: 'ca-configuration-alert-manage-allow', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/create-allow', params: { page: 'ca-configuration-alert-manage-allow', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/create-not-allow', params: { page: 'ca-configuration-alert-manage-not-allow', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/create-not-allow', params: { page: 'ca-configuration-alert-manage-not-allow', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/create-stop-overtime-province', params: { page: 'ca-configuration-alert-manage-stop-overtime-province', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/create-stop-overtime-province', params: { page: 'ca-configuration-alert-manage-stop-overtime-province', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/find-asset', params: { page: 'ca-configuration-alert-manage-find-asset', mode: 'create', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/view/update/find-asset', params: { page: 'ca-configuration-alert-manage-find-asset', mode: 'update', navLevel: 5 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/asset-result', params: { page: 'ca-configuration-alert-manage-asset-result', navLevel: 4 }, permission: 'AlertManagement' },
                { url: 'configuration/alert/create/asset-businessunit', params: { page: 'ca-configuration-alert-manage-asset-businessunit', navLevel: 5 }, permission: 'AlertManagement' },
                
                
                // Configuration - Alert Categories
                { url: 'configuration/alert-categories', params: { page: 'ca-configuration-alert-categories', navLevel: 2 }, permission: 'Configuration_Company' },
                { url: 'configuration/alert-categories/create', params: { page: 'ca-configuration-alert-categories-manage', navLevel: 3, mode: 'create'}, permission: 'Configuration_Company' },
                { url: 'configuration/alert-categories/search', params: { page: 'ca-configuration-alert-categories-search', navLevel: 3}, permission: 'Configuration_Company' },
                { url: 'configuration/alert-categories/view', params: { page: 'ca-configuration-alert-categories-view', navLevel: 3}, permission: 'Configuration_Company' },
                { url: 'configuration/alert-categories/view/update', params: { page: 'ca-configuration-alert-categories-manage', navLevel: 4, mode: 'update' }, permission: 'Configuration_Company' },
                // Configuration - POI Icon
                { url: 'configuration/poi-icon', params: { page: 'ca-configuration-poi-icon', navLevel: 2 }, permission: 'Configuration_Company' },
                { url: 'configuration/poi-icon/create', params: { page: 'ca-configuration-poi-icon-manage', mode: 'create', navLevel: 4 }, permission: 'POIIconsManagement_Company' },
                { url: 'configuration/poi-icon/view', params: { page: 'ca-configuration-poi-icon-view', navLevel: 3 }, permission: 'POIIconsManagement_Company' },
                { url: 'configuration/poi-icon/view/update', params: { page: 'ca-configuration-poi-icon-manage', mode: 'update', navLevel: 4 }, permission: 'POIIconsManagement_Company' },
                // Configuration - Translation
                { url: 'configuration/translation', params: { page: 'ca-configuration-translation', navLevel: 2 }, permission: 'Configuration_Company' },
                { url: 'configuration/translation/import', params: { page: 'ca-configuration-translation-import', navLevel: 3 }, permission: 'TranslationManagement' },
                { url: 'configuration/translation/export', params: { page: 'ca-configuration-translation-export', navLevel: 3 }, permission: 'Configuration_Company' },
                // Configuration - Barrier
                { url: 'configuration/barrier', params: { page: 'ca-configuration-barrier', navLevel: 2 }, permission: 'Configuration_Company' },
                { url: 'configuration/barrier/create', params: { page: 'ca-configuration-barrier-manage', mode: "create", navLevel: 3 }, permission: 'Configuration_Company' },
                { url: 'configuration/barrier/view', params: { page: 'ca-configuration-barrier-view', navLevel: 3 }, permission: 'Configuration_Company' },
                { url: 'configuration/barrier/view/update', params: { page: 'ca-configuration-barrier-manage', mode: "update" , navLevel: 4 }, permission: 'Configuration_Company' },
                // Configuration - Ticket Response
                { url: 'configuration/ticket-response', params: { page: 'ca-configuration-ticket-response', navLevel: 2 }, permission: 'Configuration_Company' },
                { url: 'configuration/ticket-response/create', params: { page: 'ca-configuration-ticket-response-manage', mode: "create", navLevel: 3 }, permission: 'Configuration_Company' },
                { url: 'configuration/ticket-response/view', params: { page: 'ca-configuration-ticket-response-view', navLevel: 3 }, permission: 'Configuration_Company' },
                { url: 'configuration/ticket-response/view/update', params: { page: 'ca-configuration-ticket-response-manage', mode: "update", navLevel: 4 }, permission: 'Configuration_Company' },                
                
                // Configuration - Telematices Configuration
                { url: 'configuration/telematices-configuration', params: { page: 'ca-configuration-telematices-configuration', navLevel: 2 } },
                
                // Maintenance Management
                { url: 'asset/maintenance-management', params: { page: 'ca-asset-maintenance-management', navLevel: 2 }},

                { url: 'asset/maintenance-management/plan', params: { page: 'ca-asset-maintenance-management-ma-plan', navLevel: 3 }},
                { url: 'asset/maintenance-management/plan/search', params: { page: 'ca-asset-maintenance-management-ma-plan-search', navLevel: 4 }},
                { url: 'asset/maintenance-management/plan/create', params: { page: 'ca-asset-maintenance-management-ma-plan-create', navLevel: 4 }},
                { url: 'asset/maintenance-management/plan/create/select-vehicles', params: { page: 'ca-asset-maintenance-management-ma-plan-create-vehicle', navLevel: 5 }},
                { url: 'asset/maintenance-management/plan/import', params: { page: 'ca-asset-maintenance-management-ma-plan-import', navLevel: 4 }},
                { url: 'asset/maintenance-management/plan/export', params: { page: 'ca-asset-maintenance-management-ma-plan-export', navLevel: 4 }},
                
                { url: 'asset/maintenance-management/maintenance-type', params: { page: 'ca-asset-maintenance-management-ma-type', navLevel: 3 }},
                { url: 'asset/maintenance-management/maintenance-type/create', params: { page: 'ca-asset-maintenance-management-ma-type-manage', mode: "create", navLevel: 4 }},
                { url: 'asset/maintenance-management/maintenance-type/create/import', params: { page: 'ca-asset-maintenance-management-ma-type-manage-import', navLevel: 5 }},
                { url: 'asset/maintenance-management/maintenance-type/view', params: { page: 'ca-asset-maintenance-management-ma-type-view', navLevel: 4 }},
                { url: 'asset/maintenance-management/maintenance-type/view/update', params: { page: 'ca-asset-maintenance-management-ma-type-manage', mode: "update", navLevel: 5 }},
                { url: 'asset/maintenance-management/maintenance-type/view/maintenance-subtype', params: { page: 'ca-asset-maintenance-management-ma-type-view-subtype', navLevel: 5 }},
                { url: 'asset/maintenance-management/maintenance-type/view/maintenance-subtype/create', params: { page: 'ca-asset-maintenance-management-ma-type-view-subtype-manage', mode: "create", navLevel: 6 }},
                { url: 'asset/maintenance-management/maintenance-type/view/maintenance-subtype/view', params: { page: 'ca-asset-maintenance-management-ma-type-view-subtype-view', navLevel: 6 }},
                { url: 'asset/maintenance-management/maintenance-type/view/maintenance-subtype/view/update', params: { page: 'ca-asset-maintenance-management-ma-type-view-subtype-manage', mode: "update",navLevel: 7 }},
            
                { url: 'asset/maintenance-management/appointment', params: { page: 'ca-asset-maintenance-management-appointment', navLevel: 3 }},
                { url: 'asset/maintenance-management/appointment/search', params: { page: 'ca-asset-maintenance-management-appointment-search', navLevel: 4 }},
                { url: 'asset/maintenance-management/appointment/create', params: { page: 'ca-asset-maintenance-management-appointment-manage', mode: "create", navLevel: 4 }},
                { url: 'asset/maintenance-management/appointment/view', params: { page: 'ca-asset-maintenance-management-appointment-view', navLevel: 4 }},
                { url: 'asset/maintenance-management/appointment/view/update', params: { page: 'ca-asset-maintenance-management-appointment-manage', mode: "update", navLevel: 5 }},
                { url: 'asset/maintenance-management/appointment/view/postpone', params: { page: 'ca-asset-maintenance-management-appointment-view-postpone', navLevel: 5 }},
                { url: 'asset/maintenance-management/appointment/view/cancel', params: { page: 'ca-asset-maintenance-management-appointment-view-cancel', navLevel: 5 }},

                { url: 'asset/maintenance-management/report', params: { page: 'ca-asset-maintenance-management-report', navLevel: 3 }},
                { url: 'asset/maintenance-management/report/reportviewer', params: { page: 'ca-asset-maintenance-management-report-reportviewer', navLevel: 4 }},

                // Report
                { url: 'report', params: { page: 'ca-report', navLevel: 1 } },
                // reportViewer
                { url: 'report/reportviewer', params: { page: 'ca-report-reportviewer', navLevel: 3 } },
                 // Report - userlog
                { url: 'report/userlog', params: { page: 'ca-report-userlog', navLevel: 2 } },
            ]
        },
        {
            name: "company-workspace",
            routes: [
                { url: 'map', params: { page: null } },
                // Notification
                { url: 'notification-alert', params: { page: 'cw-notification-alert', navLevel: 1 } },
                { url: 'notification-urgent-alert', params: { page: 'cw-notification-urgent-alert', navLevel: 1 } },
                { url: 'notification-announcement', params: { page: 'cw-notification-announcement', navLevel: 1 } },
                { url: 'notification-maintenance-plan', params: { page: 'cw-notification-maintenance-plan', navLevel: 1 } },
                // User Preference
                { url: 'user-preference', params: { page: 'cw-user-preference', navLevel: 1 } },
                // Global Search
                { url: 'global-search', params: { page: 'cw-global-search', navLevel: 1 }, permission: 'GlobalSearch' },
                { url: 'global-search/find', params: { page: 'cw-global-search-find', navLevel: 2 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result', params: { page: 'cw-global-search-result', navLevel: 3 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-custom-area', params: { page: 'cw-geo-fencing-custom-area-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-custom-poi', params: { page: 'cw-geo-fencing-custom-poi-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-custom-route', params: { page: 'cw-geo-fencing-custom-route-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-businessunit', params: { page: 'ca-business-unit-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-vehicle', params: { page: 'ca-asset-vehicle-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-driver', params: { page: 'ca-asset-driver-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                { url: 'global-search/find/result/view-user', params: { page: 'ca-user-view', mode: 'readonly', navLevel: 4 }, permission: 'GlobalSearch' },
                // Fleet Monitoring
                { url: 'fleet-monitoring', params: { page: 'cw-fleet-monitoring', navLevel: 1 }, permission: 'FleetMonitoring' },
                // Fleet Monitoring - Track Monitoring
                { url: 'fleet-monitoring/track', params: { page: 'cw-fleet-monitoring-track', navLevel:2 }, permission: 'TrackMonitoring' },
                { url: 'fleet-monitoring/track/find-asset', params: { page: 'cw-fleet-monitoring-track-asset-find', navLevel:3 }, permission: 'TrackMonitoring' },
                { url: 'fleet-monitoring/track/find-asset/result', params: { page: 'cw-fleet-monitoring-track-asset-find-result', navLevel:4 }, permission: 'TrackMonitoring' },
                // Fleet Monitoring - Alert Monitoring
                { url: 'fleet-monitoring/alert', params: { page: 'cw-fleet-monitoring-alert', navLevel:2 }, permission: 'ViewAlert' },
                { url: 'fleet-monitoring/alert/list', params: { page: 'cw-fleet-monitoring-alert-list', navLevel:3 }, permission: 'ViewAlert' },
                { url: 'fleet-monitoring/alert/list/search', params: { page: 'cw-fleet-monitoring-alert-list-search', navLevel:4 }, permission: 'ViewAlert' },
                // Fleet Monitoring - Alert Monitoring - Ticket
                { url: 'fleet-monitoring/alert/ticket', params: { page: 'cw-fleet-monitoring-alert-ticket', navLevel:3 }, permission: 'ViewAlert' },
                { url: 'fleet-monitoring/alert/ticket/search', params: { page: 'cw-fleet-monitoring-alert-ticket-search', navLevel:4 }, permission: 'ViewAlert' },
                { url: 'fleet-monitoring/alert/ticket/action', params: { page: 'cw-fleet-monitoring-alert-ticket-action', navLevel:4 }, permission: 'ViewAlert' },                
                // Fleet Monitoring - Playback
                { url: 'fleet-monitoring/playback', params: { page: 'cw-fleet-monitoring-playback', navLevel:2 }, permission: 'ViewPlayback' },
                { url: 'fleet-monitoring/playback/timeline', params: { page: 'cw-fleet-monitoring-playback-timeline', navLevel:3 }, permission: 'ViewPlayback' },                
                // Fleet Monitorting - Live View
                { url: 'fleet-monitoring/livevideo', params: { page: 'cw-fleet-monitoring-liveview', navLevel: 2 }, permission: 'ViewPlayback' },  
                // Fleet Monitorting - Event View
                { url: 'fleet-monitoring/eventvideo', params: { page: 'cw-fleet-monitoring-eventview', navLevel: 2 }, permission: 'ViewPlayback' },
                { url: 'fleet-monitoring/eventvideo/list', params: { page: 'cw-fleet-monitoring-eventview-list', navLevel: 3 }, permission: 'ViewPlayback' },
                // Fleet Monitoring - Event View - Ticket
                { url: 'fleet-monitoring/eventvideo/ticket', params: { page: 'cw-fleet-monitoring-eventview-ticket', navLevel:3 }, permission: 'ViewAlert' },
                { url: 'fleet-monitoring/eventvideo/ticket/search', params: { page: 'cw-fleet-monitoring-eventview-ticket-search', navLevel:4 }, permission: 'ViewAlert' },
                { url: 'fleet-monitoring/eventvideo/ticket/action', params: { page: 'cw-fleet-monitoring-eventview-ticket-action', navLevel:4 }, permission: 'ViewAlert' },                
                
                // Fleet Monitoring - Playback Analysis Tool
                { url: 'fleet-monitoring/playback-tools/search', params: { page: 'cw-fleet-monitoring-playback-analysis-tool-search', navLevel: 2 } },
                { url: 'fleet-monitoring/playback/analysis-tool', params: { page: 'cw-fleet-monitoring-playback-analysis-tool', navLevel:3 }},
                
                //Debrief
                { url: 'fleet-monitoring/debrief', params: { page: 'cw-fleet-monitoring-debrief', navLevel:2 }, permission: 'ViewDebrief' },
                { url: 'fleet-monitoring/debrief/view', params: { page: 'cw-fleet-monitoring-debrief-view', navLevel:3 }, permission: 'ViewDebrief' },
                // Fleet Monitoring - Trip Analysis
                { url: 'fleet-monitoring/tripanalysis', params: { page: 'cw-fleet-monitoring-tripanalysis', navLevel: 2 } },
                { url: 'fleet-monitoring/tripanalysis/report', params: { page: 'cw-fleet-monitoring-tripanalysis-report', navLevel: 3 } },

                // Geo-Fencing
                { url: 'geo-fencing', params: { page: 'cw-geo-fencing', navLevel: 1 } },
                // Geo-Fencing - Custom POI
                { url: 'geo-fencing/custom-poi', params: { page: 'cw-geo-fencing-custom-poi', navLevel: 2 } },
                { url: 'geo-fencing/custom-poi/search', params: { page: 'cw-geo-fencing-custom-poi-search', navLevel: 3 } },
                { url: 'geo-fencing/custom-poi/import', params: { page: 'cw-geo-fencing-custom-poi-import', navLevel: 3 } },
                { url: 'geo-fencing/custom-poi/create', params: { page: 'cw-geo-fencing-custom-poi-manage', mode: 'create', navLevel: 4 } },
                { url: 'geo-fencing/custom-poi/create/select-accessible-business-unit', params: { page: 'cw-geo-fencing-custom-poi-manage-accessible-business-unit-select', mode: 'create', navLevel: 5 } },
                { url: 'geo-fencing/custom-poi/create/sub-poi-area', params:{page:'cw-geo-fencing-custom-poi-manage-sub-poi-area', mode: 'create', navLevel: 6 }},
                { url: 'geo-fencing/custom-poi/view-subarea', params: { page: 'cw-geo-fencing-custom-poi-sub-area-view', navLevel: 4 } },
                { url: 'geo-fencing/custom-poi/view', params: { page: 'cw-geo-fencing-custom-poi-view', navLevel: 3 } },
                { url: 'geo-fencing/custom-poi/view/update', params: { page: 'cw-geo-fencing-custom-poi-manage', mode: 'update', navLevel: 4 } },
                { url: 'geo-fencing/custom-poi/view/update/select-accessible-business-unit', params: { page: 'cw-geo-fencing-custom-poi-manage-accessible-business-unit-select', mode: 'update', navLevel: 5 } },
                { url: 'geo-fencing/custom-poi/update/sub-poi-area', params:{page:'cw-geo-fencing-custom-poi-manage-sub-poi-area', mode: 'update', navLevel: 5 }},
                { url: 'geo-fencing/custom-poi/view/verify', params: { page: 'cw-geo-fencing-custom-poi-view-verify', navLevel: 4 } },
                // Geo-Fencing - Custom POI Category
                { url: 'geo-fencing/custom-poi-category', params: { page: 'cw-geo-fencing-custom-poi-category', navLevel: 2 } },
                { url: 'geo-fencing/custom-poi-category/create', params: { page: 'cw-geo-fencing-custom-poi-category-manage', mode: 'create', navLevel: 4 } },
                { url: 'geo-fencing/custom-poi-category/view', params: { page: 'cw-geo-fencing-custom-poi-category-view', navLevel: 3 } },
                { url: 'geo-fencing/custom-poi-category/view/update', params: { page: 'cw-geo-fencing-custom-poi-category-manage', mode: 'update', navLevel: 4 } },
                // Geo-Fencing - Custom Route
                { url: 'geo-fencing/custom-route', params: { page: 'cw-geo-fencing-custom-route', navLevel: 2 } },
                { url: 'geo-fencing/custom-route/search', params: { page: 'cw-geo-fencing-custom-route-search', navLevel: 3 } },
                { url: 'geo-fencing/custom-route/create', params: { page: 'cw-geo-fencing-custom-route-manage', mode: 'create', navLevel: 4 } },
                { url: 'geo-fencing/custom-route/create/select-accessible-business-unit', params: { page: 'cw-geo-fencing-custom-route-manage-accessible-business-unit-select', mode: 'create', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/create/custom-poi', params: { page: 'cw-geo-fencing-custom-route-manage-custom-poi-select',mode:'create', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/create/custom-poi-detail', params: { page: 'cw-geo-fencing-custom-route-manage-custom-poi-detail', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/create/create-custom-area', params: { page: 'cw-geo-fencing-custom-route-manage-custom-area-create',mode:'create', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/create/setting', params: { page: 'cw-geo-fencing-custom-route-manage-setting', mode:'create',navLevel: 5 } },
                { url: 'geo-fencing/custom-route/view', params: { page: 'cw-geo-fencing-custom-route-view', navLevel: 3 } },
                { url: 'geo-fencing/custom-route/view/update', params: { page: 'cw-geo-fencing-custom-route-manage', mode: 'update', navLevel: 4 } },
                { url: 'geo-fencing/custom-route/view/update/select-accessible-business-unit', params: { page: 'cw-geo-fencing-custom-route-manage-accessible-business-unit-select', mode: 'update', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/view/update/custom-poi', params: { page: 'cw-geo-fencing-custom-route-manage-custom-poi-select',mode:'update',navLevel: 5 } },
                { url: 'geo-fencing/custom-route/view/update/custom-poi-detail', params: { page: 'cw-geo-fencing-custom-route-manage-custom-poi-detail', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/view/update/create-custom-area', params: { page: 'cw-geo-fencing-custom-route-manage-custom-area-create',mode:'update', navLevel: 5 } },
                { url: 'geo-fencing/custom-route/view/update/setting', params: { page: 'cw-geo-fencing-custom-route-manage-setting', mode:'update',navLevel: 5 } },
                // Geo-Fencing - Custom Route Category
                { url: 'geo-fencing/custom-route-category', params: { page: 'cw-geo-fencing-custom-route-category', navLevel: 2 } },
                { url: 'geo-fencing/custom-route-category/create', params: { page: 'cw-geo-fencing-custom-route-category-manage', mode: 'create', navLevel: 4 } },
                { url: 'geo-fencing/custom-route-category/view', params: { page: 'cw-geo-fencing-custom-route-category-view', navLevel: 3 } },
                { url: 'geo-fencing/custom-route-category/view/update', params: { page: 'cw-geo-fencing-custom-route-category-manage', mode: 'update', navLevel: 4 } },
                // Geo-Fencing - Custom POI Suggestion
                { url: 'geo-fencing/custom-poi-suggestion', params: { page: 'cw-geo-fencing-custom-poi-suggestion', navLevel: 2 }, permission: "COM01319" },   
                { url: 'geo-fencing/custom-poi-suggestion/create', params: { page: 'cw-geo-fencing-custom-poi-suggestion-create', navLevel: 3 }, permission: "COM01319" },
                { url: 'geo-fencing/custom-poi-suggestion/create/accessible-business-unit-select', params: { page: 'cw-geo-fencing-custom-poi-suggestion-create-accessible-business-unit-select', mode: 'create' , navLevel: 4 }, permission: "COM01319" },               
                // Geo-Fencing - Custom Area
                { url: 'geo-fencing/custom-area', params: { page: 'cw-geo-fencing-custom-area', navLevel: 2 } },
                { url: 'geo-fencing/custom-area/search', params: { page: 'cw-geo-fencing-custom-area-search', navLevel: 3 } },
                { url: 'geo-fencing/custom-area/create', params: { page: 'cw-geo-fencing-custom-area-manage', mode: 'create', navLevel: 4 } },
                { url: 'geo-fencing/custom-area/create/select-accessible-business-unit', params: { page: 'cw-geo-fencing-custom-area-manage-accessible-business-unit-select', mode: 'create', navLevel: 5 } },
                { url: 'geo-fencing/custom-area/view', params: { page: 'cw-geo-fencing-custom-area-view', navLevel: 3 } },
                { url: 'geo-fencing/custom-area/view/update', params: { page: 'cw-geo-fencing-custom-area-manage', mode: 'update', navLevel: 4 } },
                { url: 'geo-fencing/custom-area/view/update/select-accessible-business-unit', params: { page: 'cw-geo-fencing-custom-area-manage-accessible-business-unit-select', mode: 'update', navLevel: 5 } },
                // Geo-Fencing - Custom Area Category
                { url: 'geo-fencing/custom-area-category', params: { page: 'cw-geo-fencing-custom-area-category', navLevel: 2 } },
                { url: 'geo-fencing/custom-area-category/create', params: { page: 'cw-geo-fencing-custom-area-category-manage', mode: 'create', navLevel: 4 } },
                { url: 'geo-fencing/custom-area-category/view', params: { page: 'cw-geo-fencing-custom-area-category-view', navLevel: 3 } },
                { url: 'geo-fencing/custom-area-category/view/update', params: { page: 'cw-geo-fencing-custom-area-category-manage', mode: 'update', navLevel: 4 } },
                // Geo-Fencing - Route from Here
                { url: 'geo-fencing/routes', params: { page: 'cw-geo-fencing-routes-from-here', navLevel: 2 } },
               
                // Geo-Fencing - Working Area
                { url: 'geo-fencing/working-area', params: { page: 'cw-geo-working-area', navLevel: 2}},

                // Shipment
                { url: 'shipment', params: { page: 'cw-shipment', navLevel: 1 }, permission: 'Shipment' },

                // Shipment - Default Value
                { url: 'shipment/default-value', params: { page: 'cw-shipment-default-value', navLevel: 2 }, permission: "Shipment" },

                // Shipment - Dashboard
                { url: 'shipment/dashboard', params: { page: 'cw-shipment-dashboard', navLevel: 2 }, permission: 'Shipment' },
                { url: 'shipment/dashboard/search', params: { page: 'cw-shipment-dashboard-search', navLevel: 3 }, permission: 'Shipment' },

                // Shipment - List Table
                { url: 'shipment/all', params: { page: 'cw-shipment-list', navLevel: 2 }, permission: 'Shipment' },
                { url: 'shipment/all/search', params: { page: 'cw-shipment-list-search', navLevel: 3 }, permission: 'Shipment' },

                { url: 'shipment/all/import', params: { page: 'cw-shipment-import', navLevel: 3 }, permission: 'Shipment' },
                { url: 'shipment/all/manage', params: { page: 'cw-shipment-manage', navLevel: 3 }, permission: 'Shipment' },
                { url: 'shipment/all/do-manage', params: { page: 'cw-shipment-list-do-manage', navLevel: 3 }, permission: 'Shipment' },
                { url: 'shipment/all/do-waypoint', params: { page: 'cw-shipment-list-do-waypoint', navLevel: 3 }, permission: 'Shipment' },
                { url: 'shipment/all/import-with-template', params: { page: 'cw-shipment-import-with-template', navLevel: 3 }, permission: 'Shipment' },

                { url: 'shipment/all/manage/cancel', params: { page: 'cw-shipment-cancel', navLevel: 4 }, permission: 'Shipment' },
                { url: 'shipment/all/manage/finish', params: { page: 'cw-shipment-finish', navLevel: 4 }, permission: 'Shipment' },

                // Shipment - Summary Table
                { url: 'shipment/all/summary', params: { page: 'cw-shipment-summary', navLevel: 3}, permission: 'Shipment' },
                
                // Shipment - PO Search
                { url: 'shipment/po', params: { page: 'cw-shipment-posearch', navLevel: 2 }, permission: 'Shipment' },
                { url: 'shipment/po/posearch', params: { page: 'cw-shipment-polist-search', navLevel: 3 }, permission: 'Shipment' },

                // Shipment - Timeline
                { url: 'shipment/timeline', params: { page: 'cw-shipment-timeline', navLevel: 2 }, permission: 'Shipment' },

                // Shipment - Template
                { url: 'shipment/templates', params: { page: 'cw-shipment-templates-list', navLevel: 2 }, permission: 'Shipment' },
                { url: 'shipment/templates/search', params: { page: 'cw-shipment-templates-search', navLevel: 3 }, permission: 'Shipment' },
                { url: 'shipment/templates/manage', params: { page: 'cw-shipment-templates-manage', navLevel: 3 }, permission: 'Shipment' },
                
                // Shipment - Recurring
                // { url: 'shipment/recurring', params: { page: 'cw-shipment-recurring', navLevel: 2 }, permission: 'Shipment' },
                // { url: 'shipment/recurring/create', params: { page: 'cw-shipment-recurring-manage', mode: 'create', navLevel: 3 }, permission: 'Shipment' },
                

                // Shipment - Job
                { url: 'shipment/job', params: { page: 'cw-shipment-job', navLevel: 2 } },
                { url: 'shipment/job/search', params: { page: 'cw-shipment-job-search', navLevel: 3 } },
                { url: 'shipment/job/import', params: { page: 'cw-shipment-job-import', navLevel: 3 } },
                { url: 'shipment/job/import/preview', params: { page: 'cw-shipment-job-import-preview', navLevel: 3 } },
                { url: 'shipment/job/select-template', params: { page: 'cw-shipment-job-template-select', navLevel: 3 } },
                { url: 'shipment/job/select-template/create', params: { page: 'cw-shipment-job-manage', mode: 'create', navLevel: 4 } },
                { url: 'shipment/job/select-template/create/select-vehicle-and-driver', params: { page: 'cw-shipment-job-manage-vehicle-and-driver-select', navLevel: 5 } },
                { url: 'shipment/job/select-template/create/destination', params: { page: 'cw-shipment-job-manage-destination', navLevel: 5 } },
                { url: 'shipment/job/select-template/create/destination/select-waypoint-order', params: { page: 'cw-shipment-job-manage-destination-waypoint-order-select', navLevel: 6 } },
                { url: 'shipment/job/select-template/create/destination/select-order', params: { page: 'cw-shipment-job-manage-destination-order-select', navLevel: 6 } },
                { url: 'shipment/job/select-template/create/destination/direction', params: { page: 'cw-shipment-job-manage-destination-direction', navLevel: 6 } },
                { url: 'shipment/job/select-template/create/destination/direction/routing', params: { page: 'cw-shipment-job-manage-destination-direction-routing', navLevel: 7 } },
                { url: 'shipment/job/select-template/create/select-alert', params: { page: 'cw-shipment-job-manage-alert-select', navLevel: 5 } },
                { url: 'shipment/job/view', params: { page: 'cw-shipment-job-view', navLevel: 3 } },
                { url: 'shipment/job/view/cancel', params: { page: 'cw-shipment-job-cancel', navLevel: 4 } },
                { url: 'shipment/job/view/update', params: { page: 'cw-shipment-job-manage', mode: 'update', navLevel: 4 } },
                { url: 'shipment/job/view/update/select-vehicle-and-driver', params: { page: 'cw-shipment-job-manage-vehicle-and-driver-select', navLevel: 5 } },
                { url: 'shipment/job/view/update/destination', params: { page: 'cw-shipment-job-manage-destination', navLevel: 5 } },
                { url: 'shipment/job/view/update/destination/select-waypoint-order', params: { page: 'cw-shipment-job-manage-destination-waypoint-order-select', navLevel: 6 } },
                { url: 'shipment/job/view/update/destination/select-order', params: { page: 'cw-shipment-job-manage-destination-order-select', navLevel: 6 } },
                { url: 'shipment/job/view/update/destination/direction', params: { page: 'cw-shipment-job-manage-destination-direction', navLevel: 6 } },
                { url: 'shipment/job/view/update/destination/direction/routing', params: { page: 'cw-shipment-job-manage-destination-direction-routing', navLevel: 7 } },
                { url: 'shipment/job/view/update/select-alert', params: { page: 'cw-shipment-job-manage-alert-select', navLevel: 5 } },
                // Shipment - Job Template
                { url: 'shipment/job-template', params: { page: 'cw-shipment-job-template', navLevel: 2 } },
                { url: 'shipment/job-template/search', params: { page: 'cw-shipment-job-template-search', navLevel: 3 } },
                { url: 'shipment/job-template/create', params: { page: 'cw-shipment-job-template-manage', mode: 'create', navLevel: 4 } },
                { url: 'shipment/job-template/create/select-vehicle-and-driver', params: { page: 'cw-shipment-job-template-manage-vehicle-and-driver-select', navLevel: 5 } },
                { url: 'shipment/job-template/create/destination', params: { page: 'cw-shipment-job-template-manage-destination', navLevel: 5 } },
                { url: 'shipment/job-template/create/destination/select-waypoint-order', params: { page: 'cw-shipment-job-template-manage-destination-waypoint-order-select', navLevel: 6 } },
                { url: 'shipment/job-template/create/destination/select-order', params: { page: 'cw-shipment-job-template-manage-destination-order-select', navLevel: 6 } },
                { url: 'shipment/job-template/create/destination/direction', params: { page: 'cw-shipment-job-template-manage-destination-direction', navLevel: 6 } },
                { url: 'shipment/job-template/create/destination/direction/routing', params: { page: 'cw-shipment-job-template-manage-destination-direction-routing', navLevel: 7 } },
                { url: 'shipment/job-template/create/select-alert', params: { page: 'cw-shipment-job-template-manage-alert-select', navLevel: 5 } },
                { url: 'shipment/job-template/view', params: { page: 'cw-shipment-job-template-view', navLevel: 3 } },
                { url: 'shipment/job-template/view/update', params: { page: 'cw-shipment-job-template-manage', mode: 'update', navLevel: 4 } },
                { url: 'shipment/job-template/view/update/select-vehicle-and-driver', params: { page: 'cw-shipment-job-template-manage-vehicle-and-driver-select', navLevel: 5 } },
                { url: 'shipment/job-template/view/update/destination', params: { page: 'cw-shipment-job-template-manage-destination', navLevel: 5 } },
                { url: 'shipment/job-template/view/update/destination/select-waypoint-order', params: { page: 'cw-shipment-job-template-manage-destination-waypoint-order-select', navLevel: 6 } },
                { url: 'shipment/job-template/view/update/destination/select-order', params: { page: 'cw-shipment-job-template-manage-destination-order-select', navLevel: 6 } },
                { url: 'shipment/job-template/view/update/destination/direction', params: { page: 'cw-shipment-job-template-manage-destination-direction', navLevel: 6 } },
                { url: 'shipment/job-template/view/update/destination/direction/routing', params: { page: 'cw-shipment-job-template-manage-destination-direction-routing', navLevel: 7 } },
                { url: 'shipment/job-template/view/update/select-alert', params: { page: 'cw-shipment-job-template-manage-alert-select', navLevel: 5 } },
                // Shipment - Customer
                { url: 'shipment/customer', params: { page: 'cw-shipment-customer', navLevel: 2 } },
                { url: 'shipment/customer/search', params: { page: 'cw-shipment-customer-search', navLevel: 3 } },
                { url: 'shipment/customer/import', params: { page: 'cw-shipment-customer-import', navLevel: 3 } },
                { url: 'shipment/customer/create', params: { page: 'cw-shipment-customer-manage', mode: 'create', navLevel: 4 } },
                { url: 'shipment/customer/create/bill-to-address', params: { page: 'cw-shipment-customer-manage-bil-to-addr-manage', navLevel: 5 } },
                { url: 'shipment/customer/create/ship-to-address', params: { page: 'cw-shipment-customer-manage-ship-to-addr', navLevel: 5 } },
                { url: 'shipment/customer/create/ship-to-address/create', params: { page: 'cw-shipment-customer-manage-ship-to-addr-manage', mode: 'create', navLevel: 7 } },
                { url: 'shipment/customer/create/ship-to-address/view', params: { page: 'cw-shipment-customer-manage-ship-to-addr-view', navLevel: 6 } },
                { url: 'shipment/customer/create/ship-to-address/view/update', params: { page: 'cw-shipment-customer-manage-ship-to-addr-manage', mode: 'update', navLevel: 7 } },
                { url: 'shipment/customer/view', params: { page: 'cw-shipment-customer-view', navLevel: 3 } },
                { url: 'shipment/customer/view/update', params: { page: 'cw-shipment-customer-manage', mode: 'update', navLevel: 4 } },
                { url: 'shipment/customer/view/update/bill-to-address', params: { page: 'cw-shipment-customer-manage-bil-to-addr-manage', navLevel: 5 } },
                { url: 'shipment/customer/view/update/ship-to-address', params: { page: 'cw-shipment-customer-manage-ship-to-addr', navLevel: 5 } },
                { url: 'shipment/customer/view/update/ship-to-address/create', params: { page: 'cw-shipment-customer-manage-ship-to-addr-manage', mode: 'create', navLevel: 7 } },
                { url: 'shipment/customer/view/update/ship-to-address/view', params: { page: 'cw-shipment-customer-manage-ship-to-addr-view', navLevel: 6 } },
                { url: 'shipment/customer/view/update/ship-to-address/view/update', params: { page: 'cw-shipment-customer-manage-ship-to-addr-manage', mode: 'update', navLevel: 7 } },
                // Shipment - Product
                { url: 'shipment/product', params: { page: 'cw-shipment-product', navLevel: 2 } },
                { url: 'shipment/product/search', params: { page: 'cw-shipment-product-search', navLevel: 3 } },
                { url: 'shipment/product/import', params: { page: 'cw-shipment-product-import', navLevel: 3 } },
                { url: 'shipment/product/create', params: { page: 'cw-shipment-product-manage', mode: 'create', navLevel: 4 } },
                { url: 'shipment/product/view', params: { page: 'cw-shipment-product-view', navLevel: 3 } },
                { url: 'shipment/product/view/update', params: { page: 'cw-shipment-product-manage', mode: 'update', navLevel: 4 } },
                // Shipment - Order
                { url: 'shipment/order', params: { page: 'cw-shipment-order', navLevel: 2 } },
                { url: 'shipment/order/search', params: { page: 'cw-shipment-order-search', navLevel: 3 } },
                { url: 'shipment/order/import', params: { page: 'cw-shipment-order-import', navLevel: 3 } },
                { url: 'shipment/order/deliver', params: { page: 'cw-shipment-order-deliver', navLevel: 3 } },
                { url: 'shipment/order/create', params: { page: 'cw-shipment-order-manage', mode: 'create', navLevel: 4 } },
                { url: 'shipment/order/view', params: { page: 'cw-shipment-order-view', navLevel: 3 } },
                { url: 'shipment/order/view/update', params: { page: 'cw-shipment-order-manage', mode: 'update', navLevel: 4 } },
                // Shipment - POI Trip
                { url: 'shipment/poi-trip', params: { page: 'cw-shipment-poi-trip', navLevel: 2 } },
                { url: 'shipment/poi-trip/search', params: { page: 'cw-shipment-poi-trip-search', navLevel: 3 } },
                { url: 'shipment/poi-trip/create', params: { page: 'cw-shipment-poi-trip-manage', mode: 'create', navLevel: 4 } },
                { url: 'shipment/poi-trip/view', params: { page: 'cw-shipment-poi-trip-view', navLevel: 3 } },
                { url: 'shipment/poi-trip/view/update', params: { page: 'cw-shipment-poi-trip-manage', mode: 'update', navLevel: 4 } },

                // Shipment - Category
                { url: 'shipment/category', params: { page: 'cw-shipment-category', navLevel: 2 } },
                { url: 'shipment/category/create', params: { page: 'cw-shipment-category-manage',mode: 'create', navLevel: 3 } },
                { url: 'shipment/category/view/update', params: { page: 'cw-shipment-category-manage', mode: 'update', navLevel: 4 } },
                { url: 'shipment/category/create', params: { page: 'cw-shipment-category-manage', mode: 'create', navLevel: 3 } },
                { url: 'shipment/category/view', params: { page: 'cw-shipment-category-view', navLevel: 3 } },
                { url: 'shipment/category/create/select-accessible-business-unit', params: { page: 'cw-shipment-category-manage-accessible-business-unit-select', mode: 'create', navLevel: 4 } },
                { url: 'shipment/category/view/update/select-accessible-business-unit', params: { page: 'cw-shipment-category-manage-accessible-business-unit-select', mode: 'update', navLevel: 5 } },

                //Shipment - Zones
                { url: 'shipment/zone', params: { page: 'cw-shipment-zone', navLevel: 2 } },
                { url: 'shipment/zone/view', params: { page: 'cw-shipment-zone-view', navLevel: 3 } },
                { url: 'shipment/zone/create', params: { page: 'cw-shipment-zone-manage', mode: 'create', navLevel: 3 } },
                { url: 'shipment/zone/view/update', params: { page: 'cw-shipment-zone-manage', mode: 'update', navLevel: 4 } },

                // Report
                { url: 'report', params: { page: 'cw-report', navLevel: 1 } },
                // reportViewer
                { url: 'report/reportviewer', params: { page: 'cw-report-reportviewer', navLevel: 4 } },
                 // Report - General
                { url: 'report/general', params: { page: 'cw-report-general', navLevel: 2 } },
                // Report and Group - Subscription (standard)
                { url: 'report/standard/overspeed', params: { page: 'cw-report-standard-overspeed', navLevel: 2 } },
                { url: 'report/standard/overspeedminute', params: { page: 'cw-report-standard-overspeedminute', navLevel: 2 } },
                { url: 'report/standard/overspeedmonth', params: { page: 'cw-report-standard-overspeedmonth', navLevel: 2 } },
                { url: 'report/standard/parkingengine', params: { page: 'cw-report-standard-parkingengine', navLevel: 2 } },
                // Report and Group - Subscription (telematics)
                { url: 'report/telematics', params: { page: 'cw-report-telematics', navLevel: 2}},
                { url: 'report/telematics/sevendays', params: { page: 'cw-report-telematics-sevendays', navLevel: 3 } },
                { url: 'report/telematics/driverperformance', params: { page: 'cw-report-telematics-driverperformance', navLevel: 3 } },
                { url: 'report/telematics/parkingoverview', params: { page: 'cw-report-telematics-parkingoverview', navLevel: 3 } },
                { url: 'report/telematics/detailedfuel', params: { page: 'cw-report-telematics-detailedfuel', navLevel: 3 } },
                { url: 'report/telematics/detailedtrip', params: { page: 'cw-report-telematics-detailedtrip', navLevel: 3 } },
                { url: 'report/telematics/summaryadas', params: { page: 'cw-report-telematics-summaryadas', navLevel: 3 } },
                // Report - Comparison
                { url: 'report/comparison', params: { page: 'cw-report-comparison', navLevel: 2 } },
                // Report - Summary
                { url: 'report/summary', params: { page: 'cw-report-summary', navLevel: 2 } },
                // Report - Delinquent
                { url: 'report/delinquent', params: { page: 'cw-report-delinquent', navLevel: 2 } },
                // Report - AdvanceReport
                { url: 'report/advanceReport', params: { page: 'cw-report-advance', navLevel: 2 } },
                // Report - FleetTypeLogReport
                { url: 'report/advanceReport', params: { page: 'cw-report-fleettype', navLevel: 2 } },


                //Report - KpiReport
                { url: 'report/kpiReport', params: { page: 'cw-report-kpi-search', navLevel: 2 } },
                { url: 'report/kpiReport/dashboard-overall', params: { page: 'cw-report-kpi-dashboard-overall', navLevel: 3 } },
                { url: 'report/kpiReport/dashboard-bu', params: { page: 'cw-report-kpi-dashboard-bu', navLevel: 4 } },


                //Report = Advance Driver Performance
                { url: 'report/advance/driverperformance', params: { page: 'cw-report-advance-driverperformance', navLevel: 2 } },

                //Map Function - Find Nearest Asset
                { url: 'map-functions/find-nearest-asset', params: { page: 'cw-map-functions-find-nearest-asset', navLevel: 2 } },
                { url: 'map-functions/find-nearest-asset/result', params: { page: 'cw-map-functions-find-nearest-asset-result', navLevel: 2 } },
                { url: 'map-functions/send-to-navigator', params: { page: 'cw-map-functions-send-to-navigator', navLevel: 2 } },

                 // Dashboard
                { url: 'dashboard', params: { page: 'cw-dashboard', navLevel: 1 } },

                // Dashboard - Truck Status
                { url: 'dashboard/truck-status', params: { page: 'cw-dashboard-truck-status', navLevel: 2 } },
                { url: 'dashboard/truck-status/dashboard-overall', params: { page: 'cw-dashboard-truck-status-dashboard-all', navLevel: 3 } },
                { url: 'dashboard/truck-status/dashboard-department', params: { page: 'cw-dashboard-truck-status-dashboard-dept', navLevel: 4 } },
                { url: 'dashboard/truck-status/dashboard/details', params: { page: 'cw-dashboard-truck-status-details', navLevel: 5 } },

                // Dashboard - Utilization Driving Time
                { url: 'dashboard/utilization-driving-time', params: { page: 'cw-dashboard-utlization-driving-time', navLevel: 2 } },
                { url: 'dashboard/utilization-driving-time/dashboard', params: { page: 'cw-dashboard-utilization-driving-time-dashboard', navLevel: 3}},
                { url: 'dashboard/utilization-drivingtime/dashboard/details', params: { page: 'cw-dashboard-utilization-driving-time-details', navLevel: 4 } },

                // Dashboard - Driver Performance Score
                { url: 'dashboard/driver-performance-score', params: { page: 'cw-dashboard-driver-score', navLevel: 2 } },
                { url: 'dashboard/driver-performance-score/dashboard', params: { page: 'cw-dashboard-driver-score-dashboard', navLevel: 3 } },
                { url: 'dashboard/driver-performance-score/dashboard/details', params: { page: 'cw-dashboard-driver-score-details', navLevel: 4 } },

                //Safety Dashboard
                { url: 'dashboard/safety-dashboard', params: { page: 'cw-dashboard-safety-dashboard-search', navLevel: 2 } },
                { url: 'dashboard/safety-dashboard/dashboard', params: { page: 'cw-dashboard-safety-dashboard-dashboard', navLevel: 3 } },
                { url: 'dashboard/safety-dashboard/report', params: { page: 'cw-dashboard-safety-dashboard-report', navLevel: 4 } },
                
                // Auto-Mail-Report - Menu
                { url: 'automail-report', params: { page: 'cw-automail-report-manage', navLevel: 1 } },
                { url: 'automail-report/list', params: { page: 'cw-automail-report-manage-list', navLevel: 2 } },
                // { url: 'automail-report/manage', params: { page: 'cw-automail-report-manage-create', navLevel: 3 } },
                { url: 'automail-report/manage/create', params: { page: 'cw-automail-report-manage-create', mode: 'create', navLevel: 3 } },
                { url: 'automail-report/manage/update', params: { page: 'cw-automail-report-manage-create', mode: 'update', navLevel: 3 } },

                // BOMs - Menu
                { url: 'boms', params: { page: 'cw-boms-menu', navLevel: 1 } },

                // BOMs - Real Time Passenger
                { url: 'boms/realtime-passenger-dashboard/list', params: { page: 'cw-boms-realtime-passenger-dashboard-list', navLevel: 3 } },
                { url: 'boms/realtime-passenger-dashboard/form', params: { page: 'cw-boms-realtime-passenger-dashboard-form', navLevel: 2 } },
                { url: 'boms/realtime-passenger-dashboard/search', params: { page: 'cw-boms-realtime-passenger-dashboard-search', navLevel: 4 } },
                { url: 'boms/realtime-passenger-dashboard/search/result', params: { page: 'cw-boms-realtime-passenger-dashboard-search-result', navLevel: 5 } },
                
                // BOMs - Real Time Passenger
                { url: 'boms/reports', params: { page: 'cw-boms-reports', navLevel: 2 } },
                { url: 'boms/reports/summary-round-trip/form', params: { page: 'cw-boms-reports-summary-round-trip-form', navLevel: 3 } },
                { url: 'boms/reports/realtime-passenger-on-board/form', params: { page: 'cw-boms-reports-realtime-passenger-on-board-form', navLevel: 3 } },
                { url: 'boms/reports/summary-passenger-by-route/form', params: { page: 'cw-boms-reports-summary-passenger-by-route-form', navLevel: 3 } },
                { url: 'boms/reports/summary-passenger-by-zone/form', params: { page: 'cw-boms-reports-summary-passenger-by-zone-form', navLevel: 3 } },
                { url: 'boms/reports/bus-usage-statistics/form', params: { page: 'cw-boms-reports-bus-usage-statistics-form', navLevel: 3 } },
                { url: 'boms/reports/summary-transportation/form', params: { page: 'cw-boms-reports-summary-transportation-form', navLevel: 3 } },
                { url: 'boms/reports/monthly-summary-transportation/form', params: { page: 'cw-boms-reports-monthly-summary-transportation-form', navLevel: 3 } },
                { url: 'boms/reports/summary-passenger-by-zone-and-vehicle-type/form', params: { page: 'cw-boms-reports-summary-passenger-by-zone-and-vehicle-type-form', navLevel: 3 } },

                //BOMs - Default Values
                { url: 'boms/default-values/list', params: { page: 'cw-boms-defaultvalues-list', navLevel: 2 } },
                { url: 'boms/default-values/view', params: { page: 'cw-boms-defaultvalues-view', navLevel: 3 } },
                { url: 'boms/default-values/create', params: { page: 'cw-boms-defaultvalues-manage', mode: 'create', navLevel: 3 } },
                { url: 'boms/default-values/view/update', params: { page: 'cw-boms-defaultvalues-manage', mode: 'update', navLevel: 4 } },
            ]
        }
    ],

    commonRoutes: [
        // -- Developer Asset --
        { url: 'developer', params: { page: 'developerHome', navLevel: 1 } },
        { url: 'developer/label', params: { page: 'labelPage', navLevel: 2 } },
        { url: 'developer/textbox', params: { page: 'textBoxPage', navLevel: 2 } },
        { url: 'developer/textarea', params: { page: 'textAreaPage', navLevel: 2 } },
        { url: 'developer/dropdown', params: { page: 'dropdownPage', navLevel: 2 } },
        { url: 'developer/button', params: { page: 'buttonPage', navLevel: 2 } },
        { url: 'developer/tab', params: { page: 'tabPage', navLevel: 2 } },
        { url: 'developer/datatable', params: { page: 'datatableIndex', navLevel: 2 } },
        { url: 'developer/datatable/readme', params: { page: 'datatableReadme', navLevel: 3 } },
        { url: 'developer/datatable/simple', params: { page: 'datatableSimple', navLevel: 3 } },
        { url: 'developer/datatable/advance', params: { page: 'datatableAdvance', navLevel: 3} },
        { url: 'developer/datatable/end', params: { page: 'datatableEndPoint', navLevel: 3 } },
        { url: 'developer/datatable/others', params: { page: 'datatableOthers', navLevel: 3 } },
        { url: 'developer/datatable/style', params: { page: 'datatableStyle', navLevel: 3 } },
        { url: 'developer/datatable/row-selector', params: { page: 'datatableRowSelector', navLevel: 3 } },
        { url: 'developer/datatable/api', params: { page: 'datatableAPI', navLevel: 3 } },
        { url: 'developer/datatable/exp', params: { page: 'datatableEXP', navLevel: 3 } },
        { url: 'developer/datatable/exp/load-more', params: { page: 'datatableLoadMore', navLevel: 4 } },
        { url: 'developer/datatable/exp/serverSide', params: { page: 'datatableServerside', navLevel: 4 } },
        { url: 'developer/tooltip', params: { page: 'tooltipPage', navLevel: 2 } },
        { url: 'developer/fileupload', params: { page: 'fileUploadPage', navLevel: 2 } },
        { url: 'developer/checkbox', params: { page: 'checkBoxPage', navLevel: 2 } },
        { url: 'developer/datetimepicker', params: { page: 'dateTimepickerPage', navLevel: 2 } },
        { url: 'developer/chart', params: { page: 'chartPage', navLevel: 2 } },
        { url: 'developer/chart/chartdetail', params: { page: 'chartEndPoint', navLevel: 3 } },
        { url: 'developer/combobox', params: { page: 'comboboxPage', navLevel: 2 } },
        { url: 'developer/chartbar', params: { page: 'chartBarPage', navLevel: 2 } },

        { url: 'developer/map', params: { page: 'mapIndex', navLevel: 2 } },
        { url: 'developer/map/clear', params: { page: 'mapClear', navLevel: 3 } },
        //{ url: 'developer/map/drawPin', params: { page: 'mapDrawPin', navLevel: 3 } },
        
        { url: 'developer/map/drawOnePointZoom', params: { page: 'mapDrawOnePointZoom', navLevel: 3 } },
        { url: 'developer/map/drawOneLineZoom', params: { page: 'mapDrawOneLineZoom', navLevel: 3 } },
        { url: 'developer/map/drawOnePolygonZoom', params: { page: 'mapDrawOnePolygonZoom', navLevel: 3 } },
        { url: 'developer/map/drawMultiplePoint', params: { page: 'mapDrawMultiplePoint', navLevel: 3 } },
        { url: 'developer/map/drawMultiplePolygonZoom', params: { page: 'mapDrawMultiplePolygonZoom', navLevel: 3 } },
        { url: 'developer/map/drawMultpleLineZoom', params: { page: 'mapDrawMultpleLineZoom', navLevel: 3 } },
        { url: 'developer/map/drawMultipleVehicle', params: { page: 'mapDrawMultipleVehicle', navLevel: 3 } },
        { url: 'developer/map/drawOneRoute', params: { page: 'mapDrawOneRoute', navLevel: 3 } },
        
        { url: 'developer/map/drawPointBuffer', params: { page: 'mapDrawPointBuffer', navLevel: 3 } },
        { url: 'developer/map/drawRouteBuffer', params: { page: 'mapDrawRouteBuffer', navLevel: 3 } },
        { url: 'developer/map/drawCustomArea', params: { page: 'mapDrawCustomArea', navLevel: 3 } },
        { url: 'developer/map/zoomPoint', params: { page: 'mapZoomPoint', navLevel: 3 } },
        { url: 'developer/map/trackVehicles', params: { page: 'mapTrackVehicles', navLevel: 3 } },
        { url: 'developer/map/enableClickMap', params: { page: 'mapEnableClickMap', navLevel: 3 } },
        { url: 'developer/map/enableDrawPolygon', params: { page: 'mapEnableDrawPolygon', navLevel: 3 } },

        { url: 'developer/map/playbackAnimate', params: { page: 'mapPlaybackAnimate', navLevel: 3}},
        { url: 'developer/map/playbackFootprint', params: { page: 'mapPlaybackFootprint', navLevel: 3}},
        { url: 'developer/map/playbackFully', params: { page: 'mapPlaybackFully', navLevel: 3}},

        { url: 'developer/screen', params: { page: 'screenDevIndex', navLevel: 2 } },
        { url: 'developer/screen/readme', params: { page: 'screenDevReadme', navLevel: 3 } },
        { url: 'developer/screen/trackChange', params: { page: 'screenDevTrackChange', navLevel: 3 } },
        { url: 'developer/screen/validation', params: { page: 'screenDevValidation', navLevel: 3 } },
        { url: 'developer/screen/validation-demo', params: { page: 'screenDevValidationDemo', navLevel: 3 } },
        { url: 'developer/screen/i18n', params: { page: 'screenDevI18N', navLevel: 3 } },
        { url: 'developer/screen/gulp', params: { page: 'screenDevGulp', navLevel: 3 } },
        { url: 'developer/screen/faq', params: { page: 'screenDevFAQ', navLevel: 3 } },
        { url: 'developer/colorPicker', params: { page: 'colorPickerPage', navLevel: 2 } },
        { url: 'developer/imageCombobox', params: { page: 'imageComboboxPage', navLevel: 2 } },
        { url: 'developer/screen/faq', params: { page: 'screenDevFAQ', navLevel: 3 } },
        { url: 'developer/tree', params: { page: 'treeViewPage', navLevel: 2 } },
        { url: 'developer/dropdowntree', params: { page: 'dropdownTreePage', navLevel: 2 } },
        { url: 'developer/messagebox', params: { page: 'messageboxHelpPage', navLevel: 2 } },
        { url: 'developer/reportViewer', params: { page: 'reportViewerDemo', navLevel: 2 } },
        
        { url: 'developer/services-testing', params: { page: 'serviceTestingIndex', navLevel: 1 } },
        { url: 'developer/services-testing/shipment', params: { page: 'serviceTestingShipment', navLevel: 2 } },
    ]
};

export default routing;
