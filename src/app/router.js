import ko from 'knockout';
import crossroads from 'crossroads';
import hasher from 'hasher';
import ErrorBase from "./frameworks/core/errorBase";
import routing from "./router.config";
import EventAggregator from "./frameworks/core/eventAggregator";
import Logger from "./frameworks/core/logger";
import Utility from "./frameworks/core/utility";
import Singleton from "./frameworks/core/singleton";
import WebConfig from "./frameworks/configuration/webconfiguration";
import {Constants, Enums} from "./frameworks/constant/apiConstant";
import * as EventAggregatorConstant from "./frameworks/constant/eventAggregator";

/**
 * Router register all posible route in the application and manage browser history.
 * It work internally under NavigationService
 * 
 * The following event will be published
 * - EventAggregatorConstant.CORE_NAV_BACK: when router detect backward navigation
 * - EventAggregatorConstant.CORE_NAV_NEXT: when router detect forward navigation
 * 
 * @class Router
 */
class Router {

    /**
     * Creates an instance of Router.
     * 
     * @param {any} config
     */
    constructor(config) {
        // Logger.info("initializing Router");
        var self = this;

        this._eventAggregator = new EventAggregator();
        this._routeMap = new Map();
        this._historyStack = [];
        this.config = config;
        this.portal = WebConfig.userSession.currentPortal;

        // For helping create NavigationHistory
        this.previousOptions = null;
        this.previousJourneyId = "";
        this.previousBladeId = "";

        // Configure Crossroads route handlers
        // TODO: Now add all possible routes, we may not need it.
        let backOfficeRoutes = this.getRoutesByPortal("back-office");
        let companyAdminRoutes = this.getRoutesByPortal("company-admin");
        let companyWorkspaceRoutes = this.getRoutesByPortal("company-workspace");
        let allConfigRoutes = config.commonRoutes.concat(backOfficeRoutes, 
            companyAdminRoutes, companyWorkspaceRoutes);

        ko.utils.arrayForEach(allConfigRoutes, (route) => {
            var xrRoute = crossroads.addRoute(route.url);
            var componentName = route.params.page;
            if(componentName) {
                var componentUri = route.url;
                var routeMapKey = self._getRouteMapKeyByComponent(componentName, route.params);
                self._routeMap.set(routeMapKey, { 
                    uri: componentUri,
                    xr: xrRoute,
                    navLevel: route.params.navLevel
                });
            }
        });

        // ko.utils.arrayForEach(config.routes, (route) => {
        //     var xrRoute = crossroads.addRoute(route.url);
        //     var componentName = route.params.page;
        //     if(componentName) {
        //         var componentUri = route.url;
        //         self._routeMap.set(componentName, { 
        //             uri: componentUri,
        //             xr: xrRoute,
        //             navLevel: route.params.navLevel
        //         });
        //     }
        // });
        
        // Config Global 404 cross road handlers.
        crossroads.bypassed.add(console.log, console);
        
        /**
         * Listen to hash change, check if the change is BACK, NEXT or JUMP
         * BACK == newHash is in history (2nd from top)
         * NEXT == newHash is not in history 
         * 
         * @param {any} newHash
         * @param {any} oldHash
         */
        function hashChange(newHash, oldHash) {
            // Logger.log("router: newHash = " + newHash);
            crossroads.parse(newHash);

            let latestHistory = self.getLatestHistory();
            if(latestHistory && 
                (latestHistory instanceof MarkerNavigationHistory) &&
                (newHash === "")) {
                return;
            }

            // If latestHistory.uri === newHash means ignore
            if(latestHistory.uri !== newHash) {

                // Determine if it is BACK or NEXT
                // If history-1 uri === newHash, it is BACK
                // Else it is NEXT
                let isBackCommand = false;
                let previousHistory = self.getLatestHistory(-1);
                if(!previousHistory) {
                    // If there is no history-1, check if latetHistory is MarkerNavigationHistory type="start"
                    // This also mean BACK
                    var isMarker = (latestHistory instanceof MarkerNavigationHistory);
                    isBackCommand = isMarker && (newHash === "");
                } else {
                    isBackCommand = (previousHistory.uri === newHash);
                }
                if(isBackCommand) {
                    // Pop latestHistory out
                    self._historyStack.pop();
                    
                    // Publish event CORE_NAV_BACK with targetHistory
                    var targetHistory = self._historyStack[self._historyStack.length-1];
                    self._eventAggregator.publish(EventAggregatorConstant.CORE_NAV_BACK, targetHistory);
                } else {
                    var route = self.findRouteByUri(newHash);
                    var targetHistory = null;

                    // If route is not found, newHash is a special command
                    if(route && route.params.page) {
                        if(!self.validatePermission(route)) {
                            Logger.error("router: insufficient permission to access");
                            return;
                        }

                        targetHistory = new NavigationHistory({
                            uri: newHash,
                            componentName: route.params.page,
                            options: self.previousOptions,
                            journeyId: self.previousJourneyId,
                            bladeId: self.previousBladeId,
                            navLevel: route.params.navLevel
                        });

                        // Allow polymorph component to identify itself.
                        var mode = Utility.extractParam(route.params.mode, null);
                        targetHistory.options = Utility.createOptionIfParamExist("mode", mode);

                        // Verify if targetHistory is valid
                        // It is possible that browser refresh cause browser's history and nav history out of sync
                        // This will lead in to child blade render BEFORE parent blade.
                        if(latestHistory.navLevel >= targetHistory.navLevel) {
                            Logger.warn("DETECT try to back to non exisitng blade, discard command...");
                            targetHistory = null;
                        }

                    } else {
                        // Determine which command?
                        var switchJourneyIndex = newHash.lastIndexOf("journey/", 0);
                        if(switchJourneyIndex !== -1) {
                            // Switch journey command
                            var journeyId = newHash.substring(newHash.lastIndexOf("/")+1);
                            targetHistory = new SwitchJourneyNavigationHistory(journeyId);
                        } else {
                            // Toggle journey command
                            targetHistory = new HideAllJourneyNavigationHistory(newHash);
                        }
                    }

                    // Publish event CORE_NAV_NEXT with targetHistory
                    if(targetHistory != null) {
                        self._eventAggregator.publish(EventAggregatorConstant.CORE_NAV_NEXT, targetHistory);
                    }
                }
            }
        }
    
        // Activate Crossroads
        crossroads.normalizeFn = crossroads.NORM_AS_OBJECT; // This is a key to onRouteMatched recieving params object
        hasher.initialized.add(hash => crossroads.parse(hash));
        hasher.changed.add(hashChange);
        hasher.init();
    }

    /**
     * Called when crossroads.route recieve signal when uri matched
     * Does not use now...
     * 
     * @deprecated
     * @private 
     * @param {any} params
     */
    onRouteMatched(params) {
        // FIXME: There is problem when using both Replay (selg.onNextNavigation) and crossroads
        // As both are working together cause infinite loop.
        Logger.log("router: onRouteMatched - ");
        Logger.log(params);
    }

    validatePermission(route) {
        var isValid = true;

        // There are 3 properties to check [permission, permissionsAnd, permissionsOr] all are mutual exclusive
        // The priority is permission > permissionsAnd > permissionsOr
        if(!_.isNil(route.permission)) {
            var perm = Constants.Permission[route.permission];
            isValid = WebConfig.userSession.hasPermission(perm);
        } else if(!_.isNil(route.permissionsAnd) && _.isArray(route.permissionsAnd)) {
            for(let i = 0; i < route.permissionsAnd.length; i++) {
                var perm = Constants.Permission[route.permissionsAnd[i]];
                isValid = isValid && WebConfig.userSession.hasPermission(perm);
                if(!isValid) break;
            }
        } else if(!_.isNil(route.permissionsOr) && _.isArray(route.permissionsOr)) {
            for(let i = 0; i < route.permissionsOr.length; i++) {
                var perm = Constants.Permission[route.permissionsOr[i]];
                isValid = isValid || WebConfig.userSession.hasPermission(perm);
                if(isValid) break;
            }
        }
        
        return isValid;
    }
    
    /**
     * Navigate to initial state, create MarkerNavigationHistory for determine BACK/NEXT command
     * @public
     */
    navigateToInitialState(navOption = null) {
        var history = new MarkerNavigationHistory("start");
        this._historyStack.push(history);

        var currentUri = hasher.getHash();
        if(currentUri === "") {
            hasher.setHash("#");

            // If show landing page then find the default route
            if(navOption && navOption.routeOption) {
                if(this.validatePermission(navOption.routeOption)) {
                    var targetHistory = new NavigationHistory({
                        uri: currentUri,
                        componentName: navOption.routeOption.page,
                        options: this.previousOptions,
                        journeyId: this.previousJourneyId,
                        bladeId: this.previousBladeId
                    });
                    this._eventAggregator.publish(EventAggregatorConstant.CORE_NAV_NEXT, targetHistory);
                } else {
                    Logger.error("router: insufficient permission to access = " + currentUri);
                }
            }

        } else {
            // Manual trigger replay when user copy url and place in new browser.
            var route = this.findRouteByUri(currentUri);
            if(route) {
                if(this.validatePermission(route)) {
                    var targetHistory = new NavigationHistory({
                        uri: currentUri,
                        componentName: route.params.page,
                        options: this.previousOptions,
                        journeyId: this.previousJourneyId,
                        bladeId: this.previousBladeId
                    });

                    // Allow polymorph component to identify itself.
                    var mode = Utility.extractParam(route.params.mode, null);
                    targetHistory.options = Utility.createOptionIfParamExist("mode", mode);

                    this._eventAggregator.publish(EventAggregatorConstant.CORE_NAV_NEXT, targetHistory);

                } else {
                    Logger.error("router: insufficient permission to access = " + currentUri);
                }
            } else {
                Logger.error("router: cannot find route with given request = " + currentUri);
            }
        }
    }

    /**
     * Navigate to target journey, create SwitchJourneyNavigationHistory for determine BACK/NEXT command
     * @public 
     * @param {any} journey
     */
    navigateToJourney(journey) {
        var history = new SwitchJourneyNavigationHistory(journey.id);
        this._historyStack.push(history);

        hasher.setHash("journey/" + journey.id);
    }

    /**
     * Hide all journey, create HideAllJourneyNavigationHistory for determine BACK/NEXT command
     * @public
     * @param {any} uri
     */
    navigateToHideAllJourney(uri) {
        var history = new HideAllJourneyNavigationHistory(uri);
        this._historyStack.push(history);

        hasher.setHash(uri);
    }

    /**
     * Navigate to target component, create NavigationHistory for determine BACK/NEXT command
     * 
     * @public
     * @param {any} componentName
     * @param {any} params, expect the following parameters
     * - options: options to be parse to Screen ViewModel.
     * - isNewJourney: whether the navigation happen on new journey or not.
     * - journeyId: id of journey this navigation going to.
     * - bladeId: id of blade this navigation going to.
     */
    navigate(componentName, params) {
        if(!params) {
            throw new RoutingError("params", "Router.navigate requires params");
        }
        if(!params.journeyId) {
            throw new RoutingError("params.journeyId", "Router.navigate requires params.journeyId");
        }
        if(!params.bladeId) {
            throw new RoutingError("params.bladeId", "Router.navigate requires params.bladeId");
        }

        var routeMapKey = this._getRouteMapKeyByComponent(componentName, params.options);
        if(this._routeMap.has(routeMapKey)) {
            var routeItem = this._routeMap.get(routeMapKey);

            // Create new NavigationHistory for this navigation
            var history = new NavigationHistory({
                uri: routeItem.uri,
                componentName: componentName,
                options: Utility.extractParam(params.options, null),
                journeyId: Utility.extractParam(params.journeyId),
                bladeId: Utility.extractParam(params.bladeId),
                navLevel: Utility.extractParam(routeItem.navLevel, -1)
            });
            this._historyStack.push(history);

            // Remember these state for next navigation
            this.previousOptions = Utility.extractParam(params.options, null);
            this.previousJourneyId = Utility.extractParam(params.journeyId);
            this.previousBladeId = Utility.extractParam(params.bladeId);

            // Marking in URL so user can bookmark, go back and next
            hasher.setHash(routeItem.uri);

        } else {
            throw new RoutingError("componentName", "Cannot find target component, make sure it is registed in route config");
        }
    }
    
    /**
     * Remove target NavigationHistory by bladeId
     * @public
     * @param {any} bladeId
     */
    removeNavigationHistoryByBladeId(bladeId) {
        var latestHistory = this.getLatestHistory();
        if(latestHistory && (latestHistory.bladeId === bladeId)) {
            Logger.log("router: auto remove history.");
            this._historyStack.pop();
        }
    }

    /**
     * Print routing information in console, this is for debugging purpose.
     * Use it from Browser's Debugger by access window.shell.router object
     * Call window.shell.router.printInfo()
     * @public
     */
    printInfo() {
        Logger.log("--== Router.printInfo() ==--");
        for(let [key, value] of this._routeMap) {
            Logger.log("key = " + key);
            Logger.log("value = " + value);
        }
    }
    
    /**
     * Print navigation history in console, this is for debugging purpose.
     * Use it from Browser's Debugger by access window.shell.router object
     * Call window.shell.router.printHistory()
     * @public
     */
    printHistory() {
        Logger.log("--== Router.printHistory() ==--");
        Logger.log(this._historyStack);
    }

    
    /**
     * Get current route config 
     * 
     * @public
     * @returns {object} entry in router.config.js
     */
    getCurrentRoute() {
        var currentUri = hasher.getHash();
        var currentRoute = null;
        if(!_.isEmpty(currentUri)) {
            currentRoute = this.findRouteByUri(currentUri);
        }
        return currentRoute;
    }

    /**
     * Find route in this.config.routes
     * @private
     * @param {any} uri
     * @returns
     */
    findRouteByUri(uri) {
        var result = null;
        let portalRoutes = this.getRoutesByPortal(this.portal);
        let allowedRoutes = this.config.commonRoutes.concat(portalRoutes);
        for(let i = 0; i < allowedRoutes.length; i++) {
            let route = allowedRoutes[i];
            if(uri === route.url) {
                result = route;
                break;
            }
        }
        return result;
    }
    
    /**
     * Create RouteMapKey from component and options
     * This is required since router.config.js allow multiple route to has the same page param
     * 
     * @private 
     * @param {any} componentName
     * @param {any} options
     * @returns
     */
    _getRouteMapKeyByComponent(componentName, options) {
        let key = componentName;
        if(!_.isNil(options)) {
            if(!_.isEmpty(options.mode)) {
                key = key.concat("_" + options.mode);
                if(!_.isEmpty(options.path)) {
                    key = key.concat("_" + options.path);
                }
            }
        }
        return key;
    }
    
    /**
     * 
     * @private
     * @param {any} portalName
     * @returns
     */
    getRoutesByPortal(portalName) {
        var result = [];
        for(let i = 0; i < this.config.portals.length; i++) {
            let portalConfig = this.config.portals[i];
            if(portalConfig.name === portalName) {
                result = portalConfig.routes;
                break;
            }
        }
        return result;
    }
    
    /**
     * Get history with offset
     * @private
     * @returns
     */
    getLatestHistory(offset = 0) {
        var targetOffset = this._historyStack.length - 1;
        targetOffset += offset;

        var result = null;
        if(this._historyStack.length > 0) {
            result = this._historyStack[targetOffset];
        }
        return result;
    }

    /**
     * Get Router singleton instance. 
     * @static
     * @returns
     */
    static getInstance() {
        if(!Router._instance) {
            Router._instance = new Router(routing);
        }
        return Router._instance;
        // return Singleton.getInstance("router", new Router(routing));
    }
}

/**
 * Navigation History for all navigation done by Router.navigate()
 * 
 * @private
 * @class NavigationHistory
 */
class NavigationHistory {
    constructor(params) {
        this.uri = params.uri;
        this.componentName = params.componentName;
        this.options = params.options;
        this.journeyId = params.journeyId;
        this.bladeId = params.bladeId;
        this.navLevel = params.navLevel;
    }
}

/**
 * Specilize Navigation History, represent a starting point.
 * 
 * @class MarkerNavigationHistory
 * @extends {NavigationHistory}
 */
class MarkerNavigationHistory extends NavigationHistory {
    constructor(type) {
        super({
            uri: "",
            componentName: null,
            options: null,
            journeyId: null,
            bladeId: null,
            navLevel: -1
        });
        this.type = type;
    }
}

/**
 * Specilize Navigation History, represent switching journey action.
 * 
 * @class SwitchJourneyNavigationHistory
 * @extends {NavigationHistory}
 */
class SwitchJourneyNavigationHistory extends NavigationHistory {
    constructor(journeyId) {
        super({
            uri: "journey/" + journeyId,
            componentName: null,
            options: null,
            journeyId: journeyId,
            bladeId: null,
            navLevel: -1
        });
        this.journeyId = journeyId;
    }
}

/**
 *  Specilize Navigation History, represent hide all journey action.
 * 
 * @class HideAllJourneyNavigationHistory
 * @extends {NavigationHistory}
 */
class HideAllJourneyNavigationHistory extends NavigationHistory {
    constructor(uri) {
        super({
            uri: uri,
            componentName: null,
            options: null,
            journeyId: null,
            bladeId: null,
            navLevel: -1
        });
    }
}

/**
 * Error when router is in illegal state
 * 
 * @class RoutingError
 * @private
 * @extends {ErrorBase}
 */
class RoutingError extends ErrorBase {
    constructor(invalidParam, reason) {
        super(invalidParam);
        this.reason = reason
    }
}

export { 
    Router,
    SwitchJourneyNavigationHistory,
    HideAllJourneyNavigationHistory
}