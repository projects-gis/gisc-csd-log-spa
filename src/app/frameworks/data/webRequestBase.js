import WebConfig from "../configuration/webconfiguration";
import Logger from "../core/logger";
import Utility from "../core/utility";

/**
 * Facade to access Web API
 * 
 * @class WebRequest
 */
class WebRequestBase {
    /**
     * Creates an instance of WebRequest.
     * @private
     */
    constructor(applicationUrl) {
        this.applicationUrl = applicationUrl;
    }

    /**
     * Get header for ajax request
     * 
     * @returns
     */
    _getHeaders (){
        var ajaxHeader = {};

        switch(WebConfig.userSession.currentPortal) {
            case "back-office":
                // For back office portal usage do not send any language.
                break;

            case "company-admin":
            case "company-workspace":
                // For company portl usage always send languages of user, company.
                if(!_.isNil(WebConfig.userSession.currentUserLanguage)) {
                    ajaxHeader["GISC-UserLanguage"] = WebConfig.userSession.currentUserLanguage;
                }
                if(!_.isNil(WebConfig.companySettings.languageCode)) {
                    ajaxHeader["GISC-CompanyLanguage"] = WebConfig.companySettings.languageCode;
                }
                if(!_.isNil(WebConfig.userSession.currentCompanyId)) {
                    ajaxHeader["GISC-CompanyId"] = WebConfig.userSession.currentCompanyId;
                }
                break;
        }
        return ajaxHeader;
    }

    /**
     * Base AJAX request method
     * 
     * @param {any} verb
     * @param {any} url
     * @param {any} data
     * @param {boolean} [includeHeader=true]
     * @returns jQuery Deferred object
     */
    _ajaxRequest(verb, url, data, includeHeader = true) {
        var ajaxOption  = {
            cache: false,
            url: this.resolveUrl(url),
            type: verb,
            data: $.isEmptyObject(data) ? "{}" : data,
            crossDomain: true,
            xhrFields: { // Force jQuery send cookie request.
                withCredentials: true
            }
        };

        if (includeHeader) {
            ajaxOption.headers = this._getHeaders();
        }

        // jQuery AJAX response is array with 3 objects, 
        // 1. the actual response object
        // 2. response status
        // 3. XHR object.
        // We need only 1, so create Deferred to handle it manually.
        var dfd = $.Deferred();

        var beginAjax = new Date();
        $.ajax(ajaxOption).done((r) => {
            dfd.resolve(r);
        }).fail((e) => {
            dfd.reject(e);
        }).always(()=>{
            // Inform developer when service is greather than 1 seconds.
            var ajaxDuration = ((new Date() - beginAjax) / 1000);
            if(ajaxDuration > 1) {
                Logger.warn(`${verb} ${url} service takes ${ajaxDuration} seconds, please contact your service provider.`);
            }
        });

        return dfd;
    }

    /**
     * Raise jQuery ajax POST.
     * 
     * @param {any} url
     * @param {any} data
     * @param {boolean} [includeHeader=true]
     * @returns jQuery Deferred object
     */
    ajaxPost(url, data, includeHeader = true) {
        return this._ajaxRequest("post", url, this._postify(data), includeHeader);
    }

    /**
     * Raise jQuery ajax with GET.
     * 
     * @param {any} url
     * @param {any} data - query string, will be _postify internally
     * @param {boolean} [includeHeader=true]
     * @returns jQuery Deferred object
     */
    ajaxGet(url, data, includeHeader = true) {
        return this._ajaxRequest("get", url, this._postify(data), includeHeader);
    }

    /**
     * Raise jQuery ajax with PUT.
     * 
     * @param {any} url
     * @param {any} data
     * @param {boolean} [includeHeader=true]
     * @returns jQuery Deferred object
     */
    ajaxPut(url, data, includeHeader = true) {
        return this._ajaxRequest("put", url, this._postify(data), includeHeader);
    }

    /**
     * Raise jQuery ajax with DELETE.
     * 
     * @param {any} url
     * @param {any} data
     * @param {boolean} [includeHeader=true]
     * @returns jQuery Deferred object
     */
    ajaxDelete(url, data, includeHeader = true) {
        return this._ajaxRequest("delete", url, this._postify(data), includeHeader);
    }

    /**
     * Resolve absolute path from server relative path.
     * 
     * @param {any} serverRelativePath
     */
    resolveUrl(serverRelativePath) {
        return Utility.resolveUrl(this.applicationUrl, serverRelativePath);
    }

    /**
     * Convert Complex JSON object to flat object.
     * This method is used for prepare complex JSON data before calling GET.
     * 
     * @param {any} value
     * @param {any} prefix
     * @returns
     */
    _postify (value, prefix) {
        var result = {};

        var buildResult = function (object, prefix) {
            for (var key in object) {

                var postKey = isFinite(key)
                    ? (prefix != "" ? prefix : "") + "[" + key + "]"
                    : (prefix != "" ? prefix + "." : "") + key;
                switch (typeof (object[key])) {
                    case "number": case "string": case "boolean":
                        result[postKey] = object[key];
                        break;

                    case "object":
                        if (object[key] != null && object[key].toUTCString)
                            result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
                        else {
                            buildResult(object[key], postKey != "" ? postKey : key);
                        }
                }
            }
        };

        if(value !== null && value !== undefined){ 
            buildResult(value, (typeof (prefix) === "string" && prefix.length > 0) ? prefix : "");
        }

        return result;
    }
}

export default WebRequestBase;