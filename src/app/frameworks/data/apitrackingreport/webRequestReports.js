import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestReports
 * @extends {WebRequest}
 */
class WebRequestReports extends WebRequestBase {

    /**
     * Creates an instance of WebRequestUser.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);

    }


    /**
     * Create singleton for webrequestReports.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apireport-webrequestReports", 
            new WebRequestReports("")
        );
    }


    /**
     * Get list of summary business unit.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestBusinessUnit
     */
    listBusinessUnitSummary(filter) {
       return this.ajaxPost(WebConfig.appSettings.apiCoreUrl+"/api/businessUnit/summary/list", filter);
    }


    /**
     * List summary of vehicles
     * @param {any} filter
     * @returns
     */
    listVehicleSummary(filter) {
        return this.ajaxPost(WebConfig.appSettings.apiTrackingCoreUrl+"/api/vehicle/summary/list", filter);
    }




}

export default WebRequestReports;