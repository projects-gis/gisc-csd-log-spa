import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestReports
 * @extends {WebRequest}
 */
class WebRequestReportExports extends WebRequestBase {

    /**
     * Creates an instance of WebRequestUser.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);

    }


    /**
     * Create singleton for webrequestReports.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apireport-webrequestReports", 
            new WebRequestReportExports(WebConfig.appSettings.apiTrackingReportUrl)
        );
    }


    /**
     * Export General CSV.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestExports
     */
    exportCSVTracklocReport() {
        return this.resolveUrl("~/api/TracklocReport/exportCSV");
    }

    sendEmailTraclocReport(filter) {
        return this.ajaxPost("~/api/TracklocReport/SendEmail", filter);
    }

    exportCSVDrivingReport() {
        return this.resolveUrl("~/api/DrivingReport/exportCSV");
    }

    exportCSVParkingReport() {
        return this.resolveUrl("~/api/StoppageReport/exportCSV");
    }

    exportCSVOpenCloseReport() {
        return this.resolveUrl("~/api/GateOpenReport/exportCSV");
    }

    exportCSVBadGpsReport() {
        return this.resolveUrl("~/api/BadGpsReport/exportCSV");
    }

    exportCSVOverSpeedReport() {
        return this.resolveUrl("~/api/OverSpeedReport/exportCSV");
    }

    exportCSVTruckNotUpdateReport() {
        return this.resolveUrl("~/api/TruckNotUpdateReport/exportCSV");
    }


    /**
     * Export Comparison CSV.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestExports
     */
    exportCSVComparisonReport() {
        return this.resolveUrl("~/api/ComparisonReport/exportCSV");
    }


    /**
     * Export Summary CSV.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestExports
     */
    exportCSVTravelDistanceTimeSummaryReport() {
        return this.resolveUrl("~/api/TravelDistanceTimeReport/exportCSV");
    }

    exportCSVParkingEngineOnOverTimeReport() {
        return this.resolveUrl("~/api/ParkingEngineOnReport/exportCSV");
    }

    exportCSVOverallSummaryReport() {
        return this.resolveUrl("~/api/OverallSummaryReport/exportCSV");
    }

    exportCSVDailySummaryReport() {
        return this.resolveUrl("~/api/SummaryReportDay/exportCSV");
    }

    exportCSVDrivingTimeDurationSummaryReport() {
        return this.resolveUrl("~/api/DrivingSummaryReportTimeDuration/ExportCSV");
    }

    exportCSVVehicleUsageSummaryReport() {
        return this.resolveUrl("~/api/VehicleUsageSummaryReport/exportCSV");
    }

    exportCSVShipmentSummaryReport() {
        return this.resolveUrl("~/api/DriverSummaryShipmentReport/exportCSV");
    }

    exportCSVActivatingDrivingSummaryReport() {
        return this.resolveUrl("~/api/ActivatingDrivingLicenseReport/exportCSV");
    }

    exportCSVWorkingTimeSummaryReport() {
        return this.resolveUrl("~/api/WorkingTimeSummaryReport/exportCSV");
    }


    /**
     * Export Delinquent CSV.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestExports
     */
    exportCSVDelinquentReport() {
        return this.resolveUrl("~/api/DelinquentReport/exportCSV");
    }

    
    /**
     * Export Advance CSV.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestExports
     */
    exportCSVTrackLocationAdvanceReport() {
        return this.resolveUrl("~/api/TrackLocationAdvanceReport/exportCSV");
    }

    exportCSVOverspeedAdvanceReport() {
        return this.resolveUrl("~/api/OverspeedAdvanceReport/exportCSV");
    }
    previewCSVTrackLocationAdvanceReport(filter) {
        return this.ajaxPost("~/api/TrackLocationAdvanceReport/PreviewReportCSV", filter);
    }
    previewCSVOverspeedAdvanceReport(filter) {
        return this.ajaxPost("~/api/OverspeedAdvanceReport/exportCSV", filter);
    }


}

export default WebRequestReportExports;