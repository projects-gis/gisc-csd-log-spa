import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /City 
 * 
 * @class WebRequestCity
 * @extends {WebRequest}
 */
class WebRequestCity extends WebRequestBase {

    /**
     * Creates an instance of WebRequestCity.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestCity.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestCity", 
            new WebRequestCity(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of city.
     * 
     * @param {any} filter
     * @returns
     */
    listCity(filter) {
        return this.ajaxPost("~/api/city/list", filter);
    }
}

export default WebRequestCity;