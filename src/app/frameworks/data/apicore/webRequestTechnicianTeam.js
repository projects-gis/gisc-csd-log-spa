﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Subscription 
 * 
 * @class WebRequestTechnicianTeam
 * @extends {WebRequest}
 */
class WebRequestTechnicianTeam extends WebRequestBase {

    /**
     * Creates an instance of WebRequestTechnicianTeam.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestTechnicianTeam.
     * 
     * @static
     * @returns instance of WebRequestTechnicianTeam
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestTechnicianTeam",
            new WebRequestTechnicianTeam(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of subscription.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestTechnicianTeam
     */
    listTechnicianTeam(filter) {
        return this.ajaxPost("~/api/TechnicianTeam/list", filter);
    }

    /**
     * Get list of subscription summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestTechnicianTeam
     */
    listTechnicianTeamSummary(filter) {
        return this.ajaxPost("~/api/TechnicianTeam/summary/list", filter);
    }

    /**
    * Get list of subscription summary
    * 
    * @param {any} filter
    * @returns
    * 
    * @memberOf WebRequestTechnicianTeam
    */
    listTechnicianTeamSummaryUserlist(filter) {
        return this.ajaxPost("~/api/TechnicianTeam/summary/userlist", filter);
    }

    listTechnicianTeamSummaryUserlistUpdate(id,includeAssociationNames=null) {
        return this.ajaxPost(`~/api/TechnicianTeam/summary/userlist?id=${id}&includeAssociationName=${includeAssociationNames}`);
    }

    /**
     * Get subscription by id
     * 
     * @param {any} id, TechnicianId
     * @param {Array} includeAssociationNames, associations in user result.
     * @returns jQuery deferred.
     * 
     * @memberOf WebRequestTechnicianTeam
     */
    getTechnicianTeam(id,includeAssociationNames ) {
        
        return this.ajaxGet(`~/api/TechnicianTeam/${id}`,includeAssociationNames);
    }

    /**
     * Create subscription with given subscription info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebRequestTechnicianTeam
     */
    createTechnicianTeam(info, returnResult = true) {
        return this.ajaxPost(`~/api/TechnicianTeam?returnResult=${returnResult}`, info);
    }

    /**
     * Update subscription with given subscription info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf WebRequestTechnicianTeam
     */
    updateTechnicianTeam(info, returnResult = false) {
        return this.ajaxPut(`~/api/TechnicianTeam?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an subscription 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestTechnicianTeam
     */
    deleteTechnicianTeam(id) {
        return this.ajaxDelete("~/api/TechnicianTeam", [id]);
    }
}

export default WebRequestTechnicianTeam;