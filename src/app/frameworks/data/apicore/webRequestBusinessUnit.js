import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Vendor 
 * 
 * @class WebRequestVendor
 * @extends {WebRequestBase}
 */
class WebRequestBusinessUnit extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBusinessUnit.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request business unit.
     * 
     * @static
     * @returns instance of WebRequestBusinessUnit
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestBusinessUnit",
            new WebRequestBusinessUnit(WebConfig.appSettings.apiCoreUrl)
        );
    }
    /**
     * Get list of summary business unit.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestBusinessUnit
     */
    listBusinessUnitSummary(filter) {
       return this.ajaxPost("~/api/businessUnit/summary/list", filter);
    }

    listBusinessUnitParentSummary(filter) {
        return this.ajaxPost("~/api/businessUnit/summary/parentList", filter);
     } 
     listBusinessUnitChildListSummary(filter){
        return this.ajaxPost("~/api/businessUnit/summary/childList", filter);
     }
     

    /**
     * Get business unit by id
     * 
     * @param {any} id, businessUnitId
     * @returns
     * 
     * @memberOf webRequestBusinessUnit
     */
    getBusinessUnit(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/businessUnit/${id}`, data);
    }
    /**
     * Create business unit with given business unit info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestBusinessUnit
     */
    createBusinessUnit(info, returnResult = false) {
        return this.ajaxPost(`~/api/businessUnit?returnResult=${returnResult}`, info);
    }
    /**
     * Update business unit with given business unit info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestBusinessUnit
     */
    updateBusinessUnit(info, returnResult = false) {
        return this.ajaxPut(`~/api/businessUnit?returnResult=${returnResult}`, info);
    }
    /**
     * Delete an business unit 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestBusinessUnit
     */
    deleteBusinessUnit(id) {
        return this.ajaxDelete("~/api/businessUnit", [id]);
    }
}

export default WebRequestBusinessUnit;