import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /City 
 * 
 * @class WebRequestAnnouncement
 * @extends {WebRequest}
 */
class WebRequestAnnouncementBusinessUnit extends WebRequestBase {

    /**
     * Creates an instance of webRequestAnnouncementBusinessUnit.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for webRequestAnnouncementBusinessUnit.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestAnnouncementBusinessUnit", 
            new WebRequestAnnouncementBusinessUnit(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of announcementBusinessUnit.
     * 
     * @param {any} filter
     * @returns
     */
    listAnnouncementBusinessUnitSummary(filter) {
        return this.ajaxPost("~/api/announcementBusinessUnit/summary/list", filter);
    }

    /**
     * Get announcementBusinessUnit entity from server.
     * 
     * @param {any} filter
     * @returns 
     */
    getAnnouncementBusinessUnit(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/announcementBusinessUnit/${id}`, includeAssociationNames);
    }

     /**
     * Create announcementBusinessUnit.
     * 
     * @param {any} filter
     * @returns 
     */
    createAnnouncementBusinessUnit(info, returnResult = true) {
        return this.ajaxPost(`~/api/announcementBusinessUnit?returnResult=${returnResult}`, info);
    }

    /**
     * Create announcementBusinessUnit.
     * 
     * @param {any} filter
     * @returns 
     */
    updateAnnouncementBusinessUnit(info, returnResult = true) {
        return this.ajaxPut(`~/api/announcementBusinessUnit?returnResult=${returnResult}`, info);
    }

    /**
     * Get or Update announcementBusinessUnit notification.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listAnnouncementBusinessUnitNotification(filter) {
        return this.ajaxPost("~/api/announcementBusinessUnit/notification/list", filter);
    }
     /**
     * Delete announcementBusinessUnit.
     * 
     * @param {any} id
     * @returns
     */
    deleteAnnouncementBusinessUnit(id) {
        return this.ajaxDelete("~/api/announcementBusinessUnit", [id]);
    }


    /**
     * Update notification status of announcementBusinessUnit.
     * 
     * @param {any} info
     * @returns jQuery deferred.
     */
    updateAnnouncementBusinessUnitNotification(info) {
        return this.ajaxPost("~/api/announcementBusinessUnit/notification/status", info);
    }
}

export default WebRequestAnnouncementBusinessUnit;