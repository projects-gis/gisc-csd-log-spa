import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequestBase}
 */
class WebRequestUser extends WebRequestBase {

    /**
     * Creates an instance of WebRequestUser.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns instance of WebRequestUser
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestUser",
            new WebRequestUser(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Check current user has already logged in.
     * 
     * @returns true if current user is already authenticated, false otherwise.
     */
    isAutheticated () {
        return this.ajaxGet("~/api/user/authenticate/check", null, false);
    }

    /**
     * Login user by token id.
     * 
     * @param {any} token
     * @returns jQuery deferred.
     */
    loginByToken (token) {
        var dfd = $.Deferred();

        this.isAutheticated().done(isAutheticated => {
            if (!isAutheticated) {
                // Calling ajax for refersh data.
                this.ajaxPost("~/api/user/loginByToken", {token: token}, false).done(userInfo => {
                    dfd.resolve();
                }).fail((error)=>{
                    dfd.reject(error);
                });
            }
            else {
                // Already logged in so skip server token validation.
                dfd.resolve();
            }
        });

        return dfd;
    }

    /**
     * Get User permissions.
     * 
     * @returns jQuery deferred.
     */
    getUserPermissions() {
        return this.ajaxGet("~/api/user/permissions");
    }

    /**
     * Get user preferrence from server.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    getUserPreferences(filter) {
        return this.ajaxGet("~/api/user/preferences", filter);
    }

    /**
     * Update user preferences from page.
     * 
     * @param {any} model
     */
    updateUserPreferences(model) {
        return this.ajaxPost("~/api/user/preferences", model);
    }

    /**
     * Logout user from authentication session.
     * 
     * @memberOf WebRequestUser
     */
    logout (){
        return this.ajaxPost("~/api/user/logout", null, false);
    }

    /**
     * Reset password for an user.
     * 
     * @param {any} userId
     * @param {any} [newPassword=null]
     * @param {any} [confirmPassword=null]
     * @returns
     */
    resetUserPassword(userId, newPassword = null, confirmPassword = null) {
        var data = {
            id: userId,
            newPassword: newPassword,
            confirmPassword: confirmPassword
        };
        return this.ajaxPost("~/api/user/password/reset", data);
    }

    /**
     * Get list of user in summary format.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listUserSummary(filter) {
        return this.ajaxPost("~/api/user/summary/list", filter);
    }

    /**
     * Get user by id
     * 
     * @param {any} id, userId
     * @returns jQuery deferred.
     */
    getUser(id, includeAssociationNames = [], includeHeader = true) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/user/${id}`, data, includeHeader);
    }
    
    /**
     * Create user with given userModel
     * 
     * @param {any} userModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createUser(userModel, returnResult = true) {
        return this.ajaxPost(`~/api/user?returnResult=${returnResult}`, userModel);
    }

    /**
     * Update user with given userModel
     * 
     * @param {any} userModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    updateUser(userModel, returnResult = true) {
        return this.ajaxPut(`~/api/user?returnResult=${returnResult}`, userModel);
    }

    /**
     * Delete an user 
     * 
     * @param {any} id
     * @returns
     */
    deleteUser(id) {
        return this.ajaxDelete("~/api/user", [id]);
    }

    /**
     * Get user portal mode for check admin.
     * 
     * @returns
     */
    getUserPortalModes() {
        return this.ajaxGet("~/api/user/portalModes");
    }

    /**
     * Determine current vehicle has already associated with fleet service.
     * 
     * @param {any} id 
     * @returns {jQuery Deferred}
     */
    checkFleetServiceAssociation(id) {
        return this.ajaxGet(`~/api/user/${id}/mobileService/associate`);
    }
    /**
    * Get Employee PEA Detail 
    * 
    * @param {any} Username 
    */
    getPEAIdmEmployee(username) {
        return this.ajaxGet(`~/api/user/PEAIdmEmployee?Username=${username}`);
    }
}

export default WebRequestUser;