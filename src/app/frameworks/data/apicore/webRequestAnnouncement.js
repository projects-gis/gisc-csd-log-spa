import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /City 
 * 
 * @class WebRequestAnnouncement
 * @extends {WebRequest}
 */
class WebRequestAnnouncement extends WebRequestBase {

    /**
     * Creates an instance of webRequestAnnouncement.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for webRequestAnnouncement.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestAnnouncement", 
            new WebRequestAnnouncement(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of announcement.
     * 
     * @param {any} filter
     * @returns
     */
    listAnnouncementSummary(filter) {
        return this.ajaxPost("~/api/announcement/summary/list", filter);
    }

    /**
     * Get announcement entity from server.
     * 
     * @param {any} filter
     * @returns 
     */
    getAnnouncement(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/announcement/${id}`, includeAssociationNames);
    }

     /**
     * Create announcement.
     * 
     * @param {any} filter
     * @returns 
     */
    createAnnouncement(info, returnResult = true) {
        return this.ajaxPost(`~/api/announcement?returnResult=${returnResult}`, info);
    }

    /**
     * Create announcement.
     * 
     * @param {any} filter
     * @returns 
     */
    updateAnnouncement(info, returnResult = true) {
        return this.ajaxPut(`~/api/announcement?returnResult=${returnResult}`, info);
    }

    /**
     * Get or Update announcement notification.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listAnnouncementNotification(filter) {
        return this.ajaxPost("~/api/announcement/notification/list", filter);
    }
     /**
     * Delete announcement.
     * 
     * @param {any} id
     * @returns
     */
    deleteAnnouncement(id) {
        return this.ajaxDelete("~/api/announcement", [id]);
    }


    /**
     * Update notification status of announcement.
     * 
     * @param {any} info
     * @returns jQuery deferred.
     */
    updateAnnouncementNotification(info) {
        return this.ajaxPost("~/api/announcement/notification/status", info);
    }
}

export default WebRequestAnnouncement;