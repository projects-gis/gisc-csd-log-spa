import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Country 
 * 
 * @class WebRequestCountry
 * @extends {WebRequest}
 */
class WebRequestCountry extends WebRequestBase {

    /**
     * Creates an instance of WebRequestCountry.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestCountry.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestCountry", 
            new WebRequestCountry(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of country.
     * 
     * @param {any} filter
     * @returns
     */
    listCountry(filter) {
        return this.ajaxPost("~/api/country/list", filter);
    }
}

export default WebRequestCountry;