import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Vendor 
 * 
 * @class WebRequestVendor
 * @extends {WebRequestBase}
 */
class WebRequestEnumResource extends WebRequestBase {

    /**
     * Creates an instance of WebRequestEnumResource.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request enum resource.
     * 
     * @static
     * @returns instance of WebRequestEnumResource
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestEnumResource",
            new WebRequestEnumResource(WebConfig.appSettings.apiCoreUrl)
        );
    }
    /**
     * Get list of enum resource.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestEnumResource
     */
    listEnumResource(filter) {
       return this.ajaxPost("~/api/enumResource/list", filter);
    }
}

export default WebRequestEnumResource;