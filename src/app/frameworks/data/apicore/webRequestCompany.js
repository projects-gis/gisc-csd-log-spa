import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Company 
 * 
 * @class WebRequestCompany
 * @extends {WebRequest}
 */
class WebRequestCompany extends WebRequestBase {

    /**
     * Creates an instance of WebRequestCompany.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for WebRequestCompany.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webrequestCompany", 
            new WebRequestCompany(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of company in summary format.
     * 
     * @param {any} filter
     * @returns
     */
    listCompanySummary(filter) {
        return this.ajaxPost("~/api/company/summary/list", filter);
    }

    listCompany(filter) {
        return this.ajaxPost("~/api/company/list", filter);
    }

    /**
     * Get company information.
     * 
     * @param {any} id, companyId
     * @param {Array} includeAssociationNames, associations in company result.
     * @returns jQuery deferred.
     */
    getCompany(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/company/${id}`, includeAssociationNames);
    }

    /**
     * 
     * Info = CompanyInfo
     * @param {any} info
     * @returns
     */
    validateCompanySubscription(info){
        return this.ajaxPost("~/api/company/subscription/validate", info);
    }

    validateCompanyContact (infos) {
        return this.ajaxPost("~/api/company/contact/validate", infos);
    }

    validateCompanyRegional (infos) {
        return this.ajaxPost("~/api/company/regional/validate", infos);
    }

    validateCompanySetting (infos) {
        return this.ajaxPost("~/api/company/setting/validate", infos);
    }

    createCompany (info, returnResult = false) {
        return this.ajaxPost(`~/api/company?returnResult=${returnResult}`, info);
    }

    updateCompany (info, returnResult = false) {
        return this.ajaxPut(`~/api/company?returnResult=${returnResult}`, info);
    }

    deleteCompany (id) {
        return this.ajaxDelete("~/api/company", [id]);
    }

    
    /**
     * 
     * Get Company Setting
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestCompany
     */
    getCompanySetting (id) {
        return this.ajaxGet(`~/api/company/setting/${id}`);
    }
}

export default WebRequestCompany;