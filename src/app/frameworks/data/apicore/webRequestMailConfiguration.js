﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Language 
 * 
 * @class WebRequestLanguage
 * @extends {WebRequestBase}
 */
class WebRequestMailConfiguration extends WebRequestBase {

    /**
     * Creates an instance of WebRequestLanguage.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request language.
     * 
     * @static
     * @returns instance of WebRequestLanguage
     */
    static getInstance() {
        return Singleton.getInstance("apicore-WebRequestMailConfiguration",
            new WebRequestMailConfiguration(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of MailConfig.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestMailConfiguration
     */
    listMailConfig(filter) {
        return this.ajaxPost("~/api/MailConfiguration/list", filter);
    }

    summarylistMailConfig(filter) {
        return this.ajaxPost("~/api/MailConfiguration/summary/list", filter);
    }

    findLogsSummary(filter) {
        return this.ajaxPost("~/api/MailConfiguration/FindLogSummary", filter);
    } 

    expoertEntity(filter) {
        return this.ajaxPost("~/api/MailConfiguration/ExportEntity", filter);
    }

    getMailConfiguration(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/MailConfiguration/${id}`, includeAssociationNames);
    }

    createMailConfiguration(filter){
        return this.ajaxPost("~/api/MailConfiguration/Create/", filter);
    }  
    updateMailConfiguration(filter){
        return this.ajaxPut ("~/api/MailConfiguration/Update",filter);
    }
    deleteMailConfiguration(id){
        return this.ajaxDelete ("~/api/MailConfiguration/Delete",[id]);
    }
    getVersionSummary(filter){
        return this.ajaxPost ("~/api/MailConfiguration/VersionHistorySummary",filter);   
    }

    downloadFileAutoMail(filter) {
        console.log(filter);
        return this.ajaxPost("~/api/MailConfiguration/DownloadFile", filter);
    }

    validateSqlStatement (info){
        return this.ajaxPost("~/api/MailConfiguration/validate",info);
    }

}

export default WebRequestMailConfiguration;