﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";


class WebRequestEmail extends WebRequestBase {


    constructor(applicationUrl) {
        super(applicationUrl);
    }

   
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestEmail",
            new WebRequestEmail(WebConfig.appSettings.apiCoreUrl)
        );
    }
  
    listEmailLog(filter) {
        return this.ajaxPost("~/api/EmailLogs/Summary/list", filter);
    }
}

export default WebRequestEmail;