import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /City 
 * 
 * @class webRequestDLTReport
 * @extends {WebRequest}
 */
class WebRequestDLTReport extends WebRequestBase {

    /**
     * Creates an instance of webRequestDLTReport.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Create singleton for webRequestDLTReport.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("apicore-webRequestDLTReport", 
            new WebRequestDLTReport(WebConfig.appSettings.apiCoreUrl)
        );
    }

    /**
     * Get list of DLTReport.
     * 
     * @param {any} filter
     * @returns
     */
    listDLTSummary(filter) {
        return [this.resolveUrl("~/api/DLTReport/summary/list"),filter];
    }

    exportDLTSummary(filter) {
        return this.ajaxPost("~/api/DLTReport/export", filter);
    }

}

    export default WebRequestDLTReport;