﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

class WebRequestAppointment extends WebRequestBase { 
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestAppointment",
            new WebRequestAppointment(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    listAppointment(filter) {
        return this.ajaxPost("~/api/appointment/summary/list", filter);
    }

    listAppointmentVehicle(filter) {
        return this.ajaxPost("~/api/appointment/vehicle/list", filter);
    }

    getAppointment (id, includeAssociationNames) {
        return this.ajaxGet(`~/api/appointment/${id}`, includeAssociationNames);
    }

    listVehicle(filter) {
        return this.ajaxPost("~/api/appointment/vehicle/list", filter);
    }

    createAppointment (info, returnResult = false) {
        return this.ajaxPost(`~/api/appointment?returnResult=${returnResult}`, info);
    }

    updateAppointment (info, returnResult = false) {
        return this.ajaxPut(`~/api/appointment?returnResult=${returnResult}`, info);
    }

    postponeAppointment (info, returnResult = false) {
        return this.ajaxPut(`~/api/appointment/postpone?returnResult=${returnResult}`, info);
    }

    completeAppointment(info, returnResult = false) {
        return this.ajaxPut(`~/api/appointment/complete?returnResult=${returnResult}`, info);
    }

    cancelAppointment (info, returnResult = false) {
        return this.ajaxPut(`~/api/appointment/cancel?returnResult=${returnResult}`, info);
    }

    deleteAppointment (id) {
        return this.ajaxDelete(`~/api/appointment`, [id]);
    }
}

export default WebRequestAppointment;