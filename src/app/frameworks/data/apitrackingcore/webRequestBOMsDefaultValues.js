import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestDMS
 * @extends {WebRequest}
 */
class WebRequestBOMsDefaultValues extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBOMsDefaultValues.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestBOMsDefaultValues
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestBOMsDefaultValues",
            new WebRequestBOMsDefaultValues(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Create BOMs DefaultValues.
     * 
     */
    createBOMsDefaultValues(filter, returnResult = true) {
        return this.ajaxPost(`~/api/BOMsDefaultValue?returnResult=${returnResult}`, filter);
    }

    /**
     * Update BOMs DefaultValues.
     * 
     */
    updateBOMsDefaultValues(filter, returnResult = true) {
        return this.ajaxPut(`~/api/BOMsDefaultValue?returnResult=${returnResult}`, filter);
    }

    /**
    * Delete BOMs DefaultValues.
    * 
    */
    deleteBOMsDefaultValues(id) {
        return this.ajaxDelete(`~/api/BOMsDefaultValue`, [id]);
    }

    /**
     * Get BOMs DefaultValues.
     * 
     */
    getBOMsDefaultValues(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/BOMsDefaultValue/${id}`, includeAssociationNames);
    }

    /**
     * List BOMs DefaultValues.
     * 
     */
    listBOMsDefaultValues(filter) {
        return this.ajaxPost(`~/api/BOMsDefaultValue/summary/list`, filter);
    }
    
}

export default WebRequestBOMsDefaultValues;