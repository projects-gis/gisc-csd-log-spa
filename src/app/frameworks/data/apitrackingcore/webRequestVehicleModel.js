import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * 
 * 
 * @class WebRequestVehicleModel
 * @extends {WebRequestBase}
 */
class WebRequestVehicleModel extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVehicleModel.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestVehicleModel
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }


    /**
     * Get singleton of vehicle web request.
     *
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestVehicleModel",
            new WebRequestVehicleModel(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestVehicleModel
     */
    listVehicleModelSummary(filter) {
        return this.ajaxPost("~/api/vehicleModel/summary/list", filter);
    }

    /**
     * Get specific vehicle model.
     * 
     * @param {any} id
     */
    getVehicleModel(id) {
        var data = {};
        return this.ajaxGet(`~/api/vehicleModel/${id}`, data);
    }

    /**
     * Create vehicle model.
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createVehicleModel(info, returnResult = false) {
        return this.ajaxPost(`~/api/vehicleModel?returnResult=${returnResult}`, info);
    }

    /**
     * Update vehicle model.
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateVehicleModel(info, returnResult = false) {
        return this.ajaxPut(`~/api/vehicleModel?returnResult=${returnResult}`, info);
    }

    /**
     * Delete vehicle model by Ids.
     * 
     * @param {any} id
     * @returns
     */
    deleteVehicleModel(id) {
        return this.ajaxDelete("~/api/vehicleModel", [id]);
    }
}

export default WebRequestVehicleModel;