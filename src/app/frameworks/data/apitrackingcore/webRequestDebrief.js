﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestDebrief extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDriver.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request driver.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDriver
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestDebrief",
            new WebRequestDebrief(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of driver summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    debrief(filter) {
        return this.ajaxPost("~/api/Debrief", filter);
    }


  
}

export default WebRequestDebrief;