import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class webRequestTickerResponse
 * @extends {WebRequestBase}
 */
class WebRequestTickerResponse extends WebRequestBase {

    /**
     * Creates an instance of WebRequestTickerResponse.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestTickerResponse
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestTickerResponse
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestTickerResponse",
            new WebRequestTickerResponse(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    ticketResponseList(filter) {
        return this.ajaxPost("~/api/alertTicketResponseConfiguration/summary/list", filter);
    }
    
    ticketResponseViewDetail(id, includeAssociationNames){
        return this.ajaxGet(`~/api/alertTicketResponseConfiguration/${id}`, includeAssociationNames);
    }

    createTicketResponse(filter, returnResult = true){
        return this.ajaxPost(`~/api/alertTicketResponseConfiguration?returnResult=${returnResult}`,filter);
    }

    updateTicketResponse(filter, returnResult = true){
        return this.ajaxPut(`~/api/alertTicketResponseConfiguration?returnResult=${returnResult}`,filter);
    }

    deleteTicketResponse(id){
        return this.ajaxDelete("~/api/alertTicketResponseConfiguration",[id]);
    }    
}

export default WebRequestTickerResponse;