﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";
import {EntityAssociation, Enums} from "../../constant/apiConstant";

/**
 * Facade to access Core Web API in /CustomPOI
 * 
 * @class WebRequestCustomPOI
 * @extends {WebRequestBase}
 */
class WebRequestCustomPOI extends WebRequestBase {

    /**
     * Creates an instance of WebRequestCustomPOI
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request CustomPOI.
     * 
     * @static
     * @returns instance of WebRequestCustomPOI
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webrequestCustomPOI",
            new WebRequestCustomPOI(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of customPOI in summary format.
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listCustomPOISummary(filter) {
        return this.ajaxPost("~/api/customPoi/summary/list", filter);
    }

    /**
     * Get customPOI by id
     * 
     * @param {any} id, customPOIId
     * @returns jQuery deferred.
     */
    getCustomPOI(id) {
        var data = { 
            includeAssociationNames: [
                EntityAssociation.CustomPoi.Category,
                EntityAssociation.CustomPoi.Icon,
                EntityAssociation.CustomPoi.AccessibleBusinessUnits,
                EntityAssociation.CustomPoi.Country,
                EntityAssociation.CustomPoi.Province,
                EntityAssociation.CustomPoi.City,
                EntityAssociation.CustomPoi.Town
            ]
        };
        return this.ajaxGet(`~/api/customPoi/${id}`, data);
    }
    
    /**
     * Create CustomPOI with given customPOIModel
     * 
     * @param {any} customPOIModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createCustomPOI(customPOIModel, returnResult = true) {
        return this.ajaxPost(`~/api/customPoi?returnResult=${returnResult}`, customPOIModel);
    }

        /**
         * Update customPOI with given customPOIModel
         * 
         * @param {any} customPOIModel
         * @param {boolean} [returnResult=true]
         * @returns jQuery deferred.
         */
        updateCustomPOI(customPOIModel, returnResult = true) {
            return this.ajaxPut(`~/api/customPoi?returnResult=${returnResult}`, customPOIModel);
        }

        /**
         * Delete an CustomPOI 
         * 
         * @param {any} id
         * @returns
         */
        deleteCustomPOI(id) {
            return this.ajaxDelete(`~/api/customPoi`, [id]);
        }

        /**
         * Import Box
         * @memberOf WebRequestBox
         */
        importCustomPOI (info, save = false) {
            return this.ajaxPost(`~/api/customPoi/import?save=${save}`, info);
        }

        /**
        * Export CustomPoi
        * @param {any} filter
        * @returns
        */
        exportCustomPOI (filter) {
            return this.ajaxPost(`~/api/customPoi/export`, filter);
        }

        /**
         * Download import CustomPoi template
         * @returns
         */
        downloadCustomPoiTemplate (templateFileType = Enums.TemplateFileType.Xlsx) {
            return this.ajaxGet(`~/api/customPoi/template/download?templateFileType=${templateFileType}`);
        }

        autoComplete(filter) {
            return this.ajaxPost(`~/api/customPoi/autoComplete`, filter);
        }

        autoCompleteForShipment(filter) {
            return this.ajaxPost(`~/api/customPoi/autoCompleteForShipment`, filter);
        }
    }

    export default WebRequestCustomPOI;