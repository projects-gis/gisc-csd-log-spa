import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

class WebRequestCategory extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBox.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBox
     * 
     * @static
     * @returns instance of WebRequestBox
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebrequestCategory",
            new WebRequestCategory(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of category vehicle 
     * 
     * @param {any} filter
     * @returns
     */
    listCategory(filter) {
        return this.ajaxPost("~/api/category/list", filter);
    }

}

export default WebRequestCategory;