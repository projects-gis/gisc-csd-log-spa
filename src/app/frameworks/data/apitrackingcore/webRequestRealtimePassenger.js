import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestRealtimePassenger
 * @extends {WebRequest}
 */
class WebRequestRealtimePassenger extends WebRequestBase {

    /**
     * Creates an instance of WebRequestRealtimePassenger.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestRealtimePassenger
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestRealtimePassenger",
            new WebRequestRealtimePassenger(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List Dashboard.
     * 
     * @memberOf WebRequestRealtimePassenger
     */
    listDashboard(filter) {
        return this.ajaxPost("~/api/realtimepassenger/dashboard", filter);
    }

    /**
     * List Vehicle.
     * 
     * @memberOf WebRequestRealtimePassenger
     */
    listVehicle(filter) {
        return this.ajaxPost("~/api/realtimepassenger/vehiclelist", filter);
    }

    /**
     * List Autocomplete.
     * 
     * @memberOf WebRequestRealtimePassenger
     */
    autoComplete(filter) {
        return this.ajaxPost("~/api/realtimepassenger/Autocomplete", filter);
    }

    /**
     * Export.
     * 
     * @memberOf WebRequestRealtimePassenger
     */
    exportFile(filter) {
        return this.ajaxPost("~/api/realtimepassenger/export", filter);
    }

    /**
     * Post employeelist by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestRealtimePassenger
     */
    listEmployee(id) {
        return this.ajaxPost(`~/api/realtimepassenger/employeelist?JobId=${id}`);
    }

    
}

export default WebRequestRealtimePassenger;