import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestUser extends WebRequestBase {

    /**
     * Creates an instance of WebRequestUser.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestUser
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebrequestUser",
            new WebRequestUser(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**-------- Common ---------**/

    /**
     * Check current user has already logged in.
     * 
     * @returns
     * 
     * @memberOf WebRequestUser
     */
    isAutheticated () {
        return this.ajaxGet("~/api/user/authenticate/check", null, false);
    }

    /**
     * Login user by token id.
     * 
     * @param {any} token
     * @returns jQuery deferred.
     * 
     * @memberOf WebRequestUser
     */
    loginByToken (token) {
        var dfd = $.Deferred();

        this.isAutheticated().done(isAutheticated => {
            if (!isAutheticated) {
                // Calling ajax for refersh data.
                this.ajaxPost("~/api/user/loginByToken", {token: token}, false).done( userInfo => {
                    dfd.resolve();
                });
            } 
            else {
                // Already logged in so skip server token validation.
                dfd.resolve();
            }
        });

        return dfd;
    }

    /**
     * Logout user from authentication session.
     * 
     * @memberOf WebRequestUser
     */
    logout (){
        return this.ajaxPost("~/api/user/logout", null, false);
    }
}

export default WebRequestUser;