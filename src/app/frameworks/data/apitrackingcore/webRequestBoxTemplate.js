import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import {EntityAssociation} from "../../constant/apiConstant";

class WebRequestBoxTemplate extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBoxTemplate.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBoxTemplate
     * 
     * @static
     * @returns instance of WebRequestBoxTemplate
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebrequestBoxTemplate",
            new WebRequestBoxTemplate(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of boxTemplate in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxTemplateSummary(filter) {
        return this.ajaxPost("~/api/boxTemplate/summary/list", filter);
    }

    /**
     * Get list of box template in full format.
     * 
     * @param {any} filter
     * @returns
     */
    listBoxTemplate(filter) {
        return this.ajaxPost("~/api/boxTemplate/list", filter);
    }

    /**
     * Get box template by id.
     * 
     * @param {any} id
     * @returns
     */
    getBoxTemplate(id) {
        var data = { 
            includeAssociationNames: [
                EntityAssociation.BoxTemplate.Features
            ]
        };
        return this.ajaxGet(`~/api/boxTemplate/${id}`, data);
    }

    /**
     * Create Box Template.
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createBoxTemplate(info, returnResult = false) {
        return this.ajaxPost(`~/api/boxTemplate?returnResult=${returnResult}`, info);
    }

    /**
     * Update Box Template.
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateBoxTemplate(info, returnResult = false) {
        return this.ajaxPut(`~/api/boxTemplate?returnResult=${returnResult}`, info);
    }

    /**
     * Delete box template.
     * 
     * @param {any} id
     */
    deleteBoxTemplate(id) {
        return this.ajaxDelete("~/api/boxTemplate", [id]);
    }
}

export default WebRequestBoxTemplate;