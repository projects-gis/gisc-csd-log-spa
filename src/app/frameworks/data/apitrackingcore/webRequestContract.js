﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Alert 
 * 
 * @class WebRequestContract
 * @extends {WebRequestBase}
 */
class WebRequestContract extends WebRequestBase {

    /**
     * Creates an instance of WebRequestContract.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request alert.
     * 
     * @static
     * @returns instance of WebRequestContract
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestContract",
            new WebRequestContract(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

   
    listContract(filter) {
        return this.ajaxPost("~/api/contract/summary/list", filter);
    }

    updateContract(filter) {
        return this.ajaxPost("~/api/contract/asset/update", filter);
    }

   

}

export default WebRequestContract;