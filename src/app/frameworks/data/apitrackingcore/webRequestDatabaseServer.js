import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";


/**
 * 
 * 
 * @class WebRequestDatabaseServer
 * @extends {WebRequestBase}
 */
class WebRequestDatabaseServer extends WebRequestBase {
    /**
     * Creates an instance of WebRequestDatabaseServer.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestDatabaseServer
     */
    constructor(applicationUrl) {
        super(applicationUrl);

    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDatabaseServer
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-WebRequestDatabaseServer",
            new WebRequestDatabaseServer(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestDatabaseServer
     */
    listDatabaseServer(filter) {
       return this.ajaxPost("~/api/databaseServer/list", filter);
    }
}

export default WebRequestDatabaseServer;