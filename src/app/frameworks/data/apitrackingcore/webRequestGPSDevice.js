﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestGPSDevice extends WebRequestBase {

    /**
     * Creates an instance of WebRequestGPSDevice.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request GPSDevice.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestGPSDevice
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestGPSDevice",
            new WebRequestGPSDevice(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of GPSDevice summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestGPSDevice
     */
    listGPSDevice(filter) {
        return this.ajaxPost("~/api/gpsDevice/summary/list", filter);
    }

    /**
     * Get GPSDevice by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestGPSDevice
     */
    getGPSDevice(id) {
        return this.ajaxGet(`~/api/gpsDevice/${id}`);
    }

    /**
     * Update GPSDevice with given fleetService info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestFleetService
     */
    updateGPSDevice(info, returnResult = false) {
        return this.ajaxPut(`~/api/gpsDevice?returnResult=${returnResult}`, info);
    }

    exportGPSDevice(filter){
        return this.ajaxPost("~/api/gpsDevice/summary/export",filter);
   }
}

export default WebRequestGPSDevice;