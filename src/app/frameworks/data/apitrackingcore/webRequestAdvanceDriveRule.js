import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * 
 * @class WebRequestAdvanceDriverRule
 * @extends {WebRequestBase}
 */
class WebRequestAdvanceDriverRule extends WebRequestBase {

    /**
     * Creates an instance of WebRequestAdvanceDriverRule.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request WebRequestAdvanceDriverRule.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestAdvanceDriverRule",
            new WebRequestAdvanceDriverRule(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of advance driver rule summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestAdvanceDriverRule
     */
    listAdvanceDriverRuleSummary(filter) {
        return this.ajaxPost("~/api/advanceDriverPerformanceRule/summary/list", filter);
    }

    /**
     * Get advance driver rule by id
     * 
     * @param {any} id
     * @returns
     */
    getAdvanceDriverRule(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/advanceDriverPerformanceRule/${id}`, data);
    }

    /**
     * Create advance driver rule with given driver info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     */
    createAdvanceDriverRule(info, returnResult = true) {
        return this.ajaxPost(`~/api/advanceDriverPerformanceRule?returnResult=${returnResult}`, info);
    }

    /**
     * Update advance driver rule with given driver info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateAdvanceDriverRule(info, returnResult = true) {
        return this.ajaxPut(`~/api/advanceDriverPerformanceRule?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an advance driver rule
     * 
     * @param {any} id
     * @returns
     */
    deleteAdvanceDriverRule(id) {
        return this.ajaxDelete("~/api/advanceDriverPerformanceRule", [id]);
    }
}

export default WebRequestAdvanceDriverRule;