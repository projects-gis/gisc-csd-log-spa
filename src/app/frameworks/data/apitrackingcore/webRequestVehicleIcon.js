import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * 
 * 
 * @class WebRequestVehicleIcon
 * @extends {WebRequestBase}
 */
class WebRequestVehicleIcon extends WebRequestBase {


    /**
     * Creates an instance of WebRequestVehicleIcon.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestVehicleIcon
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }


    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestVehicleIcon",
            new WebRequestVehicleIcon(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * 
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    listVehicleIconSummary(filter) {
        return this.ajaxPost("~/api/vehicleIcon/summary/list", filter);
    }

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    createVehicleIcon(info, returnResult = true) {
        return this.ajaxPost(`~/api/vehicleIcon?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    updateVehicleIcon (info, returnResult = true) {
        return this.ajaxPut(`~/api/vehicleIcon?returnResult=${returnResult}`, info);
    }

    /**
     * 
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    getVehicleIcon(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/vehicleIcon/${id}`, includeAssociationNames);
    }

    /**
     * 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    deleteVehicleIcon (id) {
        return this.ajaxDelete("~/api/vehicleIcon", [id]);
    }
}

export default WebRequestVehicleIcon;