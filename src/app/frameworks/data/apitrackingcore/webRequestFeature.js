import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

class WebRequestFeature extends WebRequestBase {

    /**
     * Creates an instance of WebRequestFeature.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestFeature
     * 
     * @static
     * @returns instance of WebRequestFeature
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebrequestFeature",
            new WebRequestFeature(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of feature in full format.
     * 
     * @param {any} filter
     * @returns
     */
    listFeature(filter) {
        return this.ajaxPost("~/api/feature/list", filter);
    }

    /**
     * Get list of feature in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listFeatureSummary(filter) {
        return this.ajaxPost("~/api/feature/summary/list", filter);
    }

    /**
     * Get feature by id
     * 
     * @param {any} id, featureId
     * @returns jQuery deferred.
     */
    getFeature(id) {
        return this.ajaxGet(`~/api/feature/${id}`, null);
    }

    /**
     * Create feature with given featureModel
     * 
     * @param {any} featureModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createFeature(featureModel, returnResult = true) {
        return this.ajaxPost(`~/api/feature?returnResult=${returnResult}`, featureModel);
    }

    /**
     * Update feature with given featureModel
     * 
     * @param {any} userModel
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    updateFeature(featureModel, returnResult = true) {
        return this.ajaxPut(`~/api/feature?returnResult=${returnResult}`, featureModel);
    }

    /**
     * Delete a feature
     * 
     * @param {any} id of feature to delete
     * @returns jQuery deferred.
     */
    deleteFeature(id) {
        return this.ajaxDelete("~/api/feature", [id]);
    }
}

export default WebRequestFeature;