import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * Contains global search service (logistics, map). 
 * 
 * @class WebRequestGlobalSearch
 * @extends {WebRequest}
 */
class WebRequestGlobalSearch extends WebRequestBase {

    /**
     * Creates an instance of WebRequestGlobalSearch.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request driver.
     * 
     * @static
     * @returns WebRequestGlobalSearch
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestGlobalSearch",
            new WebRequestGlobalSearch(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Find category names by type.
     * 
     * @param {any} categoryType 
     * @returns {jQuery Deferred}
     */
    getCategory(categoryType) {
        return this.ajaxGet(`~/api/globalSearch/category`, {type: categoryType});
    }

    /**
     * Perform search.
     * 
     * @param {any} filter
     * @returns {jQuery Deferred}
     */
    list(filter) {
        return this.ajaxPost("~/api/globalSearch/list", filter);
    }

    /**
     * Auto Complete.
     * 
     * @param {any} filter
     * @returns {jQuery Deferred}
     */
    autocomplete(filter) {
        return this.ajaxPost("~/api/globalSearch/autoComplete", filter);
    }


    /**
     * Auto Complete.
     * 
     * @param {any} filter
     * @returns {jQuery Deferred}
     */
    autocompleteDriverVehicle(filter) {
        return this.ajaxPost("~/api/globalSearch/autocomplete/drivervehicle", filter);
    }



}

export default WebRequestGlobalSearch;