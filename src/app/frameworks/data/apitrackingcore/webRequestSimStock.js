import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestADAS
 * @extends {WebRequest}
 */
class WebRequestSimStock extends WebRequestBase {

    /**
     * Creates an instance of WebRequestSimStock.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestSimStock
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestSimStock",
            new WebRequestSimStock(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    createSimStock(info, returnResult = true) {
        return this.ajaxPost(`~/api/simStock?returnResult=${returnResult}`, info);
    }
    updateSimStock(info, returnResult = true) {
        return this.ajaxPut(`~/api/simStock?returnResult=${returnResult}`, info);
    }

    deleteSimStock(id) {
        return this.ajaxDelete("~/api/simStock", [id]);
    }
    /**
     * List SimStock Summary.
     * 
     * @memberOf WebRequestADAS
     */
    listSimStockSummary(filter) {
        return this.ajaxPost("~/api/simStock/summary/list", filter);
    }


    getSimStock(id, includeAssociationNames = []) {
        let data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/simStock/${id}`, data);
    }
}

export default WebRequestSimStock;