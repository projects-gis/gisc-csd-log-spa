import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /Vendor 
 * 
 * @class WebRequestVendor
 * @extends {WebRequestBase}
 */
class WebRequestVendor extends WebRequestBase {

    /**
     * Creates an instance of WebRequestVendor.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request vendor.
     * 
     * @static
     * @returns instance of WebRequestVendor
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestVendor",
            new WebRequestVendor(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
     * Get list of vendor.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestVendor
     */
    listVendor(filter) {
       return this.ajaxPost("~/api/vendor/list", filter);
    }

    listVendorSummary(filter) {
        return this.ajaxPost("~/api/vendor/summary/list", filter);
    }
    
    /**
     * Get vendor by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf webRequestVendor
     */
    getVendor(id) {
        return this.ajaxGet(`~/api/vendor/${id}`);
    }

    /**
     * Create vendor with given vendor info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestVendor
     */
    createVendor(info, returnResult = false) {
        return this.ajaxPost(`~/api/vendor?returnResult=${returnResult}`, info);
    }


    /**
     * Update vendor with given vendor info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     * @memberOf webRequestVendor
     */
    updateVendor(info, returnResult = false) {
        return this.ajaxPut(`~/api/vendor?returnResult=${returnResult}`, info);
    }

    /**
     * Delete vendor
     * @param {any} id
     * @returns
     */
    deleteVendor(id) {
        return this.ajaxDelete("~/api/vendor", [id]);
    }
}

export default WebRequestVendor;