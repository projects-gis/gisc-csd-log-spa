import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestDMS
 * @extends {WebRequest}
 */
class WebRequestDMS extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDMS.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDMS
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestDMS",
            new WebRequestDMS(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List DMS Summary.
     * 
     * @memberOf WebRequestDMS
     */
    listDMSSummary(filter) {
        return this.ajaxPost("~/api/DMS/summary/list", filter);
    }

    /**
     * Create DMS
     * 
     * @param {any} filter
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createDMS(filter, returnResult = true) {
        return this.ajaxPost(`~/api/DMS?returnResult=${returnResult}`, filter);
    }

    /**
     * DMS Model
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    //listDMSModel(filter) {
    //    return this.ajaxPost(`~/api/DMSModel/summary/list`, filter);
    //}

    /**
     * DMS Fleet
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    listDMSFleet(filter) {
        return this.ajaxPost(`~/api/DMSFleet/summary/list`, filter);
    }

    /**
     * Check Using Device
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    CheckUsingDevice(filter) {
        return this.ajaxPost(`~/api/DMS/checkUsingDevice`, filter);
    }

    /**
     * Get DMS by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestDMS
     */
    getDMS(id, includeAssociationNames = []) {
        let data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/DMS/${id}`, data);
    }

    /**
     * Update DMS
     * 
     * @param {any} filter
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    updateDMS(filter, returnResult = true) {
        return this.ajaxPut(`~/api/DMS?returnResult=${returnResult}`, filter);
    }

    /**
     * Delete an DMS 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestDMS
     */
    deleteDMS(id) {
        return this.ajaxDelete("~/api/DMS", [id]);
    }

    /**
     * Check Using Device
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    checkExistingDevice(filter) {
        return this.ajaxPost(`~/api/DMS/CheckExistingDevice`, filter);
    }

    /**
     * Check Using Device
     * 
     * @param {any} filter
     * @returns jQuery deferred.
     */
    checkMoveDevice(filter){
        return this.ajaxPost(`~/api/DMS/checkMoveDevice`, filter);
    }
    
}

export default WebRequestDMS;