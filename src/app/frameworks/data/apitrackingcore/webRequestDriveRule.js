import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Tracking Core Web API in /DriveRule 
 * 
 * @class WebRequestDriveRule
 * @extends {WebRequestBase}
 */
class WebRequestDriveRule extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDriveRule.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request driver.
     * 
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestDriveRule",
            new WebRequestDriveRule(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of drive rule summary
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestDriver
     */
    listDriveRuleSummary(filter) {
        return this.ajaxPost("~/api/driveRule/summary/list", filter);
    }

    /**
     * Get drive rule by id
     * 
     * @param {any} id
     * @returns
     */
    getDriveRule(id, includeAssociationNames = []) {
        var data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/driveRule/${id}`, data);
    }

    /**
     * Create drive rule with given driver info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     */
    createDriveRule(info, returnResult = true) {
        return this.ajaxPost(`~/api/driveRule?returnResult=${returnResult}`, info);
    }

    /**
     * Update drive rule with given driver info
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateDriveRule(info, returnResult = false) {
        return this.ajaxPut(`~/api/driveRule?returnResult=${returnResult}`, info);
    }

    /**
     * Delete an drive rule
     * 
     * @param {any} id
     * @returns
     */
    deleteDriveRule(id) {
        return this.ajaxDelete("~/api/driveRule", [id]);
    }
}

export default WebRequestDriveRule;