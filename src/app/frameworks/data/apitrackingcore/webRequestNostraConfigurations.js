﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";


/**
 * Facade to access Core Web API in /map
 * 
 * @class WebRequestMap
 * @extends {WebRequestBase}
 */
class WebRequestNostraConfigurations extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMap.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request map.
     * 
     * @static
     * @returns instance of WebRequestMap
     */
    static getInstance() {
        return Singleton.getInstance("trackingcore-webRequestNostraConfigurations",
            new WebRequestNostraConfigurations(WebConfig.appSettings.apiMapUrl)
        );
    }

    /**
     * List Nostra
     * @param {any} filter
     * @returns
     */
    nostraList(filter) {
        return this.ajaxPost("~/api/nostraconfiguration/list", filter);
    }

    /**
     * Get Nostra
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getNostra(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/nostraconfiguration/${id}`, includeAssociationNames);
    }

    /**
     * Update Nostra
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateNostra(info, returnResult = false) {
        return this.ajaxPut(`~/api/nostraconfiguration?returnResult=${returnResult}`, info);
    }

    

}

export default WebRequestNostraConfigurations;