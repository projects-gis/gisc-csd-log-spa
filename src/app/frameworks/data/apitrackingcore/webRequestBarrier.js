﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestBarrier
 * @extends {WebRequestBase}
 */
class WebRequestBarrier extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBarrier.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestBarrier
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestBarrier
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestBarrier",
            new WebRequestBarrier(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    list(filter) {
        return this.ajaxPost('~/api/barrier/summary/list', filter);
    }

    barrierDetail(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/barrier/${id}`, includeAssociationNames);
    }

    createBarrier(filter) {
        return this.ajaxPost('~/api/barrier', filter);
    }

    editBarrier(filter) {
        return this.ajaxPut('~/api/barrier', filter);
    }

    deleteBarrier(id) {
        return this.ajaxDelete('~/api/barrier', [id]);
    }

}

export default WebRequestBarrier;