import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestPassenger
 * @extends {WebRequest}
 */
class WebRequestPassenger extends WebRequestBase {

    /**
     * Creates an instance of WebRequestPassenger.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestPassenger
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestPassenger",
            new WebRequestPassenger(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List Department.
     * 
     * @memberOf WebRequestPassenger
     */
    listDepartment(filter) {
        return this.ajaxGet(`~/api/passenger/department/list`);
    }

    /**
     * List Section.
     * 
     * @memberOf WebRequestPassenger
     */
    listSection(filter) {
        return this.ajaxPost("~/api/passenger/section/list", filter);
    }

    /**
     * List Autocomplete.
     * 
     * @memberOf WebRequestPassenger
     */
    autoComplete(filter) {
        return this.ajaxPost("~/api/passenger/autocomplete", filter);
    }
    
}

export default WebRequestPassenger;