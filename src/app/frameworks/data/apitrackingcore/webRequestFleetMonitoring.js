import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webConfiguration";

/**
 * Facade to access Core Web API in /FleetMonitoring 
 * 
 * @class WebRequestFleetMonitoring
 * @extends {WebRequestBase}
 */
class WebRequestFleetMonitoring extends WebRequestBase {

    /**
     * Creates an instance of WebRequestFleetMonitoring.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request fleetMonitoring.
     * 
     * @static
     * @returns instance of WebRequestFleetMonitoring
     */
    static getInstance() {
        return Singleton.getInstance("trackingCore-webRequestFleetMonitoring",
            new WebRequestFleetMonitoring(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
     * Get list of TrackLocation.
     * 
     * @param {any} filter
     * @returns
     */
    listTrackLocation(filter) {
       return this.ajaxPost("~/api/fleetMonitoring/trackLocation/list", filter);
    }

    
    /**
     * Export list of TrackLocation
     * 
     * @param {any} filter
     * @returns
     */
    exportTrackLocation(filter) {
        return this.ajaxPost("~/api/fleetMonitoring/trackLocation/export", filter);
    }

    
    /**
     * Get list of asset result
     * 
     * @param {any} filter
     * @returns
     */
    listAsset(filter) {
        return this.ajaxPost("~/api/fleetMonitoring/asset/list", filter);
    }

    
    /**
     * Get list of nearest asset
     * filter = NearestAssetFilter
     * @param {any} filter
     * @returns
     */
    listNearestAsset(filter) {
        return this.ajaxPost("~/api/fleetMonitoring/nearestAsset/list", filter);
    }

    /**
     * Export list of nearest asset
     * filter = NearestAssetFilter
     * @param {any} filter
     * @returns
     */
    exportNearestAsset(filter) {
        return this.ajaxPost("~/api/fleetMonitoring/nearestAsset/export", filter);
    }

    /**
     * Send message to navigator.
     * 
     * @param {any} filter
     * @returns
     */
    sendMessageToNavigator(filter) {
        return this.ajaxPost("~/api/fleetMonitoring/message/send", filter);
    }

    
    /**
     * Find Playback Timelines
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestFleetMonitoring
     */
    findPlaybackTimelines(filter){
        return this.ajaxPost("~/api/fleetMonitoring/playback/timeline", filter);
    }

    
    /**
     * Get list of Playback TrackLocation.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf WebRequestFleetMonitoring
     */
    findPlaybackTrackLocations(filter){
        return this.ajaxPost("~/api/fleetMonitoring/playback/trackLocation", filter);
    }

    /**
     * Get list sms
     * 
     * @returns
     * 
     * @memberOf WebRequestFleetMonitoring
     */
    listSms() {
        return this.ajaxGet(`~/api/fleetMonitoring/trackLocationMobile/smslist`);
    }

    /**
     * Get send sms
     * 
     * @param {any} mobileno
     * @param {any} message
     * @returns
     * 
     * @memberOf WebRequestFleetMonitoring
     */
    sendSms(mobileno, message) {
        return this.ajaxGet(`~/api/fleetMonitoring/trackLocationMobile/sendsms?mobileno=${mobileno}&message=${message}`);
    }

    /**
     * Get list playback analysistool
     * 
     * @returns
     * 
     * @memberOf WebRequestFleetMonitoring
     */
    listPlaybackAnalysistool(filter) {
        return this.ajaxPost(`~/api/fleetMonitoring/playback/analysistool`, filter);
    }
}

export default WebRequestFleetMonitoring;