﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";
/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestTruckDriverUtilization extends WebRequestBase {

    /**
     * Creates an instance of WebRequestTruckDriverUtilization.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestTruckDriverUtilization",
            new WebRequestTruckDriverUtilization(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
   * Get list of tucket appointment.
   * 
   * @param {any} filter
   * @returns
   * 
   * @memberOf WebRequestTruckDriverUtilization
   */
    utilizationgraph(filter) {
        return this.ajaxPost("~/api/utilization/drivingtime/graph", filter);
    }

    /**
   * Get list of tucket appointment.
   * 
   * @param {any} filter
   * @returns
   * 
   * @memberOf WebRequestTruckDriverUtilization
   */
    utilizationdetail(filter) {
        return this.ajaxPost("~/api/utilization/drivingtime/detail", filter);
    }

    /**
  * Get list of tucket appointment.
  * 
  * @param {any} filter
  * @returns
  * 
  * @memberOf WebRequestTruckDriverUtilization
  */
    utilizationexportdetail(filter) {
        return this.ajaxPost("~/api/utilization/drivingtime/export", filter);
    }


      /**
   * Get truck status graph all
   * 
   * @param {any} filter
   * @returns
   * 
   * @memberOf WebRequestTruckDriverUtilization
   */
    truckStatusGraphAll(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/graphAll", filter);
    }

    truckStatusGraphBU(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/graphBusinessUnit", filter);
    }

    truckStatusGraphParkingMoving(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/graphParkingMoving", filter);
    }

    truckStatusGraphParkingDetail(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/graphParkingDetail", filter);
    }
    
    truckStatusGraphMovingDetail(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/graphMovingDetail", filter);
    }

    truckStatusList(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/list", filter);
    }

    truckStatusExportList(filter) {
        return this.ajaxPost("~/api/utilization/truckstatus/exportList", filter);
    }

    truckStatusDetailListt(filter){
        return this.ajaxPost("~/api/utilization/truckstatus/list", filter);    
    }

    driverPerformanceAll(filter){
        return this.ajaxPost("~/api/utilization/performance/all", filter)
    }
    driverPerformanceList(filter){
        return this.ajaxPost("~/api/utilization/performance/list", filter)
    }
    driverPerformanceDetailExport(filter){
        return this.ajaxPost("~/api/utilization/performance/export", filter)
    }

    //Utilization SafetyDashboard
    safetyDashboardSummaryList(filter){
        return this.ajaxPost("~/api/utilization/safetyDashboard/summary/list", filter);    
    }

    //KPI Report 
    kpiReportSummaryList(filter){
        return this.ajaxPost("~/api/kpi/kpiDashboard/summary/list", filter);    
    }
    
    kpiReportBarchartSummaryList(filter){
        return this.ajaxPost("~/api/kpi/barChart/summary/list", filter);    
    }
    

}
export default WebRequestTruckDriverUtilization;