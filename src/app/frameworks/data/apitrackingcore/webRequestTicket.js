﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";
/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestTicket extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDriver.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestTicket",
            new WebRequestTicket(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
   * Get list of tucket appointment.
   * 
   * @param {any} filter
   * @returns
   * 
   * @memberOf WebRequestTicket
   */
    createTicketAppointment(filter){
        return this.ajaxPost("~/api/ticketAppointment", filter);
    }
    updateTicketAppointment(filter){
        return this.ajaxPut("~/api/ticketAppointment", filter);
    }
    updateTicket(filter){
        return this.ajaxPut("~/api/ticket", filter);
    }
    listTicketAppointment(filter) {
        return this.ajaxPost("~/api/ticketAppointment/summary/list", filter);
    }    
    listTicketAppointmentDataGrid(filter) {
        return [this.resolveUrl("~/api/ticketAppointment/summary/list"),filter];
    } 
    listTicketMailManagement(filter) {
        return this.ajaxPost("~/api/ticketMail/summary/list", filter);
    }
    listTicketMailManagementDataGrid(filter) {
        return [this.resolveUrl("~/api/ticketMail/summary/list"),filter];
    } 
    exportAppointmentTicket(filter){
        return this.ajaxPost("~/api/ticketAppointment/export",filter);
    }

    getMailManagement(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/ticketMail/${id}`, includeAssociationNames);
    }

    deleteMailManagement(id){
        return this.ajaxDelete("~/api/ticketMail",[id]);
    }
    updateMailManagement(filter){
        return this.ajaxPut("~/api/ticketMail", filter);
    }
    listTicketSummary(filter){
        return this.ajaxPost("~/api/ticket/summary/list", filter);
    }
    listServiceTypeSummary(){
        return this.ajaxPost("~/api/ticket/summary/list/servicetype");
    }
    listTicketSummaryDataGrid(filter){
        return [this.resolveUrl("~/api/ticket/summary/list"),filter];
        //return [this.resolveUrl("~/api/assetMonitoring/summary/list"), filter]; Example to datagrid
    }
    listTicketMaintenanceType(filter){
        return this.ajaxPost("~/api/ticketAppointment/MaintenanceType/Summary", filter);
    }


    createMailManagement(filter){
        return this.ajaxPost("~/api/ticketMail",filter);
    }

    createTicket(filter){
        return this.ajaxPost("~/api/ticket",filter);
    }

    dashboardTicket(filter){
        return this.ajaxPost("~/api/ticket/dashboard",filter);
    }

    getTicket(id, includeAssociationNames = null){
        return this.ajaxGet(`~/api/ticket/${id}`, includeAssociationNames);
    }
    getLogTicket(id, includeAssociationNames){
        return this.ajaxGet(`~/api/ticket/summary/list/ticketlog/${id}`,includeAssociationNames);
    }
    getLogTicketDataGrid(filter){
        return [this.resolveUrl("~/api/ticket/summary/list/ticketlog"),filter];
        //return this.ajaxGet(`~/api/ticket/summary/list/ticketlog/${id}`,includeAssociationNames);
    }
    getLogTicketMail(id){
        return this.ajaxGet(`~/api/ticketMail/emaillog/${id}`);
    }
    getTicketEmailLog(filter){
        return this.ajaxPost("~/api/ticketMail/summaryemaillog/list",filter);
    }
    getTicketEmailLogDataGrid(filter){
        return [this.resolveUrl("~/api/ticketMail/summaryemaillog/list"),filter];
    }
    exportTicket(filter){
        return this.ajaxPost("~/api/ticket/export",filter);
    }
    //Sim Managenement
    listSimManagement(filter){
        return this.ajaxPost("~/api/ticketSim/summary/list",filter);
    }
    getTicketSimDataGrid(filter){
        return [this.resolveUrl("~/api/ticketSim/summary/list"),filter];
    }
    getSimManagement(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/ticketSim/${id}`, includeAssociationNames);
    }
    deleteSimManagement(id){
        return this.ajaxDelete("~/api/ticketSim",[id]);
    }
    exportsimManagement(filter){
        return this.ajaxPost("~/api/ticketSim/export",filter);
    }

    listSimWithFreetId(filter){
        return this.ajaxPost("~/api/ticketSim/SimFleet/summary",filter);
    }
    createSimManagement(filter){
        return this.ajaxPost("~/api/ticketSim",filter);
    }
    updateSimManagement(filter){
        return this.ajaxPut("~/api/ticketSim",filter);    
    }
    downloadSimTemplate (templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/ticketSim/template/download?templateFileType=${templateFileType}`);
    }
    importSim (info, save = false) {
        return this.ajaxPost(`~/api/ticketSim/import?save=${save}`, info);
     }


    //Pendingcause and Subcause
    getPendingCause(){
        return this.ajaxGet(`~/api/ticket/summary/list/pendingcause`);
    }
    getPendingSubCause(id){
        return this.ajaxGet(`~/api/ticket/summary/list/pendingsubcause?id=${id}`);
    }
    
    getTicketSummary(filter){
        // return this.ajaxPost("~/api/ticket/summary/list",filter);
        return [this.resolveUrl("~/api/ticket/summary/list"),filter];
    }

    updateTicketSummary(filter){
        return [this.resolveUrl("~/api/ticket/grid"),filter];
    }

    listTicketMailSendMail(filter) {
        return this.ajaxPost("~/api/ticketMail/SendMail", filter);
    }

    TicketSend(filter) {
        return this.ajaxPost("~/api/ticketMail/TicketSend", filter);
    }
    
    putTicketSummary(filter){
        return this.ajaxPut("~/api/ticket/grid",filter);  
    }

    deleteTicket(id){
        return this.ajaxDelete("~/api/ticket",[id]);
    }

    getTicketAppointment(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/ticketAppointment/${id}`, includeAssociationNames);
    }

    //CloseCause and SubCause
    closeCause(){
        return this.ajaxGet(`~/api/ticket/summary/list/closeCause`);
    }
    closeSubCause(id){
        return this.ajaxGet(`~/api/ticket/summary/list/closeSubCause?id=${id}`);
    }

}
export default WebRequestTicket;