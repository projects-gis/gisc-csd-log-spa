﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestShipment
 * @extends {WebRequestBase}
 */
class WebRequestShipment extends WebRequestBase {

    /**
     * Creates an instance of WebRequestShipment.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestShipment
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestShipment
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestShipment",
            new WebRequestShipment(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List summary of shipment
     * @param {any} filter
     * @returns
     */
    listShipmentSummary(filter) {
        return this.ajaxPost("~/api/shipment/summary/list", filter);
    }

    /**
     * Export Summary of Shipment
     * @param {any} filter
     * @returns
     */
    exportShipment(filter) {
        return this.ajaxPost("~/api/shipment/exportShipment", filter);
        //return this.ajaxPost("~/api/shipment/shipmentSummary/export", filter);        
    }

    exportShipmentDetail(filter) {
        return this.ajaxPost("~/api/shipment/exportShipmentDetail", filter);
        //return this.ajaxPost("~/api/shipment/shipmentSummary/export", filter);        
    }

    getShipmentDetail(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/summary/waypointList/${id}`, includeAssociationNames);
    }

    /*
    * Autocomplete Service for Search Blade
    */
    autocompleteShipmentCode(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/shipmentCode", filter);
    }

    autocompleteShipmentName(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/shipmentName", filter);
    }

    autocompleteShipmentDOCode(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/doCode", filter);
    }

    autocompleteShipmentDOName(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/doName", filter);
    }

    autocompleteShipmentCreateBy(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/createBy", filter);
    }

    autocompletePONumber(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/PONumber", filter);
    }

    autocompletePOShipmentCode(filter) {
        return this.ajaxPost("~/api/shipment/autocomplete/POShipmentCode", filter);
    }

    getShipmentSummary(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/summary/shipmentSummary/${id}`, includeAssociationNames);
    }

    getExportShipmentSummary(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/exportShipmentSummary/${id}`, includeAssociationNames);
    }

    getDataViewOnMap(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/viewonmap/${id}`, includeAssociationNames);
    }
    
    listPOSummary(filter) {
        return this.ajaxPost("~/api/shipment/summary/POSummary", filter);
    }

    getDashboardToday(filter) {
        return this.ajaxPost("~/api/shipment/Dashboardsummary/todaylist", filter);
    }

    getDashboardHistorical(filter) {
        return this.ajaxPost("~/api/shipment/Dashboardsummary/historicallist", filter);
    }

    listRegionSummary(filter) {
        return this.ajaxPost("~/api/shipment/regionSummary/list", filter);
    }
    getDashboardExport(filter) {
        return this.ajaxPost("~/api/shipment/Dashboardsummary/export", filter);
    }


    deleteShipment(id) {
        return this.ajaxDelete("~/api/shipment", [id]);
    }

    getDefaultValue(filter) {
        return this.ajaxGet("~/api/shipment/GetShipmentDefaultValues", filter);
    }

    updateDefaultValue(filter) {
        return this.ajaxPut("~/api/shipment/ShipmentDefaultValues", filter)
    }
    getShipmentUpdate(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/${id}`, includeAssociationNames);
    }
    postShipmentManageCancel(filter) {
        return this.ajaxPost("~/api/shipment/cancel", filter);
    }


    getShipmentDeliveryOrder(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/summary/doList/${id}`, includeAssociationNames);
    }

    cancelCauseList(filter) {
        return this.ajaxPost('~/api/shipment/cancelcause/list', filter);
    }

    createShipment(filter) {
        return this.ajaxPost('~/api/shipment', filter);
    }

    updateShipment(filter) {
        return this.ajaxPut('~/api/shipment', filter);
    }

    finishCauseList(filter) {
        return this.ajaxPost('~/api/shipment/finishcause/list', filter);
    }

    getIncompleteWaypointShipment(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/summary/wpincompleteList/${id}`, includeAssociationNames);
    }

    downloadImportTemplate(templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/shipment/template/download?templateFileType=${templateFileType}`);
    }

    importShipment(info, save = false) {
        return this.ajaxPost(`~/api/shipment/import?save=${save}`, info);
    }


    downloadImportWithTemplateTemplate(templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/shipment/template/shipmentWithTemplate/download?templateFileType=${templateFileType}`);
    }

    importShipmentWithTemplate(info, save = false) {
        return this.ajaxPost(`~/api/shipment/import/shipmentWithTemplate?save=${save}`, info);
    }

    saveFinishShipment(filter) {
        return this.ajaxPost('~/api/shipment/finish', filter);
    }


    //Shipment Template 
    listShipmentTemplateSummary(filter) {
        return this.ajaxPost("~/api/shipment/shipmentTemplate/summary/list", filter);
    }
    getShipmentTemplateDetail(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipment/shipmentTemplate/listWaypointTemplate/${id}`, includeAssociationNames);
    }
    deleteShipmentTemplate(id){
        return this.ajaxDelete("~/api/shipment/shipmentTemplate", [id]);
        
    }
    updateShipmentTemplate(filter) {
        return this.ajaxPut("~/api/shipment/shipmentTemplate", filter)
    }
    getShipmentTemplate(id){
        return this.ajaxGet(`~/api/shipment/shipmentTemplate/${id}`);
    }
    createShipmentTemplate(filter){
        return this.ajaxPost("~/api/shipment/shipmentTemplate", filter);
    }
    viewOnmap(id){
        return this.ajaxGet(`~/api/shipment/shipmentTemplate/viewonmap//${id}`)
    }

    //Shipment Timeline
    shipmentTimelineSummary(filter){
        return this.ajaxPost("~/api/shipment/timeline", filter)
    }


    //Shipment Category
    createShipmentCategory(filter, returnResult = true) {
        return this.ajaxPost(`~/api/ShipmentCategory?returnResult=${returnResult}`, filter);
    }
    updateShipmentCategory(filter, returnResult = true) {
        return this.ajaxPut(`~/api/ShipmentCategory?returnResult=${returnResult}`, filter);
    }
    deleteShipmentCategory(id) {
        return this.ajaxDelete("~/api/ShipmentCategory", [id]);
    }
    shipmentCategorySummary(filter) {
        return this.ajaxPost("~/api/ShipmentCategory/summary/list", filter);
    }
    getShipmentCategory(id) {
        return this.ajaxGet(`~/api/ShipmentCategory/${id}`);
    }

    autocompleteShipmentTemplate(filter) {
        return this.ajaxPost("~/api/shipment/shipmentTemplate/autoComplete", filter);
    }
    
    findBestSequence(filter) {
        return this.ajaxPost("~/api/shipment/FindBestSequence", filter);
    }

}

export default WebRequestShipment;