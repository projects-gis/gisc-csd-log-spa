import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestDMS
 * @extends {WebRequest}
 */
class WebRequestBOMs extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDMS.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestBOMs
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestBOMs",
            new WebRequestBOMs(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List DMS Summary.
     * 
     */
    // listDMSSummary(filter) {
    //     return this.ajaxPost("~/api/DMS/summary/list", filter);
    // }

    /**
     * List Region.
     * 
     */
    listRegion(){

        return $.post(
            "http://l1.nostralogistics.com/Telematics/TOYOTA/report_api/api/realtimepassenger/region",
            {}
        )
    } 

    getDashboard(filter){

        return $.post(
            "http://l1.nostralogistics.com/Telematics/TOYOTA/report_api/api/realtimepassenger/dashboard",filter
        )
    }

    getVehiclelist (filter){

        return $.post(
            "http://l1.nostralogistics.com/Telematics/TOYOTA/report_api/api/realtimepassenger/vehiclelist",filter
        )
    }

    
    getExport (filter){

        return $.post(
            "http://l1.nostralogistics.com/Telematics/TOYOTA/report_api/api/realtimepassenger/export",filter
        )
    }

    getEmployee (id){

        return $.post(
            "http://l1.nostralogistics.com/Telematics/TOYOTA/report_api/api/realtimepassenger/employeelist?JobId=" + id
        )
    }

    autoComplete (filter){

        return $.post(
            "http://l1.nostralogistics.com/Telematics/TOYOTA/report_api/api/realtimepassenger/Autocomplete", filter
        )
    }
    
}

export default WebRequestBOMs;