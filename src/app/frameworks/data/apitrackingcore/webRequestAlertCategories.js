import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

class WebRequestAlertCategories extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBox.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBox
     * 
     * @static
     * @returns instance of WebRequestBox
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestAlertCategories",
            new WebRequestAlertCategories(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of category  
     * 
     * @param {any} filter
     * @returns
     */
    listAlertCategorySummary(filter) {
        return this.ajaxPost("~/api/alertCategory/summary/list", filter);
    }

    /**
     * Create Alert Category
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf webRequestAlertConfiguration
     */
    createAlertCategory(info, returnResult = true) {
        return this.ajaxPost(`~/api/alertCategory?returnResult=${returnResult}`, info);
    }

    /**
     * Update Alert Category 
     * 
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     * 
     */
    updateAlertCategory(info, returnResult = true) {
        return this.ajaxPut(`~/api/alertCategory?returnResult=${returnResult}`, info);
    }

    /**
     * Get single alert Category information by id.
     * 
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns jQuery Deferred
     */
    getAlertCategory(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/alertCategory/${id}`, includeAssociationNames);
    }

    /**
     * Delete an Alert Category
     * 
     * @param {any} id
     * @returns
     * 
     */
    deleteAlertCategory(id) {
        return this.ajaxDelete("~/api/alertCategory", [id]);
    }

    /*
    * Autocomplete Service for Search Blade
    */

    autocompleteAlertCategory(filter) {
        return this.ajaxPost("~/api/alertCategory/autocomplete/name/", filter);
    }

}

export default WebRequestAlertCategories;