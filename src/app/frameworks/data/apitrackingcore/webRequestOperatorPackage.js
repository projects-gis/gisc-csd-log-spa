import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestOperatorPackage extends WebRequestBase {

    /**
     * Creates an instance of WebRequestOperatorPackage.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf webRequestOperatorPackage
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestOperatorPackage",
            new WebRequestOperatorPackage(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Get list of operator package.
     * 
     * @param {any} filter
     * @returns
     * 
     * @memberOf webRequestOperatorPackage
     */
    listOperatorPackage(filter) {
        return this.ajaxPost("~/api/operatorPackage/list", filter);
    }

      /**
     * Create OperatorPackage. 
     * 
     * @param {any} filter
     * @returns
     */
    createOperatorPackage(info, returnResult = true) {
        return this.ajaxPost(`~/api/operatorPackage?returnResult=${returnResult}`, info);
    }

    /**
     * Get OperatorPackage entity.
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    getOperatorPackage(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/operatorPackage/${id}`, includeAssociationNames);
    }

     /**
     * Update OperatorPackage
     * 
     * @param {any} info
     * @param {boolean} [returnResult=true]
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    updateOperatorPackage (info, returnResult = true) {
        return this.ajaxPut(`~/api/operatorPackage?returnResult=${returnResult}`, info);
    }

     /**
     * Delete OperatorPackage
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestVehicleIcon
     */
    deleteOperatorPackage (id) {
        return this.ajaxDelete("~/api/operatorPackage", [id]);
    }
}

export default WebRequestOperatorPackage;