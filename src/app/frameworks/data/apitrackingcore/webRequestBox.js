import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

class WebRequestBox extends WebRequestBase {

    /**
     * Creates an instance of WebRequestBox.
     * @private
     * @param {any} applicationUrl
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of WebRequestBox
     * 
     * @static
     * @returns instance of WebRequestBox
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebrequestBox",
            new WebRequestBox(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    
    /**
     * Get list of box in summary format 
     * 
     * @param {any} filter
     * @returns
     */
    listBoxSummary(filter) {
        return this.ajaxPost("~/api/box/summary/list", filter);
    }

    /**
     * Get box by box id
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getBox (id, includeAssociationNames) {
        return this.ajaxGet(`~/api/box/${id}`, includeAssociationNames);
    }

    /**
     * Create Box
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createBox (info, returnResult = false) {
        return this.ajaxPost(`~/api/box?returnResult=${returnResult}`, info);
    }
    
    /**
     * Update Box
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateBox (info, returnResult = false) {
        return this.ajaxPut(`~/api/box?returnResult=${returnResult}`, info);
    }

    /**
     * Delete Box
     * @param {any} id
     * @returns
     */
    deleteBox (id) {
        return this.ajaxDelete("~/api/box", [id]);
    }

    
    /**
     * Export Box
     * @param {any} filter
     * @returns
     */
    exportBox (filter) {
        return this.ajaxPost("~/api/box/export", filter);
    }

    /**
     * Download import box template
     * @returns
     */
    downloadBoxTemplate (templateFileType = Enums.TemplateFileType.Xlsx) {
        return this.ajaxGet(`~/api/box/template/download?templateFileType=${templateFileType}`);
    }

    /**
     * Import Box
     * @memberOf WebRequestBox
     */
    importBox (info, save = false) {
        return this.ajaxPost(`~/api/box/import?save=${save}`, info);
    }

    /**
     * Assign Boxes from stock to selected business unit
     * @param {any} info (AssignBoxInputInfo)
     * @returns
     */
    assignBoxes (info) {
        return this.ajaxPost("~/api/box/assign", info);
    }

    /**
     * Return Boxes from business unit to selected stock
     * @param {any} info
     * @returns
     */
    returnBoxes (info) {
        return this.ajaxPost("~/api/box/return", info);
    }
}

export default WebRequestBox;