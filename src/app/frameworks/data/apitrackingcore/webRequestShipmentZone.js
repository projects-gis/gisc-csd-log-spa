﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestShipmentZone
 * @extends {WebRequestBase}
 */
class WebRequestShipmentZone extends WebRequestBase {

    /**
     * Creates an instance of WebRequestShipmentZone.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestShipmentZone
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestShipmentZone
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestShipmentZone",
            new WebRequestShipmentZone(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    listShipmentZone(filter) {
        return this.ajaxPost("~/api/shipmentzone/summary/list", filter);
    }

    /**
     * Get Shipment Zone
     * @param {any} filter
     * @returns
     */
    getShipmentZone(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/shipmentzone/${id}`, includeAssociationNames);
    }

    /**
     * Create Shipment Zone
     * @param {any} filter
     * @returns
     */
    createShipmentZone(filter, returnResult = true) {
        return this.ajaxPost(`~/api/shipmentzone?returnResult=${returnResult}`, filter)
    }

    /**
     * Update Shipment Zone
     * @param {any} filter
     * @returns
     */
    updateShipmentZone(filter, returnResult = true) {
        return this.ajaxPut(`~/api/shipmentzone?returnResult=${returnResult}`, filter)
    }

    /**
     * Delete Shipment Zone
     */
    deleteShipmentZone(id) {
        return this.ajaxDelete("~/api/shipmentzone", [id]);
    }


}

export default WebRequestShipmentZone;