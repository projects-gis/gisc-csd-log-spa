﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";
/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestUser
 * @extends {WebRequest}
 */
class WebRequestTransporter extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDriver.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-webRequestTransporter",
            new WebRequestTransporter(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }
    /**
   * Get list of tucket appointment.
   * 
   * @param {any} filter
   * @returns
   * 
   * @memberOf WebRequestTicket
   */
    listTransporter(filter) {
        return this.ajaxPost("~/api/transporter/summary/list", filter);
    }
    
    createTransporter(filter){
        return this.ajaxPost("~/api/transporter", filter);
    }

    updateTransporter(filter){
        return this.ajaxPut("~/api/transporter",filter);
    }

    getTransporter(id){
        return this.ajaxGet(`~/api/transporter/${id}`);
    }

    deleteTransporter(id){
        return this.ajaxDelete("~/api/transporter",[id]);
    }

    userTransporter(filter){
        return this.ajaxPost("~/api/transporter/UserSummary/list", filter);
    }
    


}
    export default WebRequestTransporter;