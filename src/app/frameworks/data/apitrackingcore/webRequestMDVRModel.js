﻿import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";
import { Enums } from "../../constant/apiConstant";

/**
 * 
 * 
 * @class WebRequestMDVRModel
 * @extends {WebRequestBase}
 */
class WebRequestMDVRModel extends WebRequestBase {

    /**
     * Creates an instance of WebRequestMDVRModel.
     * 
     * @param {any} applicationUrl
     * 
     * @memberOf WebRequestMDVRModel
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * 
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestMDVRModel
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestMDVRModel",
            new WebRequestMDVRModel(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * List summary of MDVRModel
     * @param {any} filter
     * @returns
     */
    listMDVRModelList(filter) {
        return this.ajaxPost("~/api/MDVRModel/summary/list", filter);
    }
    

    /**
     * Get Vehicle
     * @param {any} id
     * @param {any} includeAssociationNames
     * @returns
     */
    getMDVRModel(id, includeAssociationNames) {
        return this.ajaxGet(`~/api/MDVRModel/${id}`, includeAssociationNames);
    }


    /**
     * Create MDVR
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    createMDVRModel(info, returnResult = false) {
        return this.ajaxPost(`~/api/MDVRModel?returnResult=${returnResult}`, info);
    }

    /**
     * Update MDVR
     * @param {any} info
     * @param {boolean} [returnResult=false]
     * @returns
     */
    updateMDVRModel(info, returnResult = false) {
        return this.ajaxPut(`~/api/MDVRModel?returnResult=${returnResult}`, info);
    }

    /**
     * Delete MDVR
     * @param {any} id
     * @returns
     */
    deleteMDVRModel(id) {
        return this.ajaxDelete("~/api/MDVRModel", [id]);
    }


}

export default WebRequestMDVRModel;