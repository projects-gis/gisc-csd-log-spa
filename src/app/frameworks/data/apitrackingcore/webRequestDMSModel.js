import Singleton from "../../core/singleton";
import WebRequestBase from "../webRequestBase";
import WebConfig from "../../configuration/webconfiguration";

/**
 * Facade to access Core Web API in /User 
 * 
 * @class WebRequestADAS
 * @extends {WebRequest}
 */
class WebRequestDMSModel extends WebRequestBase {

    /**
     * Creates an instance of WebRequestDMSModel.
     * @private
     */
    constructor(applicationUrl) {
        super(applicationUrl);
    }

    /**
     * Get instance of web request user.
     * 
     * @static
     * @returns
     * 
     * @memberOf WebRequestDMSModel
     */
    static getInstance() {
        return Singleton.getInstance(
            "trackingCore-WebRequestDMSModel",
            new WebRequestDMSModel(WebConfig.appSettings.apiTrackingCoreUrl)
        );
    }

    /**
     * Create DMSModel
     * 
     * @param {any} filter
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    createDMSModel(filter, returnResult = true) {
        return this.ajaxPost(`~/api/DMSModel?returnResult=${returnResult}`, filter);
    }

    /**
     * Update DMSModel
     * 
     * @param {any} filter
     * @param {boolean} [returnResult=true]
     * @returns jQuery deferred.
     */
    updateDMSModel(filter, returnResult = true) {
        return this.ajaxPut(`~/api/DMSModel?returnResult=${returnResult}`, filter);
    }


    /**
     * Delete an DMSModel 
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestDMSModel
     */
    deleteDMSModel(id) {
        return this.ajaxDelete("~/api/DMSModel", [id]);
    }


    /**
     * List DMSModel Summary.
     * 
     * @memberOf WebRequestDMSModel
     */
    listDMSModelSummary(filter) {
        return this.ajaxPost("~/api/DMSModel/summary/list", filter);
    }


     /**
     * Get DMSModel by id
     * 
     * @param {any} id
     * @returns
     * 
     * @memberOf WebRequestDMSModel
     */
    getDMSModel(id, includeAssociationNames = []) {
        let data = { 
            includeAssociationNames: includeAssociationNames
        };
        return this.ajaxGet(`~/api/DMSModel/${id}`, data);
    }



    
    /**
     * List DMSBrand Summary.
     * 
     * @memberOf WebRequestDMSModel
     */
    getDMSBrand() {
        return this.ajaxPost("~/api/DMSModel/getBrand");
    }

}

export default WebRequestDMSModel;