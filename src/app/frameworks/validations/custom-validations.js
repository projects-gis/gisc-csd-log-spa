import 'jquery';
import ko from 'knockout';
import 'knockout-validation';

/**
 * Ensures a field has the same value as another field (E.g. "Confirm Password" same as "Password"
 * Parameter: otherField: the field to compare to
 * Example
 *
 * viewModel = {
 *   var vm = this;
 *   vm.password = ko.observable();
 *   vm.confirmPassword = ko.observable();
 * }   
 * viewModel.confirmPassword.extend( areSame: { params: viewModel.password, message: "Confirm password must match password" });
 */
ko.validation.rules["areSame"] = {
    getValue: function (o) {
        return (typeof o === "function" ? o() : o);
    },
    validator: function (val, otherField) {
        return val === this.getValue(otherField);
    },
    message: "The fields must have the same value"
};

/**
 * Require validation on ObservableArray
 * Parameter: propertyName of inner object.
 * Example
 *
 * viewModel = {
 *   var vm = this;
 *   vm.collection = ko.observableArray();
 * }   
 * viewModel.collection.extend( arrayIsValid: { params: 'isValid', message: "Collection is not valid." });
 */
ko.validation.rules["arrayIsValid"] = {
    validator: function (val, propertyName) {
        var isValid = true;
        if(!_.isEmpty(propertyName)) {
            // Loop for internal changed.
            val.forEach((item) => {
                var isValidProp = item[propertyName];
                var isValidPropValue;

                if(_.isFunction(isValidProp)) {
                    isValidPropValue = isValidProp();
                }
                else {
                    isValidPropValue = isValidProp;
                }

                // Mark isDirty when evaluated value is false only.
                if(isValidPropValue === false) {
                    isValid = false;
                }
            });
        }

        return isValid; 
    },
    message: "All items must valid."
};

/**
 * Require validation on ObservableArray
 * Parameter: true if required, false otherwise.
 * Example
 *
 * viewModel = {
 *   var vm = this;
 *   vm.collection = ko.observableArray();
 * }   
 * viewModel.collection.extend( arrayRequired: { params: true, message: "Collection cannot be empty" });
 */
ko.validation.rules["arrayRequired"] = {
    validator: function (val, required) {
        if(required) {
            return (val.length > 0);
        }
        return true; 
    },
    message: "This field is required."
};

/**
 * Validate if observable has ip address exceed limit number.
 * Parameter: number of limit ip address.
 * Example
 * 
 * viewModel = {
 *   var vm = this;
 *   vm.ipAddress = ko.observable();
 * }   
 * viewModel.ipAddress.extend({ ipAddressMaximum: 5 });
 */
ko.validation.rules["ipAddressMaximum"] = {
    validator: function (val, maxNo) {
        var stringVal = _.toString(val);
        var stringArray = stringVal.split(',');
        return (stringArray.length <= maxNo);
    },
    message: "Maximum IP Address."
};

/**
 * Fake validation, this is used in conjunction with ko.extenders.serverValidate
 * See example usage in SDK page. 
 */
ko.validation.rules["serverValidate"] = {
    validator: function(val, serverFieldName) {
        return true;
    },
    message: "Server validation fail."
};

/**
 * Fileupload file type limt
 */
ko.validation.rules["fileRequired"] = {
    validator: function(file) {
        return (file && file.name !== "");
    },
    message: "Fileupload is required."
};

/**
 * Fileupload file type limt
 */
ko.validation.rules["fileExtension"] = {
    validator: function(file, extensions = []) {
        var isValid = false;

        if(file && extensions && extensions.length) {
            extensions.forEach((ext)=>{
                // Using lower case comparison.
                if(ext.toLowerCase() === file.extension.toLowerCase()){
                    isValid = true;
                }
            });
        }
        else
        {
            // Allow all files because no limit.
            isValid = true;
        }

        return isValid;
    },
    message: "Unsupported file format."
};

/**
 * Fileupload file size limit.
 */
ko.validation.rules["fileSize"] = {
    validator: function(file, maxFileSizeMb = 4) {
        if (file) {
            var expectedSize = 1024 * (1000 * maxFileSizeMb);
            return (file.size < expectedSize);
        }
        return true;
    },
    message: "The file size cannot be larger than {0} MB."
};

/**
 * Fileupload file image size limit.
 */
ko.validation.rules["fileImageDimension"] = {
    validator: function(file, options = []) {
        var isValid = true;
        if (options.length >= 2) {
            var validationType = "max";
            var specifyWidth = options[0];
            var specifyHeight = options[1];
            if(options.length === 3){
                validationType = options[2];
            }

            switch(validationType) {
                case "equal":
                    isValid = (
                        file.imageWidth == specifyWidth &&
                        file.imageHeight == specifyHeight
                    );
                    break;
                case "min":
                    isValid = (
                        file.imageWidth >= specifyWidth &&
                        file.imageHeight >= specifyHeight
                    );
                    break;
                default: //case "max":
                    isValid = (
                        file.imageWidth <= specifyWidth &&
                        file.imageHeight <= specifyHeight
                    );
                    break;
            }
        }

        return isValid;
    },
    message: "The uploaded image exceeds the maximum dimensions allowed."
};

/**
 * Limit array length.
 */
ko.validation.rules["maxArrayLength"] = {
    validator: function (val, maxLength) {
        if(maxLength) {
            return (val.length <= maxLength);
        }
        return true; 
    },
    message: "This field is not valid."
};

/**
 * Limit array length. for PlaybackAnalysis
 */
ko.validation.rules["maxArrayPlaybackAnalysisLength"] = {
    validator: function (val, maxLength) {
        if(maxLength) {
            return (val.length <= maxLength);
        }
        return true; 
    },
    message: "This field is not valid."
};

ko.validation.registerExtenders();