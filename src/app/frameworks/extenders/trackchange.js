import ko from 'knockout';
import gc from '../core/gc';
import Utility from '../core/utility';

ko.extenders.trackChange = function(target, track) {
    if (track) {
        target.isDirty = ko.observable(false);

        var typeGuardValue = function(rawValue){
            if((rawValue === undefined) || (rawValue === "")) {
                return null;
            }
            return rawValue;
        };
        target.originalValueHash = Utility.getHashCode(ko.toJSON(typeGuardValue(target())));
        
        // Using subscriber to detect new value changes.
        var subscriber = target.subscribe(function(newValue) {
            // use != not !== so numbers will equate naturally.
            var newValueHash = Utility.getHashCode(ko.toJSON(typeGuardValue(newValue)));
            target.isDirty(newValueHash !== target.originalValueHash);
        });
        
        // Cleanup subscribe because we don't have any VM parent.
        // We need to add extra disposable
        gc.attachDisposable(target, subscriber);
    }
    return target;
};

ko.extenders.trackArrayChange = function(target, track) {
     if (track) {
        var uniqueFromPropertyName = null;
        var stationaryObject = null;
        if(_.isObjectLike(track)) {
            uniqueFromPropertyName = _.isNil(track.uniqueFromPropertyName) ? null : track.uniqueFromPropertyName;
            stationaryObject = _.isNil(track.stationaryObject) ? null : track.stationaryObject;
        }

        // Original value must be a clone, not reference
        target.originalValue = _.cloneDeep(target());

        if(stationaryObject) {
            target.originalValueHash = Utility.getHashCode(ko.toJSON(target.originalValue));
        }

        target.isDirty = () => {
            let originalSet = null;
            let currentSet = null;

            if(!_.isNull(uniqueFromPropertyName)) {
                // Compare shallow by property name
                originalSet = target.originalValue.map((o) => {
                    return o[uniqueFromPropertyName];
                });
                currentSet = target().map((o) => {
                    return o[uniqueFromPropertyName];
                });

                originalSet.sort();
                currentSet.sort();

                return !_.isEqual(originalSet, currentSet);
            } else if(!_.isNull(stationaryObject)) {
                // Compare stationary, ignore order
                currentSet = target();
                var currentValueHash = Utility.getHashCode(ko.toJSON(currentSet));

                return target.originalValueHash !== currentValueHash;
            } else {
                // Compare deep complex object
                originalSet = target.originalValue.sort();
                currentSet = target().sort();

                return !_.isEqual(originalSet, currentSet);
            }
        };
    }
    return target;
};

ko.extenders.trackChangeByProperty = function(target, track) {
    if (track) {
        var propertyName = !_.isObjectLike(track) ? "isDirty" : track.propertyName; 
        target.isDirty = () => {
            var rawData =  target();
            var dirty = false;
            rawData.forEach((data)=>{
                if(data[propertyName]){
                    var itemDirty = false;
                    if(_.isFunction(data[propertyName])) {
                        itemDirty = data[propertyName]();
                    } else {
                        itemDirty = data[propertyName];
                    }
                    if(itemDirty) {
                        dirty = true;
                    }
                }
            });
            return dirty;
       };
    }
    return target;
};