import { Constants } from "../constant/apiConstant";
class UserSession {
    constructor() {
        this._userSession = window.userSession;
        this._companyLogoUrl = null;
        this._companyName = null;
        this._userPermissions = [];
        this._prefUserLanguage = null;
        this._prefEnableSoundAlert = false;
        this._prefFleetDelayTime = 0;
        this._prefEnableAlertNotification = false;
        this._perfAssetIdsForFleetMonitoring = null;
        this._accessibleFirstCompanyId = 0;
        this._accessibleCompanyCount = 0;
        this._accessibleBusinessUnits = [];
        this._i18n = null;
        this._portalModes = [];
        this._isSysAdmin = false;
        this._authenticationType = 0;
        this._currentDataGridState = [];
        this._companyThemeName = null;
        this._distanceUnit = null;
        this._distanceUnitSymbol = null;
        this._areaUnit = null;
        this._areaUnitSymbol = null;
    }

    // Static loader from startup page.
    get id () {
        return this._userSession.id;
    }
    get loginToken() {
        return this._userSession.loginToken;
    }
    get userType () {
        return this._userSession.userType;
    }
    get currentPortal() {
        return this._userSession.currentPortal;
    }
    get currentCompanyId (){
        return this._userSession.currentCompanyId;
    }
    get fullname (){
        return this._userSession.fullname;
    }
    get remainingExpirationDays() {
        return this._userSession.remainingExpirationDays;
    }
    get accessibleCompanyCount () {
        return this._accessibleCompanyCount;
    }
    set accessibleCompanyCount (value) {
        this._accessibleCompanyCount = value;
    }
    get accessibleFirstCompanyId () {
        return this._accessibleFirstCompanyId;
    }
    set accessibleFirstCompanyId (value) {
        this._accessibleFirstCompanyId = value;
    }
    get currentCompanyName (){
        return this._companyName;
    }
    set currentCompanyName (value){
        this._companyName = value;
    }
    get permissionType() {
        return this._userSession.permissionType;
    }
    set permissionType(value) {
        this._userSession.permissionType = value;
    }

    // Runtime loader based on portal environment.
    get currentCompanyLogoUrl () {
        return this._companyLogoUrl;
    }
    set currentCompanyLogoUrl (value) {
        this._companyLogoUrl = value;
    }

    // User preference which loaded from shell.
    get currentUserLanguage () {
        return this._prefUserLanguage;
    }
    set currentUserLanguage (value) {
        this._prefUserLanguage = value;
    }
    get enableSoundAlert () {
        return this._prefEnableSoundAlert;
    }
    set enableSoundAlert (value) {
        this._prefEnableSoundAlert = value;
    }
    get fleetDelayTime () {
        return this._prefFleetDelayTime;
    }
    set fleetDelayTime (value) {
        this._prefFleetDelayTime = value;
    }
    get enableAlertNotification () {
        return this._prefEnableAlertNotification;
    }
    set enableAlertNotification (value) {
        this._prefEnableAlertNotification = value;
    }
    get assetIdsForFleetMonitoring() {
        return this._perfAssetIdsForFleetMonitoring;
    }
    set assetIdsForFleetMonitoring(value) {
        this._perfAssetIdsForFleetMonitoring = value;
    }
    get i18n () {
        return this._i18n;
    }
    set i18n (value) {
        this._i18n = value;
    }
    get portalModes() {
        return this._portalModes;
    }
    set portalModes(value) {
        this._portalModes = value;
    }
    get isSysAdmin() {
        return this._isSysAdmin;
    }
    set isSysAdmin(value) {
        this._isSysAdmin = value;
    }
    get authenticationType() {
        return this._authenticationType;
    }
    set authenticationType(value) {
        this._authenticationType = value;
    }
    get currentDataGridState () {
        return this._currentDataGridState;
    }
    set currentDataGridState (value) {
        this._currentDataGridState = value;
    }

    get companyThemeName () {
        return this._companyThemeName;
    }
    set companyThemeName(value) {
        this._companyThemeName = value;
    }

    get distanceUnit () {
        return this._distanceUnit;
    }
    set distanceUnit (value) {
        this._distanceUnit = value;
    }
    get distanceUnitSymbol () {
        return this._distanceUnitSymbol;
    }
    set distanceUnitSymbol (value) {
        this._distanceUnitSymbol = value;
    }


    get areaUnit () {
        return this._areaUnit;
    }
    set areaUnit (value) {
        this._areaUnit = value;
    }
    get areaUnitSymbol () {
        return this._areaUnitSymbol;
    }
    set areaUnitSymbol (value) {
        this._areaUnitSymbol = value;
    }

    /**
     * Verify current user has proper permission.
     * 
     * @param {string or array} requiredPermissions
     * @returns
     * 
     * @memberOf UserSession
     */
    hasPermission (requiredPermissions) {
        // Check requirePermissions is array.
        if(!(requiredPermissions instanceof Array)){
            // Auto create array with one member then process interaction.
            requiredPermissions = [requiredPermissions];
        }
        else {
            // If Array is empty then return true.
            if(!requiredPermissions.length) {
                return true;
            }
        }

        // Intersection for user permission and required permission.
        var matchPermissions = this._userPermissions.filter(function(n) {
            return requiredPermissions.indexOf(n) != -1;
        });

        return matchPermissions.length;
    }

    hasPermissionNotificationMaintenances() {
        return this.hasPermission(Constants.Permission.Asset) && this.hasPermission(Constants.Permission.Alert_Maintenance);
    }

    hasPermissionNotificationUrgentAlerts() {
        return this.hasPermission(Constants.Permission.UserManagement_Company) && this.hasPermission(Constants.Permission.Alert_Urgent);
    }

    hasPermissionNotificationAlerts() {
        return (this.hasPermission(Constants.Permission.UserManagement_Company) || this.hasPermission(Constants.Permission.Asset)) && this.hasPermission([
            Constants.Permission.Alert_AccRapid,
            Constants.Permission.Alert_AcquiredGPS,
            Constants.Permission.Alert_BadGPS,
            Constants.Permission.Alert_VehicleBatteryLow,
            Constants.Permission.Alert_DeviceBatteryLow,
            Constants.Permission.Alert_DecRapid,
            Constants.Permission.Alert_EngineIdle,
            Constants.Permission.Alert_EngineOff,
            Constants.Permission.Alert_EngineOn,
            Constants.Permission.Alert_GateClose,
            Constants.Permission.Alert_GateOpen,
            Constants.Permission.Alert_GPSDisCnt,
            Constants.Permission.Alert_GPSReCnt,
            Constants.Permission.Alert_Move,
            Constants.Permission.Alert_OverSpeed,
            Constants.Permission.Alert_OverTemperature,
            Constants.Permission.Alert_PowerDisCnt,
            Constants.Permission.Alert_PowerReCn,
            Constants.Permission.Alert_Stop,
            Constants.Permission.Alert_Swerve,
            Constants.Permission.Alert_UnderTemperature,
            Constants.Permission.Alert_UnsFuelDec,
            Constants.Permission.Alert_CloseGateInsPOI,
            Constants.Permission.Alert_CloseGateOtsPOI,
            Constants.Permission.Alert_JobAvgSpeed,
            Constants.Permission.Alert_JobTotDist,
            Constants.Permission.Alert_JobTotTime,
            Constants.Permission.Alert_OpenGateInsPOI,
            Constants.Permission.Alert_OpenGateOtsPOI,
            Constants.Permission.Alert_OverTimePark,
            Constants.Permission.Alert_IncmWP,
            Constants.Permission.Alert_OutgWP,
            Constants.Permission.Alert_SOS,
            Constants.Permission.Alert_Parking,
            Constants.Permission.Alert_POIParking,
            Constants.Permission.Alert_POIInOut,
            Constants.Permission.Alert_AreaParking,
            Constants.Permission.Alert_AreaInOutVehicle,
            Constants.Permission.Alert_AreaInOutJob,
            Constants.Permission.Alert_MoveNonWorking,
            Constants.Permission.Alert_ExceedLimitDriving,
            Constants.Permission.Alert_CustomAlert,
            Constants.Permission.Alert_Dispatcher,
            Constants.Permission.Alert_ExceedLimitDrivingIn24Hours
        ]);
    }

    get permissions () {
        return this._userPermissions;
    }

    /**
     * Restore user permissions on first load.
     * 
     * @param {any} permissions
     */
    initPermission (permissions) {
        this._userPermissions = permissions;
    }

    get accessibleBusinessUnits () {
        return this._accessibleBusinessUnits;
    }

    initAccessibleBusinessUnits (value) {
        this._accessibleBusinessUnits = value;
    }
}

export default UserSession;