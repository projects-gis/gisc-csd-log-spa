import ko from "knockout";
import DefaultSorting from "../constant/defaultSorting";
import Utility from "../core/utility";
import MapManager from "../../../components/controls/gisc-chrome/map/mapManager";
import { Enums } from "../constant/apiConstant";
import AssetInfo from "../../../components/portals/company-workspace/asset-info";
import EventAggregator from "../core/eventAggregator";
import * as EventAggregatorConstant from "../constant/eventAggregator";

class FleetMonitoring {
    constructor() {
        //Private member
        this._eventAggregator = new EventAggregator();

        //Track
        this.companyId = null;
        this.trackLocationLastSync = ko.observable();
        this.trackLocationItems = ko.observableArray([]);//for dataTable
        this.trackLocationVehicleIcons = ko.observableArray([]);
        this.trackLocationPoiIcons = ko.observableArray([]);
        this.trackVehicles = ko.observableArray([]);//for map track-vehicles
        this.removeTrackVehicles = ko.observableArray([]);//for map remove-track-vehicles
        this.mapStartupComplete = ko.observable(false);
        this.trackLocationViewVehicleIds = ko.observableArray([]);
        this.trackLocationViewDeliveryManIds = ko.observableArray([]);
        this.trackLocationFollowVehicleIds = ko.observableArray([]);
        this.trackLocationFollowDeliveryManIds = ko.observableArray([]);
        this.trackableIdPrefixVehicle = "V-";
        this.trackableIdPrefixDeliveryMan = "D-";
        this.fleetDelayTimeDatetimeReference = ko.observable();
        this.trackLocationFilter = ko.observable({});

        //Alert
        this.alertDashboardItems = ko.observableArray([]);
        this.alertDashboardLastSync = ko.observable();
        this.alertDashboardLastSyncDatetime = ko.observable();
    }

    /**
     * Handle update tracking location from signalr signal.
     * 
     * @param {any} webRequestFleetMonitoring
     * @param {Any} userSession
     * @param {any} [vehicleIds=[]]
     * @param {any} [deliveryManIds=[]]
     */
    signalTrackLocation(webRequestFleetMonitoring, userSession, vehicleIds = [], deliveryManIds = []) {
        var isUpdate = false;//for check if update Id is asset in track location items

        var effectiveVehicleIds = [];
        var effectiveDeliveryManIds = [];

        if (vehicleIds && vehicleIds.length) {
            effectiveVehicleIds = _.intersection(this.trackLocationViewVehicleIds(), vehicleIds);
        }
        if (deliveryManIds && deliveryManIds.length) {
            effectiveDeliveryManIds = _.intersection(this.trackLocationViewDeliveryManIds(), deliveryManIds);
        }

        if (effectiveVehicleIds.length || effectiveDeliveryManIds.length) {
            return this._findTrackLocations(webRequestFleetMonitoring, effectiveVehicleIds, effectiveDeliveryManIds, this.trackLocationFilter()).done((result) => {
                this._updateTrackLocationItems(result, userSession);
            });
        }
        else {
            return Utility.emptyDeferred();
        }
    }

    // Need to send WebConfig.userSession.fleetDelayTime because we cannot import WebConfig here
    syncTrackLocation(webRequestFleetMonitoring, userSession, filter = {}) {
        this.trackLocationFilter(filter);
        return this._findTrackLocations(webRequestFleetMonitoring, this.trackLocationViewVehicleIds(), this.trackLocationViewDeliveryManIds(), this.trackLocationFilter()).done((result) => {
            var vehicleIcons = result.vehicleIcons;
            var poiIcons = result.poiIcons;
            var items = [];
            var trackVehicles = [];
            var trackVehicleIds = [];
            _.forEach(result.items, (trackLocation) => {
                /**
                 * isView is always true for all items in datasource
                 * isOverFleetDelayTime is compare between trackLoc dateTime and reference dateTime
                 */
                trackLocation.isView = true;
                trackLocation.isFollow = trackLocation.vehicleId ? _.indexOf(this.trackLocationFollowVehicleIds(), trackLocation.vehicleId) > -1 : _.indexOf(this.trackLocationFollowDeliveryManIds(), trackLocation.deliveryManId) > -1;
                trackLocation.isOverFleetDelayTime = this.trackLocationIsOverFleetDelayTime(trackLocation.dateTime, userSession.fleetDelayTime);
                trackLocation.removeIcon = '../images/icon-trackmonitoring-remove.svg';
                items.push(trackLocation);

                var trackVehicle = this._getTrackVehicleItem(trackLocation, vehicleIcons, userSession);
                trackVehicles.push(trackVehicle);
                trackVehicleIds.push(trackVehicle.id);//trackableId
            });

            // find item which in old track list but not in new track list to be remove from map
            var removeTrackVehicles = [];
            _.forEach(this.trackVehicles(), (o) => {
                if (_.indexOf(trackVehicleIds, o.id) === -1) {
                    removeTrackVehicles.push(o.id);
                }
            });

            this.trackLocationPoiIcons(poiIcons);
            this.trackLocationVehicleIcons(vehicleIcons);
            this.trackLocationItems(items);
            this.trackLocationLastSync(result.formatTimestamp);
            this.trackVehicles(trackVehicles);
            this.removeTrackVehicles(removeTrackVehicles);

            //this.trackLocationItems.valueHasMutated(); // Comment this line prevent double notify.

            //for check if map startup complete, call map from shell.js does not work since map does not complete yet
            if (this.mapStartupComplete()) {
                this.trackVehiclesOnMap();
            }
        });
    }

    /**
     * Clear track monitoring
     */
    clearTrackMonitoring() {
        var removeTrackVehicles = [];
        _.forEach(this.trackVehicles(), (o) => {
            removeTrackVehicles.push(o.attributes.ID);
        });

        if (removeTrackVehicles.length) {
            MapManager.getInstance().removeTrackVehicles(this.id, removeTrackVehicles);
        }

        this.trackLocationItems([]);
        this.trackLocationVehicleIcons([]);
        this.trackLocationPoiIcons([]);
        this.trackVehicles([]);
        this.removeTrackVehicles([]);
        this.trackLocationViewVehicleIds([]);
        this.trackLocationViewDeliveryManIds([]);
        this.trackLocationFollowVehicleIds([]);
        this.trackLocationFollowDeliveryManIds([]);
        this.trackLocationItems.valueHasMutated();
    }

    /**
     * Publish event aggregator for saving tracking state.
     */
    triggerSaveState() {
        var state = {
            viewVehicleIds: this.trackLocationViewVehicleIds().slice(),
            viewDeliveryManIds: this.trackLocationViewDeliveryManIds().slice(),
            followVehicleIds: this.trackLocationFollowVehicleIds().slice(),
            followDeliveryManIds: this.trackLocationFollowDeliveryManIds().slice()
        };

        this._eventAggregator.publish(EventAggregatorConstant.ASSETIDS_FOR_FLEET_CHANGE, state);
    }

    /**
     * 
     * @param {any} vehicleIds
     * @param {any} deliveryManIds
     */
    updateTrackLocationAssetIds(webRequestFleetMonitoring, userSession, viewVehicleIds = [], viewDeliveryManIds = [], followVehicleIds = [], followDeliveryManIds = []) {
        var currentViewVehicleIds = this.trackLocationViewVehicleIds();
        var currentViewDeliveryManIds = this.trackLocationViewDeliveryManIds();
        var currentFollowVehicleIds = this.trackLocationFollowVehicleIds();
        var currentFollowDeliveryManIds = this.trackLocationFollowDeliveryManIds();

        var addViewVehicleIds = _.filter(viewVehicleIds, (id) => {
            return _.indexOf(currentViewVehicleIds, id) === -1;
        });

        var removeViewVehicleIds = _.filter(currentViewVehicleIds, (id) => {
            return _.indexOf(viewVehicleIds, id) === -1;
        });

        var addViewDeliveryManIds = _.filter(viewDeliveryManIds, (id) => {
            return _.indexOf(currentViewDeliveryManIds, id) === -1;
        });

        var removeViewDeliveryManIds = _.filter(currentViewDeliveryManIds, (id) => {
            return _.indexOf(viewDeliveryManIds, id) === -1;
        });

        var addFollowVehicleIds = _.filter(followVehicleIds, (id) => {
            return _.indexOf(currentFollowVehicleIds, id) === -1;
        });

        var removeFollowVehicleIds = _.filter(currentFollowVehicleIds, (id) => {
            return _.indexOf(followVehicleIds, id) === -1;
        });

        var addFollowDeliveryManIds = _.filter(followDeliveryManIds, (id) => {
            return _.indexOf(currentFollowDeliveryManIds, id) === -1;
        });

        var removeFollowDeliveryManIds = _.filter(currentFollowDeliveryManIds, (id) => {
            return _.indexOf(followDeliveryManIds, id) === -1;
        });

        var trackLocationItems = this.trackLocationItems();
        var trackVehicles = this.trackVehicles();
        var removeTrackVehicles = [];

        _.forEach(removeViewVehicleIds, (id) => {
            var index = _.findIndex(trackLocationItems, (item) => {
                return item.vehicleId === id;
            });
            trackLocationItems.splice(index, 1);

            var indexTrackVehicle = _.findIndex(trackVehicles, (item) => {
                return item.id === this.trackableIdPrefixVehicle + id;
            });
            trackVehicles.splice(indexTrackVehicle, 1);
            removeTrackVehicles.push(this.trackableIdPrefixVehicle + id);
        });

        _.forEach(removeViewDeliveryManIds, (id) => {
            var index = _.findIndex(trackLocationItems, (item) => {
                return item.deliveryManId === id;
            });
            trackLocationItems.splice(index, 1);

            var indexTrackVehicle = _.findIndex(trackVehicles, (item) => {
                return item.id === this.trackableIdPrefixDeliveryMan + id;
            });
            trackVehicles.splice(indexTrackVehicle, 1);
            removeTrackVehicles.push(this.trackableIdPrefixDeliveryMan + id);
        });

        _.forEach(addFollowVehicleIds, (id) => {
            var index = _.findIndex(trackLocationItems, (item) => {
                return item.vehicleId === id;
            });

            if (index > -1) {
                trackLocationItems[index].isFollow = true;
            }

            var indexTrackVehicle = _.findIndex(trackVehicles, (item) => {
                return item.id === this.trackableIdPrefixVehicle + id;
            });

            if (indexTrackVehicle > -1) {
                trackVehicles[indexTrackVehicle].track = true;
            }
        });

        _.forEach(removeFollowVehicleIds, (id) => {
            var index = _.findIndex(trackLocationItems, (item) => {
                return item.vehicleId === id;
            });
            
            //check if item is still exist, because it may be removed from removeViewVehicleIds
            if (index > -1) {
                trackLocationItems[index].isFollow = false;
            }

            var indexTrackVehicle = _.findIndex(trackVehicles, (item) => {
                return item.id === this.trackableIdPrefixVehicle + id;
            });
            
            if (indexTrackVehicle > -1) {
                trackVehicles[indexTrackVehicle].track = false;
            }
        });

        _.forEach(addFollowDeliveryManIds, (id) => {
            var index = _.findIndex(trackLocationItems, (item) => {
                return item.deliveryManId === id;
            });

            if (index > -1) {
                trackLocationItems[index].isFollow = true;
            }

            var indexTrackVehicle = _.findIndex(trackVehicles, (item) => {
                return item.id === this.trackableIdPrefixDeliveryMan + id;
            });

            if (indexTrackVehicle > -1) {
                trackVehicles[indexTrackVehicle].track = true;
            }
        });

        _.forEach(removeFollowDeliveryManIds, (id) => {
            var index = _.findIndex(trackLocationItems, (item) => {
                return item.deliveryManId === id;
            });
            
            //check if item is still exist, because it may be removed from removeViewDeliveryManIds
            if (index > -1) {
                trackLocationItems[index].isFollow = false;
            }

            var indexTrackVehicle = _.findIndex(trackVehicles, (item) => {
                return item.id === this.trackableIdPrefixDeliveryMan + id;
            });
            
            if (indexTrackVehicle > -1) {
                trackVehicles[indexTrackVehicle].track = false;
            }
        });

        //update assetIds
        this.trackLocationViewVehicleIds(viewVehicleIds);
        this.trackLocationViewDeliveryManIds(viewDeliveryManIds);
        this.trackLocationFollowVehicleIds(followVehicleIds);
        this.trackLocationFollowDeliveryManIds(followDeliveryManIds);
        this.trackLocationItems.poke(trackLocationItems);
        this.trackVehicles(trackVehicles);
        this.removeTrackVehicles(removeTrackVehicles);

        //trigger update.
        this.triggerSaveState();


        //console.log("BBBBBBBBBBB");

        if (addViewVehicleIds.length === 0 && addViewDeliveryManIds.length === 0) {
            //trigger changed trackLocationItems
            this.trackLocationItems.valueHasMutated();

            //if no new asset added, no need to call service. just update track vehicle on maps.
            if (this.mapStartupComplete()) {
                this.trackVehiclesOnMap();
                this.resetFilterVehicleOnMap();
            }



            return Utility.emptyDeferred();
        }
        else {

            this.resetFilterVehicleOnMap();

            //if no vehicleIds and no deliveryManIds, also call service to get lastSync updated 
            var filter = (viewVehicleIds.length === 0 && viewDeliveryManIds.length === 0) ? { displayLength: 0 } : {};
            return this._findTrackLocations(webRequestFleetMonitoring, addViewVehicleIds, addViewDeliveryManIds, filter).done((result) => {
                this._updateTrackLocationItems(result, userSession);
            });
        }
    }

    /**
     * call web request to find track location list
     * @param {any} webRequestFleetMonitoring
     * @param {any} [viewVehicleIds=[]]
     * @param {any} [viewDeliveryManIds=[]]
     * @param {any} [filter={}]
     * @returns
     */
    _findTrackLocations(webRequestFleetMonitoring, viewVehicleIds = [], viewDeliveryManIds = [], filter = {}) {
        var webRequestFilter = $.extend(true, {}, {
            companyId: this.companyId,
            vehicleIds: viewVehicleIds,
            deliveryManIds: viewDeliveryManIds,
            sortingColumns: DefaultSorting.TrackLocation
        }, filter);

        // prevent system from get all tracklocations when user remove all assets.
        // also check displayLength to make sure this is not first load.
        if (viewVehicleIds.length == 0 && viewDeliveryManIds.length == 0 && filter.displayLength == null) {
            webRequestFilter.displayLength = 0;
        }

        return webRequestFleetMonitoring.listTrackLocation(webRequestFilter).fail((e) => {
            if (e && e.responseJSON && e.responseJSON.code === "M062") {
                this.clearTrackMonitoring();
            }
        });
    }

    
    /**
     * update track location for specific items
     * @param {any} result
     */
    _updateTrackLocationItems(result, userSession) {
        if (result.items.length > 0) {
            //update POI Icons
            var trackLocationPoiIcons = this.trackLocationPoiIcons();
            _.forEach(result.poiIcons, (value, key) => {
                trackLocationPoiIcons[key] = value;
            });
            this.trackLocationPoiIcons(trackLocationPoiIcons);

            //update Vehicle Icons
            var trackLocationVehicleIcons = this.trackLocationVehicleIcons();
            _.forEach(result.vehicleIcons, (value, key) => {
                trackLocationVehicleIcons[key] = value;
            });
            this.trackLocationVehicleIcons(trackLocationVehicleIcons);

            //update trackLocationItems, lastSync, trackVehicles
            var trackLocationItems = this.trackLocationItems();
            var trackVehicles = this.trackVehicles();
            var followVehicleIds = this.trackLocationFollowVehicleIds();
            var followDeliveryManIds = this.trackLocationFollowDeliveryManIds();

            _.forEach(result.items, (trackLocation) => {
                trackLocation.isView = true;
                trackLocation.isOverFleetDelayTime = this.trackLocationIsOverFleetDelayTime(trackLocation.dateTime, userSession.fleetDelayTime);
                trackLocation.removeIcon = '../images/icon-trackmonitoring-remove.svg';

                var index = -1;
                var indexTrackVehicle = -1;

                if (trackLocation.vehicleId) {
                    trackLocation.isFollow = _.indexOf(followVehicleIds, trackLocation.vehicleId) > -1;

                    index = _.findIndex(trackLocationItems, (o) => {
                        return o.vehicleId === trackLocation.vehicleId;
                    });

                    indexTrackVehicle = _.findIndex(trackVehicles, (o) => {
                        return o.id === this.trackableIdPrefixVehicle + trackLocation.vehicleId;
                    });
                }
                else if (trackLocation.deliveryManId) {
                    trackLocation.isFollow = _.indexOf(followDeliveryManIds, trackLocation.deliveryManId) > -1;

                    index = _.findIndex(trackLocationItems, (o) => {
                        return o.deliveryManId === trackLocation.deliveryManId;
                    });

                    indexTrackVehicle = _.findIndex(trackVehicles, (o) => {
                        return o.id === this.trackableIdPrefixDeliveryMan + trackLocation.deliveryManId;
                    });
                }

                if (index > -1) {
                    trackLocationItems.splice(index, 1, trackLocation);
                }
                else {
                    trackLocationItems.push(trackLocation);
                }

                if (indexTrackVehicle > -1) {
                    trackVehicles.splice(indexTrackVehicle, 1, this._getTrackVehicleItem(trackLocation, result.vehicleIcons, userSession));
                }
                else {
                    trackVehicles.push(this._getTrackVehicleItem(trackLocation, result.vehicleIcons, userSession));
                }
            });

            this.trackLocationItems(trackLocationItems);
            //this.trackLocationItems.valueHasMutated(); // This line cause double notify so i comment this.

            this.trackVehicles(trackVehicles);
            if (this.mapStartupComplete()) {
                this.trackVehiclesOnMap();
            }
        }

        //always updated lastSync
        this.trackLocationLastSync(result.formatTimestamp);
    }

    /**
     * get direction image list for delivery man
     * 
     * @param {any} movementType
     * @returns
     * 
     */
    _getDeliveryManMovementDirectionImageList(movementType) {
        var movement = "";
        switch (movementType) {
            case Enums.ModelData.MovementType.Move:
                movement = "move";
                break;
            case Enums.ModelData.MovementType.Stop:
                movement = "stop";
                break;
            case Enums.ModelData.MovementType.Park:
                movement = "park";
                break;
        }

        var imageUrl = Utility.resolveUrl("/", "~/images/icon-walk-" + movement + "-direction-");

        return {
            N: imageUrl + "n.png",
            E: imageUrl + "e.png",
            W: imageUrl + "w.png",
            S: imageUrl + "s.png",
            NE: imageUrl + "ne.png",
            NW: imageUrl + "nw.png",
            SE: imageUrl + "se.png",
            SW: imageUrl + "sw.png"
        };
    }

    
    /**
     * get direction image list for vehicle
     * @param {any} vehicleIcon
     * @returns
     */
    _getVehicleMovementDirectionImageList(vehicleIcon) {
        return {
            N: vehicleIcon.imageUrl + "/n.png",
            E: vehicleIcon.imageUrl + "/e.png",
            W: vehicleIcon.imageUrl + "/w.png",
            S: vehicleIcon.imageUrl + "/s.png",
            NE: vehicleIcon.imageUrl + "/ne.png",
            NW: vehicleIcon.imageUrl + "/nw.png",
            SE: vehicleIcon.imageUrl + "/se.png",
            SW: vehicleIcon.imageUrl + "/sw.png"
        };
    }

    /**
     * Get TrackVehicleItem for Map
     * 
     * @param {any} trackLocation
     * @param {any} vehicleIcons
     * @param {any} userSession
     * @returns
     */
    _getTrackVehicleItem(trackLocation, vehicleIcons, userSession,showDuration = true) {
        var trackableId = trackLocation.vehicleId ? this.trackableIdPrefixVehicle + trackLocation.vehicleId : this.trackableIdPrefixDeliveryMan + trackLocation.deliveryManId;
        var pic = null;
        if (trackLocation.vehicleId) {
            var vehicleIcon = _.find(vehicleIcons[trackLocation.vehicleIconId], (o) => {
                return o.movementType === trackLocation.movement;
            });

            pic = this._getVehicleMovementDirectionImageList(vehicleIcon);
        }
        else if (trackLocation.deliveryManId) {
            pic = this._getDeliveryManMovementDirectionImageList(trackLocation.movement);
        }

        var status = AssetInfo.getStatuses(trackLocation, userSession);

        var retItem = {
            id: trackableId,
            title: trackLocation.vehicleId ? trackLocation.vehicleLicense : trackLocation.username,
            track: trackLocation.isFollow,
            location: {
                lat: trackLocation.latitude,
                lon: trackLocation.longitude
            },
            pic: pic,
            attributes: {
                ID: trackableId,
                TITLE: trackLocation.vehicleId ? trackLocation.vehicleLicense : trackLocation.username,
                BOX_ID: trackLocation.vehicleId ? trackLocation.boxSerialNo : trackLocation.email,
                DRIVER_NAME: trackLocation.driverName,
                DEPT: trackLocation.businessUnitPath,
                DATE: trackLocation.formatDateTime,
                PARK_TIME: trackLocation.formatParkDurationCustom,//trackLocation.parkDuration,
                PARK_IDLE_TIME: trackLocation.formatIdleTimeCustom,//trackLocation.idleTime,
                LOCATION: trackLocation.location,
                STATUS: status,
                RAW: trackLocation,
                MOVEMENT: trackLocation.movement
            }
        };

        retItem.attributes.SHIPMENTINFO = AssetInfo.getShipmentInfo(trackLocation, retItem.attributes.STATUS,showDuration);

        return retItem;
    }

    /**
     * Call map to track/remove vehicles
     * 
     */
    trackVehiclesOnMap() {
        if (this.trackVehicles().length > 0) {
            MapManager.getInstance().trackVehicles(this.id, this.trackVehicles());
        }
        if (this.removeTrackVehicles().length > 0) {
            MapManager.getInstance().removeTrackVehicles(this.id, this.removeTrackVehicles());
            this.removeTrackVehicles([]);
        }

        //this.fakeTrackVehiclesOnMap();
    }

    resetFilterVehicleOnMap() {
        MapManager.getInstance().resetFilterVehicleOnMap();
    }

    
    /**
     * 
     * Fake data to track vehicles on map
     * 
     * @memberOf FleetMonitoring
     */
    fakeTrackVehiclesOnMap() {
        var trackLocationTimer;
        clearInterval(trackLocationTimer);
        trackLocationTimer = setInterval(() => {
            var trackVehicles = [];
            _.forEach(this.trackVehicles(), (o) => {
                o.location.lat = o.location.lat + getRandomArbitrary(-0.009, 0.009);
                o.location.lon = o.location.lon + getRandomArbitrary(-0.009, 0.009);
                trackVehicles.push(o);
            });

            this.trackVehicles(trackVehicles);
            MapManager.getInstance().trackVehicles(this.id, this.trackVehicles());
        }, 10000);

        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }
    }


    /**
     * Compare fleet delay time with track location items
     * Set flag isOverFleetDelayTime
     * @param {any} fleetDelayTime
     */
    compareFleetDelayTime(fleetDelayTime) {
        _.forEach(this.trackLocationItems(), (item) => {
            item.isOverFleetDelayTime = this.trackLocationIsOverFleetDelayTime(item.dateTime, fleetDelayTime);
        }); 
        //trigger change
        this.trackLocationItems.valueHasMutated();
    }

    
    /**
     * Calculate track location datetime is over fleet delay time
     * @param {any} datetime
     * @param {any} fleetDelayTime
     * @returns
     */
    trackLocationIsOverFleetDelayTime(datetime, fleetDelayTime) {
        return !_.isNil(fleetDelayTime) && fleetDelayTime > 0 && Utility.compareDateTimeAsMinutes(datetime, this.fleetDelayTimeDatetimeReference()) > fleetDelayTime;
    }

    
    /**
     * Sync alert dashboard data
     * @param {any} webRequestAlert
     * @returns
     */
    syncAlertDashboard(webRequestAlert, keyword="") {
        return webRequestAlert.getAlertDashboard(this.companyId, keyword).done((response) => {
            this.alertDashboardLastSync(response.formatTimestamp);
            this.alertDashboardLastSyncDatetime(response.timestamp);
            this.alertDashboardItems(response.items);
        });
    }
}

export default FleetMonitoring;