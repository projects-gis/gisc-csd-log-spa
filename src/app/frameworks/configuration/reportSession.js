class ReportSession {
    constructor() {
        this._reportSession = window.reportSession;
    }

    // Static loader from startup page.
    get userID () {
        return this._reportSession.userID;
    }

    get reportUrl () {
        return this._reportSession.reportUrl;
    }


}

export default ReportSession;