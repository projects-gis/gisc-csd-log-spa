﻿import 'jquery';
import Singleton from './singleton';
import UIConstants from '../constant/uiConstant';

class Toastr {

    constructor() {
        this.options = {};
        this.version = "2.1.3";

        this.toastId = 0;
        this.$container = null;
        this.listener = null;
    }
    
    static getInstance() {
        return Singleton.getInstance("toastObject", new Toastr());
    }

    getContainer(options, create) {
        if (!options) { options = getOptions(); }
        this.$container = $('#' + options.containerId);
        if (this.$container.length) {
            return this.$container;
        }
        if (create) {
            this.$container = this.createContainer(options);
        }
        return this.$container;
    }
    
    error(message, title, optionsOverride) {
        return this.notify({
            type: UIConstants.ToastrType.Error,
            iconClass: this.getOptions().iconClasses.error,
            message: message,
            optionsOverride: optionsOverride,
            title: title
        });
    }

    info(message, title, optionsOverride) {
        return this.notify({
            type: UIConstants.ToastrType.Info,
            iconClass: this.getOptions().iconClasses.info,
            message: message,
            optionsOverride: optionsOverride,
            title: title
        });
    }

    success(message, title, optionsOverride) {
        return this.notify({
            type: UIConstants.ToastrType.Success,
            iconClass: this.getOptions().iconClasses.success,
            message: message,
            optionsOverride: optionsOverride,
            title: title
        });
    }

    warning(message, title, optionsOverride) {
        return this.notify({
            type: UIConstants.ToastrType.Warning,
            iconClass: this.getOptions().iconClasses.warning,
            message: message,
            optionsOverride: optionsOverride,
            title: title
        });
    }
    
    subscribe(callback) {
        this.listener = callback;
    }

    clear($toastElement, clearOptions) {
        var options = this.getOptions();
        if (!this.$container) { this.getContainer(options); }
        if (!this.clearToast($toastElement, options, clearOptions)) {
            this.clearContainer(options);
        }
    }

    remove($toastElement) {
        var options = this.getOptions();
        if (!this.$container) { this.getContainer(options); }
        if ($toastElement && $(':focus', $toastElement).length === 0) {
            this.removeToast($toastElement);
            return;
        }
        if (this.$container.children().length) {
            this.$container.remove();
        }
    }

    // internal functions

    getDefaults() {
        return {
            hideIconClass: "toast-hide-default-icon",
            tapToDismiss: true,
            toastClass: 'toast',
            containerId: 'toast-container',
            debug: false,

            showMethod: 'fadeIn', //fadeIn, slideDown, and show are built into jQuery
            showDuration: 300,
            showEasing: 'swing', //swing and linear are built into jQuery
            onShown: undefined,
            hideMethod: 'fadeOut',
            hideDuration: 1000,
            hideEasing: 'swing',
            onHidden: undefined,
            closeMethod: false,
            closeDuration: false,
            closeEasing: false,
            closeOnHover: true,

            extendedTimeOut: 1000,
            iconClasses: {
                error: 'toast-error',
                info: 'toast-info',
                success: 'toast-success',
                warning: 'toast-warning'
            },
            iconClass: 'toast-info',
            positionClass: 'toast-top-right',
            timeOut: 1500, // Set timeOut and extendedTimeOut to 0 to make it sticky
            titleClass: 'toast-title',
            messageClass: 'toast-message',
            escapeHtml: false,
            target: 'body',
            closeHtml: '<button type="button">&times;</button>',
            closeClass: 'toast-close-button',
            newestOnTop: true,
            preventDuplicates: false,
            progressBar: false,
            progressClass: 'toast-progress',
            rtl: false
        };
    }


    clearContainer (options) {
        var toastsToClear = this.$container.children();
        for (var i = toastsToClear.length - 1; i >= 0; i--) {
            this.clearToast($(toastsToClear[i]), options);
        }
    }

    clearToast ($toastElement, options, clearOptions) {
        var force = clearOptions && clearOptions.force ? clearOptions.force : false;
        if ($toastElement && (force || $(':focus', $toastElement).length === 0)) {
            $toastElement[options.hideMethod]({
                duration: options.hideDuration,
                easing: options.hideEasing,
                complete: function () { this.removeToast($toastElement); }
            });
            return true;
        }
        return false;
    }

    createContainer(options) {
        this.$container = $('<div/>')
            .attr('id', options.containerId)
            .addClass(options.positionClass);

        this.$container.appendTo($(options.target));
        return this.$container;
    }

    publish(args) {
        if (!this.listener) { return; }
        this.listener(args);
    }

    notify(map) {
        let self = this;
        let options = this.getOptions();
        let iconClass = map.iconClass || options.iconClass;

        if (typeof (map.optionsOverride) !== 'undefined') {
            options = $.extend(options, map.optionsOverride);
            iconClass = map.optionsOverride.iconClass || iconClass;
        }

        if (shouldExit(options, map)) { return; }

        this.toastId++;

        this.$container = this.getContainer(options, true);

        let intervalId = null;
        let $toastElement = $('<div/>');
        let $titleElement = $('<div/>');
        let $messageElement = $('<div/>');
        let $progressElement = $('<div/>');
        let $iconElement = $('<div class="toast-custom-icon"/>');
        let $closeElement = $(options.closeHtml);
        let progressBar = {
            intervalId: null,
            hideEta: null,
            maxHideTime: null
        };
        let response = {
            toastId: this.toastId,
            state: 'visible',
            startTime: new Date(),
            options: options,
            map: map
        };

        personalizeToast();

        displayToast();

        handleEvents();

        this.publish(response);

        if (options.debug && console) {
            console.log(response);
        }

        return $toastElement;

        function escapeHtml(source) {
            if (source == null) {
                source = '';
            }

            return source
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
        }

        function personalizeToast() {
            setIcon();
            setTitle();
            setMessage();
            setCloseButton();
            setProgressBar();
            setRTL();
            setSequence();
            setAria();
        }

        function setAria() {
            var ariaValue = '';
            switch (map.iconClass) {
                case 'toast-success':
                case 'toast-info':
                    ariaValue =  'polite';
                    break;
                default:
                    ariaValue = 'assertive';
            }
            $toastElement.attr('aria-live', ariaValue);
        }

        function handleEvents() {
            if (options.closeOnHover) {
                $toastElement.hover(stickAround, delayedHideToast);
            }

            if (!options.onclick && options.tapToDismiss) {
                $toastElement.click(hideToast);
            }

            if (options.closeButton && $closeElement) {
                $closeElement.click(function (event) {
                    if (event.stopPropagation) {
                        event.stopPropagation();
                    } else if (event.cancelBubble !== undefined && event.cancelBubble !== true) {
                        event.cancelBubble = true;
                    }

                    if (options.onCloseClick) {
                        options.onCloseClick(event);
                    }

                    hideToast(true);
                });
            }

            if (options.onclick) {
                $toastElement.click(function (event) {
                    options.onclick(event);
                    hideToast();
                });
            }
        }

        function displayToast() {
            $toastElement.hide();

            $toastElement[options.showMethod](
                {duration: options.showDuration, easing: options.showEasing, complete: options.onShown}
            );

            if (options.timeOut > 0) {
                intervalId = setTimeout(hideToast, options.timeOut);
                progressBar.maxHideTime = parseFloat(options.timeOut);
                progressBar.hideEta = new Date().getTime() + progressBar.maxHideTime;
                if (options.progressBar) {
                    progressBar.intervalId = setInterval(updateProgress, 10);
                }
            }
        }

        function setIcon() {

            if(options.customIcon) {
                $iconElement.append($(options.customIcon));
                $toastElement.addClass(options.toastClass).addClass(`${iconClass}-custom`);
                $toastElement.append($iconElement);
            }
            else if(map.iconClass) {
                $toastElement.addClass(options.toastClass).addClass(iconClass);
            }
        }

        function setSequence() {
            if (options.newestOnTop) {
                self.$container.prepend($toastElement);
            } else {
                self.$container.append($toastElement);
            }
        }

        function setTitle() {
            if (map.title) {
                var suffix = map.title;
                if (options.escapeHtml) {
                    suffix = escapeHtml(map.title);
                }
                $titleElement.append(suffix).addClass(options.titleClass);
                $toastElement.append($titleElement);
            }
        }

        function setMessage() {
            if (map.message) {
                var suffix = map.message;
                if (options.escapeHtml) {
                    suffix = escapeHtml(map.message);
                }
                $messageElement.append(suffix).addClass(options.messageClass);
                $toastElement.append($messageElement);
            }
        }

        function setCloseButton() {
            if (options.closeButton) {
                $closeElement.addClass(options.closeClass).attr('role', 'button');
                $toastElement.prepend($closeElement);
            }
        }

        function setProgressBar() {
            if (options.progressBar) {
                $progressElement.addClass(options.progressClass);
                $toastElement.prepend($progressElement);
            }
        }

        function setRTL() {
            if (options.rtl) {
                $toastElement.addClass('rtl');
            }
        }

        function shouldExit(options, map) {
            if (options.preventDuplicates) {
                if (map.message === self.previousToast) {
                    return true;
                } else {
                    selft.previousToast = map.message;
                }
            }
            return false;
        }

        function hideToast(override) {
            var method = override && options.closeMethod !== false ? options.closeMethod : options.hideMethod;
            var duration = override && options.closeDuration !== false ?
                options.closeDuration : options.hideDuration;
            var easing = override && options.closeEasing !== false ? options.closeEasing : options.hideEasing;
            if ($(':focus', $toastElement).length && !override) {
                return;
            }
            clearTimeout(progressBar.intervalId);
            return $toastElement[method]({
                duration: duration,
                easing: easing,
                complete: function () {
                    self.removeToast($toastElement);
                    clearTimeout(intervalId);
                    if (options.onHidden && response.state !== 'hidden') {
                        options.onHidden();
                    }
                    response.state = 'hidden';
                    response.endTime = new Date();
                    self.publish(response);
                }
            });
        }

        function delayedHideToast() {
            if (options.timeOut > 0 || options.extendedTimeOut > 0) {
                intervalId = setTimeout(hideToast, options.extendedTimeOut);
                progressBar.maxHideTime = parseFloat(options.extendedTimeOut);
                progressBar.hideEta = new Date().getTime() + progressBar.maxHideTime;
            }
        }

        function stickAround() {
            clearTimeout(intervalId);
            progressBar.hideEta = 0;
            $toastElement.stop(true, true)[options.showMethod](
                {duration: options.showDuration, easing: options.showEasing}
            );
        }

        function updateProgress() {
            var percentage = ((progressBar.hideEta - (new Date().getTime())) / progressBar.maxHideTime) * 100;
            $progressElement.width(percentage + '%');
        }
    }

    getOptions() {
        return $.extend({}, this.getDefaults(), this.options);
    }

    removeToast($toastElement) {
        if (!this.$container) { this.$container = this.getContainer(); }
        if ($toastElement.is(':visible')) {
            return;
        }
        $toastElement.remove();
        $toastElement = null;
        if (this.$container.children().length === 0) {
            this.$container.remove();
            this.previousToast = undefined;
        }
    }
}

export default Toastr.getInstance();