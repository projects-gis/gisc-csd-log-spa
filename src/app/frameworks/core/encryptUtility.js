﻿import ko from "knockout";
import _ from "lodash";
import Singleton from "./singleton";
import WebRequestBase from "../data/webRequestBase";
import WebConfig from "../../../app/frameworks/configuration/webConfiguration";
import "jsencrypt";

class EncryptUtility extends WebRequestBase {
    constructor() {
        super();

    }
    static getInstance() {
        return Singleton.getInstance("encryptUtility", new EncryptUtility());
    }

    encryption(user,pass,ip) {
        var encrypt = new JSEncrypt();
        encrypt.setKey(WebConfig.appSettings.encryptKey)
        return encrypt.encrypt(user + "|" + pass + "|" + ip);
    }
    decryption(key) {
        var decrypt = new JSEncrypt();
        decrypt.setPrivateKey(WebConfig.appSettings.decryptKey);
        return decrypt.decrypt(key);
    }
}

export default EncryptUtility;