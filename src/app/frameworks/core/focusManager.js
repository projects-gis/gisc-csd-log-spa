import Singleton from "./singleton";

class FocusManager {
    constructor() {
        
        // this.managedElements = [];
        this.managedElementMap = new Map();
    }

    registerFocusElement(elementKey, domElement, onLostFocus) {
        // this.managedElements.push(domElement);
        this.managedElementMap.set(elementKey, { 
            domElement: domElement,
            onLostFocus: onLostFocus 
        });

        // this.managedElementMap.set(elementKey, "aaa");
    }

    // getManagedElements() {
    //     // return this.managedElements;
    // }

    handleActiveElementChanged(domActiveElement) {
        // console.log("handleActiveElementChanged");
        this.managedElementMap.forEach((item) => {
            // console.log("item.domElement",item.domElement);
            // console.log("domActiveElement",domActiveElement);
            // console.log("contains = ", $.contains(item.domElement ,domActiveElement));
            if(!$.contains(item.domElement ,domActiveElement)) {
                if(_.isFunction(item.onLostFocus)) {
                    item.onLostFocus(domActiveElement);
                }
            }
        });
    }

    /**
     * Get instance of VisualEffect 
     * 
     * @public
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("focusManager", new FocusManager());
    }
}

export default FocusManager;