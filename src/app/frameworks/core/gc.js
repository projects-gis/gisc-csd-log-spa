import ko from 'knockout';

/**
 * Garbage Collector for subscribe event management.
 */
class GC {
    static attachDisposable(target, disposable) {
        // Attach GC disposable collection to target.
        if (!target.gcDisposables) {
            // Create temporary array.
            target.gcDisposables = [];
        }
        // Add disposable item to array.
        target.gcDisposables.push(disposable);
    }
    static dispose(target, includeProp = true) {
        // Always clear disposable.
        if (target && target.gcDisposables && target.gcDisposables.length) {
            ko.utils.arrayForEach(target.gcDisposables, GC.disposeOne);
            // Remove temporary array.
            delete target.gcDisposables;
        }
        
        if (!includeProp) {
            return;
        }

        // Unsubscription for every property if implement disposable.
        ko.utils.objectForEach(target, (propOrValue, value) => {
            var prop = value || propOrValue;

            // Available auto cleanup for non-purecomputed only.
            if (ko.isObservable(prop) && !ko.isPureComputed(prop)) {
                if (prop.indexOf !== undefined) {
                    // If current prop is observableArray then iterate for cleanup.
                    ko.utils.arrayForEach(prop(), (arrayItem) => {
                        GC.dispose(arrayItem, true);
                    });
                } else {
                    // Else invoke directly.
                    GC.dispose(prop, false);
                }
            }
            GC.disposeOne(propOrValue, value);
        });
    }
    static disposeOne(propOrValue, value) {
        var disposable = value || propOrValue;

        // Invoke dispose function if exits.
        if (disposable && typeof disposable.dispose === "function") {
            // console.log("Disposing", propOrValue, "...");
            disposable.dispose();
        }
    }
}

export default GC;