import Singleton from "./singleton";
import Logger from "./logger";

/**
 * Browser geolocation wrapper class.
 */
class GeoLocation {
    constructor(){
        this.latitude = null;
        this.longitude = null;
        this.lastSync = null;
        this.gpsTimeout = 6000;
        this.maximumAge = 60000;
        this.useAccuracy = false;
    }

    /**
     * Get geolocation instance (singleton).
     * @static
     * @returns GeoLocation
     */
    static getInstance() {
        return Singleton.getInstance("geolocation", new GeoLocation());
    }

    /**
     * Get current location from browser.
     * @return jQeury Deferred
     */
    getCurrentPosition()
    {
        var dfdLocationResolution = $.Deferred();

        // Invoke default browser navigator service.
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(
                (position) => {
                    // Populate position to model.
                    this.latitude = position.coords.latitude;
                    this.longitude = position.coords.longitude;
                    this.lastSync = new Date();

                    // Return location to caller.
                    dfdLocationResolution.resolve(this.latitude, this.longitude);
                },
                (error) => {
                    // For IE only first request is success 
                    // We fix this issue by using previous attempt.
                    if(this.latitude && this.longitude){
                        // Use cached version.
                        Logger.warn("Use cached location since", this.lastSync);
                        dfdLocationResolution.resolve(this.latitude, this.longitude);
                    }
                    else {
                        // Always error.
                        Logger.warn("geolocation error with", error);
                        dfdLocationResolution.reject();
                    }
                },
                {
                    maximumAge: this.maximumAge,
                    timeout: this.gpsTimeout,
                    enableHighAccuracy: this.useAccuracy
                }
            );
        } else {
            Logger.warn("Browser doesn't support geo location");
            dfdLocationResolution.reject();
        }

        return dfdLocationResolution;
    }
}

export default GeoLocation;