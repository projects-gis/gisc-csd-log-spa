import ko from 'knockout';
import Utility from './utility';
import Logger from './logger';

/**
 * Base class which handle tracking change. Derive this class if you have
 * custom data structure that need to support trackChange extender.
 * 
 * @class ObjectBase
 */
class ObjectBase {
    
    /**
     * Creates an instance of ObjectBase.
     */
    constructor() {
        this.id = Utility.getUniqueId();
        this.createDate = new Date();
    }
    
    /**
     * Check if object is dirty, used in conjunction with trackChange extender.
     * 
     * @returns true if object is consider dirty (state changed, 
     * state not consistent with previous, etc...) false otherwise.
     */
    isDirty() {
        var isItemDirty = false;

        for (let key in this) {
            

                var property = this[key];

                
                if (this.hasOwnProperty(key)){
                    if (!_.isNil(property)){
                        if (typeof property.isDirty === 'function' && key != 'navigationService' && key != 'widgetInstance' && key != 'widgetService' && key!= 'widgetParams'){
                            if (property.isDirty()){
                                isItemDirty = true;
                            }
                        }
                    }
                }
                // if (this.hasOwnProperty(key) &&
                //     !_.isNil(property) &&
                //     typeof property.isDirty === 'function' &&
                //     property.isDirty()) {
                //         console.log("AAAAAAaa");
                //     //Logger.info(key, "is dirty =", true);
                //     //return true;
                // }
            // }

        }

        return isItemDirty;
    }

    /**
     * Return JSON object for current object (extract Observable).
     */
    toJSON() {
        return ko.toJS(this);
    }

    /**
     * Return JSON string for current object.
     */
    toString() {
        return ko.toJSON(this);
    }
}

export default ObjectBase;