import WebConfig from "../configuration/webconfiguration";

class Logger {
    static log() {
        if (WebConfig.appSettings.isDebug && (WebConfig.appSettings.logLevel.indexOf("log") >= 0)) {
            console.log.apply(console, arguments);
        }
    }
    static info() {
        if (WebConfig.appSettings.isDebug && (WebConfig.appSettings.logLevel.indexOf("info") >= 0)) {
            console.info.apply(console, arguments);
        }
    }
    static warn() {
        if (WebConfig.appSettings.isDebug && (WebConfig.appSettings.logLevel.indexOf("warn") >= 0)) {
            console.warn.apply(console, arguments);
        }
    }
    static error() {
        if (WebConfig.appSettings.isDebug && (WebConfig.appSettings.logLevel.indexOf("error") >= 0)) {
            console.error.apply(console, arguments);
        }
    }
}

export default Logger;