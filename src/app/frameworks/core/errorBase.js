/**
 * Base class for all error object
 * 
 * @public
 * @class ErrorBase
 */
class ErrorBase {
    constructor(message, id = 0) {
        this.id = this._padErrorId(id, 4);
        this.message = message;
        this.stackTrace = (new Error()).stack;
    }

    _padErrorId(num, size) {
        return ('000000000' + num).substr(-size);
    }
}

export default ErrorBase;