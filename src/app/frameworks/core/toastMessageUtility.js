﻿import toastr from "./toastr";
import Singleton from "./singleton";
import EventAggregator from "./eventAggregator";
import * as EventAggregatorConstant from "../constant/eventAggregator";


class ToastMessageUtility {
    constructor() {

        this.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "1500",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        this._eventAggregator = new EventAggregator();
    }

    static getInstance() {
        return Singleton.getInstance("toastMessageUtility", new ToastMessageUtility());
    }

    showToast(type, message, icon, data) {
        toastr.options = this.options;
        toastr.options.customIcon = icon;
        toastr.options.onclick = ()=> {
            this.publishMessage(EventAggregatorConstant.TOASTR_CLICK, data)
        }
        toastr[type](message);
    }

    publishMessage(messageId, messageValue) {
        this._eventAggregator.publish(messageId, messageValue);
    }
}

export default ToastMessageUtility;