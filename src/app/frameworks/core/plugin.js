import "jquery";
import ko from "knockout";

// subscribeArrayChanged is mainly used by gisc-ui-datatable
ko.observableArray.fn.subscribeArrayChanged = function(addCallback, deleteCallback, afterCallback) {
    if(this.alreadySubscribeArrayChanged) {
        this.subscribeBeforChangeRef.dispose();
        this.subscribeArrayChangeRef.dispose();
    }

    var previousValue = undefined;

    this.subscribeBeforChangeRef = this.subscribe(function(_previousValue) {
        previousValue = _previousValue.slice(0);
    }, undefined, 'beforeChange');

    this.subscribeArrayChangeRef = this.subscribe(function(latestValue) {
        var editScript = ko.utils.compareArrays(previousValue, latestValue);
        var deletedItems = [];
        var addedItems = [];

        for (var i = 0, j = editScript.length; i < j; i++) {
            switch (editScript[i].status) {
                case "retained":
                    break;
                case "deleted":
                    deletedItems.push(editScript[i].value);
                    break;
                case "added":
                    addedItems.push(editScript[i].value);
                    break;
            }
        }

        if(deleteCallback) {
            deleteCallback(deletedItems);
        }

        if(addCallback) {
            addCallback(addedItems);
        }

        if(afterCallback) {
            afterCallback(latestValue);
        }

        previousValue = undefined;
    });

    // Avoid multiple registrations on the same observable array
    this.alreadySubscribeArrayChanged = true;
};

ko.observableArray.fn.reset = function(newArray) {
    var observableArray = this;
    observableArray.removeAll();
    newArray.forEach(function(element) {
        observableArray.push(element);
    }, this);
};

ko.observableArray.fn.merge = function(newArray) {
    var observableArray = this;
    newArray.forEach(function(element) {
        observableArray.push(element);
    }, this);
};

ko.observableArray.fn.updateOrAdd = function(updatedItem, primaryKey) {
    var observableArray = this;
    var targetItem = null;
    var targetIndex = -1;
    observableArray().forEach(function(element, index) {
        if (element[primaryKey] === updatedItem[primaryKey]) {
            targetItem = element;
            targetIndex = index;
        }
    }, this);

    if (targetItem) {
        // Using observableArray.replace  will cause ko.observableArray.fn.subscribeArrayChanged
        // to fire added before deleted, this is troublesome because it try to added then delete the same element
        // therefore it has to fire in order, delete first then add.
        observableArray.remove(targetItem);
        observableArray.splice(targetIndex, 0, updatedItem);
    } else {
        observableArray.push(updatedItem);
    }
};

ko.observableArray.fn.replaceAll = function(newArray) {
    var observableArray = this;
    var newArrayCopy = $.extend(true, [], newArray);
    observableArray.removeAll();
    ko.utils.arrayPushAll(observableArray, newArrayCopy);
};

/*
    Allow silent update on observable
    Usage:
    this.name = ko.observable();
    this.name.ignorePokeSubscribe((newValue) => { ... }); // this is subscribe WHICH ignore poke

    When set value like this:
    this.name("someone");

    It will go to subscribe normally, however if poke:
    this.name.poke("sheeesh");

    It will silent update the value
*/
ko.observable.fn.ignorePokeSubscribe = function(callback, thisValue, event) {
    var self = this;
    this.subscribe(function(newValue) {
        if(!self.paused) {
            callback(newValue);
        }
    }, thisValue, event);
    return this;
};
ko.observable.fn.poke = function(newValue) {
    this.paused = true;
    var result = this(newValue);
    this.paused = undefined;
    return result;
};