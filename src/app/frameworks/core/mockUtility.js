import ko from "knockout";

/**
 * Static class which provide utility method for mocking data
 * Note: Please keep this class as static class
 * 
 * @class MockUtility
 */
class MockUtility {
    
    /**
     * Create entity and put in datasourceArray. Key is the id property of item.
     * id MUST unique and be Number.
     * 
     * @static
     * @public
     * @param {array} datasourceArray
     * @param {any} item
     * @param {string} [key="id"]
     */
    static createEntity(datasourceArray, item, key = "id") {
        var newId = datasourceArray.length + 1;
        item[key] = newId;
        datasourceArray.push(item);
    }

    /**
     * Update entity in datasourceArray. Key is the id property of item.
     * id MUST unique and be Number.
     * 
     * @static
     * @public
     * @param {array} datasourceArray
     * @param {any} item
     * @param {string} [key="id"]
     */
    static updateEntity(datasourceArray, item, key = "id") {
        let foundIndex = -1;
        for(let i = 0; i < datasourceArray.length; i++) {
            var target = datasourceArray[i];
            if(target[key] === item[key]) {
                foundIndex = i;
                break;
            }
        }
        if(foundIndex >= 0) {
            datasourceArray.splice(foundIndex, 1, item);
        } else {
            throw new Error("Cannot find object to update");
        }
    }

    /**
     * Delete entity in datasourceArray. keyValue is the value of id property of item.
     * id MUST unique and be Number.
     * 
     * @static
     * @public
     * @param {any} datasourceArray
     * @param {any} keyValue
     * @param {string} [key="id"]
     */
    static deleteEntity(datasourceArray, keyValue, key = "id") {
        let foundIndex = -1;
        for(let i = 0; i < datasourceArray.length; i++) {
            var target = datasourceArray[i];
            if(target[key] === keyValue) {
                foundIndex = i;
                break;
            }
        }
        if(foundIndex >= 0) {
            datasourceArray.splice(foundIndex, 1);
        } else {
            throw new Error("Cannot find object to delete");
        }
    }
    
    /**
     * Find target entity in datasourceArray. keyValue is the value of id property of item.
     * id MUST unique and be Number.
     * 
     * @static
     * @public
     * @param {any} datasourceArray
     * @param {any} keyValue
     * @param {string} [key="id"]
     * @returns
     */
    static findEntity(datasourceArray, keyValue, key = "id") {
        let foundIndex = -1;
        let foundItem = null;
        for(let i = 0; i < datasourceArray.length; i++) {
            var target = datasourceArray[i];
            if(target[key] === keyValue) {
                foundIndex = i;
                break;
            }
        }
        if(foundIndex >= 0) {
            foundItem = datasourceArray[foundIndex];
        }
        return foundItem;
    }


    /**
     * WARNING: this method give side-effect to objectWithId parameters
     * 
     * @static
     * @param {any} seedLength
     * @param {any} objectWithId
     * @param {string} [idProperty="id"]
     * @returns new seedLength
     */
    static ensureIdMaxPositiveInteger(seedLength, objectWithId, idProperty = "id") {
        if(objectWithId[idProperty] === undefined) return;

        if((objectWithId[idProperty] === null) || (objectWithId[idProperty] === -1)) {
            objectWithId[idProperty] = ++seedLength;
        }
        return seedLength;
    }
}

export default MockUtility;