import ko from "knockout";
import "knockout-postbox";

/**
 * Engine for PubSub 
 * 
 * @class EventAggregator
 */
class EventAggregator {

    /**
     * Creates an instance of EventAggregator.
     */
    constructor() {
        this.disposableObjects = [];
    }

    /**
     * Subscribe to eventTopicKey, when there is publish on eventTopicKey callback will be called.
     * 
     * @public
     * @param {string} eventTopicKey
     * @returns {Promise} Deferred Object
     */
    subscribe(eventTopicKey, callback) {
        this.disposableObjects.push(ko.postbox.subscribe(eventTopicKey, callback));
    }
    
    /**
     * Publish on eventTopicKey, all subscriber will recieve Promise.done
     * 
     * @public
     * @param {string} eventTopicKey
     * @param {any} topicValue
     */
    publish(eventTopicKey, topicValue) {
        ko.postbox.publish(eventTopicKey, topicValue);
    }
    
    /**
     * Destroy the EventAggregator and all its holding reference.
     */
    destroy() {
        this.disposableObjects.forEach(function(element) {
            element.dispose();
        }, this);
        this.disposableObjects = [];
    }
}

export default EventAggregator;