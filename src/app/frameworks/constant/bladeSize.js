export const XSmall = 0;
export const Small = 1;
export const Medium = 2;
export const XMedium = 2.5;
export const Large = 3;
export const XLarge = 4;
export const XLarge_A0 = 5;
export const XLarge_A1 = 6;
export const XLarge_A2 = 7;
//full screen only report
export const XLarge_Report = 8;