export const DROPMENU_CLOSE = "app-dropmenu-hidden";
export const DROPMENU_OPEN = "app-dropmenu-is-open";
export const SELECTED ="app-is-selected";

// Blade UI Stylesheet references.
export const APP_SIDEBAR_COLLAPSED = "app-sidebar-is-collapsed";
export const APP_BLADE_SIZE_XSMALL = "app-bladesize-xsmall";
export const APP_BLADE_SIZE_SMALL = "app-bladesize-small";
export const APP_BLADE_SIZE_MEDIUM = "app-bladesize-medium";
export const APP_BLADE_SIZE_XMEDIUM = "app-bladesize-xmedium";
export const APP_BLADE_SIZE_LARGE = "app-bladesize-large";
export const APP_BLADE_SIZE_XLARGE = "app-bladesize-xlarge";
export const APP_BLADE_SIZE_XLARGE_A0 = "app-bladesize-xlarge-a0";
export const APP_BLADE_SIZE_XLARGE_A1 = "app-bladesize-xlarge-a1";
export const APP_BLADE_SIZE_XLARGE_A2 = "app-bladesize-xlarge-a2";
export const APP_BLADE_MINIMIZED = "app-blade-minimized";
export const APP_BLADE_MAXIMIZED = "app-blade-maximized";
export const APP_PANNING_CURSOR = "app-panning-cursor";
export const APP_BLADE_MARGIN_NARROW = "app-blade-margin-narrow";
export const APP_BLADE_LAYOUT_STATIC = "app-blade-layout-static";
export const APP_BLADE_TYPE_COMPACT = "app-blade-type-compact";
export const APP_BLADE_SIZE_XLARGE_REPORT = "app-bladesize-xlarge-report";

