// Gisc.Csd.Log.Domain.Common.Constant.Constants (completed)
export class Constants {
    static get RegularExpression () {
        return class RegularExpression {
            static get PasswordRule() { return "(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!\"#$%&'\\[\\](){}\\\\*+,./:;<=>?@^_`|~-]).{8,}";}
        };
    }

    static get LoggerName () {
        return class LoggerName {
            static get ServiceLog() { return "ServiceLog";}
            static get PerformanceLog() { return "PerformanceLog";}
            static get DatabaseLog() { return "DatabaseLog";}
            static get WebserviceLog() { return "WebserviceLog";}
        };
    }

    static get SystemConfigurationName () {
        return class SystemConfigurationName {
            static get DefaultRadiusFindNearest() { return "DefaultRadiusFindNearest";}
            static get DefaultTimeRangeFindAsset() { return "DefaultTimeRangeFindAsset";}
            static get DefaultTimeRangeFindNearest() { return "DefaultTimeRangeFindNearest";}
            static get NoSignalTimeout() { return "NoSignalTimeout";}
            static get GoogleAnalyticsAccount() { return "GoogleAnalyticsAccount";}
            static get MaxReportDateRange() { return "MaxReportDateRange";}
            static get FleetMonitoringUIRefreshRate() { return "FleetMonitoringUIRefreshRate";}
            static get ExpireUserRemindDays() { return "ExpireUserRemindDays";}
            static get FuelDecreaseTimes() { return "FuelDecreaseTimes";}
            static get SendToNavigatorUrl() { return "SendToNavigatorUrl";}
            static get DltServerPort() { return "DltServerPort";}
        };
    }

    static get SystemConfigurationType () {
        return class SystemConfigurationType {
            static get General() { return "General";}
            static get Interface() { return "Interface";}
            static get Gateway() { return "Gateway";}
        };
    }

    static get Permission () {
        return class Permission {
            static get ViewAssetReferenceNo () { return "TrkVwAssetReferenceNo";}  // รหัสรถ
            static get ViewDeliveryMan () { return "TrkVwDeliveryMan";}         // คนสางของ
            static get ViewDriverName () { return "TrkVwDriverName";}         // คนขับ
            static get ViewCoDriverName () { return "TrkVwCoDriver";}         // ผู้ช่วยขับ
            static get ViewBusinessUnit () { return "TrkVwBusinessUnit";}            // แผนก
            static get ViewJob () { return "TrkVwJob";}                       // งาน
            static get ViewEngine () { return "TrkVwEngine";}              // สถานะเครื่องยนต์
            static get ViewGate () { return "TrkVwGate";}                  // สถานะประตู1
            static get ViewFuel () { return "TrkVwFuel";}                  // ระดับน้ำมัน
            static get ViewTemp () { return "TrkVwTemp";}                  // อุณหภูมิ1
            static get ViewSOS () { return "TrkVwSOS";}                    // SOS
            static get ViewPOWER () { return "TrkVwPower";}                // Power
            static get ViewGarminFMI () { return "TrkVwGarminFMI";}        // GarminFMI
            static get ViewCardReader () { return "TrkVwCardReader";}      // CardReader
            static get ViewCamera () { return "TrkVwCamera";}              // Camera
            static get ViewTotalEvent () { return "TrkVwTotalEvent";}                // ข้อมูลแจ้งเตือน
            static get ViewDistance () { return "TrkVwDistance";}          // ระยะทาง
            static get ViewAltitude () { return "TrkVwAltitude";}           // Altitude
            static get ViewDLTStatus () { return "TrkVwDLTStatus";}         // DLT Status
            static get ViewCustomFeature () { return "TrkVw<FeatureCode>";}  // Featurenames
            static get ViewVehicleBattery  () { return "TrkVwVehicleBattery";}  // ระดับแบตเตอรี่รถ
            static get ViewDeviceBattery   () { return "TrkVwDeviceBattery";}  // ระดับแบตเตอรี่อุปกรณ์
            static get ViewDriverCardReader    () { return "TrkVwDRVCardReader";}  // DriverCardReader
            static get ViewPassengerCardReader    () { return "TrkVwPSGRCardReader";}  // PassengerCardReader
            static get ViewMileage    () { return "TrkVwMileage";}  // Mileage
            static get ViewPoi    () { return "TrkVwPoi";}  // POI

            static get SubscriptionManagement () { return "SYS001";}
            static get CreateSubscription () { return "SYS0011";}
            static get UpdateSubscription () { return "SYS0012";}
            static get DeleteSubscription () { return "SYS0013";}
            static get CreateGroup_BackOffice () { return "SYS0014";}
            static get UpdateGroup_BackOffice () { return "SYS0015";}
            static get DeleteGroup_BackOffice () { return "SYS0016";}
            static get Configuration_BackOffice () { return "SYS002";}
            static get UpdateSettings () { return "SYS0021";}
            static get Translation () { return "SYS0022";}
            static get POIIconsManagement_BackOffice () { return "SYS0023";}
            static get VehicleIconsManagement () { return "SYS0024";}
            static get UpdateVendors () { return "SYS0025"; }
            static get TrackingDatabaseManagement () { return "SYS0026"; }
            static get MapServiceManagement () { return "SYS0027"; }
            static get UpdateNostraConfiguration() { return "SYS0028"; }
            static get GISServiceConfiguration () { return "SYS0029"; }
            static get AutoMail () { return "SYS00210"; }
            static get UserManagement_BackOffice () { return "SYS003";}
            static get CreateUser_BackOffice () { return "SYS0031";}
            static get UpdateUser_BackOffice () { return "SYS0032";}
            static get DeleteUser_BackOffice () { return "SYS0033";}
            static get CompanyManagement () { return "SYS004";}
            static get CreateCompany () { return "SYS0041";}
            static get UpdateCompany () { return "SYS0042";}
            static get DeleteCompany () { return "SYS0043";}
            static get AssetManagement () { return "SYS007";}
            static get CreateBoxFeature () { return "SYS0071";}
            static get UpdateBoxFeature () { return "SYS0072";}
            static get DeleteBoxFeature () { return "SYS0073";}
            static get CreateBoxModel () { return "SYS0074";}
            static get UpdateBoxModel () { return "SYS0075";}
            static get DeleteBoxModel () { return "SYS0076";}
            static get CreateBoxTemplate () { return "SYS0077";}
            static get UpdateBoxTemplate () { return "SYS0078";}
            static get DeleteBoxTemplate () { return "SYS0079";}
            static get CreateBoxsStock () { return "SYS00710";}
            static get UpdateBoxsStock () { return "SYS00711";}
            static get DeleteBoxsStock () { return "SYS00712";}
            static get CreateBox () { return "SYS00713";}
            static get UpdateBox () { return "SYS00714";}
            static get DeleteBox () { return "SYS00715";}
          
            static get CreateVehicleModel () { return "SYS00719";}
            static get UpdateVehicleModel () { return "SYS00720";}
            static get DeleteVehicleModel () { return "SYS00721";}
            static get CreateMDVRModel () { return "SYS00722";}
            static get UpdateMDVRModel () { return "SYS00723";}
            static get DeleteMDVRModel () { return "SYS00724";}
            static get CreateMDVRs () { return "SYS00725";}
            static get UpdateMDVRs () { return "SYS00726";}
            static get DeleteMDVRs () { return "SYS00727";}
            static get CreateMDVRsConfig () { return "SYS00728";}
            static get UpdateMDVRsConfig () { return "SYS00729";}
            static get DeleteMDVRsConfig () { return "SYS00730";}
            static get Reports_BackOffice () { return "SYS008";}
            static get GenerateNoSignalReport () { return "SYS0081";}
            static get GenerateCompanyExpirationReport () { return "SYS0082";}
            static get GenerateContractExpirationReport () { return "SYS0083";}
            static get GenerateFleetServiceExpirationReport () { return "SYS0084";}
            static get GenerateMobileServiceExpirationReport () { return "SYS0085";}
            static get GenerateBoxMaintenanceExpirationReport () { return "SYS0086";}
            static get EmailLogReport () { return "SYS0087";}
            static get DLTReport () { return "SYS0088";}
            static get DLTNewInstallation () { return "SYS0089";}
            static get DLTNextYear () { return "SYS00810";}
            static get DLTLetter () { return "SYS00811";}
            static get Announcement () { return "SYS009";}
            static get CreateAnnouncement () { return "SYS0091";}
            static get UpdateAnnouncement () { return "SYS0092";}
            static get DeleteAnnouncement () { return "SYS0093";}
            static get CanAccessAllCompanies () { return "SYS010";}
            static get Ticket () { return "SYS011";}
            static get AssetMonitoring () { return "SYS0111";}
            static get TicketDashboard () { return "SYS0112";}
            static get Installation () { return "SYS0113";}
            static get Appointments () { return "SYS0114";}
            static get MailManagement () { return "SYS0115";}
            static get SimManagement () { return "SYS0116";}
            static get OperatorPackage () { return "SYS0117";}
            static get TransportersManagement () { return "SYS0118";}
            static get VendorsManagement () { return "SYS0119";}

            static get DMS () { return "SYS012";}
            static get DMSModelCreate () { return "SYS0121";}
            static get DMSModelUpdate () { return "SYS0122";}
            static get DMSModelDelete () { return "SYS0123";}
            static get DMSDeviceCreate () { return "SYS0124";}
            static get DMSDeviceUpdate () { return "SYS0125";}
            static get DMSDeviceDelete () { return "SYS0126";}
            static get DMSConfigCreate () { return "SYS0127";}
            static get DMSConfigUpdate () { return "SYS0128";}
            static get DMSConfigDelete () { return "SYS0129";}

            static get ADAS () { return "SYS013";}
            static get ADASModelUpdate () { return "SYS0132";}
            static get ADASModelCreate () { return "SYS0131";}
            static get ADASModelDelete () { return "SYS0133";}
            static get ADASDeviceCreate () { return "SYS0134";}
            static get ADASDeviceUpdate () { return "SYS0135";}
            static get ADASDeviceDelete () { return "SYS0136";}

            static get BusinessUnitManagement () { return "COM002";}
            static get ViewBusinessUnit () { return "COM0024";}
            static get CreateBusinessUnit () { return "COM0021";}
            static get UpdateBusinessUnit () { return "COM0022";}
            static get DeleteBusinessUnit () { return "COM0023";}

            static get GroupManagement () { return "COM003";}
            static get ViewGroup () { return "COM0034";}
            static get CreateGroup_Company () { return "COM0031";}
            static get UpdateGroup_Company () { return "COM0032";}
            static get DeleteGroup_Company () { return "COM0033";}

            static get UserManagement_Company () { return "COM004";}
            static get ViewUser_Company () { return "COM0044";}
            static get CreateUser_Company () { return "COM0041";}
            static get UpdateUser_Company () { return "COM0042";}
            static get DeleteUser_Company () { return "COM0043";}

            static get Configuration_Company () { return "COM005";}

            static get AlertManagement () { return "COM0051";}
            static get ViewAlertManagement () { return "COM005101";}
            static get CreateAlertManagement () { return "COM005102";}
            static get UpdateAlertManagement () { return "COM005103";}
            static get DeleteAlertManagement () { return "COM005104";}

            static get TranslationManagement () { return "COM0052";}

            static get POIIconsManagement_Company () { return "COM0053";}
            static get ViewPOIIconsManagement () { return "COM005301";}
            static get CreatePOIIconsManagement () { return "COM005302";}
            static get UpdatePOIIconsManagement () { return "COM005303";}
            static get DeletePOIIconsManagement () { return "COM005304";}

            static get Barrier () { return "COM00540";}
            static get ViewBarrier () { return "COM005401";}
            static get Barriers_Create() { return "COM0054"; }
            static get Barriers_Update() { return "COM0055"; }
            static get Barriers_Delete() { return "COM0056"; }

            static get AlertCategory () { return "COM00550";}
            static get ViewAlertCategory () { return "COM005501";}
            static get Alert_Category_Create() { return "COM0059"; }
            static get Alert_Category_Update() { return "COM0060"; }
            static get Alert_Category_Delete() { return "COM0061"; }

            static get Ticket_Response () { return "COM00560";}
            static get ViewTicket_Response () { return "COM005601";}
            static get Ticket_Response_Create() { return "COM0062"; }
            static get Ticket_Response_Update() { return "COM0063"; }
            static get Ticket_Response_Delete() { return "COM0064"; }

            static get Telematics_Configuration () { return "COM00580";}

            static get Manage_Ticket () { return "COM0057";}
            static get Ticket_View_Only() { return "COM0058"; }
            static get AnnouncementAdmin() { return "COM0065"; }
            static get CreateAnnouncementAdmin() { return "COM0066"; }
            static get UpdateAnnouncementAdmin() { return "COM0067"; }
            static get DeleteAnnouncementAdmin() { return "COM0068"; }
            static get GlobalSearch () { return "COM008";}
            static get Logistics () { return "COM0081";}
            static get Logistics_BusinessUnit () { return "COM00811";}
            static get Logistics_User () { return "COM00812";}
            static get Logistics_Driver () { return "COM00813";}
            static get Logistics_Vehicle () { return "COM00814";}
            static get Map () { return "COM0082";}
            static get AdminPoly () { return "COM0083";}
            static get Street () { return "COM0084";}
            static get FleetMonitoring () { return "COM010";}
            static get TrackMonitoring () { return "COM0101";}
            static get ViewAlert () { return "COM0102";}
            static get ViewPlayback () { return "COM0103";}
            static get PlaybackWorkingTime() { return "COM0104"; }
            static get LiveVideo() { return "COM0105"; }
            static get EventVideo() { return "COM0106"; }
            static get Debrief() { return "COM0107"; }
            static get TripAnalysis() { return "COM0108" }
            static get PlaybackAnalysisTool() { return "COM0109";}

            static get MapFunctions () { return "COM011";}
            static get Reports () { return "COM012";}

            static get GeneralReport () { return "COM0121";}
            static get GeneralReport_Tracking () { return "COM012101";}
            static get GeneralReport_Driving () { return "COM012102";}
            static get GeneralReport_Stoppage () { return "COM012103";}
            static get GeneralReport_OpenCloseGate () { return "COM012104";}
            static get GeneralReport_BadGPS () { return "COM012105";}
            static get GeneralReport_OverSpeed () { return "COM012106";}
            static get GeneralReport_TruckNotUpateGPS () { return "COM012107";}
            static get GeneralReport_ConcreteDischarge () { return "COM012108";}
            static get GeneralReport_TrackingPEA () { return "COM012109";}
            static get GeneralReport_EventReport () { return "COM012110";}

            static get ComparisonReport () { return "COM0122";}
            static get ComparisonReport_FuelTemperature () { return "COM012201";}
            static get ComparisonReport_TemperatureSpeed () { return "COM012202";}
            static get ComparisonReport_FuelSpeed () { return "COM012203";}
            static get ComparisonReport_TemperatureGate () { return "COM012204";}

            static get SummaryReport () { return "COM0123";}
            static get SummaryReport_TravelDistance () { return "COM012301";}
            static get SummaryReport_POIParking () { return "COM012302";}
            static get SummaryReport_ParingOvertime () { return "COM012303";}
            static get SummaryReport_DriverPerformance () { return "COM012304";}
            static get SummaryReport_Overall () { return "COM012305";}
            static get SummaryReport_Backward () { return "COM012306";}
            static get SummaryReport_Monthly () { return "COM012307";}
            static get SummaryReport_Daily () { return "COM012308";}
            static get SummaryReport_Driving () { return "COM012309";}
            static get SummaryReport_VehicleUsage () { return "COM012310";}
            static get SummaryReport_Shipment () { return "COM012311";}
            static get SummaryReport_AvtivatingDriving () { return "COM012312";}
            static get SummaryReport_WoringTime () { return "COM012313";}
            static get SummaryReport_RAGScore () { return "COM012314";}
            static get SummaryReport_AdvanceScore () { return "COM012315";}
            

            static get DelinquentReport () { return "COM0124";}
            static get DelinquentReport_DriverOvertime () { return "COM012401";}
            static get DelinquentReport_AnonymousDriver () { return "COM012402";}
            static get DelinquentReport_IncorrectDriverLicense () { return "COM012403";}
            static get DelinquentReport_ConnectionLost () { return "COM012404";}
            static get DelinquentReport_RestLessThan10Hours () { return "COM012405";}
            

            static get AdvanceReport () { return "COM0125";}
            static get FleetTypeLogReport() { return "COM0126"; }
            static get KPIReport() { return "COM0127"; }
            static get AdvanceDriverPerformanceReport() { return "COM0128"; }

            static get GeoFencing () { return "COM013";}

            static get CustomPOI () { return "COM01330";}
            static get ViewCustomPOI () { return "COM013301";}
            static get CreateCustomPOI () { return "COM0131";}
            static get UpdateCustomPOI () { return "COM0132";}
            static get DeleteCustomPOI () { return "COM0133";}
            static get SharedCustomPOI() { return "COM01321"; }
            static get ManagePOIArea() { return "COM013302"; }

            static get CustomPOICategory () { return "COM01340";}
            static get ViewCustomPOICategory () { return "COM013401";}
            static get CreateCustomPOICategory () { return "COM0134";}
            static get UpdateCustomPOICategory () { return "COM0135";}
            static get DeleteCustomPOICategory () { return "COM0136";}

            static get CustomRoute () { return "COM01350";}
            static get ViewCustomRoute () { return "COM013501";}
            static get CreateCustomRoute () { return "COM0137";}
            static get UpdateCustomRoute () { return "COM0138";}
            static get DeleteCustomRoute () { return "COM0139";}
            static get SharedCustomRoute() { return "COM01322"; }

            static get CustomRouteCategory () { return "COM01360";}
            static get ViewCustomRouteCategory () { return "COM013601";}
            static get CreateCustomRouteCategory () { return "COM01310";}
            static get UpdateCustomRouteCategory () { return "COM01311";}
            static get DeleteCustomRouteCategory () { return "COM01312";}

            static get CustomArea () { return "COM01370";}
            static get ViewCustomArea () { return "COM013701";}
            static get CreateCustomArea () { return "COM01313";}
            static get UpdateCustomArea () { return "COM01314";}
            static get DeleteCustomArea () { return "COM01315";}
            static get SharedCustomArea() { return "COM01323"; }

            static get CustomAreaCategory () { return "COM01380";}
            static get ViewCustomAreaCategory () { return "COM013801";}
            static get CreateCustomAreaCategory () { return "COM01316";}
            static get UpdateCustomAreaCategory () { return "COM01317";}
            static get DeleteCustomAreaCategory () { return "COM01318";}

            static get CustomPOISuggestion () { return "COM01319";}
            static get Routes() { return "COM01320"; }            
            
            static get Asset () { return "COM014";}
            static get CreateBoxMaintenance () { return "COM0141";}
            static get UpdateBoxMaintenance () { return "COM0142";}
            static get DeleteBoxMaintenance () { return "COM0143";}

            static get Vehicle () { return "COM014420";}
            static get ViewVehicle () { return "COM0144201";}
            static get CreateVehicle () { return "COM0144";}
            static get UpdateVehicle () { return "COM0145";}
            static get DeleteVehicle () { return "COM0146";}

            static get VehicleMaintenancePlan () { return "COM014480";}
            static get CreateVehicleMaintenancePlan () { return "COM0144801";}
            static get UpdateVehicleMaintenancePlan () { return "COM0144802";}
            
            static get Driver () { return "COM014430";}
            static get ViewDriver () { return "COM0144301";}
            static get CreateDriver () { return "COM01410";}
            static get UpdateDriver () { return "COM01411";}
            static get DeleteDriver () { return "COM01412";}

            static get FleetService () { return "COM014440";}
            static get ViewFleetService () { return "COM0144401";}
            static get CreateFleetService () { return "COM01413";}
            static get UpdateFleetService () { return "COM01414";}
            static get DeleteFleetService () { return "COM01415";}

            static get MobileService () { return "COM014450";}
            static get ViewMobileService () { return "COM0144501";}
            static get CreateMobileService () { return "COM01416";}
            static get UpdateMobileService () { return "COM01417";}
            static get DeleteMobileService () { return "COM01418";}

            static get SMSService () { return "COM014460";}
            static get ViewSMSService () { return "COM0144601";}
            static get CreateSMSService () { return "COM01419";}
            static get UpdateSMSService () { return "COM01420";}
            static get DeleteSMSService () { return "COM01421";}
            
            static get ViewMaintenance () { return "COM01422"}
            static get UpdateMaintenance() { return "COM01423" }

            static get GPSDevices() { return "COM01424"}

            /* Trainer Management Permission : SCCC-S8 */
            static get Trainer() { return "COM014470"; }
            static get ViewTrainer() { return "COM0144701"; }
            static get CreateTrainer() { return "COM01425"; }
            static get UpdateTrainer() { return "COM01426"; }
            static get DeleteTrainer() { return "COM01427"; }

            static get Shipment() { return "SHM001" }
            static get ShipmentDashboard() { return "SHM0011" }
            static get ShipmentManagement() { return "SHM0012" }
            static get ShipmentPOSearch() { return "SHM0013" }
            static get ShipmentDefaultValue() { return "SHM0014" }
            static get ShipmentCreate() { return "SHM0015" }
            static get ShipmentUpdate() { return "SHM0016" }
            static get ShipmentDelete() { return "SHM0017" }
            static get ShipmentFinish() { return "SHM0018" }
            static get ShipmentCancel() { return "SHM0019" }

            static get ShipmentLastWaypoint() { return "SHM0020" }
            static get ShipmentTimeline() { return "SHM0021" }

            static get ShipmentTemplatesCreate() { return "SHM0022" }
            static get ShipmentTemplatesUpdate() { return "SHM0023" }
            static get ShipmentTemplatesDelete() { return "SHM0024" }

            static get ShipmentTemplatesManagement() { return "SHM0029" }

            static get ShipmentCategory() { return "SHM0031" }
            static get ShipmentCategoryCreate() { return "SHM00311" }
            static get ShipmentCategoryUpdate() { return "SHM00312" }
            static get ShipmentCategoryDelete() { return "SHM00313" }

            static get ShipmentZone() { return "SHM0032"; }
            static get ShipmentZoneCreate() { return "SHM00321"; }
            static get ShipmentZoneUpdate() { return "SHM00322"; }
            static get ShipmentZoneDelete() { return "SHM00323";}

            static get DriverPerformanceRule () { return "COM015";}
            static get DriverPerformanceRule () { return "COM01510";}
            static get ViewDriverPerformanceRule () { return "COM015101";}
            static get CreateDriverPerformanceRule () { return "COM0151";}
            static get UpdateDriverPerformanceRule () { return "COM0152";}
            static get DeleteDriverPerformanceRule() { return "COM0153"; }

            static get AdvanceDriverPerformanceRule() { return "COM01520"; }
            static get ViewAdvanceDriverPerformanceRule() { return "COM015201"; }
            static get CreateAdvanceDriverPerformanceRule() { return "COM0154"; }
            static get UpdateAdvanceDriverPerformanceRule() { return "COM0155"; }
            static get DeleteAdvanceDriverPerformanceRule() { return "COM0156"; }

            static get CanAccessAllBUs () { return "COM016";}

            static get VendorManagement () { return "COM017";}
            static get CreateTransporter () { return "COM0171";}
            static get UpdateTransporter () { return "COM0172";}
            static get DeleteTransporter () { return "COM0173";}
            static get CreateVendor () { return "COM0174";}
            static get UpdateVendor () { return "COM0175";}
            static get DeleteVendor () { return "COM0176";}

            /* Dashboard : Utilization Driving Time : SCCC-S8 */
            static get Dashboard() { return "COM018"; }
            static get DashboardUtilizationDrivingTime() { return "COM0181"; }
            static get TruckStatus() { return "COM0182"; }
            static get DriverPerformanceReport() { return "COM0183"; }
            static get SafetyDashboard() { return "COM0184"; }

            //AUTOMAIL 
            static get AUTOMAILREPORT() { return "COM020"; }
            static get VIEWAUTOMAILREPORT() { return "COM0201"; }
            static get CREATAUTOMAILREPORT() { return "COM0202"; }
            static get UPDATEAUTOMAILREPORT() { return "COM0203"; }
            static get DELETEAUTOREPORT() { return "COM0204"; }


            //BOMs
            static get BOMs() { return "COM019"; }
            static get BOMsRealtimePassengerDashboard() { return "COM0191"; }
            static get BOMsReports() { return "COM0192"; }

            //BOMs Reports
            static get BOMsReportsSummaryRoundTrip() { return "COM01926"; }
            static get BOMsReportsRealtimePassengerOnBoard() { return "COM01927"; }
            static get BOMsReportsSummaryPassengerByRoute() { return "COM01921"; }
            static get BOMsReportsSummaryPassengerByZone() { return "COM01922"; }
            static get BOMsReportsBusUsageStatistics() { return "COM01923"; }
            static get BOMsReportsSummaryTransportation() { return "COM01924"; }
            static get BOMsReportsMonthlySummartTransportation() { return "COM01925"; }

            //Boms Default Values
            static get BomsDefaultValue() { return "COM0193"; }
            static get CreateBomsDefaultValue() { return "COM01931"; }
            static get UpdateBomsDefaultValue() { return "COM01932";}
            static get DeleteBomsDefaultValue() { return "COM01933";}

            static get Alert_AccRapid () { return "ALT001";}
            static get Alert_AcquiredGPS () { return "ALT002";}
            static get Alert_BadGPS () { return "ALT003";}
            static get Alert_VehicleBatteryLow () { return "ALT004";}
            static get Alert_DeviceBatteryLow () { return "ALT005";}
            static get Alert_DecRapid () { return "ALT006";}
            static get Alert_EngineIdle () { return "ALT007";}
            static get Alert_EngineOff () { return "ALT008";}
            static get Alert_EngineOn () { return "ALT009";}
            static get Alert_GateClose () { return "ALT010";}
            static get Alert_GateOpen () { return "ALT011";}
            static get Alert_GPSDisCnt () { return "ALT012";}
            static get Alert_GPSReCnt () { return "ALT013";}
            static get Alert_Move () { return "ALT014";}
            static get Alert_OverSpeed () { return "ALT015";}
            static get Alert_OverTemperature () { return "ALT016";}
            static get Alert_PowerDisCnt () { return "ALT017";}
            static get Alert_PowerReCn () { return "ALT018";}
            static get Alert_Stop () { return "ALT019";}
            static get Alert_Swerve () { return "ALT020";}
            static get Alert_UnderTemperature () { return "ALT021";}
            static get Alert_UnsFuelDec () { return "ALT022";}
            static get Alert_CloseGateInsPOI () { return "ALT023";}
            static get Alert_CloseGateOtsPOI () { return "ALT024";}
            static get Alert_JobAvgSpeed () { return "ALT025";}
            static get Alert_JobTotDist () { return "ALT026";}
            static get Alert_JobTotTime () { return "ALT027";}
            static get Alert_OpenGateInsPOI () { return "ALT028";}
            static get Alert_OpenGateOtsPOI () { return "ALT029";}
            static get Alert_OverTimePark () { return "ALT030";}
            static get Alert_IncmWP () { return "ALT031";}
            static get Alert_OutgWP () { return "ALT032";}
            static get Alert_SOS () { return "ALT033";}
            static get Alert_Parking () { return "ALT034";}
            static get Alert_POIParking () { return "ALT035";}
            static get Alert_POIInOut () { return "ALT036";}
            static get Alert_AreaParking () { return "ALT037";}
            static get Alert_AreaInOutVehicle () { return "ALT038";}
            static get Alert_AreaInOutJob () { return "ALT039";}
            static get Alert_MoveNonWorking () { return "ALT040";}
            static get Alert_ExceedLimitDriving () { return "ALT041";}
            static get Alert_CustomAlert () { return "ALT042";}
            static get Alert_Dispatcher () { return "ALT043";}
            static get Alert_Urgent () { return "ALT044";}
            static get Alert_Maintenance () { return "ALT045";}
            static get Alert_ExceedLimitDrivingIn24Hours () { return "ALT046";}
            static get Alert_InvalidDriver () { return "ALT047";}
            static get Alert_TemperatureError() { return "ALT048"; }

        };
    }

    static get EmailVariableName () {
        return class EmailVariableName {
            static get FirstName () { return "firstname";}
            static get LastName () { return "lastname";}
            static get UserName () { return "username";}
            static get Password () { return "password";}
        };
    }

    static get EmailTemplateCode () {
        return class EmailTemplateCode {
            static get CreateUserForm () { return "CreateUserForm";}
            static get CreateUserActiveDirectory () { return "CreateUserActiveDirectory";}
            static get ForgotPassword () { return "ForgotPassword";}
            static get ResetPassword () { return "ResetPassword";}
        };
    }

    static get SmsTemplateCode () {
        return class SmsTemplateCode {
            static get ForgotPassword () { return "ForgotPassword";}
            static get ResetPassword () { return "ResetPassword";}
        };
    }

    static get AssociateEntityType () {
        return class AssociateEntityType {
            static get MobileService () { return "Mobile Service";}
            static get User () { return "User";}
            static get Box () { return "Box";}
            static get FleetService () { return "Fleet Service";}
            static get BoxTemplate () { return "Box Template";}
            static get AlertConfiguration () { return "Alert Configuration";}

            static get BusinessUnit () { return "Business Unit";}
            static get Vehicle () { return "Vehicle";}
            static get Driver () { return "Driver";}
            static get Job () { return "Job";}
            static get JobTemplate () { return "Job Template";}
            static get Order () { return "Order";}
            static get PoiTrip () { return "PoiTrip";}
        };
    }

    static get FeatureCode () {
        return class FeatureCode {
            static get Speed () { return "TrkVwSpeed";}
            static get FuelLevel () { return "TrkVwFuel";}
            static get Temperature1 () { return "TrkVwTemp";}
            static get Temperature2 () { return "TrkVwTemp2";}
            static get Temperature3 () { return "TrkVwTemp3";}
            static get BatteryLevel () { return "TrkVwBattery";}
            static get GarminFMI () { return "TrkVwGarminFMI";}
        };
    }

    static get FileExtention () {
        return class FileExtention {
            static get Xls () { return "xls";}
            static get Xlsx () { return "xlsx";}
            static get Pdf () { return "pdf";}
            static get Zip () { return "zip";}
            static get Png () { return "png";}
        };
    }

    static get VehicleImageSetName () {
        return class VehicleImageSetName {
            static get N () { return "n.png";}
            static get E () { return "e.png";}
            static get W () { return "w.png";}
            static get S () { return "s.png";}
            static get NE () { return "ne.png";}
            static get SE () { return "se.png";}
            static get NW () { return "nw.png";}
            static get SW() { return "sw.png";}
        };
    }

    static get Report () {
        return class Report {
            static get Box () { 
                return class Box {
                    static get FileName () {
                        return class FileName {
                            static get Export () { return "Boxes.xlsx";}
                            static get DownloadTemplate () { return "Import_Boxes.xlsx";}
                        };
                    }
                    static get Column () {
                        return class Column {
                            static get SerialNo () { return "Serial No.";}
                            static get Imei () { return "IMEI";}
                            static get IsForRent () { return "Is For Rent";}
                            static get Company () { return "Company";}
                            static get BusinessUnit () { return "Business Unit";}
                            static get Stock () { return "Stock";}
                            static get Status () { return "Status";}
                            static get InstalledDateTime () { return "Installed Date Time";}
                            static get TrackInterval () { return "Track Interval (second)";}
                            static get AdjustTemperature () { return "Adjust Temperature";}
                            static get CalculateDistanceByMileage () { return "Calculate Distance by Mileage";}
                            static get Template () { return "Template";}
                            static get InputEvents () { return "Input Events";}
                            static get OutputEvents () { return "Output Events";}
                            static get Accessories () { return "Accessories";}
                            static get Note () { return "Note";}
                        };
                    }
                    static get FieldName () {
                        return class FieldName {
                            static get SerialNo () { return "SerialNo";}
                            static get Imei () { return "Imei";}
                            static get IsForRent () { return "FormatIsForRent";}
                            static get CompanyName () { return "CompanyName";}
                            static get BusinessUnitName () { return "BusinessUnitName";}
                            static get StockName () { return "BoxStockName";}
                            static get Status () { return "BoxStatusDisplayName";}
                            static get InstalledDateTime () { return "FormatInstalledDate";}
                            static get TrackInterval () { return "TrackInterval";}
                            static get AdjustTemperature () { return "AdjustTemperature";}
                            static get CalculateDistanceByMileage () { return "FormatIsCalculateDistanceByMileage";}
                            static get BoxTemplateName () { return "BoxTemplateName";}
                            static get InputEvents () { return "InputEvents";}
                            static get OutputEvents () { return "OutputEvents";}
                            static get Accessories () { return "Accessories";}
                            static get Note () { return "Note";}
                        };
                    }
                };
            }

            static get TrackMonitoring () {
                return class TrackMonitoring {
                    static get FileName () {
                        return class FileName {
                            static get Export () { return "TrackMonitoring.xlsx";}
                        };
                    }
                    static get FieldName () {
                        return class FieldName {
                            static get Datetime () { return "Datetime";}
                        };
                    }
                };
            }

            static get Alert () {
                return class Alert {
                    static get FileName () {
                        return class FileName {
                            static get Export () { return "Alerts.xlsx";}
                        };
                    }
                };
            }

            static get NearestAssets () {
                return class NearestAssets {
                    static get FileName () {
                        return class FileName {
                            static get Export () { return "NearestAssets.xlsx";}
                        };
                    }
                };
            }

            static get Vehicle () {
                return class Vehicle {
                    static get Column () {
                        return class Column {
                            static get ReferenceNo () { return "Reference No.";}
                            static get Type () { return "Type";}
                            static get VehicleModel () { return "VehicleModel";}
                            static get Icon () { return "Icon";}
                            static get BusinessUnit () { return "Business Unit";}
                            static get Enable () { return "Enable";}
                            static get Seat () { return "Seat";}
                            static get Volume () { return "Volume (Volume Unit*)";}
                            static get Description () { return "Description";}
                            static get FuelRate () { return "Fuel Rate (km/l)";}
                            static get StopDuration () { return "Stop Duration (second)";}
                            static get RegisteredType () { return "Registered Type";}
                            static get Trail () { return "Trail";}
                            static get OverSpeedDelay () { return "Over Speed Delay (second)";}
                            static get ParkEngineOnLimit () { return "Park Engine On Limit (second)";}
                            static get License () { return "License";}
                        };
                    }
                };
            }

            static get Driver () {
                return class Driver {
                    static get FileName () {
                        return class FileName {
                            static get Export () { return "Drivers.xlsx";}
                            static get DriversDownloadTemplate () { return "Import_Drivers.xlsx";}
                            static get DriverPicturesDownloadTemplate () { return "Import_DriverPictures.xlsx";}
                        };
                    }
                    static get Column () {
                        return class Column {
                            static get EmployeeId () { return "Employee ID";}
                            static get CardType () { return "Card Type";}
                            static get CardId () { return "Card ID";}
                            static get Gender () { return "Gender";}
                            static get Title () { return "Title";}
                            static get FirstName () { return "First Name";}
                            static get LastName () { return "Last Name";}
                            static get BusinessUnit () { return "Business Unit";}
                            static get Phone () { return "Phone";}
                            static get LicenseNo () { return "License No.";}
                            static get LicenseType () { return "License Type";}
                            static get MinimumRestTime () { return "Minimum Rest Time (minutes)";}
                        };
                    }
                    static get FieldName () {
                        return class FieldName {
                            static get EmployeeId () { return "EmployeeId";}
                            static get CardType () { return "CardType";}
                            static get CardId () { return "CardId";}
                            static get Gender () { return "Gender";}
                            static get Title () { return "Title";}
                            static get FirstName () { return "FirstName";}
                            static get LastName () { return "LastName";}
                            static get BusinessUnit () { return "BusinessUnit";}
                            static get Phone () { return "Phone";}
                            static get LicenseNo () { return "LicenseNo";}
                            static get LicenseType () { return "LicenseType";}
                            static get MinimumRestTime () { return "MinRestTime";}
                        };
                    }
                };
            }

            static get StringResource () {
                return class StringResource {
                    static get FileName () {
                        return class FileName {
                            static get Export () { return "Translation_{0}_{1}.xlsx";}
                            static get DownloadTemplate () { return "Translation_{0}.xlsx";}
                        };
                    }
                    static get Column () {
                        return class Column {
                            static get Key () { return "Key";}
                            static get DefaultLanguage () { return "Default Language";}
                            static get TranslatedLanguage () { return "Translated Language";}
                        };
                    }
                    static get FieldName () {
                        return class FieldName {
                            static get Key () { return "ResourceKey";}
                            static get DefaultLanguage () { return "DefaultLanguageValue";}
                            static get TranslatedLanguage () { return "TranslatedLanguageValue";}
                        };
                    }
                };
            }
        };
    }

    static get ImportErrorReason () {
        return class ImportErrorReason {
            static get MissingColumn () { return "{0} - {1} column is missing."; }
            static get RequiredValue () { return "{0},{1} - {2} is required."; }
            static get InvalidValue () { return "{0},{1} - {2} is invalid."; }
            static get DuplicatedValue () { return "{0},{1} - {2} is duplicated."; }
            static get MissingSystemKeys () { return "Cannot import default language. Some keys in file is missing."; }
        };
    }

    static get ValidationErrorFieldName () {
        return class ValidationErrorFieldName {
            static get Name () { return "Name"; }
            static get Code () { return "Code"; }
            static get Imei () { return "Imei"; }
            static get SerialNo () { return "SerialNo"; }
            static get ReferenceNo () { return "ReferenceNo"; }
            static get License () { return "License"; }
            static get Username () { return "Username"; }
            static get CardId () { return "CardId"; }
            static get EmployeeId () { return "EmployeeId"; }
        };
    }

    static get ResourceKey () {
        return class ResourceKey {
            static get UiLabel () {
                return class UiLabel {
                    static get Common_Yes () { return "Common_Yes";}
                    static get Common_No () { return "Common_No";}
                };
            }
        };
    }

    static get CountryCode () {
        return class CountryCode {
            static get TH () { return "66";}
            static get SG () { return "65";}
            static get MY () { return "60";}
            static get BN () { return "673";}
            static get VN () { return "84";}
            static get PH () { return "63";}
            static get ID () { return "62";}
            static get MM () { return "95";}
            static get LA () { return "856";}
            static get KH () { return "855";}
        };
    }
}

// Gisc.Csd.Log.Domain.Common.Constant.Enums (on demand)
export class Enums {
    static get ModelData () {
        return class ModelData {
            static get DeliveryType () {
                return class DeliveryType
                {
                    static get Both () { return 1; }
                    static get Deliver () { return 2; }
                    static get Pickup () { return 3; }
                };
            }

            static get AuthenticationType () {
                return class AuthenticationType
                {
                    static get Form () { return 1;}
                    static get ActiveDirectory () { return 2;}
                    static get PEAIdm() { return 3;}
                };
            }

            static get AlertLevel () {
                return class AlertLevel
                {
                    static get Vehicle () { return 1;}
                    static get Dispatcher () { return 2;}
                    static get Job () { return 3;}
                    static get JobWaypoint () { return 4;}
                };
            }
            
            static get ResourceType () {
                return class ResourceType {
                    static get AlertParkingType () { return 1;}
                    static get AlertType () { return 2;}
                    static get AreaParkingType () { return 3;}
                    static get AreaUnit () { return 4;}
                    static get AuthenticationType () { return 5;}
                    static get BoxStatus () { return 6;}
                    static get ContractType () { return 7;}
                    static get Currency () { return 8;}
                    static get CustomAreaType () { return 9;}
                    static get DirectionType () { return 10;}
                    static get DistanceUnit () { return 11;}
                    static get DriverCardType () { return 12;}
                    static get DriverLicenseType () { return 13;}
                    static get DrivingType () { return 14;}
                    static get EventDataType () { return 15;}
                    static get EventType () { return 16;}
                    static get FeatureValue () { return 17;}
                    static get Gender () { return 18;}
                    static get GpsStatus () { return 19;}
                    static get GsmStatus () { return 20;}
                    static get JobStatus () { return 21;}
                    static get JobWaypointStatus () { return 22;}
                    static get MaintenanceStatus () { return 23;}
                    static get MovementType () { return 25;}
                    static get ParkingType () { return 26;}
                    static get PaymentType () { return 27;}
                    static get PlaybackDisplayType () { return 28;}
                    static get PlaybackTrackingType () { return 29;}
                    static get PoiCheckInType () { return 30;}
                    static get RouteMode () { return 31;}
                    static get RouteOption () { return 32;}
                    static get SearchEntityType () { return 33;}
                    static get TemperatureUnit () { return 34;}
                    static get TrackVehicleIconType () { return 35;}
                    static get UILabel () { return 36;}
                    static get UIMessage () { return 37;}
                    static get VehicleRegisteredType () { return 38;}
                    static get VehicleType () { return 39;}
                    static get VolumeUnit () { return 40;}
                    static get WeightUnit () { return 41;}
                    static get DltStatus() { return 42;}
                    static get GlobalSearchType() { return 43;}
                    static get GlobalSearchLogisticsCategory() { return 44;}
                    static get GlobalSearchMapCategory() { return 45;}
                    static get GlobalSearchAdminPolyCategory() { return 46;}
                    static get GeneralReportType() { return 47;}
                    static get DrivingReportType() { return 48;}
                    static get ComparisonReportType() { return 49;}
                    static get SummaryReportType() { return 50;}
                    static get DelinquentReportType() { return 51;}
                    static get BackwardDurationType() { return 52;}
                    static get AppointmentStatus() { return 53;}
                    static get VehicleMaintenanceType() { return 54;}
                    static get FuelType() { return 55;}
                    static get EngineType() { return 56;}
                    static get GroupByType () { return 57;}
                    static get MaintenanceReportType () { return 59;}
                    static get ERMVehicleType() { return 60;}
                    static get DeliveryType() { return 61;}
                    static get AccessType() { return 62;}
                    static get ManeuverLevel() { return 63;}
                    static get DeliveryOrderStatus() { return;}
                    static get AlertTicketStatus() { return 65;}
                    static get JobAutoFinish() { return 66;}
                    static get LateShipmentRole() { return 67;}
                    static get PoiCategoryType() { return 68;}
                    static get ParkingUtilizationType() { return 69;}
                    static get MovingUtilizationType() { return 70;}
                    static get JobTimelineTruckStatus() { return 71;}
                    static get JobTimelineSort() { return 72;}
                    static get AlertUnauthorizedCondition() { return 73;}
                    static get AlertUnauthorizedSpeedType() { return 74;}
                    static get Day() { return 75;}
                    static get AreaCategoryType() { return 76;}
                    static get JobArrivalType() { return 77;}
                    static get TravelMode() { return 78; }
                    static get PermissionType() { return 79; }
                    static get PoiType() { return 80; }
                };
            }

            static get EnumResourceType () {
                return class EnumResourceType
                {
                    static get AlertLevel () { return 1;}
                    static get AlertParkingType () { return 2;}
                    static get AlertType () { return 3;}
                    static get AreaParkingType () { return 5;}
                    static get AreaUnit () { return 6;}
                    static get AuditTrailActionType () { return 7;}
                    static get AuditTrailEntityType () { return 8;}
                    static get AuthenticationType () { return 9;}
                    static get BoxFeatureEventType () { return 10;}
                    static get BoxStatus () { return 11;}
                    static get ContractType () { return 12;}
                    static get Currency () { return 13;}
                    static get CustomAreaType () { return 14;}
                    static get DirectionType () { return 15;}
                    static get DistanceUnit () { return 16;}
                    static get DriveGrade () { return 17;}
                    static get DriverCardType () { return 18;}
                    static get DriverLicenseType () { return 19;}
                    static get DrivingType () { return 20;}
                    static get EnumResourceType () { return 21;}
                    static get EventActionType () { return 22;}
                    static get EventDataType () { return 23;}
                    static get EventType () { return 24;}
                    static get FeatureUnit () { return 25;}
                    static get FileType () { return 26;}
                    static get Gender () { return 27;}
                    static get GpsStatus () { return 28;}
                    static get GsmStatus () { return 29;}
                    static get JobStatus () { return 30;}
                    static get JobWaypointStatus () { return 31;}
                    static get LoginActionType () { return 32;}
                    static get MaintenanceStatus () { return 33;}
                    static get MediaEntityType () { return 35;}
                    static get MediaFieldName () { return 36;}
                    static get MovementType () { return 37;}
                    static get NotificationAction () { return 38;}
                    static get NotificationStatus () { return 39;}
                    static get NotificationType () { return 40;}
                    static get PaymentType () { return 41;}
                    static get PermissionCategory () { return 42;}
                    static get PlaybackDisplayType () { return 43;}
                    static get PlaybackTrackingType () { return 44;}
                    static get PoiCheckInType () { return 45;}
                    static get Portal () { return 46;}
                    static get ReportTemplateSection () { return 47;}
                    static get ReportTemplateType () { return 48;}
                    static get ResourceType () { return 49;}
                    static get RouteMode () { return 50;}
                    static get RouteOption () { return 51;}
                    static get RouteOptionDescription () { return 52;}
                    static get SearchEntityType () { return 53;}
                    static get StopChange () { return 54;}
                    static get TaskLogStatus () { return 55;}
                    static get TemperatureUnit () { return 56;}
                    static get TrackableType () { return 57;}
                    static get TrackVehicleIconType () { return 58;}
                    static get UserPreferenceKey () { return 59;}
                    static get VehicleRegisteredType () { return 60;}
                    static get VehicleType () { return 61;}
                    static get VolumeUnit () { return 62;}
                    static get WeightUnit () { return 63;}
                    static get ReportFieldDataType () { return 64;}
                    static get PortalMode () { return 65;}
                    static get GpsChange () { return 66;}
                    static get DltStatus () { return 67;}
                    static get GlobalSearchLogisticsCategory () { return 68;}
                    static get LocationReferenceType () { return 69;}
                    static get GlobalSearchType () { return 70;}
                    static get TimeUnit () { return 71;}
                    static get ParkingType () { return 72;}
                    static get GeneralReportType () { return 73;}
                    static get DrivingReportType () { return 74;}
                    static get ComparisonReportType () { return 75;}
                    static get SummaryReportType () { return 76;}
                    static get DelinquentReportType () { return 77;}
                    static get BackwardDurationType () { return 78;}
                    static get AppointmentStatus () { return 79;}

                    //Edit :: Run Constant ใหม่
                    static get VehicleMaintenanceType () { return 80;}
                    static get FuelType () { return 81;}
                    static get EngineType () { return 82;}
                    static get GroupByType () { return 83;}
                    static get MaintenanceReportType () { return 84;}
                    static get TicketStatus () { return 85;}
                    static get TicketAppointmentStatus () { return 86;}
                    static get ServiceType () { return 87;}
                    static get TicketDuration () { return 88;}
                    static get SimStatus () { return 89;}
                    static get CategoryType () { return 90;}
                    static get ERMVehicleType() { return 91;}
                    static get DeliveryType() { return 92; }
                    static get AccessType() { return 93; }
                    static get ManeuverLevel() { return 95;}
                    static get AlertTicketStatus() { return 97; }        

                    static get JobAutoFinish() { return 98; }
                    static get LastShipmentRole() { return 99; }
                    static get PoiCategoryType() { return 100; }
                    static get ParkingUtilizationType() { return 101; }
                    static get MovingUtilizationType() { return 102; }
                    static get MDVRModel() { return 103; }
                    static get JobTimelineTruckStatus() { return 104; }
                    static get JobTimelineSort() { return 105; }
                    static get GISServiceClassType() { return 106; }
                    static get GISServiceType() { return 107; }
                    static get AlertUnauthorizedCondition() { return 108; }
                    static get AlertUnauthorizedConfigType() { return 109; }
                    static get AlertUnauthorizedSpeedType() { return 110; }
                    static get Day() {return 111;}
                    static get AreaCategoryType() {return 112;}
                    static get JobArrivalType() {return 113;}
                    static get TravelMode() {return 114;}
                    static get PermissionType() {return 115;}
                    static get PoiType() { return 116; }
                    static get IntegrationType() { return 117; }
                    static get ManeuverLevelFilter() { return 118;}
                    static get TurnOption() { return 119;}
                    static get AutomailReportCategory() { return 120;}
                    static get ScheduleFrequency() { return 121;}
                    static get TripAnalysisReport() { return 122;}
                    static get AlertSound() { return 123;}

                };
            }

            static get FeatureUnit () {
                return class FeatureUnit
                {
                    static get Percent () { return 1;}
                    static get Temperature () { return 2;}
                    static get Speed () { return 3;}
                    static get Gate () { return 4;}
                };
            }

            static get UserPreferenceKey () {
                return class UserPreferenceKey
                {
                    static get DefaultVehicleIconType () { return 1;}
                    static get EnableSoundAlert () { return 2;}
                    static get FleetDelayTime () { return 3;}
                    static get Dashboard () { return 4;}
                    static get Bookmark () { return 5;}
                    static get PreferredLanguage () { return 6;}
                    static get AssetIdsForFleetMonitoring() { return 7;}
                    static get DataGridState() { return 8;}
                    static get EnableAlertNotification() { return 9;}
                };
            }

            static get Portal () {
                return class Portal {
                    static get BackOffice () { return 1;}
                    static get Company () { return 2;}
                    static get FleetService () { return 3;}
                    static get DispatcherService () { return 4;}
                };
            }

            static get PortalMode () {
                return class PortalMode {
                    static get Admin() { return 1;}
                    static get Workspace() { return 2;}
                };
            }

            static get PermissionCategory () {
                return class PermissionCategory {
                    static get Visibility () { return 1;}
                    static get Functionality () { return 2;}
                    static get Alert () { return 3;}
                };
            }

            static get EventType() {
                return class EventType {
                    static get Input() { return 1; }
                    static get Output() { return 2; }
                };
            }

            static get EventDataType() {
                return class EventDataType {
                    static get Digital() { return 1; }
                    static get Analog() { return 2; }
                    static get Text() { return 3; }
                };
            }

            static get BoxFeatureEventType () {
                return class BoxFeatureEventType {
                    static get InputEvent () { return 1;}
                    static get OutputEvent () { return 2;}
                    static get InputAccessory () { return 3;}
                    static get OutputAccessory () { return 4;}
                };
            }

            static get BoxStatus () {
                return class BoxStatus {
                    static get Idle () { return 1;}
                    static get InProgress () { return 2;}
                    static get Ready () { return 3;}
                    static get Maintenance () { return 4;}
                };
            }

            static get VehicleType () {
                return class VehicleType {
                    static get Car () { return 1;}
                    static get Truck () { return 2;}
                    static get Trailer () { return 3;}
                    static get Trail () { return 4;}
                    static get PassengerCar () { return 5;}
                    static get Ship () { return 6;}
                };
            }

            static get DriverLicenseType () {
                return class DriverLicenseType {
                    static get DLT_11 () { return 11;}
                    static get DLT_12 () { return 12;}
                    static get DLT_13 () { return 13;}
                    static get DLT_14 () { return 14;}
                    static get DLT_21 () { return 21;}
                    static get DLT_22 () { return 22;}
                    static get DLT_23 () { return 23;}
                    static get DLT_24 () { return 24;}
                    static get DLT_Other () { return -1;}
                };
            }

            static get AlertParkingType () {
                return class AlertParkingType {
                    static get Park () { return 1;}
                    static get ParkOverTime () { return 2;}
                };
            }

            static get AlertType () {
                return class AlertType {
                    static get AccRapid() { return 1;}
                    static get AcquiredGPS() { return 2;}
                    static get BadGPS() { return 3;}
                    static get VehicleBatteryLow() { return 4;}
                    static get DeviceBatteryLow() { return 5;}
                    static get DecRapid() { return 6;} 
                    static get EngineIdle() { return 7;}
                    static get EngineOff() { return 8;}
                    static get EngineOn() { return 9;}
                    static get GateClose() { return 10;}
                    static get GateOpen() { return 11;} 
                    static get GPSDisCnt() { return 12;}
                    static get GPSReCnt() { return 13;}
                    static get Move() { return 14;}
                    static get OverSpeed() { return 15;}
                    static get OverTemperature() { return 16;}
                    static get PowerDisCnt() { return 17;}
                    static get PowerReCnt() { return 18;}
                    static get Stop() { return 19;}
                    static get Swerve() { return 20;}
                    static get UnderTemperature() { return 21;}
                    static get UnsFuelDec() { return 22;}
                    static get CloseGateInsPOI() { return 24;}
                    static get CloseGateOtsPOI() { return 25;}
                    static get JobAvgSpeed() { return 26;}
                    static get JobTotDist() { return 27;}
                    static get JobTotTime() { return 28;}
                    static get OpenGateInsPOI() { return 29;}
                    static get OpenGateOtsPOI() { return 30;}
                    static get OverTimePark() { return 31;}
                    static get IncmWP() { return 32;}
                    static get OutgWP() { return 33;}
                    static get Dispatcher() { return 34;}
                    static get Urgent() { return 35;}
                    static get Maintenance() { return 36;}
                    static get SOS() { return 37;}
                    static get Parking() { return 38;}
                    static get POIParking() { return 39;}
                    static get POIInOut() { return 40;}
                    static get AreaParking() { return 41;}
                    static get AreaInOutVehicle() { return 42;}
                    static get AreaInOutJob() { return 43;}
                    static get MoveNonWorking() { return 44;}
                    static get ExceedLimitDriving() { return 45;}
                    static get InvalidDriver() { return 46;}
                    static get CustomAlert() { return 47;}
                    static get ExceedLimitDrivingIn24Hours() { return 48;}
                    static get TemperatureError() { return 49;}
                    static get Acceleration() {return 50;}
                    static get Breaking() {return 51;}
                    static get Turn() {return 52;}
                    static get WideTurn() {return 53;}
                    static get SwerveWhileAccelerating() {return 54;}
                    static get WideSwerveWhileAccelerating() {return 55;}
                    static get SwerveWhileDecelerating() {return 56;}
                    static get WideSwerveWhileDecelerating() {return 57;}
                    static get Roundabout() {return 58;}
                    static get LaneChange() {return 59;}
                    static get Bypassing() {return 60;}
                    static get SpeedBump() {return 61;}
                    static get AccidentSuspicious() {return 62;}
                    static get RealAccident() {return 63;}
                    static get GreenBandDriving() {return 64;}
                    static get OverRPM() {return 65;}
                    static get NoSeatBelt() {return 66;}
                    static get FCW() {return 67;}
                    static get UFCW() {return 68;}
                    static get PCW() {return 69;}
                    static get LDW() {return 70;}
                    static get HMW() {return 71;}
                    static get TSR() {return 72;}
                    static get TOW() {return 73;}
                    static get UnauthorizedStop() { return 74; }
                    static get FatigueOther() { return 75; }
                    static get FatigueMicroSleep() { return 76; }
                    static get FatigueYawning() { return 77; }
                    static get FatigueDrowsiness() { return 78; }
                    static get Distraction() { return 79; }
                    static get FieldOfViewException() { return 80; }
                    static get ExceedLimitDrivingVehicle() { return 81; }
                    static get FreeWheeling() { return 82; }
                };
            }

            static get ReportTemplateType () {
                return class ReportTemplateType {
                    static get Alert () { return 1;}
                    static get TrackMonitoring () { return 2;}
                    static get NearestAssets () { return 3;}
                    static get Tracking () { return 4;}
                    static get OverSpeed () { return 5;}
                    static get DecRapidDrive () { return 6;}
                    static get AccRapidDrive () { return 7;}
                    static get SwerveDrive () { return 8;}
                    static get Stoppage () { return 9;}
                    static get OpenCloseGate () { return 10;}
                    static get BadGPS () { return 11;}
                    static get FuelTemperature () { return 12;}
                    static get TemperatureSpeed () { return 13;}
                    static get FuelSpeed () { return 14;}
                    static get TemperatureGate () { return 15;}
                    static get SummaryReport () { return 16;}
                    static get OverSpeedSummary () { return 17;}
                    static get TravelDistanceTime () { return 18;}
                    static get POIParkingSummary () { return 19;}
                    static get OverTimeIdlingSummary () { return 20;}
                    static get DriverPerformanceSummary () { return 21;}
                    static get OverallSummary () { return 22;}
                    static get BackwardSummary () { return 23;}
                    static get DelinquentOverSpeed () { return 24;}
                    static get DelinquentDriveOver4Hours () { return 25;}
                    static get DelinquentDriveOver8Hours () { return 26;}
                    static get DelinquentAnonymousDriver () { return 27;}
                    static get DelinquentIncorrectDriverLicenseType () { return 28;}
                    static get DelinquentConnectionLost () { return 29;}
                    static get DailySummary () { return 30;}
                    static get MonthlySummary () { return 31;}
                    static get DelinquentDriveOver () { return 32;}
                    static get AssetMonitoring () { return 33;}
                };
            }

            static get SearchEntityType () {
                return class SearchEntityType {
                    static get Vehicle () { return 1;}
                    static get DeliveryMan () { return 2;}
                    static get Driver () { return 3;}
                    static get Job () { return 4;}
                };
            }

            static get ParkingType () {
                return class ParkingType {
                    static get InsidePOI () { return 1;}
                    static get OutsidePOI () { return 2;}
                    static get SpecifiedInsidePOI () { return 3;}
                };
            }

            static get MovementType () {
                return class MovementType {
                    static get Move () { return 1;}
                    static get Stop () { return 2;}
                    static get Park () { return 3;}
                    static get ParkEngineOn() { return 4; }
                    static get MoveOver35Hrs() { return 5; }
                    static get MoveOver4Hrs() { return 6; }
                };
            }

            static get DirectionType () {
                return class DirectionType {
                    static get N () { return 1;}
                    static get NE () { return 2;}
                    static get E () { return 3;}
                    static get SE () { return 4;}
                    static get S () { return 5;}
                    static get SW () { return 6;}
                    static get W () { return 7;}
                    static get NW () { return 8;}
                };
            }

            static get GpsStatus () {
                return class GpsStatus {
                    static get Bad () { return 1;}
                    static get Medium () { return 2;}
                    static get Good () { return 3;}
                };
            }

            static get GsmStatus () {
                return class GsmStatus {
                    static get Bad () { return 1;}
                    static get Medium () { return 2;}
                    static get Good () { return 3;}
                };
            }

            static get DltStatus () {
                return class DltStatus {
                    static get Success () { return 1;}
                    static get Fail () { return 2;}
                };
            }

            static get PlaybackDisplayType () {
                return class PlaybackDisplayType {
                    static get Moving () { return 1;}
                    static get AllTrackingTrails () { return 2;}
                    static get MovingAndTrails () { return 3;}
                };
            }

            static get PlaybackTrackingType () {
                return class PlaybackTrackingType {
                    static get AllTracking () { return 1;}
                    static get OnlyPark () { return 2;}
                };
            }

            static get DriveGrade () {
                return class DriveGrade {
                    static get F () { return 1;}
                    static get D () { return 2;}
                    static get C () { return 3;}
                    static get B () { return 4;}
                    static get A () { return 5;}
                };
            }

            static get NotificationAction () {
                return class NotificationAction {
                    static get Read () { return 1;}
                    static get Hide () { return 2;}
                };
            }

            static get MaintenanceStatus () {
                return class MaintenanceStatus {
                    static get Pending () { return 1;}
                    static get Completed () { return 2;}
                    static get Cancelled () { return 3;}
                    static get Overdue () { return 4;}
                    static get Inprogress () { return 5;}
                    static get Ready () { return 6;}
                };
            }

            static get GlobalSearchLogisticsCategory () {
                return class GlobalSearchLogisticsCategory {
                    static get BusinessUnit() { return 1;}
                    static get Vehicle() { return 2;}
                    static get CustomPoi() { return 3;}
                    static get CustomArea() { return 4;}
                    static get CustomRoute() { return 5;}
                    static get Driver() { return 6;}
                    static get User() { return 7;}
                };
            }

            static get GlobalSearchType () {
                return class GlobalSearchType {
                    static get Logistics() { return 1;}
                    static get Map() { return 2;}
                    static get AdminPoly() { return 3;}
                    static get Street() { return 4;}
                };
            }

            static get GeneralReportType () {
                return class GeneralReportType {
                    static get Tracking() { return 1;}
                    static get Driving() { return 2;}
                    static get Stoppage() { return 3;}
                    static get OpenCloseGate() { return 4;}
                    static get BadGPS() { return 5;}
                    static get OverSpeed() { return 6;}
                    static get TruckNotUpdateGPS() { return 7;}
                    static get ConcreteDischarge() { return 8;}
                    static get TrackingPEA() { return 9;}
                    static get EventDetail() {return 10;}
                    

                }
            }

            static get DrivingReportType () {
                return class DrivingReportType {
                    static get OverSpeed() { return 1;}
                    static get DecRapidDrive() { return 2;}
                    static get AccRapidDrive() { return 3;}
                    static get SwerveDrive() { return 4;}
                }
            }

            static get ComparisonReportType () {
                return class ComparisonReportType {
                    static get FuelTemperature() { return 1;}
                    static get TemperatureSpeed() { return 2;}
                    static get FuelSpeed() { return 3;}
                    static get TemperatureGate() { return 4;}
                }
            }

            static get SummaryReportType () {
                return class SummaryReportType {
                    static get SummaryReport() { return 1;}
                    static get OverSpeedSummary() { return 2;}
                    static get TravelDistanceTime() { return 3;}
                    static get POIParkingSummary() { return 4;}
                    static get OverTimeIdlingSummary() { return 5;}
                    static get DriverPerformanceSummary() { return 6;}
                    static get OverallSummary() { return 7;}
                    static get BackwardSummary() { return 8;}
                    static get MonthlySummary () { return 9;}
                    static get DailySummary () { return 10;}
                    static get DrivingSummaryReportTimeDuration () { return 11;}
                    static get VehicleUsageSummary () { return 12;}
                    static get ShipmentSummary() { return 13; }
                    static get ActivatingDrivingSummary() { return 14; }
                    static get WorkingTimeSummary() { return 15; }
                    static get RAGScore() { return 16; }
                    static get AdvanceScore() { return 17; }
                    
                }
            }

            static get DelinquentReportType () {
                return class DelinquentReportType {
                    static get DelinquentOverSpeed() { return 1;}
                    static get DelinquentDriveOverTime() { return 2;}
                    static get DelinquentAnonymousDriver() { return 3;}
                    static get DelinquentIncorrectDriverLicenseType() { return 4;}
                    static get DelinquentConnectionLost() { return 5;}
                    static get DelinquentRestBelow10Hours() { return 6;}
                }
            }

            static get BackwardDurationType () {
                return class BackwardDurationType {
                    static get PreviousSeventDays() { return 1;}
                    static get PreviousFourWeeks() { return 2;}
                    static get PreviousThreeMonths() { return 3;}
                }
            }

            static get AppointmentStatus () {
                return class AppointmentStatus {
                    static get Active() { return 1;}
                    static get Postpone() { return 2;}
                    static get Cancelled() { return 3;}
                    static get Completed() { return 4;}
                }
            }

            static get VehicleMaintenanceType () {
                return class VehicleMaintenanceType {
                    static get Plan() { return 1;}
                    static get Log() { return 2;}
                }
            }

            static get FuelType () {
                return class FuelType {
                    static get Percent() { return 1;}
                    static get Litre() { return 2;}
                }
            }

            static get EngineType () {
                return class EngineType {
                    static get All() { return 1;}
                    static get On() { return 2;}
                    static get Off() { return 3;}
                }
            }

            static get MaintenanceReportType () {
                return class MaintenanceReportType {
                    static get Appointment() { return 1;}
                    static get MaintenancePlan() { return 2;}
                }
            }
            

           

            static get GroupByType () {
                return class GroupByType {
                    static get BusinessUnit() { return 1;}
                    static get DateTime() { return 2;}
                    static get Driver() { return 3;}
                    static get Vehicle() { return 4;}
                    static get UserName() { return 5;}
                }
            }

            static get TicketStatus () {
                return class TicketStatus {
                    static get New () { return 1;}
                    static get Acknowledge () { return 2;}
                    static get Inprogress () { return 3;}
                    static get Confirm () { return 4;}
                    static get Follow () { return 5;}
                    static get Pending () { return 6;}
                    static get Close () { return 7;}
                };
            }

            static get TicketAppointmentStatus () {
                return class TicketAppointmentStatus {
                    static get New () { return 1;}
                    static get Appoint () { return 2;}
                    static get Inprogress () { return 3;}
                    static get Finish () { return 4;}
                    static get Pass () { return 5;}
                    static get Fail () { return 6;}
                    static get Follow () { return 7;}
                    static get Cancel () { return 8;}
                };
            }

            static get ServiceType () {
                return class ServiceType {
                    static get InstallNew () { return 1;}
                    static get InstallReplace () { return 2;}
                    static get UninstallReplace () { return 3;}
                    static get Repair () { return 4;}
                    static get Relocate () { return 5;}
                    static get UninstallGisc () { return 6;}
                    static get UninstallCustomer () { return 7;}
                    static get PM () { return 8;}
                    static get SIMPause () { return 9;}
                    static get SIMClose () { return 10;}
                    static get Demo () { return 11;}
                };
            }

            static get ERMVehicleType () {
                return class ERMVehicleType {
                    static get BusAutomatic () { return 1;}
                    static get FordF350F150 () { return 2;}
                    static get FullTrailerSemiAutomatic () { return 3;}
                    static get FullTrailerSemiManual () { return 4;}
                    static get InternationalTruck () { return 5;}
                    static get International6Wheels () { return 6;}
                    static get MidiBusAutomatic () { return 7;}
                    static get MiniBusAutomatic () { return 8;}
                    static get MiniBusManual () { return 9;}
                    static get MiniBus6WheelsAutomatic () { return 10;}
                    static get MiniBus6WheelsManual () { return 11;}
                    static get OffRoadCarAutomatic () { return 12;}
                    static get OffRoadCarManual () { return 13;}
                    static get OffRoadCarGeneric () { return 14;}
                    static get PrivateCarAbove1800Hp () { return 15;}
                    static get PrivateCarAutomatic () { return 16;}
                    static get PrivateCarManual () { return 17;}
                    static get SmallPrivateCarAutomatic () { return 18;}
                    static get SmallPrivateCarManual () { return 19;}
                    static get SmallTruckAutomatic () { return 20;}
                    static get SmallTruckManual () { return 21;}
                    static get SportsCar () { return 22;}
                    static get TruckManual () { return 23;}
                    static get ManualSettings () { return 99;}
                    static get InternalESafeIsDisabled () { return 254;}
                    static get DefaultSettings () { return 255;}
                }
            }
            static get TicketDuration () {
                return class TicketDuration {
                    static get SevenDay () { return 1;}
                    static get FifteenDay () { return 2;}
                    static get ThirtyDay () { return 3;}
                    static get MoreThanThirtyDay () { return 4;}
                    static get FortyFiveDay () { return 5;}
                };
            }

            static get SimStatus () {
                return class SimStatus {
                    static get New () { return 1;}
                    static get Active () { return 2;}
                    static get InUse () { return 3;}
                    static get Pause () { return 4;}
                    static get PrepareToClose () { return 5;}
                    static get Close () { return 6;}
                };
            }

            static get CategoryType () {
                return class CategoryType {
                    static get Vehicle () { return 1;}
                    static get ShippingType () { return 2;}
                };
            }

            static get ManeuverLevel(){
                return class ManeuverLevel {
                    static get Normal () { return 1;}
                    static get Aggressive () { return 2;}
                    static get Dangerous () { return 3;}
                };
            }

            static get AlertTicketStatus () {
                return class TicketStatus {
                    static get New () { return 1;}
                    static get Inprogress () { return 2;}
                    static get Close () { return 3;}
                };
            }

            static get JobAutoFinish() {
                return class JobAutoFinish {
                    static get Midnight() { return 1; }
                    static get AfterPlanEnd() { return 2; }
                    static get Manual() { return 3; }
                };
            }

            static get LastShipmentRole() {
                return class LateShipmentRole {
                    static get WaypointArrival() { return 1; }
                    static get ShipmentEndTime() { return 2; }
                };
            }


            static get PoiCategoryType() {
                return class PoiCategoryType {
                    static get DC() { return 1; }
                    static get CustomerPoi() { return 2; }
                    static get UnauthorizedParking() { return 3; }
                };
            }

            static get MDVRModel() {
                return class MDVRModel {
                    static get All() { return 1; }
                    static get HIKvision() { return 2; }
                    static get IQTech() { return 3; }
                    static get DFI() { return 4; }

                };
            }

            static get JobArrivalType ()
            {
                return class JobArrivalType {
                    static get Move() { return 1; }
                    static get Stop() { return 2; }
                    static get Park() { return 3; }
                };
            }

            static get AlertDisplayType ()
            {
                return class AlertDisplayType {
                    static get Video() { return 1; }
                    static get Picture() { return 2; }
                };
            }

            static get SimType ()
            {
                return class SimType {
                    static get Box() { return 1; }
                    static get MDVR() { return 2; }
                    static get DMS() { return 3; }
                    static get ADAS() { return 4; }
                };
            }

            static get TravelMode ()
            {
                return class TravelMode {
                    static get Inbound() { return 1; }
                    static get Outbound() { return 2; }
                };
            }

            static get BOMPeriod ()
            {
                return class BOMPeriod {
                    static get BeforeNoon() { return 1; }
                    static get AfterNoon() { return 2; }
                };
            }

            static get RealtimePassengerStatus ()
            {
                return class RealtimePassengerStatus {
                    static get Late() { return 1; }
                    static get MaybeLate() { return 2; }
                    static get OnTime() { return 3; }
                };
            }

            static get AccessType ()
            {
                return class AccessType {
                    static get Arrival() { return 1; }
                    static get Depart() { return 2; }
                };
            }

            static get IntegrationType() {
                return class IntegrationType {
                    static get Guardian() { return 1; }
                };
            }

            static get PermissionType() {
                return class PermissionType {
                    static get Public() { return 1; }
                    static get Private() { return 2; }
                };
            }

            static get PoiType() {
                return class PoiType {
                    static get Public() { return 1; }
                    static get Private() { return 2; }
                    static get Shared() { return 3; }
                };
            }

            static get LocationDescriptionColor(){
                return class LocationDescriptionColor {
                    static get Black() { return 1; }
                    static get Blue() { return 2; }
                    static get Green() { return 3; }
                };
            }

            static get EndTripCriteria() {
                return class EndTripCriteria {
                    static get POItoPOI() { return 1; }
                    static get Parking() { return 2; }
                    static get EngineOnOff() { return 3; }
                    static get DriverLogInOut() { return 4; }
                };
            }

            static get DLTReportType() {
                return class DLTReportType {
                    static get NewInstallation() { return 1; }
                    static get NextYear() { return 2; }
                    static get Letter() { return 3; }
                };
            }

            static get StockType() {
                return class StockType {
                    static get BoxStock() { return 1; }
                    static get SimStock() { return 2; }
                };
            }

            static get DisplayMode() {
                return class DisplayMode {
                    static get Full() { return 1; }
                    static get Overview() { return 2; }
                    static get OverviewWithAlert() { return 3; }
                };
            }

            static get TurnOption() {
                return class TurnOption {
                    static get Both() { return 1; }
                    static get TurnRight() { return 2; }
                    static get TurnLeft() { return 3; }
                };
            }
        };
    }

    

    static get UserType () {
        return class UserType {
            static get BackOffice () { return 1;}
            static get Company () { return 2;}
        };
    }

    static get AlertNotificationType () {
        return class AlertNotificationType {
            static get Web () { return 1;}
            static get Email () { return 2;}
            static get Sms () { return 3;}
            static get Interface () { return 4;}
        };
    }

    static get AuthenticationType() {
        return class AuthenticationType {
            static get Form () { return 1; }
            static get ActiveDirectory() { return 2; }
            static get PEAIdm() { return 3;}
        };
    }

    static get InfoStatus () {
        return class InfoStatus {
            static get Original () { return 1;}
            static get Add () { return 2;}
            static get Update () { return 3;}
            static get Delete () { return 4;}
        };
    }

    static get SortingDirection () {
        return class SortingDirection {
            static get Ascending () { return 1;}
            static get Descending () { return 2;}
        };
    }

    static get SortingColumnName () {
        return class SortingColumnName {
            static get Id () { return 1;}
            static get Name () { return 2;}
            static get StartDate () { return 3;}
            static get EndDate () { return 4;}
            static get FullName () { return 5;}
            static get Email () { return 6;}
            static get Group () { return 7;}
            static get Username () { return 8;}
            static get BusinessUnitName () { return 9;}
            static get Enable () { return 10;}
            static get Code () { return 11;}
            static get EventType () { return 12;}
            static get EventDataType () { return 13;}
            static get IsAccessory () { return 14;}
            static get IsCustomAlert () { return 15;}
            static get Brand () { return 16;}
            static get MaxInputEvent () { return 17;}
            static get MaxOutputEvent () { return 18;}
            static get MaxAccessories () { return 19;}
            static get Description () { return 20;}
            static get CreateDate () { return 21;}
            //static get ?? () { return 22;}
            static get AlertType () { return 23;}
            static get AssetId () { return 24;}
            static get VehicleLicense () { return 25;}
            static get VehicleReferenceNo () { return 26;}
            static get Movement () { return 27;}
            static get DirectionType () { return 28;}
            static get TotalAlerts () { return 29;}
            static get DriverEmployeeId () { return 30;}
            static get DriverName () { return 31;}
            static get JobCode () { return 32;}
            static get AlertDateTime () { return 33;}
            static get DateTime () { return 34;}
            static get PoiIconName () { return 35;}
            static get Radius () { return 36;}
            static get Imei () { return 37;}
            static get SerialNo () { return 38;}
            static get CompanyName () { return 39;}
            static get BoxStatus () { return 40;}
            static get BoxTemplateName () { return 41;}
            static get IsForRent () { return 42;}
            static get BoxStockName () { return 43;}
            static get VendorName () { return 44;}
            static get Model () { return 45;}
            static get ReferenceNo () { return 46;}

            static get License () { return 47;}
            static get VehicleType () { return 48;}
            static get Mobile () { return 49;}
            static get BoxSerialNo () { return 50;}

            static get VendorId () { return 51;}

            static get Speed () { return 52;}
            static get GpsStatus () { return 53;}
            static get GsmStatus () { return 54;}
            static get JobStatus () { return 55;}

            static get EmployeeId () { return 56;}
            static get FirstName () { return 57;}
            static get LastName () { return 58;}
            static get BusinessUnit () { return 59;}

            static get Operator () { return 60;}
            static get Promotion () { return 61;}

            static get Order () { return 62;}

            static get Apn () { return 63;}
            static get ApnUsername () { return 64;}
            static get OrApnPasswordder () { return 65;}

            static get LimitRate () { return 66;}
            static get MaxItems () { return 67;}

            static get AlertTypeId () { return 68;}

            //DatabaseServer
            //DatabaseConnection
            static get ServerName () { return 69;}
            static get TrackingDatabaseName () { return 70;}
            static get TotalCompanies () { return 71;}
            static get TotalBoxes () { return 72;}
            static get DatabaseServerName () { return 73;}

            // DriveRule
            static get TotalItems () { return 74;}

            //VehicleMaintenancePlan
            static get MaintenanceDate () { return 75;}
            static get MaintenanceDistance () { return 76;}
            static get Title () { return 77;}
            static get MaintenanceStatus () { return 78;}

            static get Altitude () { return 79;}
            static get DltStatus () { return 80;}

            //BoxMainenancePlan
            static get Author () { return 81;}

            static get IsDelay () { return 82;}

            //Vehicle
            static get FormatEnable () { return 83;}
        };
    }

    static get UserGroup () {
        return class UserGroup {
            static get SystemAdmin () { return 1;}
            static get Administrator () { return 2;}
            static get Executive () { return 3;}
            static get Sales () { return 4;}
            static get Technician () { return 5;}
        };
    }

    static get DateTimePattern () {
        return class DateTimePattern {
            static get ShortDate () { return 1;}
            static get ShortTime () { return 2;}
            static get LongDate () { return 3;}
            static get LongTime () { return 4;}
        };
    }

    static get TemplateFileType () {
        return class TemplateFileType {
            static get Xls () { return 1;}
            static get Xlsx () { return 2;}
            static get Zip () { return 3;}
        };
    }
    static get JobStatus() {
        return class JobStatus {
            static get Unassigned() { return 1; }
            static get Waiting() { return 2; }
            static get Start() { return 3; }
            static get OnTheWay() { return 4; }
            static get InsideWaypoint() { return 5; }
            static get GoingToTerminal() { return 6; }
            static get InsideTerminal() { return 7; }
            static get Finished() { return 8; }
            static get FinishedLate() { return 9; }
            static get FinishedIncomplete() { return 10; }
            static get FinishedLateIncomplete() { return 11; }
            static get Cancelled() { return 12; }
            static get Pending() { return 13; }
            static get PrepareToFinish() { return 14; }
            static get PrepareToStart() { return 15; }
        }
    }
    static get JobWaypointStatus() {
        return class JobWaypointStatus {
            static get Waiting() { return 1; }
            static get InsideWaypoint() { return 2; }
            static get Loading() { return 3; }
            static get Unloading() { return 4; }
            static get Holding() { return 5; }
            static get Finished() { return 6; }
            static get FinishedLate() { return 7; }
            static get FinishedIncomplete() { return 8; }
            static get FinishedLateIncomplete() { return 9; }
            static get Cancelled() { return 10; }
        }
    }

    static get DistanceUnit() {
        return class DistanceUnit {
            static get Metre() { return 1; }
            static get Kilometre() { return 2; }
            static get Mile() { return 3; }
        }
    }

    static get AreaUnit() {
        return class AreaUnit {
            static get SquareMetres() { return 1; }
            static get SquareKilometres() { return 2; }
            static get ThailandUnit() { return 3; }
        }
    }

    static get RouteMode() {
        return class RouteMode {
            static get Car() { return 1; }
            static get Motorcycle() { return 2; }
            static get Pedestrain() { return 3; }
            static get Truck() { return 4; }
        }
    }

    static get AssetFeatures() {
        return class AssetFeatures {
            static get AssetReferenceNo () { return "TrkVwAssetReferenceNo";}  // รหัสรถ
            static get DeliveryMan () { return "TrkVwDeliveryMan";}         // คนสางของ
            static get DriverName () { return "TrkVwDriverName";}         // คนขับ
            static get CoDriverName () { return "TrkVwCoDriver";}         // ผู้ช่วยขับ
            static get BusinessUnit () { return "TrkVwBusinessUnit";}            // แผนก
            static get Job () { return "TrkVwJob";}                       // งาน
            static get Engine () { return "TrkVwEngine";}              // สถานะเครื่องยนต์
            static get Gate () { return "TrkVwGate";}                  // สถานะประตู1
            static get Fuel () { return "TrkVwFuel";}                  // ระดับน้ำมัน
            static get Temp () { return "TrkVwTemp";}                  // อุณหภูมิ1
            static get SOS () { return "TrkVwSOS";}                    // SOS
            static get POWER () { return "TrkVwPower";}                // Power
            static get GarminFMI () { return "TrkVwGarminFMI";}        // GarminFMI
            static get CardReader () { return "TrkVwCardReader";}      // CardReader
            static get Camera () { return "TrkVwCamera";}              // Camera
            static get TotalEvent () { return "TrkVwTotalEvent";}                // ข้อมูลแจ้งเตือน
            static get Distance () { return "TrkVwDistance";}          // ระยะทาง
            static get Altitude () { return "TrkVwAltitude";}           // Altitude
            static get DLTStatus () { return "TrkVwDLTStatus";}         // DLT Status
            static get CustomFeature () { return "TrkVw<FeatureCode>";}  // Featurenames
            static get VehicleBattery  () { return "TrkVwVehicleBattery";}  // ระดับแบตเตอรี่รถ
            static get DeviceBattery   () { return "TrkVwDeviceBattery";}  // ระดับแบตเตอรี่อุปกรณ์
            static get DriverCardReader    () { return "TrkVwDRVCardReader";}  // DriverCardReader
            static get PassengerCardReader    () { return "TrkVwPSGRCardReader";}  // PassengerCardReader
            static get Mileage    () { return "TrkVwMileage";}  // Mileage
            static get Poi    () { return "TrkVwPoi";}  // POI
        }
    }

    
    static get JobTimelineSort () {
        return class JobTimelineSort
        {
            static get TruckStatus () { return 1;}
            static get License () { return 2;}
            static get Shipment () { return 3;}
            
        };
    }

    static get JobTimelineTruckStatus () {
        return class JobTimelineTruckStatus
        {
            static get Delay () { return 1;}
            static get MaybeDelay () { return 2;}
            static get InsideWayopint () { return 3;}
            static get OnTime () { return 4;}
            static get Plan () { return 5;}
            static get Finish () { return 6;}
            
        };
    }

   
}

// Gisc.Csd.Log.Domain.Common.Constant.EntityAssociation (completed)
export class EntityAssociation {
    static get Group () {
        return class Group {
            static get GroupPermissions () { return "Group.GroupPermissions";}
        };
    }

    static get TechnicianTeam() {
        return class TechnicianTeam {
            static get Member () { return "TechnicianTeam.Member";}
            static get TeamLeader () { return "TechnicianTeam.TeamLeader";}
            static get TeamMember () { return "TechnicianTeam.TeamMember";}
            
        };
    }

    static get Permission () {
        return class Permission {
            static get ChildPermissions () { return "Permission.ChildPermissions";}
        };
    }

    static get Company () {
        return class Company {
            static get Logo () { return "Company.Logo";}
            static get Address () { return "Company.Address";}
            static get Setting () { return "Company.Setting";}
            static get Contacts () { return "Company.Contacts";}
            static get Subscriptions () { return "Company.Subscriptions";}
            static get MapServices () { return "Company.MapServices";}
            static get PairableSetting () { return "Company.PairableSetting";}
        };
    }

    static get User() {
        return class User {
            static get UserPreferences () { return "User.UserPreferences";}
            static get BusinessUnit () { return "User.BusinessUnit";}
            static get Groups () { return "User.Groups";}
            static get AccessibleCompanies () { return "User.AccessibleCompanies";}
            static get AccessibleBusinessUnits () { return "User.AccessibleBusinessUnits";}
            static get IsDeliveryMan () { return "User.IsDeliveryMan";}
            static get Translation () { return "User.Translation";}
            static get Image () { return "User.Image";}
        };
    }

    static get Subscription () {
        return class SubScription {
            static get Module () { return "Subscription.Module";}
        };
    }



    static get BoxPackageType () {
        return class BoxPackageType {
            static get Items () { return "BoxPackageType.Items";}
        };
    }

    static get BoxModel () {
        return class BoxModel {
            static get Image () { return "BoxModel.Image";}
            static get PackageType () { return "BoxModel.PackageType";}
            static get Attachment () { return "BoxModel.Attachment";}
        };
    }

    static get BoxTemplate () {
        return class BoxTemplate {
            static get Features () { return "BoxTemplate.Features";}
        };
    }

    static get Box () {
        return class Box {
            static get Vendor () { return "Box.Vendor";}
            static get BusinessUnit () { return "Box.BusinessUnit";}
            static get Features () { return "Box.Features";}
        };
    }

    static get BusinessUnit () {
        return class BusinessUnit {
            static get ChildBusinessUnits () { return "BusinessUnit.ChildBusinessUnits";}
            static get ResourceSummary () { return "BusinessUnit.ResourceSummary";}
            static get ParentBusinessUnit () { return "BusinessUnit.ParentBusinessUnit";}
        };
    }

    static get PoiIcon () {
        return class PoiIcon {
            static get Image () { return "PoiIcon.Image";}
        };
    }

    static get VehicleIcon () {
        return class VehicleIcon {
            static get PreviewImage () { return "VehicleIcon.PreviewImage";}
            static get Images () { return "VehicleIcon.Images";}
            static get AccessibleCompanies () { return "VehicleIcon.AccessibleCompanies";}
        };
    }

    static get ReportTemplate () {
        return class ReportTemplate {
            static get ReportTemplateFields () { return "ReportTemplate.ReportTemplateFields";}
        };
    }

    static get ReportTemplateField () {
        return class ReportTemplateField {
            static get Feature () { return "ReportTemplateField.Feature";}
        };
    }

    static get Vehicle () {
        return class Vehicle {
            static get Images () { return "Vehicle.Images";}
            static get Icon () { return "Vehicle.Icon";}
            static get Trail () { return "Vehicle.Trail";}
            static get DefaultDriver () { return "Vehicle.DefaultDriver";}
            static get CoDriver1 () { return "Vehicle.CoDriver1";}
            static get CoDriver2 () { return "Vehicle.CoDriver2";}
            static get AlertConfigurations () { return "Vehicle.AlertConfigurations";}
            static get VehicleRegisteredType () { return "Vehicle.VehicleRegisteredType";}
            static get Model () { return "Vehicle.Model";}
            static get License () { return "Vehicle.License";}
            static get InverseFeatures () { return "Vehicle.InverseFeatures";}
        };
    }

    static get VehicleMaintenancePlan () {
        return class VehicleMaintenancePlan {
            static get CurrentDistance () { return "VehicleMaintenancePlan.CurrentDistance";}
        };
    }

    static get VehicleMaintenancePlanAlert () {
        return class VehicleMaintenancePlanAlert {
            static get Messages () { return "VehicleMaintenancePlanAlert.Messages";}
        };
    }

    static get Driver () {
        return class Driver {
            static get Image () { return "Driver.Image";}
            static get BusinessUnit () { return "Driver.BusinessUnit";}
            static get User () { return "Driver.User";}
        };
    }

    static get Trainer () {
        return class Trainer {
            static get Image () { return "Trainer.Image";}
            static get BusinessUnit () { return "Trainer.BusinessUnit";}
        };
    }

    static get FleetService () {
        return class FleetService {
            static get BusinessUnit () { return "FleetService.BusinessUnit";}
            static get Promotion () { return "FleetService.Promotion";}
            static get Box () { return "FleetService.Box";}
            static get Vehicle () { return "FleetService.Vehicle";}
        };
    }

    static get MobileService () {
        return class MobileService {
            static get BusinessUnit () { return "MobileService.BusinessUnit";}
            static get Promotion () { return "MobileService.Promotion";}
            static get User () { return "MobileService.User";}
        };
    }

    static get AlertConfiguration () {
        return class AlertConfiguration {
            static get Vehicles () { return "AlertConfiguration.Vehicles";}
            static get DeliveryMen () { return "AlertConfiguration.DeliveryMen";}
            static get CustomPOICategory () { return "AlertConfiguration.CustomPOICategory";}
            static get CustomPOI () { return "AlertConfiguration.CustomPOI";}
            static get CustomAreaType () { return "AlertConfiguration.CustomAreaType";}
            static get CustomAreaCategory () { return "AlertConfiguration.CustomAreaCategory";}
            static get CustomArea () { return "AlertConfiguration.CustomArea";}
            static get Feature () { return "AlertConfiguration.Feature";}
        };
    }

    static get Alert () {
        return class Alert {
            static get BusinessUnit () { return "Alert.BusinessUnit";}
            static get Messages () { return "Alert.Messages";}
            static get Vehicle () { return "Alert.Vehicle";}
            static get DeliveryMan () { return "Alert.DeliveryMan";}
            static get Driver () { return "Alert.Driver";}
            static get CoDrivers () { return "Alert.CoDrivers";}
            static get FeatureValues () { return "Alert.FeatureValues";}
        };
    }

    static get TrackLocation () {
        return class TrackLocation {
            static get BusinessUnit () { return "TrackLocation.BusinessUnit";}
            static get CoDrivers () { return "TrackLocation.CoDrivers";}
            static get FeatureValues () { return "TrackLocation.FeatureValues";}
        };
    }

    static get Announcement () { 
        return class Announcement {
            static get AccessibleCompanies () { return "Announcement.AccessibleCompanies";}
            static get Messages () { return "Announcement.Messages";}
        };
    }

    static get DriverPerformanceRule () {
        return class DriverPerformanceRule {
            static get DriveRuleRegulations () { return "DriverPerformanceRule.DriveRuleRegulations";}
        };
    }

    static get CustomPoi () {
        return class CustomPoi {
            static get Category () { return "CustomPoi.Category";}
            static get Icon () { return "CustomPoi.Icon";}
            static get AccessibleBusinessUnits () { return "CustomPoi.AccessibleBusinessUnits";}
            static get Country () { return "CustomPoi.Country";}
            static get Province () { return "CustomPoi.Province";}
            static get City () { return "CustomPoi.City";}
            static get Town () { return "CustomPoi.Town";}
        };
    }

    static get CustomRoute () {
        return class CustomRoute {
            static get Category () { return "CustomRoute.Category";}
            static get RouteOption () { return "CustomRoute.RouteOption";}
            static get ViaPoints () { return "CustomRoute.ViaPoints";}
            static get AccessibleBusinessUnits () { return "CustomRoute.AccessibleBusinessUnits";}
        };
    }

    static get CustomArea () {
        return class CustomArea {
            static get Category () { return "CustomArea.Category";}
            static get Type () { return "CustomArea.Type";}
            static get AccessibleBusinessUnits () { return "CustomArea.AccessibleBusinessUnits";}
        };
    }

    static get DatabaseConnection () {
        return class DatabaseConnection {
            static get Companies () { return "DatabaseConnection.Companies";}
        };
    }

    static get MaintenanceSubType () {
        return class MaintenanceSubType {
            static get Checklist () { return "MaintenanceSubType.Checklist"; }
        }
    }
}
