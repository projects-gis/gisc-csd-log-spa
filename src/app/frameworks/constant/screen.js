// Constant for class derived from ScreenBase
export const SCREEN_MODE = "mode";
export const SCREEN_MODE_CREATE = "create";
export const SCREEN_MODE_UPDATE = "update";
export const SCREEN_MODE_READONLY = "readonly";