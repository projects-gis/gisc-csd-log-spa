import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./reportViewer.html";
import "kendo.all.min";
import "./telerikReportViewer-11.1.17.503.min.js";
import ScreenHelper from "../../../portals/screenhelper";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";

/**
 * Telerik report integration.
 * 
 * @class ReportViewerControl
 * @extends {ControlBase}
 */
class ReportViewerControl extends ControlBase {
    /**
     * Creates an instance of ReportViewerControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.reportUrl = this.ensureNonObservable(params.reportUrl, "");
        this.reportSource = this.ensureNonObservable(params.reportSource, {
            // The report can be set to a report file name (.trdx or .trdp report definition)
            // or CLR type name (report class definition).
            report: "",

            // Parameters name value dictionary
            parameters: {}
        });
        this.reportExport = this.ensureNonObservable(params.reportExport, {});


    }

    ajaxRequest(url, includeHeader = true) {
        var ajaxOption  = {
            cache: false,
            url: url,
            type: "POST",
            data: this.reportSource.parameters,
            crossDomain: true,
            xhrFields: { // Force jQuery send cookie request.
                withCredentials: true
            }
        };

        if (includeHeader) {
            ajaxOption.headers = this._getHeaders();
        }

        // jQuery AJAX response is array with 3 objects, 
        // 1. the actual response object
        // 2. response status
        // 3. XHR object.
        // We need only 1, so create Deferred to handle it manually.
        var dfd = $.Deferred();

        $.ajax(ajaxOption).done((r) => {
            dfd.resolve(r);
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Get header for ajax request
     * 
     * @returns
     */
    _getHeaders (){
        var ajaxHeader = {};

        switch(WebConfig.userSession.currentPortal) {
            case "back-office":
                // For back office portal usage do not send any language.
                break;

            case "company-admin":
            case "company-workspace":
                // For company portl usage always send languages of user, company.
                if(!_.isNil(WebConfig.userSession.currentUserLanguage)) {
                    ajaxHeader["GISC-UserLanguage"] = WebConfig.userSession.currentUserLanguage;
                }
                if(!_.isNil(WebConfig.companySettings.languageCode)) {
                    ajaxHeader["GISC-CompanyLanguage"] = WebConfig.companySettings.languageCode;
                }
                if(!_.isNil(WebConfig.userSession.currentCompanyId)) {
                    ajaxHeader["GISC-CompanyId"] = WebConfig.userSession.currentCompanyId;
                }
                break;
        }
        return ajaxHeader;
    }

    /**
     * Initialize report viewer when dom is ready.
     * 
     * @memberOf ReportViewerControl
     */
    onDomReady() {
        var firstLoad = true;
        $("#" + this.id).telerik_ReportViewer({
            // The URL of the service which will serve reports.
            // The URL corresponds to the name of the controller class (ReportsController).
            // For more information on how to configure the service please check http://www.telerik.com/help/reporting/telerik-reporting-rest-conception.html.
            serviceUrl: this.reportUrl,

            // The URL for the report viewer template. The template can be edited -
            // new functionalities can be added and unneeded ones can be removed.
            // For more information please check http://www.telerik.com/help/reporting/html5-report-viewer-templates.html.
            templateUrl: window.location.origin + '/report-viewer/templates/telerikReportViewerTemplate-Logistics.html',

            //ReportSource - report description
            reportSource: this.reportSource,

            // Specifies whether the viewer is in interactive or print preview mode.
            // PRINT_PREVIEW - Displays the paginated report as if it is printed on paper. Interactivity is not enabled.
            // INTERACTIVE - Displays the report in its original width and height without paging. Additionally interactivity is enabled.
            viewMode: telerikReportViewer.ViewModes.INTERACTIVE,

            // Sets the scale mode of the viewer.
            // Three modes exist currently:
            // FIT_PAGE - The whole report will fit on the page (will zoom in or out), regardless of its width and height.
            // FIT_PAGE_WIDTH - The report will be zoomed in or out so that the width of the screen and the width of the report match.
            // SPECIFIC - Uses the scale to zoom in and out the report.
            scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,

            // Zoom in and out the report using the scale
            // 1.0 is equal to 100%, i.e. the original size of the report
            scale: 1.0,

        }).on('click', "#telerik_ReportViewer_export_CSV", (e) => {
            var el = $(e.target).parents();
            var r = el.find("#" + this.id);
            var m = r.find(".trv-pages-area[data-role='telerik_ReportViewer_PagesArea']");
            var rv = r.data("telerik_ReportViewer");

            if (_.size(this.reportExport)) {
                // disable button export
                rv.commands.export.enabled(false);
                // add class for show text message
                m.addClass("error")
                    .find(".trv-error-message")
                    .text(window.telerikReportViewer.sr.preparingDownload);
                if (this.reportExport.CSV) {
                    this.ajaxRequest(this.reportExport.CSV).done((response) => {
                        //download file
                        ScreenHelper.downloadFile(response.fileUrl);
                        // enable button export
                        rv.commands.export.enabled(true);
                        // remove class for hide text message 
                        m.removeClass("error");
                    }).fail((o) => {
                        m.find(".trv-error-message")
                            .text(o.responseText);
                        rv.commands.export.enabled(true);
                    });
                } else {
                    // enable button export
                    rv.commands.export.enabled(true);
                    // remove class for hide text message 
                    m.removeClass("error");
                }

            }


            e.preventDefault();

        }).on('mouseover', "a[data-command='telerik_ReportViewer_export']", (e) => {

            if (firstLoad) {
                $("#telerik_ReportViewer_export_PDF").hide();
                $("#telerik_ReportViewer_export_Excel").hide();
                $("#telerik_ReportViewer_export_DOCX").hide();
                $("#telerik_ReportViewer_export_CSV").hide();

                firstLoad = false;
            }
            
           
            if (_.size(this.reportExport)) {

                if (this.reportExport.PDF !== false) {
                    $("#telerik_ReportViewer_export_PDF").show();
                }

                if (this.reportExport.Excel !== false) {
                    $("#telerik_ReportViewer_export_Excel").show();
                }

                if (this.reportExport.Word !== false) {
                    $("#telerik_ReportViewer_export_DOCX").show();
                }
                if (this.reportExport.CSV !== false) {
                    $("#telerik_ReportViewer_export_CSV").show();
                }

            } else {
                //$("#telerik_ReportViewer_export_CSV").hide();
                $("#telerik_ReportViewer_export_PDF").show();
                $("#telerik_ReportViewer_export_Excel").show();
                $("#telerik_ReportViewer_export_DOCX").show();
            }

            e.preventDefault();
        });

        //$("#telerik_ReportViewer_export_PDF").hide();
       // $("#telerik_ReportViewer_export_Excel").hide();
        //$("#telerik_ReportViewer_export_DOCX").hide();

        

    }

    onUnload () {
        // http://www.telerik.com/forums/html-5-report-viewer-destroy
        // The viewer does not have a destroy method. Please consider that all viewers loaded on a page must have unique IDs. On reloading the viewer, all requests to the Reporting REST service will re-executed.
        // We will appreciate it if you let us know how it goes, and in case there is an issue open a support ticket with a demo project to check your settings.
        // Regards,
        // Stef 
        // Telerik
    }
}

export default {
    viewModel: ReportViewerControl,
    template: templateMarkup
};