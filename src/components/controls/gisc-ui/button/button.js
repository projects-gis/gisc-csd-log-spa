import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./button.html";
import Logger from "../../../../app/frameworks/core/logger";

/**
 * 
 * 
 * @class Button
 * @extends {ControlBase}
 */
class Button extends ControlBase {
    /**
     * Creates an instance of Button.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        /**
         * Check if onclick is binding
         */
        if (params.onclick) {
            this.onclick = params.onclick;
        } else {
            this.onclick = () => {
                Logger.warn("Please implement onclick");
            };
        }
        this.text = this.ensureObservable(params.text);
        this.type = this.ensureNonObservable(params.type, 'default');

    }

    onLoad() {}
    onUnload() {
        this.text = null;
    }
}

export default {
    viewModel: Button,
    template: templateMarkup
};