﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./stackChart.html";
import ChartJs from "chart";
import Logger from "../../../../app/frameworks/core/logger";

class BarChart extends ControlBase {
    constructor(params) {
        super(params);

        ChartJs.defaults.global.defaultFontFamily = "dbhx";
        ChartJs.defaults.global.defaultFontSize = 16;

        this.data = this.ensureObservable(params.data);
        this.type = this.ensureNonObservable(params.type, "bar");
        this.selected = this.ensureFunction(params.selected);
        
        this.chartInstance = null;

        this.isArrayChange = this.data.subscribe(function (changes) {
            this.clickData = changes.options.bindingData;
            this.createChart(changes);
        }, this);
    }

    onChartClick(evt) {   
         var activePoint = this.chartInstance.getElementAtEvent(evt);
         if (activePoint.length === 0) {
             Logger.warn("Please click on segment.");
         } else {
             return this.selected ? this.selected(activePoint[0]._model) : null;
         }
    }

    afterRender(element) {
        this.createChart(this.data());
    }

    onUnload() {
        this.chartInstance.destroy();
        this.data = null;
    }

    createChart(val) {
        var self = this;
        var ctx = $("#" + self.id);
        try {
            if (!this.chartInstance) {
                this.chartInstance = new ChartJs(ctx, {
                    type: this.type,
                    data: {
                        labels: [],
                        datasets: [{
                            label: [],
                            backbroundColor: [],
                            data: []
                        }]
                    },
                    options: {  
                        maintainAspectRatio: false,
                        legend: {
                            display: true,
                            position: val.data.data.labels[0].indexOf("Close") !=-1 ? "right" : "right",
                            labels:{
                                    usePointStyle: true,
                                    fontSize: 13,
                                    padding: 20}
                        },
                        tooltips: {
                            cornerRadius: 1,
                            backgroundColor: "#333333",
                            bodyFontFamily: "dbhx",
                            bodyFontColor: "rgb(255,209,23)",
                            yPadding: 8,
                        },
                        title: {
                            display: true,
                            fontSize: 25,
                            text: ""
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value, index, values) {
                                        if (Math.floor(value) === value) { // Check ว่าเป็น int หรือเปล่า
                                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else if ($.type(value) === "string") { // Check ว่าเป็น String หรือเปล่า
                                            return value;
                                        }
                                    }
                                },
                                scaleLabel: {
                                    display: val.options.yAxes ? true : false,
                                    labelString: val.options.yAxes ? val.options.yAxes.title : "",
                                    fontColor: 'rgb(70, 183, 143)',
                                },
                                stacked: true
                            }],
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value, index, values) {
                                        if (Math.floor(value) === value) { // Check ว่าเป็น int หรือเปล่า
                                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else if ($.type(value) === "string") { // Check ว่าเป็น String หรือเปล่า
                                            return value;
                                        }
                                    }
                                },
                                scaleLabel: {
                                    display: val.options.xAxes ? true : false,
                                    labelString: val.options.xAxes ? val.options.xAxes.title : "",
                                    fontColor: 'rgb(70, 183, 143)'
                                },
                                stacked: true
                            }]
                        }
                    }
                });
            }
            this.chartInstance.data.labels = val.data.data.labels;
            this.chartInstance.data.datasets = val.data.data.datasets;
            this.chartInstance.options.title.text = this.data().options.title;
            this.chartInstance.update();
        } catch (error) {
        }
    }
}

export default {
    viewModel: BarChart,
    template: templateMarkup
};