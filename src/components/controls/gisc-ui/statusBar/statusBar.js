import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./statusBar.html";

/**
 * Display success or fail status bar on blade.
 * 
 * @class StatusBar
 * @extends {ControlBase}
 */
class StatusBar extends ControlBase {
    constructor(params) {
        super(params);
        this.text = this.ensureObservable(params.text);
        this.type = this.ensureObservable(params.type);
        this.effectiveIcon = ko.pureComputed(() => {
            switch(this.type()) {
                case "fail":
                    return "svg-ic-statusbar-fail";
                default:
                    return "svg-ic-statusbar";
            }
        });
    }

    /**
     * Close status bar
     */
    close(){
        this.visible(false);
    }
}

export default {
    viewModel: StatusBar,
    template: templateMarkup
};