import ko from "knockout";
import "blueimp.fileupload";
import ControlBase from "../../controlBase";
import templateMarkup from "text!./fileupload.html";
import FileUploadModel from "./FileUploadModel";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";

/**
 * Fileupload control class.
 * @public
 * @class FileUpload
 * @extends {ControlBase}
 * @see https://github.com/blueimp/jQuery-File-Upload/wiki/Basic-plugin
 */
class FileUpload extends ControlBase {
    constructor(params) {
        super(params);
        // selectedfile is using for validation binding only.
        this.selectedFile = this.ensureObservable(params.selectedFile);

        // Passing filename and validation binding from screen.
        this.fileName = this.ensureObservable(params.fileName, "");
        this.fileUrl = this.ensureObservable(params.fileUrl, "");
        this.isHyperLink = ko.pureComputed(() => { 
            return !_.isEmpty(this.fileUrl());
        });
        this.fileNameVisible = this.ensureObservable(params.fileNameVisible, true);
        this.fileNameVisibleInternal = ko.pureComputed(() => {
            return this.fileNameVisible() && !_.isEmpty(this.fileName());
        });

        // formData is used for passing information before upload.
        this.formData = this.ensureObservable(params.formData, {});

        // Specify target POST upload url.
        this.url = this.ensureNonObservable(params.url, WebConfig.appSettings.uploadUrl);

        // Internal render fileupload control.
        this.elementId = this.id + "_file";

        // Specify file upload filter dialog.
        this.accept = this.ensureNonObservable(params.accept, "*/*");

        // Event handlers.
        this.onRemoveFile = params.onRemoveFile;// Function reference
        this.onBeforeUpload = params.onBeforeUpload; // Function refernece.
        this.onUploadSuccess = params.onUploadSuccess; // Function refernece.
        this.onUploadFail = params.onUploadFail; // Function refernece.


        // Internal bindings.
        this.isUploading = ko.observable(false);
        this.uploadProgress = ko.observable(0);

        this.enableReference = this.enable.subscribe((enable) => {
            var $file = $("#" + this.elementId);
            if (enable) {
                $file.fileupload('enable');
                // need to manually set disable prop to input[type=file]
                $file.prop("disabled", false);
            }
            else {
                $file.fileupload('disable');
                // need to manually set disable prop to input[type=file]
                $file.prop("disabled", true);
            }
        });
    }

    /**
     * Remove selected file from UI.
     */
    onRemoveFileInternal() {
        // Auto remove fileName and selectedFile.
        this.fileName("");
        this.clearFileUrl();
        this.selectedFile(null);

        // Notify to screen if available.
        if(this.onRemoveFile) {
            this.onRemoveFile();
        }
    }

    /**
     * Called when View is loaded.
     * @param {boolean} isFirstLoad
     * @public
     */
    onLoad(isFirstLoad) {}

    /**
     * Create jquery file upload instance after DOM element is rendered to page.
     * @public
     * @param {any} element
     */
    onDomReady() {
        // Get file upload DOM element from element id.
        var self = this;
        var $file = $("#" + self.elementId);

        // Initialize file upload instance.
        $file.fileupload({
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            dataType: "json",
            add: function (e, data) {
                var dfdFileSelected = $.Deferred();
                // Prepare file upload model.
                var fileObj = data.files[0];
                var fileUploadModel = new FileUploadModel();
                fileUploadModel.name = fileObj.name;
                fileUploadModel.extension = fileObj.name.substring(fileObj.name.lastIndexOf('.')+1);
                fileUploadModel.size = fileObj.size;
                fileUploadModel.type = fileObj.type;

                // Detect image dimensions if image is available.
                if(fileUploadModel.type.indexOf("image") >= 0) {
                    // Detect image dimension.
                    var fr = new FileReader();
                    fr.onload = function () {
                        // Create virutal image loader element.
                        var img = new Image();
                        img.onload = function () {
                            fileUploadModel.imageWidth = this.width;
                            fileUploadModel.imageHeight = this.height;
                            // Image then selection is completed.
                            dfdFileSelected.resolve();
                        };
                        // Assign data-url from file reader to image.
                        img.src = fr.result;
                    };
                    // Read selected file as data-url.
                    fr.readAsDataURL(fileObj);
                }
                else {
                    // Non image then selection is completed.
                    dfdFileSelected.resolve();
                }

                // Perform validation then upload.
                $.when(dfdFileSelected).done(()=>{
                    // Assign to observable field.
                    self.selectedFile(fileUploadModel);

                    // Check for is valid.
                    var isValidToSubmit = false;
                    if(self.selectedFile.isValid) {
                        // There are validator assigned from observable so we validate it.
                        isValidToSubmit = self.selectedFile.isValid();
                    }
                    else {
                        // No validator assigned so it safe to upload.
                        isValidToSubmit = true;
                    }

                    if (self.onBeforeUpload) {
                        self.onBeforeUpload(isValidToSubmit);
                    }

                    self.fileName(fileObj.name);
                    self.clearFileUrl();

                    // Perform upload if validation is completed.
                    if (isValidToSubmit) {
                        // Turn on progress indicator.
                        self.isUploading(true);

                        // Prepare form data before submiting.
                        data.formData = self.formData();

                        // Send data to server.
                        var jqXHR = data.submit()
                        .success(function (result, textStatus, jqXHR) {
                            if (self.onUploadSuccess) {
                                self.onUploadSuccess(result);
                            }
                        })
                        .error(function (jqXHR, textStatus, errorThrown) {
                            if (self.onUploadFail) {
                                self.onUploadFail(jqXHR);
                            }
                        })
                        .complete(function (result, textStatus, jqXHR) {
                            // Always run after success or fail.
                            self.isUploading(false);
                            self.uploadProgress(0);
                        });
                    }
                });
            },
            done: function (e, data) {
                // Reset uploading status.
                self.isUploading(false);
                self.uploadProgress(0);
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                self.uploadProgress(progress);
            }
        });
    }

    /**
     * Cleanup file upload instance when dom is removing from page.
     * @public
     */
    onUnload() {
        var $file = $("#" + this.elementId);
        
        // Destroy file upload instance.
        $file.fileupload("destroy");

        this.selectedFile = null;
        this.fileName = null;
        this.fileUrl = null;
        this.fileNameVisible = null;
        this.formData = null;
    }
    /**
     * Remove file URL with safe pureComputed operation.
     */
    clearFileUrl() {
        if(!ko.isComputed(this.fileUrl) || !ko.isPureComputed(this.fileUrl)) {
            this.fileUrl("");
        }
    }
}

export default {
    viewModel: FileUpload,
    template: templateMarkup
};