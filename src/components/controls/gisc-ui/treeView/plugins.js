import "jstree";

if(!$.jstree.plugins.conditionalselectAsync) { 
    $.jstree.defaults.conditionalselectAsync = function () { return true; };
$.jstree.plugins.conditionalselectAsync = function (options, parent) {
    // own function
    this.activate_node = function (obj, e) {
        var result = this.settings.conditionalselectAsync.call(this, this.get_node(obj), e);
        if(result && result.state){
            // Deferred object.
            result.done(()=>{
                parent.activate_node.call(this, obj, e);
            });
        }
        else {
            // Standard js object.
            if(result === true) {
                parent.activate_node.call(this, obj, e);
            }
        }
    };
};
 }
