import ko from "knockout";
import templateMarkup from "text!./treeView.html";
import ControlBase from "../../controlbase";
import "./plugins";

/**
 * Treeview control.
 * 
 * @class TreeView
 * @extends {ControlBase}
 */
class TreeView extends ControlBase {

    constructor(params) {
        super(params);

        this.datasource = this.ensureObservable(params.datasource);
        this.dataID = this.ensureNonObservable(params.dataID);
        this.textField = this.ensureNonObservable(params.text);
        this.parentField = this.ensureNonObservable(params.parent);
        this.childField = this.ensureNonObservable(params.children);
        this.selectedNodes = this.ensureObservable(params.selectedNodes);
        this.collapsedNodes = this.ensureObservable(params.collapsedNodes);
        this.disableNodes = this.ensureObservable(params.disableNodes);
        this.onSelectingNode = this.ensureFunction(params.onSelectingNode);
        this.mode = this.ensureNonObservable(params.mode, "view");
        this.checkbox = ko.observable();
        this.wholerow = this.ensureNonObservable(params.wholerow, "");
        this.autoSelection = this.ensureObservable(params.autoSelection, true);
        this.parentCollectionIds = ko.observableArray();// Store data id to check if parent id is exist.

        //To set if node should be toggled if the text is double clicked.
        this.dbClickNode = ko.pureComputed(()=> {
            return this.autoSelection().value ? true : false;
        });

        this.cssClass = null;
        this.internalData = ko.observableArray();
        this.onBuildNode = params.onBuildNode;

        if (this.mode) {
            switch (this.mode) {
                case "view":
                    this.checkbox("");
                    this.cssClass = "";
                    break;
                case "update":
                    this.checkbox("checkbox");
                    this.cssClass = "jstree-checkbox";
                    break;
            }
        }

        this.onSelectedChangeRef = null;

        //subscribe when datasource was add/remove.
        this.subscribeArray = this.datasource.subscribe((newValue) => {
            var $el = $("#" + this.id);
            $el.fadeOut("fast", () => {
                var newParentIds = this._getDataId(this.datasource());
                this.parentCollectionIds(newParentIds);

                var newData = this.convertDataCollection(newValue, this.selectedNodes(), this.collapsedNodes(), this.disableNodes());
                this.internalData(newData);

                $el.jstree(true).settings.core.data = this.internalData();
                $el.jstree(true).refresh();

                $el.fadeIn("fast");
            });
        });

        //For selected state to empty.
        this.selectedNodesSubscribe = this.selectedNodes.subscribe((newValue) => {
            var $el = $("#" + this.id);
            if (!newValue) {
                $el.jstree(true).deselect_all(true);
            }
        });

        // Subscribe auto selection change.
        this.autoSelectionSubscribe = this.autoSelection.subscribe((newValue) => {
            // We cannot update treeview settings after initialization phase so we need to destroy current and re-initialize it.
            this.initializeTree(true);
        });
    }

    /**
     * Handle when user change checkbox.
     * @param {any} e
     * @param {any} data
     */
    onSelectedChange(e, data) {

        var oldValue = this.selectedNodes();
        var newValue = data.selected;

        // Vashira Note: this is work around, I found that in update mode
        // it must fire onSelectedChange regardless of oldValue/newValue
        // otherwise the subscription chain will broken. 
        if (this.mode === "view") {
            if (!_.isEqual(oldValue, newValue)) {
                this.selectedNodes(data.selected);
            }
        } else if (this.mode === "update") {
            if (this.autoSelection().value) {
                this.selectedNodes(data.selected);
            } else {
                var orderArr = [];
                var selectedData = data.selected;
                _.each(this.internalData(), (internalVal) => {
                    var id = internalVal.id;
                    _.each(selectedData, (selectedVal) => {
                        if (id == selectedVal) {
                            orderArr.push(selectedVal);
                        }
                    })
                });
                this.selectedNodes(orderArr);
            }
        }
    }

    /**
     * Prepare internal data for user latest changes.
     */
    initializeInternalData() 
    {
        // Convert hierachy structure to flat structure.
        var convertData = this.convertDataCollection(
            this.datasource(),
            this.selectedNodes(),
            this.collapsedNodes(),
            this.disableNodes());

        // Keep flat data structure to internal data.
        this.internalData(convertData);
    }

    /**
     * Initalize JS Tree from current option.
     * @param {boolean} [forceDestroy=false]
     */
    initializeTree(forceDestroy = false) {
        
        // Destory existing instance.
        if(forceDestroy) {
            // Require calculate new internal data to keep user selection.
            // If we don't call this function, tree view seletion start will be reset to first load.
            this.initializeInternalData();

            // Find existing instance and destroy.
            var jsTreeRef = $.jstree.reference(this.id);
            if(jsTreeRef) {
                jsTreeRef.destroy();
            }
        }

        var self = this;
        var $el = $("#" + this.id);
        this.onSelectedChangeRef = this.onSelectedChange.bind(this);

        $el.jstree({
            "conditionalselectAsync": function (node, event) {
                if (self.mode === "view") {
                    var selectedId = node.id;
                    var shouldCheckGuard = _.isFunction(self.onSelectingNode);
                    if (shouldCheckGuard) {
                        return self.onSelectingNode(selectedId);
                    }

                }

                return true;
            },
            "checkbox": {
                "keep_selected_style": true,
                "three_state": this.autoSelection().value
            },
            ui: {
                theme_name: "checkbox"
            },
            "plugins": [this.checkbox(), this.wholerow, "conditionalselectAsync"],
            'core': {
                'dblclick_toggle':this.dbClickNode(),
                'themes': {
                    'icons': false
                },
                'data': this.internalData()
            }
        }).on('loaded.jstree', () => {

            $el.on('changed.jstree', this.onSelectedChangeRef);

            $el.on('open_node.jstree', (e, data) => {
                if (this.collapsedNodes()) {
                    this.collapsedNodes.remove(data.node.id);
                }
            });

            $el.on('close_node.jstree', (e, data) => {
                if (this.collapsedNodes()) {
                    this.collapsedNodes.push(data.node.id);
                }
            });
        });
    }

    /**
     * Handle when screen is loading.
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (isFirstLoad) {
            var parentIds = this._getDataId(this.datasource());
            this.parentCollectionIds(parentIds);
            this.initializeInternalData();
        }
    }

    /**
     * Handle when DOM is ready to use.
     */
    onDomReady() {
        this.initializeTree();
    }

    /**
     * Convet hierachy datastructure to flast data structure.
     * @param {any} data
     * @param {any} selectedItem
     * @param {any} openedState
     * @param {any} disableState
     * @returns
     */
    convertDataCollection(data, selectedItem, openedState, disableState) {
        var dataCollection = [];
        _.each(data, (dt) => {
            var id = this.ensureNonObservable(dt[this.dataID]);
            var parent = this.ensureNonObservable(dt[this.parentField]);
            var text = this.ensureNonObservable(dt[this.textField]);
            var selected = false;
            var opened = true;
            var disabled = false;

            _.each(selectedItem, (s) => {
                if (s == id) {
                    selected = true;
                }
            });

            _.each(openedState, (op) => {
                if (op == id) {
                    opened = false;
                }
            });

            
            if( (disableState && this.mode === "view") || (disableState && !this.autoSelection().value && this.mode === "update") ){
                 _.each(disableState, (dis) => {
                    if (dis == id) {
                        disabled = true;
                    }
                });
            }

            var index = _.includes(this.parentCollectionIds(), parent);// Check if node's parent id is exist in datasource.
            
            if (!index) {
                parent = "#";
            }

            let item = {
                id: id,
                parent: parent,
                text: text,
                state: {
                    opened: opened,
                    selected: selected,
                    disabled: disabled,
                },
                li_attr: {
                    title: text

                },
                a_attr: {
                    class: this.cssClass,
                    href: "javascript:void(0);"
                },
                rawData: dt
            };

            if (this.onBuildNode) {
                this.onBuildNode(item);
            }

            dataCollection.push(item);

            var child = dt[this.childField];

            if (child && child.length > 0) {
                let children = this.convertDataCollection(child, selectedItem, openedState, disableState);
                dataCollection = _.union(dataCollection, children);
            }
        });
        return dataCollection;
    }

    
    /**
     * Return array of data id.
     * @param {any} data
     * @returns
     */
    _getDataId(data){
        var ids = [];
        _.each(data, (dat) => {
            ids.push(dat.id);

            var child = dat[this.childField];

            if (child && child.length > 0) {
                let children = this._getDataId(child);
                ids = _.union(ids, children);
            }
        });
        return ids;
    }

    /**
     * Handle when control is unload.
     */
    onUnload() {
        var $el = $("#" + this.id);
        $el.unbind();
        $el.jstree(true).destroy();
        this.subscribeArray.dispose();
        
        this.datasource = null;
        this.selectedNodes = null;
        this.collapsedNodes = null; 
        this.disableNodes = null;
    }
}

export default {
    viewModel: TreeView,
    template: templateMarkup
};