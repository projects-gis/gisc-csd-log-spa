import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./video.html";
// import "./global";
import videojs from 'videojs';

/**
 * 
 * Using for multi-line text input control
 * @class TextArea
 * @extends {ControlBase}
 */
class VideoView extends ControlBase {
    /**
     * Creates an instance of TextArea.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.id = this.ensureNonObservable(params.id, this.id);
        this.src = this.ensureNonObservable(params.src, null);
        this.type = this.ensureNonObservable(params.type, "video/mp4");
        this.controls = this.ensureNonObservable(params.controls, true);
        this.autoplay = this.ensureNonObservable(params.autoplay, false);
        this.preload = this.ensureNonObservable(params.preload, "auto");
        this.width = this.ensureNonObservable(params.width, "100%");
        this.height = this.ensureNonObservable(params.height, "100%");
        this.controlsList = this.ensureNonObservable(params.controlsList, "nodownload"); // ex. "nofullscreen nodownload noremoteplayback"
        this.disablepictureinpicture = this.ensureNonObservable(params.disablepictureinpicture, true);
        this.poster = this.ensureNonObservable(params.poster, null);
        
        
    }

    onDomReady() {
        var options = {}
        videojs(document.getElementById(this.id), options).ready(function() {
            // console.log('onPlayerReady', this);
        });
    }



    onLoad() {

    }

    onUnload() {

    }
}

export default {
    viewModel: VideoView,
    template: templateMarkup
};