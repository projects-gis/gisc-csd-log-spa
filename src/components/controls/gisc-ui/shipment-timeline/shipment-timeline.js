﻿import 'jquery';
import ko from "knockout";
import Konva from 'konva';
import ControlBase from "../../controlbase";
import templateMarkup from "text!./shipment-timeline.html";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {
    GanttConfig,
    GanttConstants
} from './shipment-timeline-constants';

class ShipmentTimelineControl extends ControlBase {
    constructor(params) {
        super(params)
        this.ganttConfig = GanttConfig.DrawingConfig
        this.ganttDrawingLayers = GanttConstants.layers
        this.data = this.ensureObservable(params.data)
        this.handlerShowTooltip = this.ensureObservable(params.showTooltip)
        this.tooltipHandler = this.ensureFunction(params.tooltipHandler)
        this.onClick = this.ensureFunction(params.onClick)

        this.mainData = []; //data ทั้งหมด
        this.selectedData = []; //data ที่กำลังแสดง
        this.drawData = []; //data ที่จะวาด

        this.ZOOM_FACTOR_1X = 1;
        this.ZOOM_FACTOR_2X = 2;
        this.ZOOM_FACTOR_4X = 4;
        this.incrementalCounter = WebConfig.appSettings.shipmentTimelineDisplayRow;
        this.zoomFactor = this.ensureObservable(params.zoomFactor, this.ZOOM_FACTOR_1X);
        this.dateCounter = this.ensureObservable(params.dateCounter, 0);
        this.isDateBackVisible = ko.computed(() => {
            return this.dateCounter() > -1?null:"disabled-zoom";
        });
        this.isDateNextVisible = ko.computed(() => {
            return this.dateCounter() < 1?null:"disabled-zoom";
        });


        this.isZoomInVisible = ko.computed(() => {
            return this.zoomFactor() < this.ZOOM_FACTOR_4X?null:"disabled-zoom";
        });

        this.isZoomOutVisible = ko.computed(() => {
            return this.zoomFactor() > this.ZOOM_FACTOR_1X?null:"disabled-zoom";
        });

        this.textParamStatus = ko.observable();

        this.paramStatus = this.ensureObservable(params.paramStatus);

        this.selectedDate = params.selectedDate; 
        this.startIndex = this.ensureObservable(params.startIndex, 0);
        this.endIndex = this.ensureObservable(params.endIndex, 0);


        this.currentTime = this.ensureObservable(params.currentTime)


        this.isLoadMoreVisible = ko.pureComputed(() => {
            return this.mainData.length - 1 != this.endIndex();
        }, this);

        this.isAfterRender = false;

        this.onZoomIn = () => {
            if(this.zoomFactor() == this.ZOOM_FACTOR_4X){
                return
            }
            if (this.zoomFactor() == this.ZOOM_FACTOR_1X) {
                this.zoomFactor(this.ZOOM_FACTOR_2X);
            } else if (this.zoomFactor() == this.ZOOM_FACTOR_2X) {
                this.zoomFactor(this.ZOOM_FACTOR_4X);
            }
            
            this.drawHeader();
            this.changeDate(0) // เปลี่ยนวันให้กลายเป็นวันเดิม
        }

        this.onZoomOut = () => {
            if(this.zoomFactor() == this.ZOOM_FACTOR_1X){
                return
            }
            if (this.zoomFactor() == this.ZOOM_FACTOR_4X) {
                this.zoomFactor(this.ZOOM_FACTOR_2X);
            } else if (this.zoomFactor() == this.ZOOM_FACTOR_2X) {
                this.zoomFactor(this.ZOOM_FACTOR_1X);
            }
           
            this.drawHeader();
            this.changeDate(0) // เปลี่ยนวันให้กลายเป็นวันเดิม
        }

        this.data.subscribe((items) => {
            if (this.isAfterRender && items.length > 0) {
                this.mainData = items;
                var startIndex = 0;

                if (this.endIndex() >= items.length) {
                    this.endIndex(items.length - 1)
                }

                this.drawData = this.filterToDrawData(startIndex, this.endIndex());
                this.selectedData = this.drawData;
                this.clearAllLayers();
                this.renderTimeline(startIndex);
            }

        });

        this.paramStatus.subscribe((res)=>{
            this.setTimelineStatusCount(res);
        });
    }

    afterRender() {
        this.setLayersTimeline();
        this.handlerShowTooltip.subscribe((params) => {
            this.showTooltip(params.template)
        });

        //Do Only First Draw (Reload Page will go to data.subscribe function)
        if (this.data().length > 0){
            this.mainData = this.data();
            var startIndex = 0;

            this.endIndex(WebConfig.appSettings.shipmentTimelineDisplayRow - 1);
            if (this.endIndex() >= this.data().length) {
                this.endIndex(this.data().length - 1)
            }

            this.drawData = this.filterToDrawData(startIndex, this.endIndex());
            this.selectedData = this.drawData;
            this.clearAllLayers();
            this.renderTimeline(startIndex);
        }
        this.setTimelineStatusCount(this.paramStatus());
    }

    /* เริ่มต้นสร้างStage และ Layersสำหรับวาด */
    setLayersTimeline() {
        var self = this
        this.stage = new Konva.Stage({
            container: self.id,
            width: window.innerWidth,
            height: window.innerHeight
        });

        /* Create All Drawing Layer */
        this.layers = {};
        this.layers[GanttConstants.layers.header] = new Konva.Layer({
            setZIndex: 1
        });
        this.layers[GanttConstants.layers.footer] = new Konva.Layer({
            setZIndex: 2
        });
        this.layers[GanttConstants.layers.license] = new Konva.Layer({
            setZIndex: 3
        });

        this.layers[GanttConstants.layers.time] = new Konva.Layer({
            setZIndex: 4
        });
        this.layers[GanttConstants.layers.timeLine] = new Konva.Layer({ //layer ที่ใช้วาดเส้นคั่นเวลาที่ตาราง Time Line
            setZIndex: 5
        });
        this.layers[GanttConstants.layers.plan] = new Konva.Layer({
            setZIndex: 6
        });
        this.layers[GanttConstants.layers.planTooltip] = new Konva.Layer({
            setZIndex: 7
        });
        this.layers[GanttConstants.layers.actual] = new Konva.Layer({
            setZIndex: 8
        });
        this.layers[GanttConstants.layers.actualTooltip] = new Konva.Layer({
            setZIndex: 9
        });
        this.layers[GanttConstants.layers.current] = new Konva.Layer({
            setZIndex: 10
        });
        this.layers[GanttConstants.layers.tooltip] = new Konva.Layer({
            setZIndex: 11
        });
        
        

        this.stage.add(
            this.layers[GanttConstants.layers.header],
            this.layers[GanttConstants.layers.footer],
            this.layers[GanttConstants.layers.current],
            this.layers[GanttConstants.layers.license],
            this.layers[GanttConstants.layers.time],
            this.layers[GanttConstants.layers.timeLine],
            this.layers[GanttConstants.layers.plan],
            this.layers[GanttConstants.layers.planTooltip],
            this.layers[GanttConstants.layers.actual],
            this.layers[GanttConstants.layers.actualTooltip],
            this.layers[GanttConstants.layers.tooltip]
            
        );

        this.isAfterRender = true;
        this.resizeDrawStage();
        this.drawHeader();
        // this.drawFooter();
    }

     /* Set Status */
    setTimelineStatusCount(statusItem){
        if (statusItem){
            let textStatus = "(";
            textStatus += `${this.i18n("Common_Total")()} ${statusItem.total}`;
            textStatus += statusItem.delay != 0 ? ` | ${this.i18n("Transportation_Mngm_ShipmentTimeline_Delay")()} ${statusItem.delay}` : '';
            textStatus += statusItem.maybeDelay != 0 ? ` | ${this.i18n("Transportation_Mngm_ShipmentTimeline_Maybe_Delay")()} ${statusItem.maybeDelay}` : '';
            textStatus += statusItem.insideWaypoint != 0 ? ` | ${this.i18n("Transportation_Mngm_ShipmentTimeline_Inside_Waypoint")()} ${statusItem.insideWaypoint}` : '';
            textStatus += statusItem.onTime != 0 ? ` | ${this.i18n("Transportation_Mngm_ShipmentTimeline_Ontime")()} ${statusItem.onTime}` : '';
            textStatus += statusItem.plan != 0 ? ` | ${this.i18n("Transportation_Mngm_ShipmentTimeline_Plan")()} ${statusItem.plan}` : '';
            textStatus += statusItem.finish != 0 ? ` | ${this.i18n("Transportation_Mngm_ShipmentTimeline_Finish")()} ${statusItem.finish}` : '';
            textStatus += ")";
            this.textParamStatus(textStatus);
        }
    }

    /* เมื่อมีการวาดอะไรหรือเพิ่มอะไรใหม่ ต้องเรียกคำสั่งนี้เพื่อให้ Konva วาดขึ้นมา */
    redrawAllLayer() {
        this.layers[GanttConstants.layers.license].batchDraw();
        this.layers[GanttConstants.layers.time].batchDraw();
        this.layers[GanttConstants.layers.plan].batchDraw();
        this.layers[GanttConstants.layers.planTooltip].batchDraw();

        this.layers[GanttConstants.layers.tooltip].batchDraw();
        this.layers[GanttConstants.layers.current].batchDraw();
        this.layers[GanttConstants.layers.footer].batchDraw();
        this.layers[GanttConstants.layers.header].batchDraw();
        this.layers[GanttConstants.layers.timeLine].batchDraw();

        var self = this;
        setTimeout(() => {
            self.layers[GanttConstants.layers.actual].batchDraw();
            self.layers[GanttConstants.layers.actualTooltip].batchDraw();
        }, 300);
    }

    /* ลบไอเทมเดิมออกจากLayer */
    clearAllLayers() {
        this.layers[GanttConstants.layers.license].removeChildren();
        this.layers[GanttConstants.layers.time].removeChildren();
        this.layers[GanttConstants.layers.plan].removeChildren();
        this.layers[GanttConstants.layers.planTooltip].removeChildren();
        this.layers[GanttConstants.layers.actual].removeChildren();
        this.layers[GanttConstants.layers.actualTooltip].removeChildren();
        this.layers[GanttConstants.layers.current].removeChildren();
        this.layers[GanttConstants.layers.tooltip].removeChildren();
    }

    /* วาดTimelineทั้งหมด */
    renderTimeline(startIndex) {
        this.resizeDrawStage(this.selectedData.length);
        for (var i = 0; i < this.drawData.length; i++){
          
            this.drawLicense(this.drawData[i], i, startIndex);
            this.drawTimeline(this.drawData[i], i,startIndex);
            this.drawPlan(this.drawData[i], i, startIndex);
            this.drawActual(this.drawData[i], i, startIndex);
            // if (this.dateCounter() == 0) {
            //     this.drawActual(this.drawData[i], i, startIndex);
            // }
        }
        this.drawFooter(this.drawData.length,startIndex);
        this.drawHeader();
        this.drawCurrentTimeLine(startIndex);
        this.redrawAllLayer();
    }

    /* เลือกไอเทมที่จะใช้วาดเพิ่มเติมบนStage */
    filterToDrawData(startIndex, endIndex) {
        var drawData = new Array();
        for (var indexMain = startIndex; indexMain <= endIndex; indexMain++) {
            
            var itemMain = this.mainData[indexMain];
            
            let currentPlanDate = 0;
            let currentActualDate = 0;

            let planStartDate = null // เวลาเริ่มต้นของ plan
            let planEndDate = null // เวลาสิ้นสุดของ plan
            let planCurrentDate = new Date(this.selectedDate()) // เวลาปัจจุบันของ plan

            let actualStartDate = null // เวลาเริ่มต้นของ actual
            let actualEndDate = null // เวลาสิ้นสุดของ actual
            let actualCurrentDate = new Date(this.selectedDate()) // เวลาปัจจุบันของ actual
                itemMain.waypointDetail.filter((itemPlan, indexPlan) => { // หาว่าใน plan มีอันไหนที่วันที่ไม่ตรงกับ selected หรือเปล่า
                    let objD = null
                    let isSelectedPlan = null

                    if (itemPlan == undefined) {
                        isSelectedPlan = false
                    } else {
                        objD = new Date(itemPlan.arrivePlan)
                        if (objD.getHours() < 6) {
                            objD.setDate(objD.getDate() - 1)
                        }

                        isSelectedPlan = this.checkSelectedDate(objD)
                    }

                    if (isSelectedPlan) {
                        currentPlanDate++
                    } else {}

                    if (indexPlan == 0) {
                        planStartDate = new Date(itemPlan.arrivePlan)
                    }

                    if (indexPlan == (itemMain.waypointDetail.length - 1)) {
                        planEndDate = new Date(itemPlan.arrivePlan)
                    }
                });

                if (planCurrentDate.getTime() < planEndDate.getTime() && // check ว่า plan อยู่ในระหว่างวันเวลาปัจจุบันหรือไม่
                    planCurrentDate.getTime() > planStartDate.getTime()) {
                    currentPlanDate++
                }

                if (actualCurrentDate < actualEndDate && // check ว่า actual อยู่ในระหว่างวันเวลาปัจจุบันหรือไม่
                    actualCurrentDate > actualStartDate) {
                    currentActualDate++
                }

                if (currentPlanDate != 0 || currentActualDate != 0) { //ถ้ามีในช่วงวันที่แสดง ก็จะเก็บไว้วาด
                    drawData.push(itemMain);
                }
        }

        return drawData;
    }

    /* คำนวนพื้นที่วาดตารางทั้งหมด */
    resizeDrawStage(totalDisplayRow) { 

        let widthBorder = this.ganttConfig.licenseTable.row.totalLicenseWidth // ความกว้างของตาราง license
        let heightBorder = ((this.ganttConfig.rowHeight * totalDisplayRow) + this.ganttConfig.headerHeight) // ความสูงของตาราง license

        this.stage.setWidth(widthBorder + (this.ganttConfig.timeTable.hrWidth * this.ganttConfig.hours * this.zoomFactor()) + (this.ganttConfig.margin.left + this.ganttConfig.margin.right)) // หาความกว้างทั้งหมดของ stage

        this.stage.setHeight(heightBorder + (this.ganttConfig.margin.top + this.ganttConfig.margin.bottom)+this.ganttConfig.headerHeight) // หาความสูงทั้งหมดของ stage

        let displayWidth = $(window).width() - 130;
        $(".shipment-timeline").attr("style", `width: ${displayWidth}px`) // set ให้ gantt chart เต็มหน้าจอ

    }

    onLoadMore() {
        this.endIndex(this.endIndex() + this.incrementalCounter);
        if (this.endIndex() >= this.mainData.length) {
            this.endIndex(this.mainData.length - 1);
        }


        var drawEndIndex = (this.selectedData.length - 1) + this.incrementalCounter;
        if (drawEndIndex > this.mainData.length) {
            drawEndIndex = this.mainData.length - 1;
        }

        var startIndex = drawEndIndex - (this.incrementalCounter - 1);

        this.drawData = this.filterToDrawData(startIndex, drawEndIndex);
        this.selectedData = this.selectedData.concat(this.drawData);
        this.renderTimeline(startIndex);
    }

    onClickDateBack(params){
        if (this.dateCounter() > -1){
            this.changeDate(params);
        }
    }

    onClickDateNext(params){
        if (this.dateCounter() < 1){
            this.changeDate(params);
        }
    }

    changeDate(params) {
        let objDate = new Date(this.selectedDate())
        objDate.setDate(objDate.getDate() + parseInt(params))

        let tempDateCounter = this.dateCounter() + parseInt(params);
    
        this.dateCounter(tempDateCounter);
        this.selectedDate(objDate);
        var startIndex = 0;

        this.drawData = this.filterToDrawData(startIndex, this.endIndex());
        this.selectedData = this.drawData;
        this.clearAllLayers();
        this.renderTimeline(startIndex);

    }

    checkSelectedDate(params) { // ส่ง objDate มาเพื่อหาวันที่ selected ** format :: yyyy-mm-ddThh:mm:ss || new Date()
        let isSelected = false
        let objDate = new Date(params)

        if (objDate.getDate() == this.selectedDate().getDate() &&
            objDate.getMonth() == this.selectedDate().getMonth()) {
            isSelected = true
        }

        return isSelected
    }

    /* เริ่มต้นวาดHeader */
    drawHeader() {
        this.layers[GanttConstants.layers.header].removeChildren();
        this.layers[GanttConstants.layers.timeLine].removeChildren(); 
        this.createLicenseTableHeader();
        this.createTimelineHeader();
    }
    /* วาดfooter */
    drawFooter(rowIndex,startIndex) {

        this.layers[GanttConstants.layers.footer].removeChildren();
        let heightHeader = this.ganttConfig.headerHeight;
        let yRow = this.ganttConfig.yStartPoint.header + heightHeader + (rowIndex * this.ganttConfig.rowHeight) + (startIndex * this.ganttConfig.rowHeight);

        this.createLicenseTableFooter(yRow);
        this.createTimelineFooter(yRow);
    }

    /* เริ่มต้นวาดFooter ส่วนLicense */
    createLicenseTableFooter(yRow) {

         //Create Header
         let xStart = this.ganttConfig.xStartPoint.license;
         let heightHeader = this.ganttConfig.headerHeight;
         let widthStatus = this.ganttConfig.licenseTable.row.statusWidth;
         this.drawRect({ // create header status
             x: xStart,
             y: yRow,
             width: widthStatus,
             height: heightHeader,
             fillColor: this.ganttConfig.color.lineChart,
             // borderColor: this.ganttConfig.color.borderColorLicense
         }, GanttConstants.layers.footer);
 
         let textStatus = this.i18n("Transportation_Mngm_ShipmentTimeline_Status")();
 
         var textItemStatus = this.drawText({
             x: this.calculatedTextCentredX(xStart, widthStatus, textStatus),
             y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, textStatus),
             text: textStatus,
             fontColor: this.ganttConfig.color.font,
             fontStyle: this.ganttConfig.fontStyle.bold
         }, GanttConstants.layers.footer);
 
         xStart += widthStatus;
 
         let widthLicense = this.ganttConfig.licenseTable.row.licenseWidth;
         this.drawRect({ // create header vehicle license
             x: xStart,
             y: yRow,
             width: widthLicense,
             height: heightHeader,
             fillColor: this.ganttConfig.color.lineChart,
             // borderColor: this.ganttConfig.color.borderColorLicense
         }, GanttConstants.layers.footer);
 
         let textLicense = this.i18n("Common_License")();
 
         var textItemLicense = this.drawText({
             x: this.calculatedTextCentredX(xStart, widthLicense, textLicense),
             y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, textLicense),
             text: textLicense,
             fontColor: this.ganttConfig.color.font,
             fontStyle: this.ganttConfig.fontStyle.bold
         }, GanttConstants.layers.footer);
 
         xStart += widthLicense;
 
         //xStart += widthTrackLicense;
         let widthShipmentNumber = this.ganttConfig.licenseTable.row.shipmentNumberWidth;
 
         this.drawRect({ // create header shipmentNumber
             x: xStart,
             y: yRow,
             width: widthShipmentNumber,
             height: heightHeader,
             fillColor: this.ganttConfig.color.lineChart,
             // borderColor: this.ganttConfig.color.borderColorLicense
         }, GanttConstants.layers.footer);
 
         let textShipmentNo = this.i18n("Transportation_Mngm_ShipmentTimeline_ShipmentNumber")();
 
         var textItemShipmentNo = this.drawText({
             x: this.calculatedTextCentredX(xStart, this.ganttConfig.licenseTable.row.shipmentNumberWidth, textShipmentNo),
             y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, textShipmentNo),
             text: textShipmentNo,
             fontColor: this.ganttConfig.color.font,
             fontStyle: this.ganttConfig.fontStyle.bold
         }, GanttConstants.layers.footer)
 
         textItemStatus.moveToTop();
         textItemLicense.moveToTop();
         textItemShipmentNo.moveToTop();



    }

    /* เริ่มต้นวาดFooter ส่วนTimeline */
    createTimelineFooter(yRow) {

        let xRow = this.ganttConfig.xStartPoint.time

        // ความยาวทั้งหมดของตาราง time
        let subWidth = this.ganttConfig.timeTable.hrWidth * this.ganttConfig.hours * this.zoomFactor();

        // ความสูงทั้งหมดของตาราง time
        let subHeight = (this.ganttConfig.rowHeight * this.selectedData.length) + this.ganttConfig.headerHeight;

        let xSubRow = this.ganttConfig.xStartPoint.license +
            this.ganttConfig.licenseTable.row.totalLicenseWidth

        // จุดเริ่มต้นของตาราง time (y)
        let ySubRow = this.ganttConfig.yStartPoint.header;

        // ความยาวของแต่ละช่องของตาราง time (header, detail)
        let subTimeWidth = this.ganttConfig.timeTable.hrWidth * this.zoomFactor();

        // ความสูงของแต่ละช่องของตาราง time (header)
        let subTimeHeight = this.ganttConfig.headerHeight;

        let hours = this.ganttConfig.hours;

        let objDate = new Date();
        objDate.setHours(this.ganttConfig.startHour);
        
        this.drawRect({
            x: xRow,
            y: yRow,
            width: subWidth,
            height: subTimeHeight,
            fillColor: this.ganttConfig.color.colorTitle
        }, GanttConstants.layers.footer);

        for (let index = 0; index < hours; index++) {

            let formatDisplayTime = "";

            if (objDate.getHours().toString().length == 1) {
                formatDisplayTime = `0${objDate.getHours()}:00` // set format การแสดงชั่วโมงตาราง
            } else {
                formatDisplayTime = `${objDate.getHours()}:00` // set format การแสดงชั่วโมงตาราง
            }

            this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา footer
                x: xSubRow,
                y: yRow,//this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, formatDisplayTime)-4,
                width: subTimeWidth,
                height: subTimeHeight,
                borderColor:'#d3d3d3',
                strokeWidth:1
            }, GanttConstants.layers.footer);

            this.drawText({
                x: xSubRow + 4,
                y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, formatDisplayTime),
                text: formatDisplayTime,
                fontColor: this.ganttConfig.color.font,
                fontStyle: this.ganttConfig.fontStyle.bold
            }, GanttConstants.layers.footer);

            if (this.zoomFactor() >= this.ZOOM_FACTOR_2X) {
                var format30MinTime = formatDisplayTime.split(':');

                this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา footer
                    x: xSubRow + (subTimeWidth / 2),
                    y: yRow,//this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, formatDisplayTime)-4,
                    width: subTimeWidth,
                    height: subTimeHeight,
                    borderColor:'#d3d3d3',
                    strokeWidth:1
                }, GanttConstants.layers.footer);

                this.drawText({
                    x: (xSubRow + (subTimeWidth / 2)) + 4,
                    y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight),
                    text: format30MinTime[0] + ":30",
                    fontColor: this.ganttConfig.color.font,
                    fontStyle: this.ganttConfig.fontStyle.bold
                }, GanttConstants.layers.footer);
            }

            if (this.zoomFactor() >= this.ZOOM_FACTOR_4X) {
                var format30MinTime = formatDisplayTime.split(':');

                this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา footer
                    x: xSubRow + (subTimeWidth * 0.25),
                    y: yRow,//this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, formatDisplayTime)-4,
                    width: subTimeWidth,
                    height: subTimeHeight,
                    borderColor:'#d3d3d3',
                    strokeWidth:1
                }, GanttConstants.layers.footer);

                this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา footer
                    x: xSubRow + (subTimeWidth * 0.75),
                    y: yRow,//this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight, formatDisplayTime)-4,
                    width: subTimeWidth,
                    height: subTimeHeight,
                    borderColor:'#d3d3d3',
                    strokeWidth:1
                }, GanttConstants.layers.footer);

                this.drawText({
                    x: (xSubRow + (subTimeWidth * 0.25)) + 4,
                    y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight),
                    text: format30MinTime[0] + ":15",
                    fontColor: this.ganttConfig.color.font,
                    fontStyle: this.ganttConfig.fontStyle.bold
                }, GanttConstants.layers.footer);

                this.drawText({
                    x: (xSubRow + (subTimeWidth * 0.75)) + 4,
                    y: this.calculatedTextCentredY(yRow, this.ganttConfig.headerHeight),
                    text: format30MinTime[0] + ":45",
                    fontColor: this.ganttConfig.color.font,
                    fontStyle: this.ganttConfig.fontStyle.bold
                }, GanttConstants.layers.footer);
            }

            xSubRow += subTimeWidth
            objDate.setHours(parseInt(objDate.getHours() + 1))

        }

    }
    

    /* เริ่มต้นวาดHeader ส่วนLicense */
    createLicenseTableHeader() { //วาดหัวตาราง License
        //Create Header
        let xStart = this.ganttConfig.xStartPoint.license;
        let yStart = this.ganttConfig.yStartPoint.header;
        let heightHeader = this.ganttConfig.headerHeight;

        let widthStatus = this.ganttConfig.licenseTable.row.statusWidth;
        this.drawRect({ // create header status
            x: xStart,
            y: yStart,
            width: widthStatus,
            height: heightHeader,
            fillColor: this.ganttConfig.color.lineChart,
            // borderColor: this.ganttConfig.color.borderColorLicense
        }, GanttConstants.layers.header);

        let textStatus = this.i18n("Transportation_Mngm_ShipmentTimeline_Status")();

        var textItemStatus = this.drawText({
            x: this.calculatedTextCentredX(xStart, widthStatus, textStatus),
            y: this.calculatedTextCentredY(yStart, this.ganttConfig.headerHeight, textStatus),
            text: textStatus,
            fontColor: this.ganttConfig.color.font,
            fontStyle: this.ganttConfig.fontStyle.bold
        }, GanttConstants.layers.header);

        xStart += widthStatus;

        let widthLicense = this.ganttConfig.licenseTable.row.licenseWidth;
        this.drawRect({ // create header vehicle license
            x: xStart,
            y: yStart,
            width: widthLicense,
            height: heightHeader,
            fillColor: this.ganttConfig.color.lineChart,
            // borderColor: this.ganttConfig.color.borderColorLicense
        }, GanttConstants.layers.header);

        let textLicense = this.i18n("Common_License")();

        var textItemLicense = this.drawText({
            x: this.calculatedTextCentredX(xStart, widthLicense, textLicense),
            y: this.calculatedTextCentredY(yStart, this.ganttConfig.headerHeight, textLicense),
            text: textLicense,
            fontColor: this.ganttConfig.color.font,
            fontStyle: this.ganttConfig.fontStyle.bold
        }, GanttConstants.layers.header);

        xStart += widthLicense;

        //xStart += widthTrackLicense;
        let widthShipmentNumber = this.ganttConfig.licenseTable.row.shipmentNumberWidth;

        this.drawRect({ // create header shipmentNumber
            x: xStart,
            y: yStart,
            width: widthShipmentNumber,
            height: heightHeader,
            fillColor: this.ganttConfig.color.lineChart,
            // borderColor: this.ganttConfig.color.borderColorLicense
        }, GanttConstants.layers.header);

        let textShipmentNo = this.i18n("Transportation_Mngm_ShipmentTimeline_ShipmentNumber")();

        var textItemShipmentNo = this.drawText({
            x: this.calculatedTextCentredX(xStart, this.ganttConfig.licenseTable.row.shipmentNumberWidth, textShipmentNo),
            y: this.calculatedTextCentredY(yStart, this.ganttConfig.headerHeight, textShipmentNo),
            text: textShipmentNo,
            fontColor: this.ganttConfig.color.font,
            fontStyle: this.ganttConfig.fontStyle.bold
        }, GanttConstants.layers.header)

        textItemStatus.moveToTop();
        textItemLicense.moveToTop();
        textItemShipmentNo.moveToTop();
    }

    /* เริ่มต้นวาดHeader ส่วนTimeline (เพิ่มการวาดเส้นคั่นช่องเวลาที่ Header และ ที่ตาราง Timeline 18/10/2018) */ 
    createTimelineHeader() { //วาดหัวตารางของ Timeline
        let xRow = this.ganttConfig.xStartPoint.time

        // จุดเริ่มต้นของตาราง time (y)
        let yRow = this.ganttConfig.margin.top;

        // ความยาวทั้งหมดของตาราง time
        let subWidth = this.ganttConfig.timeTable.hrWidth * this.ganttConfig.hours * this.zoomFactor();

        // ความสูงทั้งหมดของตาราง time
        let subHeight = (this.ganttConfig.rowHeight * this.selectedData.length) + this.ganttConfig.headerHeight;

        let xSubRow = this.ganttConfig.xStartPoint.license +
            this.ganttConfig.licenseTable.row.totalLicenseWidth

        // จุดเริ่มต้นของตาราง time (y)
        let ySubRow = this.ganttConfig.yStartPoint.header;

        // ความยาวของแต่ละช่องของตาราง time (header, detail)
        let subTimeWidth = this.ganttConfig.timeTable.hrWidth * this.zoomFactor();

        // ความสูงของแต่ละช่องของตาราง time (header)
        let subTimeHeight = this.ganttConfig.headerHeight;

        // ความสูงของแต่ละช่องของตาราง time(detail)
        let rowHeight = this.ganttConfig.rowHeight

        let hours = this.ganttConfig.hours;

        let objDate = new Date();
        objDate.setHours(this.ganttConfig.startHour);

        this.drawRect({
            x: xRow,
            y: ySubRow,
            width: subWidth,
            height: subTimeHeight,
            fillColor: this.ganttConfig.color.colorTitle
        }, GanttConstants.layers.header);

        for (let index = 0; index < hours; index++) {

            let formatDisplayTime = "";

            if (objDate.getHours().toString().length == 1) {
                formatDisplayTime = `0${objDate.getHours()}:00` // set format การแสดงชั่วโมงตาราง
            } else {
                formatDisplayTime = `${objDate.getHours()}:00` // set format การแสดงชั่วโมงตาราง
            }

            this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา 
                x: xSubRow,
                y: ySubRow,
                width: subTimeWidth,
                height: subTimeHeight,
                borderColor:'#d3d3d3',
                strokeWidth:1
            }, GanttConstants.layers.header);

            this.drawLine({ //วาดเส้นเวลาในตารางไทม์ไลน์ (ตาราง Timelline)
                points: [
                    xSubRow, ySubRow + subTimeHeight,
                    xSubRow, ySubRow + subTimeHeight + (rowHeight * this.selectedData.length)
                ],
                lineColor: '#d3d3d3'//this.ganttConfig.color.lineColorTime
            }, GanttConstants.layers.timeLine);

            this.drawText({
                x: xSubRow + 4,
                y: this.calculatedTextCentredY(ySubRow, this.ganttConfig.headerHeight, formatDisplayTime),
                text: formatDisplayTime,
                fontColor: this.ganttConfig.color.font,
                fontStyle: this.ganttConfig.fontStyle.bold
            }, GanttConstants.layers.header);


            if (this.zoomFactor() >= this.ZOOM_FACTOR_2X) {
                var format30MinTime = formatDisplayTime.split(':');

                this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา 
                    x: (xSubRow + (subTimeWidth / 2)),
                    y: ySubRow,
                    width: (subTimeWidth / 2),
                    height: subTimeHeight,
                    borderColor:'#d3d3d3',
                    strokeWidth:1,
                    fillColor: this.ganttConfig.color.colorTitle
                }, GanttConstants.layers.header);

                this.drawLine({ //วาดเส้นเวลาในตารางไทม์ไลน์ (ตาราง Timelline)
                    points: [
                        xSubRow + (subTimeWidth / 2), ySubRow + subTimeHeight,
                        xSubRow + (subTimeWidth / 2), ySubRow + subTimeHeight + (rowHeight * this.selectedData.length)
                    ],
                    lineColor: '#d3d3d3'//this.ganttConfig.color.lineColorTime
                }, GanttConstants.layers.timeLine);

                this.drawText({
                    x: (xSubRow + (subTimeWidth / 2)) + 4,
                    y: this.calculatedTextCentredY(ySubRow, this.ganttConfig.headerHeight),
                    text: format30MinTime[0] + ":30",
                    fontColor: this.ganttConfig.color.font,
                    fontStyle: this.ganttConfig.fontStyle.bold
                }, GanttConstants.layers.header);
            }

            if (this.zoomFactor() >= this.ZOOM_FACTOR_4X) {
                var format30MinTime = formatDisplayTime.split(':');
                
                this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา 15
                    x: (xSubRow + (subTimeWidth * 0.25)),
                    y: ySubRow,
                    width: (subTimeWidth * 0.25),
                    height: subTimeHeight,
                    borderColor:'#d3d3d3',
                    strokeWidth:1,
                    fillColor: this.ganttConfig.color.colorTitle
                }, GanttConstants.layers.header);

                this.drawText({
                    x: (xSubRow + (subTimeWidth * 0.25)) + 4,
                    y: this.calculatedTextCentredY(ySubRow, this.ganttConfig.headerHeight),
                    text: format30MinTime[0] + ":15",
                    fontColor: this.ganttConfig.color.font,
                    fontStyle: this.ganttConfig.fontStyle.bold
                }, GanttConstants.layers.header);

               

                this.drawRect({ //วาดเส้นคั่นระหว่างช่วงเวลา 45
                    x: (xSubRow + (subTimeWidth * 0.75)),
                    y: ySubRow,
                    width: (subTimeWidth * 0.75),
                    height: subTimeHeight,
                    borderColor:'#d3d3d3',
                    strokeWidth:1,
                    fillColor: this.ganttConfig.color.colorTitle
                }, GanttConstants.layers.header);

                this.drawLine({ //วาดเส้นเวลาในตารางไทม์ไลน์ (ตาราง Timelline)
                    points: [
                        xSubRow + (subTimeWidth * 0.25), ySubRow + subTimeHeight,
                        xSubRow + (subTimeWidth * 0.25), ySubRow + subTimeHeight + (rowHeight * this.selectedData.length)
                    ],
                    lineColor: '#d3d3d3'//'#d3d3d3'//this.ganttConfig.color.lineColorTime
                }, GanttConstants.layers.timeLine);

                this.drawLine({ //วาดเส้นเวลาในตารางไทม์ไลน์ (ตาราง Timelline)
                    points: [
                        xSubRow + (subTimeWidth * 0.75), ySubRow + subTimeHeight,
                        xSubRow + (subTimeWidth * 0.75), ySubRow + subTimeHeight + (rowHeight * this.selectedData.length)
                    ],
                    lineColor: '#d3d3d3'//'#d3d3d3'//this.ganttConfig.color.lineColorTime
                }, GanttConstants.layers.timeLine);

                this.drawText({
                    x: (xSubRow + (subTimeWidth * 0.75)) + 4,
                    y: this.calculatedTextCentredY(ySubRow, this.ganttConfig.headerHeight),
                    text: format30MinTime[0] + ":45",
                    fontColor: this.ganttConfig.color.font,
                    fontStyle: this.ganttConfig.fontStyle.bold
                }, GanttConstants.layers.header);
            }

            xSubRow += subTimeWidth
            objDate.setHours(parseInt(objDate.getHours() + 1))

        }
    }



    /* วาด Item License */
    drawLicense(item,rowIndex,startIndex) { //วาดช่อง License

        let xStart = this.ganttConfig.xStartPoint.license;
        let yStart = this.ganttConfig.yStartPoint.header;
        let heightHeader = this.ganttConfig.headerHeight;


        let xRow = this.ganttConfig.xStartPoint.license
        let yRow = this.ganttConfig.yStartPoint.header + heightHeader + (rowIndex * this.ganttConfig.rowHeight) + (startIndex * this.ganttConfig.rowHeight);
        let subWidth = this.ganttConfig.licenseTable.row.statusWidth
        let subHeight = this.ganttConfig.rowHeight
        let onclick = false
        // status rect
        this.drawRect({
            x: xRow,
            y: yRow,
            width: subWidth,
            height: subHeight,
            fillColor: null,
            // borderColor: this.ganttConfig.color.borderColorLicense
        }, GanttConstants.layers.license);

        // status color
        this.drawRect({
            x: (xRow + 11),
            y: (yRow + 11),
            width: 48,
            height: 13,
            fillColor: item.timelineTruckStatusColor,
            cornerRadius: 50,
            borderColor: item.timelineTruckStatusColor
        }, GanttConstants.layers.license);

        xRow += this.ganttConfig.licenseTable.row.statusWidth
        subWidth = this.ganttConfig.licenseTable.row.licenseWidth

        // vehicle license
        this.drawRect({
            x: xRow,
            y: yRow,
            width: subWidth,
            height: subHeight,
            fillColor: this.ganttConfig.color.colorLicense,
            // borderColor: this.ganttConfig.color.borderColorLicense
        }, GanttConstants.layers.license);


        this.drawText({
            x: this.calculatedTextCentredX(xRow, this.ganttConfig.licenseTable.row.licenseWidth, this.setFormatTextForDisplay(item.license)),
            y: this.calculatedTextCentredY(yRow, this.ganttConfig.rowHeight, this.setFormatTextForDisplay(item.license)),
            text: this.setFormatTextForDisplay(item.license),
            fontColor: this.ganttConfig.color.font,
            fontStyle: this.ganttConfig.fontStyle.bold
        }, GanttConstants.layers.license, {shipment:item.license},onclick)

        xRow += this.ganttConfig.licenseTable.row.licenseWidth
        subWidth = this.ganttConfig.licenseTable.row.shipmentNumberWidth

        // shipment number
        this.drawRect({
            x: xRow,
            y: yRow,
            width: subWidth,
            height: subHeight,
            fillColor: this.ganttConfig.color.colorLicense,
            // borderColor: this.ganttConfig.color.borderColorLicense
        }, GanttConstants.layers.license);

        
        this.drawText({
            x: this.calculatedTextCentredX(xRow, this.ganttConfig.licenseTable.row.shipmentNumberWidth, this.setFormatTextForDisplay(item.shipmentCode)),
            y: this.calculatedTextCentredY(yRow, this.ganttConfig.rowHeight, this.setFormatTextForDisplay(item.shipmentCode)),
            text: this.setFormatTextForDisplay(item.shipmentCode),
            fontColor: this.ganttConfig.color.font,
            fontStyle: this.ganttConfig.fontStyle.bold
        }, GanttConstants.layers.license, { shipment: item }); //, { shipment: item} tooltip binding
    }

    /* วาด กรอบของTimelineแต่ล่ะLicense */
    drawTimeline(item,index,startIndex) { //วาดตาราง timeline 
        // จุดเริ่มต้นของตาราง time (x)
        let xRow = this.ganttConfig.xStartPoint.time;
        let rowHeight = this.ganttConfig.rowHeight

        // ความยาวทั้งหมดของตาราง time
        let subWidth = this.ganttConfig.timeTable.hrWidth * this.ganttConfig.hours * this.zoomFactor();

        let yRow = this.ganttConfig.margin.top + this.ganttConfig.headerHeight + (index * this.ganttConfig.rowHeight) + (startIndex * this.ganttConfig.rowHeight);
        let subHeight = this.ganttConfig.rowHeight

        let xSubRow = this.ganttConfig.xStartPoint.license +
            this.ganttConfig.licenseTable.row.totalLicenseWidth

        // จุดเริ่มต้นของตาราง time (y)
        let ySubRow = this.ganttConfig.yStartPoint.header;

        // ความยาวของแต่ละช่องของตาราง time (header, detail)
        let subTimeWidth = this.ganttConfig.timeTable.hrWidth * this.zoomFactor();

        // ความสูงของแต่ละช่องของตาราง time (header)
        let subTimeHeight = this.ganttConfig.headerHeight;

        let hours = this.ganttConfig.hours;
        let objDate = new Date();
        objDate.setHours(this.ganttConfig.startHour);

       
        let rect = this.drawRect({
            x: xRow,
            y: yRow,
            width: subWidth,
            height: subHeight,
            fillColor: '#FFFFFF' //this.ganttConfig.color.colorTime
        }, GanttConstants.layers.time);

        rect.moveToBottom();
    }

    /* วาดเส้น current Time */
    drawCurrentTimeLine(startIndex) {
        if (!this.checkSelectedDate(this.currentTime())) { // ถ้าไม่ใช่ current date จะไม่วาดเส้น current time (วันย้อนหลัง || วันเดินหน้า ต้องไม่วาด)
            return false
        }

        let dffHour = this.currentTime().getHours() - this.ganttConfig.startHour
        let currentMinutes = this.currentTime().getMinutes()
        let xRow = (dffHour * (this.ganttConfig.timeTable.hrWidth * this.zoomFactor())) + ((currentMinutes * this.zoomFactor()) + this.ganttConfig.xStartPoint.time)
        let yRow = (this.ganttConfig.yStartPoint.header + this.ganttConfig.headerHeight) + (startIndex * this.ganttConfig.rowHeight);

        let line = this.drawLine({
            points: [
                xRow, yRow,
                xRow, yRow + (this.ganttConfig.rowHeight * this.drawData.length)
            ],
            lineColor: this.ganttConfig.color.currentLine,
            strokeWidth: 3
        }, GanttConstants.layers.time)

        line.moveToTop()
    }

    /* วาดเส้น และจุด Plan (Waypoint) */
    drawPlan(itemMain, indexMain, startIndex) {

        let yRow = (this.ganttConfig.yStartPoint.content) - (this.ganttConfig.rowHeight * 0.5) + (startIndex * this.ganttConfig.rowHeight) //คำนวนหา y
        let lineXY = []
        yRow += this.ganttConfig.rowHeight * (indexMain + 1);

        itemMain.waypointDetail.map((itemPlan, indexPlan) => {
            let objD = new Date(itemPlan.arrivePlan)

            if (objD.getHours() < 6) {

                objD.setDate(objD.getDate() - 1)
            }
            if (this.checkSelectedDate(objD)) {

                let xRow = this.calculatedFromDateX(itemPlan.arrivePlan);

                lineXY.push(xRow)
                lineXY.push(yRow)
                let assingOrder = itemPlan.assingOrder

                this.drawCircle({ //circle สำหรับtooltip
                    x: xRow,
                    y: yRow,
                    circleNumber: assingOrder,
                    radius:11,
                    //fillColor: 'red'//itemPlan.waypointStatusColor //this.ganttConfig.color.planPoint
                }, GanttConstants.layers.planTooltip,itemMain)

                this.drawCircle({
                    x: xRow,
                    y: yRow,
                    circleNumber: assingOrder,
                    fillColor: itemPlan.waypointStatusColor //this.ganttConfig.color.planPoint
                }, GanttConstants.layers.plan)

                this.drawText({
                    x: xRow - ((assingOrder < 10) ? 3.5 : 6.5), // คำนวนให้ text อยู่กลางวงลม
                    y: yRow - 6.5,
                    text: assingOrder,
                    fontColor: '#000000', //this.ganttConfig.color.fontPlan,
                    fontStyle: this.ganttConfig.fontStyle.bold,
                    fontSize: this.ganttConfig.fontSize.fontSizePoint
                }, GanttConstants.layers.plan)
            } else {
                let xRow = 0

                if (new Date(itemPlan.arrivePlan) < new Date(this.selectedDate())) {

                    xRow = this.ganttConfig.licenseTable.row.totalLicenseWidth
                } else {

                    xRow = this.stage.width()
                }

                lineXY.push(xRow)
                lineXY.push(yRow)
            }
        });


        let line = this.drawLine({
            points: lineXY,
            lineColor: 'grey',
            strokeWidth: 1.5,
            dash: [2, 2]
        }, GanttConstants.layers.plan)
        line.moveToBottom();
    }

    /* วาดจุด Actual */
    drawActual(itemMain, indexMain, startIndex) {

        if (itemMain.currentTruckDetail.timelineDateTime){

            let yRow = (this.ganttConfig.yStartPoint.content) - (this.ganttConfig.rowHeight * 0.8) + (startIndex * this.ganttConfig.rowHeight);
            yRow += this.ganttConfig.rowHeight * (indexMain + 1);
            
            let onclick = false
            let objD = new Date(itemMain.currentTruckDetail.timelineDateTime)

            if (objD.getHours() < 6) {

                objD.setDate(objD.getDate() - 1)
            }
            
            if (this.checkSelectedDate(objD)) {

                let xRow = this.calculatedFromDateX(itemMain.currentTruckDetail.timelineDateTime);

                this.drawCircle({
                    x: xRow+8,
                    y: yRow+9,
                    radius:12,
                    circleName: GanttConstants.layers.actualTooltip,
                    fillColor: null
                }, GanttConstants.layers.actualTooltip,itemMain , onclick)

                this.drawCircle({
                    x: xRow+8,
                    y: yRow+9,
                    radius:12,
                    fillColor: '#4710e1'//itemPlan.waypointStatusColor //this.ganttConfig.color.planPoint
                }, GanttConstants.layers.actual,null , onclick)
    
                this.drawSvg({
                    x: xRow,
                    y: yRow
                }, GanttConstants.layers.actual, null , onclick)

            }
        }
    }

    /*ใช้วันที่คำนวณจุด X ในการวาด*/
    calculatedFromDateX(date) {

        let objDate = new Date(date);
        let hour = objDate.getHours() < 6 ? (objDate.getHours() + this.ganttConfig.hours) : objDate.getHours()
        let dffHour = (hour - this.ganttConfig.startHour)
        let currentMinutes = objDate.getMinutes()
        let x = (dffHour * (this.ganttConfig.timeTable.hrWidth * this.zoomFactor())) + ((currentMinutes * this.zoomFactor()) + this.ganttConfig.xStartPoint.time)
        
        return x
    }

    calculatedTextCentredX(xStart, rectWidth, text) {
        return xStart + (rectWidth / 2) - this.calculatedApproxTextWidth(text);
    }

    calculatedTextCentredY(yStart, rectHeight, text) {
        return yStart + (rectHeight / 2) - this.calculatedApproxTextHeight(text);
    }

    calculatedApproxTextWidth(text) {
        let textPixel = 3.25;
        return text ? text.length * textPixel : 0;
    }

    calculatedApproxTextHeight(text) {
        let textPixel = 8;
        return textPixel;
    }

    /* Utility วาดเส้น */
    drawLine(params, layer, attibutes) {
        if (!params) {
            return
        }

        let x = 0
        let y = 0
        let stroke = params.lineColor
        let tension = 1
        let points = params.points
        let strokeWidth = (params.strokeWidth) ? params.strokeWidth : 0.5;//params.strokeWidth || 0.5

        let line = new Konva.Line({
            x,
            y,
            points,
            stroke,
            tension,
            strokeWidth
        });

        if (params.dash) {
            line.dash(params.dash)
        }

        if (attibutes) {
            line.on('click', () => {
            });

            line.on('mousemove', (e) => {
                this.hideTooltip()
                $("#divGantt").css('cursor', 'pointer')
                if (this.tooltipHandler != null) {
                    return this.tooltipHandler(attibutes)
                }
            });

            line.on('mouseout', (e) => {
                $("#divGantt").css('cursor', 'default')
                this.hideTooltip()
            });
        }

        this.layers[layer].add(line);
        return line;
    }

    /* Utility วาดสี่เหลี่ยม */
    drawRect(params, layer, attibutes) {
        if (!params) {
            return
        }

        let x = params.x;
        let y = params.y;
        let width = params.width;
        let height = params.height;
        let fill = params.fillColor || null;
        let strokeWidth = params.strokeWidth || 0.5;
        let cornerRadius = (params.cornerRadius) ? params.cornerRadius : null;
        let stroke = params.borderColor || '#d3d3d3'; //this.ganttConfig.color.border;



        let rect = new Konva.Rect({
            x,
            y,
            width,
            height,
            fill,
            cornerRadius,
            stroke,
            strokeWidth
        });

        if (attibutes) {
            this.bindingTooltipEvents(rect, attibutes)
        }

        this.layers[layer].add(rect);
        return rect;
    }

    /* Utility วาดText */
    drawText(params, layer, attibutes, onclick = true) {
        
        if (!params) {
            return
        }

        var text = new Konva.Text({
            x: params.x,
            y: params.y,
            text: params.text,
            fontSize: params.fontSize || 13,
            fontFamily: 'Calibri',
            fill: params.fontColor || this.ganttConfig.color.font,
            fontStyle: params.fontStyle || this.ganttConfig.fontStyle.normal
        });
        if (attibutes) {
            // if(attibutes.event == "click"){
            //     this.bindingOnClick(text, attibutes)
            // }else{
                this.bindingTooltipEvents(text, attibutes, onclick)
            //}
            
        }

        this.layers[layer].add(text);
        return text;
    }

    /* Utility วาดวงกลม */
    drawCircle(params, layer, attibutes ,onclick = true) {
        if (!params) {
            return
        }

        var circle = new Konva.Circle({
            x: params.x,
            y: params.y,
            circleName:params.circleName || '-',
            circleNumber: params.circleNumber,
            radius: params.radius || 9,
            fill: params.fillColor,
            stroke: params.lineColor || null,
            strokeWidth: params.widthLine || 0
        });

        if (attibutes) {
            this.bindingTooltipEvents(circle, attibutes ,onclick)
        }

        this.layers[layer].add(circle);
    }

    /* Utility วาดPolygon */
    drawPolygon(params, layer, attibutes) {
        if (!params) {
            return
        }

        var polygon = new Konva.RegularPolygon({
            x: params.x,
            y: params.y,
            sides: 3,
            radius: 9,
            fill: params.fillColor,
            stroke: null,
            strokeWidth: 0,
            rotation: 90
        });

        if (attibutes) {
            this.bindingTooltipEvents(polygon, attibutes)
        }

        this.layers[layer].add(polygon);
    }

    /* Utility วาดImage */
    drawImage(params, layer, attibutes,onclick = true) {
        if (!params) {
            return;
        }

        var image = new Konva.Image({
            x: params.x,
            y: params.y,
            image: params.image,
            width: params.width,
            height: params.height
        });

        if (attibutes) {
            this.bindingTooltipEvents(image, attibutes, onclick)
        }
        
        this.layers[layer].add(image);
        image.moveToTop()
    }

    /* Utility วาดSvg */
    drawSvg(params, layer,attibutes,onclick = true) {

        if (!params) {
            return
        }

        var path = new Konva.Path({
            x: params.x,
            y: params.y,
            data: 'M15.733,13.653h-.425V10.732a1.136,1.136,0,0,0-1.137-1.137h-.3V3.171a1.29,1.29,0,0,0-1.289-1.289h0L12.368,1.3A1.98,1.98,0,0,0,10.505,0H5.537A1.982,1.982,0,0,0,3.67,1.308l-.208.568h0A1.29,1.29,0,0,0,2.172,3.166V9.59h-.3A1.136,1.136,0,0,0,.739,10.727v2.921H.282A.283.283,0,0,0,0,13.93V15.5a.283.283,0,0,0,.282.282h1.2v.753a.568.568,0,0,0,.568.568H3.554a.568.568,0,0,0,.568-.568v-.753h7.8v.753a.568.568,0,0,0,.568.568H14a.568.568,0,0,0,.568-.568v-.753h1.165a.283.283,0,0,0,.282-.282V13.93A.279.279,0,0,0,15.733,13.653ZM3.836,3.462h8.37v2.8H3.836Zm.347,8.855a.537.537,0,0,1-.536.536H2.676a.537.537,0,0,1-.536-.536V12.3a.537.537,0,0,1,.536-.536h.971a.537.537,0,0,1,.536.536Zm6.341.129a.568.568,0,0,1-.568.568H6.092a.568.568,0,0,1-.568-.568v-3.6a.568.568,0,0,1,.568-.568H9.955a.568.568,0,0,1,.568.568Zm3.383-.129a.537.537,0,0,1-.536.536H12.4a.537.537,0,0,1-.536-.536V12.3a.537.537,0,0,1,.536-.536h.971a.537.537,0,0,1,.536.536Z',
            fill: 'white',
            scale: {
              x: 1,
              y: 1
            }
          });

          if (attibutes) {

            this.bindingTooltipEvents(path, attibutes, onclick)
        }

        this.layers[layer].add(path);
        path.moveToTop();
    }

    showTooltip(message) {

        let position = this.stage.getPointerPosition();
        let width = 0
        let height = 0
        let x = position.x
        let y = position.y

        var tooltip = new Konva.Label({
            y: y,
            opacity: 0.75
        });

        tooltip.add(new Konva.Tag({
            fill: 'black',
            pointerDirection: 'down',
            pointerWidth: 0,
            pointerHeight: 0,
            lineJoin: 'round',
            shadowColor: 'black',
            shadowBlur: 10,
            shadowOffset: 10,
            shadowOpacity: 0.5
        }));

        tooltip.add(new Konva.Text({
            text: message,
            fontFamily: 'Calibri',
            fontSize: 14,
            padding: 5,
            fill: '#ffffff'
        }));

        width = tooltip.width() // get ความกว้างของ tooltip
        height = tooltip.height() // get ความสูงของ tooltip

        // คำนวณหลบหน้าจอ
        if (this.stage.attrs.width < (x + (width / 2))) {
            x = x - (x + (width / 2) - this.stage.attrs.width)
        }

        if ((y - height) < 0) {
            y += (height + 5)
        }
        // คำนวณหลบหน้าจอ
        if (this.stage.height() < y) {
            y /= 1.5
        }
        tooltip.x(x)
        tooltip.y(y)


        this.layers[GanttConstants.layers.tooltip].add(tooltip);
        this.layers[GanttConstants.layers.tooltip].batchDraw();
    }

    hideTooltip() {
        this.layers[GanttConstants.layers.tooltip].removeChildren();
        this.layers[GanttConstants.layers.tooltip].batchDraw();
    }

    bindingOnClick(item,attibutes){
        var selt = this
        item.on('mouseenter', function () {
            selt.stage.attrs.container.style.cursor = 'pointer';
        });

        item.on('mouseleave', function () {
            selt.stage.attrs.container.style.cursor = 'default';
        });
        item.on('click', () => {
            return this.onClick(attibutes)
        });


    }

    bindingTooltipEvents(item, attibutes,onclick = true) {
        var selt = this
        if(onclick){
            item.on('mouseenter', function () {
                selt.stage.attrs.container.style.cursor = 'pointer';
            });
    
            item.on('mouseleave', function () {
                selt.stage.attrs.container.style.cursor = 'default';
            });

            item.on('click', () => {
            return this.onClick(attibutes)
            });
            
        }

        item.on('mouseover', () => {
            this.hideTooltip()
            if (this.tooltipHandler != null) {
                
                return this.tooltipHandler(attibutes, item)
            }
        });

        item.on('mouseout', () => {
            
            this.hideTooltip()
        });

    }

    setFormatTextForDisplay(text ,length = 25) {
        let maxLength = length;
        let format = "";

        if (text != null && text.length > maxLength) {
            format = text.substring(0, maxLength) + "..";
        }
        else {
            format = text;
        }

        return format;
    }
}

export default {
    viewModel: ShipmentTimelineControl,
    template: templateMarkup
};