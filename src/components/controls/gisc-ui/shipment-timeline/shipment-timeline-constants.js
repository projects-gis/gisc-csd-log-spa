﻿export class GanttConfig {
    static get DrawingConfig() {
        return class GanttDrawingConfig {
            static get margin() {
                return class GanttMarginConfig {
                    static get left() {
                        return 0;
                    }

                    static get top() {
                        return 0;
                    }

                    static get right() {
                        return 0;
                    }

                    static get bottom() {
                        return 10;
                    }
                }
            }

            static get tooltipConfig() {
                return class GanttTooltipConfig {
                    static get padding() {
                        return class GanttTooltipPaddingConfig {
                            static get left() {
                                return 5;
                            }
        
                            static get top() {
                                return 5;
                            }
        
                            static get right() {
                                return 5;
                            }
        
                            static get bottom() {
                                return 5;
                            }
                        }
                    }
                }
            }

            static get fontStyle() {
                return class GanttFontStyleConfig {
                    static get normal() { //ฟ้อนตัวธรรมดา
                        return "normal";
                    }

                    static get bold() { //ฟ้อนตัวหนา
                       return "bold"; 
                    }
                }
            }

            static get fontSize() {
                return class GanttFontSizeConfig {
                    static get fontSizePoint() { //ขนาดตัวฟ้อนในจุดplan
                        return 13;
                    }
                }
            }

            static get color() {
                return class GanttColorConfig {
                    static get currentLine() {
                        return "#ffc000"; //สีของเส้น current time
                    }

                    static get colorTitle() {
                        return "#e7e6e6"; //สีกล่องหัวตาราง
                     }

                    static get lineChart() {
                       return "#e7e6e6"; //สีของกรอบ gantt chart

                    }

                    static get font() {
                        return "#000"; //สีฟ้อนทั่วไป
                    }

                    static get fontPlan() {
                        return "#767171"; //สีฟ้อนของ Plan
                    }

                    static get planPoint() {
                        return "#dbdbdb"; //สีของจุด planpoint
                    }

                    static get planLine() {
                        return '#a6a6a6'; //สีของเส้น plan
                    }

                    static get colorLicense() {
                        //return '#e7e6e6'; //สีของกล่อง layer license
                        return '#ffffff';
                    }

                    static get borderColorLicense() {
                        //return '#ffffff'; //สีกรอบของ layer license
                        return "#000000";//สีกรอบของ layer license
                    }

                    static get lineColorTime() {
                        return '#a6a6a6'; //สีเส้นแนวนอนของ layer time
                    }

                    static get lineColorTimeHalf() {
                        return '#a6a6a6'; //สีเส้นแนวนอนของ layer time แบบครึ่งชั่วโมง
                    }

                    static get lineColorTimeQuarter() {
                        return '#a6a6a6'; //สีเส้นแนวนอนของ layer time แบบ15นาที
                    }

                    static get colorTime() {
                        return '#fbfbfb'; //สีของกล่อง layer time
                    }
                    
                    static get border() {
                        //return "#dbdbdb"; //สีของขอบทั่วไป
                        return "#000000"; //สีของขอบทั่วไป
                    }
                }
            }

            static get row() {
                return 20;
            }

            static get rowHeight() {
                return 33;
            }

            static get headerHeight() {
                return 30;
            }

            static get startHour() { //เริ่ม chart ที่ 6 โมง
                return 6;
            }

            static get hours() { //จำนวนชั่วโมงในการแสดงคอลัม
                return 24;
            }

            static get licenseTable() {
                return class GanttLicenseTableConfig {
                    static get row() {
                        return class GanttLicenseTableRow {
                            static get statusWidth() {
                                return 70;
                            }

                            static get licenseWidth() {
                                return 70;
                            }

                            static get shipmentNumberWidth() {
                                return 190;
                            }

                            static get totalLicenseWidth() {
                                return this.statusWidth + this.licenseWidth + this.shipmentNumberWidth;
                            }
                        }
                    }

                    static get shipmentStatus() {
                        return [{
                                status: 1,
                                color: "#FF0000"
                            },
                            {
                                status: 2,
                                color: "#FFFF00"
                            },
                            {
                                status: 3,
                                color: "#00FF00"
                            }
                        ];
                    }
                }
            }

            static get timeTable() {
                return class GanttTimeTableConfig {
                    static get hrWidth() {
                        return 60;
                    }
                }
            }

            static get xStartPoint() {
                return class XStartPoint {
                    //ขอบซ้ายตารางหลัก และ ตารางlicense
                    static get license() {
                        return GanttConfig.DrawingConfig.margin.left;
                    }

                    static get time() {
                        return GanttConfig.DrawingConfig.margin.left + GanttConfig.DrawingConfig.licenseTable.row.totalLicenseWidth;
                    }
                }
            }

            static get yStartPoint() {
                return class YStartPoint {

                    static get header() {
                        return GanttConfig.DrawingConfig.margin.top;
                    }

                    static get content() {
                        return GanttConfig.DrawingConfig.margin.top + GanttConfig.DrawingConfig.headerHeight;
                    }
                }
            }
        }
    }

}

export class GanttConstants {
    static get layers() {
        return class GanttDrawingLayers {

            static get header(){
                return "Header";
            }

            static get license() {
                return "LicenseLayer";
            }
            static get time() {
                return "TimeLayer";
            }

            static get timeLine() {
                return "TimeLineLayer";
            }

            static get plan() {
                return "PlanLayer";
            }
            static get actual() {
                return "ActualLayer";
            }
            static get current() {
                return "CurrentLayer";
            }
            static get tooltip() {
                return 'Tooltip';
            }
            static get footer() {
                return 'Footer';
            }
            static get planTooltip() {
                return 'PlanTooltip';
            }
            static get actualTooltip() {
                return 'ActualTooltip';
            }

            
        }
    }
}