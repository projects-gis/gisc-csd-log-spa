import ko from "knockout";
import templateMarkup from "text!./dropdownMultiSelect.html";
import ControlBase from "../../controlbase";

class DropdownMultiSelect extends ControlBase {
    constructor(params) {
        super(params);
        // Properties
        this.options = this.ensureObservable(params.options);
        this.optionsID = this.ensureNonObservable(params.optionsID);
        this.optionsText = this.ensureNonObservable(params.optionsText);
        this.optionsParent = this.ensureNonObservable(params.optionsParent, "parent");
        this.optionsChildren = this.ensureNonObservable(params.optionsChildren, "children");
        this.disableNodes = this.ensureObservable(params.disableNodes);
        this.autoSelection = this.ensureObservable(params.autoSelection, true);
        this.selectable = this.ensureNonObservable(params.selectable, true);
        this.optionsCaption = this.ensureObservable(params.optionsCaption);
        this.value = this.ensureObservable(params.value, this.optionsCaption);
        this.name = ko.observable();
        this.zIndex = this.ensureNonObservable(params.zIndex, '1');

        this.placeholderFilterText = ko.observable(this.i18n("Common_Search")());

        this.onClickEventRef = null;
        this.clearSelectedItemRef = null;

        this.arrayValue = ko.observableArray();
        this.arrayValueSubscribe = this.arrayValue.subscribe((newValue) => {
            if (newValue) {
                var data = _.filter(newValue, function(id) { 
                    return id != 0; 
                 });
                var n = data;
                this.hasSelected(true);
                if(n.length < 2){
                    this.name(this.findNodeName(this.options(), n[0]));
                }else{
                    this.name(n.length + " " + this.i18n("Common_Selected")());
                }
                this.value(n);
            }

        });
        this.visibleCloseBtn = ko.observable();
        this.selectedValueSubscribe = this.value.subscribe((newValue) => {
            if (!newValue) {
                this.arrayValue("");
                this.name(null);
                this.visibleCloseBtn(false);
            } else {
                this.visibleCloseBtn(true);
            }
            // this.isDropdownOpened(true);
        });


        this.isDropdownOpened = ko.observable(false);
        this.hasSelected = ko.observable();

        this._enableSubscribe = this.enable.subscribe((value) => {
            var $el = $("#" + this.id);
            var $clear = $(".app-remove-icon" + "." + this.id);
            if (value) {
                $el.on('click', this.onClickEventRef);
                $clear.on("click", this.clearSelectedItemRef);
            } else {
                $el.off('click', this.onClickEventRef);
                $clear.off("click", this.clearSelectedItemRef);
            }
        });



        this.isFocus = ko.observable(false);
        this.subFocus = this.isFocus.subscribe((newValue)=> {
            // new valule change logic
            if(!this.enable()){
                this.isFocus(false);
            }
        });

        //add select all
        if(this.selectable && _.size(this.options())){
            var selectable = {};
            var data = [];

            selectable[this.optionsID] = 0;
            selectable[this.optionsText] = this.i18n("Common_CheckAll")();
            selectable[this.optionsParent] = "#";

            _.each(this.options(), (obj) => {
                obj[this.optionsParent] = selectable[this.optionsID]
            });

            this.options().unshift(selectable);
            this.options(this.options());
        }

        

    }


    findNodeName(object, id) {
        var name;
        _.each(object, (obj) => {
            var nodeID = this.ensureNonObservable(obj[this.optionsID]);
            if (nodeID == id) {
                name = this.ensureNonObservable(obj[this.optionsText]);
                // console.log("Found at parent: ",name);
            } else {
                var child = obj[this.optionsChildren];
                if (child && child.length > 0 && !name) {
                    name = this.findNodeName(child, id);
                    // console.log("Found at child: ",name);
                }
            }
        });
        return name;
    }

    onClick(e) {
        //Check if click input search will not close dropdown
        var filterId = (e.target) ? e.target.id : null;
        if(e.type === "click" || e.charCode === 32){
            if(this.hasSelected() === true){
                this.isDropdownOpened(true);
                this.hasSelected(false);
            }
            else{
                // //click input search
                if(filterId === this.id + "FilterText"){
                    this.isDropdownOpened(this.isDropdownOpened());
                }else{
                    
                    var $el = $("#" + this.id);
                    $el.find("i:not(a>i)").removeClass("jstree-icon");
                    $el.find("ul.jstree-children>li").css("margin-left", "0");

                    this.isDropdownOpened(!this.isDropdownOpened());
                }        
            }
            
        }
        
    }

    clearSelectedItem(e) {
        this.value(null);
        this.isDropdownOpened(false);
        //clear dropdown treeview
        $("#" + this.id + "FilterText").val(null);
        $("#" + this.id + " .app-treeview-container .jstree-container-ul").find("li").show();
        e.stopPropagation();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var h = !this.value() ? false : true;
        this.visibleCloseBtn(h);
        this.name(this.findNodeName(this.options(), this.value()));
        if (this.value()) {
            this.arrayValue([this.value()]);
        }
    }

    onDomReady() {
        // if(!this.enable()) return;

        var self = this;

        this.onClickEventRef = this.onClick.bind(this);
        this.clearSelectedItemRef = this.clearSelectedItem.bind(this);

        if (this.enable()) {
            var $el = $("#" + self.id);
            var $clear = $(".app-remove-icon" + "." + self.id);
            $el.on('click keypress', this.onClickEventRef);
            $clear.on("click", this.clearSelectedItemRef);

            $el.focusin((e)=>{
                self.hasSelected(false);
            });
            $el.focusout((e) => {
                // Check if the original click comes from children or outside DOM tree.
                var isInnerElement = $el.find(e.relatedTarget).length > 0;
                var selectedId = (e.relatedTarget) ? e.relatedTarget.id : null;

                //fix dropdown is open 2 times
                if(selectedId == self.id) {
                    self.isDropdownOpened(true);
                }

                if(!isInnerElement && selectedId != self.id) {
                    self.isDropdownOpened(false);
                }
            });
        }

        

        $("#" + self.id +"FilterText").keyup(function (e) {
            var filterText = $(this).val().toLowerCase();
            if(filterText.length !== ""){
                $(this).parent().find("li").hide();
                $(this).parent().find(".jstree-anchor").each(function() {
                    let text = $(this).text().toLowerCase();
                    if(text.match(filterText)) {
                        $(this).show();
                    }   
                });
                $(this).parent().find(".jstree-anchor").each(function() {
                    let text = $(this).text().toLowerCase();
                    if(text.match(filterText)) {
                        $(this).parents("ul, li").each(function () {
                            $(this).show();
                        });
                    }   
                }); 
            }
            else {
                //if not filterText null reset treeview
                $(this).parent().find("li").show();
            }
        });
        
    }

    onUnload() {
        var $el = $("#" + self.id);
        var $clear = $(".app-remove-icon" + "." + this.id);

        this._enableSubscribe.dispose();
        this.arrayValueSubscribe.dispose();

        $el.unbind();
        $clear.unbind();
        this.value = null;
        this.options = null;
        this.hasSelected = null;
    }
}

export default {
    viewModel: DropdownMultiSelect,
    template: templateMarkup
};