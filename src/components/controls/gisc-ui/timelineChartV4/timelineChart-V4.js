﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./timelineChart-V4.html";
import "am4core";
import "am4charts";
import "am4themes_animated";
import moment from "moment";

class TimelineChartV4 extends ControlBase {
    constructor(params) {
        super(params);
        this.data = this.ensureObservable(params.data);
        this.graphId = ko.observable(null);
        this.mainColor = ko.observable("#20af80");
        this.onClickEvent = this.ensureFunction(params.onClickEvent, null);
        this.onTimelineZoom = this.ensureFunction(params.onTimelineZoom, ()=>{});
        this.lineChartId = this.ensureNonObservable(this.makeid(), "timeline-line-chart-id");
        this.ganttChartId = this.ensureNonObservable(this.makeid(), "timeline-gantt-chart-id");
        this.listenerEvent = ko.observable();
        this.timeOutRangechanged = null;
    }

    afterRender(element) {
        if(this.data()["graphs"] != undefined) {
            // Themes begin
            am4core.useTheme(am4themes_animated);
            // Themes end
            this.createLineChart();
            this.createGanttChart();
        }        
    }

    generateLineChartData() {

        var UTC =  this.data().isIos?"+07:00":"";
        var graphs = this.data()["graphs"];
        var events = this.data()["events"];
        var chartData = [];
        _.forEach(graphs, (item, index)=>{
            let dt = new Date(item.date+UTC);
            let data = {
                date: dt,
                speed: item.speed
            }
            let fData = _.find(events, (o)=>{
                return o.date === item.date;
            });
            if(_.size(fData)){
                fData.descriptionHTML = this.generateDescriptionEvent(fData.imagePath, fData.description, data.speed);
                data.stockEvent = fData;
            }
            chartData.push(data);
        })

        return chartData;
    }

    generateGanttChartData() {
        var UTC =  this.data().isIos?"+07:00":"";
        var items = this.data()["graphs"];
        // items.splice(10);
        var chartData = [];
        _.forEach(items, (item, index)=>{

            let startDate = new Date(item.date+UTC);
            let endDate = new Date(item.date+UTC);
            
            if((index+1) != _.size(items)){
                endDate = new Date(items[index+1].date+UTC);
                endDate = new Date(endDate.setSeconds(endDate.getSeconds(), -1));
            }else{
                endDate = new Date(endDate.setSeconds(endDate.getSeconds(), 1));
            }
            
            chartData.push({
                startDate: startDate,
                endDate: endDate,
                formatStartDateTime: moment(startDate).format('DD-MM-YYYY HH:mm:ss'),
                formatEndDateTime: moment(endDate).format('DD-MM-YYYY HH:mm:ss'),
                formatStartDate: moment(startDate).format('DD-MM-YYYY'),
                formatEndDate: moment(endDate).format('DD-MM-YYYY'),
                formatStartTime: moment(endDate).format('HH:mm:ss'),
                formatEndTime: moment(endDate).format('HH:mm:ss'),
                color: (item.color == "transparent") ? "rgba(255, 0, 0, 0)" : item.color,
                name: " "
            });

        });
        return chartData;
    }

    createLineChart() {
        // Create chart instance
        var chart = am4core.create(this.lineChartId, am4charts.XYChart);

        // Add data
        chart.data = this.generateLineChartData();

        // Create axes
        var xAxis = chart.xAxes.push(new am4charts.DateAxis());
        xAxis.renderer.minGridDistance = 60;

        var yAxis = chart.yAxes.push(new am4charts.ValueAxis());
        yAxis.min = 0;
        
        // Create series
        var series = chart.series.push(new am4charts.LineSeries());
        series.dataFields.valueY = "speed";
        series.dataFields.dateX = "date";
        // series.strokeWidth = 2;
        // series.minBulletDistance = 10;
        series.tooltipText = "{valueY}";
        // series.tooltip.pointerOrientation = "vertical";
        // series.tooltip.label.padding(12,12,12,12);
        series.fill = am4core.color("#20af80");
        series.stroke = am4core.color("#20af80");
        series.tooltip.background.cornerRadius = 5;
        series.tooltip.background.strokeOpacity = 0;
        series.tooltip.getFillFromObject = false;
        series.tooltip.background.fill = am4core.color("#FFFFFF");
        series.tooltip.label.fill = am4core.color("#000000");
        // series.tooltip.pointerOrientation = "vertical";
        // series.tooltip.label.minWidth = 40;
        // series.tooltip.label.minHeight = 40;
        // series.tooltip.label.textAlign = "middle";
        // series.tooltip.label.textValign = "middle";
        // series.adapter.add("tooltipText", (text, target) => {
        //     let data = target.tooltipDataItem.dataContext;
        //     if (data && data.stockEvent) {
        //         return "";
        //     }
        //     return text;
        // });

        if(_.size(this.data().events)){
            series.adapter.add("tooltipHTML", (text, target) => {
                let label = "";
                let data = target.tooltipDataItem.dataContext;
                if (data && data.stockEvent) {
                    label = data.stockEvent.descriptionHTML;
                }else{
                    label = "<div align='center'>" + data.speed + "</div>";
                }
                return label;
            });
            
            var bullet = series.bullets.push(new am4charts.CircleBullet());
            bullet.properties.scale = 2;
            bullet.dy = -12;
            // bullet.fill = am4core.color("#f2f2f2");
            // bullet.stroke = am4core.color("#f2f2f2");
            bullet.adapter.add("stroke", (text, target) => {
                let color = "";
                let data = target.tooltipDataItem && target.tooltipDataItem.dataContext;
                if (data && data.stockEvent) {
                    color = data.stockEvent.fontColor;
                }
                return color;
            });
            bullet.adapter.add("fill", (text, target) => {
                let color = "";
                let data = target.tooltipDataItem && target.tooltipDataItem.dataContext;
                if (data && data.stockEvent) {
                    color = data.stockEvent.backgroundColor;
                }
                return color;
            });
            
            // var circle = bullet.createChild(am4core.Circle);
            // circle.stroke = "#000";
            // circle.strokeWidth = 1;
            // circle.radius = 10;
            // circle.dy = -10;
            // circle.adapter.add("fill", (text, target) => {
            //     let color = "";
            //     let data = target.tooltipDataItem && target.tooltipDataItem.dataContext;
            //     if (data && data.stockEvent) {
            //         color = data.stockEvent.backgroundColor;
            //     }
            //     return color;
            // });
    
            // var line = bullet.createChild(am4core.Line);
            // line.stroke = "#000";
            // line.strokeWidth = 1;
            // line.height = 10;
    
            var label = bullet.createChild(am4core.Label);
            label.strokeWidth = 0;
            label.dy = -5;
            label.textAlign = "middle";
            label.horizontalCenter = "middle";
            label.properties.scale = 0.5;
            label.adapter.add("fill", (text, target) => {
                let color = "";
                let data = target.tooltipDataItem && target.tooltipDataItem.dataContext;
                if (data && data.stockEvent) {
                    color = data.stockEvent.fontColor;
                }
                return color;
            });
            
            label.adapter.add("text", (text, target) => {
                let label = "";
                let data = target.tooltipDataItem && target.tooltipDataItem.dataContext;
                if (data && data.stockEvent) {
                    label = data.stockEvent.text;
                }
                return label;
            });
            
    
            bullet.events.on("inited", (event) => {
                let data = event.target.dataItem && event.target.dataItem.dataContext;
                if (data && !data.stockEvent) {
                    event.target.disabled = true;
                }
            });
    
            bullet.events.on("hit", (event) => {
                let data = event.target.dataItem && event.target.dataItem.dataContext;
                if (data && data.stockEvent) {
                    this.onClickEvent(data.stockEvent);
                }
            });
        }

        

        // Add scrollbar
        chart.scrollbarX = new am4charts.XYChartScrollbar();
        chart.scrollbarX.series.push(series);
        chart.scrollbarX.minHeight = 40;
        chart.scrollbarX.height = 40;
        chart.scrollbarX.series.each(function(s){
            s.bullets.each(function(b){
                b.clones.each(function(c){
                    c.properties.disabled = true;
                });
            });
        });
        // chart.scrollbarX.series.events.on("inited", (event) => {
        //     // let data = event.target.dataItem && event.target.dataItem.dataContext;
        //     // if (data && !data.stockEvent) {
        //     //     event.target.disabled = true;
        //     // }
        //     event.target.disabled = true;
        // });
        // chart.scrollbarX.series.bullet.events.on("inited", (event) => {
        //     // let data = event.target.dataItem && event.target.dataItem.dataContext;
        //     // if (data && !data.stockEvent) {
        //     //     event.target.disabled = true;
        //     // }
        //     event.target.disabled = true;
        // });

        // Add cursor
        // chart.cursor = new am4charts.XYCursor();
        // chart.cursor.xAxis = dateAxis;
        // chart.cursor.snapToSeries = series;

        // Add cursor
		chart.cursor = new am4charts.XYCursor();
		chart.cursor.behavior = "zoomXY";

        chart.cursor.events.on("zoomended", (ev) => {
            const axis = ev.target.chart.xAxes.getIndex(0);
            const start = axis.positionToDate(axis.toAxisPosition(ev.target.xRange.start));
            const end = axis.positionToDate(axis.toAxisPosition(ev.target.xRange.end));
            let event = {
                dateTime:{
                    startDate: start,
                    endDate: end
                },
                mode: "zoomended"
            };
            this.onTimelineZoom(event);
            this.listenerEvent(event);
        });

        chart.scrollbarX.events.on("rangechanged", (ev) => {
            const axis = ev.target.chart.xAxes.getIndex(0);
            const start = axis.positionToDate(ev.target.start);
            const end = axis.positionToDate(ev.target.end);
            
            clearTimeout(this.timeOutRangechanged);
            this.timeOutRangechanged = setTimeout(()=>{
                if(_.isDate(start) && _.isDate(end)){
                    let event = {
                        dateTime:{
                            startDate: start,
                            endDate: end
                        },
                        mode: "rangechanged"
                    };
                    this.onTimelineZoom(event);
                    this.listenerEvent(event);
                }
            }, 1000);
        });

        // chart.scrollbarX.events.onAll(function(ev) {
        //     console.log("something happened ", ev);
        // }, this);

        chart.zoomOutButton.events.on("hit", (ev) => {
            let event = {
                mode: "zoomOut"
            };
            this.onTimelineZoom(event);
            this.listenerEvent(event);
        });

    }

    createGanttChart() {

        var chart = am4core.create(this.ganttChartId, am4charts.XYChart);
        // chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

        chart.data = this.generateGanttChartData();

        var yAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        yAxis.dataFields.category = "name";
        yAxis.renderer.grid.template.opacity = 0;
        
        var xAxis = chart.xAxes.push(new am4charts.DateAxis());
        xAxis.renderer.minGridDistance = 60;
        xAxis.renderer.grid.template.opacity = 0;
        xAxis.renderer.labels.template.disabled = true;

        var series = chart.series.push(new am4charts.ColumnSeries());
        series.columns.template.width = am4core.percent(100);
        // series.columns.template.tooltipText = "{formatStartTime} - {formatEndTime}";

        series.dataFields.openDateX = "startDate";
        series.dataFields.dateX = "endDate";
        series.dataFields.categoryY = "name";
        series.columns.template.propertyFields.fill = "color";
        series.columns.template.propertyFields.stroke = "color";
        series.columns.template.strokeOpacity = 1;

        chart.zoomOutButton.disabled = true;


        this.listenerEvent.subscribe((data)=>{
            if(data.mode == "zoomOut"){
                xAxis.zoom({start:0,end:1});
            }else{
                xAxis.zoomToDates(
                    data.dateTime.startDate,
                    data.dateTime.endDate
                );
            }
        })

    }

    isValidDate(dateTime) {
        var d = new Date(dateTime);
        return d.getFullYear() && d.getMonth() && d.getDate();
    }

    generateDescriptionEvent(img, des, speed) {
        var result = null;
        var image = null;
        var description = null;

        if(img){
            image = "<img src='" + this.resolveUrl(img) + "'>";
        }

        if(des){
            description = des;
        }
        
        if(image || description){
            result = "<div align='center' style='padding:10px'>";
            result += image;
            result += (image)?"<br/>":"";
            result += description;
            result += "<br/>";
            result += this.i18n("Common_Speed")() + " " + speed;
            result += "</div>";
        }

        return result;
    }

    makeid() {
        var text = "G";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 10; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
}

export default {
viewModel: TimelineChartV4,
    template: templateMarkup
};
