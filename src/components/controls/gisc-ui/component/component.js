import ko from "knockout";
import i18nextko from "knockout-i18next";
import templateMarkup from "text!./component.html";
import hljs from "highlightjs";

class ComponentLoader {
    constructor() {
        this.viewModel = null;
        this.contentTemplateNodes = null;
        this.supportSourceCodeHighlight = false;
        this.isViewModelExist = ko.observable(false);
        this.isLoaded = ko.pureComputed(this.computeFinishLoad, this);
        this.isError = ko.pureComputed(this.computeIsError, this);
        this.additionalCssClasses = null;
        this.cssClasses = ko.pureComputed(function() {
            var cssClass = "component-container";

            // Support observable binding.
            if(_.isFunction(this.additionalCssClasses)){
                cssClass += " ";
                cssClass += this.additionalCssClasses();
            }
            // Support static binding.
            else if(this.additionalCssClasses) {
                cssClass += " ";
                cssClass += this.additionalCssClasses;
            }

            return cssClass;
        }, this);

        this.viewStateKey = null;
        this._bladeRef = null;
    }
    computeFinishLoad() {
        var isLoaded = false;

        if (this.isViewModelExist()) {
            isLoaded = this.viewModel.isLoaded();
        }

        return isLoaded;
    }
    computeIsError() {
        var isError = false;

        if (this.isViewModelExist()) {
            isError = this.viewModel.isError();
        }

        return isError;
    }
    afterRender(element, parent) {
        var self = this;

        // Deteact view model from parent binding context instead of inject from parameters.
        self.viewModel = parent;
        self.isViewModelExist(true);

        if (self.viewModel.onViewAttach) {
            var task = self.viewModel.onViewAttach();

            if (task && task.state) {
                // Using promise so waiting sucess from view model response.
                task.done((data) => {
                    // On success
                    self.viewModel.isLoaded(true);
                    self.viewModel.isFirstLoad(false);
                }).fail((xhr) => {
                    self.viewModel.isError(true);
                    self.viewModel.errorMessage((xhr && xhr.responseJSON) ? xhr.responseJSON.message : this.i18n("Common_ErrorInstruction")());
                    self.viewModel.isLoaded(true);
                });
            } else {
                // No promise so display content immediatly.
                self.viewModel.isLoaded(true);
                self.viewModel.isFirstLoad(false);
            }
        } else {
            // Cannot find any loading method so render view immediatly.
            self.isLoaded(true);
            self.viewModel.isLoaded(true);
            self.viewModel.isFirstLoad(false);
        }
    }

    /**
     * Handle on component template DOM is ready to use.
     * This function is executed after onAfterRender.
     * @private
     * @param {any} element
     */
    domReady(element, parents) {
        // Load ViewState

        if(this.viewModel.onLoadViewState) {
            // Resolve blade from parent context
            this._bladeRef = _.find(parents, (p) => {
                // Cannot check with constructor because minify will rename class.
                // We need to check by blade property.
                return !_.isEmpty(p.bladeSize);
            });

            if(!_.isNil(this._bladeRef)) {
                this.viewStateKey = this._bladeRef.id + this.viewModel.id;
                var state = this._bladeRef.getItem(this.viewStateKey);
                if(_.isNil(state)) {
                    state = {};
                }
                this.viewModel.onLoadViewState(state); 
            }
        }

        // Invoke View Model if implemented.
        if (this.viewModel.onDomReady) {
            this.viewModel.onDomReady();
        }

        // Use hljs for developer portal sample
        if(this.supportSourceCodeHighlight) {
            $(element).find("pre code").each(function(i, block) {
                hljs.highlightBlock(block);
            });
        }
    }

    /**
     * Handle dispose reference object, release unused memory
     */
    dispose() {
        // Save ViewState
        if(this.viewModel.onSaveViewState) {
            if(!_.isNil(this._bladeRef)) {
                var state = this._bladeRef.getItem(this.viewStateKey);
                if(_.isNil(state)) {
                    state = {};
                }
                this.viewModel.onSaveViewState(state);
                this._bladeRef.setItem(this.viewStateKey, state);
            }
        }

        // Unreference memvar
        this.viewStateKey = null;
        this._bladeRef = null;

        // Trigger to VM for dispose logic.
        if (this.viewModel.onViewDetach) {
            var task = this.viewModel.onViewDetach();
        }
    }

    /**
     * Get i18n value for given key
     * @public
     * @param {string} key
     * @param {any} options
     * @returns
     */
    i18n(key, options) {
        return i18nextko.t(key, options);
    }
}

export default {
    viewModel: {
        createViewModel: function(params, componentInfo) {
            var vm = new ComponentLoader();
            // vm.viewModel = params.viewModel; do not passing from caller.

            // Cache template note for deplayed binding.
            vm.contentTemplateNodes = componentInfo.templateNodes;

            if(params !== undefined) {
                vm.supportSourceCodeHighlight = params.codeHighlight ? params.codeHighlight : false;
                vm.additionalCssClasses = params.additionalCssClasses ? params.additionalCssClasses : null;
            }
            
            return vm;
        }
    },
    template: templateMarkup
};