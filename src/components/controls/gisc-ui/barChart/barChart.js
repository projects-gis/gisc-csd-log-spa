﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./barChart.html";
import ChartJs from "chart";
import Logger from "../../../../app/frameworks/core/logger";

class BarChart extends ControlBase {
    constructor(params) {
        super(params);

        ChartJs.defaults.global.defaultFontFamily = "dbhx";
        ChartJs.defaults.global.defaultFontSize = 16;

        this.data = this.ensureObservable(params.data);
        this.type = this.ensureNonObservable(params.type, "bar");
        this.selected = this.ensureFunction(params.selected);

        this.clickData = this.data().options.bindingData;

        this.chartInstance = null;

        this.isArrayChange = this.data.subscribe(function (changes) {
            this.clickData = changes.options.bindingData;
            this.createChart(changes);
        }, this);
    }

    onChartClick(evt) {
        var activePoints = this.chartInstance.getElementsAtEvent(evt);
        if (activePoints.length === 0) {
            Logger.warn("Please click on segment.");
        } else {
            return this.selected ? this.selected(this.clickData[activePoints[0]._index]) : null;
        }
    }

    afterRender(element) {
        this.createChart(this.data());
    }

    onUnload() {
        this.chartInstance.destroy();
        this.data = null;
    }

    createChart(val) {
        var self = this;
        var ctx = $("#" + self.id);

        try {
            if (!this.chartInstance) {
                this.chartInstance = new ChartJs(ctx, {
                    type: this.type == "horizontal" ? "horizontalBar" : "bar",

                    // The data for our dataset
                    data: {
                        labels: [],
                        datasets: [{
                            label: [],
                            backbroundColor: [],
                            data: []
                        }]
                    },

                    // Configuration options go here
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: false
                        },
                        tooltips: {
                            cornerRadius: 1,
                            backgroundColor: "#333333",
                            bodyFontFamily: "dbhx",
                            bodyFontColor: "rgb(255,209,23)",
                            yPadding: 8,
                            callbacks: val.options.tooltipCallback
                        },
                        title: {
                            display: true,
                            text: ""
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value, index, values) {
                                        if (Math.floor(value) === value) { // Check ว่าเป็น int หรือเปล่า
                                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else if ($.type(value) === "string") { // Check ว่าเป็น String หรือเปล่า
                                            return value;
                                        }
                                    }
                                },
                                scaleLabel: {
                                    display: val.options.yAxes ? true : false,
                                    labelString: val.options.yAxes ? val.options.yAxes.title : "",
                                    fontColor: 'rgb(70, 183, 143)',
                                }
                            }],
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    callback: function (value, index, values) {
                                        if (Math.floor(value) === value) { // Check ว่าเป็น int หรือเปล่า
                                            return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                        } else if ($.type(value) === "string") { // Check ว่าเป็น String หรือเปล่า
                                            return value;
                                        }
                                    }
                                },
                                scaleLabel: {
                                    display: val.options.xAxes ? true : false,
                                    labelString: val.options.xAxes ? val.options.xAxes.title : "",
                                    fontColor: 'rgb(70, 183, 143)'
                                }
                            }]
                        }
                    }
                });

            }

            this.chartInstance.data.labels = [];
            this.chartInstance.data.dataset = [];
            this.chartInstance.options.title.text = this.data().options.title;

            this.chartInstance.options.scales.yAxes[0].scaleLabel.labelString = val.options.yAxes ? val.options.yAxes.title : "";
            this.chartInstance.options.scales.xAxes[0].scaleLabel.labelString = val.options.xAxes ? val.options.xAxes.title : "";

            val.data.forEach(function (element, ind) {
                this.chartInstance.data.labels = val.options.labels;
                this.chartInstance.data.datasets[0] = element;
            }, this);

            var maxData = parseInt(Math.max.apply(Math, this.chartInstance.data.datasets[0].data));
            var stepSize, maxValue, formula;

            if (maxData < 10) {
                stepSize = Math.ceil(maxData / 5);
                maxValue = (stepSize * 5);
            } else {
                formula = (Math.pow(10, ((maxData.toString().length) - 2))) * 5;
                maxValue = ((Math.ceil(maxData / formula)) * formula);
                stepSize = maxValue / 5;
            }

            this.chartInstance.options.scales.xAxes[0].ticks.max = maxValue;
            this.chartInstance.options.scales.xAxes[0].ticks.stepSize = stepSize;
            this.chartInstance.options.scales.yAxes[0].ticks.max = maxValue;
            this.chartInstance.options.scales.yAxes[0].ticks.stepSize = stepSize;
            this.chartInstance.update();
        } catch (error) {
        }
    }
}

export default {
    viewModel: BarChart,
    template: templateMarkup
};