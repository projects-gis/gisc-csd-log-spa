﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./radarChart.html";
import Logger from "../../../../app/frameworks/core/logger";
import "amcharts";
import "amcharts-radar";

class RadarChart extends ControlBase {
    constructor(params) {
        super(params);
        
        this.data = this.ensureObservable(params.data);
        // this.nameRadar = this.ensureObservable(this.data().name)
        this.onClickRadarChart = this.ensureFunction(params.onClickRadarChart, null);

    }

    afterRender(element) {
        this.checkData()
        this.data.subscribe(() => {
            this.checkData()
        })
    }

    checkData() {
        var self = this;
        this.createChart();
        if (this.data() != undefined) {
           if (this.data()['length'] != 0) {
               this.createChart();
           } else {
               $("#" + self.id).html(this.i18n('Common_DataNotFound')())
           }
        }
    }

    createChart() {
        var self = this;
        var chart = {
            "type": "radar",
            "theme": "light",
            "dataProvider": this.data(),
            "valueAxes": [{
                "axisTitleOffset": 20,
                "minimum": 0,
                "axisAlpha": 0.15
            }],
            "startDuration": 2,
            "graphs": [{
                "balloonText": "[[name]]: [[value]]",
                "bullet": "round",
                "lineThickness": 2,
                "valueField": "value",
                "lineColor": "#0000ff",
            }],
            "categoryField": "name"
        };

        let radarChart = AmCharts.makeChart(self.id, chart);

        //donutChart.addListener('rollOverSlice', function (e) {
        //    donutChart.clickSlice(e.dataItem.index);
        //});

        //donutChart.addListener('rollOutSlice', function (e) {
        //    donutChart.clickSlice(e.dataItem.index);
        //});

        //donutChart.addListener('clickSlice', function (e) {

        //    if (e.event != undefined) { // check mouseClick
        //        self.onClickPieChart(e);
        //        e.chart.validateData();
        //    } else { }

        //});
    }

    onUnload() {

    }
}

export default {
    viewModel: RadarChart,
    template: templateMarkup
};
