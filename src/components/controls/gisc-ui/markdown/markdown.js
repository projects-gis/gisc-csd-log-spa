import ko from "knockout";
import templateMarkup from "text!./markdown.html";
import ControlBase from "../../controlbase";
import MarkdownIt from "markdown-it";
import hljs from "highlightjs";
import Utility from "../../../../app/frameworks/core/utility";

/**
 * gisc-ui-markdown is a special container which accept markdown content instead of HTML.
 * 
 * @class MarkdownContainer
 * @extends {ControlBase}
 */
class MarkdownContainer extends ControlBase {
    /**
     * Creates an instance of MarkdownContainer.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.contentNodes = null;
        this.mdParser = new MarkdownIt({
            html: true,
            linkify: true,
            typographer: true,
            highlight: function(str, lang) {
                if (lang && hljs.getLanguage(lang)) {
                    try {
                        return '<pre class="hljs"><code>' +
                            hljs.highlight(lang, str, true).value +
                            '</code></pre>';
                    } catch (__) {}
                }
                return '<pre class="hljs"><code>' + MarkdownIt.utils.escapeHtml(str) + '</code></pre>';
            }
        });

        this.contentText = Utility.extractParam(params.contentText, null);
    }

    /**
     * Hook from template when render node is completed
     * @public
     * @param {any} element
     */
    afterRender(element) {
        // contentNodes is the information before transform md to markupContent
        try {

            // Try to use external source first, if cannot find fallback to contentNodes
            var markdownContent = null;
            if(this.contentText && this.contentText()) {
                markdownContent = this.contentText();
            } else {
                markdownContent = this.contentNodes[0].nodeValue;
            }

            var markupContent = this.mdParser.render(markdownContent);
            $(element).append(markupContent);

            $(element).show();
        } catch (error) {
            console.log(error);
        }
    }

    onLoad(isFirstLoad) {}

    /**
     * Cleanup resource when dom is removed from page.
     */
    onUnload() {
        this.contentNodes = null;
        this.mdParser = null;
    }
}

export default {
    viewModel: {
        createViewModel: function(params, componentInfo) {
            var vm = new MarkdownContainer(params);
            vm.contentNodes = componentInfo.templateNodes;
            return vm;
        }
    },
    template: templateMarkup
};