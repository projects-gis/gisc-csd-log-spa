import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./flatpickr.html";
import "flatpickr";

/**
 * 
 * Using for multi-line text input control
 * @class TextArea
 * @extends {ControlBase}
 */
class DatePicker extends ControlBase {
    /**
     * Creates an instance of TextArea.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.id = this.ensureNonObservable(params.id, this.id);
        this.value = this.ensureObservable(params.value, new Date().toLocaleDateString('en-US'));
        this.enable = this.ensureObservable(params.enable, true);
        this.showTime = this.ensureNonObservable(params.showTime, false);
        this.minDate = this.ensureObservable(params.minDate, new Date().toLocaleDateString('en-US'));
        this.maxDate = this.ensureObservable(params.maxDate, null);
        this.dateTimeFormat = this.ensureNonObservable(params.dateTimeFormat, "d/m/Y H:i");

    }

    onDomReady() {
        let options = {
            enableTime: this.showTime,
            dateFormat: this.dateTimeFormat,
            time_24hr: true,
            defaultDate: this.value(),
            minDate: this.minDate(),
            maxDate: this.maxDate(),
            // onClose: (selectedDates, dateStr, instance) => {
            //     fpickr.set('minDate', this.minDate());
            //     fpickr.set('maxDate', this.maxDate());
            // },
            onOpen: (selectedDates, dateStr, instance) => {
                fpickr.setDate(this.value());
            }
        }

        var fpickr = $("#" + this.id).flatpickr(options);

    }


    onLoad() {
        
    }

    onUnload() {
        this.value = null;
    }
}

export default {
    viewModel: DatePicker,
    template: templateMarkup
};