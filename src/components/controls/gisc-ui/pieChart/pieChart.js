﻿import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./pieChart.html";
import ChartJs from "chart";
import Logger from "../../../../app/frameworks/core/logger";

class PieChart extends ControlBase {
    constructor(params) {
        super(params);

        //console.log(params);

        ChartJs.defaults.global.defaultFontFamily = "dbhx";
        ChartJs.defaults.global.defaultFontSize = 16;

        this.data = this.ensureObservable(params.data);
        this.selected = this.ensureFunction(params.selected);

        this.clickData = this.data().options.bindingData;

        this.chartInstance = null;

        this.chartNameTxt = ko.observable(this.i18n("Vehicle_MA_Dashboard_Title_NextDayMaintenanceType")());
        this.noDataTxt = ko.observable(this.i18n("M111")());
        this.isNoData = ko.observable(false);
        this.isData = ko.observable(true);

        this.isArrayChange = this.data.subscribe((changes) => {
            this.clickData = changes.options.bindingData;
            this.createChart(changes);
        });
    }

    onChartClick(evt) {
        // debugger
        var activePoints = this.chartInstance.getElementsAtEvent(evt);
        if (activePoints.length === 0) {
            Logger.warn("Please click on segment.");
        } else {
            return this.selected ? this.selected(this.clickData[activePoints[0]._index]) : null;

        }
    }

    afterRender(element) {
        this.createChart(this.data());
    }

    onUnload() {
        this.chartInstance.destroy();
        this.data = null;
    }

    createChart(val) {
        var self = this;
        var ctx = $("#" + self.id);

        //console.log(this.data());

        try {
            this.isNoData(this.data().data[0].data[0] == 0);
            this.isData(!this.isNoData());
            //if (this.isNoData()) {
                
            //} else {

                if (!this.chartInstance) {
                    this.chartInstance = new ChartJs(ctx, {
                        type: 'pie',

                        // The data for our dataset
                        data: {
                            labels: [],
                            datasets: [{
                                data: [],
                                backgroundColor: []
                            }]
                        },

                        // Configuration options go here
                        options: {
                            responsive: true,
                            maintainAspectRatio: false,
                            legend: {
                                display: true,
                                position: 'bottom'
                            },
                            tooltips: {
                                cornerRadius: 1,
                                backgroundColor: "#333333",
                                bodyFontColor: "rgb(255,209,23)",
                                bodyFontFamily: "dbhx",
                                yPadding: 8,
                                callbacks: val.options.tooltipCallback
                            },
                            title: {
                                display: true,
                                text: ""
                            },
                            elements: {
                                arc: {
                                    borderWidth: 1
                                }
                            }
                        }
                    });
                }

                this.chartInstance.data.labels = [];
                this.chartInstance.data.datasets = [];
                
                this.chartInstance.options.title.text = this.data().options.title;
                //val.data[0].data.push(20, 40, 10, 50, 100, 10, 70);
                //val.data[0].backgroundColor.push("rgb(200, 0, 0)", "rgb(200, 200, 200)", "rgb(0, 200, 0)", "rgb(0, 0, 200)", "rgb(123, 123, 123)", "rgb(50, 50, 0)");
                //this.data().options.labels.push("การซ่อมบำรุงรถยนต์_Update5", "การซ่อมบำรุงรถยนต์_Update5", "การซ่อมบำรุงรถยนต์_Update5", "การซ่อมบำรุงรถยนต์_Update5", "การซ่อมบำรุงรถยนต์_Update5", "การซ่อมบำรุงรถยนต์_Update5", "การซ่อมบำรุงรถยนต์_Update5");

                val.data.forEach(function (element, ind) {
                    this.chartInstance.data.labels = this.data().options.labels;
                    this.chartInstance.data.datasets[0] = element;
                    //this.chartInstance.data.datasets[0].backgroundColor = element.backgroundColor;
                }, this);

                this.chartInstance.update();
            //}
        }
        catch (error) {

        }
    }
}

export default {
    viewModel: PieChart,
    template: templateMarkup
};