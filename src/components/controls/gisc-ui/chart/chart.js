import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./chart.html";
import ChartJs from "chart";
import Logger from "../../../../app/frameworks/core/logger";

/**
 * 
 * 
 * @class Chart
 * @extends {ControlBase}
 */
class Chart extends ControlBase {

    /**
     * Creates an instance of Chart.
     * 
     * @param {any} params
     */
    constructor(params) {
            super(params);
            this.data = this.ensureObservable(params.data);
            this.labelField = this.ensureNonObservable(params.label);
            this.valueField = this.ensureNonObservable(params.value);
            this.colorField = this.ensureNonObservable(params.color);

            this.onclick = params.onclick;
            this.chartInstance = null;

            //Subscribe when data was add/remove.
            this.isArrayChange = this.data.subscribe(function(changes) {
                this.createChart(changes);
            }, this);
        }
        /**
         * 
         * Return index value to page when implement onclick.
         * @param {any} evt
         * @returns
         */
    onChartClick(evt) {
            // debugger
            var activePoints = this.chartInstance.getElementsAtEvent(evt);
            if (activePoints.length === 0) {
                Logger.warn("Please click on segment.");
            } else {
                return this.onclick ? this.onclick(activePoints[0]._index) : null;
               
            }
        }
        /**
         * 
         * Crate chart after page render.
         * @param {any} element
         */
    afterRender(element) {
            this.createChart(this.data());
        }
        /**
         * 
         * To create chart
         * @param {any} val
         */
    createChart(val) {
        var self = this;
        var ctx = $("#" + self.id);
        try {

            if (!this.chartInstance) {
                this.chartInstance = new ChartJs(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: [],
                        datasets: [{
                            data: [],
                            backgroundColor: [],
                        }]
                    },
                    options: {
                        responsive: true,
                        cutoutPercentage : 62,
                        legend: {
                            display: false
                        },
                        tooltips: {
                            cornerRadius: 1,
                            backgroundColor: "#333333",
                            bodyFontFamily: "Arial, Helvetica, sans-serif",
                            yPadding: 8
                        }
                    }
                });
            }

            this.chartInstance.data.labels = [];
            this.chartInstance.data.datasets[0].data = [];
            this.chartInstance.data.datasets[0].backgroundColor = [];



            val.forEach(function(element) {
                var labelText = this.ensureNonObservable(element[this.labelField]);
                var value = this.ensureNonObservable(element[this.valueField]);
                var color = this.ensureNonObservable(element[this.colorField]);
                this.chartInstance.data.labels.push(labelText);
                this.chartInstance.data.datasets[0].data.push(value);
                this.chartInstance.data.datasets[0].backgroundColor.push(color);
            }, this);

            this.chartInstance.update();

        } catch (error) {

        }
    }

    /**
     *  Cleanup chart instance when dom is removing from page.
     */
    onUnload() {
        this.chartInstance.destroy();
        this.data = null;
    }
}

export default {
    viewModel: Chart,
    template: templateMarkup
};