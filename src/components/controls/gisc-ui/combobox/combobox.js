import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./combobox.html";
import "./jquery-ui-combobox";

/**
 * Combobox controls for editable dropdown.
 * 
 * @class Combobox
 * @extends {ControlBase}
 */
class Combobox extends ControlBase {
    constructor(params) {
        super(params);

        this.options = this.ensureObservable(params.options);
        this.optionsText = this.ensureNonObservable(params.optionsText);
        this.optionsValue = this.ensureNonObservable(params.optionsValue);
        this.optionsCaption = this.ensureNonObservable(params.optionsCaption);
        this.value = this.ensureObservable(params.value);

        this.selectedOption = this.ensureObservable(params.selectedOption);

        this._subscribeRef_value = this.value.subscribe(function(newValue) {
            var targetOption = ko.utils.arrayFirst(this.options(), function(option) {
                return option[this.optionsValue] === newValue;
            }.bind(this));
            if(targetOption) {
                this.selectedOption(targetOption);
            }
        }, this);
    }

    afterRender(element) {
        var $selectElement = $(element).find("select");
        $selectElement.combobox();

    }

    onUnload() {
        this._subscribeRef_value.dispose();
        this.options = null;
        this.value = null;
        this.selectedOption = null;
    }
}

export default {
    viewModel: Combobox,
    template: templateMarkup
};