import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./label.html";

/**
 * Label control.
 * 
 * @class Label
 * @extends {ControlBase}
 */
class Label extends ControlBase {
    /**
     * Creates an instance of Label.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.forElementId = this.ensureNonObservable(params.for, "");
        this.text = this.ensureObservable(params.text, "");
        this.isRequired = this.ensureObservable(params.isRequired, false);
        this.isHtmlEncode = this.ensureNonObservable(params.isHtmlEncode, true);
        this.type = this.ensureNonObservable(params.type ,"field");
        this.labelStyles = ko.pureComputed(()=> {
            return this.type === "group" ? "group" : null;
        });
    }

    onLoad() {}
    onUnload() {
        this.text = null;
        this.isRequired = null;
    }
}

export default {
    viewModel: Label,
    template: templateMarkup
};