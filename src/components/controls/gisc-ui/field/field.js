import ko from "knockout";
import templateMarkup from "text!./field.html";
import ControlBase from "../../controlbase";

class Field extends ControlBase {
    /**
     * Creates an instance of Field.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.value = this.ensureObservable(params.value, null);
        this.cssClass = this.ensureNonObservable(params.cssClass, "");
        this.onClick = this.ensureFunction(params.onClick, null);

        this.displayValue = ko.pureComputed(() => {
            var rawValue = _.toString(this.value());
            var value = null;
            if (!_.isEmpty(rawValue)) 
            {
                this.onClickField = this.onClick;
                value = rawValue.replace(new RegExp("\r?\n", "g"), "<br>");
                if(typeof this.onClickField == "function")
                {
                    value = "<u style='cursor: pointer;'>" + value + "</u>";
                }
            }
            else
            {
                this.onClickField = null;
                value = "-";
            }
            return value;
        });

    }

    onLoad() {}
    onUnload() {
        this.value = null;
    }
}

export default {
    viewModel: Field,
    template: templateMarkup
};