import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./textArea.html";
import "jquery-autogrow-textarea";

/**
 * 
 * Using for multi-line text input control
 * @class TextArea
 * @extends {ControlBase}
 */
class TextArea extends ControlBase {
    /**
     * Creates an instance of TextArea.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.placeholder = this.ensureObservable(params.placeholder);
        this.value = this.ensureObservable(params.value);
        this.maxlength = this.ensureObservable(params.maxlength);
        this.height = this.ensureNonObservable(params.height,'70px');
        this.width = this.ensureNonObservable(params.width,'100%');
        //set auto grow textarea
        $( "textarea" ).autogrow();
    }

    onLoad() {}
    onUnload() {
        this.placeholder = null;
        this.value = null;
    }
}

export default {
    viewModel: TextArea,
    template: templateMarkup
};