import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./dialogBox.html";

class DialogBox extends ControlBase {

    /**
     * Creates an instance of DialogBox.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.title = this.ensureObservable(params.title);
        this.text = this.ensureObservable(params.text);

        this.type = this.ensureObservable(params.type, "oc");
        this.onclick = params.onclick;
        this.useFocus = this.ensureNonObservable(params.useFocus, true);
        this.isFocus = this.ensureObservable(params.hasFocus,false);
        this.displayable = ko.pureComputed(function() {
            return this.useFocus ? this.isFocus() : true;
        }, this);

        //btn value
        this.textButtonOk = this.i18n("Common_OK");
        this.textButtonCancel = this.i18n("Common_Cancel");
        this.textButtonYes = this.i18n("Common_Yes");
        this.textButtonNo = this.i18n("Common_No");

        //Check dialogBox type for display button
        this.isOkVisible = ko.pureComputed(function() {
            switch (this.type()) {
                case "oc":
                case "ok":
                    return true;
                default:
                    return false;
            }
        }, this);

        this.isCancelVisible = ko.pureComputed(function() {
            switch (this.type()) {
                case "oc":
                    return true;
                default:
                    return false;
            }
        }, this);

        this.isYesVisible = ko.pureComputed(function() {
            switch (this.type()) {
                case "yn":
                    return true;
                default:
                    return false;
            }
        }, this);

        this.isNoVisible = ko.pureComputed(function() {
            switch (this.type()) {
                case "yn":
                    return true;
                default:
                    return false;
            }
        }, this);

    }

    /**
     * Passing button value in DialogBox to callback function
     */
    click(result) {
        this.onclick(result);
        this.isFocus(false);
    }
}

export default {
    viewModel: DialogBox,
    template: templateMarkup
};