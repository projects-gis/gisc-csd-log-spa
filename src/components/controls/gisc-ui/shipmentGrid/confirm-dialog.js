﻿import ko from "knockout";
import ScreenBaseWidget from "../../../../components/portals/screenbasewidget";
import templateMarkup from "text!./confirm-dialog.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {
    Constants,
    Enums
} from "../../../../app/frameworks/constant/apiConstant";

class ShipmentGridConfirmWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        this.params = this.ensureNonObservable(params);

        this.textDistance = ko.observable(null);
        this.textTime = ko.observable(null);
        this.valueDistance = ko.observable(null);
        this.valueTime = ko.observable(null);

        this.isRadio = ko.observable(2);
        this.isFixFirst = ko.observable(true);
        this.isFixLast = ko.observable(true);

    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

        var filter = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.RouteOption]
        };

        this.webRequestEnumResource.listEnumResource(filter).done((response) => {
            if(_.size(response.items)){
                this.textTime(response.items[0].displayName);
                this.textDistance(response.items[1].displayName);
                this.valueTime(response.items[0].value);
                this.valueDistance(response.items[1].value);
            }
        });
    }

    /**
     * Save data to web service
     */
    onClickOK(){
        let filter = {
            routeOption: this.isRadio(),
            preserveFirstStop: this.isFixFirst(),
            preserveLastStop: this.isFixLast()
        }
        this.dispatchEvent("gisc-ui-shipment-grid-confirm-dialog-find", "find", filter);
        var confirmDialog = $('#gisc-ui-shipment-grid-confirm-dialog').data("kendoWindow");
        confirmDialog.close();
    }

    /**
     * Close modal dialog
     */
    onClickCancel(){
        var confirmDialog = $('#gisc-ui-shipment-grid-confirm-dialog').data("kendoWindow");
        confirmDialog.close();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.params = null;
        this.src = null;
    }


}

export default {
    viewModel: ScreenBaseWidget.createFactory(ShipmentGridConfirmWidget),
    template: templateMarkup
};