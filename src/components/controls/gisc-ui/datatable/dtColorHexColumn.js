import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';

class DTColorHexColumn extends DTBaseColumn {
    
    /**
     * Creates an instance of DTColorHexColumn.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Force this column to non orderable/searchable
        this.searchable = false;
        this.orderable = false;

        var self = this;
        this.render = function(data, type, row) {
            let node = '<div class="dt-hexbox" style="background-color:' +
                data + '"></div>';
            return node;
        }
    }
}

export default DTColorHexColumn;