﻿import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

/**
 * Editable textbox as DataTable column
 * 
 * @class DTTextboxColumnOne
 * @extends {DTBaseColumn}
 */
class DTTextboxColumnOne extends DTBaseColumn {
    
    /**
     * Creates an instance of DTTextboxColumnOne.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.maxLength = Utility.extractParam(params.maxlength);

        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            let renderableDataText = data;
            if(_.isNil(renderableDataText)) {
                renderableDataText = "";
            }
            let node = '<input type="text" class="dt-textboxone" ' + 
                'data-target-field="' + self.data + '" ' +
                'value="' + renderableDataText +'"'+ (self.maxLength ? ' maxlength="'+ self.maxLength +'" ' : '')
                + '/>';
            return node;
        }
    }
}

export default DTTextboxColumnOne;