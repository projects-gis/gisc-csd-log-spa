import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';

class DTCustomColumn extends DTBaseColumn {
    constructor(params) {
        super(params);

        this.render = params.render ? params.render : null;
    }
}

export default DTCustomColumn;