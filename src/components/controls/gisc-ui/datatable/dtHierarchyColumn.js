import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import Utility from "../../../../app/frameworks/core/utility";

class DTHierarchyColumn extends DTBaseColumn {
    constructor(params) {
        super(params);

        this.className = "dt-hierarchy";

        // Must define render function here instead of ES6 public method.
        var self = this;
        this.render = function(data, type, row) {
            let name = data["name"];
            let nodeLevel = data["nodeLevel"];
            let node = "";

            if(nodeLevel > 0) {
                node += '<div style="padding-left:';
                var paddingLeft = nodeLevel * 10;
                node += paddingLeft;
                node += 'px;">';
            }

            let hasChildren = data["hasChildren"];
            if(hasChildren) {
                node += '<i class="expander" aria-hidden="true"></i>';
            }

            node += '<span class="display-name';
            if(!hasChildren) {
                node += ' no-expander';
            }
            node += '">';
            node += name;
            node += '</span>';

            if(nodeLevel > 0) {
                node += "</div>";
            }

            return node;
        }
    }
}

export default DTHierarchyColumn;