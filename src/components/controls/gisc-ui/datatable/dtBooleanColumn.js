import ko from "knockout";
import DTBaseColumn from './dtBaseColumn';
import i18nextko from "knockout-i18next";

class DTBooleanColumn extends DTBaseColumn {
    constructor(params) {
        super(params);

        // Force this column to non searchable
        this.searchable = false;

        var self = this;
        this.render = function(data, type, row) {
            if(data) {
                return i18nextko.t("Common_Yes"); //'Yes'; //Common_Yes
            }
            return i18nextko.t("Common_No"); //'No'; //Common_No
        }
    }
}

export default DTBooleanColumn;