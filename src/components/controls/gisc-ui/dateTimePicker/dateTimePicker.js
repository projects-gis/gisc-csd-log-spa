import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./dateTimePicker.html";
import moment from "moment";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../app/frameworks/core/utility";
import { DatetimeLocalizations } from "./datetimeLocalizations";
import { MomentLocalizations } from "./momentLocalizations";
const afterCalendar = ".ui-datepicker-calendar";
const afterTimePicker = ".ui-timepicker-div";
const utcFormat = "YYYY-MM-DD[T]HH:mm:ss";

/**
 * 
 * 
 * @class DateTimePicker
 * @extends {ControlBase}
 */
class DateTimePicker extends ControlBase {

    /**
     * Creates an instance of DateTimePicker.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // window.date = this;

        this.mode = this.ensureNonObservable(params.mode, "datetime");

        this.dateTimeLocalizations = new DatetimeLocalizations();
        this.momentLocalizations = new MomentLocalizations();
        this.currentLang = WebConfig.userSession.currentUserLanguage;

        this.timezone = this.ensureObservable(params.timezone, WebConfig.companySettings.timezoneName);
        this.datetime = this.ensureObservable(params.value);
        this.value = ko.observable();

        this.dateFormat = this.ensureNonObservable(params.dateFormat, WebConfig.companySettings.shortDateFormat);
        this.timeFormat = this.ensureNonObservable(params.timeFormat, WebConfig.companySettings.shortTimeFormat);

        this.jQuerydateFormat = this.convertToJqueryFormat(this.dateFormat);
        this.momentDateFormat = this.convertToMomentFormat(this.dateFormat);
        
        this.momentTimeFormat = this.timeFormat.replace("tt", "a");
        this.formatsMomentISO = [moment.ISO_8601, this.momentDateFormat];
        
        //Set min date.
        this.minDate = this.ensureObservable(params.minDate);
        this.subMinDate = this.minDate.subscribe((newValue) => {
            $("#" + this.id).datepicker("option", "minDate", this.minDateInternal());

            // for case minDate is greater than current value, set current value = minDate
            if (moment(this.datetime()).isBefore(newValue)) {
                this.value($("#" + this.id).val());
                this.datetime(this.formatDateTime(newValue, utcFormat));
            }
        });

        this.minDateInternal = ko.pureComputed(
            () => {
                var minDateIsUTC = moment(this.minDate(), this.formatsMomentISO, true).isValid();
                return minDateIsUTC ? moment(this.minDate()).format(this.momentDateFormat) : this.minDate();
            }
        );

        //Set max date.
        this.maxDate = this.ensureObservable(params.maxDate);
        this.subMaxDate = this.maxDate.subscribe((newValue) => {
            $("#" + this.id).datepicker("option", "maxDate", this.maxDateInternal());
            
            // for case maxDate is less than current value, set current value = maxDate
            if (moment(this.datetime()).isAfter(newValue)) {
                this.value($("#" + this.id).val());
                this.datetime(this.formatDateTime(newValue, utcFormat));
            }
        });
        this.maxDateInternal = ko.pureComputed(
            () => {
                var maxDateIsUTC = moment(this.maxDate(), this.formatsMomentISO, true).isValid();
                return maxDateIsUTC ? moment(this.maxDate()).format(this.momentDateFormat) : this.maxDate();
            }
        );

        //Set min time.
        this.minTime = this.ensureObservable(params.minTime);
        this.preMinTime = this.minTime() ? this.minTime() : null;
        this.subMinTime = this.minTime.subscribe((newValue) => {
            
            var $ele = $("#" + this.id);
            var preMin = _.isFunction(this.preMinTime) ? this.preMinTime() : this.preMinTime;

            if (!newValue && $ele.timepicker()) {

                this.timePickerOption.minTime = null;
                this.destroyTimePicker($ele);
            } 
            else if ( moment(preMin).isAfter(newValue) && $ele.val() ) {

                this.destroyTimePicker($ele);
                this.setMinTime($ele);
            } 
            else if (moment(newValue).isAfter(this.datetime())) {

                this.destroyTimePicker($ele);
                this.setMinTime($ele);

                this.datetime(newValue);
                this.value(this.formatDateTime(newValue, this.momentTimeFormat));
            } 
            else {

                this.setMinTime($ele);
                if(!$ele.val()){
                    this.datetime(newValue);
                    this.value(this.formatDateTime(newValue, this.momentTimeFormat));
                }
            }
            this.preMinTime = newValue;
            !$ele.val() ? $ele.val(this.value()) : '';
            
        });

        this.minTimeInternal = ko.pureComputed(
            () => {
                var minTimeUTC = moment(this.minTime(), this.formatsMomentISO, true).isValid();
                var min = this.minTime() ? moment(this.minTime()).format("H:mm:ss") : '';
                return minTimeUTC ? min : this.minTime();
            }
        );

        //Set max time.
        this.maxTime = this.ensureObservable(params.maxTime);
     
        this.maxTimeInternal = ko.pureComputed(
            () => {
                var maxTimeIsUTC = moment(this.maxTime(), this.formatsMomentISO, true).isValid();
                var max = this.maxTime() ? moment(this.maxTime()).format("H:mm:ss") : '';
                return maxTimeIsUTC ? max : this.maxTime();
            }
        );

        this.timeRange = this.ensureObservable(params.timeRange, false);
        this.showSecond = ko.pureComputed(()=> {
            return !this.timeRange();
        });


        //For append timezone to DOM
        this.appendTimeZone = null;
        this.appendMonth = null;
        this.appendYear = null;
        this.appendClearBtn = null;

        this._onBladeScrollHandler = null;
        this._onShowCalenderRef = null;
        this._clearValueRef = null;

        this.visibleCloseBtn = ko.observable();


        this.timePickerOption = null;

        /****************** RealTime *****************/
        // this.enableCurrentTime = this.ensureObservable(params.enableCurrentTime, false);
        // this.isChecked = this.ensureObservable(params.isCurrentTimeChecked, false);

        // this.myTime = null;


        // if (this.enableCurrentTime()) {
        //     this._checkedSubscribe = this.isChecked.subscribe((checked) => {
        //         if (checked) {
        //             this.enable(false);
        //             this.myTime = setInterval(() => {
        //                 this.initialDatetime(new Date(), true)
        //             }, 1000);
        //         } else {
        //             this.enable(true);
        //             clearInterval(this.myTime);
        //         }
        //     });
        // }
        /****************** RealTime *****************/

        this._subscribeDatetime = this.datetime.subscribe((newValue) => {
            // new valule change logic
            if (!newValue) {
                this._clearValue();
            } else {
                //fixed change date first times
                this.initialDatetime(newValue, false);
                //////////////////////////////////////
                this.visibleCloseBtn(true)
            }
        });

        


        this._subscribeValue = this.value.subscribe((newValue) => {
            var $ele = $("#" + this.id);
            if (this.mode === "time") {
                if(newValue){

                    var dt = moment(newValue, [this.momentTimeFormat]).format("HH:mm");

                    var utcTime = this.formatTimeToUTC(dt);
                    this.datetime(utcTime);
                    if( moment(this.datetime()).isSame(this.minTime()) ){
                        var time = moment(this.datetime()).add(1, 'm').format(utcFormat);
                        this.datetime(time);
                        this.value(this.formatDateTime(this.datetime(), this.momentTimeFormat));
                    }
                }
            }
        });

        this.cssClass = ko.pureComputed(function () {
            return this.enable() === false ? "enable" : "";
        }, this);

        this._enableSubscribe = this.enable.subscribe((value) => {
            var $elClendar = $(".app-icon-calender" + "." + this.id);
            var $elClear = $(".app-remove-icon" + "." + this.id);
            if (value) {
                $elClendar.on("click", this._onShowCalenderRef);
                $elClear.on("click", this._clearValueRef);
            } else {
                $elClendar.off("click", this._onShowCalenderRef);
                $elClear.off("click", this._clearValueRef);
            }
        });
    }

     /**
     * 
     * Convert .Net to moment js datetime format.
     * @param {any} param
     * @returns
     * 
     * @memberOf DateTimePicker
     */
    convertToMomentFormat(format) {

        var currentFormat = format.toUpperCase();

        if (currentFormat.indexOf("DDDD") !== -1) {
            currentFormat = currentFormat.replace("DDDD", "dddd");

        } else if (currentFormat.indexOf("DDD") !== -1) {

            currentFormat = currentFormat.replace("DDD", "ddd");

        } else if (currentFormat.indexOf("DD") !== -1) {

            currentFormat = currentFormat.replace("DD", "DD");

        } else {
            currentFormat = currentFormat.replace("D", "D");
        }
        
        currentFormat = _.replace(currentFormat, "GG", "ค.ศ.");  //TO DO : Change "ค.ศ." to this.i18n("Common_AD")()
        currentFormat = _.replace(currentFormat, /'/g, "");        

        return currentFormat;
    }


    /**
     * 
     * Convert date format for datepicker
     * @param {any} format
     * @returns
     * 
     * @memberOf DateTimePicker
     */
    convertToJqueryFormat(format) {

        var currentFormat = format;
        
        // Convert the date
        currentFormat = currentFormat.replace("dddd", "DD");
        currentFormat = currentFormat.replace("ddd", "D");

        // Convert month
        if (currentFormat.indexOf("MMMM") !== -1) {

            currentFormat = currentFormat.replace("MMMM", "MM");

        } else if (currentFormat.indexOf("MMM") !== -1) {

            currentFormat = currentFormat.replace("MMM", "M");

        } else if (currentFormat.indexOf("MM") !== -1) {

            currentFormat = currentFormat.replace("MM", "mm");

        } else {

            currentFormat = currentFormat.replace("M", "m");

        }
        
        // Convert year
        currentFormat = currentFormat.indexOf("yyyy") !== -1 ? currentFormat.replace("yyyy", "yy") : currentFormat.replace("yy", "y");
        currentFormat = _.replace(currentFormat, "gg", "ค.ศ.");  //TO DO : Change "ค.ศ." to this.i18n("Common_AD")()

        return currentFormat;

    }

    /**
     * 
     * Format Datetme with moment js
     * @param {any} value
     * @param {any} format
     * @returns
     * 
     * @memberOf DateTimePicker
     */
    formatDateTime(value, format) {
        var f = moment(value).format(format);
        return moment(value).format(format);
    }


    /**
     * 
     * 
     * @param {any} timeValue
     * 
     * @memberOf DateTimePicker
     */
    formatTimeToUTC(timeValue) {
        
        var newDatetime = "2000-01-01" + " " + timeValue;
        var formats = [moment.ISO_8601, "yyyy-mm-dd" + " " + this.momentTimeFormat];
        var formattedDatetime = moment(newDatetime, formats).format(utcFormat);

        return formattedDatetime;
    }


    insertDropdownMonth() {
        clearTimeout(this.appendMonth);
        this.appendMonth = setTimeout(function () {
            $(".ui-datepicker-month").wrap("<div class='ui-datepicker-dropdown'></div>");
            $("<span class= \"ui-datepicker-dropdown-arrow\"></span>").insertBefore($(".ui-datepicker-month"));
        }, 1);
    }

    insertDropdownYear() {
        clearTimeout(this.appendYear);
        this.appendYear = setTimeout(function () {
            $(".ui-datepicker-year").wrap("<div class='ui-datepicker-dropdown'></div>");
            $("<span class= \"ui-datepicker-dropdown-arrow\"></span>").insertBefore($(".ui-datepicker-year"));
        }, 1);
    }

    /**
     * 
     * Use timeout to append timezone label to datetimepickder.To ensure that the content gets created before append.
     * @param {any} text
     * @param {any} position
     * @param {any} timezoneText
     */
    insertTimezone(text, position, timezoneText) {
        clearTimeout(this.appendTimeZone);
        this.appendTimeZone = setTimeout(function () {
            $('<div class="ui-timepicker-div">\
                         <dl>\
                         <dt class = "ui_tpicker_timezone_label" >' + timezoneText + '\
                         <dd class = "ui_tpicker_timezone" >\
                         <div class = "ui_tpicker_time_input">' + text + '</div>\
                         </dd> \
                        </dl> \
                        </div>').insertAfter(position);
        }, 1);
    }


    /**
     * This method was called onLoad and when enableCurrentTime has been set by page.
     * 
     * @param {any} datetime
     * 
     * @memberOf DateTimePicker
     */
    initialDatetime(datetime, isCurrent) {
        if (!datetime) {
            //Set input to empty when datetime value is null
            this.value("");

        } else {
            //Format datetime value from page to dateTimePicker
            switch (this.mode) {
                case "datetime":
                    this.value(this.formatDateTime(datetime, this.momentDateFormat + " " + this.momentTimeFormat));
                    break;
                case "date":
                    this.value(this.formatDateTime(datetime, this.momentDateFormat));
                    break;
                case "time":
                    this.value(this.formatDateTime(datetime, this.momentTimeFormat));
                    break;
            }
            if (isCurrent) {
                this.datetime(this.formatDateTime(datetime, utcFormat));
            }

        }
    }

    _onBladeScroll() {
        var $el = $("#" + this.id)
        $el.datepicker("hide");
        $el.blur();
    }

    _showCalendar() {
        var $el = $("#" + this.id)
        $el.datepicker("show");
    }

    _clearValue() {
        var $el = $("#" + this.id);
        this.visibleCloseBtn(false);
        this.value(null);
        this.datetime(null);

        if (this.mode === "time") {
            $el.timepicker("destroy");
            $el.timepicker(this.timePickerOption);
        }
    }

    /**
     * 
     * Set input value to null if input field was clear.
     * @param {any} value
     * 
     * @memberOf DateTimePicker
     */
    onInputChange(value) {
        if (!value) {
            this.datetime(null);
        }
    }

    destroyTimePicker($ele){
        $ele.timepicker("destroy");
        $ele.timepicker(this.timePickerOption);
    }

    setMinTime($ele){
        $ele.timepicker("option", "minTime", this.minTimeInternal());
        this.timePickerOption.minTime  = this.minTimeInternal();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return

        var h = !this.datetime() ? false : true;
        this.visibleCloseBtn(h);
        moment.updateLocale(this.currentLang, this.momentLocalizations.moment_regional);
        this.initialDatetime(this.datetime(), false);

    }

    /**
     * 
     * Create jquery datetimepicker after DOM element is rendered to page.
     * @param {any} element
     */
    onDomReady(element) {

            var self = this;
            var $el = $("#" + self.id);
            // var currentLang = WebConfig.userSession.currentUserLanguage;

            //Datepicker localizations
            $.datepicker.regional[this.currentLang] = this.dateTimeLocalizations.datepicker_regional;

            //Timepicker localizations
            $.timepicker.regional[this.currentLang] = this.dateTimeLocalizations.timepicker_regional;

            $.datepicker.setDefaults($.datepicker.regional[this.currentLang]);
            $.timepicker.setDefaults($.timepicker.regional[this.currentLang]);

            this._onBladeScrollHandler = this._onBladeScroll.bind(this);
            $('.app-blade-content').on("scroll", this._onBladeScrollHandler);
            $('#main-panorama').on("scroll", this._onBladeScrollHandler);

            this._onShowCalenderRef = this._showCalendar.bind(this);
            this._clearValueRef = this._clearValue.bind(this);


            if (self.enable()) {
                $(".app-icon-calender" + "." + self.id).on("click", this._onShowCalenderRef);
                $(".app-remove-icon" + "." + self.id).on("click", this._clearValueRef);
            }



            switch (this.mode) {
                case "datetime":

                    $el.datetimepicker({
                        controlType: 'select',
                        showButtonPanel: false,
                        autoUpdateInput: false,
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "c-5:c+5",
                        oneLine: true,
                        dateFormat: this.jQuerydateFormat,
                        timeFormat: this.timeFormat,
                        minDate: this.minDateInternal(),
                        maxDate: this.maxDateInternal(),
                        minTime: this.minTimeInternal(),
                        maxTime: this.maxTimeInternal(),

                        //To append timezone label when datetimepickder display
                        beforeShow: function (datetime) {
                            self.insertTimezone(self.timezone(), afterTimePicker, self.i18n("Common_Timezone")());
                            self.insertDropdownMonth();
                            self.insertDropdownYear();
                        },
                        //To append timezone label when the datetimepickder moves to a new month and/or year
                        onChangeMonthYear: function (year, month, inst) {
                            self.insertTimezone(self.timezone(), afterTimePicker, self.i18n("Common_Timezone")());
                            self.insertDropdownMonth();
                            self.insertDropdownYear();
                        },
                        //To return selected datetime to page.
                        onSelect: function (datetime) {
                            self.value(datetime);

                            //To trim word 'วัน' and 'ที่' from string format.
                            self.momentDateFormat =  _.replace(self.momentDateFormat, /วัน|ที่/g, "");    
                            datetime =  _.replace(datetime, /วัน|ที่/g, "");  

                            var formattedDatetime = moment(datetime, self.momentDateFormat + " " + self.momentTimeFormat).format(utcFormat);
                            self.datetime(formattedDatetime);
                        }
                    });

                    $el.on('change', function (e) {
                        self.onInputChange(this.value);
                    });
                    break;
                case "date":
                    $el.datepicker({
                        controlType: 'select',
                        showButtonPanel: false,
                        changeMonth: true,
                        changeYear: true,
                        yearRange: "c-5:c+5",
                        dateFormat: this.jQuerydateFormat,
                        minDate: this.minDateInternal(),
                        maxDate: this.maxDateInternal(),

                        //To append timezone label when datepickder display
                        beforeShow: function (date) {
                            self.insertTimezone(self.timezone(), afterCalendar, self.i18n("Common_Timezone")());
                            self.insertDropdownMonth();
                            self.insertDropdownYear();
                        },
                        //To append timezone label when the datepicker moves to a new month and/or year
                        onChangeMonthYear: function (year, month, inst) {
                            self.insertTimezone(self.timezone(), afterCalendar, self.i18n("Common_Timezone")());
                            self.insertDropdownMonth();
                            self.insertDropdownYear();
                        },
                        //To return selected date to page.
                        onSelect: function (date) {
                            self.value(date);

                            //To trim word 'วัน' and 'ที่' from string format.
                            self.momentDateFormat =  _.replace(self.momentDateFormat, /วัน|ที่/g, "");    
                            date =  _.replace(date, /วัน|ที่/g, "");   

                            var formattedDatetime = moment(date, self.momentDateFormat).format(utcFormat);
                            self.datetime(formattedDatetime);
                        }
                    });

                    $el.on('change', function (e) {
                        self.onInputChange(this.value);
                    });
                    break;
                case "time":

                    this.timePickerOption = {
                        controlType: 'select',
                        showButtonPanel: false,
                        timeFormat: this.timeFormat,
                        minTime: this.minTimeInternal(),
                        maxTime: this.maxTimeInternal(),
                        showSecond:this.showSecond(),

                        //To return selected time to page.
                        beforeShow: function (time) {
                            if (!self.datetime() || !self.value()) {
                                time.value = "";
                            }
                        },
                        onSelect: function (newTime) {
                            if (newTime.length < 8) {
                                newTime = "0" + newTime;
                            }

                            if(self.minTimeInternal()){
                                var equal = Date.parse('01/01/2000 '+newTime) === Date.parse('01/01/2000 '+self.minTimeInternal());
                                
                                if(equal){
                                    $el.timepicker("hide");
                                }
                            }
                        }
                    };
                    $el.timepicker(this.timePickerOption);

                    $el.on('change', function (e) {

                        self.onInputChange(this.value);
                    });
                    break;
            }
        }
        /**
         * Cleanup datetimepicker when dom is removing from page.
         */
    onUnload() {
        // var self = this;
        var $el = $("#" + this.id);
        var $elClendar = $(".app-icon-calender" + "." + this.id);
        var $elClear = $(".app-remove-icon" + "." + this.id);

        $('.app-blade-content').off("scroll", this._onBladeScrollHandler);
        $('#main-panorama').off("scroll", this._onBladeScrollHandler);
        $el.unbind()
        $elClendar.unbind();
        $elClear.unbind();

        clearInterval(this.myTime);

        this.minDate = null;
        this.maxDate = null;
        this.minTime = null;
        this.maxTime = null;
        this.timezone = null;
        this.datetime = null;
        this.value = null;
        this.visibleCloseBtn = null;
        // this.enableCurrentTime = null;
        // this.isChecked = null;
    }
}

export default {
    viewModel: DateTimePicker,
    template: templateMarkup
};