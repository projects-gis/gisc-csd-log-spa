import ko from "knockout";
import i18nextko from "knockout-i18next";

class MomentLocalizations {
    constructor () {

        this.moment_regional = {
            months: [
                    i18nextko.t("Common_MonthJan")(),
                    i18nextko.t("Common_MonthFeb")(),
                    i18nextko.t("Common_MonthMar")(),
                    i18nextko.t("Common_MonthApr")(),
                    i18nextko.t("Common_MonthMay")(),
                    i18nextko.t("Common_MonthJun")(),
                    i18nextko.t("Common_MonthJul")(),
                    i18nextko.t("Common_MonthAug")(),
                    i18nextko.t("Common_MonthSep")(),
                    i18nextko.t("Common_MonthOct")(),
                    i18nextko.t("Common_MonthNov")(),
                    i18nextko.t("Common_MonthDec")()
            ],

            monthsShort : [
                    i18nextko.t("Common_MonthShortJan")(),
                    i18nextko.t("Common_MonthShortFeb")(),
                    i18nextko.t("Common_MonthShortMar")(),
                    i18nextko.t("Common_MonthShortApr")(),
                    i18nextko.t("Common_MonthShortMay")(),
                    i18nextko.t("Common_MonthShortJun")(),
                    i18nextko.t("Common_MonthShortJul")(),
                    i18nextko.t("Common_MonthShortAug")(),
                    i18nextko.t("Common_MonthShortSep")(),
                    i18nextko.t("Common_MonthShortOct")(),
                    i18nextko.t("Common_MonthShortNov")(),
                    i18nextko.t("Common_MonthShortDec")()
            ],
            weekdays : [
                    i18nextko.t("Common_DaySun")(),
                    i18nextko.t("Common_DayMon")(),
                    i18nextko.t("Common_DayTue")(),
                    i18nextko.t("Common_DayWed")(),
                    i18nextko.t("Common_DayThu")(),
                    i18nextko.t("Common_DayFri")(),
                    i18nextko.t("Common_DaySat")()
            ],
            weekdaysShort : [
                    i18nextko.t("Common_DayShortSun")(),
                    i18nextko.t("Common_DayShortMon")(),
                    i18nextko.t("Common_DayShortTue")(),
                    i18nextko.t("Common_DayShortWed")(),
                    i18nextko.t("Common_DayShortThu")(),
                    i18nextko.t("Common_DayShortFri")(),
                    i18nextko.t("Common_DayShortSat")()
            ],
            weekdaysMin : [
                    i18nextko.t("Common_DayMinSun")(),
                    i18nextko.t("Common_DayMinMon")(),
                    i18nextko.t("Common_DayMinTue")(),
                    i18nextko.t("Common_DayMinWed")(),
                    i18nextko.t("Common_DayMinThu")(),
                    i18nextko.t("Common_DayMinFri")(),
                    i18nextko.t("Common_DayMinSat")()
            ],
            
        }
    }
}
export { MomentLocalizations };
