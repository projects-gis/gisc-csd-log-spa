import ko from "knockout";
import {
    Enums
} from "../../../../app/frameworks/constant/apiConstant";
import {
    AssetIcons
} from "../../../../app/frameworks/constant/svg";
import svgSearch from "text!../../../../components/svgs/cmd-search.html";
import svgCreate from "text!../../../../components/svgs/cmd-add.html";
import svgDelete from "text!../../../../components/svgs/cmd-delete.html";
import svgEdit from "text!../../../../components/svgs/cmd-edit.html";
import svgVdo from "text!../../../../components/svgs/ic_vdo.html";
import Utility from "../../../../app/frameworks/core/utility";

class DataGridColumns {

    static getColumn(template, data) {
        var temp = null;
        if (template) {
            //find function
            let fn = DataGridColumns.findFn(template);

            if (fn.length === 1) {
                let dg = new DataGridColumns;
                //call function and return data
                var d = data;

                if (typeof data == "object") {
                    d = JSON.stringify(data);
                }
                temp = eval('dg.' + fn[0] + '(' + d + ')');
            } else {
                temp = (template) ? template : data;
            }
        }
        return temp;
    }

    //find function
    static findFn(template) {
        //get all object in class
        let getFn = Object.getOwnPropertyNames(DataGridColumns.prototype);
        //fund object in class
        let filterFn = getFn.filter(x => x == template);
        return filterFn;
    }

    renderMovementColumn(data) {
      
        if (data) {
            var cssClass = " ";
            switch (data) {
                case Enums.ModelData.MovementType.Move:
                    cssClass += "move";
                    break;
                case Enums.ModelData.MovementType.Stop:
                    cssClass += "stop";
                    break;
                case Enums.ModelData.MovementType.Park:
                    cssClass += "park";
                    break;
                case Enums.ModelData.MovementType.ParkEngineOn:
                    cssClass += "parkEngineOn"
                    break;
            }

            let node = '<div title="" class="icon-movement' + cssClass + '">' + AssetIcons.IconMovementTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderDriverColumn(data) {
        return Utility.createDriverNameWithTelephoneTag(data.driverName, data.driverMobile);
    }

    renderDirectionColumn(data) {
        if (data) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data) {
                case Enums.ModelData.DirectionType.N:
                    iconTemplateMarkup = AssetIcons.IconDirectionNTemplateMarkup;
                    cssClass += "n";
                    break;
                case Enums.ModelData.DirectionType.NE:
                    iconTemplateMarkup = AssetIcons.IconDirectionNETemplateMarkup;
                    cssClass += "ne";
                    break;
                case Enums.ModelData.DirectionType.E:
                    iconTemplateMarkup = AssetIcons.IconDirectionETemplateMarkup;
                    cssClass += "e";
                    break;
                case Enums.ModelData.DirectionType.SE:
                    iconTemplateMarkup = AssetIcons.IconDirectionSETemplateMarkup;
                    cssClass += "se";
                    break;
                case Enums.ModelData.DirectionType.S:
                    iconTemplateMarkup = AssetIcons.IconDirectionSTemplateMarkup;
                    cssClass += "s";
                    break;
                case Enums.ModelData.DirectionType.SW:
                    iconTemplateMarkup = AssetIcons.IconDirectionSWTemplateMarkup;
                    cssClass += "sw";
                    break;
                case Enums.ModelData.DirectionType.W:
                    iconTemplateMarkup = AssetIcons.IconDirectionWTemplateMarkup;
                    cssClass += "w";
                    break;
                case Enums.ModelData.DirectionType.NW:
                    iconTemplateMarkup = AssetIcons.IconDirectionNWTemplateMarkup;
                    cssClass += "nw";
                    break;
            }

            let node = '<div title="" class="icon-direction' + cssClass + '">' + iconTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderGsmStatusColumn(data) {
        if (data) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data) {
                case Enums.ModelData.GsmStatus.Bad:
                    iconTemplateMarkup = AssetIcons.IconGsmBadTemplateMarkup;
                    cssClass += "bad";
                    break;
                case Enums.ModelData.GsmStatus.Medium:
                    iconTemplateMarkup = AssetIcons.IconGsmMediumTemplateMarkup;
                    cssClass += "medium";
                    break;
                case Enums.ModelData.GsmStatus.Good:
                    iconTemplateMarkup = AssetIcons.IconGsmGoodTemplateMarkup;
                    cssClass += "good";
                    break;
            }

            let node = '<div title="" class="icon-gsm' + cssClass + '">' + iconTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderGpsColumn(data) {
        var cssClass = " ";
        switch (data) {
            case true:
                cssClass += "good";
                break;
            case false:
                cssClass += "bad";
                break;
        }

        let node = '<div title="" class="icon-gps' + cssClass + '">' + AssetIcons.IconGpsTemplateMarkup + '</div>';
        return node;
    }

    renderGpsStatusColumn(data) {
        var cssClass = " ";
        switch (data) {
            case Enums.ModelData.GpsStatus.Bad:
                cssClass += "bad";
                break;
            case Enums.ModelData.GpsStatus.Medium:
                cssClass += "medium";
                break;
            case Enums.ModelData.GpsStatus.Good:
                cssClass += "good";
                break;
            default:
                return "";
                break;
        }

        let node = '<div title="" class="icon-gps' + cssClass + '">' + AssetIcons.IconGpsTemplateMarkup + '</div>';
        return node;
    }

    renderDltStatusColumn(data) {
        if (data) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data) {
                case Enums.ModelData.DltStatus.Success:
                    cssClass += "success";
                    iconTemplateMarkup = AssetIcons.IconDltStatusSuccessTemplateMarkup;
                    break;
                case Enums.ModelData.DltStatus.Fail:
                    cssClass += "fail";
                    iconTemplateMarkup = AssetIcons.IconDltStatusFailTemplateMarkup;
                    break;
            }

            let node = '<div title="" class="icon-dltstatus' + cssClass + '">' + iconTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderEngineColumn(data) {
        if (!_.isNil(data)) {

            var engine = {};
            var d = {};
            for (var i = 0; i < Object.keys(data).length; i++) {
                d = data[Object.keys(data)[i]];

                if (d.featureId == 1) {
                    engine = d;
                }
            }

            var cssClass = " ";
            switch (engine.valueDigital) {
                case 0:
                    cssClass += "off";
                    break;
                case 1:
                    cssClass += "on";
                    break;
                default:
                    return "";
                    break;
            }

            let node = '<div title="" class="icon-engine' + cssClass + '">' + AssetIcons.IconEngineTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    };

    renderShipmentWayPoint(data) {

        if (!_.isNil(data)) {
            var node = null;
            if (data.waypointName == null) {
                node = "-";
            } else {
                var img = data.iconName != null ? '<img style="width:16px;" src="' + data.iconName + '"/>' : '';
                var eta = data.formatETA == null ? '-' : data.formatETA;
                var ne = data.formatNE == null ? '-' : data.formatNE;
                var html = img + data.waypointName + '<br/>' +
                    'ETA ' + eta + '<br/>' +
                    'NE ' + ne;

                node = '<div style="color:' + data.etaColor + ';">' + html + '</div>';
            }


            return node;
        } else {
            return "";
        }
    }

    renderShipmentStartDate(data) {

        if (!_.isNil(data)) {
            var html = ""

            if (data.formatPlanStartDate != '' && data.formatPlanStartDate != null) {
                html += 'Plan ' + '<span>' + data.formatPlanStartDate + '</span>';
            }
            if (data.formatActualStartDate != '' && data.formatActualStartDate != null) {
                var isStartLateColor = data.startLateColor;
                html += '<br/>Actual ' + '<span style="color:' + isStartLateColor + ';">' + data.formatActualStartDate + '</span>';
            }

            let node = '<div>' + html + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderShipmentEndDate(data) {
        if (!_.isNil(data)) {
            var html = ""

            if (data.formatPlanEndDate != '' && data.formatPlanEndDate != null) {
                html += 'Plan ' + '<span>' + data.formatPlanEndDate + '</span>';
            }

            if (data.formatActualEndDate != '' && data.formatActualEndDate != null) {
                var isEndLateColor = data.endLateColor;
                html += '<br/>Actual ' + '<span style="color:' + isEndLateColor + ';">' + data.formatActualEndDate + '</span>';
            }

            let node = '<div>' + html + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderShipmentColorData(data) {
        if (!_.isNil(data)) {
            var html = ""
            html += '<span style="color:' + data.color + ';">' + data.text + '</span>';
            return html;
        } else {
            return "";
        }
    }

    renderPOSearchNextWayPoint(data) {
        if (!_.isNil(data)) {
            var html = "";

            if (data.text == null && data.icon == null) {
                html = "-";
            } else {
                var img = data.icon == null ? '' : '<img style="width:16px;" src="' + data.icon + '"/>';
                var text = data.text == null ? '-' : "<span>" + data.text + "</span>";
                html += img + text;
            }

            return html;
        } else {
            return "-";
        }
    }

    renderConfirmDMDate(data) {
        if (!_.isNil(data)) {
            var html = "";

            if (data.plan != '' && data.plan != null) {
                html += 'Plan ' + '<span>' + data.plan + '</span>';
            }

            if (data.actual != '' && data.actual != null) {
                var isEndLateColor = data.actualColor;
                html += '<br/>Actual ' + '<span style="color:' + isEndLateColor + ';">' + data.actual + '</span>';
            }

            let node = '<div>' + html + '</div>';
            return node;
        } else {
            return "-";
        }
    }

    renderGateColumn(data) {
        if (!_.isNil(data) && !_.isNil(data.valueDigital)) {
            var cssClass = " ";
            var iconTemplateMarkup = null;
            switch (data.valueDigital) {
                case 0:
                    iconTemplateMarkup = AssetIcons.IconGateOffTemplateMarkup;
                    cssClass += "off";
                    break;
                case 1:
                    iconTemplateMarkup = AssetIcons.IconGateOnTemplateMarkup;
                    cssClass += "on";
                    break;
            }

            let node = '<div title="" class="icon-gate' + cssClass + '">' + iconTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    };

    renderSOSColumn(data) {
        if (!_.isNil(data)) {
            var cssClass = " ";
            switch (data) {
                case 0:
                    cssClass += "off";
                    break;
                case 1:
                    cssClass += "on";
                    break;
            }

            let node = '<div title="" class="icon-sos' + cssClass + '">' + AssetIcons.IconSosTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    };

    renderPowerColumn(data) {
        if (!_.isNil(data)) {
            var cssClass = " ";
            switch (data) {
                case 0:
                    cssClass += "off";
                    break;
                case 1:
                    cssClass += "on";
                    break;
            }

            let node = '<div title="" class="icon-power' + cssClass + '">' + AssetIcons.IconPowerTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderPoiIconColumn(data) {
        if (data) {
            var poiIcons = this.poiIcons();
            var poiIcon = poiIcons[data];

            let node = '<img class="icon-poi" title="" src="' + poiIcon + '" />';
            return node;
        } else {
            return "";
        }
    }

    renderDelayColumn(data) {
        if (data && data == true) {
            let node = '<div title="" class="icon-delay">' + AssetIcons.IconDelayTemplateMarkup + '</div>';
            return node;
        } else {
            return "";
        }
    }

    renderVendorLogColumn(data) {
        let node = svgSearch;
        return node;
    }

    renderPlayVideoIcon(data) {
        return svgVdo;
    }
    renderVideoIcon(data) { //if data is json use this function

        if (data.deviceId == null && data.deviceCode == null) {
            return "";
        } else {
            return svgVdo;
        }

    }



    renderShipmentTemplateDeleteIcon() {
        return svgDelete;
    }
    renderShipmentTemplateUpdateIcon() {
        return svgEdit;
    }
    renderShipmentDeleteIcon(data) {
        let node = "";
        switch (data) {
            case Enums.JobStatus.Unassigned:
            case Enums.JobStatus.Waiting:
                node = svgDelete;
                break;
            default:
                node = "";
                break;
        }

        return node;
    }

    renderShipmentUpdateIcon(data) {
        let node = "";
        switch (data) {
            case Enums.JobStatus.Finished:
            case Enums.JobStatus.FinishedLate:
            case Enums.JobStatus.FinishedIncomplete:
            case Enums.JobStatus.FinishedLateIncomplete:
            case Enums.JobStatus.Cancelled:
                node = "";
                break;
            default:
                node = svgEdit;
                break;
        }
        return node;
    }

    renderUpdateIcon(data) {
        let node = svgEdit;
        return node;
    }

    renderDeleteIcon(data) {
        let node = data ? svgDelete : "";
        return node;
    }

    renderCreateIcon(data) {
        let node = data ? svgCreate : "";
        return node;
    }

    renderUpdateAlertTicketIcon(data) {
        let node = "";
        switch (data) {
            case Enums.ModelData.AlertTicketStatus.New:
            case Enums.ModelData.AlertTicketStatus.Inprogress:
                node = svgEdit;
                break;
            case Enums.ModelData.AlertTicketStatus.Close:
                break;
            default:
                node = "";
                break;
        }
        return node;
    }

}

export default DataGridColumns;