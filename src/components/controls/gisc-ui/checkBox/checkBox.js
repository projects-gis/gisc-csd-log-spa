import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./checkBox.html";
/**
 * 
 * 
 * @class CheckBox
 * @extends {ControlBase}
 */
class CheckBox extends ControlBase {
    /**
     * Creates an instance of CheckBox.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.text = params.text;
        this.checked = this.ensureObservable(params.checked ,false);

        this.isCheckboxFocus = ko.observable();
        this.cssFocusClass = ko.pureComputed(()=> {
            // Your pure compute logic
            return this.isCheckboxFocus() ? 'focus' : '';
        });
    }

    onLoad() {}
    onUnload() {
        this.checked = null;
    }
}

export default {
    viewModel: CheckBox,
    template: templateMarkup
};