import ko from "knockout";
import "jquery";
import "jquery-minicolors";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./colorPicker.html";
import Logger from "../../../../app/frameworks/core/logger";

/**
 * 
 * 
 * @class ColorPicker
 * @extends {ControlBase}
 */
class ColorPicker extends ControlBase {
    /**
     * Creates an instance of ColorPicker.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.id = this.ensureNonObservable(params.id, this.id);
        this.value = this.ensureObservable(params.value);
        this.format=this.ensureNonObservable(params.format, "hex");
    }

    onDomReady() {
        var self = this;
        var $el = $("#" + self.id);

        let setting = {
            animationSpeed: 50,
            animationEasing: 'swing',
            change: function (valuePicker, opacity) {
                if (!valuePicker) return;
                if (typeof console === 'object') {
                    self.value(valuePicker);
                }
            },
            changeDelay: 2000,
            control: 'hue',
            dataUris: true,
            defaultValue: '',
            format: self.format,
            hide: null,
            hideSpeed: 100,
            inline: false,
            keywords: '',
            letterCase: 'lowercase',
            opacity: false,
            position: 'bottom left',
            show: null,
            showSpeed: 100,
            theme: 'default',
            swatches: []
        };
        var testColors = $el.minicolors(setting);
    }

    //afterRender(element) {
       
    //    var self = this;
    //    var $el = $("#" + self.id);
        
    //    let setting = {
    //        animationSpeed: 50,
    //        animationEasing: 'swing',
    //        change: function(valuePicker, opacity) {
    //            if (!valuePicker) return;
    //            if (typeof console === 'object') {
    //                self.value(valuePicker);
    //            }
    //        },
    //        changeDelay: 2000,
    //        control: 'hue',
    //        dataUris: true,
    //        defaultValue: '',
    //        format: self.format,
    //        hide: null,
    //        hideSpeed: 100,
    //        inline: false,
    //        keywords: '',
    //        letterCase: 'lowercase',
    //        opacity: false,
    //        position: 'bottom left',
    //        show: null,
    //        showSpeed: 100,
    //        theme: 'default',
    //        swatches: []
    //    };
    //    var testColors = $el.minicolors(setting);
    //}
    onUnload() {
    }
}

export default {
    viewModel: ColorPicker,
    template: templateMarkup
};