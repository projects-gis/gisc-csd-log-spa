import Singleton from "../../../../app/frameworks/core/singleton";

const MessageBoxCtrlInstanceKey = "messageboxControl";

class MessageBoxManager {
    static getInstance() {
        return Singleton.getInstance(MessageBoxCtrlInstanceKey);
    }
    static ensureInstance(instance) {
        return Singleton.getInstance(MessageBoxCtrlInstanceKey, instance);
    }
}

export default MessageBoxManager;