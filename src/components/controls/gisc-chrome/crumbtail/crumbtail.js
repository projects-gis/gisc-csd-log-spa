import "jquery";
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./crumbtail.html";
import Logger from "../../../../app/frameworks/core/logger";
import {DropMenu} from "../shell/shellComponents";

/**
 * Handle top navigation menu.
 * 
 * @class CrumbtailControl
 * @extends {ControlBase}
 */
class CrumbtailControl extends ControlBase {
    /**
     * Creates an instance of CrumbtailControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.journey = this.ensureObservable(params.journey, null);
        this.breadCrumbsVisile = ko.pureComputed(function() {
            if (this.journey()) {
                return this.journey().items();
            }
            return [];
        }, this);
        this.breadCrumbsOverFlow = ko.pureComputed(function() {
            if (this.journey()) {
                return this.journey().items().slice(0).reverse();
            }
            return [];
        }, this);
        this.optimizeLayoutHandler = null;
        this.dropMenu = new DropMenu();
        this.lastBladeTitle = ko.computed(function(){
            var bladeTitle;
            var allItems = this.breadCrumbsOverFlow();
            if (allItems.length > 0) {
                bladeTitle = allItems[0].title();
            }

            // Ensure blade title is not null or undefined.
            if(bladeTitle === "" || bladeTitle === undefined) {
                bladeTitle = this.i18n("Common_AppTitle")();
            }

            // Apply latest blade to window title too.
            window.document.title = bladeTitle;

            return bladeTitle;
        }.bind(this)).extend({ throttle: 100 });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Optimize crumbtail based on windows width.
     */
    optimizedLayout() {
        var self = this;
        var maximumVisibleItem = this.breadCrumbsVisile().length;
        var overflowIndex = 0;
        var resetAllCrumb = function() {
            // Reset visible crumb state.
            $(".app-breadcrumb-wrapper > .app-breadcrumb-crumb").each(function(i, c) {
                var $crumb = $(c);
                $crumb.removeClass("hidden");
                $crumb.next().removeClass("hidden");
            });

            // Reset expand button state.
            $(".app-breadcrumb-dropmenu").hide();

            // Reset invisible crumb state.
            $(".app-breadcrumb-overflow > li").hide();
        };

        var detectOverflow = function() {
            var maxContainer = $(".app-breadcrumb").width();
            var crumbContainer = $(".app-breadcrumb-wrapper").width();
            return (crumbContainer >= maxContainer);
        };

        var collapseCrumb = function(overflowIndex) {
            $(".app-breadcrumb-dropmenu").show();

            // Hide visible element.
            $(".app-breadcrumb-wrapper > .app-breadcrumb-crumb").each(function(i, c) {
                if (i <= overflowIndex) {
                    var $crumb = $(c);
                    $crumb.addClass("hidden");
                    $crumb.next().addClass("hidden");
                }
            });

            // Show overflow menu based on visible item.
            $($(".app-breadcrumb-overflow > li").get().reverse()).each(function(i, li) {
                if (i <= overflowIndex) {
                    var $li = $(li);
                    $li.show();
                }
            });
        };

        resetAllCrumb();

        while (detectOverflow() && (overflowIndex < maximumVisibleItem)) {
            collapseCrumb(overflowIndex);
            overflowIndex++;
        }
    }

    /**
     * When crumbtail options are added.
     */
    onItemChanged() {
        this.optimizedLayout(true);
    }

    /**
     * When crumbtail options are removed.
     */
    onItemRemoved(elem, index) {
        // Every item removed.
        var $elem = $(elem);
        $elem.remove();
        if ($elem.hasClass("app-breadcrumb-divider")) {
            this.optimizedLayout();
        }
    }

    /**
     * @lifecycle Called when view dom is ready to use.
     */
    onDomReady() {
        this.optimizeLayoutHandler = this.optimizedLayout.bind(this);
        $(window).on("resize", this.optimizeLayoutHandler);
    }

     /**
     * @lifecycle Called when dom is removed from page.
     */
    onUnload() {
        $(window).off("resize", this.optimizeLayoutHandler);
    }
}

export default {
    viewModel: CrumbtailControl,
    template: templateMarkup
};