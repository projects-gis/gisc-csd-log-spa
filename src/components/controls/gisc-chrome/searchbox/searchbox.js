import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./searchbox.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";

/**
 * Handle quick search.
 * 
 * @class SearchBoxControl
 * @extends {ControlBase}
 */
class SearchBoxControl extends ControlBase {
    /**
     * Creates an instance of SearchBoxControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.visible(WebConfig.userSession.currentPortal === "company-workspace");
    }
        /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
}

export default {
    viewModel: SearchBoxControl,
    template: templateMarkup
};