import ko from "knockout";
import Singleton from "../../../../app/frameworks/core/Singleton";

class ContextMenuManager {
    constructor() 
    {
        this.visible = ko.observable(false);
        this.items = ko.observableArray();
        this.offsetTop = ko.observable("10px");
        this.offsetLeft = ko.observable("10px");
        this.onItemClickDeferred = null;
    }

    /**
     * Get context menu control instance.
     * 
     * @param {any} params
     * @returns {ContextMenuControl}
     */
    static getInstance(params) {
        return Singleton.getInstance("contextMenuManager", new ContextMenuManager());
    }

    /**
     * Display context menu.
     * @param {any} menuItems 
     * @param {any} fnCallback
     * @param {number} x 
     * @param {number} y 
     * @return {jQuery deferred}
     */
    show(menuItems, x, y) {
        // Clear menu item handler.
        if(this.onItemClickHandler) {
            this.onItemClickHandler.reject();
        }
        this.onItemClickHandler = $.Deferred();

        // Clear old menu.
        this.items.removeAll();

        // Clear menu item if exist.
        if(menuItems && menuItems.length) {
            // Reset menu handler with new array.
            menuItems.forEach((item) => {
                this.items.push(item);
            });
        }

        // Update position.
        this.offsetLeft(x + "px");
        this.offsetTop(y + "px");
        this.visible(true);

        // Return jquery deferred.
        return this.onItemClickHandler;
    }
}


export default ContextMenuManager;