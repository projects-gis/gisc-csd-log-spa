import Singleton from "../../../../app/frameworks/core/singleton";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

/**
 * Handle Map and SPA interaction.
 * 
 * @class MapManager
 */
class MapManager {
    /**
     * Creates an instance of MapManager.
     */
    constructor() {
        this._eventAggregator = new EventAggregator();
        this._mapOrigin = null;
        this._receiverWindow = null;
        this._receiverWindowMessageHandler = null;
    }

    /**
     * Create single instance of MapManager class.
     * @static
     * @returns MapManager
     * 
     */
    static getInstance() {
        return Singleton.getInstance("mapMgr", new MapManager());
    }

    /**
     * Initialize map instance with DOM element.
     * @param {any} mapElementId
     * @param {any} mapOrigin
     * @returns void
     * 
     */
    initialize(mapElementId, mapOrigin) {
        if (this._receiverWindow) {
            return;
        }

        // Store map endpoint for sending proper object.
        this._mapOrigin = mapOrigin;

        var $iframe = $("#" + mapElementId);
        if ($iframe.length) {
            // Finding instance of iframe content window.
            this._receiverWindow = $iframe.get(0).contentWindow;
            this._receiverWindowMessageHandler = this.postMessageCompleted.bind(this);

            // Binding on message handler.
            $(window).on("message", this._receiverWindowMessageHandler);
        } else {
            throw new Error("Map is not loaded.");
        }
    }

    /**
     * Call when map instance is removed from page.
     * 
     */
    destroy() {
        $(window).off("message", this._receiverWindowMessageHandler);
        this._mapOrigin = null;
        this._receiverWindow = null;
        this._receiverWindowMessageHandler = null;
        this._eventAggregator.destroy();
    }
    
    /**
     * Event handle which being called when map operation's completed.
     * 
     * @param {any} e
     */
    postMessageCompleted(e) {
        var data = e.originalEvent.data;
        this._eventAggregator.publish(EventAggregatorConstant.MAP_COMMAND_COMPLETE, data);
    }

    /**
     * Send request to map iframe.
     * 
     * @param {any} postParameter
     * 
     */
    sendRequest(postParameter) {
        if (this._receiverWindow != null){
            this._receiverWindow.postMessage(postParameter, "*");
        }
        
    }

    /**
     * Set user credential from MIS to map.
     * 
     * @param {any} userInfo
     * @param {any} i18n
     * @param {any} permissons
     * 
     */
    setStartupResource(userInfo, companySettings) {    
        
        var commandRequest = {
            command: "set-startup-resources",
            param: {
                //user information
                userInfo: {
                    info: userInfo,
                    appLanguage: userInfo["_prefUserLanguage"]
                },

                //i18next format resources
                i18nResources: userInfo.i18n,

                //menu permissions
                permission: userInfo.permissions,
                showPoiCluster: true
            }
        };

        if (companySettings) {
            commandRequest.param.showPoiCluster = companySettings.showPoiCluster;
        }

        this.sendRequest(commandRequest);
    }

    /**
     * Clear drawing object within page.
     * 
     * @param {any} id
     */
    clear(id) {
        // Prepare object for command request.
        var commandRequest = {
            "command": "clear-graphics",
            "param": {}
        };

        // Adding guid to request object.
        //commandRequest.guid = id;

        // Send request to system
        this.sendRequest(commandRequest);
    }
    clearGraphicsId(id){
        var commandRequest = {
            "command": "clear-graphics-id",
            "param": id
        };

        // Adding guid to request object.
        //commandRequest.guid = id;

        // Send request to system
        this.sendRequest(commandRequest);
    }

    /**
     * Draw pin to map.
     * 
     * @param {any} id
     * @param {number} [lat=0]
     * @param {number} [lon=0]
     * @param {string} [pinUrl="http://bit.ly/2aLPTs9"]
     * 
     */
    drawPin(id, lat = 0, lon = 0, pinUrl = "http://bit.ly/2aLPTs9") {
        var commandRequest = {
            command: "draw-pin",
            param: {
                zoom: 16,
                pin: {
                    url: pinUrl,
                    width: 24,
                    height: 35,
                    offset: {
                        x: 0,
                        y: 17
                    }
                },
                location: {
                    longitude: lon,
                    latitude: lat
                }
            }
        };

        // Adding guid to request object.
        commandRequest.guid = id;

        this.sendRequest(commandRequest);
    }

    /**
     * Draw vehicle to page.
     * 
     * @param {any} id
     * 
     */
    drawVehicle(id) {
        var commandRequest = {
            command: "draw-vehicle",
            param: {
                pin: {
                    url: "http://bit.ly/2b0LjZY",
                    width: 24,
                    height: 24,
                    angle: 30,
                    offset: {
                        x: 0,
                        y: 0
                    }
                },
                location: {
                    longitude: 100.53065577,
                    latitude: 13.7230276952
                },
                attributes: {
                    ID: "ASSET-001",
                    DRIVER: "Mr. X",
                    COMPANY: 1,
                    DEPARTMENT: 12
                }
            }
        };

        // Adding guid to request object.
        commandRequest.guid = id;

        this.sendRequest(commandRequest);
    }


    //command-function
    drawOnePointZoom(id, location, symbol, zoom, attributes) {
        var commandRequest = {
            command: 'draw-one-point-zoom',
            param: {
                location: location,
                symbol: symbol,
                zoom: zoom,
                attributes: attributes,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawPointPoiArea(id, location, symbol, zoom, attributes) {
        var commandRequest = {
            command: 'draw-point-main-area',
            param: {
                location: location,
                symbol: symbol,
                zoom: zoom,
                attributes: attributes,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawAlertPoint(id, location, symbol, zoom, attributes){
        var commandRequest = {
            command: 'draw-alert-point',
            param: {
                location: location,
                symbol: symbol,
                zoom: zoom,
                attributes: attributes,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawOneLineZoom(id, path, color, style, width) {

        var commandRequest = {
            command: 'draw-one-line-zoom',
            param: {
                path: path,
                symbol: {
                    color: color,
                    style: style,
                    width: width,
                },
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawOnePolygonZoom(id, ring, fillColor, borderColor, fillOpacity, borderOpacity, attributes,layer) {

        var commandRequest = {
            command: 'draw-one-polygon-zoom',
            param: {
                ring: ring,
                color: {
                    fill: fillColor,
                    border: borderColor,
                },
                opacity: {
                    fill: fillOpacity,
                    border: borderOpacity,
                },
                guid: id,
                attributes: attributes
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }
    drawOnePolygonZoomArea(id,
         ring, 
         fillColor, 
         borderColor, 
         fillOpacity,
          borderOpacity, 
          attributes,
          layer){
        var commandRequest = {
            command: 'draw-one-polygon-zoom-area',
            param: {
                ring: ring,
                color: {
                    fill: fillColor,
                    border: borderColor,
                },
                opacity: {
                    fill: fillOpacity,
                    border: borderOpacity,
                },
                guid: id,
                attributes: attributes,
                layer:layer
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }
    drawMultiplePolygonZoomArea(id,data){
        var commandRequest = {
            command: 'draw-multiple-polygon-zoom-area',
            param: {
                data:data,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawMultiplePolygonZoom(id, ring, fillColor, borderColor, fillOpacity, borderOpacity) {

        var commandRequest = {
            command: 'draw-multiple-polygon-zoom',
            param: {
                ring: ring,
                color: {
                    fill: fillColor,
                    border: borderColor,
                },
                opacity: {
                    fill: fillOpacity,
                    border: borderOpacity,
                },
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawMultipleLineZoom(id, paths, color, style, width) {

        var commandRequest = {
            command: 'draw-multiple-line-zoom',
            param: {
                path: paths,
                symbol: {
                    color: color,
                    style: style,
                    width: width,
                },
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawMultipleVehicles(id, vehicles) {

        var commandRequest = {
            command: 'draw-multiple-vehicles',
            param: {
                vehicles: vehicles,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
    }

    zoomPoint(id, lat, lon, zoom) {
        var commandRequest = {
            command: 'zoom-point',
            param: {
                location: {
                    lat: lat,
                    lon: lon
                },
                zoom: zoom,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
    }

    drawOneRoute(id, stops, pics, path, color, width, opacity, attributes) {
        var commandRequest = {
            command: 'draw-one-route',
            param: {
                stops: stops,
                pics:  pics,
                path: path,
                symbol: {
                    color: color,
                    width: width,
                    opacity: opacity
                },
                attributes: attributes,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawPointBuffer(id, locationData, radius, unit, fillColor, borderColor, fillOpacity, borderOpacity){
        var commandRequest = {
            command: 'draw-point-buffer',
            param: {
                location : locationData,
                radius: radius,
                unit: unit,
                symbol: {
                    color: {
                        fill: fillColor,
                        border: borderColor,
                    },
                    opacity: {
                        fill: fillOpacity,
                        border: borderOpacity,
                    }
                },
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawRouteBuffer(id, path, radius, unit, fillColor, borderColor, fillOpacity, borderOpacity, generalize) {
        var commandRequest = {
            command: 'draw-route-buffer',
            param: {
                path : path,
                radius: radius,
                unit: unit,
                symbol: {
                    color: {
                        fill: fillColor,
                        border: borderColor,
                    },
                    opacity: {
                        fill: fillOpacity,
                        border: borderOpacity,
                    }
                },
                generalize: generalize,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawMultiplePoint(id, points) {
        var commandRequest = {
            command: 'draw-multiple-point',
            param: {
                points: points,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawCustomPOISuggestion(id, points, mainPoint, radius, unit, zoom) {
        var commandRequest = {
            command: 'draw-poi-suggestion',
            param: {
                zoom: zoom,
                radius: radius,
                unit: unit,
                mainPoint: mainPoint,
                points: points,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    drawCustomArea(id, ring, fillColor, borderColor, fillOpacity, borderOpacity) {
        var commandRequest = {
            command: 'draw-custom-area',
            param: {
                ring: ring,
                symbol: {
                    color: {
                        fill: fillColor,
                        border: borderColor,
                    },
                    opacity: {
                        fill: fillOpacity,
                        border: borderOpacity,
                    }
                },
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    trackVehicles(id, vehicles) {
        var commandRequest = {
            command: "track-vehicles",
            param: {
                vehicles: vehicles,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
    }

    trackNearestVehicles(id, vehicles) {
        var commandRequest = {
            command: "track-nearest-vehicles",
            param: {
                vehicles: vehicles,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
    }

    removeTrackVehicles(id, vehiclesIDs) {
        var commandRequest = {
            command: "remove-track-vehicles",
            param: {
                vehicleIDs: vehiclesIDs,
                guid: id
            }
        };

        this.sendRequest(commandRequest);
    }

    resetFilterVehicleOnMap() {
        var commandRequest = {
            command: "reset-vehicle-filter",
            param: {
                
            }
        };

        this.sendRequest(commandRequest);
    }

    enableClickMap(id) {
        var commandRequest = {
            command: "enable-click-map",
            param: {
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }
    drawPOIArea(id){
        var commandRequest = {
            command: "enable-draw-POIArea",
            param: {
                guid: id
            }
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    enableDrawingPolygon(id, points, areaUnit) {
        var commandRequest = {
            command: "enable-draw-polygon",
            param: {
                guid: id,
                areaUnit: areaUnit
            }
        };

        if(points)
            commandRequest.param["point"] = points;



        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }
    enableDrawingPolygonArea(id, points,areaUnit,name,typearea){
        var commandRequest = {
            command: "enable-draw-polygon-area",
            param: {
                guid: id,
                areaUnit: areaUnit
            }
        };

        if(points)
            commandRequest.param["point"] = points;

        if(name)
            commandRequest.param["name"] = name;
        
        if(typearea)
            commandRequest.param["typearea"] = typearea;


        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    cancelClickMap() {
        var commandRequest = {
            command: "cancel-click-map"
        };

        this.sendRequest(commandRequest);
        this.enableClickAndHideMenu();
    }

    cancelDrawingPolygon() {
        var commandRequest = {
            command: "cancel-draw-polygon"
        };

        this.sendRequest(commandRequest);
    }


    printRouteMap(distance, duration, directions) {
        var commandRequest = {
            command: "print-route-map",
            param: {
                distance: distance,
                duration: duration,
                direction: directions
            }
        };

        this.sendRequest(commandRequest);
    }

    playbackAnimate(playbackData){
        var commandRequest = {
            command: "playback-animate-vehicles",
            param: playbackData
        };

        this.sendRequest(commandRequest);
    }

    playbackFootprint(playbackData){
        var commandRequest = {
            command: "playback-footprint-vehicles",
            param: playbackData
        };

        this.sendRequest(commandRequest);
    }

    playbackFully(playbackData){
        var commandRequest = {
            command: "playback-fully-vehicles",
            param: playbackData
        };

        this.sendRequest(commandRequest);

    }

    playbackDrawAlertChart(aleryPlaybackData) {
        var commandRequest = {
            command: "playback-draw-alert-chart",
            param: aleryPlaybackData
        };

        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    setFollowTracking(flg) {
        var commandRequest = {
            command: "follow-trackable",
            param: { follow : flg }
        };

        this.sendRequest(commandRequest);
        }
    
    openCustomPOILayer() {
        var commandRequest = {
            command: "open-poi-layer",
            param:{ }
        };

        this.sendRequest(commandRequest);
    }

    openShipmentLegend(param) {
        var commandRequest = {
            command: "open-shipment-legend",
            param: param
        };
        this.disableClickAndHideMenu();
        this.sendRequest(commandRequest);
    }
    openShipmentLegendTemplate(param) {
        var commandRequest = {
            command: "open-shipment-legend-template",
            param: param
        };
        this.disableClickAndHideMenu();
        this.sendRequest(commandRequest);
    }
    closePlaybackFootprint() {
        var commandRequest = {
            command: "close-footprint-vehicles",
            param: {}
        };

        this.sendRequest(commandRequest);
    }


    showAllTrackVehicle() {
        var commandRequest = {
            command: "show-all-trackvehicle",
            param: {}
        };
        this.sendRequest(commandRequest);
        this.enableClickAndHideMenu();
    }

    hideAllTrackVehicle() {
        var commandRequest = {
            command: "hide-all-trackvehicle",
            param: {}
        };
        this.sendRequest(commandRequest);
        this.disableClickAndHideMenu();
    }

    closePlayback() {
        var commandRequest = {
            command: "close-playback",
            param: {}
        };
        this.sendRequest(commandRequest);
        this.enableClickAndHideMenu();
    }

    closeViewOnMap(param = {}) {
        var commandRequest = {
            command: "close-viewonmap",
            param: param
        };
        this.sendRequest(commandRequest);
        this.enableClickAndHideMenu();
    }c
    closeLiveView() {
        var commandRequest = {
            command: "close-liveview",
            param: {}
        };
        this.sendRequest(commandRequest);
        this.enableClickAndHideMenu();
    }
    enableClickAndHideMenu() {
        var commandRequest = {
            command: "enable-click-map-and-hide",
            param: {}
        };
        this.sendRequest(commandRequest);
    }

    disableClickAndHideMenu() {
        var commandRequest = {
            command: "disable-click-map-and-hide",
            param: {}
        };
        this.sendRequest(commandRequest);
    }

    panOnePointZoom(id, location, symbol, zoom, attributes){
        var commandRequest = {
            command: "pan-one-point-zoom",
            param:{
                location: location,
                symbol: symbol,
                zoom: zoom,
                attributes: attributes,
                guid: id
            }
        }
        this.sendRequest(commandRequest);
    }


    openLiveview(params) {
        var commandRequest = {
            command: "open-liveview",
            param: params
        }
        this.sendRequest(commandRequest);
    }
}

export default MapManager;