﻿import Singleton from "../../../../app/frameworks/core/singleton";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

/**
 * Handle Map and SPA interaction for Working Map Control.
 * 
 * @class WorkingMapManager
 */
class WorkingMapManager {
    /**
     * Creates an instance of MapManager.
     */
    constructor() {
        this._eventAggregator = new EventAggregator();
        this._mapOrigin = null;
        this._receiverWindow = null;
        this._receiverWindowMessageHandler = null;
    }

    /**
     * Create single instance of MapManager class.
     * @static
     * @returns MapManager
     * 
     */
    static getInstance() {
        return Singleton.getInstance("workingMapMgr", new WorkingMapManager());
    }

    /**
     * Initialize map instance with DOM element.
     * @param {any} mapElementId
     * @param {any} mapOrigin
     * @returns void
     * 
     */
    initialize(mapElementId, mapOrigin) {
        if (this._receiverWindow) {
            return;
        }

        // Store map endpoint for sending proper object.
        this._mapOrigin = mapOrigin;

        var $iframe = $("#" + mapElementId);
        if ($iframe.length) {
            // Finding instance of iframe content window.
            this._receiverWindow = $iframe.get(0).contentWindow;
            this._receiverWindowMessageHandler = this.postMessageCompleted.bind(this);
          
            // Binding on message handler.
            $(window).on("message", this._receiverWindowMessageHandler);
        } else {
            throw new Error("Map is not loaded.");
        }
    }

    /**
     * Call when map instance is removed from page.
     * 
     */
    destroy() {
        $(window).off("message", this._receiverWindowMessageHandler);
        this._mapOrigin = null;
        this._receiverWindow = null;
        this._receiverWindowMessageHandler = null;
        this._eventAggregator.destroy();
    }
    
    /**
     * Event handle which being called when map operation's completed.
     * 
     * @param {any} e
     */
    postMessageCompleted(e) {
        var data = e.originalEvent.data;
        this._eventAggregator.publish(EventAggregatorConstant.MAP_COMMAND_COMPLETE, data);
    }

    /**
     * Send request to map iframe.
     * 
     * @param {any} postParameter
     * 
     */
    sendRequest(postParameter) {

        if (this._receiverWindow != null){
            this._receiverWindow.postMessage(postParameter, "*");
        }
        
    }

    /**
     * Set user credential from MIS to map.
     * 
     * @param {any} userInfo
     * @param {any} i18n
     * @param {any} permissons
     * 
     */
    setStartupResource(userInfo) {    
        var commandRequest = {
            command: "wm-set-startup-resources",
            param: {
                //user information
                userInfo: {
                    info: userInfo,
                    appLanguage: userInfo["_prefUserLanguage"]
                },

                //i18next format resources
                i18nResources: userInfo.i18n,

                //menu permissions
                permission:  userInfo.permissions
            }
        };

        this.sendRequest(commandRequest);
    }

    drawPolygonFromPolyLine(paths) {
        console.log("paths",paths)
        if(!paths) {
            paths = [[[100.53562921669744, 13.723118700804017], [100.53349417832177, 13.720888261272222], [100.53049010422535, 13.724056729787579], [100.52696031716205, 13.721419816542836], [100.5314878859788, 13.724786305292977], [100.53179902222449, 13.725484611151588], [100.52671355393271, 13.721273899529635], [100.53201359894568, 13.725849396967572], [100.52635950234274, 13.721221786288643], [100.5324212947159, 13.726214182216284], [100.52612346794946, 13.721148827731808], [100.53264660027314, 13.726631078948756], [100.525855247048, 13.721200940988997], [100.53275388863372, 13.726943751011856], [100.52569431450713, 13.721273899529635], [100.53297919419096, 13.727308534558677], [100.52582306053982, 13.72164911452314], [100.53239983704378, 13.72700628537446], [100.5259839930807, 13.722024328916667], [100.5326036849289, 13.727475292562549], [100.526037637261, 13.72262883973311], [100.53215307381444, 13.727308534558677], [100.52610201027734, 13.723066587972742], [100.5319063105851, 13.727350224070754], [100.52573722985136, 13.722639262319722], [100.53182047989662, 13.727412758324991], [100.53338688996118, 13.72147192973986]]];
        }

        let commandRequest = {
            command: "wm-set-create-polygon-from-line",
            param: {
                paths: paths
              }
        };

        this.sendRequest(commandRequest);
    }

    drawRoute(paths){

        //paths = [[[100.53562921669744, 13.723118700804017], [100.53349417832177, 13.720888261272222], [100.53049010422535, 13.724056729787579], [100.52696031716205, 13.721419816542836], [100.5314878859788, 13.724786305292977], [100.53179902222449, 13.725484611151588], [100.52671355393271, 13.721273899529635], [100.53201359894568, 13.725849396967572], [100.52635950234274, 13.721221786288643], [100.5324212947159, 13.726214182216284], [100.52612346794946, 13.721148827731808], [100.53264660027314, 13.726631078948756], [100.525855247048, 13.721200940988997], [100.53275388863372, 13.726943751011856], [100.52569431450713, 13.721273899529635], [100.53297919419096, 13.727308534558677], [100.52582306053982, 13.72164911452314], [100.53239983704378, 13.72700628537446], [100.5259839930807, 13.722024328916667], [100.5326036849289, 13.727475292562549], [100.526037637261, 13.72262883973311], [100.53215307381444, 13.727308534558677], [100.52610201027734, 13.723066587972742], [100.5319063105851, 13.727350224070754], [100.52573722985136, 13.722639262319722], [100.53182047989662, 13.727412758324991], [100.53338688996118, 13.72147192973986]]];
    
        let commandRequest = {
            command: "wm-create-route-from-line",
            param: {
                paths: paths
              }
        };

        this.sendRequest(commandRequest);
    }

    getResultPolygon() {
        let commandRequest = {
            command: "wm-get-result-polygon",
            param: { }
        };

        this.sendRequest(commandRequest);
    }

    clearGraphics() {
        let commandRequest = {
            command: "wm-clear-graphics",
            param: { }
        };

        this.sendRequest(commandRequest);
    }
}


export default WorkingMapManager;