import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./liveviewwidgetswitcher.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../app/frameworks/core/utility";
import {DropMenu} from "../shell/shellComponents";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import WebRequestMDVR from "../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import IQTechUtility from "../../../../app/frameworks/core/iqtechUtility";
import EncryptUtility from "../../../../app/frameworks/core/encryptUtility";
import WebRequestUserApiCore from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestFleetMonitoring from "../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";

/**
 * Handle top navigation menu.
 * 
 * @class LiveViewWidgetSwitcherControl
 * @extends {ControlBase}
 */
class LiveViewWidgetSwitcherControl extends ControlBase {
    /**
     * Creates an instance of LiveViewWidgetSwitcherControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        // Hide this item if user doesnt' have permission or in admin portal.
        var trackAvailable = WebConfig.userSession.hasPermission(Constants.Permission.LiveVideo)
            && WebConfig.userSession.currentPortal == "company-workspace";
        this.visible(trackAvailable);
        //this.visible(false);

        this.companyMenu = new DropMenu(this.i18n('LiveView_Video')());
        this.isActive = ko.observable(false);
        this.dataLiveView = ko.observable();
        this.selectedVehicles = ko.observableArray([]);

        this.tempViewVehicleIds = ko.observableArray([]);
        this.tempViewDeliveryManIds = ko.observableArray([]);
        this.tempFollowVehicleIds = ko.observableArray([]);
        this.tempFollowDeliveryManIds = ko.observableArray([]);
        this.tempTrackLocationFilter = ko.observable();


    }

    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUserApiCore() {
        return WebRequestUserApiCore.getInstance();
    }

    get iqtechUtility() {
        return IQTechUtility.getInstance();
    }
    
    get encryptUtility() {
        return EncryptUtility.getInstance();
    }

     /**
     * Find drivers and vehicles list.
     * 
     * @returns
     * 
     * @memberOf PlaybackScreen
     */
    listVehicles(buId, incBu) {
        var dfd = $.Deferred();
        let filter = {
            businessUnitId: buId,
            includeSubBusinessUnitId: incBu
        }
        var d1 = this.webRequestMDVR.listMDVRLiveVdo(filter);
        $.when(d1).done((r1) => {
            dfd.resolve(r1);
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Open widget when user clicks on menu.
     */
    onClick()
    {   
        var dfd = $.Deferred();
        this.getCurrentData().done(()=>{
            

            var vehicleInfo = new Array();
            var promises = new Array();
            
            _.forEach(this.selectedVehicles(), (item) => {
                if(item.isView){
                    let vehicle = (item.brand == Enums.ModelData.MDVRModel.IQTech) ? this.iqtechUtility.getDevIdNo(item.deviceId.toString(),this.encryptUtility.decryption(item.mdvrServerKey)) :  this.hikvisionUtility.liveView(item.deviceId.toString());
                    let info = vehicle.done((res)=>{
                        return new Promise((resolve, reject) =>{
                            item.cameraConfig = this.encryptUtility.decryption(item.mdvrServerKey);
                            let obj = Object.assign({}, item, res);
                            vehicleInfo.push(obj);
                            resolve();
                        });
                    }).fail((e)=>{
                        console.log(e);
                        reject(e);
                        this.handleError(e);
                    });
                    promises.push(info);
                }
            });
            
            Promise.all(promises).then((res) => {
                this.replaceTrackLocation(vehicleInfo).done(()=>{

                    WebConfig.fleetMonitoring.syncTrackLocation(
                        WebRequestFleetMonitoring.getInstance(),
                        WebConfig.userSession,
                        WebConfig.fleetMonitoring.trackLocationFilter())
                    .done((resultTrackLocation) => {
                        let vehicles = WebConfig.fleetMonitoring.trackLocationItems();
                        _.forEach(vehicleInfo, (item)=>{
                            let fData = _.find(vehicles, (v)=>{
                                return v.vehicleId == item.id;
                            });
                            if(_.size(fData)){
                                item.location = ko.observable(fData.location);
                                item.latitude = ko.observable(fData.latitude);
                                item.longitude = ko.observable(fData.longitude);
                                item.businessUnitName = fData.businessUnitName;
                            }else{
                                item.location = ko.observable("");
                                item.latitude = ko.observable("");
                                item.longitude = ko.observable("");
                                item.businessUnitName = "";
                            }
                            
                        });
                        dfd.resolve();
                        let width = $( window ).width() - 604;
                        width = width > 604 ? width : 0;
                        this.widget('cw-fleet-monitoring-liveview-widget', vehicleInfo, {
                            title: this.i18n("FleetMonitoring_Live_Video")(),
                            modal: false,
                            resizable: true,
                            minimize: false,
                            maximize: true,
                            target: "cw-fleet-monitoring-liveview-widget",
                            width: "640",
                            height: $( window ).height() - 110,
                            id: 'cw-fleet-monitoring-liveview-widget',
                            left: width - 90,
                            bottom: "1%",
                            onClose: () => {
                                var dfdClose = $.Deferred();
                                this.replaceTrackLocation(false).done(()=>{
                                    MapManager.getInstance().clear();
                                    dfdClose.resolve();
                                });
                            }
                        });
                    });
                });
            }).catch((e)=>{
                console.log(e);
                dfd.reject();
            })
        });
        return dfd;
    }


    getCurrentData(){
        var dfd = $.Deferred();
        let selectedVehicles = new Array();
        var preferenceFilter = {
            keys: [
                Enums.ModelData.UserPreferenceKey.DataGridState,
            ]
        };
        this.webRequestUserApiCore.getUserPreferences(preferenceFilter)
        .done((res) => {
            
            let data = JSON.parse(res[0].value);
            _.forEach(data, (item)=>{
                if(item.id == "MDVR-LiveView"){
                    
                    this.dataLiveView(item.data);
                }
            });


            if(_.size(this.dataLiveView())){

                this.listVehicles(this.dataLiveView().businessUnitId, this.dataLiveView().includeSubBusinessUnitId).done((res)=>{
                    //find view and follow vehicles
                    _.forEach(this.dataLiveView().viewVehicleIds, (id)=>{
                        let vVehicle = _.find(res, (o)=>{
                            return o.id == id;
                        });

                        let fVehicle = _.find(this.dataLiveView().followVehicleIds, (fid)=>{
                            return fid == id;
                        });

                        if(_.size(vVehicle)){
                            vVehicle.isView = true;
                            vVehicle.isFollow = (_.size(fVehicle)) ? true : false;
                            selectedVehicles.push(vVehicle);
                        }
                    });
                    this.selectedVehicles(selectedVehicles);
                    dfd.resolve();
                }).fail(e=>dfd.reject(e));
            }
        }).fail(e => dfd.reject(e));
        return dfd;
    }
    

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        
    }

    /**
     * update track location
     * if live view is true then replace live view to track loc
     * if live view is false then replace track loc to live view
     */
    replaceTrackLocation(liveView = true){
        var dfd = $.Deferred();
        if(liveView){
            
            this.tempViewVehicleIds(_.clone(WebConfig.fleetMonitoring.trackLocationViewVehicleIds()));
            this.tempViewDeliveryManIds(_.clone(WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds()));
            this.tempFollowVehicleIds(_.clone(WebConfig.fleetMonitoring.trackLocationFollowVehicleIds()));
            this.tempFollowDeliveryManIds(_.clone(WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds()));
            this.tempTrackLocationFilter(_.clone(WebConfig.fleetMonitoring.trackLocationFilter()));

            let newViewVehicleIds = new Array();
            let newFollowVehicleIds = new Array();
            _.forEach(this.selectedVehicles(), (item)=>{
                if(item.isView){
                    newViewVehicleIds.push(item.id);
                }
                if(item.isFollow){
                    newFollowVehicleIds.push(item.id);
                }
            });

            // WebConfig.fleetMonitoring.trackLocationViewVehicleIds(newViewVehicleIds);
            // WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds([]);
            // WebConfig.fleetMonitoring.trackLocationFollowVehicleIds(newFollowVehicleIds);
            // WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds([]);
            WebConfig.fleetMonitoring.trackLocationFilter({
                deliveryManIds: [],
                // displayLength: 0,
                vehicleIds: []
            });
            this._updateTrackLocationAssetIds(
                //viewVehicleIds
                newViewVehicleIds,
                //viewDeliveryManIds
                [],
                //followVehicleIds
                newFollowVehicleIds,
                //followDeliveryManIds
                []
            ).done(()=>{
                dfd.resolve();
            });
            

        }else{

            // WebConfig.fleetMonitoring.trackLocationViewVehicleIds(this.tempViewVehicleIds());
            // WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds(this.tempViewDeliveryManIds());
            // WebConfig.fleetMonitoring.trackLocationFollowVehicleIds(this.tempFollowVehicleIds());
            // WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds(this.tempFollowDeliveryManIds());
            WebConfig.fleetMonitoring.trackLocationFilter(this.tempTrackLocationFilter());
            this._updateTrackLocationAssetIds(
                //viewVehicleIds
                this.tempViewVehicleIds(),
                //viewDeliveryManIds
                this.tempViewDeliveryManIds(),
                //followVehicleIds
                this.tempFollowVehicleIds(),
                //followDeliveryManIds
                this.tempFollowDeliveryManIds()
            ).done(()=>{
                dfd.resolve();
            })
            
        }

        return dfd;

    }

    /**
     * Update Track Location Asset Ids
     */
    _updateTrackLocationAssetIds(viewVehicleIds, viewDeliveryManIds, followVehicleIds, followDeliveryManIds) {
        return WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
            this.webRequestFleetMonitoring,
            WebConfig.userSession,
            viewVehicleIds,
            viewDeliveryManIds,
            followVehicleIds,
            followDeliveryManIds
        ).fail((e) => {
            this.handleError(e);
        });
    }

    onDomReady() {
    }
}

export default {
    viewModel: LiveViewWidgetSwitcherControl,
    template: templateMarkup
};