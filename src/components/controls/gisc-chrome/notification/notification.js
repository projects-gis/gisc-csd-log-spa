import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./notification.html";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import NotificationMenu from "./notificationMenu";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import WebRequestAlert from "../../../../app/frameworks/data/apitrackingcore/webRequestAlert";
import WebRequestAnnouncement from "../../../../app/frameworks/data/apicore/webRequestAnnouncement";
import WebRequestVehicleMaintenancePlan from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import { Enums, Constants } from "../../../../app/frameworks/constant/apiConstant";
import UIConstants from "../../../../app/frameworks/constant/uiConstant";
import Utility from "../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

/**
 * Handle top notification menu.
 * 
 * @class NotificationControl
 * @extends {ControlBase}
 */
class NotificationControl extends ControlBase {
    /**
     * Creates an instance of NotificationControl.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Control properties.
        this.isShowJourney = this.ensureObservable(params.isShowJourney, true);
        this.visible(WebConfig.userSession.currentPortal === "company-workspace");
        this.notifications = ko.observableArray();
        this.audioId = this.id + "_audio";
        this.audioId2 = this.id + "_audio2";
        this.audioId3 = this.id + "_audio3";
        this.soundUrl = this.resolveUrl("~/media/notification.mp3");
        this.soundUrl2 = this.resolveUrl("~/media/chafing.mp3");
        this.soundUrl3 = this.resolveUrl("~/media/sharp.mp3");

        this.navigationService = params.navigationService;
        this.viewportSize = 99; // maximum notificaiton.

        // Menu instances.
        this.urgentMenu = null;
        this.alertMenu = null;
        this.announcementMenu = null;
        this.maintenanceMenu = null;

        // Event aggregator subscribe.
        this._eventAggregator = new EventAggregator();
        this._eventAggregator.subscribe(EventAggregatorConstant.NEW_NOTIFICATION, this.onReceiveNotification.bind(this));

        // Service properties.
        this.webRequestAlert = WebRequestAlert.getInstance();
        this.webRequestAnnouncement = WebRequestAnnouncement.getInstance();
        this.webRequestVehicleMaintenancePlan = WebRequestVehicleMaintenancePlan.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!this.visible()) {
            return;
        }

        var dfdLoadAllNotifications = $.Deferred();

        // Urgent Alerts
        if(WebConfig.userSession.hasPermissionNotificationUrgentAlerts()) {
            this.urgentMenu = new NotificationMenu(
                this.i18n("Notifications_UrgentAlerts"),
                UIConstants.SignalRNotificationType.UrgentAlert,
                "svg-noti-urgent",
                WebConfig.notificationSession.urgentAlertItems,
                WebConfig.notificationSession.urgentAlertItemsUnreadCount);

                this.notifications.push(this.urgentMenu);
        }

        // Alerts
        if(WebConfig.userSession.hasPermissionNotificationAlerts()) {
            this.alertMenu = new NotificationMenu(
                this.i18n("Notifications_Alerts"),
                UIConstants.SignalRNotificationType.Alert,
                "svg-noti-alert",
                WebConfig.notificationSession.alertItems,
                WebConfig.notificationSession.alertItemsUnreadCount);

            this.notifications.push(this.alertMenu);
        }

        // Announcements
        this.announcementMenu = new NotificationMenu(
            this.i18n("Notifications_Announcements"),
            UIConstants.SignalRNotificationType.Announcement,
            "svg-noti-announcement",
            WebConfig.notificationSession.announcementItems,
            WebConfig.notificationSession.announcementItemsUnreadCount);

        this.notifications.push(this.announcementMenu);

        // Maintenances
        if(WebConfig.userSession.hasPermissionNotificationMaintenances()) {
            this.maintenanceMenu = new NotificationMenu(
                this.i18n("Notifications_Maintenances"),
                UIConstants.SignalRNotificationType.Maintenance,
                "svg-noti-maintenance",
                WebConfig.notificationSession.maintenanceItems,
                WebConfig.notificationSession.maintenanceItemsUnreadCount);
            
            this.notifications.push(this.maintenanceMenu);
        }

        // Loading all notification types.
        WebConfig.notificationSession.loadStaticNotification(
            this.webRequestAlert,
            this.webRequestAnnouncement,
            this.webRequestVehicleMaintenancePlan,
            WebConfig.userSession.currentCompanyId)
        .always(() => {
            dfdLoadAllNotifications.resolve();
        });

        dfdLoadAllNotifications.resolve();

        return dfdLoadAllNotifications;
    }

    /**
     * Handle when notification is coming.
     */
    onReceiveNotification() {
        // let self = this
        if (WebConfig.userSession.enableSoundAlert) {
            this.playSound();
        }
    }

    /**
     * Play sound for new incomming alerts. 
     */
    playSound() {
        var $audio = $("#" + this.audioId);
        var $audio2 = $("#" + this.audioId2);
        var $audio3 = $("#" + this.audioId3);

        let alertSound = null
        this.notifications().forEach((item)=>{
            if(item.type === 0){
                alertSound = item.items.sound
            }
        })
        if(alertSound){
            alertSound.forEach((name)=>{
                switch (name) {
                    case 1:
                        setTimeout(() => {
                            if ($audio.length) {
                            $audio.get(0).play();
                            }
                        }, 1000);
                        break;
                    case 2:
                        setTimeout(() => {
                            if ($audio2.length) {
                                $audio2.get(0).play();
                            }
                        }, 2000);
                            break;
                    case 3:
                        setTimeout(() => {
                            if ($audio3.length) {
                                $audio3.get(0).play();
                            }
                        }, 3000);       
                        break;
                    default:
                        break;
                }
            })

        }

      
    }

    /**
     * Execute when control is already rendered in DOM.
     *
     */
    onDomReady() {
        if (!this.visible()) return;

        _.forEach(this.notifications(), (noti) => {
            var $noti = $("#" + noti.id);

            FocusManager.getInstance().registerFocusElement(noti.id, $noti.get(0), () => {
                if (!noti.isActive()) return;
                noti.isActive(false);
            });
        });
    }

    /**
     * On click Dismiss / Dismiss All
     * @param {NotificationType} type
     * @param {long?} id
     */
    onDismissClick(notificationType, id = null) {
        WebConfig.notificationSession.dismissNotification(
            this.webRequestAlert,
            this.webRequestAnnouncement,
            this.webRequestVehicleMaintenancePlan,
            WebConfig.userSession.currentCompanyId,
            notificationType,
            id
        );
    }

    /**
     * Hook when notification item click and update unread status.
     * @param {NotificationType} notificationType
     * @param {any} notificationItem
     */
    onItemClick(notificationType, notificationItem) {
        // Inform server if current notificaiton is unread.
        var dfdUpdateReadStatus = Utility.emptyDeferred();
        if(notificationItem.isUnRead()) {
            dfdUpdateReadStatus = WebConfig.notificationSession.readNotification(
                this.webRequestAlert,
                this.webRequestAnnouncement,
                this.webRequestVehicleMaintenancePlan,
                WebConfig.userSession.currentCompanyId,
                notificationType,
                notificationItem.id
            ).always(() => {
                notificationItem.isUnRead(false);
            });
        }

        // Open blades all types are re-use same journey.
        $.when(dfdUpdateReadStatus).always(() => {
            var targetScreen = "";

            switch(notificationType) {
                case UIConstants.SignalRNotificationType.UrgentAlert:
                    this.urgentMenu.isActive(false);
                    targetScreen = "cw-notification-urgent-alert";
                    break;
                case UIConstants.SignalRNotificationType.Alert:
                    this.alertMenu.isActive(false);
                    targetScreen = "cw-notification-alert";
                    break;
                case UIConstants.SignalRNotificationType.Announcement:
                    this.announcementMenu.isActive(false);
                    targetScreen = "cw-notification-announcement";
                    break;
                case UIConstants.SignalRNotificationType.Maintenance:
                    this.maintenanceMenu.isActive(false);
                    targetScreen = "cw-notification-maintenance-plan";
                    break;
            }

            // Inform shell to open blade if user toggled with full map.
            this.isShowJourney(true);

            // Navigate to taraget screen.
            this.navigationService.navigate(targetScreen, {
                journeyName: "notification",
                id: notificationItem.id,
                data: notificationItem.data
            });
        });
    }
}

export default {
    viewModel: NotificationControl,
    template: templateMarkup
};