import ko from "knockout";
import i18nextko from "knockout-i18next";
import Conductor from "./conductor";
import Journey from "./journey";
import Blade from "./blade";
import Logger from "../../../../app/frameworks/core/logger";
import {Router, SwitchJourneyNavigationHistory, HideAllJourneyNavigationHistory} from "../../../../app/router";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import Singleton from "../../../../app/frameworks/core/singleton";
import MessageboxManager from "../messagebox/messageboxManager";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

/**
 * NavigationService handle screen navigation by encapsulate concept of Blade/Journey underneath.
 * 
 * @public
 * @class NavigationService
 * @extends {Conductor}
 */
class NavigationService extends Conductor {

    /**
     * Creates an instance of NavigationService.
     * @public
     * Create navigation engine for website.
     */
    constructor(portal) {
        super();

        // Private member.
        this._eventAggregator = new EventAggregator();
        this._router = Router.getInstance();

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.CORE_NAV_BACK, this.onHandleBackNavigation.bind(this));
        this._eventAggregator.subscribe(EventAggregatorConstant.CORE_NAV_NEXT, this.onHandleNextNavigation.bind(this));
    }

    /**
     * Handle back navigation event 
     * @private
     * @param {any} nav
     * @returns
     */
    onHandleBackNavigation(nav) {
        Logger.info("Detect BACK navigation = ", nav);
        var journey = null;

        // If it is special command, run then exit
        if(nav instanceof SwitchJourneyNavigationHistory) {
            // Logger.log("Should switch journey");
            journey = ko.utils.arrayFirst(this.items(), function(item) {
                return item.id === nav.journeyId;
            });
            if(journey) {
                this.activate(journey);
                this._router.navigateToJourney(journey);
                this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
            }
            return;
        }

        if(nav instanceof HideAllJourneyNavigationHistory) {
            journey = this.activeItem();
            this.deactivate(journey);
            this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, false);
            return;
        }

        if(nav.type && (nav.type === "start")) {
            // Logger.log("Should reset");
            journey = this.activeItem();
            if(journey) {
                this.deactivate(journey, true);
            }
        } else {
            var shouldSwitchJourney = false;
            var hasJourney = (this.activeItem() !== null);
            if(hasJourney) {
                shouldSwitchJourney = (this.activeItem().id !== nav.journeyId);
            }

            if(shouldSwitchJourney) {
                // Logger.log("Should switch journey");
                journey = ko.utils.arrayFirst(this.items(), function(item) {
                    return item.id === nav.journeyId;
                });
                if(journey) {
                    this.activate(journey);
                    this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
                }
            } else {
                // Logger.log("Should deactivate successor");
                // Close Blade to the right of target bladeId
                journey = this.activeItem();
                if(journey) {
                    var blade = journey.findItemById(nav.bladeId);
                    if(blade) {
                        // Logger.log("Found blade = " + blade.id);
                        journey.deactivate(blade, true, false);
                    } else {
                        // Logger.log("NOT found blade...");
                        // If not found, there is replace operations
                        // A -> B 
                        // A -> C 
                        // At this stage browser history is A -> B -> C 
                        // as there is no way to remove browser history as of now
                        // the best way to to simulate the replace by close last blade and append new one.
                        var lastBlade = journey.items()[journey.items().length-1];
                        journey.deactivate(lastBlade, true);
                        this._router.removeNavigationHistoryByBladeId(nav.bladeId);
                        this.navigate(nav.componentName, nav.options, journey);
                    }
                    this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
                } else {
                    // Logger.log("No active journey, should show the top one...");
                    journey = this.firstJourney();
                    if(journey) {
                        this.activate(journey);
                        this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
                    }
                }
            }
        }
    }
    
    /**
     * Handle next navigation event 
     * @private
     * @param {any} nav
     * @returns
     */
    onHandleNextNavigation(nav) {
        Logger.info("Detect NEXT navigation = ", nav);
        var journey = null;

        // If it is special command, run then exit
        if(nav instanceof SwitchJourneyNavigationHistory) {
            // Logger.log("Should switch journey");
            journey = ko.utils.arrayFirst(this.items(), function(item) {
                return item.id === nav.journeyId;
            });
            if(journey) {
                this.activate(journey);
                this._router.navigateToJourney(journey);
                this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
            }
            return;
        }

        if(nav instanceof HideAllJourneyNavigationHistory) {
            this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, false);
            this._router.navigateToHideAllJourney(nav.uri);
            return;
        }

        var shouldSwitchJourney = false;
        var hasJourney = (this.activeItem() !== null);
        if(hasJourney) {
            shouldSwitchJourney = (this.activeItem().id !== nav.journeyId);
        }

        if(shouldSwitchJourney) {
            // Logger.log("Should switch journey");
            journey = ko.utils.arrayFirst(this.items(), function(item) {
                return item.id === nav.journeyId;
            });
            if(journey) {
                this.activate(journey);
                this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
            }
        } else {
            // Logger.log("Should replay history");
            journey = this.activeItem();
            this.navigate(nav.componentName, nav.options, journey);
            this._eventAggregator.publish(EventAggregatorConstant.SHOW_JOURNEY, true);
        }
    }

    /**
     * Navigate to initial state for Routing
     * 
     * @public
     */
    navigateToInitialState(navOption = null) {
        this._router.navigateToInitialState(navOption);
    }

    /**
     * Navigate to specify module with optional parameters.
     * 
     * @public
     * @param {any} componentName
     * @param {any} [options=null]
     * @param {any} [targetJourney=null]
     * @param {any} [opener=null]
     * @param {boolean} [showTransition=false]
     */
    navigate(componentName, options = null, targetJourney = null, opener = null, showTransition = true) {
        var journey = targetJourney;
        var isNewJourney = false;

        // Finding exist journey if specify name.
        var sharedJourney = (options && options.journeyName);
        var sharedJourneyName = null;
        if(sharedJourney) {
            sharedJourneyName = options.journeyName;

            // Remove journey name option before passing to blade screen.
            delete options.journeyName;

            // If shared journey exist then switch journey then replace first one.
            var sharedJourneyInstance = null;

            this.items().forEach((j) => {
                if(j.name === sharedJourneyName) {
                    sharedJourneyInstance = j;
                }
            });

            // Auto close when found shared object for creating new one.
            if(sharedJourneyInstance) {
                this.tryDeactivate(sharedJourneyInstance, true);
            }
        }

        // If journey is not available then create new one.
        if (!journey) {
            // Create new journey.
            journey = new Journey();

            // Naming journey if available.
            if(sharedJourney) {
                journey.name = sharedJourneyName;
            }

            isNewJourney = true;
        }

        // Adding journal to shell.
        this.activate(journey);

        // Adding new blade to journey.
        var newBlade = new Blade(componentName, options);

        // Tell VisualFX that blade is opened, with ancestors information
        if(showTransition) {
            var previousBlades = journey.items();
            var previousBladeIds = [];
            if (previousBlades.length) {
                previousBladeIds = previousBlades.map(function(item) {
                    return item.id;
                });
            }
            newBlade.nextTransitionInfo = { ancestorIds: previousBladeIds };
        }

        // Activate journey item.
        journey.activate(newBlade, opener);

        // Tell Router to log this navigation
        try {
            this._router.navigate(componentName, {
                options: options,
                isNewJourney: isNewJourney,
                journeyId: journey.id,
                bladeId: newBlade.id
            });
        } catch(e) {
            Logger.error(e);
        }
    }
    
    /**
     * Check whether navigate to componentName is possible, given targetJourney
     * and opener.
     * If any successors of opener cannot close it will return false.
     * 
     * @public
     * @param {any} componentName
     * @param {any} targetJourney
     * @param {any} opener
     * @returns true if able to navigate to componentName, false otherwise.
     */
    canNavigate(componentName, targetJourney, opener) {
        var result = true;
        var successors = targetJourney.successors(opener);
        if(successors.length) {
            for(let i = 0; i < successors.length; i++) {
                result = result && successors[i].canClose();
                if(!result) break;
            }
        }
        return result;
    }

    /**
     * Check whether targetJourney has any pending changes.
     * 
     * @public
     * @param {any} targetJourney
     * @param {any} opener
     * @returns true if there is no pending change in targetJourney 
     * (including opener), false otherwise.
     */
    canClose(targetJourney, opener) {
        var result = true;
        var successors = targetJourney.successors(opener);
        if(successors.length) {
            for(let i = 0; i < successors.length; i++) {
                result = result && successors[i].canClose();
                if(!result) break;
            }
        }
        return result;
    }

    /**
     * Change active Journey.
     * 
     * @public 
     * @param {Journey} journey
     */
    changeJourney(journey) {
        this.activate(journey);

        // Tell Router to log this action
        this._router.navigateToJourney(journey);
    }
    
    /**
     * Hide all journey, also remove it from active menu.
     * 
     * @public 
     */
    hideAllJourney() {
        var activeJourney = this.activeItem();
        this.deactivate(activeJourney);

        // Tell Router to log this action
        this._router.navigateToHideAllJourney("company/map");
    }

    /**
     * Get first journey, null if non present.
     * 
     * @private
     * @returns
     */
    firstJourney() {
        if(!this.isEmpty()) {
            return this.items()[0];
        }
        return null;
    }

    /**
     * Try deactivate the given item. if remove flag is false then use normal deactivation process.
     * Else check if item can be removed and confirm with message M100.
     * 
     * @param {any} item
     * @param {boolean} [remove=false]
     * @returns
     */
    tryDeactivate(item, remove = false) {
        if(!remove) {
            this.deactivate(item, remove);
            return;
        }

        if(item.canRemove()) {
            this.deactivate(item, true);
        } else {
            MessageboxManager.getInstance().showMessageBox("", i18nextko.t("M100")(), BladeDialog.DIALOG_OKCANCEL).done((response)=> {
                if(BladeDialog.BUTTON_OK === response) {
                    this.deactivate(item, true);
                }
            });
        }
    }

    /**
     * Get current route config 
     * 
     * @public
     * @returns {object} entry in router.config.js
     */
    getCurrentRoute() {
        return this._router.getCurrentRoute();
    }

    /**
     * Minimize all blades in current journey.
     * @param {boolean} minimizeCurrent set if current blade should be collapsed.
     */
    minimizeAll(minimizeCurrent = true, currentBladeId) {
       var journey = this.activeItem();
        if(!_.isEmpty(journey)){
            journey.items().forEach((b) => {
                
                if(minimizeCurrent){
                    b.isMinimized(true);
                }
                else {
                    // check if current match.
                    if(b.id !== currentBladeId) {
                        b.isMinimized(true);
                    }
                }
            });
        }  
    }

    /**
     * Get navigation service singleton instance. 
     * @public
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("navService", new NavigationService());
    }
}

export default NavigationService;