import "jquery";
import "hammerjs";
import ko from "knockout";
import ObjectBase from "../../../../app/frameworks/core/objectBase";

/**
 * Sample class document - change me!
 * 
 * @class Blade
 * @extends {ObjectBase}
 */
class Widget extends ObjectBase {

    /**
     * Creates an instance of Widget.
     * 
     * @param {any} componentName
     * @param {any} [options=null]
     */
    constructor(componentName, options = null, widgetOptions = null) {
        super();
        this.componentName = componentName || "";
        this.componentOptions = options;
        this.widgetOptions = widgetOptions;
    }
}

export default Widget;
