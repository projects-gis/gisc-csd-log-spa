import ko from "knockout";
import ObjectBase from "../../../../app/frameworks/core/objectBase";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import * as CSS from "../../../../app/frameworks/constant/stylesheet";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class DropMenu extends ObjectBase {
    constructor(title) {
        super();
        // Internal configuration.
        this._activeCss = CSS.SELECTED;
        this._inactiveCss = CSS.DROPMENU_CLOSE;
        this._eventAggregator = new EventAggregator();

        // Public fields.
        this.title = title;
        this.isIgnoreOtherMenu = ko.observable(false);
        this.isActive = ko.observable(false);
        this.menuStatus = ko.pureComputed(this.computeMenuStatus, this);
        this.menuContentStatus = ko.pureComputed(this.computeContentStatus, this);

        // Register event subscribe.
        // this._eventAggregator.subscribe(EventAggregatorConstant.DROP_MENU_CLICK, this.onOtherMenuClick());

        this.onClicked = null; // Event used by caller
    }
    computeMenuStatus() {
        return this.isActive() ? this._activeCss : this._inactiveCss;
    }
    computeContentStatus() {
        return this.isActive() ? CSS.DROPMENU_OPEN : CSS.DROPMENU_CLOSE;
    }
    // onOtherMenuClick() {
    //     // var self = this;
    //     // var onOtherMenuClick = (activeMenuId) => {
    //     //     if (self.isIgnoreOtherMenu()) {
    //     //         return;
    //     //     }

    //     //     if (self.id !== activeMenuId) {
    //     //         // Closing current menu.
    //     //         self.isActive(false);
    //     //     }
    //     // };
    //     // return onOtherMenuClick;
    // }
    onClick() {
        // notify to other menu to close.
        // this._eventAggregator.publish(EventAggregatorConstant.DROP_MENU_CLICK, this.id);
        // Toggle state of visibility.
        this.isActive(!this.isActive());
        
        if(_.isFunction(this.onClicked)) {
            this.onClicked(this.isActive());
        }
    }
    setCss(cssName, inActiveCss) {
        this._activeCss = cssName;
        this._inactiveCss = inActiveCss;
    }
}

export {
    DropMenu
};