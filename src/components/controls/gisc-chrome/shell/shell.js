import "signalr";
import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./shell.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import i18nextko from "knockout-i18next";
import {Router} from "../../../../app/router";
import NavigationService from "./navigation";
import WidgetNavigationService from "./widgetNavigation";
import EventAggregator from "../../../../app/frameworks/core/eventAggregator";
import Logger from "../../../../app/frameworks/core/logger";
import Utility from "../../../../app/frameworks/core/utility";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums, EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";
import WebRequestUserApiCore from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webRequestCompany";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestPermission from "../../../../app/frameworks/data/apicore/WebRequestPermission";
import WebRequestStringResource from "../../../../app/frameworks/data/apicore/webRequestStringResource";
import WebRequestSystemConfiguration from "../../../../app/frameworks/data/apicore/WebRequestSystemConfiguration";
import WebRequestUserApiTrackingCore from "../../../../app/frameworks/data/apitrackingcore/webRequestUser";
import WebRequestFleetMonitoring from "../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import WebRequestUserSignalR from "../../../../app/frameworks/data/signalr/webRequestUser";
import WebRequestAlert from "../../../../app/frameworks/data/apitrackingcore/webRequestAlert";
import WebRequestAnnouncement from "../../../../app/frameworks/data/apicore/webRequestAnnouncement";
import WebRequestVehicleMaintenancePlan from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import MessageboxManager from "../messagebox/messageboxManager";
import AuthenticationManager from "../authenticationManager/authenticationManager";
import FocusManager from "../../../../app/frameworks/core/focusManager";
import UIConstants from "../../../../app/frameworks/constant/uiConstant";
import QueueProcessor from "../../../../app/frameworks/core/queueProcessor";
import WebRequestUserApiTrackingReport from "../../../../app/frameworks/data/apitrackingreport/webRequestUser";
import { CompanyIcons } from "../../../../app/frameworks/constant/svg";
import ToastMessageUtility from "../../../../app/frameworks/core/toastMessageUtility";

/**
 * Handle top navigation menu.
 * 
 * @class Crumbtail
 * @extends {ControlBase}
 */
class Shell extends ControlBase {
    /**
     * Creates an instance of Crumbtail.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        // Private member.
        this._eventAggregator = new EventAggregator();

        // Defind properties.
        this.isMapVisible = ko.observable(false);
        this.isLoading = ko.observable(true);

        // Theme Setting
        this.themeClass = ko.pureComputed(function(){
            return this.appTheme() + ' ' + this.companyLogoClass();
        },this);

        this.appTheme = ko.observable('');
        this.appLogoLayout = ko.observable('');

        this.companyLogoClass = ko.pureComputed(function(){
            if(WebConfig.userSession.currentPortal == "back-office")
                return UIConstants.LogoLayout.Default;
            else
                return this.appLogoLayout();
        }, this);

        this.companyClass = ko.pureComputed(function() {
            if(WebConfig.userSession.currentPortal == "back-office")
                return UIConstants.Themes.Default;
            else
                // for replace class theme in tag body
                $(document.body).removeClass();
                $(document.body).addClass(this.appTheme() ? this.appTheme() : UIConstants.Themes.Default);
                return this.appTheme();
        }, this);

        //Company Logo Setting
        this.companyLoadingIcon = ko.pureComputed(function(){
            let key = this.appTheme();

            if(WebConfig.userSession.currentPortal == "back-office")
                return CompanyIcons.LoadingLogo[UIConstants.Themes.Default];
            else if(!key) {
                return "";
            }
            return CompanyIcons.LoadingLogo[key];
        },this);

        //Company Loading Setting
        this.companyIcon = ko.pureComputed(function(){
            let key = this.appTheme();
            if(!key) {
                return "";
            }
            return CompanyIcons.AppLogo[key];
        },this);

        // Journey menu.
        this.isShowJourney = ko.observable(true);

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.SHOW_JOURNEY, this.onShowJourney.bind(this));
        this._eventAggregator.subscribe(EventAggregatorConstant.SHOW_LOADING, this.onShowLoading.bind(this));
        this._eventAggregator.subscribe(EventAggregatorConstant.REFRESH_ACCESSIBLE_COMPANY, this.onRefreshAccessibleCompany.bind(this));
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
        this._eventAggregator.subscribe(EventAggregatorConstant.ASSETIDS_FOR_FLEET_CHANGE, this.onAssetIdsChange.bind(this));
        this._eventAggregator.subscribe(EventAggregatorConstant.TOASTR_CLICK, this.onToastrClick.bind(this));

        // Inject router and Navigation Service
        this.navigationService = NavigationService.getInstance();
        this.widgetNavigationService = WidgetNavigationService.getInstance();
        this.toastr = ToastMessageUtility.getInstance();
    }

    /**
     * Handle error when ajax fail.
     * 
     * @param {any} error
     * 
     * @memberOf Shell
     */
    onAjaxError (error) {
        // We cannot use messagebox control from here because shell has error before i18n is initilized so we need to use original one.
        if(error){
            alert(error.statusText + " " + error.statusText + "\n" + error.responseText);
        }
        else {
            alert("unknown error");
        }
    }

    /**
     * Handle error when ajax fail and logout.
     * 
     * @param {any} error
     * 
     * @memberOf Shell
     */
    onAjaxErrorLogout (error) {
        // We cannot use messagebox control from here because shell has error before i18n is initilized so we need to use original one.
        if(error){
            alert(error.statusText + " " + error.statusText + "\n" + error.responseText);
        }
        else {
            alert("unknown error");
        }
        // Calling signout to api servers.
        AuthenticationManager.getInstance().signOut();
    }

    /**
     * Life cycle: Trigger from template after it was rendered.
     * 
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        
        //this.toastr.showToast(UIConstants.ToastrType.Info, "Hello");

        var shell = this;
        // Step 1 Perform Authenticaton Handshake for Web APIs.
        var webRequestUserApiCore = WebRequestUserApiCore.getInstance();
        var webRequestUserApiTrackingCore = WebRequestUserApiTrackingCore.getInstance();
        var webRequestUserSignalR = WebRequestUserSignalR.getInstance();
        var webRequestUserApiTrackingReport = WebRequestUserApiTrackingReport.getInstance();
        var dfdAuthentication = $.Deferred();
        $.when(
            webRequestUserApiCore.loginByToken(WebConfig.userSession.loginToken),
            webRequestUserApiTrackingCore.loginByToken(WebConfig.userSession.loginToken),
            webRequestUserSignalR.loginByToken(WebConfig.userSession.loginToken),
            // NOTE: As discussed with N'BOM we skip authentication because
            // issue of Telerik's report viewer doesn't send authentication cookie.
            // if it supports send cookie then enable below service & update authenticationManager.
            webRequestUserApiTrackingReport.loginByToken(WebConfig.userSession.loginToken)
        )
        .done((apicoreUser, trackingCoreUser) => {
            dfdAuthentication.resolve(apicoreUser);
        })
        .fail(shell.onAjaxErrorLogout);

        // Step 2 Initialize User preferrence & user language.
        var dfdPreference = $.Deferred();
        $.when(dfdAuthentication).done(userInfo => {
            var preferenceFilter = {
                keys: [
                    Enums.ModelData.UserPreferenceKey.EnableSoundAlert,
                    Enums.ModelData.UserPreferenceKey.FleetDelayTime,
                    Enums.ModelData.UserPreferenceKey.PreferredLanguage,
                    Enums.ModelData.UserPreferenceKey.AssetIdsForFleetMonitoring,
                    Enums.ModelData.UserPreferenceKey.DataGridState,
                    Enums.ModelData.UserPreferenceKey.EnableAlertNotification
                ]
            };
            webRequestUserApiCore.getUserPreferences(preferenceFilter)
            .done((prefs) => {
                // Store preferences to memory.
                prefs.forEach(function(p) {
                    switch(p.key){
                        case Enums.ModelData.UserPreferenceKey.EnableSoundAlert:
                            WebConfig.userSession.enableSoundAlert = JSON.parse(p.value.toLowerCase());
                            break;
                        case Enums.ModelData.UserPreferenceKey.FleetDelayTime:
                            WebConfig.userSession.fleetDelayTime = p.value;
                            break;
                        case Enums.ModelData.UserPreferenceKey.PreferredLanguage:
                            WebConfig.userSession.currentUserLanguage = p.value;
                            break;
                        case Enums.ModelData.UserPreferenceKey.AssetIdsForFleetMonitoring:
                            // this structure should be json string.
                            WebConfig.userSession.assetIdsForFleetMonitoring = JSON.parse(p.value);
                            break;
                        case Enums.ModelData.UserPreferenceKey.DataGridState:
                            WebConfig.userSession.currentDataGridState = JSON.parse(p.value);
                            break;
                        case Enums.ModelData.UserPreferenceKey.EnableAlertNotification:
                            WebConfig.userSession.enableAlertNotification = JSON.parse(p.value.toLowerCase());
                            break;
                    }
                }, this);

                dfdPreference.resolve();
            })
            .fail(shell.onAjaxError);
        });

        // Step 3 Loading Company Settings if not in company portal.
        var dfdCompanySettings = $.Deferred();
        $.when(dfdPreference).done(() => {
            var webRequestCompany = WebRequestCompany.getInstance();

            switch (WebConfig.userSession.currentPortal){
                case "back-office":
                    this.appTheme(UIConstants.Themes.Default);
                    // No company id available so skip loading company settings.
                    // Just loading accessible companies for performance just check >1 for multiple access only.
                    var companyFilter = {
                        displayStart: 0, // Always use first page.
                        displayLength: 1, // Used for auto change portal for only one company id.
                        includeCount: true // Used for detect total of accessible companies.
                    };
                    var dfdCompanyList = webRequestCompany.listCompanySummary(companyFilter)
                    .done(result => {
                        WebConfig.userSession.accessibleCompanyCount = result.totalRecords;
                        if (result.totalRecords > 0) {
                            WebConfig.userSession.accessibleFirstCompanyId = result.items[0].id;
                        }
                    })
                    .fail(shell.onAjaxError);

                    // Loading system's settings for date formats.
                    var dfdSystemSettings = WebRequestSystemConfiguration.getInstance().getRegionalSystemConfiguration()
                    .done(result => {
                        result.timezone = result.timezoneId;
                        result.timezoneName = result.timezoneDisplayName;

                        WebConfig.companySettings.load(result);
                    })
                    .fail(shell.onAjaxError);

                    // Loading permission for back office portal.
                    var dfdUserPermission = webRequestUserApiCore.getUserPermissions()
                    .done((permissions) => {
                        WebConfig.userSession.initPermission(permissions);
                    })
                    .fail(shell.onAjaxErrorLogout);

                    // Check if current user is sysadmin
                    var dfdUserGroup = webRequestUserApiCore.getUser(WebConfig.userSession.id, [
                        EntityAssociation.User.Groups
                    ])
                    .done((user) => {
                        WebConfig.userSession.authenticationType = user.authenticationType;
                        var userGroup = user.groups[0];
                        if(!_.isNil(userGroup)) {
                            var isUserSysAdmin = (userGroup.groupName === "System Admin");
                            WebConfig.userSession.isSysAdmin = (userGroup.groupName === "System Admin");
                        }
                    })
                    .fail(shell.onAjaxErrorLogout);

                    // Complete task when company and permission performed.s
                    $.when(dfdCompanyList, dfdSystemSettings, dfdUserPermission, dfdUserGroup).done(()=> {
                        dfdCompanySettings.resolve();
                    });
                    break;

                case "company-admin":
                case "company-workspace":
                    // Loading company settings for viewing.
                    var companyAssociations = [
                        EntityAssociation.Company.Logo,
                        EntityAssociation.Company.Setting
                    ];

                    var dfdCompanyListForCompanyPortal = webRequestCompany.getCompany(
                        WebConfig.userSession.currentCompanyId, companyAssociations)
                    .done(company => {
                        //mock
                        //user.companyThemeName = "app-theme-sccc";
                        //user.companyThemeName = "app-theme-kubota";

                        this.appTheme(company.setting.themeName);
                        this.appLogoLayout(company.setting.logoSize);

                        WebConfig.userSession.companyThemeName = company.setting.themeName;
                        WebConfig.userSession.distanceUnit = company.setting.distanceUnit;
                        WebConfig.userSession.distanceUnitSymbol = company.setting.distanceUnitSymbol;
                        WebConfig.userSession.areaUnit = company.setting.areaUnit;
                        WebConfig.userSession.areaUnitSymbol = company.setting.areaUnitSymbol;

                        // Loading company logo if available.
                        if(company.logo) {
                            // Adding reference to company logo.
                            WebConfig.userSession.currentCompanyLogoUrl = company.logo.fileUrl;
                        }
                        else {
                            // Get char 0 to display first letter of company name
                            WebConfig.userSession.currentCompanyNameFirstLetter = !_.isEmpty(company.name) ? company.name.charAt(0) : "";
                        }

                        // Loading company settings.
                        WebConfig.companySettings.load(company.setting);

                        // Load user's company name for compnay user because company user can access only one assigned company.
                        // Back office user doesn't need to show company name.
                        if (WebConfig.userSession.userType == Enums.UserType.Company) {
                            WebConfig.userSession.currentCompanyName = company.name;
                        }

                        WebConfig.userSession.currentCompanyNameOnIcon = company.name;
                    })
                    .fail(shell.onAjaxError);

                    // Loading user's portal modes.
                    var dfdUserPortalModes = webRequestUserApiCore.getUserPortalModes()
                    .done((portalModes) => {
                        WebConfig.userSession.portalModes = portalModes;
                    })
                    .fail(shell.onAjaxError);

                    // Loading company permissions for current company.
                    var dfdCompanyPermissionForCompanyPortal = webRequestUserApiCore.getUserPermissions()
                    .done((permissions) => {
                        WebConfig.userSession.initPermission(permissions);
                    })
                    .fail(shell.onAjaxErrorLogout);

                    // Loading user authenticationType.
                    //For Back Office user must not include header
                    var dfdUserAuthenticationType = webRequestUserApiCore.getUser(WebConfig.userSession.id, [], (WebConfig.userSession.userType == Enums.UserType.Company))
                    .done((user) => {
                        WebConfig.userSession.authenticationType = user.authenticationType;
                        WebConfig.userSession.permissionType = user.permissionType;
                    })
                    .fail(shell.onAjaxErrorLogout);

                    // Loading user accessible business units.
                    var dfdAccessibleUnit = WebRequestBusinessUnit.getInstance().listBusinessUnitSummary({companyId: WebConfig.userSession.currentCompanyId})
                    .done((result) => {
                        var accessibleBUIds = [];
                        result.items.forEach((bu) => {
                            accessibleBUIds.push(bu.id);
                        });
                        WebConfig.userSession.initAccessibleBusinessUnits(accessibleBUIds);
                    })
                    .fail(shell.onAjaxError);

                    // Complete loading company.
                    $.when(dfdCompanyListForCompanyPortal,
                        dfdCompanyPermissionForCompanyPortal,
                        dfdUserAuthenticationType,
                        dfdAccessibleUnit)
                    .done(() => {
                        dfdCompanySettings.resolve();
                    });
                    break;
            }
        });

        // Step 4 Loading String resource for UI label.
        var dfdStringResource = $.Deferred();
        $.when(dfdCompanySettings).done(() => {
            var webRequestStringResource = WebRequestStringResource.getInstance();
            var resourceFilter = [
                Enums.ModelData.ResourceType.UILabel, 
                Enums.ModelData.ResourceType.UIMessage
            ];
            switch (WebConfig.userSession.currentPortal) {
                case "back-office":
                    // Use in Asset Management > Box Feature
                    resourceFilter.push(Enums.ModelData.ResourceType.EventDataType);
                    resourceFilter.push(Enums.ModelData.ResourceType.EventType);
                    break;
                case "company-admin":
                    // Use in User Management
                    resourceFilter.push(Enums.ModelData.ResourceType.AuthenticationType);
                    break;
                case "company-workspace":
                    // Use in global search.
                    resourceFilter.push(Enums.ModelData.ResourceType.GlobalSearchType);
                    break;
            }
            webRequestStringResource.list(resourceFilter, null)
            .done((languages) => {
                // Store i18n to user session for the map usage in the future.
                WebConfig.userSession.i18n = languages;

                // Initialize for i18n.
                var effectiveLanugage = null;
                switch (WebConfig.userSession.currentPortal) {
                    case "back-office":
                        effectiveLanugage = WebConfig.companySettings.languageCode;
                        break;
                    case "company-admin":
                    case "company-workspace":
                        effectiveLanugage = WebConfig.userSession.currentUserLanguage;
                        break;
                }

                // Update html lang code for dynamic font translation.
                $("html").attr("lang", effectiveLanugage);

                // Initialize i18nextko.
                i18nextko.init(languages, effectiveLanugage, ko);

                // Override default message in ko-validation library
                ko.validation.rules["required"].message = i18nextko.t("M001")();
                ko.validation.rules["arrayRequired"].message = i18nextko.t("M001")();
                ko.validation.rules["email"].message = i18nextko.t("M064")();
                ko.validation.rules["ipAddressMaximum"].message = i18nextko.t("M086")();

                // File upload validation messages.
                ko.validation.rules["fileRequired"].message = i18nextko.t("M001")();
                ko.validation.rules["fileExtension"].message = i18nextko.t("M014")();
                ko.validation.rules["fileImageDimension"].message = i18nextko.t("M027")();
                ko.validation.rules["fileSize"].message = i18nextko.t("M016")();
                ko.validation.rules["maxArrayLength"].message = i18nextko.t("M134")();
                ko.validation.rules["maxArrayPlaybackAnalysisLength"].message = i18nextko.t("M252")();

                dfdStringResource.resolve();
            })
            .fail(shell.onAjaxError);
        });

        // Step 5 Fleet Monitoring [Run Background]
        var dfdFleetMonitoring = $.Deferred();
        var cannotConnectTrackingDB = false;
        $.when(dfdStringResource).done(() => {
            if (WebConfig.userSession.currentPortal === "company-workspace") {
                //set companyId for fleetMonitoring internal use because we cannot get WebConfig.userSession in fleetMonitoring class.
                WebConfig.fleetMonitoring.companyId = WebConfig.userSession.currentCompanyId;

                if (WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring)) {
                    // Load from server for first 20 records follow requirement.
                    var filter = {
                            displayStart: 0,
                            displayLength: 20
                    };

                    // If preference available then load from last recently used ids.
                    // var state = {
                    //     viewVehicleIds: this.trackLocationViewVehicleIds(),
                    //     viewDeliveryManIds: this.trackLocationViewDeliveryManIds(),
                    //     followVehicleIds: this.trackLocationFollowVehicleIds(),
                    //     followDeliveryManIds: this.trackLocationFollowDeliveryManIds()
                    // };
                    var usingRestoredState = (WebConfig.userSession.assetIdsForFleetMonitoring !== null);
                    if(usingRestoredState){
                        filter = {
                            vehicleIds: WebConfig.userSession.assetIdsForFleetMonitoring.viewVehicleIds,
                            deliveryManIds: WebConfig.userSession.assetIdsForFleetMonitoring.viewDeliveryManIds
                        };

                        // Calculate display length from total asset ids (cannot be zero).
                        filter.displayLength = filter.vehicleIds.length + filter.deliveryManIds.length;
                    }

                    // Call web servics for track locations.
                    WebConfig.fleetMonitoring.syncTrackLocation(
                        WebRequestFleetMonitoring.getInstance(),
                        WebConfig.userSession,
                        filter)
                    .done((result) => {
                        //set viewIds for first time
                        var viewVehicleIds = [];
                        var viewDeliveryManIds = [];
                        _.forEach(result.items, (item) => {
                            if (item.vehicleId) {
                                viewVehicleIds.push(item.vehicleId);
                            }
                            else if (item.deliveryManId) {
                                viewDeliveryManIds.push(item.deliveryManId);
                            }
                        });

                        // Set view assets from search result.
                        WebConfig.fleetMonitoring.trackLocationViewVehicleIds(viewVehicleIds);
                        WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds(viewDeliveryManIds);

                        // Set follow assets from state.
                        if(usingRestoredState){
                            WebConfig.fleetMonitoring.trackLocationFollowVehicleIds(
                                WebConfig.userSession.assetIdsForFleetMonitoring.followVehicleIds
                            );
                            WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds(
                                WebConfig.userSession.assetIdsForFleetMonitoring.followDeliveryManIds
                            );
                        }

                        //set reference date time for first time. Use to calculate isOverFleetDelayTime
                        WebConfig.fleetMonitoring.fleetDelayTimeDatetimeReference(result.timestamp);
                        //calculate for first time
                        WebConfig.fleetMonitoring.compareFleetDelayTime(WebConfig.userSession.fleetDelayTime);
                        //set interval for calculate every 'x' seconds
                        var intervals = 60;
                        var timer = setInterval(() => {
                            // + datetime reference with interval
                            var newDatetimeRef = Utility.addTime(WebConfig.fleetMonitoring.fleetDelayTimeDatetimeReference(), intervals);
                            WebConfig.fleetMonitoring.fleetDelayTimeDatetimeReference(newDatetimeRef);
                            WebConfig.fleetMonitoring.compareFleetDelayTime(WebConfig.userSession.fleetDelayTime);
                        }, intervals * 1000);

                        dfdFleetMonitoring.resolve();
                    }).fail((error) => {
                        if (error && error.responseJSON && error.responseJSON.code === "M135") {
                            cannotConnectTrackingDB = true;
                            dfdFleetMonitoring.resolve();
                        }
                        else {
                            shell.onAjaxError.call(this, error);
                        }
                    });
                }
                else {
                    dfdFleetMonitoring.resolve();
                }
            }
            else {
                dfdFleetMonitoring.resolve();
            }
        });


        // Step 6 Shell initialization.
        $.when(dfdStringResource).done(() => {
            // Initialize timer for authentication checking.
            AuthenticationManager.getInstance().startAuthenticationSync();

            // Show map control (Auto detect visiblity inside).
            shell.isMapVisible(true);

            // Hide splash screen loading.
            shell.isLoading(false);

            // Verify if current user has remainingExpirationDays > 0.
            if(WebConfig.userSession.remainingExpirationDays !== null) {
                var m023Str = this.i18n("M023", [WebConfig.userSession.remainingExpirationDays]);
                MessageboxManager.getInstance().showMessageBox("", m023Str, BladeDialog.DIALOG_OK);
            }

            // Display message M135 if cannot connect tracking database.
            if (cannotConnectTrackingDB) {
                MessageboxManager.getInstance().showMessageBox("", this.i18n("M135"), BladeDialog.DIALOG_OK);
            }
        });
    }
    /**
     * DOM manipulation function when page is ready to access.
     * 
     * @memberOf Shell
     */
    onDomReady() {
        // Disable right click.
        $(document).on("contextmenu", (e) => {
            if (!WebConfig.appSettings.isDebug) {
                return false;
            }
        });
        
        //Detect click event to manage Menu bar lost focus.
        if(Utility.isIOSDevice()){
            $(document.body).on("click touchstart", function(e) {
                FocusManager.getInstance().handleActiveElementChanged(e.target);
            });
        }
        else{
            $(document.body).on("click", function(e) {
                FocusManager.getInstance().handleActiveElementChanged(document.activeElement);
            });
        }

        //For Detect device to prevent touch hover state.
        if(Utility.isTouchDeviceSupported()) {
            $(document.body).addClass("touch");
        }

        // Inject self to window -- this is required for diagnostic router in debug mode.
        if (WebConfig.appSettings.isDebug) {
            window._shell = this;
            window._router = Router.getInstance();
        }
    }

    /**
     * Handle when EventAggregatorConstant.SHOW_JOURNEY is received.
     * @private
     * @param {boolean} isShow whether Journey should be visible or hidden.
     */
    onShowJourney(isShow) {
        this.isShowJourney(isShow);
    }

    /**
     * Show loading screen.
     * 
     * @param {any} isShow
     */
    onShowLoading(isLoading) {
        this.isLoading(isLoading);
    }

    /**
     * Handle when EventAggregatorConstant.REFRESH_ACCESSIBLE_COMPANY is received.
     */
    onRefreshAccessibleCompany() {
        var webRequestCompany = WebRequestCompany.getInstance();

        var companyFilter = {
            displayStart: 0, // Always use first page.
            displayLength: 1, // Used for auto change portal for only one company id.
            includeCount: true // Used for detect total of accessible companies.
        };

        webRequestCompany.listCompanySummary(companyFilter)
        .done(result => {
            WebConfig.userSession.accessibleCompanyCount = result.totalRecords;
            if (result.totalRecords > 0) {
                WebConfig.userSession.accessibleFirstCompanyId = result.items[0].id;
            }

            // publish message to refresh user menu
            this._eventAggregator.publish(EventAggregatorConstant.REFRESH_USER_MENU);
        })
        .fail(this.onAjaxError);
    }

    /**
     * Handle map command completed.
     * 
     * @param {any} data
     */
    onMapComplete(data) {
        switch(data.command) {
            case "set-startup-resources":
                this.initializeSignalR();
                this.initializeTrackMonitoring();
                break;
            case "mark-poi":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-geo-fencing-custom-poi-manage", {mode: "create", markpoi: data.result.geometry});
                break;
            case "route-from-here":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-geo-fencing-routes-from-here", { state: 'start', lat: data.result.geometry.lat, lon: data.result.geometry.lon, extent: data.result.geometry.extent });
                //this.navigationService.navigate("cw-geo-fencing-custom-route-manage", {mode: "create", routefromhere: data.result.geometry});
                break;
            case "route-to-here":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-geo-fencing-routes-from-here", { state: 'dest', lat: data.result.geometry.lat, lon: data.result.geometry.lon, extent: data.result.geometry.extent });
                break;
            case "find-nearest-asset":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-map-functions-find-nearest-asset", { lat: data.result.geometry.lat, lon:data.result.geometry.lon});
                break;
            case "send-to-navigator":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-map-functions-send-to-navigator", { lat: data.result.geometry.lat, lon:data.result.geometry.lon });
                break;
            case "view-playback":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-fleet-monitoring-playback", { id: data.result.attributes.ID }); 
                break;
            case "view-detail-poi":
                this.isShowJourney(true);
                this.navigationService.navigate("cw-geo-fencing-custom-poi-view", { customPoiId: data.result.attributes.id }); 
                break;
            case "mapclick-close-mis-menu":
                this.navigationService.hideAllJourney();
                this.isShowJourney(false);
                //MapManager.getInstance().setFollowTracking(true);

                //var menuCloseException = [
                //    //Global-Search
                //    "cw-global-search",
                //    "cw-global-search-find",
                //    "cw-global-search-result",
                //    "cw-geo-fencing-custom-area-view",
                //    "cw-geo-fencing-custom-poi-view",
                //    "cw-geo-fencing-custom-route-view",
                //    "ca-business-unit-view",
                //    "ca-asset-vehicle-view",
                //    "ca-asset-driver-view",
                //    "ca-user-view",

                //    //View Alert
                //    "cw-notification-alert",
                //    "cw-notification-urgent-alert",
                //    "cw-notification-announcement",
                //    "cw-notification-maintenance-plan",

                //    //Playback
                //    "",
                //];


                //if (menuCloseException.indexOf(this.navigationService().getCurrentRoute().params.page) == -1) {
                //    this.navigationService.hideAllJourney();
                //    this.isShowJourney(false);
                //}

                break;
        }
    }

    /**
     * Handle when change filter.
     * 
     * @param {any} state
     */
    onAssetIdsChange(state) {
        var model =
        [
            {
                key: Enums.ModelData.UserPreferenceKey.AssetIdsForFleetMonitoring,
                value: JSON.stringify(state),
                infoStatus: Enums.InfoStatus.Update
            }
        ];
        WebRequestUserApiCore.getInstance().updateUserPreferences(model)
        .done(()=>{
            Logger.info("Complete update profile.", state);
        })
        .fail(() => {
            Logger.error("Fail update profile.")
        });
    }

    onToastrClick(data) {
        this.isShowJourney(true);
        this.navigationService.navigate("cw-notification-alert", data);
    }

    /**
     * Initialize signalr connection hubs.
     */
    initializeSignalR() {
        var hubUrl = Utility.resolveUrl(WebConfig.appSettings.notificationUrl, "~/notification");
        // Create proxy object.
        var connection = $.hubConnection(hubUrl, { useDefaultPath: false });

        // Enable loging configuration.
        if(WebConfig.appSettings.isDebug) {
            connection.logging = true;
        }

        var logisticsHub = connection.createHubProxy("logisticsHub");

        // Handle signalr on receive signal of gps tracking.
        var trackingQueue = new QueueProcessor('trackingQueue', (messages) => {
            var dfdSyncData = $.Deferred();
            var vehicleIds = [];
            var deliveryManIds = [];
            // Process multiple messages.
            messages.forEach((message) => {
                switch(message.type) {
                    case UIConstants.SignalRTrackingType.Fleet:
                        vehicleIds = _.union(vehicleIds, message.ids);
                        break;
                    case UIConstants.SignalRTrackingType.MobileService:
                        deliveryManIds = _.union(deliveryManIds, message.ids);
                        break;
                }
            });

            Logger.log("Tracking with", vehicleIds, deliveryManIds);

            // Update tracking location datasource.
            WebConfig.fleetMonitoring.signalTrackLocation(
                WebRequestFleetMonitoring.getInstance(),
                WebConfig.userSession,
                vehicleIds,
                deliveryManIds
            )
            .done(() => {
            })
            .fail(() => {
                Logger.error("Cannot get update tracking location from server.");
            })
            .always(() => {
                dfdSyncData.resolve();
            });

            return dfdSyncData;
        });

        // Handle signalr on receive signal of notification.
        var notificationQueue = new QueueProcessor('notificationQueue', (messages) => {
            
            var dfdSyncData = $.Deferred();
            var alertIds = [];
            var maintenanceIds = [];
            var announcementIds = [];
            
            // Categorize alert by signalr types.
            messages.forEach((message) => {
                switch(message.type) {
                    case UIConstants.SignalRNotificationType.Alert:
                        alertIds = _.union(alertIds, message.ids);
                        break;
                    case UIConstants.SignalRNotificationType.Announcement:
                        announcementIds = _.union(announcementIds, message.ids);
                        break;
                    case UIConstants.SignalRNotificationType.Maintenance:
                        maintenanceIds = _.union(maintenanceIds, message.ids);
                        break;
                }
            });

            // Invoke alert notification detail if necessary.
            var dfdAlertNotification = Utility.emptyDeferred();
            if(alertIds.length) {
                Logger.log("Load alert notification for", alertIds);
                dfdAlertNotification = $.Deferred();

                var dfdAlertMessageUpdater = WebConfig.notificationSession.signalNotification(
                    WebRequestAlert.getInstance(),
                    WebRequestAnnouncement.getInstance(),
                    WebRequestVehicleMaintenancePlan.getInstance(),
                    WebConfig.userSession.currentCompanyId,
                    alertIds,
                    UIConstants.SignalRNotificationType.Alert
                );
                
                // Sync alert dashboard when get alert notification.
                var dfdAlertDashboardUpdater = WebConfig.fleetMonitoring.syncAlertDashboard(WebRequestAlert.getInstance());

                // Unlock alert notification tasks when two operations completed.
                $.when(dfdAlertMessageUpdater, dfdAlertDashboardUpdater)
                .done((resAlertMessageUpdater, resAlertDashboardUpdater) => {
                    if(WebConfig.userSession.enableAlertNotification){
                        resAlertMessageUpdater.items.map((item, index)=> {
                        let formatText = Utility.stringFormat("{0} {1} ({2})", item.vehicleBrand, item.vehicleModel, item.vehicleLicense);
                        formatText += `<br/>${item.businessUnitPath}`;
                        formatText += `<br/>${item.message}`;
                        this.toastr.showToast(UIConstants.ToastrType.Error, formatText, item.alertIcon, item);
                        });
                    }

                    Logger.log("Update alert notifications.");
                })
                .fail(() => {
                    Logger.error("Cannot get alert information dashboard from server.");
                })
                .always(() => {
                    dfdAlertNotification.resolve();
                });
            }

            // Invoke announcement notification detail if necessary.
            var dfdAnnouncementNotification = Utility.emptyDeferred();
            if(announcementIds.length) {
                Logger.log("Load announcement notification for", maintenanceIds);
                dfdAnnouncementNotification = WebConfig.notificationSession.signalNotification(
                    WebRequestAlert.getInstance(),
                    WebRequestAnnouncement.getInstance(),
                    WebRequestVehicleMaintenancePlan.getInstance(),
                    WebConfig.userSession.currentCompanyId,
                    announcementIds,
                    UIConstants.SignalRNotificationType.Announcement
                );
            }

            // Invoke maintenance notification if necessary.
            var dfdMaintenanceNotification = Utility.emptyDeferred();
            if(maintenanceIds.length) {
                Logger.log("Load maintenance notification for", announcementIds);
                dfdMaintenanceNotification = WebConfig.notificationSession.signalNotification(
                    WebRequestAlert.getInstance(),
                    WebRequestAnnouncement.getInstance(),
                    WebRequestVehicleMaintenancePlan.getInstance(),
                    WebConfig.userSession.currentCompanyId,
                    maintenanceIds,
                    UIConstants.SignalRNotificationType.Maintenance
                );
            }

            // When all notification has done resolve deferred for next process queue.
            $.when(dfdAlertNotification, dfdAnnouncementNotification, dfdMaintenanceNotification)
            .fail(() => {
                Logger.error("Cannot get update notification from server.");
            })
            .always(() => {
                this._eventAggregator.publish(EventAggregatorConstant.NEW_NOTIFICATION);
                dfdSyncData.resolve();
            });

            return dfdSyncData;
        });

        // Setup SignalR notification methods to forward message to queues.
        logisticsHub.on("sendTracking", (message) => {
            if (WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring)) {
                trackingQueue.push(message);
            }
        });
        logisticsHub.on("sendNotification", (message) => {
            if (WebConfig.userSession.hasPermissionNotificationUrgentAlerts() || 
                WebConfig.userSession.hasPermissionNotificationAlerts() || 
                WebConfig.userSession.hasPermissionNotificationMaintenances()) {
                notificationQueue.push(message);
            }
        });

        // Make connection to signalR server.
        connection.start()
        .done(function() {
            Logger.info("Now connected, connection ID=" + connection.id);
            // Join group by current business units.
            logisticsHub.invoke("joinGroup", WebConfig.userSession.accessibleBusinessUnits)
            .done((result) => {
                Logger.info("Can join notification group.");
                trackingQueue.start();
                notificationQueue.start();
            })
            .fail((error) => {
                Logger.error("Cannot join notification group.");
            });
        })
        .fail(function() { 
            Logger.error("Could not connect to notification hub.");
        });
    }

    /**
     * Initialize Track Monitoring
     */
    initializeTrackMonitoring() {
        if (WebConfig.userSession.hasPermission(Constants.Permission.TrackMonitoring)) {
            WebConfig.fleetMonitoring.trackVehiclesOnMap();
            WebConfig.fleetMonitoring.mapStartupComplete(true);
        }
    }
}

export default {
    viewModel: Shell,
    template: templateMarkup
};
