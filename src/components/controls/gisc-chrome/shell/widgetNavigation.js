import ko from "knockout";
import i18nextko from "knockout-i18next";
import Conductor from "./conductor";
import Logger from "../../../../app/frameworks/core/logger";
import Singleton from "../../../../app/frameworks/core/singleton";
import Widget from "./widget";

/**
 * NavigationService handle screen navigation by encapsulate concept of Blade/Journey underneath.
 * 
 * @public
 * @class NavigationService
 * @extends {Conductor}
 */
class WidgetNavigationService extends Conductor {

    /**
     * Creates an instance of NavigationService.
     * @public
     * Create navigation engine for website.
     */
    constructor() {
        super();
        this.allowMultipleActive = true;
        this.widgetInstance = null;
    }

    /**
     * Get navigation service singleton instance. 
     * @public
     * @static
     * @returns
     */
    static getInstance() {
        return Singleton.getInstance("widgetNavigationService", new WidgetNavigationService());
    }


    /**
     * Navigate to specify module with optional parameters.
     * 
     * @public
     * @param {any} componentName
     * @param {any} [options=null]
     */
    widget(componentName, options = null, widgetOptions = null) {
        // Verify widget target exists.
        if(_.isString(widgetOptions.target) && widgetOptions.target != "_blank") {
            var targetExists = false;
            // Verify existing items has same target.
            this.items().forEach((w) => {
                if(w.widgetOptions.target === widgetOptions.target){
                    targetExists = true;
                }
            });

            // Break widget creation if widget target exists.
            if(targetExists){
                return;
            }
        }

        // create object widget.
        var newWidget = new Widget(componentName, options, widgetOptions);
        this.widgetInstance = newWidget;
               
        // activate new widget
        this.activate(newWidget);
    }

    closewidget(){
       if (this.widgetInstance.widgetInstance){
           if(this.widgetInstance.widgetInstance.widget){
            this.deactivate(this.widgetInstance, true);
        this.widgetInstance.widgetInstance.widget.close();    
           }       
       }              
    }

    deactivateWiget(widget){
        this.deactivate(widget, true);
    }
}

export default WidgetNavigationService;