import ko from "knockout";
import ControlBase from "../../controlbase";
import templateMarkup from "text!./companylogo.html";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";

/**
 * Handle company logo.
 * 
 * @class CompanyLogoControl
 * @extends {ControlBase}
 */
class CompanyLogoControl extends ControlBase {
    /**
     * Creates an instance of Company Logo.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.visible(WebConfig.userSession.currentPortal !== "back-office");
        this.companyLogoUrl = WebConfig.userSession.currentCompanyLogoUrl;
        this.companyNameFirstLetter = WebConfig.userSession.currentCompanyNameFirstLetter;
        this.companyName = WebConfig.userSession.currentCompanyNameOnIcon;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
}

export default {
    viewModel: CompanyLogoControl,
    template: templateMarkup
};