// THIS file will be used in application startup file and gulp config.
(function () {

    var componentConfig = {
        components: [
            {
                group: "gisc-ui-control",
                items: [
                    { name: "gisc-chrome-shell", path: "components/controls/gisc-chrome/shell/shell" },
                    { name: "gisc-chrome-crumbtail", path: "components/controls/gisc-chrome/crumbtail/crumbtail" },
                    { name: "gisc-chrome-map", path: "components/controls/gisc-chrome/map/map" },
                    { name: "gisc-chrome-working-map", path: "components/controls/gisc-chrome/map/workingMap" },
                    { name: "gisc-chrome-searchbox", path: "components/controls/gisc-chrome/searchbox/searchbox" },
                    { name: "gisc-chrome-companyswitcher", path: "components/controls/gisc-chrome/companyswitcher/companyswitcher" },
                    { name: "gisc-chrome-track-widget-switcher", path: "components/controls/gisc-chrome/trackwidgetswitcher/trackwidgetswitcher" },
                    { name: "gisc-chrome-sidebar", path: "components/controls/gisc-chrome/sidebar/sidebar" },
                    { name: "gisc-chrome-companylogo", path: "components/controls/gisc-chrome/companylogo/companylogo" },
                    { name: "gisc-chrome-usermenu", path: "components/controls/gisc-chrome/usermenu/usermenu" },
                    { name: "gisc-chrome-journeymenu", path: "components/controls/gisc-chrome/journeymenu/journeymenu" },
                    { name: "gisc-chrome-notification", path: "components/controls/gisc-chrome/notification/notification" },
                    { name: "gisc-chrome-messagebox", path: "components/controls/gisc-chrome/messagebox/messagebox" },
                    { name: "gisc-chrome-contextmenu", path: "components/controls/gisc-chrome/contextmenu/contextmenu" },
                    { name: "gisc-chrome-test-drive-widget-switcher", path: "components/controls/gisc-chrome/testdrivewidgetswitcher/testdrivewidgetswitcher" },
                    { name: "gisc-chrome-liveview-widget-switcher", path: "components/controls/gisc-chrome/liveviewwidgetswitcher/liveviewwidgetswitcher" },
                    { name: "gisc-ui-component", path: "components/controls/gisc-ui/component/component" },
                    { name: "gisc-ui-label", path: "components/controls/gisc-ui/label/label" },
                    { name: "gisc-ui-textbox", path: "components/controls/gisc-ui/textBox/textBox" },
                    { name: "gisc-ui-textarea", path: "components/controls/gisc-ui/textArea/textArea" },
                    { name: "gisc-ui-checkbox", path: "components/controls/gisc-ui/checkBox/checkBox" },
                    { name: "gisc-ui-dropdown", path: "components/controls/gisc-ui/dropDown/dropdown" },
                    { name: "gisc-ui-combobox", path: "components/controls/gisc-ui/combobox/combobox" },
                    { name: "gisc-ui-button", path: "components/controls/gisc-ui/button/button" },
                    { name: "gisc-ui-tab", path: "components/controls/gisc-ui/tab/tab" },
                    { name: "gisc-ui-tooltip", path: "components/controls/gisc-ui/tooltip/tooltip" },
                    { name: "gisc-ui-statusbar", path: "components/controls/gisc-ui/statusBar/statusBar" },
                    { name: "gisc-ui-datatable", path: "components/controls/gisc-ui/datatable/datatable" },
                    { name: "gisc-ui-fileupload", path: "components/controls/gisc-ui/fileupload/fileupload" },
                    { name: "gisc-ui-listview", path: "components/controls/gisc-ui/listView/listView" },
                    { name: "gisc-ui-dialogbox", path: "components/controls/gisc-ui/dialogBox/dialogBox" },
                    { name: "gisc-ui-datetime", path: "components/controls/gisc-ui/dateTimePicker/dateTimePicker" },
                    { name: "gisc-ui-chart", path: "components/controls/gisc-ui/chart/chart" },
                    { name: "gisc-ui-markdown", path: "components/controls/gisc-ui/markdown/markdown" },
                    { name: "gisc-ui-field", path: "components/controls/gisc-ui/field/field" },
                    { name: "gisc-ui-colorpicker", path: "components/controls/gisc-ui/colorPicker/colorPicker" },
                    { name: "gisc-ui-image-combobox", path: "components/controls/gisc-ui/imageCombobox/imageCombobox" },
                    { name: "gisc-ui-field", path: "components/controls/gisc-ui/field/field" },
                    { name: "gisc-ui-treeview", path: "components/controls/gisc-ui/treeView/treeView" },
                    { name: "gisc-ui-dropdowntree", path: "components/controls/gisc-ui/dropdownTree/dropdownTree" },
                    { name: "gisc-ui-chartbar", path: "components/controls/gisc-ui/chartBar/chartBar" },
                    //{ name: "gisc-ui-reportviewer", path: "components/controls/gisc-ui/reportViewer/reportViewer" },

                    { name: "gisc-ui-piechart", path: "components/controls/gisc-ui/pieChart/pieChart" },
                    { name: "gisc-ui-barchart", path: "components/controls/gisc-ui/barChart/barChart" },
                    { name: "gisc-ui-calendar", path: "components/controls/gisc-ui/calendar/calendar" },
                    { name: "gisc-ui-reportviewer", path: "components/controls/gisc-ui/reportViewer/reportViewer" },
                    { name: "gisc-ui-stackchart", path: "components/controls/gisc-ui/stackChart/stackChart" },
                    { name: "gisc-ui-datagrid", path: "components/controls/gisc-ui/datagrid/datagrid" },
                    { name: "gisc-ui-editor", path: "components/controls/gisc-ui/editor/editor" },
                    { name: "gisc-ui-radio", path: "components/controls/gisc-ui/radio/radio" },
                    { name: "gisc-ui-window", path: "components/controls/gisc-ui/window/window" },
                    { name: "gisc-ui-dropdown-multiselect", path: "components/controls/gisc-ui/dropdownMultiSelect/dropdownMultiSelect" },
                    { name: "gisc-ui-autocomplete", path: "components/controls/gisc-ui/autocomplete/autocomplete" },
                    { name: "gisc-ui-quick-search", path: "components/controls/gisc-ui/quickSearch/quickSearch" },
                    
                    { name: "gisc-ui-timelinechart", path: "components/controls/gisc-ui/timelinechart/timelinechart" },
                    { name: "gisc-ui-donutchart", path: "components/controls/gisc-ui/donutchart/donutchart" },
                    { name: "gisc-ui-baramchart", path: "components/controls/gisc-ui/baramchart/baramchart" },
                    { name: "gisc-ui-barstackamchart", path: "components/controls/gisc-ui/barstackamchart/barstackamchart" },
                    { name: "gisc-ui-linechart", path: "components/controls/gisc-ui/linechart/linechart" },
                    { name: "gisc-ui-radarchart", path: "components/controls/gisc-ui/radarChart/radarChart" },
                    { name: "gisc-ui-barhorizontalamchart", path: "components/controls/gisc-ui/barhorizontalamchart/barhorizontalamchart" },

                    { name: "gisc-ui-routedragdrop", path: "components/controls/gisc-ui/routedragdrop/routedragdrop" },

                    { name: "gisc-ui-shipment-grid", path: "components/controls/gisc-ui/shipmentGrid/shipmentGrid" },
                    { name: "gisc-ui-shipment-timeline", path: "components/controls/gisc-ui/shipment-timeline/shipment-timeline" },
                    { name: "gisc-ui-gallery", path: "components/controls/gisc-ui/gallery/gallery" },
                    // { name: "gisc-ui-shipment-recurring-grid", path: "components/controls/gisc-ui/shipmentRecurringGrid/shipmentRecurringGrid" },
                    { name: "gisc-ui-video", path: "components/controls/gisc-ui/video/video" },
                    { name: "gisc-ui-flatpickr", path: "components/controls/gisc-ui/flatpickr/flatpickr" },
                    // { name: "gisc-ui-playback-analysis-tool", path: "components/controls/gisc-ui/playbackAnalysisTool/playbackAnalysisTool" },
                    
                    { name: "gisc-ui-shipment-grid-confirm-dialog", path: "components/controls/gisc-ui/shipmentGrid/confirm-dialog" },
                    { name: "gisc-ui-playback-analysis-tool", path: "components/controls/gisc-ui/playbackAnalysisTool/playbackAnalysisTool" },
                    { name: "gisc-ui-timelinechart-v4", path: "components/controls/gisc-ui/timelineChartV4/timelineChart-V4" },
                    
                ]
            },
            {
                group: "svg",
                items: [
                    { name: "svg-nav-block", path: "text!components/svgs/nav-block.html" },
                    { name: "svg-nav-bar", path: "text!components/svgs/nav-bar.html" },
                    { name: "svg-splash-logo", path: "text!components/svgs/splash-logo.html" },

                    { name: "svg-cmd-add", path: "text!components/svgs/cmd-add.html" },
                    { name: "svg-cmd-edit", path: "text!components/svgs/cmd-edit.html" },
                    { name: "svg-cmd-refresh", path: "text!components/svgs/cmd-refresh.html" },
                    { name: "svg-cmd-search", path: "text!components/svgs/cmd-search.html" },
                    { name: "svg-cmd-delete", path: "text!components/svgs/cmd-delete.html" },
                    { name: "svg-cmd-reset-password", path: "text!components/svgs/cmd-reset-password.html" },
                    { name: "svg-cmd-import", path: "text!components/svgs/cmd-import.html" },
                    { name: "svg-cmd-export", path: "text!components/svgs/cmd-export.html" },
                    { name: "svg-cmd-download-template", path: "text!components/svgs/cmd-download-template.html" },
                    { name: "svg-cmd-languages", path: "text!components/svgs/cmd-languages.html" },
                    { name: "svg-cmd-assign", path: "text!components/svgs/cmd-assign.html" },
                    { name: "svg-cmd-return", path: "text!components/svgs/cmd-return.html" },
                    { name: "svg-cmd-clear", path: "text!components/svgs/cmd-clear-search.html" },
                    { name: "svg-cmd-contract", path: "text!components/svgs/cmd-contract.html" },
                    { name: "svg-cmd-maintenance", path: "text!components/svgs/cmd-maintenance.html" },
                    { name: "svg-cmd-maintenance-cancel", path: "text!components/svgs/cmd-maintenance-cancel.html" },
                    { name: "svg-cmd-maintenance-complete", path: "text!components/svgs/cmd-maintenance-complete.html" },
                    { name: "svg-cmd-odometer", path: "text!components/svgs/cmd-odometer.html" },
                    { name: "svg-cmd-import-picture", path: "text!components/svgs/cmd-import-picture.html" },
                    { name: "svg-cmd-view-all", path: "text!components/svgs/cmd-view-all.html" },
                    { name: "svg-cmd-clear-all", path: "text!components/svgs/cmd-clear-all.html" },
                    { name: "svg-cmd-follow-all", path: "text!components/svgs/cmd-follow-all.html" },
                    { name: "svg-cmd-clear-follow", path: "text!components/svgs/cmd-clear-follow.html" },

                    { name: "svg-action-close", path: "text!components/svgs/action-close.html" },
                    { name: "svg-action-maximize", path: "text!components/svgs/action-maximize.html" },
                    { name: "svg-action-minimize", path: "text!components/svgs/action-minimize.html" },
                    { name: "svg-action-pin", path: "text!components/svgs/action-pin.html" },
                    { name: "svg-action-restore", path: "text!components/svgs/action-restore.html" },

                    { name: "svg-journey", path: "text!components/svgs/journey.html" },
                    { name: "svg-bell", path: "text!components/svgs/bell.html" },
                    { name: "svg-warning", path: "text!components/svgs/warning.html" },
                    { name: "svg-error", path: "text!components/svgs/error.html" },
                    { name: "svg-crumb-divider", path: "text!components/svgs/crumb-divider.html" },
                    { name: "svg-info", path: "text!components/svgs/info.html" },
                    { name: "svg-noti-alert", path: "text!components/svgs/noti-alert.html" },
                    { name: "svg-noti-announcement", path: "text!components/svgs/noti-announcement.html" },
                    { name: "svg-noti-maintenance", path: "text!components/svgs/noti-maintenance.html" },
                    { name: "svg-noti-urgent", path: "text!components/svgs/noti-urgent.html" },
                    { name: "svg-ic-setting", path: "text!components/svgs/ic-settings.html" },
                    { name: "svg-ic-gis", path: "text!components/svgs/logo-gis-small.html" },
                    { name: "svg-blade-close", path: "text!components/svgs/blade-close.html" },
                    { name: "svg-blade-collapse", path: "text!components/svgs/blade-collapse.html" },
                    { name: "svg-blade-maximize", path: "text!components/svgs/blade-maximize.html" },
                    { name: "svg-blade-restoredown", path: "text!components/svgs/blade-restoredown.html" },
                    { name: "svg-noti-delete", path: "text!components/svgs/noti-delete.html" },
                    { name: "svg-noti-view", path: "text!components/svgs/noti-view.html" },
                    { name: "svg-noti-view-disable", path: "text!components/svgs/noti-view-disable.html" },
                    { name: "svg-ic-asterisk", path: "text!components/svgs/ic-asterisk.html" },
                    { name: "svg-ic-info", path: "text!components/svgs/ic-info.html" },
                    { name: "svg-ic-statusbar", path: "text!components/svgs/ic-info-status.html" },
                    { name: "svg-ic-statusbar-fail", path: "text!components/svgs/ic-info-status-fail.html" },
                    { name: "svg-ic-checked", path: "text!components/svgs/ic-checked.html" },
                    { name: "svg-ic-calendar", path: "text!components/svgs/ic-calendar.html" },
                    { name: "svg-ic-right-arrow", path: "text!components/svgs/ic-right-arrow.html" },
                    { name: "svg-menu-hamburger", path: "text!components/svgs/menu-hamburger.html" },
                    { name: "svg-company-switcher", path: "text!components/svgs/company-switcher.html" },
                    { name: "svg-track-widget-switcher", path: "text!components/svgs/track-widget-switcher.html" },
                    { name: "svg-control-checkbox-checked", path: "text!components/svgs/checkbox-checked.html" },
                    { name: "svg-test-drive-widget-switcher", path: "text!components/svgs/test-drive-widget-switcher.html" },

                    { name: "svg-menu-driver-performance", path: "text!components/svgs/menu-driver-performance.html" },
                    { name: "svg-menu-geofencing", path: "text!components/svgs/menu-geofencing.html" },
                    { name: "svg-menu-map", path: "text!components/svgs/menu-map.html" },
                    { name: "svg-menu-monitoring", path: "text!components/svgs/menu-monitoring.html" },
                    { name: "svg-menu-search", path: "text!components/svgs/menu-search.html" },
                    { name: "svg-menu-shipment", path: "text!components/svgs/menu-shipment.html" },
                    { name: "svg-menu-dashboard", path: "text!components/svgs/menu_dashboard.html" },
                    { name: "svg-menu-home", path: "text!components/svgs/menu-home.html" },
                    { name: "svg-menu-subscriptions", path: "text!components/svgs/menu-subscriptions.html" },
                    { name: "svg-menu-companies", path: "text!components/svgs/menu-companies.html" },
                    { name: "svg-menu-users", path: "text!components/svgs/menu-users.html" },
                    { name: "svg-menu-assets", path: "text!components/svgs/menu-assets.html" },
                    { name: "svg-menu-announcements", path: "text!components/svgs/menu-announcements.html" },
                    { name: "svg-menu-configurations", path: "text!components/svgs/menu-configurations.html" },
                    { name: "svg-menu-reports", path: "text!components/svgs/menu-reports.html" },
                    { name: "svg-menu-groups", path: "text!components/svgs/menu-groups.html" },
                    { name: "svg-menu-businessunits", path: "text!components/svgs/menu-businessunits.html" },
                    { name: "svg-menu-reports-telematics", path: "text!components/svgs/menu-reports-telematics.html" },
                    { name: "svg-menu-boms", path: "text!components/svgs/menu-boms.html" },

                    { name: "svg-icon-custompoi-pinclickmap", path: "text!images/icon_custompoi_pinclickmap.svg" },
                    { name: "svg-icon-customroute-clickonmap", path: "text!images/icon_customroute_clickonmap.svg" },
                    { name: "svg-icon-customroute-subarea-clickonmap", path: "text!images/ic_subpoi_drawn_normal.svg" },
                    { name: "svg-icon-customroute-subarea-clear", path: "text!images/ic_subpoi_delete_normal.svg" },
                    { name: "svg-icon-customroute-createcustomarea", path: "text!images/icon_customroute_createcustomarea.svg" },
                    { name: "svg-icon-customroute-createcustomroute", path: "text!images/icon_customroute_createcustomroute.svg" },
                    { name: "svg-icon-customroute-movedown", path: "text!images/icon_customroute_movedown.svg" },
                    { name: "svg-icon-customroute-moveup", path: "text!images/icon_customroute_moveup.svg" },
                    { name: "svg-icon-customroute-print", path: "text!images/icon_customroute_print.svg" },
                    { name: "svg-icon-customroute-reverse", path: "text!images/icon_customroute_reverse.svg" },
                    { name: "svg-icon-customroute-selectpoi", path: "text!images/icon_customroute_selectpoi.svg" },
                    { name: "svg-icon-customroute-viewonmap", path: "text!images/icon_customroute_viewonmap.svg" },
                    // { name: "svg-icon-maximize-viewmap", path: "text!components/svgs/ic-maximize.svg" },
                    { name: "svg-icon-maximize-viewmap", path: "text!components/svgs/view_onmap.svg" },
                    { name: "svg-icon-report", path: "text!components/svgs/icon-report.html" },
                    { name: "svg-icon-auto-mail-report", path: "text!components/svgs/ic_automail_report.svg" },
                    { name: "svg-icon-report-telematics", path: "text!components/svgs/icon-report-telematics.html" },
                    { name: "svg-icon-context-menu", path: "text!components/svgs/icon-context-menu.html" },
                    { name: "svg-cat-logis-bussinessunit", path: "text!components/svgs/cat-logis-bussinessunit.html" },
                    { name: "svg-cat-logis-customarea", path: "text!components/svgs/cat-logis-customarea.html" },
                    { name: "svg-cat-logis-custompoi", path: "text!components/svgs/cat-logis-custompoi.html" },
                    { name: "svg-cat-logis-driver", path: "text!components/svgs/cat-logis-driver.html" },
                    { name: "svg-cat-logis-route", path: "text!components/svgs/cat-logis-route.html" },
                    { name: "svg-cat-logis-user", path: "text!components/svgs/cat-logis-user.html" },
                    { name: "svg-cat-logis-vehicle", path: "text!components/svgs/cat-logis-vehicle.html" },
                    { name: "svg-cat-etc", path: "text!components/svgs/cat-etc.html" },
                    { name: "svg-cat-map-accommodation", path: "text!components/svgs/cat-map-accommodation.html" },
                    { name: "svg-cat-map-adminpoly", path: "text!components/svgs/cat-map-adminpoly.html" },
                    { name: "svg-cat-map-association", path: "text!components/svgs/cat-map-association.html" },
                    { name: "svg-cat-map-atm", path: "text!components/svgs/cat-map-atm.html" },
                    { name: "svg-cat-map-attraction", path: "text!components/svgs/cat-map-attraction.html" },
                    { name: "svg-cat-map-autoservice", path: "text!components/svgs/cat-map-autoservice.html" },
                    { name: "svg-cat-map-bakerycoffee", path: "text!components/svgs/cat-map-bakerycoffee.html" },
                    { name: "svg-cat-map-bank", path: "text!components/svgs/cat-map-bank.html" },
                    { name: "svg-cat-map-bussiness", path: "text!components/svgs/cat-map-bussiness.html" },
                    { name: "svg-cat-map-carcare", path: "text!components/svgs/cat-map-carcare.html" },
                    { name: "svg-cat-map-carpark", path: "text!components/svgs/cat-map-carpark.html" },
                    { name: "svg-cat-map-education", path: "text!components/svgs/cat-map-education.html" },
                    { name: "svg-cat-map-fastfood", path: "text!components/svgs/cat-map-fastfood.html" },
                    { name: "svg-cat-map-food", path: "text!components/svgs/cat-map-food.html" },
                    { name: "svg-cat-map-foodshop", path: "text!components/svgs/cat-map-foodshop.html" },
                    { name: "svg-cat-map-goverment", path: "text!components/svgs/cat-map-goverment.html" },
                    { name: "svg-cat-map-health", path: "text!components/svgs/cat-map-health.html" },
                    { name: "svg-cat-map-hospital", path: "text!components/svgs/cat-map-hospital.html" },
                    { name: "svg-cat-map-hostel", path: "text!components/svgs/cat-map-hostel.html" },
                    { name: "svg-cat-map-movie", path: "text!components/svgs/cat-map-movie.html" },
                    { name: "svg-cat-map-police", path: "text!components/svgs/cat-map-police.html" },
                    { name: "svg-cat-map-publounge", path: "text!components/svgs/cat-map-publounge.html" },
                    { name: "svg-cat-map-road", path: "text!components/svgs/cat-map-road.html" },
                    { name: "svg-cat-map-transport", path: "text!components/svgs/cat-map-transport.html" },
                    { name: "svg-cat-map-conveniencestore", path: "text!components/svgs/cat-map-conveniencestore.html" },
                    { name: "svg-cat-map-departmentstore", path: "text!components/svgs/cat-map-departmentstore.html" },
                    { name: "svg-cat-map-lpg", path: "text!components/svgs/cat-map-lpg.html" },
                    { name: "svg-cat-map-market", path: "text!components/svgs/cat-map-market.html" },
                    { name: "svg-cat-map-masstransit", path: "text!components/svgs/cat-map-masstransit.html" },
                    { name: "svg-cat-map-ngv", path: "text!components/svgs/cat-map-ngv.html" },
                    { name: "svg-cat-map-oilstation", path: "text!components/svgs/cat-map-oilstation.html" },
                    { name: "svg-cat-map-other", path: "text!components/svgs/cat-map-other.html" },
                    { name: "svg-cat-map-pharmacy", path: "text!components/svgs/cat-map-pharmacy.html" },
                    { name: "svg-cat-map-postoffice", path: "text!components/svgs/cat-map-postoffice.html" },
                    { name: "svg-cat-map-recreations", path: "text!components/svgs/cat-map-recreations.html" },
                    { name: "svg-cat-map-religion", path: "text!components/svgs/cat-map-religion.html" },
                    { name: "svg-cat-map-restaurant", path: "text!components/svgs/cat-map-restaurant.html" },
                    { name: "svg-cat-map-stores", path: "text!components/svgs/cat-map-stores.html" },
                    { name: "svg-cat-map-vehiclecharging", path: "text!components/svgs/cat-map-vehiclecharging.html" },

                    { name: "svg-ic-mt-appo-manage", path: "text!components/svgs/ic_mt_appo_manage.svg" },
                    { name: "svg-ic-mt-maintenancesplans", path: "text!components/svgs/ic_mt_maintenancesplans.svg" },
                    { name: "svg-ic-mt-maintenancestypes", path: "text!components/svgs/ic_mt_maintenancestypes.svg" },
                    { name: "svg-ic-mt-report", path: "text!components/svgs/ic_mt_report.svg" },
                    { name: "svg-ic-table-appointment", path: "text!components/svgs/ic_table_appointment.svg" },
                    { name: "svg-ic-table-notappointment", path: "text!components/svgs/ic_table_notappointment.svg" },
                    { name: "svg-ic-va-postpone", path: "text!components/svgs/ic_va_postpone.svg" },
                    { name: "svg-ic-vmp-appointments", path: "text!components/svgs/ic_vmp_appointments.svg" },
                    { name: "svg-ic-vmt-mainte-subtype", path: "text!components/svgs/ic_vmt_mainte_subtype.svg" },
                    { name: "svg-ic-vmt-obsolate", path: "text!components/svgs/ic_vmt_obsolate.svg" },

                    { name: "svg-adminmode-normal", path: "text!components/svgs/btn_adminmode_normal.html" },
                    { name: "svg-adminmode-press", path: "text!components/svgs/btn_adminmode_press.html" },
                    { name: "svg-workspacemode-normal", path: "text!components/svgs/btn_workspacemode_normal.html" },
                    { name: "svg-workspacemode-over", path: "text!components/svgs/btn_workspacemode_over.html" },
                    { name: "svg-download-xls", path: "text!components/svgs/ic_download_xls.html" },
                    { name: "svg-ticket-create", path: "text!components/svgs/ic_ticket_create.html" },
                    { name: "svg-ticket-create-appointment", path: "text!components/svgs/ic_ticket_create_appointment.html" },
                    { name: "svg-ticket-editticket", path: "text!components/svgs/ic_ticket_editticket.html" },
                    { name: "svg-ticket-sentmail", path: "text!components/svgs/ic_ticket_sentmail.html" },
                    { name: "menu-ticket", path: "text!components/svgs/menu_ticket.html" },
                    { name: "svg-icon-vendor", path: "text!components/svgs/ic_leftbar_vendors_normal.html" },
                    { name: "svg-icon-transportation", path: 'text!components/svgs/ic_transportation.svg' },
                    { name: "svg-ticket-edit-ticket", path: "text!components/svgs/ic_ticket_edit_normal.svg" },

                    { name: "svg-icon-route-mode-car", path: 'text!components/svgs/btn_route_car_normal.svg' },
                    { name: "svg-icon-route-mode-motocycle", path: 'text!components/svgs/btn_route_motocyc_normal.svg' },
                    { name: "svg-icon-route-mode-truck", path: 'text!components/svgs/btn_route_truck_normal.svg' },
                    { name: "svg-icon-route-mode-walk", path: 'text!components/svgs/btn_route_walk_normal.svg' },

                    { name: "svg-icon-pin-do", path: "text!components/svgs/icon_delivery_order_pin.svg" },

                    { name: "svg-icon-utili-driver", path: "text!components/svgs/icon_utilization_driver.html" },
                    { name: "svg-icon-utili-vehicle", path: "text!components/svgs/icon_utilization_vehicle.html" },
                    { name: "svg-icon-vdo", path: "text!components/svgs/ic_vdo.html" },
                    { name: "svg-icon-start-plugin", path: "text!components/svgs/ic_start_plugin.html" },
                    { name: "svg-icon-load-plugin", path: "text!components/svgs/ic_load_plugin.html" },
                    { name: "svg-icon-load-guide", path: "text!components/svgs/ic_load_guide.html" },
                    
                    { name: "svg-icon-btn-back", path: "text!components/svgs/btn_back.html"},
                    { name: "svg-icon-btn-next", path: "text!components/svgs/btn_next.html"},
                    { name: "svg-icon-btn-zoom-in", path: "text!components/svgs/btn_zoom.html"},
                    { name: "svg-icon-btn-zoom-out", path: "text!components/svgs/btn_zoomout.html"},
                    { name: "svg-icon-vehicle", path: "text!components/svgs/ic_vehicle.html"},
                    { name: "svg-icon-update-contract", path: "text!components/svgs/ic_updatecontract.html" },
                    { name: "svg-icon-vehicle-over35", path: "text!components/svgs/ic_vehicle_over3.5.html" },
                    { name: "svg-icon-vehicle-over4", path: "text!components/svgs/ic_vehicle_over4.html" },

                    { name: "svg-icon-best-sequence", path: "text!components/svgs/ic_bestshipment.html" },

                    { name: "svg-icon-list-video", path: "text!components/svgs/ic_listvdo_normal.html" },
                    { name: "svg-icon-live-view", path: "text!components/svgs/btn_live_normal.html" },
                    { name: "svg-icon-notification-sound", path: "text!components/svgs/btn_playsound_press.svg" }
                

                ]
            },
            {
                group: "developer-portal-screens",
                items: [
                   { name: "developerHome", path: "components/portals/developer/home/home" },
                   { name: "labelPage", path: "components/portals/developer/label/labelPage" },
                   { name: "textBoxPage", path: "components/portals/developer/textBox/textBoxPage" },
                   { name: "textAreaPage", path: "components/portals/developer/textArea/textAreaPage" },
                   { name: "dropdownPage", path: "components/portals/developer/dropdown/dropdownPage" },
                   { name: "buttonPage", path: "components/portals/developer/button/buttonPage" },
                   { name: "tabPage", path: "components/portals/developer/tab/tabPage" },
                   { name: "datatableIndex", path: "components/portals/developer/datatable/datatableIndex" },
                   { name: "datatableReadme", path: "components/portals/developer/datatable/readme" },
                   { name: "datatableSimple", path: "components/portals/developer/datatable/simple" },
                   { name: "datatableAdvance", path: "components/portals/developer/datatable/advance" },
                   { name: "datatableEndPoint", path: "components/portals/developer/datatable/endpoint" },
                   { name: "datatableOthers", path: "components/portals/developer/datatable/otherColumnTypes" },
                   { name: "datatableStyle", path: "components/portals/developer/datatable/dataTableStyle" },
                   { name: "datatableRowSelector", path: "components/portals/developer/datatable/rowSelector" },
                   { name: "datatableAPI", path: "components/portals/developer/datatable/api" },
                   { name: "datatableEXP", path: "components/portals/developer/datatable/experimental" },
                   { name: "datatableLoadMore", path: "components/portals/developer/datatable/loadMore" },
                   { name: "datatableServerside", path: "components/portals/developer/datatable/serverside" },
                   { name: "tooltipPage", path: "components/portals/developer/tooltip/tooltipPage" },
                   { name: "fileUploadPage", path: "components/portals/developer/fileUpload/fileUploadPage" },
                   { name: "checkBoxPage", path: "components/portals/developer/checkBox/checkBoxPage" },
                   { name: "dateTimepickerPage", path: "components/portals/developer/dateTimepicker/dateTimepickerPage" },
                   { name: "chartPage", path: "components/portals/developer/chart/chartPage" },
                   { name: "chartEndPoint", path: "components/portals/developer/chart/chartDetail" },
                   { name: "comboboxPage", path: "components/portals/developer/combobox/comboboxPage" },
                   { name: "chartBarPage", path: "components/portals/developer/chartBar/chartBarPage" },

                   { name: "mapIndex", path: "components/portals/developer/map/mapIndex" },
                   { name: "mapDrawPin", path: "components/portals/developer/map/mapDrawPin" },
                   { name: "mapClear", path: "components/portals/developer/map/mapClear" },

                   { name: "mapDrawOnePointZoom", path: "components/portals/developer/map/mapDrawOnePointZoom" },
                   { name: "mapDrawOneLineZoom", path: "components/portals/developer/map/mapDrawOneLineZoom" },
                   { name: "mapDrawOnePolygonZoom", path: "components/portals/developer/map/mapDrawOnePolygonZoom" },
                   { name: "mapDrawMultiplePoint", path: "components/portals/developer/map/mapDrawMultiplePoint" },
                   { name: "mapDrawMultiplePolygonZoom", path: "components/portals/developer/map/mapDrawMultiplePolygonZoom" },
                   { name: "mapDrawMultpleLineZoom", path: "components/portals/developer/map/mapDrawMultpleLineZoom" },
                   { name: "mapDrawMultipleVehicle", path: "components/portals/developer/map/mapDrawMultipleVehicle" },
                   { name: "mapDrawOneRoute", path: "components/portals/developer/map/mapDrawOneRoute" },
                   { name: "mapDrawPointBuffer", path: "components/portals/developer/map/mapDrawPointBuffer" },
                   { name: "mapDrawRouteBuffer", path: "components/portals/developer/map/mapDrawRouteBuffer" },
                   { name: "mapDrawCustomArea", path: "components/portals/developer/map/mapDrawCustomArea" },
                   { name: "mapZoomPoint", path: "components/portals/developer/map/mapZoomPoint" },
                   { name: "mapTrackVehicles", path: "components/portals/developer/map/mapTrackVehicles" },
                   { name: "mapEnableClickMap", path: "components/portals/developer/map/mapEnableClickMap" },
                   { name: "mapEnableDrawPolygon", path: "components/portals/developer/map/mapEnableDrawPolygon" },
                   { name: "mapPlaybackAnimate", path: "components/portals/developer/map/mapPlaybackAnimate" },
                   { name: "mapPlaybackFootprint", path: "components/portals/developer/map/mapPlaybackFootprint" },
                   { name: "mapPlaybackFully", path: "components/portals/developer/map/mapPlaybackFully" },

                   { name: "screenDevIndex", path: "components/portals/developer/screenDev/screenDevIndex" },
                   { name: "screenDevReadme", path: "components/portals/developer/screenDev/readme" },
                   { name: "screenDevTrackChange", path: "components/portals/developer/screenDev/trackChange" },
                   { name: "screenDevValidation", path: "components/portals/developer/screenDev/validation" },
                   { name: "screenDevValidationDemo", path: "components/portals/developer/screenDev/validationDemo" },
                   { name: "screenDevI18N", path: "components/portals/developer/screenDev/i18n" },
                   { name: "screenDevGulp", path: "components/portals/developer/screenDev/gulpTask" },
                   { name: "screenDevFAQ", path: "components/portals/developer/screenDev/faq" },
                   { name: "colorPickerPage", path: "components/portals/developer/colorPicker/colorPickerPage" },
                   { name: "imageComboboxPage", path: "components/portals/developer/imageCombobox/imageComboboxPage" },
                   { name: "treeViewPage", path: "components/portals/developer/treeView/treeViewPage" },
                   { name: "dropdownTreePage", path: "components/portals/developer/dropdownTree/dropdownTreePage" },
                   { name: "messageboxHelpPage", path: "components/portals/developer/messagebox/messageboxHelp" },
                   { name: "reportViewerDemo", path: "components/portals/developer/reportViewer/reportViewer" },

                   { name: "serviceTestingIndex", path: "components/portals/developer/services/main" },
                   { name: "serviceTestingShipment", path: "components/portals/developer/services/shipment" }
                ]
            },
            {
                group: "back-office-portal-screens",
                items: [
                    // Select Company 
                    { name: "bo-company-select", path: "components/portals/back-office/company-select" },
                    // Shared
                    { name: "bo-shared-accessible-company-select", path: "components/portals/back-office/shared/accessible-company-select" },
                    { name: "bo-shared-accessible-users-select", path: "components/portals/back-office/shared/accessible-users-select" },
                    { name: "bo-shared-accessible-inverse-features-select", path: "components/portals/back-office/shared/accessible-inverse-features-select" },
                    // Home
                    { name: "bo-home", path: "components/portals/back-office/home" },
                    // Subscription and Group
                    { name: "bo-subscription-and-group", path: "components/portals/back-office/subscription-and-group/menu" },
                    { name: "bo-subscription-and-group-subscription", path: "components/portals/back-office/subscription-and-group/subscription/list" },
                    { name: "bo-subscription-and-group-subscription-manage", path: "components/portals/back-office/subscription-and-group/subscription/manage" },
                    { name: "bo-subscription-and-group-group", path: "components/portals/back-office/subscription-and-group/group/list" },
                    { name: "bo-subscription-and-group-group-manage", path: "components/portals/back-office/subscription-and-group/group/manage" },
                    { name: "bo-subscription-and-group-technician-team", path: "components/portals/back-office/subscription-and-group/technician-team/list" },
                    { name: "bo-subscription-and-group-technician-team-manage", path: "components/portals/back-office/subscription-and-group/technician-team/manage" },
                    // Company
                    { name: "bo-company", path: "components/portals/back-office/company/list" },
                    { name: "bo-company-manage", path: "components/portals/back-office/company/manage" },
                    { name: "bo-company-manage-subscription", path: "components/portals/back-office/company/subscription" },
                    { name: "bo-company-manage-subscription-manage", path: "components/portals/back-office/company/subscription-manage" },
                    { name: "bo-company-manage-contact-manage", path: "components/portals/back-office/company/contact-manage" },
                    { name: "bo-company-manage-regional-manage", path: "components/portals/back-office/company/regional-manage" },
                    { name: "bo-company-manage-setting-manage", path: "components/portals/back-office/company/setting-manage" },
                    { name: "bo-company-manage-mapservices-manage", path: "components/portals/back-office/company/mapservices-manage" },
                    { name: "bo-company-manage-pairable-vehicle", path: "components/portals/back-office/company/pairable-vehicle" },
                    // User
                    { name: "bo-user", path: "components/portals/back-office/user/list" },
                    { name: "bo-user-manage", path: "components/portals/back-office/user/manage" },
                    // Asset
                    { name: "bo-asset", path: "components/portals/back-office/asset/menu" },
                    // Asset - Box Feature
                    { name: "bo-asset-box-feature", path: "components/portals/back-office/asset/box-feature/list" },
                    { name: "bo-asset-box-feature-manage", path: "components/portals/back-office/asset/box-feature/manage" },
                    // Asset - Box Model
                    { name: "bo-asset-box-model", path: "components/portals/back-office/asset/box-model/list" },
                    { name: "bo-asset-box-model-manage", path: "components/portals/back-office/asset/box-model/manage" },
                    // Asset - Box Template
                    { name: "bo-asset-box-template", path: "components/portals/back-office/asset/box-template/list" },
                    { name: "bo-asset-box-template-manage", path: "components/portals/back-office/asset/box-template/manage" },
                    // Asset - Box Stock
                    { name: "bo-asset-box-stock", path: "components/portals/back-office/asset/box-stock/list" },
                    { name: "bo-asset-box-stock-manage", path: "components/portals/back-office/asset/box-stock/manage" },
                    // Asset - Box
                    { name: "bo-asset-box", path: "components/portals/back-office/asset/box/list" },
                    { name: "bo-asset-box-search", path: "components/portals/back-office/asset/box/search" },
                    { name: "bo-asset-box-manage", path: "components/portals/back-office/asset/box/manage" },
                    { name: "bo-asset-box-manage-contract", path: "components/portals/back-office/asset/box/contract" },
                    { name: "bo-asset-box-manage-maintenance", path: "components/portals/back-office/asset/box/maintenance" },
                    { name: "bo-asset-box-manage-maintenance-manage", path: "components/portals/back-office/asset/box/maintenance-manage" },
                    { name: "bo-asset-box-import", path: "components/portals/back-office/asset/box/import" },
                    { name: "bo-asset-box-assign", path: "components/portals/back-office/asset/box/assign" },
                    { name: "bo-asset-box-return", path: "components/portals/back-office/asset/box/return" },
                    // Asset - Mdvr Model
                    { name: "bo-asset-mdvr-models", path: "components/portals/back-office/asset/mdvrs/mdvrmodels/list" },
                    { name: "bo-asset-mdvr-models-manage", path: "components/portals/back-office/asset/mdvrs/mdvrmodels/manage" },
                 
                    // Asset - Mdvrs
                    { name: "bo-asset-mdvr-menu", path: "components/portals/back-office/asset/mdvrs/menu" },
                    { name: "bo-asset-mdvrs", path: "components/portals/back-office/asset/mdvrs/list"},
                    { name: "bo-asset-mdvrs-manage-two", path: "components/portals/back-office/asset/mdvrs/manage2"},
                    { name: "bo-asset-mdvrs-manage", path: "components/portals/back-office/asset/mdvrs/manage" },
                    { name: "bo-asset-mdvrs-update-multiple", path: "components/portals/back-office/asset/mdvrs/update-multiple" },
                    { name: "bo-asset-mdvrs-search", path: "components/portals/back-office/asset/mdvrs/search" },
                    { name: "bo-asset-mdvrs-import", path: "components/portals/back-office/asset/mdvrs/import" },

                    // Asset - Mdvrs Config
                    { name: "bo-asset-mdvrs-config", path: "components/portals/back-office/asset/mdvrs/mdvrconfig/list" },
                    { name: "bo-asset-mdvrs-config-update", path: "components/portals/back-office/asset/mdvrs/mdvrconfig/manage" },
                    { name: "bo-asset-mdvrs-config-manage", path: "components/portals/back-office/asset/mdvrs/mdvrconfig/manage" },
                    
                    // Asset - Vehicle Model
                    { name: "bo-asset-vehicle-model", path: "components/portals/back-office/asset/vehicle-model/list" },
                    { name: "bo-asset-vehicle-model-manage", path: "components/portals/back-office/asset/vehicle-model/manage" },

                    // Asset - DMS Model
                    { name: "bo-asset-dms-model-manage", path: "components/portals/back-office/asset/dms/model/manage" },
                    { name: "bo-asset-dms-model", path: "components/portals/back-office/asset/dms/model/list" },

                    // Asset - DMS
                    { name: "bo-asset-dms", path: "components/portals/back-office/asset/dms/menu" },
                    { name: "bo-asset-dms-device", path: "components/portals/back-office/asset/dms/device/list" },
                    { name: "bo-asset-dms-manage", path: "components/portals/back-office/asset/dms/device/manage" },
                    { name: "bo-asset-dms-manage-confirm-dialog", path: "components/portals/back-office/asset/dms/device/confirm-dialog" },

                    //Asset - DMS Config
                    { name: "bo-asset-dms-config", path: "components/portals/back-office/asset/dms/config/list" },
                    { name: "bo-asset-dms-config-manage", path: "components/portals/back-office/asset/dms/config/manage" },
                    { name: "bo-asset-dms-config-config-key", path: "components/portals/back-office/asset/dms/config/config-key" },


                    // Asset - ADAS Model
                    { name: "bo-asset-adas-model-manage", path: "components/portals/back-office/asset/adas/model/manage" },
                    { name: "bo-asset-adas-model", path: "components/portals/back-office/asset/adas/model/list" },

                    // Asset - ADAS
                    { name: "bo-asset-adas", path: "components/portals/back-office/asset/adas/menu" },
                    { name: "bo-asset-adas-device", path: "components/portals/back-office/asset/adas/device/list" },
                    { name: "bo-asset-adas-manage", path: "components/portals/back-office/asset/adas/device/manage" },
                    { name: "bo-asset-adas-manage-confirm-dialog", path: "components/portals/back-office/asset/adas/device/confirm-dialog" },
                    
                    // Announcement
                    { name: "bo-announcement", path: "components/portals/back-office/announcement/list" },
                    { name: "bo-announcement-search", path: "components/portals/back-office/announcement/search" },
                    { name: "bo-announcement-manage", path: "components/portals/back-office/announcement/manage" },
                    { name: "bo-announcement-manage-message-manage", path: "components/portals/back-office/announcement/message-manage" },
                    // Configuration
                    { name: "bo-configuration", path: "components/portals/back-office/configuration/menu" },
                    // Configuration - Translation
                    { name: "bo-configuration-translation", path: "components/portals/back-office/configuration/translation/list" },
                    { name: "bo-configuration-translation-import", path: "components/portals/back-office/configuration/translation/import" },
                    { name: "bo-configuration-translation-export", path: "components/portals/back-office/configuration/translation/export" },
                    { name: "bo-configuration-translation-language", path: "components/portals/back-office/configuration/translation/language" },
                    { name: "bo-configuration-translation-language-add", path: "components/portals/back-office/configuration/translation/language-add" },
                    // Configuration - POI Icon
                    { name: "bo-configuration-poi-icon", path: "components/portals/back-office/configuration/poi-icon/list" },
                    { name: "bo-configuration-poi-icon-manage", path: "components/portals/back-office/configuration/poi-icon/manage" },
                    // Configuration - Vehicle Icon
                    { name: "bo-configuration-vehicle-icon", path: "components/portals/back-office/configuration/vehicle-icon/list" },
                    { name: "bo-configuration-vehicle-icon-manage", path: "components/portals/back-office/configuration/vehicle-icon/manage" },
                    // Configuration - System Setting
                    { name: "bo-configuration-system-setting", path: "components/portals/back-office/configuration/system-setting/list" },
                    { name: "bo-configuration-system-setting-manage", path: "components/portals/back-office/configuration/system-setting/manage" },
                    //Configuration - Tracking Databases
                    { name: "bo-configuration-tracking-databases", path: "components/portals/back-office/configuration/tracking-databases/list" },
                    { name: "bo-configuration-tracking-databases-manage", path: "components/portals/back-office/configuration/tracking-databases/manage" },
                    //Configuration - Auto Mail
                    { name: "bo-configuration-auto-mail", path: "components/portals/back-office/configuration/auto-mail/menu" },
                    { name: "bo-configuration-sql-template-config", path: "components/portals/back-office/configuration/auto-mail/sql-template-config/list" },
                    { name: "bo-automail-sql-template-manage", path: "components/portals/back-office/configuration/auto-mail/sql-template-config/manage" },
                    { name: "bo-configuration-mail-config", path: "components/portals/back-office/configuration/auto-mail/mail-config/list" },
                    { name: "bo-automail-mail-config-manage", path: "components/portals/back-office/configuration/auto-mail/mail-config/manage" },
                    { name: "bo-automail-mail-config-view", path: "components/portals/back-office/configuration/auto-mail/mail-config/view" },
                    { name: "bo-configuration-sending-log", path: "components/portals/back-office/configuration/auto-mail/sending-log/search" },
                    { name: "bo-configuration-sending-log-manage", path: "components/portals/back-office/configuration/auto-mail/sending-log/list" },

                    // Configuration - MapService
                    { name: "bo-configuration-map-service", path: "components/portals/back-office/configuration/map-service/list" },
                    { name: "bo-configuration-map-service-manage", path: "components/portals/back-office/configuration/map-service/manage" },
                   

                    // Configuration - nostra-configurations
                    { name: "bo-configuration-nostra-configurations", path: "components/portals/back-office/configuration/nostra-configurations/list" },
                    { name: "bo-configuration-nostra-configurations-manage", path: "components/portals/back-office/configuration/nostra-configurations/manage" },

                    // Configuration - GIS-Service-configurations
                    { name: "bo-configuration-gis-service-configurations", path: "components/portals/back-office/configuration/gisservice-configurations/list" },
                    { name: "bo-configuration-gis-service-configurations-manage", path: "components/portals/back-office/configuration/gisservice-configurations/manage" },

                    // Report
                    { name: "bo-report", path: "components/portals/back-office/report/menu" },
                    //Email Logs
                    { name: "bo-report-email-log", path: "components/portals/back-office/report/email-log/email-log" },
                    // Report - Company Expiration
                    { name: "bo-report-company-expiration", path: "components/portals/back-office/report/company-expiration/list" },
                    { name: "bo-report-company-expiration-search", path: "components/portals/back-office/report/company-expiration/search" },
                    // Report - Contract Expiration
                    { name: "bo-report-contract-expiration", path: "components/portals/back-office/report/contract-expiration/menu" },
                    { name: "bo-report-contract-expiration-search", path: "components/portals/back-office/report/contract-expiration/shared/search" },
                    { name: "bo-report-contract-expiration-fleet-service", path: "components/portals/back-office/report/contract-expiration/fleet-service/list" },
                    { name: "bo-report-contract-expiration-mobile-service", path: "components/portals/back-office/report/contract-expiration/mobile-service/list" },
                    { name: "bo-report-contract-expiration-box-maintenance", path: "components/portals/back-office/report/contract-expiration/box-maintenance/list" },
                    //Report - DLTReport
                    { name: "bo-report-dlt-report-search", path: "components/portals/back-office/report/dlt-report/search" },
                    { name: "bo-report-dlt-report-list", path: "components/portals/back-office/report/dlt-report/list" },
                    
                    //Report - DLT New Installation
                    { name: "bo-report-dlt-new-installation-search", path: "components/portals/back-office/report/dlt-new-installation/search" },
                    { name: "bo-report-dlt-new-installation-list", path: "components/portals/back-office/report/dlt-new-installation/list" },

                    //Report - DLT Next Year
                    { name: "bo-report-dlt-next-year-search", path: "components/portals/back-office/report/dlt-nextyear/search" },
                    { name: "bo-report-dlt-next-year-list", path: "components/portals/back-office/report/dlt-nextyear/list" },   

                    //Report - Letter
                    { name: "bo-report-letter-search", path: "components/portals/back-office/report/letter/search" },
                    { name: "bo-report-letter-list", path: "components/portals/back-office/report/letter/list" },                    
                    
                    { name: "bo-ticket-manage", path: "components/portals/back-office/ticket-manage/menu" },
                    { name: "bo-ticket-manage-asset", path: "components/portals/back-office/ticket-manage/asset-monitoring/list" },
                    { name: "bo-ticket-manage-asset-vehicle", path: "components/portals/back-office/ticket-manage/asset-monitoring/asset-find" },
                    { name: "bo-ticket-manage-asset-find", path: "components/portals/back-office/ticket-manage/asset-monitoring/asset-find" },
                    { name: "bo-ticket-manage-asset-create", path: "components/portals/back-office/ticket-manage/asset-monitoring/asset-create" },
                    { name: "bo-ticket-manage-asset-update", path: "components/portals/back-office/ticket-manage/asset-monitoring/asset-update" },
                    { name: "bo-ticket-manage-asset-history", path: "components/portals/back-office/ticket-manage/asset-monitoring/asset-history" },
                    { name: "bo-ticket-manage-asset-details", path: "components/portals/back-office/ticket-manage/asset-monitoring/asset-details" },
                    { name: "bo-ticket-manage-asset-vehicle-info", path: "components/portals/back-office/ticket-manage/asset-monitoring/vehicle-info" },
                    { name: "bo-ticket-manage-asset-detail-vehicle-info", path: "components/portals/back-office/ticket-manage/asset-monitoring/vehicle-info" },
                    { name: "bo-ticket-manage-detail-close", path: "components/portals/back-office/ticket-manage/ticket/manage/detail-close" },
                    { name: "bo-ticket-manage-asset-batch-action", path: "components/portals/back-office/ticket-manage/asset-monitoring/batch-action" },
                    
                    { name: "bo-ticket-dashboard", path: "components/portals/back-office/ticket-manage/ticket/dashboard" },
                    { name: "bo-ticket-searchDashboard", path: "components/portals/back-office/ticket-manage/ticket/search" },
                    
                    { name: "bo-ticket-manage-edit", path: "components/portals/back-office/ticket-manage/ticket/manage/edit" },
                    { name: "bo-ticket-manage-edit-appointment", path: "components/portals/back-office/ticket-manage/ticket/manage/edit-appointment" },
                    { name: "bo-ticket-manage-view-appointment", path: "components/portals/back-office/ticket-manage/ticket/manage/view-appointment" },
                    
                    { name: "bo-ticket-manage-sendmail", path: "components/portals/back-office/ticket-manage/ticket/manage/sendmail" },
                    { name: "bo-ticket-manage-create", path: "components/portals/back-office/ticket-manage/ticket/manage/create" },
                    { name: "bo-ticket-manage-fleet-service", path: "components/portals/back-office/ticket-manage/ticket/manage/fleet-service" },
                    { name: "bo-ticket-manage-detail-fleet-service", path: "components/portals/back-office/ticket-manage/ticket/manage/fleet-service" },
                    
                    { name: "bo-ticket-manage-ticket-new", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-new" },
                    { name: "bo-ticket-manage-ticket-ack", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-ack" },
                    { name: "bo-ticket-manage-ticket-inprog", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-inprog" },
                    { name: "bo-ticket-manage-ticket-confirm", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-confirm" },
                    { name: "bo-ticket-manage-ticket-pending", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-pending" },
                    { name: "bo-ticket-manage-ticket-follow", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-follow" },
                    { name: "bo-ticket-manage-ticket-close", path: "components/portals/back-office/ticket-manage/ticket/manage/ticket-close" },
                    { name: "bo-ticket-manage-ticket-view-error", path: "components/portals/back-office/ticket-manage/ticket/manage/view-error" },

                    { name: "bo-ticket-manage-installation", path: "components/portals/back-office/ticket-manage/installation/form" },

                    { name: "bo-ticket-manage-appointments", path: "components/portals/back-office/ticket-manage/appointments/form" },
                    { name: "bo-ticket-manage-appointments-view", path: "components/portals/back-office/ticket-manage/appointments/view" },



                    { name: "bo-ticket-manage-mail", path: "components/portals/back-office/ticket-manage/mail/list" },
                    { name: "bo-ticket-manage-mail-manage", path: "components/portals/back-office/ticket-manage/mail/manage" },



                    { name: "bo-ticket-manage-sim", path: "components/portals/back-office/ticket-manage/sim/list" },
                    { name: "bo-ticket-manage-sim-search", path: "components/portals/back-office/ticket-manage/sim/search" },
                    { name: "bo-ticket-manage-sim-view", path: "components/portals/back-office/ticket-manage/sim/view" },
                    { name: "bo-ticket-manage-sim-manage", path: "components/portals/back-office/ticket-manage/sim/manage" },
                    { name: "bo-ticket-manage-sim-import", path: "components/portals/back-office/ticket-manage/sim/import" },

                    { name: "bo-ticket-manage-export-with-criteria", path: "components/portals/back-office/ticket-manage/ticket/manage/export-with-criteria" },
                    
                    // Ticket Sim Stock
                    { name: "bo-ticket-manage-sim-stock", path: "components/portals/back-office/ticket-manage/sim-stock/list" },
                    { name: "bo-ticket-manage-sim-stock-manage", path: "components/portals/back-office/ticket-manage/sim-stock/manage" },

                    // Ticket - Operator Package
                    { name: "bo-ticket-manage-package", path: "components/portals/back-office/ticket-manage/operator-package/list" },
                    { name: "bo-ticket-manage-package-manage", path: "components/portals/back-office/ticket-manage/operator-package/manage" },
                    
                    // Ticket - vendors
                    { name: "bo-ticket-manage-vendor", path: "components/portals/back-office/ticket-manage/vendor/list" },
                    { name: "bo-ticket-manage-vendor-manage", path: "components/portals/back-office/ticket-manage/vendor/manage" },
                    { name: "bo-ticket-manage-vendor-manage-signature", path: "components/portals/back-office/ticket-manage/vendor/signature" },

                    // Ticket - Transporter
                    { name: "bo-ticket-manage-transporter", path: "components/portals/back-office/ticket-manage/transporter/list" },
                    { name: "bo-ticket-manage-transporter-manage", path: "components/portals/back-office/ticket-manage/transporter/manage" },
                    { name: "bo-ticket-manage-transporter-manage-vehicle", path: "components/portals/back-office/ticket-manage/transporter/vehicle" },
                    { name: "bo-ticket-manage-transporter-manage-find-vehicle", path: "components/portals/back-office/ticket-manage/transporter/find-vehicle" },

                    //Ticket - Search                    
                    { name: "bo-ticket-search", path: "components/portals/back-office/ticket-manage/ticket-search/list" },
                    { name: "bo-ticket-manage-search", path: "components/portals/back-office/ticket-manage/ticket-search/search" },
                    
                    //About
                    { name: "bo-about", path: "components/portals/back-office/about/about" }
                ]
            },
            {
                group: "company-admin-portal-screens",
                items: [
                    // Contract
                    { name: "ca-contract", path: "components/portals/company-admin/contract/list" },
                    { name: "ca-contract-search", path: "components/portals/company-admin/contract/search" },
                    { name: "ca-contract-view", path: "components/portals/company-admin/contract/view" },
                    { name: "ca-contract-manage", path: "components/portals/company-admin/contract/manage" },
                    // Business Unit
                    { name: "ca-business-unit-menu", path: "components/portals/company-admin/business-unit/menu" },
                    { name: "ca-business-unit", path: "components/portals/company-admin/business-unit/list" },
                    { name: "ca-business-unit-view", path: "components/portals/company-admin/business-unit/view" },
                    { name: "ca-business-units-hierarchy-manage", path: "components/portals/company-admin/business-unit/business-units-hierarchy-manage" },
                    { name: "ca-business-units-levels", path: "components/portals/company-admin/business-unit/business-units-levels" },
                    { name: "ca-business-units-levels-manage", path: "components/portals/company-admin/business-unit/business-units-levels-manage" },
                    { name: "ca-business-units-authorized-persons", path: "components/portals/company-admin/business-unit/authorized-persons" },

                    // Group
                    { name: "ca-group", path: "components/portals/company-admin/group/list" },
                    { name: "ca-group-view", path: "components/portals/company-admin/group/view" },
                    { name: "ca-group-manage", path: "components/portals/company-admin/group/manage" },
                    // User
                    { name: "ca-user", path: "components/portals/company-admin/user/list" },
                    { name: "ca-user-search", path: "components/portals/company-admin/user/search" },
                    { name: "ca-user-view", path: "components/portals/company-admin/user/view" },
                    { name: "ca-user-manage", path: "components/portals/company-admin/user/manage" },
                    { name: "ca-user-manage-group-select", path: "components/portals/company-admin/user/group-select" },
                    { name: "ca-user-manage-accessible-business-unit-select", path: "components/portals/company-admin/user/accessible-business-unit-select" },
                    { name: "ca-user-reset-password", path: "components/portals/company-admin/user/reset-password" },
                    // Asset
                    { name: "ca-asset", path: "components/portals/company-admin/asset/menu" },
                    // Asset - Box
                    { name: "ca-asset-box", path: "components/portals/company-admin/asset/box/list" },
                    { name: "ca-asset-box-search", path: "components/portals/company-admin/asset/box/search" },
                    { name: "ca-asset-box-view", path: "components/portals/company-admin/asset/box/view" },
                    { name: "ca-asset-box-view-contract", path: "components/portals/company-admin/asset/box/contract" },
                    { name: "ca-asset-box-view-maintenance", path: "components/portals/company-admin/asset/box/maintenance" },
                    { name: "ca-asset-box-view-maintenance-manage", path: "components/portals/company-admin/asset/box/maintenance-manage" },
                    { name: "ca-asset-box-view-maintenance-view", path: "components/portals/company-admin/asset/box/maintenance-view" },
                    // Asset - Vehicle
                    { name: "ca-asset-vehicle", path: "components/portals/company-admin/asset/vehicle/list" },
                    { name: "ca-asset-vehicle-search", path: "components/portals/company-admin/asset/vehicle/search" },
                    { name: "ca-asset-vehicle-manage", path: "components/portals/company-admin/asset/vehicle/manage" },
                    { name: "ca-asset-vehicle-manage-license-detail-manage", path: "components/portals/company-admin/asset/vehicle/license-detail-manage" },
                    { name: "ca-asset-vehicle-manage-default-driver-manage", path: "components/portals/company-admin/asset/vehicle/default-driver-manage" },
                    { name: "ca-asset-vehicle-manage-alert-manage", path: "components/portals/company-admin/asset/vehicle/alert-manage" },
                    { name: "ca-asset-vehicle-import", path: "components/portals/company-admin/asset/vehicle/import" },
                    { name: "ca-asset-vehicle-view", path: "components/portals/company-admin/asset/vehicle/view" },
                    { name: "ca-asset-vehicle-view-maintenance-plan", path: "components/portals/company-admin/asset/vehicle/maintenance-plan" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-manage", path: "components/portals/company-admin/asset/vehicle/maintenance-plan-manage" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-view", path: "components/portals/company-admin/asset/vehicle/maintenance-plan-view" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-view-log", path: "components/portals/company-admin/asset/vehicle/maintenance-log-view" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-view-cancel", path: "components/portals/company-admin/asset/vehicle/maintenance-plan-view-cancel" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-complete", path: "components/portals/company-admin/asset/vehicle/maintenance-plan-complete" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-cancel", path: "components/portals/company-admin/asset/vehicle/maintenance-plan-cancel" },
                    { name: "ca-asset-vehicle-view-maintenance-plan-create-log", path: "components/portals/company-admin/asset/vehicle/maintenance-log-create" },
                    { name: "ca-asset-vehicle-view-odo-meter-manage", path: "components/portals/company-admin/asset/vehicle/odo-meter-manage" },
                    { name: "ca-asset-vehicle-view-contract", path: "components/portals/company-admin/asset/vehicle/contract" },
                    { name: "ca-asset-vehicle-dms-detail", path: "components/portals/company-admin/asset/vehicle/dms-detail" },
                    { name: "ca-asset-vehicle-adas-detail", path: "components/portals/company-admin/asset/vehicle/adas-detail" },
                    { name: "ca-asset-vehicle-view-odo-meter-fixing", path: "components/portals/company-admin/asset/vehicle/odo-meter-fixing" },
                    { name: "ca-asset-vehicle-view-confirm-dialog-changeBu", path: "components/portals/company-admin/asset/vehicle/confirm-dialog-changeBu" },
                   
                    // Asset - Driver
                    { name: "ca-asset-driver", path: "components/portals/company-admin/asset/driver/list" },
                    { name: "ca-asset-driver-search", path: "components/portals/company-admin/asset/driver/search" },
                    { name: "ca-asset-driver-manage", path: "components/portals/company-admin/asset/driver/manage" },
                    { name: "ca-asset-driver-import", path: "components/portals/company-admin/asset/driver/import" },
                    { name: "ca-asset-driver-import-picture", path: "components/portals/company-admin/asset/driver/import-picture" },
                    { name: "ca-asset-driver-view", path: "components/portals/company-admin/asset/driver/view" },
                    { name: "ca-asset-driver-card", path: "components/portals/company-admin/asset/driver/driver-card" },
                    // Asset - Fleet Service
                    { name: "ca-asset-fleet-service", path: "components/portals/company-admin/asset/fleet-service/list" },
                    { name: "ca-asset-fleet-service-search", path: "components/portals/company-admin/asset/fleet-service/search" },
                    { name: "ca-asset-fleet-service-manage", path: "components/portals/company-admin/asset/fleet-service/manage" },
                    { name: "ca-asset-fleet-service-view", path: "components/portals/company-admin/asset/fleet-service/view" },
                    // Asset - Mobile Service
                    { name: "ca-asset-mobile-service", path: "components/portals/company-admin/asset/mobile-service/list" },
                    { name: "ca-asset-mobile-service-search", path: "components/portals/company-admin/asset/mobile-service/search" },
                    { name: "ca-asset-mobile-service-manage", path: "components/portals/company-admin/asset/mobile-service/manage" },
                    { name: "ca-asset-mobile-service-view", path: "components/portals/company-admin/asset/mobile-service/view" },
                    // Asset - SMS Service
                    { name: "ca-asset-sms-service", path: "components/portals/company-admin/asset/sms-service/list" },
                    { name: "ca-asset-sms-service-search", path: "components/portals/company-admin/asset/sms-service/search" },
                    { name: "ca-asset-sms-service-manage", path: "components/portals/company-admin/asset/sms-service/manage" },
                    { name: "ca-asset-sms-service-view", path: "components/portals/company-admin/asset/sms-service/view" },
                    
                    // Asset - Trainer
                    { name: "ca-asset-trainer", path: "components/portals/company-admin/asset/trainer/list"},
                    { name: "ca-asset-trainer-search", path: "components/portals/company-admin/asset/trainer/search"},
                    { name: "ca-asset-trainer-view", path: "components/portals/company-admin/asset/trainer/view"},
                    { name: "ca-asset-trainer-manage", path: "components/portals/company-admin/asset/trainer/manage"},
                    { name: "ca-asset-trainer-import", path: "components/portals/company-admin/asset/trainer/import"},
                    { name: "ca-asset-trainer-import-picture", path: "components/portals/company-admin/asset/trainer/import-picture"},

                    // Driver Performance Rule
                    { name: "ca-driver-performance-rule-menu", path: "components/portals/company-admin/driver-performance-rule/menu" },
                    { name: "ca-driver-performance-rule", path: "components/portals/company-admin/driver-performance-rule/list" },
                    { name: "ca-driver-performance-rule-search", path: "components/portals/company-admin/driver-performance-rule/search" },
                    { name: "ca-driver-performance-rule-manage", path: "components/portals/company-admin/driver-performance-rule/manage" },
                    { name: "ca-driver-performance-rule-view", path: "components/portals/company-admin/driver-performance-rule/view" },

                    // Advance Driver Performance Rule
                    { name: "ca-advance-driver-performance-rule", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/list" },
                    { name: "ca-advance-driver-performance-rule-manage", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/manage" },
                    { name: "ca-advance-driver-performance-rule-accessible-bu-select", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/accessible-business-unit-select" },
                    { name: "ca-advance-driver-performance-rule-speed", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/speed" },
                    { name: "ca-advance-driver-performance-rule-behavior", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/behavior" },
                    { name: "ca-advance-driver-performance-rule-fuel", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/fuel" },
                    { name: "ca-advance-driver-performance-rule-view", path: "components/portals/company-admin/driver-performance-rule/advance-driver-performance-rule/view" },

                    // Configuration
                    { name: "ca-configuration", path: "components/portals/company-admin/configuration/menu" },

                    // Announcement
                    { name: "ca-configuration-announcement", path: "components/portals/company-admin/configuration/announcement/list" },
                    { name: "ca-configuration-announcement-search", path: "components/portals/company-admin/configuration/announcement/search" },
                    { name: "ca-configuration-announcement-manage", path: "components/portals/company-admin/configuration/announcement/manage" },
                    { name: "ca-configuration-announcement-manage-message-manage", path: "components/portals/company-admin/configuration/announcement/message-manage" },
                    { name: "ca-configuration-accessible-bu-select", path: "components/portals/company-admin/configuration/announcement/accessible-business-unit-select" },
                    { name: "ca-configuration-announcement-view", path: "components/portals/company-admin/configuration/announcement/view" },

                    // Configuration - Alert
                    { name: "ca-configuration-alert", path: "components/portals/company-admin/configuration/alert/list" },
                    { name: "ca-configuration-alert-search", path: "components/portals/company-admin/configuration/alert/search" },
                    { name: "ca-configuration-alert-manage", path: "components/portals/company-admin/configuration/alert/manage" },
                    { name: "ca-configuration-alert-manage-asset-select", path: "components/portals/company-admin/configuration/alert/asset-select" },
                    { name: "ca-configuration-alert-manage-areasetting-select", path: "components/portals/company-admin/configuration/alert/areasetting-select" },
                    { name: "ca-configuration-alert-view", path: "components/portals/company-admin/configuration/alert/view" },
                    { name: "ca-configuration-alert-manage-datetimesetting-select", path: "components/portals/company-admin/configuration/alert/datetimesetting-select" },
                    { name: "ca-configuration-alert-manage-allow", path: "components/portals/company-admin/configuration/alert/condition-create/allow-manage" },
                    { name: "ca-configuration-alert-manage-not-allow", path: "components/portals/company-admin/configuration/alert/condition-create/not-allow-manage" },
                    { name: "ca-configuration-alert-manage-stop-overtime-province", path: "components/portals/company-admin/configuration/alert/condition-create/stop-overtime-province-manage" },
                    { name: "ca-configuration-alert-manage-find-asset", path: "components/portals/company-admin/configuration/alert/bu-asset-select" },
                    { name: "ca-configuration-alert-manage-asset-businessunit", path: "components/portals/company-admin/configuration/alert/asset-businessunit" },
                    { name: "ca-configuration-alert-manage-asset-result", path: "components/portals/company-admin/configuration/alert/asset-result" },
                
                    // Configuration - Alert Categories
                    { name: "ca-configuration-alert-categories", path: "components/portals/company-admin/configuration/alert-categories/list" },
                    { name: "ca-configuration-alert-categories-manage", path: "components/portals/company-admin/configuration/alert-categories/manage" },
                    { name: "ca-configuration-alert-categories-view", path: "components/portals/company-admin/configuration/alert-categories/view" },
                    { name: "ca-configuration-alert-categories-search", path: "components/portals/company-admin/configuration/alert-categories/search" },
                    // Configuration - POI Icon
                    { name: "ca-configuration-poi-icon", path: "components/portals/company-admin/configuration/poi-icon/list" },
                    { name: "ca-configuration-poi-icon-manage", path: "components/portals/company-admin/configuration/poi-icon/manage" },
                    { name: "ca-configuration-poi-icon-view", path: "components/portals/company-admin/configuration/poi-icon/view" },
                    // Configuration - Translation
                    { name: "ca-configuration-translation", path: "components/portals/company-admin/configuration/translation/list" },
                    { name: "ca-configuration-translation-import", path: "components/portals/company-admin/configuration/translation/import" },
                    { name: "ca-configuration-translation-export", path: "components/portals/company-admin/configuration/translation/export" },
                    // Configuration - Barrier
                    { name: "ca-configuration-barrier", path: "components/portals/company-admin/configuration/barrier/list" },
                    { name: "ca-configuration-barrier-manage", path: "components/portals/company-admin/configuration/barrier/manage" },
                    { name: "ca-configuration-barrier-view", path: "components/portals/company-admin/configuration/barrier/view" },
                    // Configuration - Ticket Response
                    { name: "ca-configuration-ticket-response", path: "components/portals/company-admin/configuration/ticket-response/list" },
                    { name: "ca-configuration-ticket-response-manage", path: "components/portals/company-admin/configuration/ticket-response/manage" },
                    { name: "ca-configuration-ticket-response-view", path: "components/portals/company-admin/configuration/ticket-response/view" },
                    
                   // Configuration - Telematices Configuration
                   { name: "ca-configuration-telematices-configuration", path: "components/portals/company-admin/configuration/telematics-configuration/manage" },

                    // Maintenance Management
                    // Dashboard
                    { name: "ca-asset-maintenance-management", path: "components/portals/company-admin/asset/maintenance-management/dashboard" },

                    // Maintenance Plan
                    { name: "ca-asset-maintenance-management-ma-plan", path: "components/portals/company-admin/asset/maintenance-management/maintenance-plan/list" },
                    { name: "ca-asset-maintenance-management-ma-plan-search", path: "components/portals/company-admin/asset/maintenance-management/maintenance-plan/search" },
                    { name: "ca-asset-maintenance-management-ma-plan-create", path: "components/portals/company-admin/asset/maintenance-management/maintenance-plan/create" },
                    { name: "ca-asset-maintenance-management-ma-plan-create-vehicle", path: "components/portals/company-admin/asset/maintenance-management/maintenance-plan/vehicle-select" },
                    { name: "ca-asset-maintenance-management-ma-plan-import", path: "components/portals/company-admin/asset/maintenance-management/maintenance-plan/import" },
                    { name: "ca-asset-maintenance-management-ma-plan-export", path: "components/portals/company-admin/asset/maintenance-management/maintenance-plan/export" },
                
                    //Maintenance Type & Subtype
                    { name: "ca-asset-maintenance-management-ma-type", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/list" },
                    { name: "ca-asset-maintenance-management-ma-type-manage", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/manage" },
                    { name: "ca-asset-maintenance-management-ma-type-manage-import", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/import" },
                    { name: "ca-asset-maintenance-management-ma-type-view", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/view" },
                    { name: "ca-asset-maintenance-management-ma-type-view-subtype", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/subtype-list" },
                    { name: "ca-asset-maintenance-management-ma-type-view-subtype-view", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/subtype-view" },
                    { name: "ca-asset-maintenance-management-ma-type-view-subtype-manage", path: "components/portals/company-admin/asset/maintenance-management/maintenance-type/subtype-manage" },

                    //Appointment
                    { name: "ca-asset-maintenance-management-appointment", path: "components/portals/company-admin/asset/maintenance-management/appointment/list" },
                    { name: "ca-asset-maintenance-management-appointment-search", path: "components/portals/company-admin/asset/maintenance-management/appointment/search" },
                    { name: "ca-asset-maintenance-management-appointment-manage", path: "components/portals/company-admin/asset/maintenance-management/appointment/manage" },
                    { name: "ca-asset-maintenance-management-appointment-view", path: "components/portals/company-admin/asset/maintenance-management/appointment/view" },
                    { name: "ca-asset-maintenance-management-appointment-view-postpone", path: "components/portals/company-admin/asset/maintenance-management/appointment/postpone" },
                    { name: "ca-asset-maintenance-management-appointment-view-cancel", path: "components/portals/company-admin/asset/maintenance-management/appointment/cancel" },

                   

                    //Report
                    { name: "ca-asset-maintenance-management-report", path: "components/portals/company-admin/asset/maintenance-management/report/form" },
                    { name: "ca-asset-maintenance-management-report-reportviewer", path: "components/portals/company-admin/asset/maintenance-management/report/reports" },
                    // Report
                    { name: "ca-report", path: "components/portals/company-admin/report/menu" },
                    // Report - Viewer (Telerik)
                    { name: "ca-report-reportviewer", path: "components/portals/company-admin/report/reports" },
                    // Report - userlog
                    { name: "ca-report-userlog", path: "components/portals/company-admin/report/userlog/form" },
                ]
            },
            {
                group: "company-workspace-portal-screens",
                items: [
                    // Notification
                    { name: "cw-notification-alert", path: "components/portals/company-workspace/notification/alert" },
                    { name: "cw-notification-urgent-alert", path: "components/portals/company-workspace/notification/urgent-alert" },
                    { name: "cw-notification-announcement", path: "components/portals/company-workspace/notification/announcement" },
                    { name: "cw-notification-maintenance-plan", path: "components/portals/company-workspace/notification/maintenance-plan" },
                    // User Preference
                    { name: "cw-user-preference", path: "components/portals/company-workspace/user-preference" },
                    // Global Search
                    { name: "cw-global-search", path: "components/portals/company-workspace/global-search/menu" },
                    { name: "cw-global-search-find", path: "components/portals/company-workspace/global-search/find" },
                    { name: "cw-global-search-result", path: "components/portals/company-workspace/global-search/result" },
                    // Fleet Monitoring
                    { name: "cw-fleet-monitoring", path: "components/portals/company-workspace/fleet-monitoring/menu" },
                    { name: "cw-fleet-monitoring-widget", path: "components/portals/company-workspace/fleet-monitoring/track/track-widget" },
                    // Fleet Monitoring - Track Monitoring
                    { name: "cw-fleet-monitoring-track", path: "components/portals/company-workspace/fleet-monitoring/track/list" },
                    { name: "cw-fleet-monitoring-track-asset-find", path: "components/portals/company-workspace/fleet-monitoring/track/asset-find" },
                    { name: "cw-fleet-monitoring-track-asset-find-result", path: "components/portals/company-workspace/fleet-monitoring/track/asset-result" },
                    // Fleet Monitoring - Alert Monitoring
                    { name: "cw-fleet-monitoring-alert", path: "components/portals/company-workspace/fleet-monitoring/alert/dashboard" },
                    { name: "cw-fleet-monitoring-alert-list", path: "components/portals/company-workspace/fleet-monitoring/alert/list" },
                    { name: "cw-fleet-monitoring-alert-list-search", path: "components/portals/company-workspace/fleet-monitoring/alert/search" },
                    
                    // Fleet Monitoring - Alert Monitoring - Ticket
                    { name: "cw-fleet-monitoring-alert-ticket", path: "components/portals/company-workspace/fleet-monitoring/alert/ticket/list" },
                    { name: "cw-fleet-monitoring-alert-ticket-search", path: "components/portals/company-workspace/fleet-monitoring/alert/ticket/search" },
                    { name: "cw-fleet-monitoring-alert-ticket-action", path: "components/portals/company-workspace/fleet-monitoring/alert/ticket/ticket-action" },
                    // Fleet Monitoring - Playback
                    { name: "cw-fleet-monitoring-playback", path: "components/portals/company-workspace/fleet-monitoring/playback/playback" },
                    { name: "cw-fleet-monitoring-playback-timeline", path: "components/portals/company-workspace/fleet-monitoring/playback/timeline" },
                    { name: "cw-fleet-monotoring-playback-info-widget", path: "components/portals/company-workspace/fleet-monitoring/playback/playback-info-widget"},                    
                    // Fleet Monitorting - Live View
                    { name: "cw-fleet-monitoring-liveview", path: "components/portals/company-workspace/fleet-monitoring/liveview/liveview" },
                    { name: "cw-fleet-monitoring-liveview-widget", path: "components/portals/company-workspace/fleet-monitoring/liveview/liveview-widget" },


                    // Fleet Monitorting - Event View
                    { name: "cw-fleet-monitoring-eventview", path: "components/portals/company-workspace/fleet-monitoring/eventview/dashboard" },
                    { name: "cw-fleet-monitoring-eventview-list", path: "components/portals/company-workspace/fleet-monitoring/eventview/list" },
                    { name: "cw-fleet-monitoring-eventview-widget", path: "components/portals/company-workspace/fleet-monitoring/eventview/eventview-widget"},
                    // Fleet Monitoring - Event View - Ticket
                    { name: "cw-fleet-monitoring-eventview-ticket", path: "components/portals/company-workspace/fleet-monitoring/eventview/ticket/list" },
                    { name: "cw-fleet-monitoring-eventview-ticket-search", path: "components/portals/company-workspace/fleet-monitoring/eventview/ticket/search" },
                    { name: "cw-fleet-monitoring-eventview-ticket-action", path: "components/portals/company-workspace/fleet-monitoring/eventview/ticket/ticket-action" },

                    // Fleet Monitoring - common widget
                    { name: "cw-fleet-monitoring-common-video-widget", path: "components/portals/company-workspace/fleet-monitoring/widget/video-widget" },
                    { name: "cw-fleet-monitoring-common-picture-widget", path: "components/portals/company-workspace/fleet-monitoring/widget/picture-widget" },
                    { name: "cw-fleet-monitoring-common-vehicle-status-widget", path: "components/portals/company-workspace/fleet-monitoring/widget/vehicle-status-widget" },

                    // Fleet Monitoring - Playback Analysis Tool
                    { name: "cw-fleet-monitoring-playback-analysis-tool-search", path: "components/portals/company-workspace/fleet-monitoring/playback-analysis-tool/search" },
                    { name: "cw-fleet-monitoring-playback-analysis-tool", path: "components/portals/company-workspace/fleet-monitoring/playback-analysis-tool/playback-analysis-tool" },

                    //Debrief
                    { name: "cw-fleet-monitoring-debrief", path: "components/portals/company-workspace/fleet-monitoring/driver-debrief/search" },
                    { name: "cw-fleet-monitoring-debrief-view", path: "components/portals/company-workspace/fleet-monitoring/driver-debrief/view" },

                    // Fleet Monitoring - Trip Analysis
                    { name: "cw-fleet-monitoring-tripanalysis", path: "components/portals/company-workspace/fleet-monitoring/trip-analysis/search" },
                    { name: "cw-fleet-monitoring-tripanalysis-report", path: "components/portals/company-workspace/fleet-monitoring/trip-analysis/report" },

                    // Geo-Fencing
                    { name: "cw-geo-fencing", path: "components/portals/company-workspace/geo-fencing/menu" },
                    { name: "cw-geo-fencing-vehicle-info-widget", path: "components/portals/company-workspace/geo-fencing/vehicleInfo-widget" },
                    // Geo-Fencing - Custom POI
                    { name: "cw-geo-fencing-custom-poi", path: "components/portals/company-workspace/geo-fencing/custom-poi/list" },
                    { name: "cw-geo-fencing-custom-poi-search", path: "components/portals/company-workspace/geo-fencing/custom-poi/search" },
                    { name: "cw-geo-fencing-custom-poi-import", path: "components/portals/company-workspace/geo-fencing/custom-poi/import" },
                    { name: "cw-geo-fencing-custom-poi-manage", path: "components/portals/company-workspace/geo-fencing/custom-poi/manage" },
                    { name: "cw-geo-fencing-custom-poi-manage-accessible-business-unit-select", path: "components/portals/company-workspace/geo-fencing/custom-poi/accessible-business-unit-select" },
                    { name: "cw-geo-fencing-custom-poi-manage-sub-poi-area", path: "components/portals/company-workspace/geo-fencing/custom-poi/sub-poi-area" },
                    { name: "cw-geo-fencing-custom-poi-view", path: "components/portals/company-workspace/geo-fencing/custom-poi/view" },
                    { name: "cw-geo-fencing-custom-poi-view-verify", path: "components/portals/company-workspace/geo-fencing/custom-poi/verify" },
                    { name: "cw-geo-fencing-custom-poi-sub-area-view", path: "components/portals/company-workspace/geo-fencing/custom-poi/view-subarea" },
                    
                    
                    // Geo-Fencing - Custom POI Category
                    { name: "cw-geo-fencing-custom-poi-category", path: "components/portals/company-workspace/geo-fencing/custom-poi-category/list" },
                    { name: "cw-geo-fencing-custom-poi-category-manage", path: "components/portals/company-workspace/geo-fencing/custom-poi-category/manage" },
                    { name: "cw-geo-fencing-custom-poi-category-view", path: "components/portals/company-workspace/geo-fencing/custom-poi-category/view" },
                    // Geo-Fencing - Custom Route
                    { name: "cw-geo-fencing-custom-route", path: "components/portals/company-workspace/geo-fencing/custom-route/list" },
                    { name: "cw-geo-fencing-custom-route-search", path: "components/portals/company-workspace/geo-fencing/custom-route/search" },
                    { name: "cw-geo-fencing-custom-route-manage", path: "components/portals/company-workspace/geo-fencing/custom-route/manage" },
                    { name: "cw-geo-fencing-custom-route-manage-accessible-business-unit-select", path: "components/portals/company-workspace/geo-fencing/custom-route/accessible-business-unit-select" },
                    { name: "cw-geo-fencing-custom-route-manage-custom-poi-select", path: "components/portals/company-workspace/geo-fencing/custom-route/custom-poi-select" },
                    { name: "cw-geo-fencing-custom-route-manage-custom-poi-detail", path: "components/portals/company-workspace/geo-fencing/custom-route/custom-poi-detail" },
                    { name: "cw-geo-fencing-custom-route-manage-custom-area-create", path: "components/portals/company-workspace/geo-fencing/custom-route/custom-area-create" },
                    { name: "cw-geo-fencing-custom-route-manage-setting", path: "components/portals/company-workspace/geo-fencing/custom-route/setting" },
                    { name: "cw-geo-fencing-custom-route-view", path: "components/portals/company-workspace/geo-fencing/custom-route/view" },
                    // Geo-Fencing - Custom Route Category
                    { name: "cw-geo-fencing-custom-route-category", path: "components/portals/company-workspace/geo-fencing/custom-route-category/list" },
                    { name: "cw-geo-fencing-custom-route-category-manage", path: "components/portals/company-workspace/geo-fencing/custom-route-category/manage" },
                    { name: "cw-geo-fencing-custom-route-category-view", path: "components/portals/company-workspace/geo-fencing/custom-route-category/view" },
                    // Geo-Fencing - Custom POI Suggestion
                    { name: "cw-geo-fencing-custom-poi-suggestion", path: "components/portals/company-workspace/geo-fencing/custom-poi-suggestion/list" },
                    { name: "cw-geo-fencing-custom-poi-suggestion-create", path: "components/portals/company-workspace/geo-fencing/custom-poi-suggestion/create" },
                    { name: "cw-geo-fencing-custom-poi-suggestion-create-accessible-business-unit-select", path: "components/portals/company-workspace/geo-fencing/custom-poi-suggestion/accessible-business-unit-select" },                    
                    // Geo-Fencing - Custom Area
                    { name: "cw-geo-fencing-custom-area", path: "components/portals/company-workspace/geo-fencing/custom-area/list" },
                    { name: "cw-geo-fencing-custom-area-search", path: "components/portals/company-workspace/geo-fencing/custom-area/search" },
                    { name: "cw-geo-fencing-custom-area-manage", path: "components/portals/company-workspace/geo-fencing/custom-area/manage" },
                    { name: "cw-geo-fencing-custom-area-manage-accessible-business-unit-select", path: "components/portals/company-workspace/geo-fencing/custom-area/accessible-business-unit-select" },
                    { name: "cw-geo-fencing-custom-area-view", path: "components/portals/company-workspace/geo-fencing/custom-area/view" },
                    // Geo-Fencing - Custom Area Category
                    { name: "cw-geo-fencing-custom-area-category", path: "components/portals/company-workspace/geo-fencing/custom-area-category/list" },
                    { name: "cw-geo-fencing-custom-area-category-manage", path: "components/portals/company-workspace/geo-fencing/custom-area-category/manage" },
                    { name: "cw-geo-fencing-custom-area-category-view", path: "components/portals/company-workspace/geo-fencing/custom-area-category/view" },
                    // Geo-Fencing - Route From Here
                    { name: "cw-geo-fencing-routes-from-here", path: "components/portals/company-workspace/geo-fencing/route-from-here/routes" },

                    // Geo-Fencing - Working Area
                    { name: "cw-geo-working-area", path: "components/portals/company-workspace/geo-fencing/working-area/working-area"},

                    // Shipment
                    { name: "cw-shipment", path: "components/portals/company-workspace/shipment/menu" },

                    // Shipment - List Table
                    { name: "cw-shipment-list", path: "components/portals/company-workspace/shipment/list" },
                    { name: "cw-shipment-list-search", path: "components/portals/company-workspace/shipment/search" },
                    { name: "cw-shipment-manage", path: "components/portals/company-workspace/shipment/manage" },
                    { name: "cw-shipment-import", path: "components/portals/company-workspace/shipment/import" },
                    { name: "cw-shipment-finish", path: "components/portals/company-workspace/shipment/shipment-finish" },
                    { name: "cw-shipment-cancel", path: "components/portals/company-workspace/shipment/shipment-cancel" },
                    { name: "cw-shipment-list-do-manage", path: "components/portals/company-workspace/shipment/do-manage" },
                    { name: "cw-shipment-list-do-waypoint", path: "components/portals/company-workspace/shipment/do-waypoint" },
                    { name: "cw-shipment-import-with-template", path: "components/portals/company-workspace/shipment/import-with-template" },

                    // Shipment - Summary
                    { name: "cw-shipment-summary", path: "components/portals/company-workspace/shipment/summary" },

                    // Shipment - Dashboard
                    { name: "cw-shipment-dashboard", path: "components/portals/company-workspace/shipment/dashboard" },
                    { name: "cw-shipment-dashboard-search", path: "components/portals/company-workspace/shipment/dashboard-search" },

                    // Shipment - PO Search
                    { name: "cw-shipment-posearch", path: "components/portals/company-workspace/shipment/po-list" },
                    { name: "cw-shipment-polist-search", path: "components/portals/company-workspace/shipment/po-list-search" },

                    // Shipment - Timeline
                    { name: "cw-shipment-timeline", path: "components/portals/company-workspace/shipment/timeline" },
                    
                    // Shipment - Job
                    { name: "cw-shipment-job", path: "components/portals/company-workspace/shipment/job/list" },
                    { name: "cw-shipment-job-search", path: "components/portals/company-workspace/shipment/job/search" },
                    { name: "cw-shipment-job-import", path: "components/portals/company-workspace/shipment/job/import" },
                    { name: "cw-shipment-job-import-preview", path: "components/portals/company-workspace/shipment/job/import-preview" },
                    { name: "cw-shipment-job-template-select", path: "components/portals/company-workspace/shipment/job/template-select" },
                    { name: "cw-shipment-job-manage", path: "components/portals/company-workspace/shipment/job/manage" },
                    { name: "cw-shipment-job-manage-vehicle-and-driver-select", path: "components/portals/company-workspace/shipment/job/vehicle-and-driver-select" },
                    { name: "cw-shipment-job-manage-destination", path: "components/portals/company-workspace/shipment/job/destination/destination" },
                    { name: "cw-shipment-job-manage-destination-waypoint-order-select", path: "components/portals/company-workspace/shipment/job/destination/waypoint-order-select" },
                    { name: "cw-shipment-job-manage-destination-order-select", path: "components/portals/company-workspace/shipment/job/destination/order-select" },
                    { name: "cw-shipment-job-manage-destination-direction", path: "components/portals/company-workspace/shipment/job/destination/direction" },
                    { name: "cw-shipment-job-manage-destination-direction-routing", path: "components/portals/company-workspace/shipment/job/destination/direction-routing" },
                    { name: "cw-shipment-job-manage-alert-select", path: "components/portals/company-workspace/shipment/job/alert-select" },
                    { name: "cw-shipment-job-view", path: "components/portals/company-workspace/shipment/job/view" },
                    { name: "cw-shipment-job-cancel", path: "components/portals/company-workspace/shipment/job/cancel" },
                    // Shipment - Job Template
                    { name: "cw-shipment-job-template", path: "components/portals/company-workspace/shipment/job-template/list" },
                    { name: "cw-shipment-job-template-search", path: "components/portals/company-workspace/shipment/job-template/search" },
                    { name: "cw-shipment-job-template-manage", path: "components/portals/company-workspace/shipment/job-template/manage" },
                    { name: "cw-shipment-job-template-manage-vehicle-and-driver-select", path: "components/portals/company-workspace/shipment/job-template/vehicle-and-driver-select" },
                    { name: "cw-shipment-job-template-manage-destination", path: "components/portals/company-workspace/shipment/job-template/destination/destination" },
                    { name: "cw-shipment-job-template-manage-destination-waypoint-order-select", path: "components/portals/company-workspace/shipment/job-template/destination/waypoint-order-select" },
                    { name: "cw-shipment-job-template-manage-destination-order-select", path: "components/portals/company-workspace/shipment/job-template/destination/order-select" },
                    { name: "cw-shipment-job-template-manage-destination-direction", path: "components/portals/company-workspace/shipment/job-template/destination/direction" },
                    { name: "cw-shipment-job-template-manage-destination-direction-routing", path: "components/portals/company-workspace/shipment/job-template/destination/direction-routing" },
                    { name: "cw-shipment-job-template-manage-alert-select", path: "components/portals/company-workspace/shipment/job-template/alert-select" },
                    { name: "cw-shipment-job-template-view", path: "components/portals/company-workspace/shipment/job-template/view" },
                    // Shipment - Customer
                    { name: "cw-shipment-customer", path: "components/portals/company-workspace/shipment/customer/list" },
                    { name: "cw-shipment-customer-search", path: "components/portals/company-workspace/shipment/customer/search" },
                    { name: "cw-shipment-customer-import", path: "components/portals/company-workspace/shipment/customer/import" },
                    { name: "cw-shipment-customer-manage", path: "components/portals/company-workspace/shipment/customer/manage" },
                    { name: "cw-shipment-customer-manage-bil-to-addr-manage", path: "components/portals/company-workspace/shipment/customer/bill-to-address" },
                    { name: "cw-shipment-customer-manage-ship-to-addr", path: "components/portals/company-workspace/shipment/customer/ship-to-address" },
                    { name: "cw-shipment-customer-manage-ship-to-addr-manage", path: "components/portals/company-workspace/shipment/customer/ship-to-address-manage" },
                    { name: "cw-shipment-customer-manage-ship-to-addr-view", path: "components/portals/company-workspace/shipment/customer/ship-to-address-view" },
                    { name: "cw-shipment-customer-view", path: "components/portals/company-workspace/shipment/customer/view" },
                    // Shipment - Product
                    { name: "cw-shipment-product", path: "components/portals/company-workspace/shipment/product/list" },
                    { name: "cw-shipment-product-search", path: "components/portals/company-workspace/shipment/product/search" },
                    { name: "cw-shipment-product-import", path: "components/portals/company-workspace/shipment/product/import" },
                    { name: "cw-shipment-product-manage", path: "components/portals/company-workspace/shipment/product/manage" },
                    { name: "cw-shipment-product-view", path: "components/portals/company-workspace/shipment/product/view" },
                    // Shipment - Order
                    { name: "cw-shipment-order", path: "components/portals/company-workspace/shipment/order/list" },
                    { name: "cw-shipment-order-search", path: "components/portals/company-workspace/shipment/order/search" },
                    { name: "cw-shipment-order-import", path: "components/portals/company-workspace/shipment/order/import" },
                    { name: "cw-shipment-order-deliver", path: "components/portals/company-workspace/shipment/order/deliver" },
                    { name: "cw-shipment-order-manage", path: "components/portals/company-workspace/shipment/order/manage" },
                    { name: "cw-shipment-order-view", path: "components/portals/company-workspace/shipment/order/view" },
                    // Shipment - POI Trip
                    { name: "cw-shipment-poi-trip", path: "components/portals/company-workspace/shipment/poi-trip/list" },
                    { name: "cw-shipment-poi-trip-search", path: "components/portals/company-workspace/shipment/poi-trip/search" },
                    { name: "cw-shipment-poi-trip-manage", path: "components/portals/company-workspace/shipment/poi-trip/manage" },
                    { name: "cw-shipment-poi-trip-view", path: "components/portals/company-workspace/shipment/poi-trip/view" },
                    // Shipment - Default Value
                    { name: "cw-shipment-default-value", path: "components/portals/company-workspace/shipment/default-value" },
                    //Shipment - Template
                    { name: "cw-shipment-templates-list", path: "components/portals/company-workspace/shipment/templates/list" },
                    { name: "cw-shipment-templates-search", path: "components/portals/company-workspace/shipment/templates/search" },
                    { name: "cw-shipment-templates-manage", path: "components/portals/company-workspace/shipment/templates/manage" },
                    //Shipment - Recurring
                    // { name: "cw-shipment-recurring", path: "components/portals/company-workspace/shipment/recurring/list" },
                    // { name: "cw-shipment-recurring-manage", path: "components/portals/company-workspace/shipment/recurring/manage" },

                    //Shipment - Category
                    { name: "cw-shipment-category", path: "components/portals/company-workspace/shipment/category/list" },
                    { name: "cw-shipment-category-view", path: "components/portals/company-workspace/shipment/category/view" },
                    { name: "cw-shipment-category-manage", path: "components/portals/company-workspace/shipment/category/manage" },
                    { name: "cw-shipment-category-manage-accessible-business-unit-select", path: "components/portals/company-workspace/shipment/category/accessible-business-unit-select" },

                    //Shipment - Zone
                    { name: "cw-shipment-zone", path: "components/portals/company-workspace/shipment/zone/list" },
                    { name: "cw-shipment-zone-manage", path: "components/portals/company-workspace/shipment/zone/manage" },
                    { name: "cw-shipment-zone-view", path: "components/portals/company-workspace/shipment/zone/view" },

                    // Report
                    { name: "cw-report", path: "components/portals/company-workspace/report/menu" },
                    // Report - Viewer (Telerik)
                    { name: "cw-report-reportviewer", path: "components/portals/company-workspace/report/reports" },
                    // Report - General
                    { name: "cw-report-general", path: "components/portals/company-workspace/report/general/form" },
                    { name: "cw-report-general-sentmail-dialog", path: "components/portals/company-workspace/report/general/dialog-sentmail"},
                    //report - standard
                    { name: "cw-report-standard-overspeed", path: "components/portals/company-workspace/report/standard/overspeed/form" },
                    { name: "cw-report-standard-overspeedminute", path: "components/portals/company-workspace/report/standard/overspeedminute/form" },
                    { name: "cw-report-standard-overspeedmonth", path: "components/portals/company-workspace/report/standard/overspeedmonth/form" },
                    { name: "cw-report-standard-parkingengine", path: "components/portals/company-workspace/report/standard/parkingengine/form" },
                    //report - telematics
                    { name: "cw-report-telematics", path: "components/portals/company-workspace/report/telematics/menu" },
                    { name: "cw-report-telematics-sevendays", path: "components/portals/company-workspace/report/telematics/sevendays/form" },
                    { name: "cw-report-telematics-driverperformance", path: "components/portals/company-workspace/report/telematics/driverperformance/form" },
                    { name: "cw-report-telematics-parkingoverview", path: "components/portals/company-workspace/report/telematics/parkingoverview/form" },
                    { name: "cw-report-telematics-detailedfuel", path: "components/portals/company-workspace/report/telematics/detailedfuel/form" },
                    { name: "cw-report-telematics-detailedtrip", path: "components/portals/company-workspace/report/telematics/detailedtrip/form" },
                    { name: "cw-report-telematics-summaryadas", path: "components/portals/company-workspace/report/telematics/summaryadas/form" },
                    // Report - Comparison
                    { name: "cw-report-comparison", path: "components/portals/company-workspace/report/comparison/form" },
                    // Report - Summary
                    { name: "cw-report-summary", path: "components/portals/company-workspace/report/summary/form" },
                    // Report - Delinquent
                    { name: "cw-report-delinquent", path: "components/portals/company-workspace/report/delinquent/form" },
                    // Report - Advance
                    { name: "cw-report-advance", path: "components/portals/company-workspace/report/advance/form" },
                    // Report - FleetTypeLogReport
                    { name: "cw-report-fleettype", path: "components/portals/company-workspace/report/fleettypelog/form" },

                    //KPI Report
                    { name: "cw-report-kpi-search", path: "components/portals/company-workspace/report/kpi/form" },
                    { name: "cw-report-kpi-dashboard-overall", path: "components/portals/company-workspace/report/kpi/dashboard-overall" },
                    { name: "cw-report-kpi-dashboard-bu", path: "components/portals/company-workspace/report/kpi/dashboard-bu" },
                    
                    // Report - Advance DriverPerformance
                    { name: "cw-report-advance-driverperformance", path: "components/portals/company-workspace/report/advance-driverperformance/form" },
                    
                    //Map Functions
                    //cw-map-functions
                    { name: "cw-map-functions-find-nearest-asset", path: "components/portals/company-workspace/map-functions/find-nearest-asset" },
                    { name: "cw-map-functions-find-nearest-asset-result", path: "components/portals/company-workspace/map-functions/nearest-asset-result" },
                    { name: "cw-map-functions-send-to-navigator", path: "components/portals/company-workspace/map-functions/send-to-navigator" },
                    

                    // Dashboard
                    { name: "cw-dashboard", path: "components/portals/company-workspace/dashboard/menu" },

                    // Dashboard - Truck Status
                    { name: "cw-dashboard-truck-status", path: "components/portals/company-workspace/dashboard/truck-status/truck-status-search" },
                    { name: "cw-dashboard-truck-status-dashboard-all", path: "components/portals/company-workspace/dashboard/truck-status/dashboard-overall" },
                    { name: "cw-dashboard-truck-status-dashboard-dept", path: "components/portals/company-workspace/dashboard/truck-status/dashboard-department" },
                    { name: "cw-dashboard-truck-status-details", path: "components/portals/company-workspace/dashboard/truck-status/details" },

                    // Dashboard - Utilization Driving Time
                    { name: "cw-dashboard-utlization-driving-time", path: "components/portals/company-workspace/dashboard/utilization-driving-time/utilization-search" },
                    { name: "cw-dashboard-utilization-driving-time-dashboard", path: "components/portals/company-workspace/dashboard/utilization-driving-time/dashboard" },
                    { name: "cw-dashboard-utilization-driving-time-details", path: "components/portals/company-workspace/dashboard/utilization-driving-time/details" },

                    // Dashboard - Driver Performance Score
                    { name: "cw-dashboard-driver-score", path: "components/portals/company-workspace/dashboard/driver-performance-score/driver-score-search" },
                    { name: "cw-dashboard-driver-score-dashboard", path: "components/portals/company-workspace/dashboard/driver-performance-score/dashboard" },
                    { name: "cw-dashboard-driver-score-details", path: "components/portals/company-workspace/dashboard/driver-performance-score/details" },

                    // Dashboard - Safety Dashboard
                    { name: "cw-dashboard-safety-dashboard-search", path: "components/portals/company-workspace/dashboard/safety-dashboard/safety-dashboard-search" },
                    { name: "cw-dashboard-safety-dashboard-dashboard", path: "components/portals/company-workspace/dashboard/safety-dashboard/dashboard" },
                    { name: "cw-dashboard-safety-dashboard-report", path: "components/portals/company-workspace/dashboard/safety-dashboard/report" },

                    // AUTOMAIL-REPORT  
                    { name: "cw-automail-report-manage", path: "components/portals/company-workspace/automail-report/manage" },
                    { name: "cw-automail-report-manage-list", path: "components/portals/company-workspace/automail-report/list" },
                    { name: "cw-automail-report-manage-create", path: "components/portals/company-workspace/automail-report/manage" },

                    // BOMs - Menu
                    { name: "cw-boms-menu", path: "components/portals/company-workspace/boms/menu" },

                    // BOMs - Real Time Passenger
                    { name: "cw-boms-realtime-passenger-dashboard-list", path: "components/portals/company-workspace/boms/realtime-passenger-dashboard/list" },
                    { name: "cw-boms-realtime-passenger-dashboard-form", path: "components/portals/company-workspace/boms/realtime-passenger-dashboard/form" },
                    { name: "cw-boms-realtime-passenger-dashboard-search", path: "components/portals/company-workspace/boms/realtime-passenger-dashboard/search" },
                    { name: "cw-boms-realtime-passenger-dashboard-search-result", path: "components/portals/company-workspace/boms/realtime-passenger-dashboard/search-result" },
                    { name: "cw-boms-realtime-passenger-dashboard-detail-widget", path: "components/portals/company-workspace/boms/realtime-passenger-dashboard/detail-widget" },

                    // BOMs - Reports
                    { name: "cw-boms-reports", path: "components/portals/company-workspace/boms/reports/menu" },
                    { name: "cw-boms-reports-summary-round-trip-form", path: "components/portals/company-workspace/boms/reports/summary-round-trip/form" },
                    { name: "cw-boms-reports-realtime-passenger-on-board-form", path: "components/portals/company-workspace/boms/reports/realtime-passenger-on-board/form" },
                    { name: "cw-boms-reports-summary-passenger-by-route-form", path: "components/portals/company-workspace/boms/reports/summary-passenger-by-route/form" },
                    { name: "cw-boms-reports-summary-passenger-by-zone-form", path: "components/portals/company-workspace/boms/reports/summary-passenger-by-zone/form" },
                    { name: "cw-boms-reports-bus-usage-statistics-form", path: "components/portals/company-workspace/boms/reports/bus-usage-statistics/form" },
                    { name: "cw-boms-reports-summary-transportation-form", path: "components/portals/company-workspace/boms/reports/summary-transportation/form" },
                    { name: "cw-boms-reports-monthly-summary-transportation-form", path: "components/portals/company-workspace/boms/reports/monthly-summary-transportation/form" },
                    { name: "cw-boms-reports-summary-passenger-by-zone-and-vehicle-type-form", path: "components/portals/company-workspace/boms/reports/summary-passenger-by-zone-and-vehicle-type/form" },

                    //BOMs - DefaultValues
                    { name: "cw-boms-defaultvalues-list", path: "components/portals/company-workspace/boms/boms-defaultvalues/list" },
                    { name: "cw-boms-defaultvalues-view", path: "components/portals/company-workspace/boms/boms-defaultvalues/view" },
                    { name: "cw-boms-defaultvalues-manage", path: "components/portals/company-workspace/boms/boms-defaultvalues/manage" },
                    
                    //SCGL - Test Drive Widget
                    { name: "cw-test-drive-widget", path: "components/portals/company-workspace/test-drive-widget/test-drive-widget" },
                ]
            }
        ],
        bundles: [{
            name: "core",
            components: [
                "gisc-chrome-shell",
                "gisc-chrome-crumbtail",
                "gisc-chrome-map",
                "gisc-chrome-working-map",
                "gisc-chrome-searchbox",
                "gisc-chrome-companyswitcher",
                "gisc-chrome-track-widget-switcher",
                "gisc-chrome-sidebar",
                "gisc-chrome-companylogo",
                "gisc-chrome-usermenu",
                "gisc-chrome-journeymenu",
                "gisc-chrome-notification",
                "gisc-chrome-messagebox",
                "gisc-chrome-contextmenu",
                "gisc-chrome-test-drive-widget-switcher",
                "gisc-chrome-liveview-widget-switcher",

                "gisc-ui-component",
                "gisc-ui-label",
                "gisc-ui-textbox",
                "gisc-ui-textarea",
                "gisc-ui-checkbox",
                "gisc-ui-dropdown",
                "gisc-ui-combobox",
                "gisc-ui-button",
                "gisc-ui-tab",
                "gisc-ui-tooltip",
                "gisc-ui-datatable",
                "gisc-ui-fileupload",
                "gisc-ui-listview",
                "gisc-ui-statusbar",
                "gisc-ui-dialogbox",
                "gisc-ui-datetime",
                "gisc-ui-chart",
                "gisc-ui-markdown",
                "gisc-ui-field",
                "gisc-ui-treeview",
                "gisc-ui-dropdowntree",
                "gisc-ui-colorpicker",
                "gisc-ui-image-combobox",
                "gisc-ui-chartbar",
                "gisc-ui-reportviewer",
                "gisc-ui-barchart",
                "gisc-ui-stackchart",
                "gisc-ui-datagrid",
                "gisc-ui-editor",
                "gisc-ui-radio",
                "gisc-ui-window",
                "gisc-ui-dropdown-multiselect",
                "gisc-ui-autocomplete",
                "gisc-ui-quick-search",
                 
                "gisc-ui-piechart",
                "gisc-ui-calendar",

                "gisc-ui-timelinechart",
                "gisc-ui-donutchart",
                "gisc-ui-baramchart",
                "gisc-ui-barstackamchart",
                "gisc-ui-linechart",
                "gisc-ui-radarchart",
                "gisc-ui-barhorizontalamchart",

                "gisc-ui-routedragdrop",

                "gisc-ui-shipment-grid",
                "gisc-ui-shipment-timeline",
                // "gisc-ui-shipment-recurring-grid",

                "gisc-ui-gallery",
                "gisc-ui-video",
                "gisc-ui-flatpickr",
                // "gisc-ui-playback-analysis-tool",
                "gisc-ui-shipment-grid-confirm-dialog",
                "gisc-ui-playback-analysis-tool",
                "gisc-ui-timelinechart-v4",

                // SVG components.
                "svg-nav-block",
                "svg-nav-bar",
                "svg-splash-logo",

                "svg-cmd-add",
                "svg-cmd-edit",
                "svg-cmd-refresh",
                "svg-cmd-delete",
                "svg-cmd-search",
                "svg-cmd-reset-password",
                "svg-cmd-import",
                "svg-cmd-export",
                "svg-cmd-download-template",
                "svg-cmd-languages",
                "svg-cmd-assign",
                "svg-cmd-return",
                "svg-cmd-clear",
                "svg-cmd-contract",
                "svg-cmd-maintenance",
                "svg-cmd-maintenance-cancel",
                "svg-cmd-maintenance-complete",
                "svg-cmd-odometer",
                "svg-cmd-import-picture",
                "svg-cmd-view-all",
                "svg-cmd-clear-all",
                "svg-cmd-follow-all",
                "svg-cmd-clear-follow",

                "svg-action-close",
                "svg-action-maximize",
                "svg-action-minimize",
                "svg-action-pin",
                "svg-action-restore",
                "svg-journey",
                "svg-bell",
                "svg-warning",
                "svg-error",
                "svg-crumb-divider",
                "svg-info",
                "svg-noti-alert",
                "svg-noti-announcement",
                "svg-noti-maintenance",
                "svg-noti-urgent",
                "svg-company-switcher",
                "svg-track-widget-switcher",
                "svg-test-drive-widget-switcher",
                "svg-control-checkbox-checked",
                "svg-blade-close",
                "svg-blade-collapse",
                "svg-blade-maximize",
                "svg-blade-restoredown",
                "svg-noti-delete",
                "svg-noti-view",
                "svg-noti-view-disable",
                "svg-ic-setting",
                "svg-ic-gis",
                "svg-ic-asterisk",
                "svg-ic-info",
                "svg-ic-statusbar",
                "svg-ic-statusbar-fail",
                "svg-ic-checked",
                "svg-ic-calendar",
                "svg-ic-right-arrow",
                "svg-menu-hamburger",
                "svg-menu-driver-performance",
                "svg-menu-geofencing",
                "svg-menu-map",
                "svg-menu-monitoring",
                "svg-menu-search",
                "svg-menu-shipment",
                "svg-menu-dashboard",
                "svg-menu-home",
                "svg-menu-subscriptions",
                "svg-menu-companies",
                "svg-menu-users",
                "svg-menu-assets",
                "svg-menu-announcements",
                "svg-menu-configurations",
                "svg-menu-reports",
                "svg-menu-groups",
                "svg-menu-businessunits",
                "svg-menu-boms",

                "svg-icon-custompoi-pinclickmap",
                "svg-icon-customroute-clickonmap",
                "svg-icon-customroute-subarea-clickonmap", 
                "svg-icon-customroute-subarea-clear",
                "svg-icon-customroute-createcustomarea",
                "svg-icon-customroute-createcustomroute",
                "svg-icon-customroute-movedown",
                "svg-icon-customroute-moveup",
                "svg-icon-customroute-print",
                "svg-icon-customroute-reverse",
                "svg-icon-customroute-selectpoi",
                "svg-icon-customroute-viewonmap",
                "svg-icon-maximize-viewmap",
                "svg-icon-report",
                "svg-icon-auto-mail-report",
                "svg-icon-report-telematics",
                "svg-icon-context-menu",
                "svg-cat-logis-bussinessunit",
                "svg-cat-logis-customarea",
                "svg-cat-logis-custompoi",
                "svg-cat-logis-driver",
                "svg-cat-logis-route",
                "svg-cat-logis-user",
                "svg-cat-logis-vehicle",
                "svg-cat-etc",
                "svg-cat-map-accommodation",
                "svg-cat-map-adminpoly",
                "svg-cat-map-association",
                "svg-cat-map-atm",
                "svg-cat-map-attraction",
                "svg-cat-map-autoservice",
                "svg-cat-map-bakerycoffee",
                "svg-cat-map-bank",
                "svg-cat-map-bussiness",
                "svg-cat-map-carcare",
                "svg-cat-map-carpark",
                "svg-cat-map-education",
                "svg-cat-map-fastfood",
                "svg-cat-map-food",
                "svg-cat-map-foodshop",
                "svg-cat-map-goverment",
                "svg-cat-map-health",
                "svg-cat-map-hospital",
                "svg-cat-map-hostel",
                "svg-cat-map-movie",
                "svg-cat-map-police",
                "svg-cat-map-publounge",
                "svg-cat-map-road",
                "svg-cat-map-transport",
                "svg-cat-map-conveniencestore",
                "svg-cat-map-departmentstore",
                "svg-cat-map-lpg",
                "svg-cat-map-market",
                "svg-cat-map-masstransit",
                "svg-cat-map-ngv",
                "svg-cat-map-oilstation",
                "svg-cat-map-other",
                "svg-cat-map-pharmacy",
                "svg-cat-map-postoffice",
                "svg-cat-map-recreations",
                "svg-cat-map-religion",
                "svg-cat-map-restaurant",
                "svg-cat-map-stores",
                "svg-cat-map-vehiclecharging",

                "svg-ic-mt-appo-manage", 
                "svg-ic-mt-maintenancesplans",
                "svg-ic-mt-maintenancestypes",
                "svg-ic-mt-report",
                "svg-ic-table-appointment",
                "svg-ic-table-notappointment",
                "svg-ic-va-postpone",
                "svg-ic-vmp-appointments",
                "svg-ic-vmt-mainte-subtype",
                "svg-ic-vmt-obsolate",

                "svg-adminmode-normal",
                "svg-adminmode-press",
                "svg-workspacemode-normal",
                "svg-workspacemode-over",
                "svg-download-xls",
                "svg-ticket-create",
                "svg-ticket-create-appointment",
                "svg-ticket-editticket",
                "svg-ticket-sentmail",
                "menu-ticket",
                "svg-icon-vendor",
                "svg-icon-transportation",
                "svg-ticket-edit-ticket",
                
                "svg-icon-route-mode-car",
                "svg-icon-route-mode-motocycle",
                "svg-icon-route-mode-truck",
                "svg-icon-route-mode-walk",

                "svg-icon-pin-do",
                "svg-icon-utili-driver",
                "svg-icon-utili-vehicle",
                "svg-icon-vdo",
                "svg-icon-start-plugin",
                "svg-icon-load-plugin",
                "svg-icon-load-guide",
                "svg-icon-btn-back",
                "svg-icon-btn-next",
                "svg-icon-btn-zoom-in",
                "svg-icon-btn-zoom-out",
                "svg-icon-vehicle",
                "svg-icon-update-contract",
                "svg-icon-vehicle-over35",
                "svg-icon-vehicle-over4",
                "svg-icon-best-sequence",
                "svg-icon-list-video",
                "svg-icon-live-view",
                "svg-icon-notification-sound"
                
            ]
        }, {
            name: "gisc-dev",
            components: [
                "developerHome",
                "labelPage",
                "textBoxPage",
                "textAreaPage",
                "dropdownPage",
                "buttonPage",
                "tabPage",
                "datatableIndex",
                "datatableReadme",
                "datatableSimple",
                "datatableAdvance",
                "datatableEndPoint",
                "datatableOthers",
                "datatableStyle",
                "datatableRowSelector",
                "datatableAPI",
                "datatableEXP",
                "datatableLoadMore",
                "datatableServerside",
                "fileUploadPage",
                "tooltipPage",
                "checkBoxPage",
                "dateTimepickerPage",
                "chartPage",
                "chartEndPoint",
                "comboboxPage",
                "chartBarPage",
                "mapIndex",
                "mapDrawPin",
                "mapClear",
                "mapDrawOnePointZoom",
                "mapDrawOneLineZoom",
                "mapDrawOnePolygonZoom",
                "mapDrawMultiplePoint",
                "mapDrawMultiplePolygonZoom",
                "mapDrawMultpleLineZoom",
                "mapDrawMultipleVehicle",
                "mapDrawOneRoute",
                "mapDrawPointBuffer",
                "mapDrawRouteBuffer",
                "mapDrawCustomArea",
                "mapZoomPoint",
                "mapTrackVehicles",
                "mapEnableClickMap",
                "mapEnableDrawPolygon",
                "mapPlaybackAnimate",
                "mapPlaybackFootprint",
                "mapPlaybackFully",

                "screenDevIndex",
                "screenDevReadme",
                "screenDevTrackChange",
                "screenDevValidation",
                "screenDevValidationDemo",
                "screenDevI18N",
                "screenDevGulp",
                "screenDevFAQ",
                "colorPickerPage",
                "imageComboboxPage",
                "treeViewPage",
                "dropdownTreePage",
                "messageboxHelpPage",
                "reportViewerDemo",

                "serviceTestingIndex",
                "serviceTestingShipment"
            ]
        }, {
            name: "gisc-bo",
            components: [
                // Select Company
                "bo-company-select",
                // Shared
                "bo-shared-accessible-company-select",
                "bo-shared-accessible-users-select",
                "bo-shared-accessible-inverse-features-select",
                // Subscription and Group
                "bo-subscription-and-group",
                "bo-subscription-and-group-subscription",
                "bo-subscription-and-group-subscription-manage",
                "bo-subscription-and-group-group",
                "bo-subscription-and-group-group-manage",
                "bo-subscription-and-group-technician-team",
                "bo-subscription-and-group-technician-team-manage",
                // Company
                "bo-company",
                "bo-company-manage",
                "bo-company-manage-subscription",
                "bo-company-manage-subscription-manage",
                "bo-company-manage-contact-manage",
                "bo-company-manage-regional-manage",
                "bo-company-manage-setting-manage",
                "bo-company-manage-mapservices-manage",
                "bo-company-manage-pairable-vehicle",
                // User
                "bo-user",
                "bo-user-manage",
                // Asset
                "bo-asset",
                // Asset - Box Feature
                "bo-asset-box-feature",
                "bo-asset-box-feature-manage",
                // Asset - Box Model
                "bo-asset-box-model",
                "bo-asset-box-model-manage",
                // Asset - Box Template
                "bo-asset-box-template",
                "bo-asset-box-template-manage",
                // Asset - Box Stock
                "bo-asset-box-stock",
                "bo-asset-box-stock-manage",
                // Asset - Box
                "bo-asset-box",
                "bo-asset-box-search",
                "bo-asset-box-manage",
                "bo-asset-box-manage-contract",
                "bo-asset-box-manage-maintenance",
                "bo-asset-box-manage-maintenance-manage",
                "bo-asset-box-import",
                "bo-asset-box-assign",
                "bo-asset-box-return",
                
                // Asset - Vehicle Model
                "bo-asset-vehicle-model",
                "bo-asset-vehicle-model-manage",
                
                 // Asset - MDVR Models
                 "bo-asset-mdvr-models",
                 "bo-asset-mdvr-models-manage",

                 // Asset - MDVRs
                 "bo-asset-mdvr-menu",
                 "bo-asset-mdvrs",
                 "bo-asset-mdvrs-manage-two",
                 "bo-asset-mdvrs-manage",
                 "bo-asset-mdvrs-update-multiple",
                 "bo-asset-mdvrs-search",
                 "bo-asset-mdvrs-import",
                 
                // Asset - MDVR Config
                "bo-asset-mdvrs-config",
                "bo-asset-mdvrs-config-update",
                "bo-asset-mdvrs-config-manage",

                // Asset - DMS Model
                "bo-asset-dms-model-manage",
                "bo-asset-dms-model",

                //Asset - DMS Config
                "bo-asset-dms-config",
                "bo-asset-dms-config-manage",
                "bo-asset-dms-config-config-key",

                // Asset - DMS
                "bo-asset-dms",
                "bo-asset-dms-device",
                "bo-asset-dms-manage",
                "bo-asset-dms-manage-confirm-dialog",
                
                // Asset - ADAS Model
                "bo-asset-adas-model",
                "bo-asset-adas-model-manage",

                // Asset - ADAS
                "bo-asset-adas",
                "bo-asset-adas-device",
                "bo-asset-adas-manage",
                "bo-asset-adas-manage-confirm-dialog",

                // Announcement
                "bo-announcement",
                "bo-announcement-search",
                "bo-announcement-manage",
                "bo-announcement-manage-message-manage",
                // Configuration
                "bo-configuration",
                // Configuration - Translation
                "bo-configuration-translation",
                "bo-configuration-translation-import",
                "bo-configuration-translation-export",
                "bo-configuration-translation-language",
                "bo-configuration-translation-language-add",
                // Configuration - POI Icon
                "bo-configuration-poi-icon",
                "bo-configuration-poi-icon-manage",
                // Configuration - Vehicle Icon
                "bo-configuration-vehicle-icon",
                "bo-configuration-vehicle-icon-manage",
                // Configuration - System Setting
                "bo-configuration-system-setting",
                "bo-configuration-system-setting-manage",
                //Configuration - Tracking Databases tracking-databases
                "bo-configuration-tracking-databases",
                "bo-configuration-tracking-databases-manage",
                //Configuration - Auto-mail
                "bo-configuration-auto-mail",
                "bo-configuration-sql-template-config",
                "bo-automail-sql-template-manage",
                "bo-configuration-mail-config",
                "bo-automail-mail-config-manage",
                "bo-automail-mail-config-view",
                "bo-configuration-sending-log",
                "bo-configuration-sending-log-manage",
                // Configurations-Map Service
                "bo-configuration-map-service",
                "bo-configuration-map-service-manage",
                // Configurations-nostra-configurations
                "bo-configuration-nostra-configurations",
                "bo-configuration-nostra-configurations-manage",
                //Configurations-GIS-Service-configurations
                "bo-configuration-gis-service-configurations",
                "bo-configuration-gis-service-configurations-manage",

                // Report
                "bo-report",
                // Report - Email Logs
                "bo-report-email-log",
                // Report - Company Expiration
                "bo-report-company-expiration",
                "bo-report-company-expiration-search",
                // Report - Contract Expiration
                "bo-report-contract-expiration",
                "bo-report-contract-expiration-search",
                "bo-report-contract-expiration-fleet-service",
                "bo-report-contract-expiration-mobile-service",
                "bo-report-contract-expiration-box-maintenance",
                // Report - DLT Report
                "bo-report-dlt-report-search",
                "bo-report-dlt-report-list",
                // Report - DLT New Installation
                "bo-report-dlt-new-installation-search",
                "bo-report-dlt-new-installation-list",
                // Report - DLT Next Year
                "bo-report-dlt-next-year-search",
                 "bo-report-dlt-next-year-list",
                // Report - Letter
                "bo-report-letter-search",
                 "bo-report-letter-list",

                "bo-ticket-manage",
                "bo-ticket-manage-asset",
                "bo-ticket-manage-asset-vehicle",
                "bo-ticket-manage-asset-find",
                "bo-ticket-manage-asset-create",
                "bo-ticket-manage-asset-update",
                "bo-ticket-manage-asset-history",
                "bo-ticket-manage-asset-details",
                "bo-ticket-manage-asset-vehicle-info",
                "bo-ticket-manage-asset-detail-vehicle-info",
                "bo-ticket-manage-detail-close",
                "bo-ticket-manage-asset-batch-action",

                "bo-ticket-dashboard",
                "bo-ticket-searchDashboard",

                "bo-ticket-manage-create",
                "bo-ticket-manage-edit",
                "bo-ticket-manage-edit-appointment",
                "bo-ticket-manage-view-appointment",
                "bo-ticket-manage-sendmail",
                "bo-ticket-manage-fleet-service",
                "bo-ticket-manage-detail-fleet-service",
                
                "bo-ticket-manage-ticket-new",
                "bo-ticket-manage-ticket-ack",
                "bo-ticket-manage-ticket-inprog",
                "bo-ticket-manage-ticket-confirm",
                "bo-ticket-manage-ticket-pending",
                "bo-ticket-manage-ticket-follow",
                "bo-ticket-manage-ticket-close",
                "bo-ticket-manage-ticket-view-error",

                "bo-ticket-manage-installation",

                "bo-ticket-manage-appointments",
                "bo-ticket-manage-appointments-view",

                "bo-ticket-manage-mail",
                "bo-ticket-manage-mail-manage",

                "bo-ticket-manage-sim",
                "bo-ticket-manage-sim-search",
                "bo-ticket-manage-sim-view",
                "bo-ticket-manage-sim-manage",
                "bo-ticket-manage-sim-import",

                "bo-ticket-manage-export-with-criteria",

                // Ticket Sim Stock
                "bo-ticket-manage-sim-stock",
                "bo-ticket-manage-sim-stock-manage",

                // Ticket - Operator Package
                "bo-ticket-manage-package",
                "bo-ticket-manage-package-manage",

                // Ticket - Vendors
                "bo-ticket-manage-vendor",
                "bo-ticket-manage-vendor-manage",
                "bo-ticket-manage-vendor-manage-signature",

                // Ticket - Transporter
                "bo-ticket-manage-transporter",
                "bo-ticket-manage-transporter-manage",
                "bo-ticket-manage-transporter-manage-vehicle",
                "bo-ticket-manage-transporter-manage-find-vehicle",

                //Ticket - Search
                "bo-ticket-search",
                "bo-ticket-manage-search",


                //About
                "bo-about",
            ]
        },
        {
            name: "gisc-cs", // Shared between company admin and company workspace
            components: [
                "ca-user-view",
                "ca-business-unit-view",
                "ca-asset-vehicle-view",
                "ca-asset-driver-view"
            ]
        },
        {
            name: "gisc-ca",
            components: [
                // Contract
                "ca-contract",
                "ca-contract-search",
                "ca-contract-view",
                "ca-contract-manage",
                // Business Unit
                "ca-business-unit-menu",
                "ca-business-unit",
                "ca-business-units-hierarchy-manage",
                "ca-business-units-levels",
                "ca-business-units-levels-manage",
                "ca-business-units-authorized-persons",

                // Group
                "ca-group",
                "ca-group-view",
                "ca-group-manage",
                // User
                "ca-user",
                "ca-user-search",
                "ca-user-manage",
                "ca-user-manage-group-select",
                "ca-user-manage-accessible-business-unit-select",
                "ca-user-reset-password",
                // Asset
                "ca-asset",
                // Asset - Box
                "ca-asset-box",
                "ca-asset-box-search",
                "ca-asset-box-view",
                "ca-asset-box-view-contract",
                "ca-asset-box-view-maintenance",
                "ca-asset-box-view-maintenance-manage",
                "ca-asset-box-view-maintenance-view",
                // Asset - Vehicle
                "ca-asset-vehicle",
                "ca-asset-vehicle-search",
                "ca-asset-vehicle-manage",
                "ca-asset-vehicle-manage-license-detail-manage",
                "ca-asset-vehicle-manage-default-driver-manage",
                "ca-asset-vehicle-manage-alert-manage",
                "ca-asset-vehicle-import",
                "ca-asset-vehicle-view-maintenance-plan",
                "ca-asset-vehicle-view-maintenance-plan-manage",
                "ca-asset-vehicle-view-maintenance-plan-view",
                "ca-asset-vehicle-view-maintenance-plan-complete",
                "ca-asset-vehicle-view-maintenance-plan-cancel",
                "ca-asset-vehicle-view-maintenance-plan-view-cancel",
                "ca-asset-vehicle-view-maintenance-plan-view-log",
                "ca-asset-vehicle-view-maintenance-plan-create-log",

                "ca-asset-vehicle-view-odo-meter-manage",
                "ca-asset-vehicle-view-contract",

                "ca-asset-vehicle-dms-detail",
                "ca-asset-vehicle-adas-detail",
                "ca-asset-vehicle-view-odo-meter-fixing",

                "ca-asset-vehicle-view-confirm-dialog-changeBu",

                // Asset - Driver
                "ca-asset-driver",
                "ca-asset-driver-search",
                "ca-asset-driver-manage",
                "ca-asset-driver-import",
                "ca-asset-driver-import-picture",
                "ca-asset-driver-card",
                // Asset - Fleet Service
                "ca-asset-fleet-service",
                "ca-asset-fleet-service-search",
                "ca-asset-fleet-service-manage",
                "ca-asset-fleet-service-view",
                // Asset - Mobile Service
                "ca-asset-mobile-service",
                "ca-asset-mobile-service-search",
                "ca-asset-mobile-service-manage",
                "ca-asset-mobile-service-view",
                // Asset - SMS Service
                "ca-asset-sms-service",
                "ca-asset-sms-service-search",
                "ca-asset-sms-service-manage",
                "ca-asset-sms-service-view",
                // Asset - Trainer
                "ca-asset-trainer",
                "ca-asset-trainer-search",
                "ca-asset-trainer-view",
                "ca-asset-trainer-manage",
                "ca-asset-trainer-import",
                "ca-asset-trainer-import-picture",

                // Driver Performance Rule
                "ca-driver-performance-rule-menu",
                "ca-driver-performance-rule",
                "ca-driver-performance-rule-search",
                "ca-driver-performance-rule-manage",
                "ca-driver-performance-rule-view",

                // Advance Driver Performance Rule
                "ca-advance-driver-performance-rule",
                "ca-advance-driver-performance-rule-manage",
                "ca-advance-driver-performance-rule-accessible-bu-select",
                "ca-advance-driver-performance-rule-speed",
                "ca-advance-driver-performance-rule-behavior",
                "ca-advance-driver-performance-rule-fuel",
                "ca-advance-driver-performance-rule-view",

                // Configuration
                "ca-configuration",
                // Configuration - Announcement
                "ca-configuration-announcement",
                "ca-configuration-announcement-search",
                "ca-configuration-announcement-manage",
                "ca-configuration-announcement-manage-message-manage",
                "ca-configuration-accessible-bu-select",
                "ca-configuration-announcement-view",
                // Configuration - Alert
                "ca-configuration-alert",
                "ca-configuration-alert-search",
                "ca-configuration-alert-manage",
                "ca-configuration-alert-manage-asset-select",
                "ca-configuration-alert-manage-areasetting-select",
                "ca-configuration-alert-view",
                "ca-configuration-alert-manage-datetimesetting-select",
                "ca-configuration-alert-manage-allow",
                "ca-configuration-alert-manage-not-allow",
                "ca-configuration-alert-manage-stop-overtime-province",
                "ca-configuration-alert-manage-find-asset",
                "ca-configuration-alert-manage-asset-businessunit",
                "ca-configuration-alert-manage-asset-result",
                // Configuration - Alert Categorise
                "ca-configuration-alert-categories",
                "ca-configuration-alert-categories-manage",
                "ca-configuration-alert-categories-view",
                "ca-configuration-alert-categories-search",
                // Configuration - POI Icon
                "ca-configuration-poi-icon",
                "ca-configuration-poi-icon-manage",
                "ca-configuration-poi-icon-view",
                // Configuration - Translation
                "ca-configuration-translation",
                "ca-configuration-translation-import",
                "ca-configuration-translation-export",
                // Configuration - Barrier
                "ca-configuration-barrier",
                "ca-configuration-barrier-manage",
                "ca-configuration-barrier-view",
                // Configuration - Ticket Response
                "ca-configuration-ticket-response",
                "ca-configuration-ticket-response-manage",
                "ca-configuration-ticket-response-view",
                // Configuration - Telematices Configuration
                "ca-configuration-telematices-configuration",

                "ca-asset-maintenance-management",
                   
                "ca-asset-maintenance-management-ma-plan",
                "ca-asset-maintenance-management-ma-plan-search",
                "ca-asset-maintenance-management-ma-plan-create",
                "ca-asset-maintenance-management-ma-plan-create-vehicle",
                "ca-asset-maintenance-management-ma-plan-import",
                "ca-asset-maintenance-management-ma-plan-export",
                   
                "ca-asset-maintenance-management-ma-type",
                "ca-asset-maintenance-management-ma-type-manage",
                "ca-asset-maintenance-management-ma-type-manage-import",
                "ca-asset-maintenance-management-ma-type-view",
                "ca-asset-maintenance-management-ma-type-view-subtype",
                "ca-asset-maintenance-management-ma-type-view-subtype-view",
                "ca-asset-maintenance-management-ma-type-view-subtype-manage",
                   
                "ca-asset-maintenance-management-appointment",
                "ca-asset-maintenance-management-appointment-search",
                "ca-asset-maintenance-management-appointment-manage",
                "ca-asset-maintenance-management-appointment-view",
                "ca-asset-maintenance-management-appointment-view-postpone",
                //"ca-asset-maintenance-management-appointment-view-cancel",

                "ca-asset-maintenance-management-report",
                "ca-asset-maintenance-management-report-reportviewer",
                "ca-asset-maintenance-management-appointment-view-cancel",
                



                // Report
                "ca-report",
                // Report - Viewer (Telerik)
                "ca-report-reportviewer",
                // Report - userlog
                "ca-report-userlog",
            ]
        },
        {
            name: "gisc-cw",
            components: [
                // Notification
                "cw-notification-alert",
                "cw-notification-urgent-alert",
                "cw-notification-announcement",
                "cw-notification-maintenance-plan",
                // User Preference
                "cw-user-preference",
                // Global Search
                "cw-global-search",
                "cw-global-search-find",
                "cw-global-search-result",
                // Fleet Monitoring
                "cw-fleet-monitoring",
                "cw-fleet-monitoring-widget",
                // Fleet Monitoring - Track Monitoring
                "cw-fleet-monitoring-track",
                "cw-fleet-monitoring-track-asset-find",
                "cw-fleet-monitoring-track-asset-find-result",
                // Fleet Monitoring - Alert Monitoring
                "cw-fleet-monitoring-alert",
                "cw-fleet-monitoring-alert-list",
                "cw-fleet-monitoring-alert-list-search",
                // Fleet Monitoring - Alert Monitoring - Ticket
                "cw-fleet-monitoring-alert-ticket",
                "cw-fleet-monitoring-alert-ticket-search",
                "cw-fleet-monitoring-alert-ticket-action",
                // Fleet Monitoring - Playback
                "cw-fleet-monitoring-playback",
                "cw-fleet-monitoring-playback-timeline",
                "cw-fleet-monotoring-playback-info-widget",
                
                
                // Fleet Monitoring - Live View
                "cw-fleet-monitoring-liveview",
                "cw-fleet-monitoring-liveview-widget",
                
                // Fleet Monitoring - Event View
                "cw-fleet-monitoring-eventview",
                "cw-fleet-monitoring-eventview-list",
                "cw-fleet-monitoring-eventview-widget",
                // Fleet Monitoring - Event View - Ticket
                "cw-fleet-monitoring-eventview-ticket",
                "cw-fleet-monitoring-eventview-ticket-search",
                "cw-fleet-monitoring-eventview-ticket-action",
                // Fleet Monitoring - Common widget
                "cw-fleet-monitoring-common-video-widget",
                "cw-fleet-monitoring-common-picture-widget",
                "cw-fleet-monitoring-common-vehicle-status-widget",
                // Fleet Monitoring - Playback Analysis Tools
                "cw-fleet-monitoring-playback-analysis-tool-search",
                "cw-fleet-monitoring-playback-analysis-tool",

                //Debrief
                "cw-fleet-monitoring-debrief",
                "cw-fleet-monitoring-debrief-view",
                // Fleet Monitoring - Trip Analysis
                "cw-fleet-monitoring-tripanalysis",
                "cw-fleet-monitoring-tripanalysis-report",

                // Geo-Fencing
                "cw-geo-fencing",
                "cw-geo-fencing-vehicle-info-widget",
                // Geo-Fencing - Custom POI
                "cw-geo-fencing-custom-poi",
                "cw-geo-fencing-custom-poi-search",
                "cw-geo-fencing-custom-poi-import",
                "cw-geo-fencing-custom-poi-manage",
                "cw-geo-fencing-custom-poi-manage-accessible-business-unit-select",
                "cw-geo-fencing-custom-poi-manage-sub-poi-area",
                "cw-geo-fencing-custom-poi-view",
                "cw-geo-fencing-custom-poi-view-verify",
                "cw-geo-fencing-custom-poi-sub-area-view",
                // Geo-Fencing - Custom POI Category
                "cw-geo-fencing-custom-poi-category",
                "cw-geo-fencing-custom-poi-category-manage",
                "cw-geo-fencing-custom-poi-category-view",
                // Geo-Fencing - Custom Route
                "cw-geo-fencing-custom-route",
                "cw-geo-fencing-custom-route-search",
                "cw-geo-fencing-custom-route-manage",
                "cw-geo-fencing-custom-route-manage-accessible-business-unit-select",
                "cw-geo-fencing-custom-route-manage-custom-poi-select",
                "cw-geo-fencing-custom-route-manage-custom-poi-detail",
                "cw-geo-fencing-custom-route-manage-custom-area-create",
                "cw-geo-fencing-custom-route-manage-setting",
                "cw-geo-fencing-custom-route-view",
                // Geo-Fencing - Custom Route Category
                "cw-geo-fencing-custom-route-category",
                "cw-geo-fencing-custom-route-category-manage",
                "cw-geo-fencing-custom-route-category-view",
                // Geo-Fencing - Custom POI Suggestion
                "cw-geo-fencing-custom-poi-suggestion",
                "cw-geo-fencing-custom-poi-suggestion-create",
                "cw-geo-fencing-custom-poi-suggestion-create-accessible-business-unit-select",
                // Geo-Fencing - Custom Area
                "cw-geo-fencing-custom-area",
                "cw-geo-fencing-custom-area-search",
                "cw-geo-fencing-custom-area-manage",
                "cw-geo-fencing-custom-area-manage-accessible-business-unit-select",
                "cw-geo-fencing-custom-area-view",
                // Geo-Fencing - Custom Area Category
                "cw-geo-fencing-custom-area-category",
                "cw-geo-fencing-custom-area-category-manage",
                "cw-geo-fencing-custom-area-category-view",
                // Geo-Fencing - Route From Here
                "cw-geo-fencing-routes-from-here",
                // Geo-Fencing - Working Area
                "cw-geo-working-area",
                
                // Shipment
                "cw-shipment",

                // Shipment - List Table
                "cw-shipment-list",
                "cw-shipment-list-search",
                "cw-shipment-manage",
                "cw-shipment-import",
                "cw-shipment-import-with-template",
                "cw-shipment-finish",
                "cw-shipment-cancel",
                "cw-shipment-list-do-manage",
                "cw-shipment-list-do-waypoint",

                // Shipment - Summary
                "cw-shipment-summary",
                // Shipment - Dashboard
                "cw-shipment-dashboard",
                "cw-shipment-dashboard-search",
                // Shipment - PO Search
                "cw-shipment-posearch",
                "cw-shipment-polist-search",
                // Shipment - Timeline
                "cw-shipment-timeline",

                // Shipment - Templates
                "cw-shipment-templates-list",
                "cw-shipment-templates-search",
                "cw-shipment-templates-manage",

                // Shipment - Recurring
                // "cw-shipment-recurring",
                // "cw-shipment-recurring-manage",

                // Shipment - Job
                "cw-shipment-job",
                "cw-shipment-job-search",
                "cw-shipment-job-import",
                "cw-shipment-job-import-preview",
                "cw-shipment-job-template-select",
                "cw-shipment-job-manage",
                "cw-shipment-job-manage-vehicle-and-driver-select",
                "cw-shipment-job-manage-destination",
                "cw-shipment-job-manage-destination-waypoint-order-select",
                "cw-shipment-job-manage-destination-order-select",
                "cw-shipment-job-manage-destination-direction",
                "cw-shipment-job-manage-destination-direction-routing",
                "cw-shipment-job-manage-alert-select",
                "cw-shipment-job-view",
                "cw-shipment-job-cancel",
                // Shipment - Job Template
                "cw-shipment-job-template",
                "cw-shipment-job-template-search",
                "cw-shipment-job-template-manage",
                "cw-shipment-job-template-manage-vehicle-and-driver-select",
                "cw-shipment-job-template-manage-destination",
                "cw-shipment-job-template-manage-destination-waypoint-order-select",
                "cw-shipment-job-template-manage-destination-order-select",
                "cw-shipment-job-template-manage-destination-direction",
                "cw-shipment-job-template-manage-destination-direction-routing",
                "cw-shipment-job-template-manage-alert-select",
                "cw-shipment-job-template-view",
                // Shipment - Customer
                "cw-shipment-customer",
                "cw-shipment-customer-search",
                "cw-shipment-customer-import",
                "cw-shipment-customer-manage",
                "cw-shipment-customer-manage-bil-to-addr-manage",
                "cw-shipment-customer-manage-ship-to-addr",
                "cw-shipment-customer-manage-ship-to-addr-manage",
                "cw-shipment-customer-manage-ship-to-addr-view",
                "cw-shipment-customer-view",
                // Shipment - Product
                "cw-shipment-product",
                "cw-shipment-product-search",
                "cw-shipment-product-import",
                "cw-shipment-product-manage",
                "cw-shipment-product-view",
                // Shipment - Order
                "cw-shipment-order",
                "cw-shipment-order-search",
                "cw-shipment-order-import",
                "cw-shipment-order-deliver",
                "cw-shipment-order-manage",
                "cw-shipment-order-view",
                // Shipment - POI Trip
                "cw-shipment-poi-trip",
                "cw-shipment-poi-trip-search",
                "cw-shipment-poi-trip-manage",
                "cw-shipment-poi-trip-view",

                // Shipment - Default Value
                "cw-shipment-default-value",

                //Shipment - Category
                "cw-shipment-category",
                "cw-shipment-category-view",
                "cw-shipment-category-manage",
                "cw-shipment-category-manage-accessible-business-unit-select",

                //Shipment Zone
                "cw-shipment-zone",
                "cw-shipment-zone-manage",
                "cw-shipment-zone-view",

                // Report
                "cw-report",
                // Report - Viewer (Telerik)
                "cw-report-reportviewer",
                // Report - General
                "cw-report-general",
                "cw-report-general-sentmail-dialog",
                //Report -standard
                "cw-report-standard-overspeed",
                "cw-report-standard-overspeedminute",
                "cw-report-standard-overspeedmonth",
                "cw-report-standard-parkingengine",
                //Report -standard
                'cw-report-telematics',
                "cw-report-telematics-sevendays",
                "cw-report-telematics-driverperformance",
                "cw-report-telematics-parkingoverview",
                "cw-report-telematics-detailedfuel",
                "cw-report-telematics-detailedtrip",
                "cw-report-telematics-summaryadas",
                // Report - Comparison
                "cw-report-comparison",
                // Report - Summary
                "cw-report-summary",
                // Report - Delinquent
                "cw-report-delinquent",
                //Report - Advance
                "cw-report-advance",
                //Report - FleetTypeLog
                "cw-report-fleettype",

                //Report - KPI
                "cw-report-kpi-search",
                "cw-report-kpi-dashboard-overall",
                "cw-report-kpi-dashboard-bu",

                //Advance Driverperformance Report
                "cw-report-advance-driverperformance",

                //Map Functions
                "cw-map-functions-find-nearest-asset",
                "cw-map-functions-find-nearest-asset-result",
                "cw-map-functions-send-to-navigator",
                //Dashboard
                "cw-dashboard",

                //Dashboard - Truck Status
                "cw-dashboard-truck-status",
                "cw-dashboard-truck-status-dashboard-all",
                "cw-dashboard-truck-status-dashboard-dept",
                "cw-dashboard-truck-status-details",

                //Dashboard - Utilization Driving Time (Search)
                "cw-dashboard-utlization-driving-time",
                "cw-dashboard-utilization-driving-time-dashboard",
                "cw-dashboard-utilization-driving-time-details",

                //Dashboard - Driver Performance Score
                "cw-dashboard-driver-score",
                "cw-dashboard-driver-score-dashboard",
                "cw-dashboard-driver-score-details",

                //Dashboard - Safety Report
                "cw-dashboard-safety-dashboard-search",
                "cw-dashboard-safety-dashboard-dashboard",
                "cw-dashboard-safety-dashboard-report",

                //AUTOMAIL-REPORT
                "cw-automail-report-manage",
                "cw-automail-report-manage-list",
                "cw-automail-report-manage-create",
                // BOMs - Menu
                "cw-boms-menu",

                // BOMs - Real Time Passenger
                "cw-boms-realtime-passenger-dashboard-list",
                "cw-boms-realtime-passenger-dashboard-form",
                "cw-boms-realtime-passenger-dashboard-search",
                "cw-boms-realtime-passenger-dashboard-search-result",
                "cw-boms-realtime-passenger-dashboard-detail-widget",

                // BOMs - Reports
                "cw-boms-reports",
                "cw-boms-reports-summary-round-trip-form",
                "cw-boms-reports-realtime-passenger-on-board-form",
                "cw-boms-reports-summary-passenger-by-route-form",
                "cw-boms-reports-summary-passenger-by-zone-form",
                "cw-boms-reports-bus-usage-statistics-form",
                "cw-boms-reports-summary-transportation-form",
                "cw-boms-reports-monthly-summary-transportation-form",
                "cw-boms-reports-summary-passenger-by-zone-and-vehicle-type-form",

                //BOMs - DefaultValues
                "cw-boms-defaultvalues-list",
                "cw-boms-defaultvalues-view",
                "cw-boms-defaultvalues-manage",

                //SCGL - Test Drive Widget
                "cw-test-drive-widget",
            ]
        }]
    };

    // Global configuration for browser.
    if (typeof (window) !== "undefined") {
        window.componentConfig = componentConfig;
    } else {
        // For Gulp usage return object to VM directly.
        return componentConfig;
    }
})();
