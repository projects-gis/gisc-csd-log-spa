﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import Utility from "../../../../app/frameworks/core/utility";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import {EntityAssociation, Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

/**
 * User Management > Create/Update
 * 
 * @class UserManageScreen
 * @extends {ScreenBase}
 */
class UserManageScreen extends ScreenBase {

    /**
     * Creates an instance of UserManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Use ScreenHelper to create common datasource such as Yes/No options
        this.enableDS = ScreenHelper.createYesNoObservableArray();

        // Set blade title
        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("User_CreateUser")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("User_UpdateUser")());
        }

        this.userId = this.ensureNonObservable(params.userId, -1);
        this.dateFormat = WebConfig.companySettings.shortDateFormat;

        // These properties will be populated in onLoad
        this.firstName = ko.observable("");
        this.lastName = ko.observable("");
        this.username = ko.observable("");
        this.email = ko.observable("");
        this.phone = ko.observable("");
        this.groupId = ko.observable();
        this.expDate = ko.observable("");
        this.ipPermit = ko.observable("");
        this.accessibleCompanies = ko.observableArray([]);
        this.order = ko.observable([[1, "asc"]]);
        this.addAllCompany = ko.observable(false);

        this.isTargetUserSysAdmin = ko.observable(false);
        this.canEdit = ko.pureComputed(() => {
            // Check with this formula
            // A. {Target User} is not sysadmin.
            // B. {Current User} is sysadmin.
            return !this.isTargetUserSysAdmin() || WebConfig.userSession.isSysAdmin;
        });

        // These properties will be used to calculate delta changed.
        this._originalAccessibleCompanies = [];
        this._originalGroups = [];

        // For properties that has preset value. So trackChange work with mode=create 
        this.userEnabled = ko.observable(ScreenHelper.findOptionByProperty(this.enableDS, "value", true));

        // Observe change about accessible companies
        this.subscribeMessage("bo-accessible-companies-selected", (selectedCompanies) => {
            var mappingAccessibleCompanies = selectedCompanies.map((obj) => {
                let mObj = {
                    companyId: obj.id,
                    companyName: obj.name,
                    canManage: true
                };
                return mObj;
            });

            // #30250 - need to keep unable to manage companies + mappingAccessibleCompanies
            var notAccessibileCompanies = _.filter(this._originalAccessibleCompanies, (o) => {
                return !o.canManage;
            });
            if(notAccessibileCompanies) {
                mappingAccessibleCompanies = mappingAccessibleCompanies.concat(notAccessibileCompanies);
            }

            this.accessibleCompanies.replaceAll(mappingAccessibleCompanies);
        });

        this.showAccessibleCompanies = ko.pureComputed(function() {
            var groupIdValue = !_.isNil(this.groupId()) ? this.groupId().value : null;
            if (groupIdValue == Enums.UserGroup.SystemAdmin) {
                this.addAllCompany(false);
            }

            return groupIdValue !== Enums.UserGroup.SystemAdmin;
        }, this);

    }

    /**
     * Get WebRequest for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();

        var userRequest = (this.mode === Screen.SCREEN_MODE_UPDATE) ? 
            this.webRequestUser.getUser(this.userId, [
                EntityAssociation.User.Groups, 
                EntityAssociation.User.AccessibleCompanies
            ]) : null;
        $.when(userRequest).done((user) => {
            var groupFilter = { isSystem: true };

            if(user) {
                var userGroup = user["groups"][0];
                groupFilter.includeIds = [ userGroup.groupId ];

                // This flag will be used to determine possible actions.
                this.isTargetUserSysAdmin(ScreenHelper.isUserSysAdmin(user));
            }

            this.webRequestGroup.listGroupSummary(groupFilter).done((response) => {
                this.groupsDS = ScreenHelper.createGroupsObservableArray(response["items"]);

                if(user) {
                    this.firstName(user.firstName);
                    this.lastName(user.lastName);
                    this.username(user.username);
                    this.email(user.email);
                    this.phone(user.phone);
                    this.addAllCompany(user.autoAddCompany);

                    // this.groupId is an option object, not just the Id. 
                    var groupIdObj = ScreenHelper.findOptionByProperty(this.groupsDS, "value", user["groups"][0]["groupId"]);
                    this.groupId(groupIdObj);

                    // this.userEnabled is an option object, not just boolean
                    this.userEnabled = ko.observable(ScreenHelper.findOptionByProperty(this.enableDS, "value", user.enable));

                    this.expDate(user.expirationDate);
                    this.ipPermit(user.ipPermit);

                    this.accessibleCompanies(user.accessibleCompanies);

                    // For compute delta
                    this._originalAccessibleCompanies = $.extend(true, [], user.accessibleCompanies);
                    this._originalGroups = $.extend(true, [], user.groups);
                }

                dfd.resolve();
            }).fail((e)=> {
                dfd.reject(e);
            });

        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup track change.
        this.username.extend({
            trackChange: true
        });
        this.firstName.extend({
            trackChange: true
        });
        this.lastName.extend({
            trackChange: true
        });
        this.email.extend({
            trackChange: true
        });
        this.phone.extend({
            trackChange: true
        });
        this.groupId.extend({
            trackChange: true
        });
        this.userEnabled.extend({
            trackChange: true
        });
        this.expDate.extend({
            trackChange: true
        });
        this.ipPermit.extend({
            trackChange: true
        });
        this.accessibleCompanies.extend({
            trackArrayChange: {
                uniqueFromPropertyName: "companyId"
            }
        });

        // Manual setup validation rules
        this.username.extend({
            required: true,
            serverValidate: {
                params: "Username",
                message: this.i18n("M010")()
            }
        });
        this.email.extend({
            email: true,
            required: true
        });
        this.phone.extend({
            required: true
        });
        this.groupId.extend({
            required: true
        });
        this.userEnabled.extend({
            required: true
        });
        this.ipPermit.extend({
            ipAddressMaximum: 5
        });

        this.validationModel = ko.validatedObservable({
            username: this.username,
            email: this.email,
            phone: this.phone,
            groupId: this.groupId,
            userEnabled: this.userEnabled,
            ipPermit: this.ipPermit
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.CreateUser_BackOffice)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                    actions.push(this.createActionCancel());
                }
                break;
            case Screen.SCREEN_MODE_UPDATE:
                if(this.canEdit() && WebConfig.userSession.hasPermission(Constants.Permission.UpdateUser_BackOffice)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                    actions.push(this.createActionCancel());
                }
                break;
        }
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if(this.canResetPassword()) {
                commands.push(this.createCommand("cmdResetPassword", this.i18n("Common_ResetPassword")(), "svg-cmd-reset-password"));
            }
            if(this.canDelete()) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.userId,
            firstName: this.firstName(),
            lastName: this.lastName(),
            username: this.username(),
            email: this.email(),
            phone: this.phone(),
            enable: this.userEnabled().value,
            expirationDate: this.expDate(),
            ipPermit: this.ipPermit(),
            userType: Enums.UserType.BackOffice,
            authenticationType: Enums.AuthenticationType.Form,
            autoAddCompany: this.addAllCompany()
        };

        // SystemAdmin able to access all companies already.
        var groupIdValue = !_.isNil(this.groupId()) ? this.groupId().value : null;
        if(groupIdValue === Enums.UserGroup.SystemAdmin) {
            this.accessibleCompanies.removeAll();
        }

        // Generate array with InfoStatus on each item.
        model.groups = Utility.generateArrayWithInfoStatus(
            this._originalGroups,
            [{
                userId: this.userId,
                groupId: this.groupId().value,
                groupName: this.groupId().title,
                id: -1
            }]
        );
        model.accessibleCompanies = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleCompanies, 
            this.accessibleCompanies(),
            "companyId"
        );

        return model;
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();
            //console.log(model);
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestUser.createUser(model).done((response) => {
                        this.publishMessage("bo-user-changed", response.id);
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestUser.updateUser(model).done((response) => {
                        this.publishMessage("bo-user-changed", response.id);

                        // If update current username, notify user menu
                        if(WebConfig.userSession.id === this.userId) {
                            this._eventAggregator.publish(EventAggregatorConstant.CURRENT_USERNAME_CHANGED, this.username());
                        }

                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdResetPassword") {
            this.showMessageBox(null, this.i18n("M026")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestUser.resetUserPassword(this.userId).done(() => {
                            this.displaySuccess(this.i18n("M104"));
                        }).fail((e)=> {
                            this.handleError(e);
                        }).always(()=>{
                            this.isBusy(false);
                        });

                        break;
                }
            });

        } else if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M103")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestUser.deleteUser(this.userId).done(() => {
                            this.publishMessage("bo-user-changed", this.userId);
                            this.close(true);
                        }).fail((e)=> {
                            this.handleError(e);
                        }).always(()=>{
                            this.isBusy(false);
                        });
                        break;
                }
            });
        }
    }

    /**
     * Add accessible company
     * @private
     */
    addAccessibleCompany() {
        var targetMode = (this.mode === Screen.SCREEN_MODE_CREATE) ? "user-create" : "user-update";
        var selectedCompanyIds = this.accessibleCompanies().map((obj) => {
            return obj.companyId;
        });

        this.navigate("bo-shared-accessible-company-select", {
            mode: targetMode,
            selectedCompanyIds: selectedCompanyIds
        });
    }
    
    /**
     * Check if reset password action is possible, see formula below
     * 
     * Facts:
     * A. {Current User} has UpdateUser_BackOffice permission.
     * B. {Current User} is {Target User}
     * C. {Target User} is not sysadmin.
     * D. {Current User} is sysadmin.
     * 
     * Formula = A && (B || C || D)
     * 
     * @private
     * @returns true if criteria met, false otherwise.
     */
    canResetPassword() {
        var hasPermission = WebConfig.userSession.hasPermission(Constants.Permission.UpdateUser_BackOffice);
        
        return hasPermission && (ScreenHelper.isCurrentUser(this.userId) || 
            !this.isTargetUserSysAdmin() ||
            WebConfig.userSession.isSysAdmin);
    }
    
    /**
     * Check if delete action is possible, see formula below
     * 
     * Facts:
     * A. {Current User} has DeleteUser_BackOffice permission.
     * B. {Current User} is not {Target User}
     * C. {Target User} is not sysadmin.
     * D. {Current User} is sysadmin.
     * 
     * Formula:
     * When target is sysadmin: A && (B && D) 
     * else: A && (B && C)
     * 
     * @private
     * @returns true if criteria met, false otherwise.
     */
    canDelete() {
        var hasPermission = WebConfig.userSession.hasPermission(Constants.Permission.DeleteUser_BackOffice);
        var hasPermissionAndTargetIsSysAdmin = (hasPermission && this.isTargetUserSysAdmin());

        if(hasPermissionAndTargetIsSysAdmin) {
            return !ScreenHelper.isCurrentUser(this.userId) && WebConfig.userSession.isSysAdmin;
        }

        return hasPermission && (!ScreenHelper.isCurrentUser(this.userId) && 
            !this.isTargetUserSysAdmin());
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserManageScreen),
    template: templateMarkup
};