﻿import "jquery";
import ko from "knockout";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import templateMarkup from "text!./list.html";
import WebRequestGroup from "../../../../../app/frameworks/data/apicore/webRequestGroup";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class UserGroupListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_UserGroups")());
        this.bladeSize = BladeSize.Medium;

        this.userGroupList = ko.observableArray([]);
    
        this.filterText = ko.observable('');
        this.selectedGroup = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);
        
        this.filter = {
            isSystem: false,
            isCompanyGroup: false,
            sortingColumns: DefaultSoring.Group
        };

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (userGroup) => {
            if(userGroup) {
                return this.navigate("bo-subscription-and-group-group-manage", {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    userGroupId: userGroup.id
                });
            }
            return false;
        };

        this.subscribeMessage("bo-userGroup-change", (userGroupId) => {
            this.isBusy(true);
            this.webRequestGroup.listGroupSummary(this.filter).done((r)=> {
                 this.userGroupList.replaceAll(r.items);
                 this.recentChangedRowIds.replaceAll([userGroupId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestGroup.listGroupSummary(this.filter).done((r)=> {
            this.userGroupList(r["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateGroup_BackOffice)) {
           commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id === "cmdCreate") {
            this.navigate("bo-subscription-and-group-group-manage", { mode: Screen.SCREEN_MODE_CREATE });
        }
        this.selectedGroup(null);
        this.recentChangedRowIds.removeAll();
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedGroup(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserGroupListScreen),
    template: templateMarkup
};