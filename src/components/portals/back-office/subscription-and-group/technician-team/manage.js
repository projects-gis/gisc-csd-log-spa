﻿import "jquery";
import ko from "knockout";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestSubscription from "../../../../../app/frameworks/data/apicore/webRequestSubscription";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {
    Constants,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestTechnicianTeam from "../../../../../app/frameworks/data/apicore/webRequestTechnicianTeam";

class TechnicianTeamManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.technicianId = this.ensureNonObservable(params.technicianTeamId, -1);

        //Enable option array from ScreenHelper to create data source Yes/No
        this.enableOptions = ScreenHelper.createYesNoObservableArray();

        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);
        this.name = ko.observable();
        this.description = ko.observable();
        this.enableSelected = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", true));

        this.teamLeader = ko.observable();
        this.teamLeaderOptions = ko.observableArray([]);

        this.order = ko.observable([
            [1, "asc"]
        ]);

        this.accessibleUsers = ko.observableArray([]);
        // this.accessibleUsers = ko.observableArray(this.mockJsonData());
        this._originalAccessibleCompanies = [];
        // this.accessibleCompanies = ko.observableArray([]);

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Technician_Team_CreateTechnicianTeam")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Technician_Team_UpdateTechnicianTeam")());
                break;
        }

        this.subscribeMessage("bo-accessible-companies-selected", (selectedCompanies) => {
            // console.log("selectedCompanies", selectedCompanies);
            // var mappingAccessibleCompanies = selectedCompanies.map((obj) => {
            //     console.log("subscribeMessageObj", obj)
            //     let mObj = {
            //         userId: obj.id,
            //         userName: obj.firstName,
            //         canManage: true
            //     };
            //     return mObj;
            // });
            // console.log("mappingAccessibleCompanies", mappingAccessibleCompanies)

            // //#30250 - need to keep unable to manage companies + mappingAccessibleCompanies
            // var notAccessibileCompanies = _.filter(this._originalAccessibleCompanies, (o) => {
            //     return !o.canManage;
            // });
            // if (notAccessibileCompanies) {
            //     mappingAccessibleCompanies = mappingAccessibleCompanies.concat(notAccessibileCompanies);
            // }

            // this.accessibleUsers.replaceAll(mappingAccessibleCompanies);
            selectedCompanies.forEach( (items)=>{
                items.canManage = true;
            });
            
            this.accessibleUsers.replaceAll(selectedCompanies);
            
        });

        this.teamLeader.subscribe((res)=>{
        
            let leaderId = (res) ? res.id : null;
            let accessibleUsers = this.accessibleUsers().filter(function (items) {
               
                return (items.id!= leaderId );
                });

            this.accessibleUsers.replaceAll(accessibleUsers);
            
        });


    }


    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestSubscription() {
        return WebRequestSubscription.getInstance();
    }
    get webRequestTechnicianTeam() {
        return WebRequestTechnicianTeam.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
   
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.webRequestTechnicianTeam.listTechnicianTeamSummaryUserlist().done((response) => {

                    this.teamLeaderOptions(ScreenBase.formatName(response.items) );

                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

                break;
            case Screen.SCREEN_MODE_UPDATE:
            // console.log("update");
                let userListFilter =    {   id:this.technicianId,
                                            includeAssociationName:EntityAssociation.TechnicianTeam.TeamLeader
                                        }
                var d1 =  this.webRequestTechnicianTeam.getTechnicianTeam(this.technicianId,[EntityAssociation.TechnicianTeam.Member]);
                var d2 = this.webRequestTechnicianTeam.listTechnicianTeamSummaryUserlistUpdate(this.technicianId,EntityAssociation.TechnicianTeam.TeamLeader);
                

                $.when(d1, d2).done((r1, r2) => {
                   
                        if (r1) {

                        // console.log("r1", r1)
                        // console.log("r2", r2)

                        this.name(r1.name);
                        this.description(r1.description);
                        this.enableSelected(ScreenHelper.findOptionByProperty(this.enableOptions, "value", r1.enable));
                        this.teamLeaderOptions(ScreenBase.formatName(r2.items) );
                        this.teamLeader(ScreenHelper.findOptionByProperty(this.teamLeaderOptions, "id", r1.teamLeaderId));


                        let memberInfos = r1.memberInfos.filter(function (items) { //filter เอาทีม leader ออก
                            return (items.id != r1.teamLeaderId );
                        });
                        memberInfos.forEach((items)=>{ //เพิ่มปุ่มกากบาท
                            items.canManage = true;
                        })

                        // console.log("memberInfos",memberInfos)
                        this.accessibleUsers.replaceAll(ScreenBase.formatName(memberInfos));
                        // this.accessibleUsers.replaceAll(ScreenBase.formatName(r1.memberInfos));

                    }

                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

                
                break;
        }
        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {


        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.enableSelected.extend({
            required: true
        });

        this.accessibleUsers.extend({
            required: true
        });

        this.teamLeader.extend({
            required: true
        })

        this.validationModel = ko.validatedObservable({
            name: this.name,
            enableSelected: this.enableSelected,
            teamLeader: this.teamLeader,
            accessibleUsers: this.accessibleUsers

        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    addAccessibleUsers() {
      
        let teamLeaderId = (this.teamLeader()) ? this.teamLeader().id : undefined;
        let technicianTeamId = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.technicianId : null;
        var targetMode = (this.mode === Screen.SCREEN_MODE_CREATE) ? "technician-create" : "technician-update";
        var selectedUserIds = this.accessibleUsers().map((obj) => {
      
            return obj.id;
        });

        this.navigate("bo-shared-accessible-users-select", {
            mode: targetMode,
            selectedUserIds: selectedUserIds,
            teamLeaderId: teamLeaderId,
            technicianTeamId : technicianTeamId
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        // if ((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateSubscription)) ||
        //     (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateSubscription))) {
        //     actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        // }
        if ( (this.mode === Screen.SCREEN_MODE_CREATE) || (this.mode === Screen.SCREEN_MODE_UPDATE) ) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }

        // actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        // if (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.DeleteSubscription)) {
        //     commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        // }
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    
    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
   
            var user = []
            this.accessibleUsers().forEach(items => {
                user.push(items.id)  
            });

        var model = {

            // id: this.subscriptionId,
            name: this.name(),
            description: this.description(),
            enable: this.enableSelected().value,
            userIds: user,
            teamLeaderId: this.teamLeader().id

        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
        
                    this.isBusy(false);
                    this.webRequestTechnicianTeam.createTechnicianTeam(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-technicianteam-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                model.id = this.technicianId;
                // console.log("model", model);
                // console.log("technicianId", this.technicianId);
                    this.webRequestTechnicianTeam.updateTechnicianTeam(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-technicianteam-change", this.technicianId);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("Technician_Team_Delete_Msg")(),
                BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestTechnicianTeam.deleteTechnicianTeam(this.technicianId).done(() => {
                            this.isBusy(false);

                            this.publishMessage("bo-technicianteam-change", this.technicianId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);

                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(TechnicianTeamManageScreen),
    template: templateMarkup
};