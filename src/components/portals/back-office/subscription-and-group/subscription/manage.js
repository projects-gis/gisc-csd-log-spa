﻿import "jquery";
import ko from "knockout";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestSubscription from "../../../../../app/frameworks/data/apicore/webRequestSubscription";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";

class SubscriptionMangeScreen extends ScreenBase {
    constructor(params) {
        super(params);

        //Enable option array from ScreenHelper to create data source Yes/No
        this.enableOptions = ScreenHelper.createYesNoObservableArray();

        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Subscriptions_CreateSubscription")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Common_UpdateSubscription")());
                break;
        }

        this.subscriptionId = this.ensureNonObservable(params.subscriptionId, -1);

        this.name = ko.observable('');
        this.description = ko.observable('');
        this.maxPoi = ko.observable('100');
        this.maxArea = ko.observable('100');
        this.maxRoute = ko.observable('100');
        this.alertExpiration = ko.observable('10');
        this.dataExpiration = ko.observable('93');

         // For properties that has preset value. So trackChange work with mode=create 
        this.enableSelected = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", true));
    }
    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestSubscription() {
        return WebRequestSubscription.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                dfd.resolve();
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.webRequestSubscription.getSubscription(this.subscriptionId).done((subscription)=> {
                    if (subscription) {
                        this.name(subscription.name);
                        this.description(subscription.description);
                        this.maxPoi(subscription.maxPoi);
                        this.maxArea(subscription.maxArea);
                        this.maxRoute(subscription.maxRoute);
                        this.alertExpiration(subscription.alertExpiration);
                        this.dataExpiration(subscription.dataExpiration);
                        this.enableSelected(ScreenHelper.findOptionByProperty(this.enableOptions, "value", subscription.enable));
                    }
                    dfd.resolve();
                }).fail((e)=> {
                    dfd.reject(e);
                });
                break;
        }
        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {

        this.name.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.maxPoi.extend({
            trackChange: true
        });
        this.maxArea.extend({
            trackChange: true
        });
        this.maxRoute.extend({
            trackChange: true
        });
        this.alertExpiration.extend({
            trackChange: true
        });
        this.dataExpiration.extend({
            trackChange: true
        });
        this.enableSelected.extend({
            trackChange: true
        });

        // Manual setup validation rules
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.maxPoi.extend({
            required: true
        });
        this.maxArea.extend({
            required: true
        });
        this.maxRoute.extend({
            required: true
        });
        this.alertExpiration.extend({
            required: true
        });
        this.dataExpiration.extend({
            required: true
        });
        this.enableSelected.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            enableSelected: this.enableSelected,
            maxPoi: this.maxPoi,
            maxArea: this.maxArea,
            maxRoute: this.maxRoute,
            alertExpiration: this.alertExpiration,
            dataExpiration: this.dataExpiration,
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateSubscription))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateSubscription))) 
        {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }

        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
         if (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.DeleteSubscription)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.subscriptionId,
            name: this.name(),
            description: this.description(),
            enable: this.enableSelected().value,
            maxPoi: this.maxPoi(),
            maxArea: this.maxArea(),
            maxRoute: this.maxRoute(),
            alertExpiration: this.alertExpiration(),
            dataExpiration: this.dataExpiration()
        };

        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
         super.onActionClick(sender);
        
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestSubscription.createSubscription(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-subscription-change", response.id);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestSubscription.updateSubscription(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-subscription-change", this.subscriptionId);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
         if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M011")(),
                BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                         this.isBusy(true);

                            this.webRequestSubscription.deleteSubscription(this.subscriptionId).done(() => {
                                this.isBusy(false);

                                this.publishMessage("bo-subscription-change", this.subscriptionId);
                                this.close(true);
                            }).fail((e)=> {
                                this.isBusy(false);

                                this.handleError(e);
                            });
                            break;
                }
            });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(SubscriptionMangeScreen),
    template: templateMarkup
};