﻿import ko from "knockout";
import templateMarkup from "text!./accessible-users-select.html";
import ScreenBase from "../../screenbase";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webRequestCompany";
import TechnicianTeam from "../../../../app/frameworks/data/apicore/webRequestTechnicianTeam";
import {

    EntityAssociation
} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Select accessible companies
 * 
 * @class AccessibleCompaniesSelectScreen
 * @extends {ScreenBase}
 */
class AccessibleUsersSelectScreen extends ScreenBase {

    /**
     * Creates an instance of AccessibleCompaniesSelectScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        //console.log("params",params)
        this.bladeTitle(this.i18n("User_AccessibleUsers")());

        this.usersList = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);
        this.filterText = ko.observable();
        this.order = ko.observable([
            [1, "asc"]
        ]);
        this.teamLeaderId = ko.observable(params.teamLeaderId, null);
        this.technicianTeamId = ko.observable(params.technicianTeamId, null);
        this.mode = params.mode
        // Internal member
        this._selectedUserIds = this.ensureNonObservable(params.selectedUserIds, []);
        this._selectedItemsSubscribe = this.selectedItems.subscribe((comps) => {
            this._selectedUserIds = comps.map((obj) => {
                return obj.id;
            });
        });
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get webRequestTechnicianTeam() {
        return TechnicianTeam.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        // Create Deferred object then resolve when the webRequest done
        var dfd = $.Deferred();
        let teamLeaderId = this.teamLeaderId();
        // let userListFilter = {}


        if (this.mode === "technician-update") {
            
            // userListFilter = {
            //     id: this.technicianTeamId(),
            //     includeAssociationName: EntityAssociation.TechnicianTeam.TeamMember
            // }

            this.webRequestTechnicianTeam.listTechnicianTeamSummaryUserlistUpdate(this.technicianTeamId(),EntityAssociation.TechnicianTeam.TeamMember).done((response) => {

                let formatData = ScreenBase.formatName(response.items);
                let accessibleUsers = formatData.filter(function (items) {
                    return (items.id != teamLeaderId);
                });

                this.usersList(accessibleUsers);

                // If there is _selectedUserIds, set pre-selected value

                this._selectedUserIds.forEach(function (element) {
                    let users = ko.utils.arrayFirst(this.usersList(), function (user) {
                        return user.id === element;
                    });
                    if (users) {
                        this.selectedItems.push(users);
                    }
                }, this);

                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });

        } else {

            this.webRequestTechnicianTeam.listTechnicianTeamSummaryUserlist().done((response) => {

                let formatData = ScreenBase.formatName(response.items);
                let accessibleUsers = formatData.filter(function (items) {
                    return (items.id != teamLeaderId);
                });

                this.usersList(accessibleUsers);

                // If there is _selectedUserIds, set pre-selected value

                this._selectedUserIds.forEach(function (element) {
                    let users = ko.utils.arrayFirst(this.usersList(), function (user) {
                        return user.id === element;
                    });
                    if (users) {
                        this.selectedItems.push(users);
                    }
                }, this);

                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });

        }
        return dfd;


    }



    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Setup track change.
        this.selectedItems.extend({
            trackChange: true
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actChoose") {
            this.publishMessage("bo-accessible-companies-selected", this.selectedItems());
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(AccessibleUsersSelectScreen),
    template: templateMarkup
};