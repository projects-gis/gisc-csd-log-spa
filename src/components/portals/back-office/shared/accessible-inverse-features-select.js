﻿import ko from "knockout";
import templateMarkup from "text!./accessible-inverse-features-select.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import WebRequestFeature from "../../../../app/frameworks/data/apitrackingcore/webRequestFeature";

class AccessibleFeaturesSelectScreen extends ScreenBase {
    
    /**
     * Creates an instance of FeaturesListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        // Blade configuration.
        this.bladeTitle(this.i18n("Common_InverseSensors")());
        this.bladeSize = BladeSize.Small;

        this.listFeature = ko.observableArray([]);
        this.selectedIds = ko.observableArray([]);
        this.filterText = ko.observable();
        this.order = ko.observable([[ 1, "asc" ]]);
        this.selectedInverseFeatures = ko.observableArray(_.size(params.selectedInverseFeatures) ? params.selectedInverseFeatures : []);

        this.itemsChanged = (data) => { 
            if(data.isFeature){
                this.selectedIds.push(data.id);
            }else{
                let newIds = _.remove(this.selectedIds(), (id) => {
                    return id != data.id;
                });
                this.selectedIds(_.clone(newIds));
            }
        }

    }

    /**
     * Get WebRequest specific for Feature module in Tracking Web API access.
     * @readonly
     */
    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // Do not load web request when blade restore.
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var filter = {
            sortingColumns: DefaultSorting.BoxFeature
        };
        this.webRequestFeature.listFeatureSummary(filter).done((response)=> {
            var features = _.filter(response["items"], (item)=>{
                let fId = _.filter(this.selectedInverseFeatures(), (id)=>{
                    return id == item.id;
                });
                item.isFeature = _.size(fId) ? true : false;
                item.displayName = (item.displayName) ? item.displayName : item.name;
                return item.eventDataType == 1;
            });

            this.selectedIds(this.selectedInverseFeatures());
            this.listFeature(features);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actApply", this.i18n("Common_Apply")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id == "actApply") {
            this.publishMessage("bo-accessible-inverse-features-apply", this.selectedIds());
            this.close();
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

}

export default {
    viewModel: ScreenBase.createFactory(AccessibleFeaturesSelectScreen),
    template: templateMarkup
};