﻿import ko from "knockout";
import templateMarkup from "text!./subscription.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
/**
 * 
 * 
 * @class CompanySubscriptionListScreen
 * @extends {ScreenBase}
 */
class CompanySubscriptionListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = this.ensureNonObservable(params.companyId);
        this.timezone = this.ensureNonObservable(params.timezone);
        this.timezoneName = this.ensureNonObservable(params.timezoneName);

        this.bladeTitle(this.i18n("Companies_CompanySubscriptions")());
        this.bladeSize = BladeSize.Medium;

        var subscriptions = this.ensureNonObservable($.extend(true, [], params.subscriptions), []);
        var displaySubscriptions = ko.utils.arrayFilter(subscriptions, (subscription) => {
            return subscription.infoStatus !== Enums.InfoStatus.Delete;
        });

        this.subscriptions = ko.observableArray(subscriptions);
        this.displaySubscriptions = ko.observableArray(displaySubscriptions);
        this.selectedSubscription = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "desc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (subscription) => {
            if(subscription) {
                return this.navigate("bo-company-manage-subscription-manage", { 
                    mode: this.mode === "company-create" ? "company-create-subscription-update" : "company-update-subscription-update", 
                    subscriptions: this.subscriptions(),
                    subscription: subscription,
                    timezone: this.timezone,
                    timezoneName: this.timezoneName
                });
            }
            return false;
        };

        // if subscriptions list changed, display subscriptions list will be effected
        this._changeSubscriptionsSubscribe = this.subscriptions.subscribe((subscriptions) => {
            var displaySubscriptions = ko.utils.arrayFilter(subscriptions, (subscription) => {
                return subscription.infoStatus !== Enums.InfoStatus.Delete;
            });
            this.displaySubscriptions.replaceAll(displaySubscriptions);
        });

        this.subscribeMessage("bo-company-subscription-changed", (data) => {
            this.subscriptions.replaceAll(data.subscriptions);
            this.recentChangedRowIds.replaceAll([data.id]);
        });

        this.subscribeMessage("bo-company-subscription-deleted", (subscription) => {
            var index = _.findIndex(this.subscriptions(), (item) => {
                return item.id == subscription.id;
            });

            if (subscription.id > 0) {
                //if update, set infostatus to delete
                subscription.infoStatus = Enums.InfoStatus.Delete;
                this.subscriptions.splice(index, 1, subscription);
            }
            else{
                //if add, delete from subscription list
                this.subscriptions.splice(index, 1);
            }
        });
    }

    setupExtend() {
        this.subscriptions.extend({ trackArrayChange: true });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubscription(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdAddSubscription", this.i18n("Common_Add")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            this.publishMessage("bo-company-subscriptions-changed", this.subscriptions());
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdAddSubscription"){
            // generate id for added items
            var min = _.minBy(this.subscriptions(), 'id');
            var minId = -1;
            if (!_.isNil(min) && min.id < 0) {
                minId = (min.id - 1);
            }

            var subscription = {
                id: minId,
                infoStatus: Enums.InfoStatus.Add,
                companyId: this.companyId,
                subscriptionId: null,
                startDate: null,
                endDate: null,
                formatStartDate: null,
                formatEndDate: null,
                maxPoi: null,
                maxArea: null,
                maxRoute: null,
                alertExpiration: null,
                dataExpiration: null,
                isCanUpdate: true
            };
            this.navigate("bo-company-manage-subscription-manage", { 
                mode: this.mode == "company-create" ? "company-create-subscription-add" : "company-update-subscription-add", 
                subscriptions: this.subscriptions(), 
                subscription: subscription,
                timezone: this.timezone,
                timezoneName: this.timezoneName });
        }
        this.selectedSubscription(null);
        this.recentChangedRowIds.removeAll();
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanySubscriptionListScreen),
    template: templateMarkup
};