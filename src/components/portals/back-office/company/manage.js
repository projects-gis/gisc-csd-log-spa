﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../app/frameworks/core/utility";
import * as Screen from "../../../../app/frameworks/constant/screen";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../app/frameworks/constant/apiConstant";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCountry from "../../../../app/frameworks/data/apicore/webrequestCountry";
import WebRequestProvince from "../../../../app/frameworks/data/apicore/webrequestProvince";
import WebRequestCity from "../../../../app/frameworks/data/apicore/webrequestCity";
import WebRequestTown from "../../../../app/frameworks/data/apicore/webrequestTown";
import WebRequestLanguage from "../../../../app/frameworks/data/apicore/webrequestLanguage";
import WebRequestDateTimeFormat from "../../../../app/frameworks/data/apicore/webRequestDateTimeFormat";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import {
    CompanyAddress,
    CompanyRegional,
    CompanySettings
} from "./infos";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";

class CompanyManageScreen extends ScreenBase {

    /**
     * Creates an instance of CompanyManageScreen.
     *
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Companies_CreateCompany")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Companies_UpdateCompany")());
        }

        this.id = this.ensureNonObservable(params.id, 0);

        this.logo = ko.observable(null);
        this.logoFileName = ko.observable("");
        this.logoSelectedFile = ko.observable(null);
        this.logoMimeType = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();
        this.logoSuggestionWidth = "52";
        this.logoSuggestionHeight = "52";
        this.logoUrl = ko.pureComputed(() => {
            if (this.logo() && this.logo().infoStatus !== Enums.InfoStatus.Delete) {
                return this.logo().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        this.name = ko.observable("");
        this.code = ko.observable("");
        this.description = ko.observable("");
        this.enable = ko.observable();

        this.address = new CompanyAddress();
        this.subscriptions = ko.observableArray([]);
        this.contacts = ko.observableArray([]);
        this.regional = new CompanyRegional();
        this.settings = new CompanySettings();
        //set default continueTracking
        this.settings.continueTracking(30);
        this.mapServiceIds = ko.observableArray([]);
        this.identifyService = ko.observable();

        // Use for custom validate
        this.contactsValidator = ko.observable();
        this.regionalValidator = ko.observable();
        this.settingsValidator = ko.observable();
        // Use Check UseNostraMap
        this.useNostraMap = ko.observable(true);
        // Prepare dropdownlist options
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.countryOptions = ko.observableArray([]);
        this.provinceOptions = ko.observableArray([]);
        this.enumsMDVROptions = ko.observableArray([]);
        this.cityOptions = ko.observableArray([]);
        this.townOptions = ko.observableArray([]);

        //Pairable
        this.pairableItem = ko.observable();

        this.selectedSubMenuItem = ko.observable(null);
        // Subscribe Message when company subscriptions changed
        this.subscribeMessage("bo-company-subscriptions-changed", (subscriptions) => {
            this.subscriptions.replaceAll(subscriptions);
        });

        // Subscribe Message when company contacts changed
        this.subscribeMessage("bo-company-contact-changed", (contacts) => {
            this.contacts.replaceAll(contacts);
        });

        // Subscripe message when company regional changed
        this.subscribeMessage("bo-company-regional-changed", (regional) => {
            this.regional.languageId(regional.languageId);
            this.regional.timezone(regional.timezone);
            this.regional.timezoneName(regional.timezoneName);
            this.regional.currency(regional.currency);
            this.regional.shortDateFormat(regional.shortDateFormat);
            this.regional.longDateFormat(regional.longDateFormat);
            this.regional.shortTimeFormat(regional.shortTimeFormat);
            this.regional.longTimeFormat(regional.longTimeFormat);
            this.regional.distanceUnit(regional.distanceUnit);
            this.regional.areaUnit(regional.areaUnit);
            this.regional.weightUnit(regional.weightUnit);
            this.regional.volumeUnit(regional.volumeUnit);
            this.regional.temperatureUnit(regional.temperatureUnit);
        });

        // Subscribe Message when company settings changed
        this.subscribeMessage("bo-company-setting-changed", (settings) => {
            this.settings.companyAdminId = settings.companyAdminId;
            this.settings.companyAdminUsername(settings.companyAdminUsername);
            this.settings.companyAdminEmail(settings.companyAdminEmail);
            this.settings.adUrl(settings.adUrl);
            this.settings.adUsername(settings.adUsername);
            this.settings.adPassword(settings.adPassword);
            this.settings.applyPasswordRules(settings.applyPasswordRules);
            this.settings.gatewayServerPort(settings.gatewayServerPort);
            this.settings.databaseConnectionId(settings.databaseConnectionId);
            //this.settings.minETADistance(settings.minETADistance);
            //this.settings.themeName(settings.themeName);
            //this.settings.logoSize(settings.logoSize);
            //this.settings.keepTicketDuration(settings.keepTicketDuration);
            //this.settings.showPoiCluster(settings.showPoiCluster);
        });

        this.subscribeMessage("bo-company-mapservices-changed", (mapservices) => {
            this.useNostraMap(mapservices.useNostraMap)
            this.identifyService(mapservices.identify)
            var mapServiceList = mapservices.mapServices.map((obj) => {
                let mObj = {}
                if (this.mode == 'create') {
                    mObj = {
                        mapServiceId: obj.id
                    };
                } else {
                    mObj = {
                        companyId: this.id,
                        mapServiceId: obj.id
                    };
                }

                return mObj;
            });

            this.mapServiceIds.replaceAll(mapServiceList)


        })

        // Subscribe Message when company pairable changed
        this.subscribeMessage("bo-company-pairable-changed", (data) => {
            this.pairableItem(data);
        })

        this.isEnableAutomailFTP = ko.pureComputed(() => {
            return this.settings.enableAutomailFTP();
        });
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     *
     *
     * @readonly
     */
    get webRequestCountry() {
        return WebRequestCountry.getInstance();
    }
    /**
     *
     *
     * @readonly
     */
    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }
    /**
     *
     *
     * @readonly
     */
    get webRequestCity() {
        return WebRequestCity.getInstance();
    }
    /**
     *
     *
     * @readonly
     */
    get webRequestTown() {
        return WebRequestTown.getInstance();
    }
    /**
     *
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     *
     * @readonly
     */
    get webRequestDateTimeFormat() {
        return WebRequestDateTimeFormat.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        var d0 = $.Deferred();

        switch (this.mode) {
            case "create":
                //set default to true
                this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", true));
                this.contacts([{
                    companyId: 0,
                    name: null,
                    email: null,
                    phone: null,
                    order: 1, //Primary
                    infoStatus: Enums.InfoStatus.Original,
                    id: 0
                },
                {
                    companyId: 0,
                    name: null,
                    email: null,
                    phone: null,
                    order: 2, //Secondary
                    infoStatus: Enums.InfoStatus.Original,
                    id: 0
                },
                {
                    companyId: 0,
                    name: null,
                    email: null,
                    phone: null,
                    order: 3, //Additional
                    infoStatus: Enums.InfoStatus.Original,
                    id: 0
                }
                ]);
                this.settings.applyPasswordRules(true);

                this.webRequestCountry.listCountry({
                    sortingColumns: DefaultSorting.Country
                }).done((response) => {
                    var countries = response["items"];
                    this.countryOptions(countries);
                    var defaultCountry = ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true);
                    this.address.country(defaultCountry);

                    let filterEnum = {
                        companyId: WebConfig.userSession.currentCompanyId,
                        types: [103],
                        sortingColumns: DefaultSorting.EnumResource
                    };

                    let filterProvince = {
                        countryIds: [defaultCountry.id],
                        sortingColumns: DefaultSorting.Province
                    };
                    let w1 = this.webRequestEnumResource.listEnumResource(filterEnum);
                    let w2 = this.webRequestProvince.listProvince(filterProvince);
                    $.when(w1, w2).done((r1, r2) => {
                        var enumsList = r1["items"];
                        var provinces = r2["items"];
                        this.provinceOptions(provinces);
                        this.enumsMDVROptions(enumsList);
                        d0.resolve();
                    }).fail((e) => {
                        d0.reject(e);
                    });
                }).fail((e) => {
                    d0.reject(e);
                });

                break;
            case "update":
                this.webRequestCompany.getCompany(this.id, [EntityAssociation.Company.Logo, EntityAssociation.Company.Address, EntityAssociation.Company.Contacts, EntityAssociation.Company.Setting, EntityAssociation.Company.Subscriptions, EntityAssociation.Company.MapServices,EntityAssociation.Company.PairableSetting]).done((company) => {
                    if (company) {

                        if (company.logo) {
                            this.logo(company.logo);
                            this.logoFileName(company.logo.fileName);
                        }
                        this.name(company.name);
                        this.code(company.code);
                        this.description(company.description);
                        this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", company.enable));
                        this.useNostraMap(company.useNOSTRAMap);
                        this.address.id = company.address.id;
                        this.address.houseNo(company.address.houseNo);
                        this.address.street(company.address.street);
                        this.address.locate(company.address.locate);
                        this.address.postCode(company.address.postCode);
                        this.address.latitude(company.address.latitude);
                        this.address.longitude(company.address.longitude);
                        this.settings.minETADistance(company.setting.minETADistance);
                        this.settings.themeName(company.setting.themeName);
                        this.settings.logoSize(company.setting.logoSize);
                        this.settings.keepTicketDuration(company.setting.keepTicketDuration);
                        this.settings.satelliteBadGps(company.setting.satelliteBadGPS);
                        this.settings.showPoiCluster(company.setting.showPoiCluster);
                        this.settings.sendGeofence(company.setting.sendGeoFenceInOut);
                        this.settings.enableAutomailFTP(company.setting.enableAutomailFTP);
                        this.settings.automailFTPPath(company.setting.automailFTPPath);

                        var d1 = this.webRequestCountry.listCountry({
                            sortingColumns: DefaultSorting.Country
                        });
                        var d2 = company.address.countryId ? this.webRequestProvince.listProvince({
                            countryIds: [company.address.countryId],
                            sortingColumns: DefaultSorting.Province
                        }) : null;
                        var d3 = company.address.provinceId ? this.webRequestCity.listCity({
                            provinceIds: [company.address.provinceId],
                            sortingColumns: DefaultSorting.City
                        }) : null;
                        var d4 = company.address.cityId ? this.webRequestTown.listTown({
                            cityIds: [company.address.cityId],
                            sortingColumns: DefaultSorting.Town
                        }) : null;

                        $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {
                            var countries = r1["items"];
                            this.countryOptions(countries);
                            this.address.country(ScreenHelper.findOptionByProperty(this.countryOptions, "id", company.address.countryId));

                            if (r1) {
                                var countries = r1["items"];
                                this.countryOptions(countries);

                                if (company.address.countryId) {
                                    this.address.country(ScreenHelper.findOptionByProperty(this.countryOptions, "id", company.address.countryId));
                                } else {
                                    this.address.country(ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true));
                                }
                            }

                            if (r2) {
                                var provinces = r2["items"];
                                this.provinceOptions(provinces);

                                if (company.address.provinceId) {
                                    this.address.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "id", company.address.provinceId));
                                }
                            }

                            if (r3) {
                                var cities = r3["items"];
                                this.cityOptions(cities);

                                if (company.address.cityId) {
                                    this.address.city(ScreenHelper.findOptionByProperty(this.cityOptions, "id", company.address.cityId));
                                }
                            }

                            if (r4) {
                                var towns = r4["items"];
                                this.townOptions(towns);

                                if (company.address.townId) {
                                    this.address.town(ScreenHelper.findOptionByProperty(this.townOptions, "id", company.address.townId));
                                }
                            }

                            d0.resolve();
                        }).fail((e) => {
                            d0.reject(e);
                        });

                        //subscriptions
                        this.subscriptions(company.logisticsSubscriptions);

                        //contacts
                        this.contacts(company.contacts);

                        //regional
                        this.regional.id = company.setting.id;
                        this.regional.languageId(company.setting.languageId);
                        this.regional.timezone(company.setting.timezone);
                        this.regional.timezoneName(company.setting.timezoneName);
                        this.regional.currency(company.setting.currency);
                        this.regional.shortDateFormat(company.setting.shortDateFormat);
                        this.regional.longDateFormat(company.setting.longDateFormat);
                        this.regional.shortTimeFormat(company.setting.shortTimeFormat);
                        this.regional.longTimeFormat(company.setting.longTimeFormat);
                        this.regional.distanceUnit(company.setting.distanceUnit);
                        this.regional.areaUnit(company.setting.areaUnit);
                        this.regional.weightUnit(company.setting.weightUnit);
                        this.regional.volumeUnit(company.setting.volumeUnit);
                        this.regional.temperatureUnit(company.setting.temperatureUnit);

                        //settings
                        this.settings.id = company.setting.id;
                        this.settings.companyAdminId = company.setting.companyAdminId;
                        this.settings.companyAdminUsername(company.setting.companyAdminUsername);
                        this.settings.companyAdminEmail(company.setting.companyAdminEmail);
                        this.settings.adUrl(company.setting.adUrl);
                        this.settings.adUsername(company.setting.adUsername);
                        this.settings.adPassword(company.setting.adPassword);
                        this.settings.applyPasswordRules(company.setting.applyPasswordRules);
                        this.settings.gatewayServerPort(company.setting.gatewayServerPort);
                        this.settings.databaseConnectionId(company.setting.databaseConnectionId);
                        this.settings.minETADistance(company.setting.minETADistance);
                        this.settings.themeName(company.setting.themeName);
                        this.settings.logoSize(company.setting.logoSize);
                        this.settings.keepTicketDuration(company.setting.keepTicketDuration);
                        this.settings.continueTracking(company.setting.continuousTrackingDuration);
                        this.settings.showPoiCluster(company.setting.showPoiCluster);
                        this.settings.MDVRModel(company.setting.mdvrModel);
                        this.settings.enableOdometerFixing(company.setting.enableOdometerFixing);

                        let pairableInfo = company.pairableSetting;
                        if(pairableInfo){
                            if(_.size(pairableInfo.mainDetectFeatures)){
                                pairableInfo.mainDetectFeatures.forEach(item => {
                                    item.status = item.status == 0 ? 2 : item.status;
                                });
                            }

                            if(_.size(pairableInfo.pairingVehicleFeatures)){
                                pairableInfo.pairingVehicleFeatures.forEach(item => {
                                    item.status = item.status == 0 ? 2 : item.status;
                                });
                            }
                        }

                        this.pairableItem(company.pairableSetting);

                        this.webRequestEnumResource.listEnumResource({
                            companyId: WebConfig.userSession.currentCompanyId,
                            types: [103], //103 is MDVR Model
                            sortingColumns: DefaultSorting.EnumResource
                        }).done((r) => {
                            var enumsMDVR = r["items"];
                            this.enumsMDVROptions(enumsMDVR);
                            this.settings.MDVRModel(ScreenHelper.findOptionByProperty(this.enumsMDVROptions, "value", company.setting.mdvrModel));
                        }).fail((e) => {
                            d0.reject(e);
                        });
                        //Map Services
                        this.mapServiceIds(company.mapServices)
                        this.identifyService(company.identifyServiceId)
                    }
                }).fail((e) => {
                    d0.reject(e);
                });
                break;
        }

        $.when(d0).done(() => {
            this._changeCountrySubscribe = this.address.country.subscribe((country) => {
                if (country) {
                    this.isBusy(true);
                    this.address.province(null);
                    this.webRequestProvince.listProvince({
                        countryIds: [country.id],
                        sortingColumns: DefaultSorting.Province
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions.replaceAll(provinces);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                } else {
                    this.provinceOptions.replaceAll([]);
                }

                // check if regional has value, display M088 and clear cascade value
                // when user click save button, system should validate required regional fields and user need to review regional again
                if (!_.isNil(this.regional.languageId()) &&
                    !_.isNil(this.regional.timezone()) &&
                    !_.isNil(this.regional.currency()) &&
                    !_.isNil(this.regional.shortDateFormat()) &&
                    !_.isNil(this.regional.longDateFormat()) &&
                    !_.isNil(this.regional.shortTimeFormat()) &&
                    !_.isNil(this.regional.longTimeFormat())) {

                    this.showMessageBox(null, this.i18n("M088")(), BladeDialog.DIALOG_OK).done((button) => {
                        this.regional.languageId(null);
                        this.regional.timezone(null);
                        this.regional.timezoneName("");
                        this.regional.currency(null);
                        this.regional.shortDateFormat(null);
                        this.regional.longDateFormat(null);
                        this.regional.shortTimeFormat(null);
                        this.regional.longTimeFormat(null);
                    });
                }

                // publish message to regional screen
                this.publishMessage("bo-company-manage-country-changed", country);
            });

            this._changeProvinceSubscribe = this.address.province.subscribe((province) => {
                if (province) {
                    this.isBusy(true);
                    this.address.city(null);
                    this.webRequestCity.listCity({
                        provinceIds: [province.id],
                        sortingColumns: DefaultSorting.City
                    }).done((response) => {
                        var cities = response["items"];
                        this.cityOptions.replaceAll(cities);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                } else {
                    this.cityOptions.replaceAll([]);
                }
            });

            this._changeCitySubscribe = this.address.city.subscribe((city) => {
                if (city) {
                    this.isBusy(true);
                    this.address.town(null);
                    this.webRequestTown.listTown({
                        cityIds: [city.id],
                        sortingColumns: DefaultSorting.Town
                    }).done((response) => {
                        var towns = response["items"];
                        this.townOptions.replaceAll(towns);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                } else {
                    this.townOptions.replaceAll([]);
                }
            });
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {

        var self = this;

        this.logo.extend({
            trackChange: true
        });

        this.name.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.enable.extend({
            trackChange: true
        });

        this.address.houseNo.extend({
            trackChange: true
        });
        this.address.street.extend({
            trackChange: true
        });
        this.address.locate.extend({
            trackChange: true
        });
        this.address.country.extend({
            trackChange: true
        });
        this.address.province.extend({
            trackChange: true
        });
        this.address.city.extend({
            trackChange: true
        });
        this.address.town.extend({
            trackChange: true
        });
        this.address.postCode.extend({
            trackChange: true
        });
        this.address.latitude.extend({
            trackChange: true
        });
        this.address.longitude.extend({
            trackChange: true
        });

        this.subscriptions.extend({
            trackArrayChange: true
        });
        this.contacts.extend({
            trackArrayChange: true
        });

        this.regional.languageId.extend({
            trackChange: true
        });
        this.regional.timezone.extend({
            trackChange: true
        });
        this.regional.currency.extend({
            trackChange: true
        });
        this.regional.shortDateFormat.extend({
            trackChange: true
        });
        this.regional.longDateFormat.extend({
            trackChange: true
        });
        this.regional.shortTimeFormat.extend({
            trackChange: true
        });
        this.regional.longTimeFormat.extend({
            trackChange: true
        });
        this.regional.distanceUnit.extend({
            trackChange: true
        });
        this.regional.areaUnit.extend({
            trackChange: true
        });
        this.regional.weightUnit.extend({
            trackChange: true
        });
        this.regional.volumeUnit.extend({
            trackChange: true
        });
        this.regional.temperatureUnit.extend({
            trackChange: true
        });

        this.settings.companyAdminUsername.extend({
            trackChange: true
        });
        this.settings.companyAdminEmail.extend({
            trackChange: true
        });
        this.settings.adUrl.extend({
            trackChange: true
        });
        this.settings.adUsername.extend({
            trackChange: true
        });
        this.settings.adPassword.extend({
            trackChange: true
        });
        this.settings.applyPasswordRules.extend({
            trackChange: true
        });
        this.settings.gatewayServerPort.extend({
            trackChange: true
        });
        this.settings.databaseConnectionId.extend({
            trackChange: true
        });
        this.settings.minETADistance.extend({
            trackChange: true
        });
        this.settings.themeName.extend({
            trackChange: true
        });
        this.settings.logoSize.extend({
            trackChange: true
        });
        this.settings.keepTicketDuration.extend({
            trackChange: true
        });
        this.settings.satelliteBadGps.extend({
            trackChange: true
        });
        this.settings.continueTracking.extend({
            trackChange: true
        });
        this.settings.showPoiCluster.extend({
            trackChange: true
        });
        this.settings.MDVRModel.extend({
            trackChange: true
        });
        this.settings.sendGeofence.extend({
            trackChange: true
        });
        this.settings.enableAutomailFTP.extend({
            trackChange: true
        });
        this.settings.automailFTPPath.extend({
            trackChange: true
        });
        this.settings.enableOdometerFixing.extend({
            trackChange: true
        });

        this.settings.automailFTPPath.extend({
            required: {
                onlyIf: function() {
                    return self.isEnableAutomailFTP();
                }
            }
        });

        // validation
        this.logoSelectedFile.extend({
            fileExtension: ['JPG', 'JPEG', 'PNG'],
            fileSize: 4,
        });

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.address.country.extend({
            required: true
        });

        this.subscriptions.extend({
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });

        this.contactsValidator.extend({
            validation: {
                validator: (val, contacts) => {
                    var primaryContact = ko.utils.arrayFirst(contacts, (contact) => {
                        return contact.order === 1;
                    });
                    return !_.isNil(primaryContact) && !_.isNil(primaryContact.name) && !_.isNil(primaryContact.phone);
                },
                params: this.contacts,
                message: this.i18n("M001")()
            }
        });

        this.regionalValidator.extend({
            validation: {
                validator: function (val, regional) {
                    return regional.languageId() &&
                        regional.timezone() &&
                        regional.currency() &&
                        regional.shortDateFormat() &&
                        regional.longDateFormat() &&
                        regional.shortTimeFormat() &&
                        regional.longTimeFormat() &&
                        regional.distanceUnit() &&
                        regional.areaUnit() &&
                        regional.weightUnit() &&
                        regional.volumeUnit() &&
                        regional.temperatureUnit();
                },
                params: this.regional,
                message: this.i18n("M001")()
            }
        });

        this.settingsValidator.extend({
            validation: {
                validator: function (val, settings) {
                    return settings.companyAdminUsername() &&
                        settings.companyAdminEmail() &&
                        settings.gatewayServerPort();
                },
                params: this.settings,
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            logoSelectedFile: this.logoSelectedFile,
            name: this.name,
            country: this.address.country,
            subscriptions: this.subscriptions,
            contacts: this.contactsValidator,
            regional: this.regionalValidator,
            settings: this.settingsValidator,
            automailFTPPath : this.settings.automailFTPPath
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.DeleteCompany)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestCompany.createCompany(model, true).done((company) => {
                        this.isBusy(false);
                        this.publishMessage("bo-company-changed", company.id);
                        // publish message to refresh accessible companies, user menu for current users.
                        // to support case after first company has been created, switch portal menu item should be displayed in user menu.
                        // to support case after second company has been created, switch portal menu should display company-select blade instead of switch directly to first company.
                        this._eventAggregator.publish(EventAggregatorConstant.REFRESH_ACCESSIBLE_COMPANY);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestCompany.updateCompany(model).done(() => {
                        this.isBusy(false);
                        this.publishMessage("bo-company-changed", this.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M017")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestCompany.deleteCompany(this.id).done(() => {
                            this.isBusy(false);
                            this.publishMessage("bo-company-changed", this.id);
                            // publish message to refresh accessible companies, user menu for current users
                            // to support case after last company has been deleted, switch portal menu item shoud be hidden from user menu.
                            // to support case after second company (if there are 2 last companies) has been deleted, switch portal menu should switch to first company directly instead of display company-select blade.
                            this._eventAggregator.publish(EventAggregatorConstant.REFRESH_ACCESSIBLE_COMPANY);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        } else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_OK:
                            this.goToSubPage(index);
                            break;
                    }
                });
        }
    }

    goToSubPage(index) {
        var page = "";
        var options = {
            mode: this.mode === Screen.SCREEN_MODE_CREATE ? "company-create" : "company-update",
            companyId: this.id
        };

        switch (index) {
            case 1:
                page = "bo-company-manage-subscription";
                options.subscriptions = this.subscriptions();
                //send timezone to validate subscription has passed.
                options.timezone = this.regional.timezone() || this.address.country().localTimeZone;
                //send timezoneName to use in datepicker
                options.timezoneName = this.regional.timezoneName() || this.address.country().localTimeZoneName;
                break;
            case 2:
                page = "bo-company-manage-contact-manage";
                options.contacts = this.contacts();
                break;
            case 3:
                page = "bo-company-manage-regional-manage";
                options.regional = this.regional;
                options.country = this.address.country();
                break;
            case 4:
                page = "bo-company-manage-setting-manage";
                options.settings = this.settings;
                break;
            case 5:
                page = "bo-company-manage-mapservices-manage";
                options.mapServices = this.mapServiceIds();
                options.useNostraMap = this.useNostraMap();
                options.identifyServiceId = this.identifyService();
                break;
            case 6:
                page = "bo-company-manage-pairable-vehicle";
                options.pairableItem = this.pairableItem();
                break;
        }

        this.forceNavigate(page, options);
        this.selectedSubMenuItem(index);
    }

    generateModel() {

        let checkPairableInfo = this.pairableItem();
        if(checkPairableInfo){
            if(_.size(checkPairableInfo.mainDetectFeatures)){
                checkPairableInfo.mainDetectFeatures.forEach(item => {
                    item.status = item.status == 2 ? 0 : item.status;
                });
            }

            if(_.size(checkPairableInfo.pairingVehicleFeatures)){
                checkPairableInfo.pairingVehicleFeatures.forEach(item => {
                    item.status = item.status == 2 ? 0 : item.status;
                });
            }
        }        


        var model = {
            name: this.name(),
            code: this.code(),
            description: this.description(),
            logo: this.logo(),
            enable: this.enable().value,
            address: {
                houseNo: this.address.houseNo(),
                street: this.address.street(),
                locate: this.address.locate(),
                postCode: this.address.postCode(),
                countryId: this.address.country().id,
                provinceId: !_.isNil(this.address.province()) ? this.address.province().id : null,
                cityId: !_.isNil(this.address.city()) ? this.address.city().id : null,
                townId: !_.isNil(this.address.town()) ? this.address.town().id : null,
                latitude: this.address.latitude(),
                longitude: this.address.longitude(),
                infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : this.isAddressDirty() ? Enums.InfoStatus.Update : Enums.InfoStatus.Original,
                id: this.address.id
            },
            setting: {
                //regional
                languageId: this.regional.languageId(),
                timezone: this.regional.timezone(),
                currency: this.regional.currency(),
                shortDateFormat: this.regional.shortDateFormat(),
                longDateFormat: this.regional.longDateFormat(),
                shortTimeFormat: this.regional.shortTimeFormat(),
                longTimeFormat: this.regional.longTimeFormat(),
                distanceUnit: this.regional.distanceUnit(),
                areaUnit: this.regional.areaUnit(),
                weightUnit: this.regional.weightUnit(),
                volumeUnit: this.regional.volumeUnit(),
                temperatureUnit: this.regional.temperatureUnit(),
                //settings
                companyAdminId: this.settings.companyAdminId,
                companyAdminUsername: this.settings.companyAdminUsername(),
                companyAdminEmail: this.settings.companyAdminEmail(),
                adUrl: this.settings.adUrl(),
                adUsername: this.settings.adUsername(),
                adPassword: this.settings.adPassword(),
                applyPasswordRules: this.settings.applyPasswordRules(),
                gatewayServerPort: this.settings.gatewayServerPort(),
                databaseConnectionId: this.settings.databaseConnectionId(),
                infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : this.isRegionalDirty() || this.isSettingsDirty() ? Enums.InfoStatus.Update : Enums.InfoStatus.Original,
                id: this.settings.id,
                minETADistance: this.settings.minETADistance() ? this.settings.minETADistance() : null,
                themeName: this.settings.themeName() ? this.settings.themeName() : null,
                logoSize: this.settings.logoSize() ? this.settings.logoSize() : null,
                keepTicketDuration: this.settings.keepTicketDuration() ? this.settings.keepTicketDuration() : null,
                satelliteBadGPS: this.settings.satelliteBadGps() ? this.settings.satelliteBadGps() : null,
                continuousTrackingDuration: this.settings.continueTracking() ? this.settings.continueTracking() : null,
                showPoiCluster: this.settings.showPoiCluster(),
                mdvrModel: this.settings.MDVRModel() ? this.settings.MDVRModel().value : 0,
                SendGeoFenceInOut: this.settings.sendGeofence(),
                enableAutomailFTP: this.settings.enableAutomailFTP(),
                automailFTPPath: this.settings.automailFTPPath(),
                enableOdometerFixing: this.settings.enableOdometerFixing()
            },
            contacts: this.contacts(),
            logisticsSubscriptions: this.subscriptions(),
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.id,
            useNostraMap: this.useNostraMap(),
            mapServices: this.mapServiceIds(),
            identifyServiceId: this.identifyService(),
            pairableSetting: this.pairableItem()
        };
        return model;
    }

    /**
     * Check address fields is dirty to set infoStatus
     * @returns
     */
    isAddressDirty() {
        return this.address.houseNo.isDirty() ||
            this.address.street.isDirty() ||
            this.address.locate.isDirty() ||
            this.address.country.isDirty() ||
            this.address.province.isDirty() ||
            this.address.city.isDirty() ||
            this.address.town.isDirty() ||
            this.address.postCode.isDirty() ||
            this.address.latitude.isDirty() ||
            this.address.longitude.isDirty();
    }

    /**
     * Check regional fields is dirty to set infoStatus
     * @returns
     */
    isRegionalDirty() {
        return this.regional.languageId.isDirty() ||
            this.regional.timezone.isDirty() ||
            this.regional.currency.isDirty() ||
            this.regional.shortDateFormat.isDirty() ||
            this.regional.longDateFormat.isDirty() ||
            this.regional.shortTimeFormat.isDirty() ||
            this.regional.longTimeFormat.isDirty() ||
            this.regional.distanceUnit.isDirty() ||
            this.regional.areaUnit.isDirty() ||
            this.regional.weightUnit.isDirty() ||
            this.regional.volumeUnit.isDirty() ||
            this.regional.temperatureUnit.isDirty();
    }

    /**
     * Check settings fields is dirty to set infoStatus
     * @returns
     */
    isSettingsDirty() {
        return this.settings.companyAdminUsername.isDirty() ||
            this.settings.companyAdminEmail.isDirty() ||
            this.settings.adUrl.isDirty() ||
            this.settings.adUsername.isDirty() ||
            this.settings.adPassword.isDirty() ||
            this.settings.applyPasswordRules.isDirty() ||
            this.settings.gatewayServerPort.isDirty() ||
            this.settings.databaseConnectionId.isDirty() ||
            this.settings.minETADistance.isDirty() ||
            this.settings.themeName.isDirty() ||
            this.settings.logoSize.isDirty() ||
            this.settings.keepTicketDuration.isDirty() ||
            this.settings.satelliteBadGps.isDirty() ||
            this.settings.continueTracking.isDirty() ||
            this.settings.showPoiCluster.isDirty() ||
            this.settings.MDVRModel.isDirty() ||
            this.settings.sendGeofence.isDirty() ||
            this.settings.enableAutomailFTP.isDirty() ||
            this.settings.automailFTPPath.isDirty() ||
            this.settings.enableOdometerFixing.isDirty();
    }


    /**
     * On click remove logo icon
     */
    onRemoveFile() {
        var logo = this.logo();
        if (logo) {
            if (logo.infoStatus === Enums.InfoStatus.Add) {
                this.logo(null);
            } else {
                logo.infoStatus = Enums.InfoStatus.Delete;
                this.logo(logo);
            }
        }
    }

    /**
     * Hook onbefore fileupload
     */
    onBeforeUpload() {
        //remove current file before upload new file
        this.onRemoveFile();
    }

    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccess(data) {
        if (data && data.length > 0) {
            var newLogo = data[0];
            var currentLogo = this.logo();

            if (currentLogo) {
                switch (currentLogo.infoStatus) {
                    case Enums.InfoStatus.Add:
                        newLogo.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        newLogo.infoStatus = Enums.InfoStatus.Update;
                        newLogo.id = currentLogo.id;
                        break;
                }
            } else {
                newLogo.infoStatus = Enums.InfoStatus.Add;
            }

            this.logo(newLogo);
        }
    }


    /**
     *
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFail(e) {
        this.handleError(e);
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanyManageScreen),
    template: templateMarkup
};