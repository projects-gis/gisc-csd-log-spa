import ko from "knockout";
import templateMarkup from "text!./pairable-vehicle.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestMapServices from "../../../../app/frameworks/data/apicore/WebRequestMapServices";
import {
    Constants,
    Enums
} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import WebRequestFeature from "../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import Utility from '../../../../app/frameworks/core/utility';

/**
 * 
 * 
 * @class CompanySettingManageScreen
 * @extends {ScreenBase}
 */
class CompanyPairableVehicleScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle('Pairable Vehicle');
        this.bladeSize = BladeSize.Small;
        this.pairableItem = this.ensureNonObservable(_.cloneDeep(params.pairableItem), null);

        this.vehicleType = ko.observable();
        this.ddlType = ko.observable();

        this.isInsidePOI = ko.observable();
        this.maxDistance = ko.observable(10);
        this.maxDistanceTrackLocation = ko.observable(300);
        this.typeOptions = ko.observableArray([]);
        //this.selectedFeature = ko.observable();
        //this.selectedStatus = ko.observable();
        this.features = [];
        this.status = [
             {name: 'On', value: 1},
             {name: 'Off', value: 2}
        ];
        this.detectFeatureMain = ko.observableArray([]);
        this.detectFeaturePair = ko.observableArray([]);

        this.order = ko.observable([[ 0, "asc" ]]);

    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Feature module in Web API access.
     * @readonly
     */
    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var boxFeatureDfd = this.webRequestFeature.listFeature({
            sortingColumns: DefaultSorting.Feature
        });

        var enumRequest = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.VehicleType],
            sortingColumns: DefaultSorting.EnumResource
        })
        
        $.when(enumRequest,boxFeatureDfd).done((response,resFeaure) => {
            this.typeOptions(response.items);
            this.features = _.filter(resFeaure.items,(feature) => {
                return feature.eventDataType == Enums.ModelData.EventDataType.Digital
            });

            if(this.pairableItem){
                let item = this.pairableItem;
                this.isInsidePOI(item.isInsidePOI);
                this.maxDistance(item.maxDistance);
                this.maxDistanceTrackLocation(item.maxDifferenceTrackLocation);
                this.vehicleType(ScreenHelper.findOptionByProperty(this.typeOptions, "value", item.mainVehicleTypeId));
                this.ddlType(ScreenHelper.findOptionByProperty(this.typeOptions, "value", item.pairingVehicleTypeId));

                if(_.size(item.mainDetectFeatures)){
                    let detectFeatureItem = [];
                    _.forEach((item.mainDetectFeatures),(df) => {
                        df.features = this.features;
                        df.statuses = this.status;
                        df.selectedFeature = df.id;
                        df.selectedStatus = df.status;
                        detectFeatureItem.push(df);
                    });
                    this.detectFeatureMain(detectFeatureItem);
                }

                if(_.size(item.pairingVehicleFeatures)){
                    let detectFeatureItem = [];
                    _.forEach((item.pairingVehicleFeatures),(df) => {
                        df.features = this.features;
                        df.statuses = this.status;
                        df.selectedFeature = df.id;
                        df.selectedStatus = df.status;
                        detectFeatureItem.push(df);
                    });
                    this.detectFeaturePair(detectFeatureItem);
                }

            }
        });

    }

    /**
     * Get WebRequest for Company in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }


    /**
     * Get WebRequest for DatabaseConnection in Web API access.
     * @readonly
     * @memberOf CompanySettingManageScreen
     */

    get webRequestGISServiceConfigurations() {
        return WebRequestGISServiceConfigurations.getInstance();
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;

        this.vehicleType.extend({
            required: true
        });

        this.maxDistance.extend({
            required: true
        });

        this.maxDistanceTrackLocation.extend({
            required: true
        });

        this.ddlType.extend({
            validation:{
                validator: (val) => {
                    let checkDup = true;
                    if(self.vehicleType() && val){
                        checkDup = val.value != self.vehicleType().value
                    }
                    return checkDup;
                },
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            vehicleType: this.vehicleType,
            maxDistance: this.maxDistance,
            maxDistanceTrackLocation : this.maxDistanceTrackLocation,
            ddlType: this.ddlType
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) { }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
            }

            let filterErrorMain = _.filter(this.detectFeatureMain(),(item) => {
                return item.error || item.errorStatus ;
            });

            let filterErrorPair = _.filter(this.detectFeaturePair(),(item) => {
                return item.error || item.errorStatus ;
            });

            if(_.size(filterErrorMain)) return;
            if(_.size(filterErrorPair)) return;

            var modelData = this.generateModel()
            this.publishMessage("bo-company-pairable-changed", modelData);
            this.close(true); 
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) { }

    addDetectFeatureMain(index){
         let tmpData = [];

        if (index == 1){
            tmpData = _.clone(this.detectFeatureMain());
            this.detectFeatureMain([]);

            this.addFeature(tmpData);

            this.detectFeatureMain.replaceAll(tmpData);
        }else{      
            tmpData = _.clone(this.detectFeaturePair());
            this.detectFeaturePair([]);

            this.addFeature(tmpData);

            this.detectFeaturePair.replaceAll(tmpData);
        }
    }

    generateModel() {
        let featureMain = _.map(this.detectFeatureMain(),(item) => {
                return {
                        id: item.selectedFeature,
                        status: item.selectedStatus
                    }
            });

        let featurePair = _.map(this.detectFeaturePair(),(item) => {
                return {
                    id: item.selectedFeature,
                    status: item.selectedStatus
                }
            });

        let model = {
            isInsidePOI : this.isInsidePOI(),
            mainDetectFeatures : featureMain,
            pairingVehicleFeatures : featurePair,
            mainVehicleTypeId :  this.vehicleType().value,
            maxDistance :  this.maxDistance(),
            maxDifferenceTrackLocation :  this.maxDistanceTrackLocation(),
            pairingVehicleTypeId : this.ddlType() ? this.ddlType().value : null,
        }
        return model;
    }

    onDetectFeatureMainChanged(type,item){
        let tmpFeature = [];

        switch (type) {
            case 'main':
                tmpFeature = _.clone(this.detectFeatureMain());
                this.detectFeatureMain([]);

                this.checkErrorItems(tmpFeature);
                
                this.detectFeatureMain(tmpFeature);
                break;
            case 'pair':
                tmpFeature = _.clone(this.detectFeaturePair());
                this.detectFeaturePair([]);

                this.checkErrorItems(tmpFeature);
                
                this.detectFeaturePair(tmpFeature);
                break;
            default:
                break;
        }
        
    }

    addFeature(tmpData){
        let maxId = tmpData.length > 0 ? maxId = _.maxBy(tmpData,'id').id + 1 
                                       : 1;
        tmpData.push({
            id: maxId,
            features: this.features,
            statuses: this.status,
            error: this.i18n("M001")(),
            errorStatus: this.i18n("M001")()
        });
    }

    checkErrorItems(items){
        var dupicateIds = [];
        items.forEach(data => {
            if(data.selectedFeature){
                data.error = null;
                let findVal = dupicateIds.find((val) => {return val == data.selectedFeature})
                if(findVal){
                    data.error = this.i18n("M010")();
                }
                else{
                    dupicateIds.push(data.selectedFeature);
                }
            }
            else{
                data.error = this.i18n("M001")();
            }

            if(data.selectedStatus){
                data.errorStatus = null;
            }
            else{
                data.errorStatus = this.i18n("M001")();
            }


        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanyPairableVehicleScreen),
    template: templateMarkup
};