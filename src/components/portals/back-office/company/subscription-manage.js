﻿import ko from "knockout";
import templateMarkup from "text!./subscription-manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestSubscription from "../../../../app/frameworks/data/apicore/webRequestSubscription";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";

/**
 * Company > Create/Update > Subscriptions > Add/Update
 * 
 * @class CompanySubscriptionManageScreen
 * @extends {ScreenBase}
 */
class CompanySubscriptionManageScreen extends ScreenBase {
    
    /**
     * Creates an instance of CompanySubscriptionManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        switch (this.ensureNonObservable(params.mode)) {
            case "company-create-subscription-add":
                this.companyMode = Screen.SCREEN_MODE_CREATE;
            case "company-update-subscription-add":
                this.bladeTitle(this.i18n("Companies_AddSubscription")());
                this.mode = Screen.SCREEN_MODE_CREATE;
                this.companyMode = Screen.SCREEN_MODE_UPDATE;
                break;
            case "company-create-subscription-update":
                this.companyMode = Screen.SCREEN_MODE_CREATE;
            case "company-update-subscription-update":
                this.bladeTitle(this.i18n("Common_UpdateSubscription")());
                this.mode = Screen.SCREEN_MODE_UPDATE;
                this.companyMode = Screen.SCREEN_MODE_UPDATE;
                break;
        }

        this.companySubscriptions = this.ensureNonObservable($.extend(true, [], params.subscriptions));
        this.companySubscription = this.ensureNonObservable($.extend(true, {}, params.subscription));

        this.subscription = ko.observable();

        this.canUpdate = this.companySubscription.isCanUpdate;

        // Date
        this.fromDate = ko.observable(this.companySubscription.startDate);
        this.toDate = ko.observable(this.companySubscription.endDate);
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.timezone = this.ensureNonObservable(params.timezone);
        this.timezoneName = this.ensureNonObservable(params.timezoneName);

        this.maxPOI = ko.observable(this.companySubscription.maxPoi);
        this.maxArea = ko.observable(this.companySubscription.maxArea);
        this.maxRoute = ko.observable(this.companySubscription.maxRoute);
        this.alertExpiration = ko.observable(this.companySubscription.alertExpiration);
        this.dataExpiration = ko.observable(this.companySubscription.dataExpiration);

        this.subscriptionOptions = ko.observableArray([]);
    }
    
    /**
     * Get WebRequest for Subscription in Web API access.
     * @readonly
     */
    get webRequestSubscription() {
        return WebRequestSubscription.getInstance();
    }
    
    /**
     * Get WebRequest for Company in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d0 = $.Deferred();

        // also include selected subscription id for case update to ensure option still remain if subscription is disabled.
        var includeIds = this.mode === Screen.SCREEN_MODE_UPDATE ? [this.companySubscription.subscriptionId] : [];
        this.webRequestSubscription.listSubscription({ enable: true, includeIds: includeIds, sortingColumns: DefaultSorting.Subscription }).done((response) => {
            this.subscriptionOptions(response["items"]);

            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                this.subscription(ScreenHelper.findOptionByProperty(this.subscriptionOptions, "id", this.companySubscription.subscriptionId));
            }

            d0.resolve();
        }).fail((e) => {
            d0.reject(e);
        });

        $.when(d0).done(() => {
            this._changeSubscriptionSubscribe = this.subscription.subscribe((subscription) => {
                if (subscription) {
                    this.maxPOI(subscription.maxPoi);
                    this.maxArea(subscription.maxArea);
                    this.maxRoute(subscription.maxRoute);
                    this.alertExpiration(subscription.alertExpiration);
                    this.dataExpiration(subscription.dataExpiration);
                }
                else {
                    this.maxPOI(null);
                    this.maxArea(null);
                    this.maxRoute(null);
                    this.alertExpiration(null);
                    this.dataExpiration(null);
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.subscription.extend({ trackChange: true });
        this.fromDate.extend({ trackChange: true });
        this.toDate.extend({ trackChange: true });
        this.maxPOI.extend({ trackChange: true });
        this.maxArea.extend({ trackChange: true });
        this.maxRoute.extend({ trackChange: true });
        this.alertExpiration.extend({ trackChange: true });
        this.dataExpiration.extend({ trackChange: true });

        // Validation
        this.subscription.extend({ required: true });
        this.fromDate.extend({ required: true });
        this.toDate.extend({ required: true });
        this.maxPOI.extend({ required: true, min: 0 });
        this.maxArea.extend({ required: true, min: 0 });
        this.maxRoute.extend({ required: true, min: 0 });
        this.alertExpiration.extend({ required: true, min: 1 });
        this.dataExpiration.extend({ required: true, min: 1 });

        this.validationModel = ko.validatedObservable({
            subscription: this.subscription,
            fromDate: this.fromDate,
            toDate: this.toDate,
            maxPOI: this.maxPOI,
            maxArea: this.maxArea,
            maxRoute: this.maxRoute,
            alertExpiration: this.alertExpiration,
            dataExpiration: this.dataExpiration
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (this.canUpdate) {
            actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
            actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
        }
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === "update" && this.canUpdate) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            this.companySubscription.subscriptionId = this.subscription().id;
            this.companySubscription.subscriptionName = this.subscription().name;
            this.companySubscription.startDate = this.fromDate();
            this.companySubscription.endDate = this.toDate();
            this.companySubscription.maxPoi = this.maxPOI();
            this.companySubscription.maxArea = this.maxArea();
            this.companySubscription.maxRoute = this.maxRoute();
            this.companySubscription.alertExpiration = this.alertExpiration();
            this.companySubscription.dataExpiration = this.dataExpiration();
            this.companySubscription.infoStatus = this.companySubscription.id > 0 ? Enums.InfoStatus.Update : Enums.InfoStatus.Add;

            var companySubscriptions = $.extend(true, [], this.companySubscriptions);

            if (this.mode === Screen.SCREEN_MODE_CREATE) {
                companySubscriptions.push(this.companySubscription);
            } else {
                var index = _.findIndex(companySubscriptions, (item) => {
                    return item.id == this.companySubscription.id;
                });
                companySubscriptions.splice(index, 1, this.companySubscription);
            }

            //send timezone to validate subscription has passed.
            var companyInfo = {
                setting: {
                    timezone: this.timezone
                },
                logisticsSubscriptions: companySubscriptions
            };
            
            this.webRequestCompany.validateCompanySubscription(companyInfo).done((response) => {
                this.isBusy(false);
                this.publishMessage("bo-company-subscription-changed", { subscriptions: response, id: this.companySubscription.id });
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M107")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.publishMessage("bo-company-subscription-deleted", this.companySubscription);
                        this.close(true);
                        break;
                }
            });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanySubscriptionManageScreen),
    template: templateMarkup
};