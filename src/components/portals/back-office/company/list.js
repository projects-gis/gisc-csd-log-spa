﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";

/**
 * Company > List 
 * 
 * @class CompanyListScreen
 * @extends {ScreenBase}
 */
class CompanyListScreen extends ScreenBase {
    
    /**
     * Creates an instance of CompanyListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Companies_CompaniesPageTitle")());
        this.bladeSize = BladeSize.Medium;

        this.companies = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.selectedCompany = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (company) => {
            if(company && WebConfig.userSession.hasPermission(Constants.Permission.UpdateCompany)) {
                return this.navigate("bo-company-manage", { mode: "update", id: company.id });
            }
            return false;
        };

        // Subscribe Message when company created/updated
        this.subscribeMessage("bo-company-changed", (companyId) => {
            this.webRequestCompany.listCompanySummary(this.generateCompanyFilter()).done((response) => {
                var companies = response["items"];
                this.companies.replaceAll(companies);
                this.recentChangedRowIds.replaceAll([companyId]);
            });
        });
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();
        this.webRequestCompany.listCompanySummary(this.generateCompanyFilter()).done((response) => {
            var companies = response["items"];
            this.companies(companies);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedCompany(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateCompany)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-company-manage", { mode: "create" });
        }
        this.selectedCompany(null);
        this.recentChangedRowIds.removeAll();
    }

    generateCompanyFilter() {
        return {
            sortingColumns: DefaultSorting.Company
        };
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanyListScreen),
    template: templateMarkup
};