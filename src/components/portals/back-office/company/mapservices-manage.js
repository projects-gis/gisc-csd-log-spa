﻿import ko from "knockout";
import templateMarkup from "text!./mapservices-manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestMapServices from "../../../../app/frameworks/data/apicore/WebRequestMapServices";
import {
    Constants,
    Enums
} from "../../../../app/frameworks/constant/apiConstant";
import WebRequestGISServiceConfigurations from "../../../../app/frameworks/data/apitrackingcore/webRequestGISServiceConfigurations";

/**
 * 
 * 
 * @class CompanySettingManageScreen
 * @extends {ScreenBase}
 */
class CompanySettingManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Companies_CompanyMapservices")());
        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = this.ensureObservable(params.companyId);
        this.bladeSize = BladeSize.Medium;

        this.isValidationAddition = ko.observable(false);
        this.isValidationIdentify = ko.observable(false);
        this.useNostraMap = ko.observable(params.useNostraMap);

        this.logoSuggestionWidth = "52";
        this.logoSuggestionHeight = "52";

        this.isEnableIdentify = ko.observable(!params.useNostraMap);

        this.mapDefault = ko.observableArray([]);
        this.mapAdditional = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([]);

        this._selectedMapServices = this.ensureNonObservable(params.mapServices, []);
        this._selectedItemsSubscribe = this.selectedItems.subscribe((comps) => {
            this._selectedMapServices = comps.map((obj) => {
                return obj.id;
            });
        });

        this.identifyOptions = ko.observableArray([]);
        this.identify = ko.observable(params.identifyServiceId, null);

        this.useNostraMap.subscribe((param) => {
            this.identify(null);
            this.isEnableIdentify(!param)
        })
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;


        var dfd = $.Deferred();
        var filter = {
            companyId: this.companyId()
        }
        var dfdGISService = this.webRequestGISServiceConfigurations.gisServiceList();
        var dfdDefaultMapService = this.webRequestMapServices.listDefaultMapServices(filter);
        var dfdAdditionalMapService = this.webRequestMapServices.listAdditionalMapServices();
        $.when(dfdDefaultMapService, dfdAdditionalMapService, dfdGISService).done((responseDefault, responseAdditional, responseGISService) => {
            this.mapDefault(responseDefault["items"]);
            this.mapAdditional(responseAdditional["items"]);
            this.identifyOptions(responseGISService["items"]);

            this.identify(ScreenHelper.findOptionByProperty(this.identifyOptions, "id", this.identify()));
            // let maxLevel = !_.isNil(response.maxLevel) ? ScreenHelper.findOptionByProperty(this.maxLevelOptions, "value", response.maxLevel) : null;
            // this.maxLevel(maxLevel)
            // this.identify(ScreenHelper.findOptionByProperty(this.countryOptions, "id", company.address.countryId));


            // If there is _selectedMapServices, set pre-selected value
            this._selectedMapServices.forEach(function (element) {
                let mapservice = ko.utils.arrayFirst(this.mapAdditional(), function (map) {
                    return map.id === element.mapServiceId;
                });
                if (mapservice) {
                    this.selectedItems.push(mapservice);
                }
            }, this);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });



        return dfd;
    }

    /**
     * Get WebRequest for Company in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }


    /**
     * Get WebRequest for DatabaseConnection in Web API access.
     * @readonly
     * @memberOf CompanySettingManageScreen
     */
    get webRequestMapServices() {
        return WebRequestMapServices.getInstance();
    }

    get webRequestGISServiceConfigurations() {
        return WebRequestGISServiceConfigurations.getInstance();
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            // if (!this.validationModel.isValid()) {
            //     this.validationModel.errors.showAllMessages();
            //     return;
            // }
            if (!this.useNostraMap()) {
                if (this.selectedItems().length == 0) {
                    this.isValidationAddition(this.selectedItems().length == 0 ? true : false)
                    return
                }

                if (this.identify() === undefined) {
                    this.isValidationIdentify(this.identify() == undefined ? true : false)
                    return
                }
                
            }
            this.isBusy(true);
            let dataMapServices = {
                mapServices: this.selectedItems(),
                useNostraMap: this.mapDefault().length != 0 ? this.useNostraMap() : true,
                identify: this.identify() ? this.identify().id : null
            }

            this.publishMessage("bo-company-mapservices-changed", dataMapServices);
            this.isBusy(false);
            this.close(true);
        }

    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}


}

export default {
    viewModel: ScreenBase.createFactory(CompanySettingManageScreen),
    template: templateMarkup
};