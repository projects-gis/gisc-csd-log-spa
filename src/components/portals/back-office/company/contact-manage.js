﻿import ko from "knockout";
import templateMarkup from "text!./contact-manage.html";
import ScreenBase from "../../screenbase";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
/**
 * 
 * 
 * @class CompanyContactManageScreen
 * @extends {ScreenBase}
 */
class CompanyContactManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Companies_CompanyContacts")());
        this.contacts = this.ensureNonObservable($.extend(true, [], params.contacts));

        // Primary
        this.primaryName = ko.observable("");
        this.primaryEmail = ko.observable("");
        this.primaryPhone = ko.observable("");
        // Secondary
        this.secondaryName = ko.observable("");
        this.secondaryEmail = ko.observable("");
        this.secondaryPhone = ko.observable("");
        // Additional
        this.additionalName = ko.observable("");
        this.additionalEmail = ko.observable("");
        this.additionalPhone = ko.observable("");

        ko.utils.arrayForEach(this.contacts, (contact) => {
            switch (contact.order) {
                case 1:
                    this.primaryName(contact.name || "");
                    this.primaryEmail(contact.email || "");
                    this.primaryPhone(contact.phone || "");
                    break;
                case 2:
                    this.secondaryName(contact.name || "");
                    this.secondaryEmail(contact.email || "");
                    this.secondaryPhone(contact.phone || "");
                    break;
                case 3:
                    this.additionalName(contact.name || "");
                    this.additionalEmail(contact.email || "");
                    this.additionalPhone(contact.phone || "");
                    break;
            }
        });
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }

    /**
     * Get WebRequest for Company in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.primaryName.extend({ trackChange: true });
        this.primaryEmail.extend({ trackChange: true });
        this.primaryPhone.extend({ trackChange: true });
        this.secondaryName.extend({ trackChange: true });
        this.secondaryEmail.extend({ trackChange: true });
        this.secondaryPhone.extend({ trackChange: true });
        this.additionalName.extend({ trackChange: true });
        this.additionalEmail.extend({ trackChange: true });
        this.additionalPhone.extend({ trackChange: true });

        // Validation
        this.primaryName.extend({ required: true });
        this.primaryEmail.extend({ email: true });
        this.primaryPhone.extend({ required: true });
        this.secondaryEmail.extend({ email: true });
        this.additionalEmail.extend({ email: true });

        this.validationModel = ko.validatedObservable({
            primaryName: this.primaryName,
            primaryEmail: this.primaryEmail,
            primaryPhone: this.primaryPhone,
            secondaryEmail: this.secondaryEmail,
            additionalEmail: this.additionalEmail
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var contacts = $.extend(true, [], this.contacts);

            ko.utils.arrayForEach(contacts, (contact) => {
                switch (contact.order) {
                    case 1:
                        contact.name = this.primaryName();
                        contact.email = this.primaryEmail(),
                        contact.phone = this.primaryPhone();
                        break;
                    case 2:
                        contact.name = this.secondaryName();
                        contact.email = this.secondaryEmail(),
                        contact.phone = this.secondaryPhone();
                        break;
                    case 3:
                        contact.name = this.additionalName();
                        contact.email = this.additionalEmail(),
                        contact.phone = this.additionalPhone();
                        break;
                }
            });

            this.webRequestCompany.validateCompanyContact(contacts).done(() => {
                this.publishMessage("bo-company-contact-changed", contacts);
                this.isBusy(false);
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanyContactManageScreen),
    template: templateMarkup
};