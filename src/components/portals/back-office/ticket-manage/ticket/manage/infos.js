import ko from "knockout";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../../app/frameworks/constant/defaultSorting";
import {Enums,EntityAssociation} from "../../../../../../app/frameworks/constant/apiConstant";

class FleetServiceFilter {
    constructor (companyId = null,businessUnitIds = null) {
        this.companyId = companyId;
        this.sortingColumns = DefaultSoring.FleetService;

        if(businessUnitIds){
            this.businessUnitIds = businessUnitIds;
        }else{
            this.businessUnitIds = [];
        }
    }
}
export { FleetServiceFilter };

class BusinessUnitFilter {
    constructor (companyId = null) {
        this.companyId = companyId;
        this.sortingColumns = DefaultSoring.BusinessUnit;
        this.includeAssociationNames = [
            EntityAssociation.BusinessUnit.ChildBusinessUnits
        ];
    }
}
export { BusinessUnitFilter };

class PromotionFilter{
    constructor (companyId = null) {
        this.companyId = companyId;
        this.sortingColumns = DefaultSoring.OperatorPackageByOperatorPromotion;
    }
}
export { PromotionFilter };

class BoxFilter {
    constructor (companyId = null, businessUnitId = null, includeId = null) {
        this.companyId = companyId;
        this.sortingColumns = DefaultSoring.BoxBySerialNo;

        if(businessUnitId){
            this.businessUnitIds = [businessUnitId];
        }else{
            this.businessUnitIds = [];
        }

        this.isAssociatedWithFleetService = false;
        this.statuses = [Enums.ModelData.BoxStatus.Ready];

        if(includeId){
            this.includeIds = [includeId];
        }
    }
}
export { BoxFilter };

class VehicleFilter {
    constructor (companyId = null, businessUnitId = null, includeId = null) {
        this.companyId = companyId;
        this.sortingColumns = DefaultSoring.Vehicle;

        if(businessUnitId){
            this.businessUnitIds = [businessUnitId];
        }else{
            this.businessUnitIds = [];
        }

        this.enable = true;
        this.isAssociatedWithFleetService = false;

        if(includeId){
            this.includeIds = [includeId];
        }
    }
}
export { VehicleFilter };