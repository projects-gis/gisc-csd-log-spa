﻿import ko from "knockout";
import templateMarkup from "text!./ticket-new.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestUser from "../../../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../../app/frameworks/core/utility";
import GenerateDataGridExpand from "../../../../company-workspace/generateDataGridExpand";
import WebRequestCustomPOI from "../../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import { AssetIcons } from "../../../../../../app/frameworks/constant/svg";

class TicketNewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this._serviceType = params.serviceType;
        this._ownerId = params.ownerId;
        this._assignToId = params.assignToId;
        this._status = params.status;
        this._duration = params.duration;
        this._contractNo = params.contractNo;
        this._companyId = params.companyId;
        this._isNavigate = (typeof params.isNavigate != "undefined") ? params.isNavigate : false;

        this.bladeTitle(this.i18n("Ticket_Management")());
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        this.duration = this.ensureNonObservable(params.ticketDuration);
        this.data = this.ensureNonObservable(params.data);
        this.companyId = (this.data) ? this.data.CompanyId : null;
        this.selectedItems = ko.observableArray([]);
        this.statusTicket = ko.observableArray([]);
        this.statusTicketDtg = ko.observableArray([]);
        this.durationTicket = ko.observableArray([]);
        this.assignTo = ko.observableArray([]);
        this.serviceType = ko.observableArray([]);
        this.selectedService = ko.observable();
        this.selectedAssign = ko.observable();
        this.selectedOwner = ko.observable();
        this.selectedStatus = ko.observable();
        this.selectedDuration = ko.observable();
        this.contract = ko.observable();
        this.apiDataSource = ko.observableArray([]);
        this.isEnable = ko.observable(false);
        this.getCurrentDataSource = ko.observableArray([]);

        // gen header icon
        this.headerTemplateIcon = _.template('<div class="<%= iconCssClass %>" title="<%= title %>"><%= icon %></div>');
        this.headerTemplateGPS = this.headerTemplateIcon({ 'iconCssClass': 'icon-gps', 'title': this.i18n("Ticket_GPSStatus")(), 'icon': AssetIcons.IconGpsTemplateMarkup });


        var self = this;
        this.validationContractNo = {
            contractNo: function (input) {
                var row = input.closest("tr");
                var inputText = (input.val()) ? true : false;
                if (!inputText) {
                    input.attr("data-contractNo-msg", self.i18n("Ticket_Management")());
                }
                return inputText;
            }
        };

        this.serviceItemAutoCompleteEditor = (container, options) => {
            // create an input element
            var input = $("<input/>");
            // set its name to the field to which the column is bound ('name' in this case)
            input.attr("name", options.field);
            // append it to the container
            input.appendTo(container);
            // initialize a Kendo UI AutoComplete
            input.kendoAutoComplete({
                dataTextField: "name",
                dataValueField: "code",
                minLength: 3,
                dataSource: {
                    transport: {
                        read: this.onDatasourceRequestAutoComplete,
                        // conver params to JSON.stringify and pass to service
                        parameterMap: function (options, operation) {
                            return JSON.stringify(options);
                        }
                    },
                    serverFiltering: true
                },
                change: function (e) {
                    //set name and value(id) to object
                    var dataItem = e.sender.dataItem();
                    if (_.isObject(dataItem)) {
                        options.model.set("formatContractEndID", dataItem.code);
                        options.model.set("formatContractEnd", dataItem.name);
                    }
                },
            });
        }

        this.onDatasourceRequestAutoComplete = (request) => {
            var name = null;
            if (request.data.filter.filters.length > 0) {
                name = request.data.filter.filters[0].value;
            }
            this.webRequestCustomPOI.autoComplete({
                companyId: 1,
                name: name
            }).done((response) => {
                request.success(response);
            }).fail((e) => {
                console.log(e);
            });
        }



        //variable for model
        this.businessUnitId2 = ko.observable("");




        this.viewLicense = (data) => {
            this.navigate("bo-ticket-manage-asset-vehicle-info",
                {
                    companyId: data.companyId,
                    vehicleId: data.vehicleId,
                    vehicleLicense: data.license,
                    serialNo: data.serialNo,
                });
        };

        this.viewTicket = (data) => {
            this.navigate("bo-ticket-manage-asset-details", { ticketId: data.ticketId })
        };

        this.editorServiceType = (container, options) => {
            $('<input data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: this.serviceType(),
                    value: options.model.serviceType,
                    change: function (e) {
                        var dataItem = e.sender.dataItem();
                        options.model.set("serviceType", dataItem.id);
                        options.model.set("serviceTypeDisplayName", dataItem.name);
                    },
                    optionLabel: {
                        name: " ",
                        value: null
                    }
                });
        }

        this.templateServiceType = (data) => {
            // console.log(data);
            // return kendo.format("<span title='{0}'>{0}</span>", data.serviceTypeDisplayName);
            return (data.serviceTypeDisplayName == null) ? " " : data.serviceTypeDisplayName;
        }

        this.editorAssignToId = (container, options) => {
            $('<input data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataTextField: "displayName",
                    dataValueField: "id",
                    dataSource: this.assignTo(),
                    value: options.model.assignToId,
                    change: function (e) {
                        var dataItem = e.sender.dataItem();
                        options.model.set("assignToId", dataItem.id);
                        options.model.set("assignTo", dataItem.displayName);
                    },
                    optionLabel: {
                        displayName: " ",
                        id: null
                    }
                });
        }

        this.templateAssignToId = (data) => {
            // return kendo.format("<span title='{0}'>{0}</span>", data.serviceTypeDisplayName);
            return (data.assignTo == null) ? " " : data.assignTo;
        }

        this.editorTicketStatus = (container, options) => {
            $('<input data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "value",
                    dataSource: this.statusTicketDtg(),
                    value: options.model.status,
                    change: function (e) {
                        var dataItem = e.sender.dataItem();
                        options.model.set("status", dataItem.value);
                        options.model.set("statusDisplayName", dataItem.name);
                    }
                });
        }

        this.templateTicketStatus = (data) => {
            // return kendo.format("<span title='{0}'>{0}</span>", data.serviceTypeDisplayName);
            return data.statusDisplayName;
        }


        this.subscribeMessage("update-items-appointment", (appointId) => {
            if (this.data != null) {
                this.data.Status = Enums.ModelData.TicketStatus.New;
                this.data.Duration = this.duration;
            }
            else {
                this.data = {
                    Status: Enums.ModelData.TicketStatus.New,
                    Duration: this.duration
                };
            }

            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(this.data)
            });
        });

        this.subscribeMessage("bo-ticket-new-search-changed", (data) => {
            //set params
            this.data = data;
            this.duration = data.duration;
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(data),
            });
        });
        // Observe change about filter
        this.subscribeMessage("bo-ticket-edit-multiple-service-changed", (data) => {

            this.selectedItems().length = 0;
            let dataFilter = this.checkData(this.data);
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(dataFilter)
            });
        });

        this.viewSearch = () => {

            let OwnerId = (typeof this.selectedOwner() == "undefined") ? null : this.selectedOwner().id;
            let Assign = (typeof this.selectedAssign() == "undefined") ? null : this.selectedAssign().id;
            let ServiceId = (typeof this.selectedService() == "undefined") ? null : this.selectedService().id;
            let Status = (typeof this.selectedStatus() == "undefined") ? null : this.selectedStatus().value;
            let Duration = (typeof this.selectedDuration() == "undefined") ? null : this.selectedDuration().value;
            let ContractNo = (typeof this.contract() == "underfined") ? null : this.contract();
            let companyId = (typeof this._companyId == "undefined") ? this.companyId : this._companyId;

            var filterState = {
                serviceType: ServiceId,
                ownerId: OwnerId,
                assignToId: Assign,
                status: Status,
                duration: Duration,
                contractNo: ContractNo,
                isNavigate: true,
                companyId: companyId
            };
            if (Status == Enums.ModelData.TicketStatus.Acknowledge) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-ack", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Inprogress) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-inprog", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Confirm) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-confirm", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Follow) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-follow", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Pending) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-pending", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Close) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-close", filterState);
            }
            else {
                this.publishMessage("bo-ticket-new-search-changed", filterState);
            }
        };

        // datagrid expand
        self.detailInit = (e) => {
            var self = this;
            // set param to businessUnitId2
            self.businessUnitId2(e.data.businessUnitId);
            $("<div id='" + self.id + e.data.businessUnitId + "'/>").appendTo(e.detailCell).kendoGrid({
                // webrequest, page size
                dataSource: GenerateDataGridExpand.getDataSource(self.onDatasourceRequestInternalRead2, 10),
                // set reccord per page (ex. [10, 20, 50])
                pageable: GenerateDataGridExpand.getPageable(false),
                // set column of kendo grid
                columns: GenerateDataGridExpand.getColumns(self.generateColumns()),
            });
        }

        //webrequest
        this.onDatasourceRequestInternalRead2 = (request) => {
            var filter = Object.assign({}, this.generateModel(), request.data);
            this.webRequestTicket.getTicketSummary2(filter).done((response) => {
                request.success(response);
            }).fail((e) => {
                this.handleError(e);
            });
        }


    }

    generateModel() {
        var model = {
            businessUnitId: this.businessUnitId2(),
        }
        return model;
    }

    generateColumns() {
        var columns = [
            {
                title: 'i18n:Ticket_Owner',
                columns: [
                    {
                        title: 'i18n:Common_Company',
                        columns: [
                            { type: 'text', title: 'i18n:Common_Company', data: 'companyName', width: '125px' },
                            { type: 'text', title: 'i18n:Common_BusinessUnit', data: 'businessUnitName', width: '150px' }
                        ]
                    },
                    { type: 'text', title: 'i18n:Common_BusinessUnit', data: 'businessUnitName', width: '150px' }
                ]
            },
            { type: 'text', title: 'i18n:Ticket_TicketNo', data: 'ticketNo', width: '125px', template: '<u>#=ticketNo#</u>' },
            {
                title: 'i18n:Common_Status',
                columns: [
                    { type: 'text', title: 'i18n:Ticket_Owner', data: 'owner', width: '105px', hidden: false },
                    { type: 'text', title: 'i18n:Common_BusinessUnit', data: 'businessUnitName', width: '150px' }
                ]
            },
            { type: 'text', title: 'i18n:Ticket_Owner', data: 'owner', width: '105px', hidden: false },
            { type: 'text', title: 'i18n:Common_Company', data: 'companyName', width: '125px' },
            { type: 'text', title: 'i18n:Common_BusinessUnit', data: 'businessUnitName', width: '150px' },
        ];
        return columns;
    }

    /**
     * get data dashboard.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * get data dashboard.
     * @readonly
     */
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }


    checkData(input) {
        var data = {};
        if (input != null) {
            data = input;
            data.Status = Enums.ModelData.TicketStatus.New;
            data.Duration = this.duration;
        }
        else {
            data = {
                Status: Enums.ModelData.TicketStatus.New,
                Duration: this.duration
            };
        }
        return data;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        if (this._isNavigate) {
            var Data = {
                serviceType: this._serviceType,
                ownerId: this._ownerId,
                assignToId: this._assignToId,
                status: this._status,
                duration: this._duration,
                contractNo: this._contractNo,
                CompanyId: this._companyId
            };
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(Data),
                update: this.webRequestTicket.updateTicketSummary({})
            });
        }

        else if (this.data) {
            this.data.Status = Enums.ModelData.TicketStatus.New;
            this.data.Duration = this.duration;
            this.data.CompanyId = this.companyId;
            this.apiDataSource({
                read: this.webRequestTicket.getTicketSummary(this.data),
                update: this.webRequestTicket.updateTicketSummary({})
            });
        }
        else {
            var getTicketFilter = {
                status: Enums.ModelData.TicketStatus.New,
                duration: this.duration,
                companyId: this.companyId
            }

            this.apiDataSource({
                read: this.webRequestTicket.getTicketSummary(getTicketFilter),
                update: this.webRequestTicket.updateTicketSummary({})
            });
        }



        var userFilter = {
            groupIds: [
                Enums.UserGroup.Administrator,
                Enums.UserGroup.SystemAdmin
            ],
            sortingColumns: DefaultSorting.User,
            enable: true
        };

        var TicketDuration = {
            types: [Enums.ModelData.EnumResourceType.TicketDuration]
        };

        var TicketStatus = {
            types: [Enums.ModelData.EnumResourceType.TicketStatus],
            values: []
        };

        // var enumServiceTypeFilter = {
        //     types: [Enums.ModelData.EnumResourceType.ServiceType],
        //     values: [],
        //     sortingColumns: DefaultSorting.EnumResource
        // };

        var TicketStatusNew = {
            types: [Enums.ModelData.EnumResourceType.TicketStatus],
            values: [Enums.ModelData.TicketStatus.Acknowledge,
            Enums.ModelData.TicketStatus.Inprogress,
            Enums.ModelData.TicketStatus.Confirm,
            Enums.ModelData.TicketStatus.New
            ],
            sortingColumns: DefaultSorting.EnumResource
        };

        var d1 = this.webRequestUser.listUserSummary(userFilter);
        var d2 = this.webRequestTicket.listServiceTypeSummary();
        var d3 = this.webRequestEnumResource.listEnumResource(TicketStatusNew);
        var d4 = this.webRequestEnumResource.listEnumResource(TicketDuration);
        var d5 = this.webRequestEnumResource.listEnumResource(TicketStatus);

        $.when(d1, d2, d3, d4, d5).done((r1, r2, r3, r4, r5) => {
            var currentStaus = r3["items"];
            var currentDuration = r4["items"];
            // if fullname is null replace fullname is username
            r1.items.forEach(function (v) {
                v.displayName = (v.fullName == null || v.fullName.trim() == "") ? v.username : v.fullName
            });
            this.statusTicketDtg(currentStaus);
            this.assignTo(r1["items"]);
            this.serviceType(r2["items"]);
            this.statusTicket(r5["items"]);
            this.durationTicket(currentDuration);

            if (this._isNavigate) {
                let Isstatus = ScreenHelper.findOptionByProperty(this.statusTicket, "value", this._status); //find Status
                let Isduration = ScreenHelper.findOptionByProperty(this.durationTicket, "value", this._duration); // find Duration
                let IsassignTo = ScreenHelper.findOptionByProperty(this.assignTo, "id", this._assignToId);
                let IsserviceType = ScreenHelper.findOptionByProperty(this.serviceType, "id", this._serviceType);
                let Isowner = ScreenHelper.findOptionByProperty(this.assignTo, "id", this._ownerId);
                this.selectedAssign(IsassignTo);
                this.selectedDuration(Isduration);
                this.selectedOwner(Isowner);
                this.selectedStatus(Isstatus);
                this.selectedService(IsserviceType);
                this.contract(this._contractNo);
            } else if (this.data) {
                this.contract(this.data.ContractNo);
                let Isstatus = ScreenHelper.findOptionByProperty(this.statusTicket, "value", this.checkData(this.data).Status); //find Status
                let Isduration = ScreenHelper.findOptionByProperty(this.durationTicket, "value", this.checkData(this.data).Duration); // find Duration
                let Isassignto = ScreenHelper.findOptionByProperty(this.assignTo, "id", this.data.AssignToId); // find Owner
                let Isservicetype = ScreenHelper.findOptionByProperty(this.serviceType, "id", this.data.ServiceType); // find ServiceType
                let Isowner = ScreenHelper.findOptionByProperty(this.assignTo, "id", this.data.OwnerId); // find ServiceType                
                this.selectedDuration(Isduration);
                this.selectedStatus(Isstatus);
                this.selectedAssign(Isassignto);
                this.selectedService(Isservicetype);
                this.selectedOwner(Isowner);
            } else {
                let Isstatus = ScreenHelper.findOptionByProperty(this.statusTicket, "value", this.checkData(this.data).Status); //find Status
                let Isduration = ScreenHelper.findOptionByProperty(this.durationTicket, "value", this.checkData(this.data).Duration); // find Duration
                this.selectedDuration(Isduration);
                this.selectedStatus(Isstatus);
            }

        });

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdEdit", this.i18n("Ticket_EditMultipleTicket")(), "svg-ticket-editticket"));
        commands.push(this.createCommand("cmdCreate", this.i18n("Ticket_CreateAppointmentMultipleTicket")(), "svg-ticket-create-appointment"));
        commands.push(this.createCommand("cmdSendMail", this.i18n("Ticket_SendMailMultipleTicket")(), "svg-ticket-sentmail"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdExportCriteria", this.i18n("Export with criteria")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);


    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdEdit":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }

                this.navigate("bo-ticket-manage-edit", this.selectedItems());
                break;
            case "cmdCreate":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-create", {
                    items: this.selectedItems(),
                    Ismultiple: true
                });
                break;
            case "cmdSendMail":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-sendmail", this.selectedItems());
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filterToExport = this.checkData(this.data);
                            this.webRequestTicket.exportTicket(filterToExport).done((response) => {
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
            case "cmdExportCriteria":
                this.navigate("bo-ticket-manage-export-with-criteria");
                break;
        }
    }

    onUnLoad() {
        var filter = {
            Status: Enums.ModelData.TicketStatus.New,
            Duration: this.duration
        };
        this.apiDataSource({
            read: this.webRequestTicket.listTicketSummaryDataGrid(filter)
        });

    }

}

export default {
    viewModel: ScreenBase.createFactory(TicketNewScreen),
    template: templateMarkup
};