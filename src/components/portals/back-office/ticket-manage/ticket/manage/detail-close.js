﻿import ko from "knockout";
import templateMarkup from "text!./detail-close.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import webRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class CloseTicketScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_CloseTicket")());
        // this.bladeSize = BladeSize.XLarge;
        this.selectedStatus = ko.observable();
        this.remark = ko.observable();
        this.dt = ko.observableArray([]);
        this.status = ko.observableArray([]);
        this.isEnable = ko.observable(false);
        this.MaintenanceType = ko.observable([]);
        this.selectedMaintenanceType = ko.observable();
        this.pendingRemark = ko.observable();
        this.ticketId = this.ensureNonObservable(params.ticketId, 0);
        this.statusId = this.ensureNonObservable(params.statusId, 0);
        this.closeCause = ko.observableArray([]);
        this.closeSubCause = ko.observableArray([]);
        this.selectedClose = ko.observable();
        this.selectedSubClose = ko.observable();
        this.closeRemark = ko.observable();

        this.selectedClose.subscribe(val => {
            if(val){
                this.webRequestTicket.closeSubCause(val.id).done((reponse) => {
                    this.closeSubCause(reponse);
                });
            }
            else{
                this.closeSubCause.replaceAll([]);
            }
        });
    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestTicket(){
        return webRequestTicket.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var TicketStatus = {
            types:[Enums.ModelData.EnumResourceType.TicketStatus],
            values:[Enums.ModelData.TicketStatus.Close]
        };


        var w1 = this.webRequestEnumResource.listEnumResource(TicketStatus);
        var w2 = this.webRequestTicket.listTicketMaintenanceType({});
        var w3 = this.ticketId ? this.webRequestTicket.getTicket(this.ticketId) : null;
        var w4 = this.webRequestTicket.closeCause({});
        $.when(w1, w2, w3,w4).done((r1, r2, r3,r4)=>{
            this.status(r1.items);
            this.MaintenanceType(r2.items);
            this.selectedStatus(ScreenHelper.findOptionByProperty(this.status, "value",Enums.ModelData.TicketStatus.Close));
            this.closeCause(r4);

            if(_.size(r3)){
                this.dt([r3]);
            }
            dfd.resolve();
        }).fail((e)=>{
            dfd.reject();
        });

        return dfd;
    }
    setupExtend(){
        this.selectedMaintenanceType.extend({required:true});
        this.selectedClose.extend({required: true});
        this.closeRemark.extend({required: true});
        this.dt.extend({
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });


        this.validationModel = ko.validatedObservable({
            selectedMaintenanceType : this.selectedMaintenanceType,
            dt: this.dt,
            selectedClose: this.selectedClose,
            closeRemark: this.closeRemark
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        var enableSave = true;
        if(this.statusId == Enums.ModelData.TicketStatus.Close){
            enableSave = false;
        }
        actions.push(this.createAction("actSave", this.i18n("Common_Save")(), "default", true, enableSave));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {

        super.onActionClick(sender);
        if(sender.id=="actSave"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            } 

            var filter = {
                status:(this.selectedStatus())?this.selectedStatus().value:null,
                remark:this.remark(),
                ticketIds:this.ticketId,
                maintenanceTypeId:(this.selectedMaintenanceType()) ? this.selectedMaintenanceType().id : null,
                closeCauseId: this.selectedClose().id,
                closeSubCauseId: this.selectedSubClose() ? this.selectedSubClose().id : null,
                closeCauseRemark: this.closeRemark()
            };

            
            this.webRequestTicket.updateTicket(filter).done((response)=>{
                this.publishMessage("bo-ticket-edit-multiple-service-changed");
                this.close(true);
            }).fail((e)=>{
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
       
        
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(CloseTicketScreen),
    template: templateMarkup
};