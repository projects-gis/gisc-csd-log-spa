﻿import ko from "knockout";
import templateMarkup from "text!./ticket-inprog.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestUser from "../../../../../../app/frameworks/data/apicore/webRequestUser";
import Utility from "../../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import { AssetIcons } from "../../../../../../app/frameworks/constant/svg";

class TicketInprogScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this._serviceType = params.serviceType;
        this._ownerId = params.ownerId;
        this._assignToId = params.assignToId;
        this._status = params.status;
        this._duration = params.duration;
        this._contractNo = params.contractNo;
        this._companyId = params.companyId;
        this._isNavigate = (typeof params.isNavigate != "undefined") ? params.isNavigate : false;

        this.bladeTitle(this.i18n("Ticket_Management")());
        this.duration = this.ensureNonObservable(params.ticketDuration);
        this.data = this.ensureNonObservable(params.data);
        this.companyId = (this.data) ? this.data.CompanyId : null;
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        this.selectedItems = ko.observableArray([]);
        this.statusTicket = ko.observableArray([]);
        this.statusTicketDtg = ko.observableArray([]);
        this.durationTicket = ko.observableArray([]);
        this.assignTo = ko.observableArray([]);
        this.serviceType = ko.observableArray([]);
        this.selectedService = ko.observable();
        this.selectedAssign = ko.observable();
        this.selectedOwner = ko.observable();
        this.selectedStatus = ko.observable();
        this.selectedDuration = ko.observable();
        this.contract = ko.observable();
        this.selectedItems = ko.observableArray([]);
        this.apiDataSource = ko.observableArray([]);

        // gen header icon
        this.headerTemplateIcon = _.template('<div class="<%= iconCssClass %>" title="<%= title %>"><%= icon %></div>');
        this.headerTemplateGPS = this.headerTemplateIcon({ 'iconCssClass': 'icon-gps', 'title': this.i18n("Ticket_GPSStatus")(), 'icon': AssetIcons.IconGpsTemplateMarkup });


        this.isEnable = ko.observable(false);
        this.testClick = (data) => {
            this.navigate("bo-ticket-manage-asset-details", { ticketId: data.ticketId })
        };

        this.viewLicense = (data) => {
            this.navigate("bo-ticket-manage-asset-vehicle-info",
                {
                    companyId: data.companyId,
                    vehicleId: data.vehicleId,
                    vehicleLicense: data.license,
                    serialNo: data.serialNo,
                });
        };

        this.editorTargetDate = (container, options) => {
            $('<input type="text" />')
                .appendTo(container)
                .kendoDatePicker({
                    format: "dd/MM/yyyy",
                    value: kendo.toString(new Date(options.TargetDate), 'dd/MM/yyyy')
                });
        }

        // this.templateTargetDate = "#= kendo.toString(new Date(parseInt(TargetDate)), 'dd/MM/yyyy') #";
        this.templateTargetDate = "#= new Date(parseInt(targetDate), 'dd/MM/yyyy')#";

        this.editorTicketStatus = (container, options) => {

            $('<input data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "value",
                    dataSource: this.statusTicketDtg(),
                    value: options.model.status,
                    change: function (e) {
                        var dataItem = e.sender.dataItem();
                        options.model.set("status", dataItem.value);
                        options.model.set("statusDisplayName", dataItem.name);
                    }
                });
        }

        this.templateTicketStatus = (data) => {
            return data.statusDisplayName;
        }

        this.editorServiceType = (container, options) => {
            $('<input data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: this.serviceType(),
                    value: options.model.serviceType,
                    change: function (e) {
                        var dataItem = e.sender.dataItem();
                        options.model.set("serviceType", dataItem.id);
                        options.model.set("serviceTypeDisplayName", dataItem.name);
                    },
                    optionLabel: {
                        name: " ",
                        value: null
                    }
                });
        }

        this.templateServiceType = (data) => {
            return (data.serviceTypeDisplayName == null) ? " " : data.serviceTypeDisplayName;
        }

        this.boxSerialNoClick = (data) => {
            this.navigate("bo-ticket-manage-fleet-service", data);
        }


        this.subscribeMessage("update-items-appointment", (appointId) => {
            if (this.data != null) {
                this.data.Status = Enums.ModelData.TicketStatus.Inprogress;
                this.data.Duration = this.duration;
            }
            else {
                this.data = {
                    Status: Enums.ModelData.TicketStatus.Inprogress,
                    Duration: this.duration
                };
            }

            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(this.data)
            });
        });

        this.subscribeMessage("bo-ticket-inprog-search-changed", (data) => {
            //set params
            this.data = data;
            this.duration = data.duration;
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(data),
            });
        });

        // Observe change about filter
        this.subscribeMessage("bo-ticket-edit-multiple-service-changed", (data) => {
            this.selectedItems().length = 0;
            let dataFilter = this.checkData(this.data);
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(dataFilter)
            });
        });

        this.viewSearch = () => {
            let OwnerId = (typeof this.selectedOwner() == "undefined") ? null : this.selectedOwner().id;
            let Assign = (typeof this.selectedAssign() == "undefined") ? null : this.selectedAssign().id;
            let ServiceId = (typeof this.selectedService() == "undefined") ? null : this.selectedService().id;
            let Status = (typeof this.selectedStatus() == "undefined") ? null : this.selectedStatus().value;
            let Duration = (typeof this.selectedDuration() == "undefined") ? null : this.selectedDuration().value;
            let ContractNo = (typeof this.contract() == "underfined") ? null : this.contract();
            let companyId = (typeof this._companyId == "undefined") ? this.companyId : this._companyId;

            var filterState = {
                serviceType: ServiceId,
                ownerId: OwnerId,
                assignToId: Assign,
                status: Status,
                duration: Duration,
                contractNo: ContractNo,
                isNavigate: true,
                companyId: companyId
            }

            if (Status == Enums.ModelData.TicketStatus.New) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-new", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Acknowledge) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-ack", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Confirm) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-confirm", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Follow) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-follow", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Pending) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-pending", filterState);
            }
            if (Status == Enums.ModelData.TicketStatus.Close) {
                this.close(true);
                this.navigate("bo-ticket-manage-ticket-close", filterState);
            }
            else {
                this.publishMessage("bo-ticket-inprog-search-changed", filterState);
            }
        };
    }

    /**
     * get data dashboard.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    checkData(input) {
        var data = {};
        if (input != null) {
            data = input;
            data.Status = Enums.ModelData.TicketStatus.Inprogress;
            data.Duration = this.duration;
        }
        else {
            data = {
                Status: Enums.ModelData.TicketStatus.Inprogress,
                Duration: this.duration
            };
        }
        return data;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var userFilter = {
            groupIds: [
                Enums.UserGroup.Administrator,
                Enums.UserGroup.SystemAdmin
            ],
            sortingColumns: DefaultSorting.User,
            enable: true
        };
        var TicketStatusInprog = {
            types: [Enums.ModelData.EnumResourceType.TicketStatus],
            values: [Enums.ModelData.TicketStatus.Confirm,
            Enums.ModelData.TicketStatus.Inprogress
            ]
        };
        var TicketDuration = {
            types: [Enums.ModelData.EnumResourceType.TicketDuration]
        };
        var TicketStatus = {
            types: [Enums.ModelData.EnumResourceType.TicketStatus],
            values: []
        };
        // var enumServiceTypeFilter = {
        //     types: [Enums.ModelData.EnumResourceType.ServiceType],
        //     values: [],
        //     sortingColumns: DefaultSorting.EnumResource
        // };
        var w1 = this.webRequestEnumResource.listEnumResource(TicketStatusInprog);
        var w2 = this.webRequestEnumResource.listEnumResource(TicketDuration);
        var w3 = this.webRequestUser.listUserSummary(userFilter);
        var w4 = this.webRequestTicket.listServiceTypeSummary();
        var w5 = this.webRequestEnumResource.listEnumResource(TicketStatus);

        if (this._isNavigate) {
            var Data = {
                serviceType: this._serviceType,
                ownerId: this._ownerId,
                assignToId: this._assignToId,
                status: this._status,
                duration: this._duration,
                contractNo: this._contractNo,
                companyId: this._companyId
            };
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(Data),
                update: this.webRequestTicket.updateTicketSummary({})
            });
        }
        else if (this.data) {
            this.data.Status = Enums.ModelData.TicketStatus.Inprogress;
            this.data.Duration = this.duration;
            this.data.CompanyId = this.companyId;
            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(this.data),
                update: this.webRequestTicket.updateTicketSummary({})
            });
        }


        else {
            var getTicketFilter = {
                status: Enums.ModelData.TicketStatus.Inprogress,
                duration: this.duration,
                companyId: this.companyId
            }

            this.apiDataSource({
                read: this.webRequestTicket.listTicketSummaryDataGrid(getTicketFilter),
                update: this.webRequestTicket.updateTicketSummary({})
            });
        }

        $.when(w1, w2, w3, w4, w5).done((r1, r2, r3, r4, r5) => {
            var currentStaus = r1["items"];
            var currentDuration = r2["items"];
            r3.items.forEach((v) => {
                v.displayName = (v.fullName == null || v.fullName.trim() == "") ? v.username : v.fullName
            });
            this.statusTicketDtg(currentStaus);
            this.assignTo(r3["items"]);
            this.serviceType(r4["items"]);
            this.statusTicket(r5["items"]);
            this.durationTicket(currentDuration);

            if (this._isNavigate) {
                let Isstatus = ScreenHelper.findOptionByProperty(this.statusTicket, "value", this._status); //find Status
                let Isduration = ScreenHelper.findOptionByProperty(this.durationTicket, "value", this._duration); // find Duration
                let IsassignTo = ScreenHelper.findOptionByProperty(this.assignTo, "id", this._assignToId);
                let IsserviceType = ScreenHelper.findOptionByProperty(this.serviceType, "id", this._serviceType);
                let Isowner = ScreenHelper.findOptionByProperty(this.assignTo, "id", this._ownerId);

                this.selectedAssign(IsassignTo);
                this.selectedDuration(Isduration);
                this.selectedOwner(Isowner);
                this.selectedStatus(Isstatus);
                this.selectedService(IsserviceType);
                this.contract(this._contractNo);
            } else if (this.data) {
                this.contract(this.data.ContractNo);
                let Isstatus = ScreenHelper.findOptionByProperty(this.statusTicket, "value", this.checkData(this.data).Status); //find Status
                let Isduration = ScreenHelper.findOptionByProperty(this.durationTicket, "value", this.checkData(this.data).Duration); // find Duration
                let Isassignto = ScreenHelper.findOptionByProperty(this.assignTo, "id", this.data.AssignToId); // find Owner
                let Isservicetype = ScreenHelper.findOptionByProperty(this.serviceType, "id", this.data.ServiceType); // find ServiceType
                let Isowner = ScreenHelper.findOptionByProperty(this.assignTo, "id", this.data.OwnerId); // find ServiceType                
                this.selectedDuration(Isduration);
                this.selectedStatus(Isstatus);
                this.selectedAssign(Isassignto);
                this.selectedService(Isservicetype);
                this.selectedOwner(Isowner);
            } else {
                let Isstatus = ScreenHelper.findOptionByProperty(this.statusTicket, "value", this.checkData(this.data).Status); //find Status
                let Isduration = ScreenHelper.findOptionByProperty(this.durationTicket, "value", this.checkData(this.data).Duration); // find Duration
                this.selectedDuration(Isduration);
                this.selectedStatus(Isstatus);
            }
        });

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdEdit", this.i18n("Ticket_EditMultipleTicket")(), "svg-ticket-editticket"));
        commands.push(this.createCommand("cmdCreate", this.i18n("Ticket_CreateAppointmentMultipleTicket")(), "svg-ticket-create-appointment"));
        commands.push(this.createCommand("cmdSendMail", this.i18n("Ticket_SendMailMultipleTicket")(), "svg-ticket-sentmail"));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        commands.push(this.createCommand("cmdExportCriteria", this.i18n("Export with criteria")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdEdit":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-edit", this.selectedItems());
                break;
            case "cmdCreate":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-create", {
                    items: this.selectedItems(),
                    Ismultiple: true
                });
                break;
            case "cmdSendMail":
                if (Object.keys(this.selectedItems()).length < 1) {
                    this.showMessageBox("", this.i18n("M168")(), BladeDialog.DIALOG_OK);
                    return false;
                }
                this.navigate("bo-ticket-manage-sendmail", this.selectedItems());
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filterToExport = this.checkData(this.data);
                            this.webRequestTicket.exportTicket(filterToExport).done((response) => {
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
            case "cmdExportCriteria":
                this.navigate("bo-ticket-manage-export-with-criteria");
                break;
        }
    }

}

export default {
    viewModel: ScreenBase.createFactory(TicketInprogScreen),
    template: templateMarkup
};