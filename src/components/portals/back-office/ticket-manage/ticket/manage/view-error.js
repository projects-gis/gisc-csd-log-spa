﻿import ko from "knockout";
import templateMarkup from "text!./view-error.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class ViewErrorDetail extends ScreenBase {
    constructor(params) {
        super(params);
        this.id = this.ensureNonObservable(params);
        this.errorMsg = ko.observable();
        this.bladeTitle(this.i18n("Ticket_ErrorDetail")());
        
    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }

   

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.webRequestTicket.getLogTicketMail(this.id).done((value)=>{
            this.errorMsg(value.errorDetail);
        });
    }

    setupExtend(){
     
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
       actions.push(this.createActionCancel());
       
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
       
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
       
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
    
}

export default {
viewModel: ScreenBase.createFactory(ViewErrorDetail),
    template: templateMarkup
};