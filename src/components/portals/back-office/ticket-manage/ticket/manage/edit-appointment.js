﻿import "jqueryui-timepicker-addon";
import ko from "knockout";
import moment from "moment";
import templateMarkup from "text!./edit-appointment.html";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import webRequestCity from "../../../../../../app/frameworks/data/apicore/webRequestCity";
import webRequestCompany from "../../../../../../app/frameworks/data/apicore/webRequestCompany";
import webRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestProvince from "../../../../../../app/frameworks/data/apicore/webRequestProvince";
import webRequestTown from "../../../../../../app/frameworks/data/apicore/webRequestTown";
import webRequestUser from "../../../../../../app/frameworks/data/apicore/webRequestUser";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import Utility from "../../../../../../app/frameworks/core/utility";
import WebrequestVehicleModel from "../../../../../../app/frameworks/data/apitrackingcore/webrequestVehicleModel";


class AppointmentsUpdate extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Ticket_UpdateAppointment")());
        this.bladeSize = BladeSize.XMedium;
        this.bladeIsMaximized = true;
        this.valueParams = (params.items) ? params.items : null;
        this.id = this.valueParams ? this.valueParams.id : null;
        this.currentStatus = this.valueParams ? this.valueParams.status : null;
        this.companyId = this.ensureNonObservable(this.valueParams ? this.valueParams.companyId : null);
        var bb = (Object.keys(params).length > 0) ? this.getTimeByParams(this.valueParams) : null;
        this.status = ko.observableArray([]);
        this.appoiDate = ko.observable();
        this.appoidesc = ko.observable(this.valueParams ? this.valueParams.appointmentDescription : null);

        this.isEnable = ko.observable(false);
        this.formatSet = "dd/mm/yyyy";
        this.time = ko.observable(bb);
        this.minDate = ko.observable(0);
        this.appDatetime = ko.observable(this.valueParams ? this.valueParams.appointDate : null);
        this.province = ko.observableArray([]);
        this.techteam = ko.observable([]);
        //this.MaintenanceType = ko.observable([]);
        this.selectedProvince = ko.observable();
        //this.selectedMaintenanceType = ko.observable();
        this.selectedTechTeam = ko.observable();
        this.selectedStatus = ko.observable();
        this.isVisible = ko.observable();
        this.pendingCause = ko.observableArray([]);
        this.pendingSubCause = ko.observableArray([]);
        this.selectedPending = ko.observable();
        this.selectedSubPend = ko.observable();
        this.pendingRemark = ko.observable();
        this.vehicleId = ko.observable();
        this.license = ko.observable();
        this.boxSerialNo = ko.observable();
        this.pathImages = ko.observableArray([]);
        this.isStatusEnable = ko.observable(false);

        //  Info About Vehicle
        this.boxSerialNoDisplay = ko.observable(" BoxSerial No. (-)");
        this.installDate = ko.observable();
        this.timeInstall = ko.observable();
        this.vDescription = ko.observable();
        this.enableOptions = ScreenHelper.createYesNoObservableArrayWithDisplayValue();
        this.enableVehicle = ko.observable();
        this.defaultOverSpeed = ko.observable();
        this.refNo = ko.observable();
        this.stopDuration = ko.observable();
        this.overSpeedDelay = ko.observable();
        this.typeOptions = ko.observableArray();
        this.vType = ko.observable();
        this.modelOptions = ko.observableArray();
        this.vModel = ko.observable();
        this.licensePlate = ko.observable();
        this.licenseCountry = ko.observable();
        this.businessUnitOptions = ko.observableArray();
        this.businessUnit = ko.observable();
        this.licenseProvince = ko.observable();
        this.chassisNo = ko.observable();
        this.maxDate = new Date();
        
        this.licenseDisplay = ko.pureComputed(() => {
            let display = '';
            if (this.license()) {
                display = ` License (${this.license()})`;
            }
            else {
                display = ` License (-)`;
            }
            return display;
        });

        this.boxSerialNoDisplay = ko.pureComputed(() => {
            let display = '';
            if (this.boxSerialNo()) {
                display = ` BoxSerial No. (${this.boxSerialNo()})`;
            }
            else {
                display = ` BoxSerial No. (-)`;
            }
            return display;
        });
        /////////

        this.currentStatus = this.currentStatus == Enums.ModelData.TicketAppointmentStatus.New ? Enums.ModelData.TicketAppointmentStatus.Appoint : this.currentStatus;

        this.isVisible = ko.pureComputed(() => {
            return this.selectedStatus() && this.selectedStatus().value == Enums.ModelData.TicketAppointmentStatus.Fail;
        });

        if (this.isVisible) {
            this.webRequestTicket.getPendingCause().done((pending) => {
                this.pendingCause(pending);
            });

            this.selectedPending.subscribe(value => {
                if (value) {
                    let pendingId = (typeof this.selectedPending() == "undefined") ? null : value.id
                    this.webRequestTicket.getPendingSubCause(pendingId).done((reponse) => {
                        this.pendingSubCause(reponse);
                    });
                }
                else {
                    this.pendingSubCause.replaceAll([]);
                }
            });
        }

        this.isStatusAppoint = ko.pureComputed(() => {
            return this.selectedStatus() && (this.selectedStatus().value == Enums.ModelData.TicketAppointmentStatus.New || 
                                             this.selectedStatus().value == Enums.ModelData.TicketAppointmentStatus.Appoint);
        });

        if (this.currentStatus == Enums.ModelData.TicketAppointmentStatus.Appoint) {
            this.isStatusEnable(true);
        } else {
            this.isStatusEnable(false);
        }

    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestCompany() {
        return webRequestCompany.getInstance();
    }

    get webRequestProvince() {
        return webRequestProvince.getInstance();
    }

    get webRequestCity() {
        return webRequestCity.getInstance();
    }

    get webRequestTown() {
        return webRequestTown.getInstance();
    }

    get webRequestUser() {
        return webRequestUser.getInstance();
    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webrequestVehicleModel() {
        return WebrequestVehicleModel.getInstance();
    }
    
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var dfd3 = $.Deferred();

        var ContactsCom = [
            "Company.Contacts"
        ];
        var EnumUserGroup = {
            groupIds: [Enums.UserGroup.Technician],
            sortingColumns: DefaultSorting.User
        };
        var EnumsStatusAppoi = {
            types: [
                Enums.ModelData.EnumResourceType.TicketAppointmentStatus,
                Enums.ModelData.EnumResourceType.VehicleType
            ]
        };

        var d1 = this.webRequestEnumResource.listEnumResource(EnumsStatusAppoi);
        var d3 = this.webRequestUser.listUserSummary(EnumUserGroup);
        var d4 = this.id ? this.webRequestTicket.getTicketAppointment(this.id) : null;
        var d5 = this.webRequestProvince.listProvince({ "countryIds": [1] });
        var d8 = this.companyId ? this.webRequestBusinessUnit.listBusinessUnitSummary({
            companyId: this.companyId,
            isIncludeFullPath: true,
            sortingColumns: DefaultSorting.BusinessUnit
        }) : null;
        $.when(d1, d3, d4, d5, d8).done((r1,r3, r4, r5, r8) => {
            r3.items.forEach((v) => {
                v.displayName = (v.fullName == null || v.fullName.trim() == "") ? v.username : v.fullName + " (" + v.mobileNo + ") "
            });

            var ticketStatus = []; // ถ้าเป็น Status อื่น จะ Set ใหม่
            var ticketAppointStatus = _.filter(r1.items, (item) => { return item.type == Enums.ModelData.EnumResourceType.TicketAppointmentStatus });
            var vehicleType = _.filter(r1.items, (item) => { return item.type == Enums.ModelData.EnumResourceType.VehicleType });

            ticketAppointStatus.forEach(x => {
                if (x.value != Enums.ModelData.TicketAppointmentStatus.New) {
                    ticketStatus.push(x);
                }
            });

            Utility.applyFormattedPropertyToCollection(vehicleType, "displayText", "{0} ({1})", "displayName", "value");

            this.typeOptions(vehicleType);
            this.status(ticketStatus);
            this.techteam(r3.items);

            if (r4) {
                this.vehicleId(r4.vehicleId);
                this.license(r4.license);
                this.boxSerialNo(r4.serialNo);
                this.pathImages(r4.media);

                // Vehicle Model
                if (r4.vehicleInfo) {
                    let vehicle = r4.vehicleInfo;
                    
                    if(vehicle.installedDate){
                        let installDate = new Date(vehicle.installedDate);
                        let getTime = moment(installDate).format('HH:mm');

                        this.installDate(installDate);
                        this.timeInstall(getTime);
                    }

                    this.vType(ScreenHelper.findOptionByProperty(this.typeOptions, "value", vehicle.vehicleType))
                    this.enableVehicle(ScreenHelper.findOptionByProperty(this.enableOptions, "value", vehicle.enable));
                    this.defaultOverSpeed(vehicle.defaultOverSpeedLimit);
                    this.refNo(vehicle.referenceNo);
                    this.stopDuration(vehicle.stopDuration);
                    this.overSpeedDelay(vehicle.overSpeedDelay);
                    this.vDescription(vehicle.description);
                    this.chassisNo(vehicle.chassisNo);
                    this.licensePlate(vehicle.license.licensePlate);
                    this.licenseProvince(vehicle.license.licenseProvince);
                    this.licenseCountry(vehicle.license.licenseCountry);

                    if (r8) {
                        Utility.applyFormattedPropertyToCollection(r8.items, "displayText", "{0} ({1})", "name", "fullCode");
                        this.businessUnitOptions(r8.items);
                        this.businessUnit(vehicle.businessUnitId.toString());
                    }


                    var includeVehicleModelIds = vehicle.modelId ? [vehicle.modelId] : [];
                    this.webrequestVehicleModel.listVehicleModelSummary({
                        enable: true,
                        includeIds: includeVehicleModelIds,
                        sortingColumns: DefaultSorting.VehicleModelByBrandModelCode
                    }).done((response) => {
                        Utility.applyFormattedPropertyToCollection(response["items"], "displayText", "{0} - {1} ({2})", "brand", "model", "code");
                        this.modelOptions(response["items"]);
                        this.vModel(ScreenHelper.findOptionByProperty(this.modelOptions, "id", vehicle.modelId));
                        dfd3.resolve();
                    }).fail((e) => {
                        dfd3.reject(e);
                    });
                }

            }
            else{
                dfd3.resolve();
            }
            
            $.when(dfd3).done(() => {

                if (this.valueParams) {
                    this.selectedTechTeam(ScreenHelper.findOptionByProperty(this.techteam, "id", this.valueParams.technicianId));
                }
                this.selectedStatus(ScreenHelper.findOptionByProperty(this.status, "value", this.currentStatus));
                
                if (_.size(r5)) {
                    var provinceOptions = r5["items"];
                    this.province(provinceOptions);
    
                    if (this.valueParams && this.valueParams.provinceId) {
                        this.selectedProvince(ScreenHelper.findOptionByProperty(this.province, "id", this.valueParams.provinceId));
                    }
                }

                dfd.resolve();
            });

        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        });


        return dfd;

    }

    setupExtend() {

        var self = this;
        self.selectedPending.extend({
            required: {
                onlyIf: function () {
                    return self.isVisible() && (self.currentStatus != Enums.ModelData.TicketAppointmentStatus.New);
                }
            }
        });

        self.pendingRemark.extend({
            required: {
                onlyIf: function () {
                    return self.isVisible() && (self.currentStatus != Enums.ModelData.TicketAppointmentStatus.New);
                }
            }
        });
        this.appDatetime.extend({ trackChange: true });
        this.appoidesc.extend({ trackChange: true });
        this.selectedProvince.extend({ trackChange: true });
        this.selectedTechTeam.extend({ trackChange: true });
        //this.selectedMaintenanceType.extend({trackChange:true});
        // this.time.extend({trackChange:true});


        this.appDatetime.extend({ required: true });
        this.time.extend({ required: true });
        this.selectedProvince.extend({ required: true });
        this.selectedTechTeam.extend({ required: true });
        this.selectedStatus.extend({ required: true });
        this.license.extend({required: true, trackChange:true})
        this.boxSerialNo.extend({required: true, trackChange:true})
        // Vehicle Info
        this.vType.extend({required : true,trackChange: true});
        this.vModel.extend({required : true,trackChange: true});
        this.businessUnit.extend({required : true,trackChange: true});
        this.enableVehicle.extend({required : true,trackChange: true});
        this.stopDuration.extend({required : true,trackChange: true});
        this.overSpeedDelay.extend({required : true,trackChange: true});
        this.vDescription.extend({trackChange: true});

        this.validationModel = ko.validatedObservable({
            status: this.selectedStatus,
            Datetime: this.appDatetime,
            Description: this.appoidesc,
            Province: this.selectedProvince,
            TechTeam: this.selectedTechTeam,
            //MaintenanceType:this.selectedMaintenanceType,
            time: this.time,
            pendingCauseId: self.selectedPending,
            pendingRemark: self.pendingRemark,
            vehicleType: this.vType,
            vehicleModel: this.vModel,
            businessUnit: this.businessUnit,
            enableVehicle: this.enableVehicle,
            stopDuration: this.stopDuration,
            overSpeedDelay: this.overSpeedDelay,
            license: this.license,
            boxSerialNo: this.boxSerialNo,
            vDescription: this.vDescription
        });

        this.validNotFinish = ko.validatedObservable({

        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());

    }

    getTimeByParams(params) {
        let a = params.appointDate.getHours() < 10 ? '0' + params.appointDate.getHours() : params.appointDate.getHours();
        let b = params.appointDate.getMinutes() < 10 ? '0' + params.appointDate.getMinutes() : params.appointDate.getMinutes();
        var aa = [a, b];

        return aa.join(":");
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var formatDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.appDatetime())) + " " + this.time() + ":00";
            var filter = {
                id: this.id,
                status: this.selectedStatus().value,
                appointDate: formatDate,
                appointmentDescription: this.appoidesc(),
                provinceId: this.selectedProvince().id,
                ticketIds: this.valueParams.ticketId,
                technicianId: this.selectedTechTeam().id,
                pendingCauseId: this.selectedPending() ? this.selectedPending().id : null,
                pendingSubCauseId: this.selectedSubPend() ? this.selectedSubPend().id : null,
                pendingRemark: this.pendingRemark() ? this.pendingRemark() : null,
                boxSerialNo: this.boxSerialNo(),
                companyId: this.companyId
            }

            let installDate = "";
            if(this.installDate()){
                installDate = moment(new Date(this.installDate())).format("YYYY-MM-DD") + " " + (this.timeInstall() ? this.timeInstall() : "00:00");
            }
             
            let vehicleInfo = {
                id: this.vehicleId(),
                enable: this.enableVehicle().value,
                defaultOverSpeedLimit: this.defaultOverSpeed(),
                vehicleType: this.vType().value,
                chassisNo: this.chassisNo(),
                referenceNo: this.refNo(),
                stopDuration: this.stopDuration(),
                modelId: this.vModel().id,
                businessUnitId: this.businessUnit(),
                overSpeedDelay: this.overSpeedDelay(),
                installedDate: installDate,
                description: this.vDescription(),
                license:{
                    license: this.license(),
                    licensePlate: this.licensePlate(),
                    licenseProvince: this.licenseProvince(),
                    licenseCountry: this.licenseCountry()
                }

            };

            filter.vehicleInfo = vehicleInfo;
            this.webRequestTicket.updateTicketAppointment(filter).done((response) => {
                this.publishMessage("update-items-appointment", this.id);
                this.publishMessage("bo-ticket-manage-update-items-appointment");
                this.close(true);
            }).fail((e) => {
                this.handleError(e);
            });
        }

    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

}

export default {
    viewModel: ScreenBase.createFactory(AppointmentsUpdate),
    template: templateMarkup
};