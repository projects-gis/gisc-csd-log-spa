﻿import ko from "knockout";
import templateMarkup from "text!./edit.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestReportTemplate from "../../../../../../app/frameworks/data/apitrackingcore/webrequestReportTemplate";
import WebRequestAssetMonitoring from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAssetMonitoring";
import Utility from "../../../../../../app/frameworks/core/utility";
//import GenerateSortingColumns from "../../../company-workspace/generateSortingColumns";
//import IconFollowTemplateMarkup from "text!./../../../../svgs/icon-follow.html";
import webRequestUser from "../../../../../../app/frameworks/data/apicore/webRequestUser";
import webRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestTicket from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";

class TicketDashboardScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.dt = ko.observableArray();
        this.filterState = params.filterState ? params.filterState : {};

        if(Object.keys(this.filterState).length > 0){
            this.dt(_.size(params.items) ? params.items : []);
        }
        else{
            this.dt((Object.keys(params).length > 0) ? params : []);
        }

        if(_.size(this.dt()) == 1){
            this.bladeTitle(this.i18n("Edit Ticket")());
        }
        else{
            this.bladeTitle(this.i18n("Ticket_EditMultipleTicket")());
        }
        
        // this.bladeSize = BladeSize.XLarge;
        this.selectedStatus = ko.observable();
        this.selectedserviceType = ko.observable();
        this.selectedAssignTo = ko.observable();
        this.targetDate = ko.observable();
        this.formatSet = ko.observable("dd/MM/yyyy");
        this.remark = ko.observable();
        this.status = ko.observableArray([]);
        this.serviceType = ko.observableArray([]);
        this.values = ko.observable();
        this.AssignTo = ko.observableArray([]);

        this.pendingCause = ko.observableArray([]);
        this.pendingSubCause = ko.observableArray([]);
        this.MaintenanceType = ko.observable([]);
        this.selectedPending = ko.observable();
        this.selectedSubPend = ko.observable();
        this.selectedMaintenanceType = ko.observable();
        this.pendingRemark = ko.observable();
        this.closeCause = ko.observableArray([]);
        this.closeSubCause = ko.observableArray([]);
        this.selectedClose = ko.observable();
        this.selectedSubClose = ko.observable();
        this.closeRemark = ko.observable();
        this.isQC = ko.observable(false);
        this.isChangeBoxModel = ko.observable(false);
        this.isMoveDefaultDriver = ko.observable(false);
        
        this.moveDefaultDriverVisible = ko.pureComputed(() => {
            let serviceType = this.selectedserviceType();
            if(serviceType){
                let serviceTypeName = serviceType.name == 'Relocate' ? true : false;
                return serviceTypeName;
            }
        });

        for(var key in params) {
            var value = params[key];
        }
        this.values(typeof value =='undefined' ? null : value.status);
        if(this.values() == 1)
        {
            var tomorDay =  Utility.addDays(new Date(),3);
            this.targetDate(tomorDay);
        }
        else
        {
            this.targetDate();
        }
        this.selectedPending.subscribe(val => {
            if(val){
                let pendingId = (typeof this.selectedPending() == "undefined")? null : val.id
                this.webRequestTicket.getPendingSubCause(pendingId).done((reponse) => {
                    this.pendingSubCause(reponse);
                });
            }
            else{
                this.pendingSubCause.replaceAll([]);
            }
        });
        
        this.selectedClose.subscribe(val => {
            if(val){
                let closeId = (typeof val == "undefined")? null : val.id
                this.webRequestTicket.closeSubCause(closeId).done((reponse) => {
                    this.closeSubCause(reponse);
                });
            }
            else{
                this.closeSubCause.replaceAll([]);
            }
        });

        this.enablePending = ko.pureComputed(() => {
            return this.selectedStatus() && this.selectedStatus().value === Enums.ModelData.TicketStatus.Pending;
        });

        this.enableClose = ko.pureComputed(() => {
            return this.selectedStatus() && this.selectedStatus().value === Enums.ModelData.TicketStatus.Close;
        });
    }

    get webRequestUser() {
        return webRequestUser.getInstance();
    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestTicket(){
        return webRequestTicket.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var TicketStatus = null ; 
        if(this.values()==Enums.ModelData.TicketStatus.New){
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[Enums.ModelData.TicketStatus.Acknowledge,
                        Enums.ModelData.TicketStatus.Inprogress,
                        Enums.ModelData.TicketStatus.Close,
                        Enums.ModelData.TicketStatus.Confirm
                ]
            };
        }


        else if(this.values()==Enums.ModelData.TicketStatus.Acknowledge){
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[Enums.ModelData.TicketStatus.Inprogress,
                        Enums.ModelData.TicketStatus.Close,
                        Enums.ModelData.TicketStatus.Confirm
                ]
            };
        }

        else if(this.values()==Enums.ModelData.TicketStatus.Inprogress){
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[Enums.ModelData.TicketStatus.Pending,
                        Enums.ModelData.TicketStatus.Close,
                        Enums.ModelData.TicketStatus.Confirm
                ]
            };
        }
        else if(this.values()==Enums.ModelData.TicketStatus.Confirm){
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[Enums.ModelData.TicketStatus.Pending,
                        Enums.ModelData.TicketStatus.Follow,
                        Enums.ModelData.TicketStatus.Close,
                ]
            };
        }
        else if(this.values()==Enums.ModelData.TicketStatus.Pending){
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[Enums.ModelData.TicketStatus.Close,
                        Enums.ModelData.TicketStatus.Confirm
                ]
            };
        }

        else if(this.values()==Enums.ModelData.TicketStatus.Follow){
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[Enums.ModelData.TicketStatus.Pending,
                        Enums.ModelData.TicketStatus.Close,
                        Enums.ModelData.TicketStatus.Confirm
                ]
            };
        }
        
        else{
            var TicketStatus = {
                types:[Enums.ModelData.EnumResourceType.TicketStatus],
                values:[]
            };
        }
        var EnumUserGroup = { 
            groupIds:[
                Enums.UserGroup.Administrator,
                Enums.UserGroup.SystemAdmin
            ],
            sortingColumns: DefaultSorting.User,
            enable: true
        };

        
        // var EnumServiceType = {
        //     types:[Enums.ModelData.EnumResourceType.ServiceType]
        // };
        var w1 = this.webRequestUser.listUserSummary(EnumUserGroup);
        var w2 = this.webRequestEnumResource.listEnumResource(TicketStatus);
        var w3 = this.webRequestTicket.listServiceTypeSummary();
        var w4 = this.webRequestTicket.getPendingCause();
        var w5 = this.webRequestTicket.listTicketMaintenanceType({});
        var w6 = this.webRequestTicket.closeCause({});
        $.when(w1,w2,w3,w4,w5,w6).done((response,tickStas,ServiceType,pending,maintenanceType,closeCause)=>{
            response.items.forEach((v)=>{
                v.displayName = (v.fullName == null || v.fullName.trim() == "" ) ? v.username : v.fullName
            });
            var a = tickStas["items"] ; 
            this.AssignTo(response.items);
            this.status(tickStas.items);
            this.serviceType(ServiceType.items);
            this.pendingCause(pending);
            this.MaintenanceType(maintenanceType.items);
            this.closeCause(closeCause);

            dfd.resolve();
        }).fail((e)=>{
            dfd.reject(e);
        });

        return dfd;
    }
    setupExtend(){
        var self = this;
        self.selectedPending.extend({
            required: {           
                onlyIf: function () { 
                    return self.enablePending(); 
                }
            }
        });

        self.pendingRemark.extend({
            required: {           
                onlyIf: function () { 
                    return self.enablePending(); 
                }
            }
        });

        self.selectedMaintenanceType.extend({
            required:{
                onlyIf:function (){
                    return self.enableClose();
                }
            
            }
        
        });

        
        self.selectedClose.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableClose(); 
                }
            }
        });

        self.closeRemark.extend({
            required: {           
                onlyIf: function () { 
                    return self.enableClose(); 
                }
            }
        });





        //this.pendingRemark.extend({ required: true });
        //this.selectedPending.extend({ required: true });
        
        self.selectedStatus.extend({ trackChange: true });
        self.selectedserviceType.extend({ trackChange: true });
        self.selectedAssignTo.extend({ trackChange: true });
        self.remark.extend({ trackChange: true });

        self.validationModel = ko.validatedObservable({
            selectedStatus : self.selectedStatus,
            pendingRemark : self.pendingRemark,
            selectedPending : self.selectedPending,
            selectedMaintenanceType : self.selectedMaintenanceType,
            selectedClose: self.selectedClose,
            closeRemark: self.closeRemark
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {

        super.onActionClick(sender);
        if(sender.id=="actSave"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            } 
            var tId = this.dt().map(x=>x.ticketId);
            var filter = {
                status:(this.selectedStatus())?this.selectedStatus().value:null,
                serviceType:(this.selectedserviceType())?this.selectedserviceType().id:null,
                targetDate:this.targetDate(),
                assignToId:(this.selectedAssignTo())?this.selectedAssignTo().id:null,
                remark:this.remark(),
                pendingCauseId: (this.selectedPending()) ? this.selectedPending().id : null,
                pendingSubCauseId: (this.selectedSubPend()) ? this.selectedSubPend().id : null,
                pendingRemark: this.pendingRemark(),
                ticketIds:tId,
                maintenanceTypeId:(this.selectedMaintenanceType()) ? this.selectedMaintenanceType().id : null,
                closeCauseId: (this.selectedClose()) ? this.selectedClose().id : null,
                closeSubCauseId:(this.selectedSubClose()) ? this.selectedSubClose().id : null,
                closeCauseRemark: this.closeRemark(),
                isQC: this.isQC(),
                isReplaceBoxWithOtherModel: this.isChangeBoxModel(),
                IsMoveDefaultDriver: this.moveDefaultDriverVisible() == true ? this.isMoveDefaultDriver() : null
            };

            this.webRequestTicket.updateTicket(filter).done((response)=>{
                    this.publishMessage("bo-ticket-edit-multiple-service-changed");
                    // go to Ticket Search
                    this.publishMessage("bo-ticket-manage-ticket-search",this.filterState);
                    this.close(true);
                }).fail((e)=>{
                    this.handleError(e);
                });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
       
        
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(TicketDashboardScreen),
    template: templateMarkup
};