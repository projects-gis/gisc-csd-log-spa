﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webRequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import Utility from "../../../../../app/frameworks/core/utility";

class TicketDashboardScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_Dashboard")());
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        this.activeTicketData = ko.observableArray([]);
        this.inActiveTicketData = ko.observableArray([]);
        this.data = ko.observable();
        this.response = ko.observableArray([]);
        
        this.subscribeMessage("bo-ticket-dashboard-change", (resource) => {
            this.getDashboard(resource.items);
            this.data(resource.data);
        });

        this.activeTicketSelected = (val) => {
            var gStatus = val.label;
            var gTime = val.datasetLabel;
            let filter = {};

            if(gTime.indexOf("7") !=-1){
                filter.TicketDuration = Enums.ModelData.TicketDuration.SevenDay;
            }else if(gTime.indexOf("15") !=-1){
                filter.TicketDuration = Enums.ModelData.TicketDuration.FifteenDay;
            }else if(gTime.indexOf(">30") !=-1){
                filter.TicketDuration = Enums.ModelData.TicketDuration.MoreThanThirtyDay;
            }else{ //30 Days
                filter.TicketDuration = Enums.ModelData.TicketDuration.ThirtyDay;
            }

            if(gStatus.indexOf("New") !=-1){
                this.navigate("bo-ticket-manage-ticket-new",{
                    ticketDuration: filter.TicketDuration,
                    data: this.data()
                });
            }else if(gStatus.indexOf("Acknowledge") !=-1){
                this.navigate("bo-ticket-manage-ticket-ack",{
                    ticketDuration: filter.TicketDuration,
                    data: this.data()
                });
            }else if(gStatus.indexOf("InProgress") !=-1){
                this.navigate("bo-ticket-manage-ticket-inprog",{
                    ticketDuration: filter.TicketDuration,
                    data: this.data()
                });
            }else if(gStatus.indexOf("Confirm") !=-1){
                this.navigate("bo-ticket-manage-ticket-confirm",{
                    ticketDuration: filter.TicketDuration,
                    data: this.data()
                });
            }else if(gStatus.indexOf("Pending") !=-1){
                this.navigate("bo-ticket-manage-ticket-pending",{
                    ticketDuration: filter.TicketDuration,
                    data: this.data()
                });
            }else{
                this.navigate("bo-ticket-manage-ticket-follow",{
                    ticketDuration: filter.TicketDuration,
                    data: this.data()
                });
            }
        }

        this.inActiveTicketSelected = (val) => {
            var gTime = val.datasetLabel;
            let filter = {};

            if(gTime.indexOf("7") !=-1){
                filter.TicketDuration = Enums.ModelData.TicketDuration.SevenDay;
            }else if(gTime.indexOf("15") !=-1){
                filter.TicketDuration = Enums.ModelData.TicketDuration.FifteenDay;
            }else if(gTime.indexOf("30") !=-1){
                filter.TicketDuration = Enums.ModelData.TicketDuration.ThirtyDay;
            }else{ //30 Days
                filter.TicketDuration = Enums.ModelData.TicketDuration.FortyFiveDay;
            }
            //go to close status
            this.navigate("bo-ticket-manage-ticket-close",{
                ticketDuration:filter.TicketDuration,
                data:(this.data())?this.data() : null
            });
        }

        this.subscribeMessage("update-items-appointment",(appointId) =>{
            this.getDashboardData();
        });

        this.subscribeMessage("bo-ticket-edit-multiple-service-changed",(data) => {
            this.getDashboardData();
        });
       
    }

    getDashboard(data){
                                     //green    //yellow   //orange   //red
        var backgroundColor = ["rgba(170,231,110,0.5)", "rgba(242,197,17,0.5)", "rgba(242,85,17,0.5)", "rgba(190,63,125,0.5)"];
                                      //green    //yellow   //orange   //purple
        var backgroundInActiveColor = ["rgba(170,231,110,0.5)", "rgba(242,197,17,0.5)", "rgba(242,85,17,0.5)","rgba(89,2,66,0.5)"];

        if(data){
            var activeTicketData = {
                data: {
                    labels: [],
                    datasets: []
                }
            };

            var inActiveTicketData = {
                data: {
                    labels: [],
                    datasets: []
                }
            };

            var activeTicketDataOptions = {};
            var inActiveTicketDataOptions = {};
            var dashboardData = data;
       
            //set labels
            activeTicketData.data.labels = dashboardData.dashboardActive[0].chartName;
            var i = 0;
            dashboardData.dashboardActive[0].label.forEach(function(v){
                activeTicketData.data.datasets.push({
                    label: v.stackName,
                    backgroundColor: backgroundColor[i],
                    data: v.value
                });
                i++;
            });
            //set labels
            inActiveTicketData.data.labels = dashboardData.dashboardInActive[0].chartName;
            i = 0;
            dashboardData.dashboardInActive[0].label.forEach(function(v){
                inActiveTicketData.data.datasets.push({
                    label: v.stackName,
                    backgroundColor: backgroundInActiveColor[i],
                    data: v.value
                });
                i++;
            });
            
            activeTicketDataOptions.yAxes = {
                title: this.i18n("Ticket_TicketQuantity")()
            }
            inActiveTicketDataOptions.yAxes = {
                title: this.i18n("Ticket_TicketQuantity")()
            }

            activeTicketDataOptions.title = this.i18n("Ticket_ActiveTickets")();
            inActiveTicketDataOptions.title = this.i18n("Ticket_InActiveTickets")();

            activeTicketDataOptions.position = "top";
            inActiveTicketDataOptions.position = "right";
            
            this.activeTicketData({data: activeTicketData, options: activeTicketDataOptions});
            this.inActiveTicketData({data: inActiveTicketData, options: inActiveTicketDataOptions});
        }
    }
   
    /**
     * get data dashboard.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }
      
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        this.getDashboardData().done(()=>{
            dfd.resolve();
        });
        return dfd;
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id =="cmdSearch"){
            this.navigate("bo-ticket-searchDashboard");
        }
    }

    getDashboardData(){
        var dfd = $.Deferred();
            
        var d1 = this.webRequestTicket.dashboardTicket({});
    
        $.when(d1).done((r1) => {
            this.getDashboard(r1);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

}

export default {
    viewModel: ScreenBase.createFactory(TicketDashboardScreen),
    template: templateMarkup
};