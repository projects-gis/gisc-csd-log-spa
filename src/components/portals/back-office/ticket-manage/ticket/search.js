﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webRequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";

class TicketDashboardScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_SearchDashBoard")());
        this.bladeSize = BladeSize.Small;
        //this.bladeIsMaximized = true;

        this.listCompany = ko.observableArray([]);
        this.listBu = ko.observableArray([]);
        this.listTech = ko.observableArray([]);
        this.listServiceType = ko.observableArray([]);
        this.listOwner = ko.observableArray([]);
        this.selectedOwner = ko.observable();
        this.selectedAssignTo = ko.observable();
        this.selectedServiceType = ko.observable();
        this.selectedBu = ko.observable();
        this.selectedCompany = ko.observable();
        this.selectedTech = ko.observable();
        this.contract = ko.observable();
        this.isIncludeSubBu = ko.observable(true);
        this.vehicleOptions = ko.observableArray();
        this.vehicleId = ko.observable();
        this.ticketId = ko.observable();
        this.license = ko.observable();

        this.selectedCompany.subscribe((val)=>{
            let id = (typeof val =='undefined') ? null : val.id;
            this.webRequestBusinessUnit.listBusinessUnitSummary({companyId:id}).done((response)=>{
                this.listBu(response["items"]);
            });
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.vehicleOptions([]);
            if(this.selectedBu() != undefined){
                var businessUnitIds = (this.isIncludeSubBu()) ? Utility.includeSubBU(this.listBu(),this.selectedBu()) : this.selectedBu();
                var currentCompanyId = (typeof this.selectedCompany() == 'undefined') ? null : this.selectedCompany().id;
                //Vehicle
                this.webRequestVehicle.listVehicleSummary({
                    companyId: currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                     let listVehicle = response.items;
                    // //add displayName "Report_All" and id 0
                    // if(Object.keys(listVehicle).length > 0 && listVehicle[0].id !== 0){
                    //     listVehicle.unshift({
                    //         license: this.i18n('Report_All')(),
                    //         id: 0
                    //     });
                    // }
                    this.vehicleOptions(listVehicle);
                });
            }
            return ' ';
        });
    }
    setupExtend(){
     
    }
   
    /**
     * get data dashboard.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get webRequestUser(){
        return WebRequestUser.getInstance();
    }
    get webRequestEnumResource(){
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        let enumAdmin = {
            groupIds: [Enums.UserGroup.Administrator, Enums.UserGroup.SystemAdmin],
            sortingColumns: DefaultSorting.User,
            enable:true};
        let enumTechnician = { groupIds:[Enums.UserGroup.Technician] };
        let dfd = $.Deferred();
        let EnumServiceType = { types:[Enums.ModelData.EnumResourceType.ServiceType] };

        var listCompany = this.webRequestCompany.listCompanySummary({});
        var listTechnician = this.webRequestUser.listUserSummary(enumTechnician);
        //var listServ = this.webRequestEnumResource.listEnumResource(EnumServiceType);
        var listServ = this.webRequestTicket.listServiceTypeSummary();
        var listAdmin = this.webRequestUser.listUserSummary(enumAdmin);

        $.when(listCompany,listTechnician,listServ,listAdmin).done((r1,r2,r3,r4) => {
            this.listCompany(r1["items"]);
            this.listTech(r2["items"]);
            this.listServiceType(r3["items"]);

            //loop if fullname null or "" show username
            r4.items.forEach((v)=>{
                v.displayName = (v.fullName==null || v.fullName.trim()=="") ? v.username:v.fullName

            });
            this.listOwner(r4["items"]);
            dfd.resolve ();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;

    }

   

    modelData(){
        var model = {
            CompanyId : (this.selectedCompany()) ? this.selectedCompany().id:null,
            BusinessUnitId : (this.selectedBu())? this.selectedBu():null,
            ContractNo: (this.contract())? this.contract():null,
            TechnicianId : (this.selectedTech())? this.selectedTech().id:null,
            ServiceType : (this.selectedServiceType())? this.selectedServiceType().id:null,
            IncludeSubBusinessUnit : (this.isIncludeSubBu()) ? true : false,
            OwnerId : (this.selectedOwner()) ? this.selectedOwner().id : null ,
            AssignToId : (this.selectedAssignTo()) ? this.selectedAssignTo().id:null,
            TicketId : this.ticketId() ? this.ticketId() : null,
            VehicleId: this.vehicleId() ? this.vehicleId().id : null,
            License : (this.license())? this.license():null,
        };
        return model ; 
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(),"svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id=="actSearch"){
            this.webRequestTicket.dashboardTicket(this.modelData()).done((response) => {
                this.publishMessage("bo-ticket-dashboard-change",{
                    items:response,
                    data:this.modelData()
                });
            });
        }
        
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id=="cmdClear"){
            this.selectedBu('');
            this.selectedCompany('');
            this.selectedTech('');
            this.selectedServiceType('');
            this.selectedOwner('');
            this.selectedAssignTo(''); 
            this.contract(null);
            this.selectedTicket(null);
            this.vehicleId(null);
            this.license(null);
        }
        
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(TicketDashboardScreen),
    template: templateMarkup
};