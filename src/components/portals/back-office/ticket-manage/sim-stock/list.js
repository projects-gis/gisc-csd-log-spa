import ko from "knockout";
import templateMarkup from "text!./list.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestSimStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestSimStock";
import ScreenBase from "../../../screenbase";

class SimStockListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Sim Stock List")());
        this.bladeSize = BladeSize.Medium;

        this.simStockInfos = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);
        this.filterText = ko.observable("");
        this.selectedSimStock = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);

        this._selectingRowHandler = (val) => {
            if(val) {
                return this.navigate("bo-ticket-manage-sim-stock-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: val.id });
            }
            return false;
        };
        this.subscribeMessage("bo-ticket-sim-stock-changed", (id) => {
            // Refresh entire datasource
            this.isBusy(true);
             this.webRequestSimStock.listSimStockSummary({}).done((response)=> {
                this.simStockInfos(response["items"]);
                this.recentChangedRowIds.replaceAll([id]);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });
    }

    get webRequestSimStock(){
        return WebRequestSimStock.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        this.webRequestSimStock.listSimStockSummary({}).done((r) => {
            this.simStockInfos(r["items"]);
            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
            dfd.reject(e);
        });

        return dfd;

    }

    generateModel() {
        return {};

    }

    setupExtend() {


        this.validationModel = ko.validatedObservable({
        });
    }


    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        //if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxsStock)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        //}
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-ticket-manage-sim-stock-manage", { mode: Screen.SCREEN_MODE_CREATE });
            this.recentChangedRowIds.removeAll();
        }
    }

     /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSimStock(null);
    }
}
export default {
    viewModel: ScreenBase.createFactory(SimStockListScreen),
    template: templateMarkup
};