import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import WebRequestSimStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestSimStock";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestTechnicianTeam from "../../../../../app/frameworks/data/apicore/webRequestTechnicianTeam";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class SimStockManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Set blade title
        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Create Sim Stock")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Update Sim Stock")());
        }
        this.simStockId = this.ensureNonObservable(params.id, null);
        this.name = ko.observable();
        this.code = ko.observable();
        this.desc = ko.observable();

        this.techTeamInfos = ko.observableArray([]);
        this.selectedTechTeam = ko.observable();
    }

    get webRequestSimStock() {
        return WebRequestSimStock.getInstance();
    }
    get webRequestTechnicianTeam() {
        return WebRequestTechnicianTeam.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d1 = this.simStockId ? this.webRequestSimStock.getSimStock(this.simStockId) : null;
        
        $.when(d1).done((r1) => {

            if (r1) {
                this.name(r1.name);
                this.code(r1.code);
                this.desc(r1.description);
            }

            this.webRequestTechnicianTeam.listTechnicianTeamSummary({ 
                stockType: Enums.ModelData.StockType.SimStock,
                technicianTeamId: (this.mode === Screen.SCREEN_MODE_UPDATE) ? r1.technicianTeamId : null,
             }).done((techTeamInfo) => {
                this.techTeamInfos(techTeamInfo["items"]);
                if(r1){
                    this.selectedTechTeam(ScreenHelper.findOptionByProperty(this.techTeamInfos, "id", r1.technicianTeamId));
                }
                dfd.resolve();
             });
            
        }).fail((e) => {
            this.handleError(e);
            dfd.reject();
        });

        return dfd;
    }

    generateModel() {
        return {
            name: this.name(),
            code: this.code(),
            description: this.desc(),
            technicianTeamId: this.selectedTechTeam() ? this.selectedTechTeam().id : null
        };

    }
    setupExtend() {
        //Track change
        this.name.extend({
            required: true,
            trackChange: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.code.extend({
            trackChange: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.desc.extend({
            trackChange: true
        });

        this.selectedTechTeam.extend({
            trackChange: true
        });


        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code
        });
    }


    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            //if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxsStock)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            //}
        }
    }

    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null,"Are you sure you want to delete this sim’s stock?", BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestSimStock.deleteSimStock(this.simStockId).done(() => {
                            this.isBusy(false);

                            this.publishMessage("bo-ticket-sim-stock-changed");
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);

                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            let info = this.generateModel();

            this.isBusy(true);

            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestSimStock.createSimStock(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-ticket-sim-stock-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    model.id = this.simStockId;
                    this.webRequestSimStock.updateSimStock(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-ticket-sim-stock-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }
}
export default {
    viewModel: ScreenBase.createFactory(SimStockManageScreen),
    template: templateMarkup
};