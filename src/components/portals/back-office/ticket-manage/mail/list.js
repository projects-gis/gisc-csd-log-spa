﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";

class MailManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Ticket_MailManagement")());
        this.bladeSize = BladeSize.Large;
        
        this.apiDataSource = ko.observableArray([]);

        this.selectRow = (data) => {
            this.navigate("bo-ticket-manage-mail-manage",
                {
                    mode: 'update', 
                    mailId: data.id
                });
        };

        // Observe change about filter
        this.subscribeMessage("bo-ticket-mail-management-changed",(data) => { 
            this.apiDataSource({ 
                read: this.webRequestTicket.listTicketMailManagementDataGrid(data),
                update: this.webRequestTicket.listTicketMailManagementDataGrid(data)
            });
        });
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = { 
            
        };

        this.apiDataSource({
            read: this.webRequestTicket.listTicketMailManagementDataGrid(filter),
            update: this.webRequestTicket.listTicketMailManagementDataGrid(filter)
        });

    }

    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(),"svg-cmd-add" ));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-ticket-manage-mail-manage", { mode: 'create' });
                // this.recentChangedRowIds.removeAll();
                break;
        }
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(MailManageScreen),
    template: templateMarkup
};