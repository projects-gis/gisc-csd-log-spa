﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";

class TicketTransporterManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;

        this.data = this.ensureNonObservable(params.data, []);
        this.mode = this.ensureNonObservable(params.mode, Screen.SCREEN_MODE_CREATE);
        this.code = ko.observable();
        this.name = ko.observable();
        this.formalName = ko.observable();

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Update Transporter")());
        } else {
            this.bladeTitle(this.i18n("Create Transporter")());
        }

        this.status = ko.observableArray([]);
        this.selectedStatus = ko.observable();

        this.mailTo = ko.observable();
        this.mailCC = ko.observable();
        this.mailSubject = ko.observable();
        this.mailBody = ko.observable();

        this.selectedSubMenuItem = ko.observable(null);

        this.vehicles = ko.observableArray([]);


        // Subscribe Message when signature create/update 
        this.subscribeMessage("ticket-transporter-vehicle", (data) => {

            let newData = _.clone(data);
            this.vehicles(newData);
        });

    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestTransporter() {
        return WebRequestTransporter.getInstance();
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }


    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        } else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.goToSubPage(index);
                        break;
                }
            });
        }
    }

    goToSubPage(index) {
        var page = "";
        var options = {};

        switch (index) {
            case 1:
                page = "bo-ticket-manage-transporter-manage-vehicle";
                // options.mode = this.mode
                options.vehicles = this.vehicles();

                break;
        }

        this.forceNavigate(page, options);
        this.selectedSubMenuItem(index);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        let statusInfo = [
            {
                id: 1,
                displayName: "Active",
                value: true
            },
            {
                id: 2,
                displayName: "In Active",
                value: false
            }
        ];
        this.status(statusInfo);

        if(this.mode == Screen.SCREEN_MODE_UPDATE){
            this.webRequestTransporter.getTransporter(this.data.id).done((res)=>{
                this.code(res.code);
                this.name(res.name);
                this.formalName(res.formalName);
                this.mailTo(res.mailTo);
                this.mailCC(res.mailCC);
                this.mailSubject(res.mailSubject);
                this.mailBody(res.mailBody);
                this.selectedStatus(ScreenHelper.findOptionByProperty(this.status,"value", res.enable));
                let vehicles = _.size(res.vehicleInfos) ? res.vehicleInfos : [];
                this.vehicles(vehicles);
                dfd.resolve();
            }).fail((e)=>{
                this.handleError(e);
                dfd.reject();
            })
        }else{
            dfd.resolve();
        }

        return dfd;
    }

   
    setupExtend() {


        this.code.extend({ trackChange: true });
        this.name.extend({ trackChange: true });
        this.formalName.extend({ trackChange: true });
        this.selectedStatus.extend({ trackChange: true });
        this.mailTo.extend({ trackChange: true });
        this.mailCC.extend({ trackChange: true });
        this.mailSubject.extend({ trackChange: true });
        this.mailBody.extend({ trackChange: true });
        this.vehicles.extend({ trackArrayChange: true });

        this.code.extend({ 
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.name.extend({ 
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.formalName.extend({ required: true });
        this.selectedStatus.extend({ required: true });
        this.mailTo.extend({ required: true });
        this.mailSubject.extend({ required: true });
        this.mailBody.extend({ required: true });
        
        this.mailTo.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("M064")()
            }
        });

        this.mailCC.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("M064")()
            }
        });
        
        this.vehicles.extend({
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            code: this.code,
            name: this.name,
            formalName: this.formalName,
            selectedStatus: this.selectedStatus,
            mailSubject: this.mailSubject,
            mailBody: this.mailBody,
            mailTo: this.mailTo,
            mailCC: this.mailCC,
            vehicles: this.vehicles
        });
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {

        let vehicleIds = new Array();
        _.forEach(this.vehicles(), (item)=>{
            vehicleIds.push(item.vehicleId);
        })

        var model = {
            id: (this.data) ? this.data.id : 0,
            code: this.code(),
            name: this.name(),
            formalName: this.formalName(),
            enable: this.selectedStatus() && this.selectedStatus().value,
            mailSubject: this.mailSubject(),
            mailBody: this.mailBody(),
            mailTo: this.mailTo(),
            mailCC: this.mailCC(),
            vehicleIds: vehicleIds
        };
        return model;
    }


    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === "update") {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestTransporter.createTransporter(model).done((response) => {
                        this.publishMessage("ticket-transporter-manage-change");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestTransporter.updateTransporter(model).done((response) => {
                        this.publishMessage("ticket-transporter-manage-change");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, "Do you want to delete this Transporter?", BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestTransporter.deleteTransporter(this.data.id).done(() => {
                                this.isBusy(false);
                                this.publishMessage("ticket-transporter-manage-change");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;

        }
    }
}
export default {
viewModel: ScreenBase.createFactory(TicketTransporterManageScreen),
    template: templateMarkup
};