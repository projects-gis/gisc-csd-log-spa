﻿import ko from "knockout";
import templateMarkup from "text!./find-vehicle.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
// import AssetUtility from "../../../company-workspace/asset-utility"
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import AssetUtility from "../../../company-workspace/asset-utility"

class TicketTransporterFindVehicleScreen extends ScreenBase {
    constructor(params) {
        super(params);


        this.bladeTitle(this.i18n("Transporters")());
        this.bladeSize = BladeSize.Small;


        this.companyOptions = ko.observableArray([]); 
        this.businessUnitOptions = ko.observableArray([]);

        this.selectedCompany = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);

        this.selectedAllItems = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);

        this.results = ko.observableArray([]);
        this.dataSource = ko.observableArray([]);
        this.companyName = ko.observable();

        this.selectedCompany.subscribe((value) => {
            
            if(value){
                this.results([]);
                this.companyName(value.name);
                this.isBusy(true);
                var filterBU = {
                    companyId: value.id,
                    sortingColumns: DefaultSorting.BusinessUnit,
                    includeAssociationNames: [
                        EntityAssociation.BusinessUnit.ChildBusinessUnits
                    ]
                };
                this.webRequestBusinessUnit.listBusinessUnitSummary(filterBU).done((res)=>{
                    this.businessUnitOptions(res.items);
                    this.isBusy(false);
                });
            }
        });

        this.selectedItems.subscribe((value) => {
    
            if(this.selectedAllItems().length == 0){
                this.selectedAllItems.push(value);
            }else{
                var checkItem = _.filter(this.selectedAllItems(), ['id', value.id]);
                if(_.size(checkItem)){
                    _.forEach(this.selectedAllItems(), (obj) => {
                        if(obj.id == value.id){
                            obj.items = value.items;
                        }
                    });
                }else{
                    this.selectedAllItems.push(value);
                }
            }
        });
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for List Asset module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    searchVehicle(){

        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }
        
        this.isBusy(true);
        let filter = {
            companyId: this.selectedCompany().id,
            businessUnitIds: this.selectedBusinessUnit(),
            searchEntityType: Enums.ModelData.SearchEntityType.Vehicle,

        }
        // let filter = JSON.parse('{"companyId":1,"businessUnitIds":["1","80","9","31","33","82","11","38","26","28","5","39","119","41","110","117","43","104","105","44","27","227","52","111","54","109","107","108","53","55","112","56","57","58","59","6","114","115","62","99","113","83","63","84","64","116","65","66","98","13","67","29","69","70","71","86","72","73","74","75","87","76","77","47","78","79","15","204","49","16","89","88","17","3","100","18","101","102","103","19","106","20","21","22","25","118","30"],"searchEntityType":1}');
        var dfd = $.Deferred();
        this.webRequestFleetMonitoring.listAsset(filter).done((res)=>{
            this.dataSource(res);
            dfd.resolve();
        });

        $.when(dfd).done(()=>{
            var results = _.map(this.dataSource(), (item, index) => {
                var result = new FindAssetsResultItem(item, index, this.companyName());
                return result;
            });
            
            this.results(results);
            this.genAccordion();
            this.isBusy(false);
        })
        
    }


    genAccordion() {

        $("#" + this.id).each(function(){
            $(this).addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
          .find("h3")
            .addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
            .hover(function() { $(this).toggleClass("ui-state-hover"); })
            .prepend('<span class="ui-icon ui-icon-triangle-1-e"></span>')
            .click(function() {
              $(this)
                .toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
                .find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
                .next().slideToggle();
              return false;
            })
            .next()
              .addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom")
              .hide();
        });

        //set padding accordion content
        $("#" + this.id + " .ui-accordion-content").css({
            "padding": "0 5px 8px 5px"
        });
        
        //set css kendo data grid on click
        $("ul#" + this.id + ">li>h3").click((e) => {
            var active = $(e.target).hasClass("ui-state-active");
            if(active){
                $("#" + this.id + " .k-grid-header").css({
                    "padding": "0"
                });
                $("#" + this.id + " .k-header").css({
                    "background-color": "#ffffff"
                });
                $("#" + this.id + " .k-grid-content").css({
                    "overflow-y": "visible"
                });
            }
        });

    }

    setupExtend() {
        this.selectedCompany.extend({ required: true });
        this.selectedBusinessUnit.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedCompany: this.selectedCompany,
            selectedBusinessUnit: this.selectedBusinessUnit
        });

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();

        var filterCompany = { sortingColumns: DefaultSorting.CompanyByName };

        var d1 = this.webRequestCompany.listCompanySummary(filterCompany);

        $.when(d1).done((r1) => {
            this.companyOptions(r1["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;

    }

    onDomReady() {
        
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actApply", this.i18n("Common_Apply")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        // commands.push(this.createCommand("cmdAdd", this.i18n("Common_Add")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actApply") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            

            // _.forEach(this.selectedAllItems(), (obj) => {
            //     if(_.size(obj.items)){
            //         obj.businessUnitPath = obj.items[0].businessUnitPath;
            //     }else{
            //         obj.businessUnitPath = "No businessUnitPath"
            //     }
            // });

            var items = new Array();
            _.forEach(this.selectedAllItems(), (obj) => {
                if(_.size(obj.items)){
                    _.forEach(obj.items, (item) => {
                        if(_.size(obj.items)){
                            items.push(item);
                        }
                    });
                }
            });

            // console.log(_.groupBy(items, 'businessUnitPath'));
            _.forEach(this.selectedAllItems(), (item)=>{
                this.dispatchEvent(item.id, "refresh");
            })
            
            this.publishMessage("ticket-transporter-find-vehicle", items);

        }

    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {

            // case "cmdAdd":
            //     this.navigate("bo-ticket-manage-transporter-manage-vehicle");
            // break;
        }
    }

    onUnLoad(){
        
    }
    
}

class FindAssetsResultItem {
    constructor (info, index, companyName) {
        
        var data = _.clone(info);
        this.id = data.id;
        _.forEach(data.assets, (item)=>{
            item.businessUnitPath = data.businessUnitPath;
            item.companyName = companyName;
        });
        
        var items = data.assets;
        this.name = data.businessUnitPath + " (" + items.length + ")";
        // var columns = [
        //     { type: 'label', title: 'i18n:License', data: 'license', searchable: true }
        // ];

        this.assets = ko.observableArray(items);

        this.dtId = 'dgTicketTransporterVehicle' + index;
        // this.items = this.onDatasourceRequestRead.bind(this);
        // this.columns = ko.observable(columns);
        this.order = ko.observableArray([ 1, "asc" ]);
        this.filterText = ko.observable();
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = this.assets();
        var totalItems = items.length;
        dfd.resolve({items: items, totalRecords: totalItems, currentPage: 1});
        return dfd;
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketTransporterFindVehicleScreen),
    template: templateMarkup
};