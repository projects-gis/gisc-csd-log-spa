﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import AssetUtility from "../../../company-workspace/asset-utility";
import webRequestVendor from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendor";

class TicketVendorsScreen extends ScreenBase {
    constructor(params) {
        super(params);


        this.bladeTitle(this.i18n("Vendors")());
        this.bladeSize = BladeSize.XMedium;

        this.selectedRow = (data) => {
            this.navigate("bo-ticket-manage-vendor-manage", { data: data, mode: Screen.SCREEN_MODE_UPDATE });
        }

        this.subscribeMessage("bo-ticket-vendor-list-refresh", info => {
            this.dispatchEvent("Ticket-Vendors", "refresh");
        });
        
    

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
    * Get WebRequest specific for Vendor module in Web API access.
    * @readonly
    */
    get webRequestVendor() {
        return webRequestVendor.getInstance();
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        this.webRequestVendor.listVendorSummary(gridOption).done((res) => {
            dfd.resolve({
                items : res.items,
                totalRecords : res.items.length
            });
        }).fail((e) => {
            dfd.reject();
            this.handleError(e);
        });
        return dfd;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);


    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {

            case "cmdCreate":
                this.navigate("bo-ticket-manage-vendor-manage", { mode: Screen.SCREEN_MODE_CREATE });
            break;
        }
    }

    onUnLoad(){
        
    }
    
}

export default {
    viewModel: ScreenBase.createFactory(TicketVendorsScreen),
    template: templateMarkup
};