﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import webRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import webRequestVendor from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendor";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";

class TicketVendorsManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;

        this.data = this.ensureNonObservable(params.data, {});
        this.id = (params.data) ? params.data.id : -1;
        this.mode = this.ensureNonObservable(params.mode, Screen.SCREEN_MODE_CREATE);
        this.recentChangedRowIds  = ko.observableArray();

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Update Vendor")());
        } else {
            this.bladeTitle(this.i18n("Create Vendor")());
        }

        // Logo
        this.logo = ko.observable(null);
        this.logoFileName = ko.observable("");
        this.logoSelectedFile = ko.observable(null);
        this.logoMimeType = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();
        this.logoSuggestionWidth = "59";
        this.logoSuggestionHeight = "37";
        this.logoUrl = ko.pureComputed(() => {
            if (this.logo() && this.logo().infoStatus !== Enums.InfoStatus.Delete) {
                return this.logo().fileUrl;
            }
            else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        // Stamp Logo
        this.stampLogo = ko.observable(null);
        this.stampLogoFileName = ko.observable("");
        this.stampLogoSelectedFile = ko.observable(null);
        this.stampLogoMimeType = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();
        this.stampLogoSuggestionWidth = "59";
        this.stampLogoSuggestionHeight = "37";
        this.stampLogoUrl = ko.pureComputed(() => {
            if (this.stampLogo() && this.stampLogo().infoStatus !== Enums.InfoStatus.Delete) {
                return this.stampLogo().fileUrl;
            }
            else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        this.vendorId = ko.observable();
        this.code = ko.observable();
        this.name = ko.observable();
        this.masterFileUserName = ko.observable();
        this.masterFilePassword = ko.observable();
        this.gpsTransactionUserName = ko.observable();
        this.gpsTransactionPassword = ko.observable();
        this.companyName = ko.observable();
        this.prefixLetter = ko.observable();
        this.address = ko.observable();
        this.signatures = ko.observableArray([]);
        this.orderSignature = ko.observable([[ 1, "asc" ]]);
        this.recentChangedRowIds = ko.observableArray([]);
        this._originalSignatures = [];
        this.fileUploadURL = WebConfig.appSettings.uploadUrlTrackingCore;
 

        // Subscribe Message when signature create/update
        this.subscribeMessage("bo-ticket-vendor-signature", (data) => {
            if(data.dataUpdate){
                this.signatures.replaceAll(data.dataUpdate);
                this.recentChangedRowIds.replaceAll([data.idUpdate]);
            }else{
                this.signatures.push(data);
                this.recentChangedRowIds.replaceAll([data.id]);
            }

            // this._originalSignatures = _.cloneDeep(this.signatures());

        });
        
        this._selectingRowHandler = (data) => {

            if(data) {
                console.log({
                    data: data,
                    dataOnDt: this.signatures(),
                    mode: 'update'
                });
                return this.navigate("bo-ticket-manage-vendor-manage-signature", {
                    data: data,
                    dataOnDt: this.signatures(),
                    mode: 'update'
                });
            }
            return false;
        };

    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }

    get webRequestVendor() {
        return webRequestVendor.getInstance();
    }

    /**
     * On click remove logo icon
     */
    onRemoveFileLogo() {
        var logo = this.logo();
        if (logo) {
            if(logo.infoStatus === Enums.InfoStatus.Add){
                this.logo(null);
            }
            else {
                logo.infoStatus = Enums.InfoStatus.Delete;
                this.logo(logo);
            }
        }
    }

    /**
     * Hook onbefore fileupload
     */
    onBeforeUploadLogo() {
        //remove current file before upload new file
        this.onRemoveFileLogo();
    }

    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccessLogo(data) {
        if(data && data.length > 0){
            var newLogo = data[0];
            var currentLogo = this.logo();

            if (currentLogo) {
                switch (currentLogo.infoStatus) {
                    case Enums.InfoStatus.Add:
                        newLogo.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        newLogo.infoStatus = Enums.InfoStatus.Update;
                        newLogo.id = currentLogo.id;
                        break;
                }
            }
            else {
                newLogo.infoStatus = Enums.InfoStatus.Add;
            }
            this.logo(newLogo);
        }
    }

    /**
     *
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFailLogo(e) {
        this.handleError(e);
    }


    /**
     * On click remove stamp logo icon
     */
    onRemoveFileStampLogo() {
        var logo = this.stampLogo();
        if (logo) {
            if(logo.infoStatus === Enums.InfoStatus.Add){
                this.stampLogo(null);
            }
            else {
                logo.infoStatus = Enums.InfoStatus.Delete;
                this.stampLogo(logo);
            }
        }
    }

    /**
     * Hook onbefore fileupload
     */
    onBeforeUploadStampLogo() {
        //remove current file before upload new file
        this.onRemoveFileStampLogo();
    }

    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccessStampLogo(data) {
        if(data && data.length > 0){
            var newLogo = data[0];
            var currentLogo = this.stampLogo();

            if (currentLogo) {
                switch (currentLogo.infoStatus) {
                    case Enums.InfoStatus.Add:
                        newLogo.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        newLogo.infoStatus = Enums.InfoStatus.Update;
                        newLogo.id = currentLogo.id;
                        break;
                }
            }
            else {
                newLogo.infoStatus = Enums.InfoStatus.Add;
            }

            this.stampLogo(newLogo);
        }
    }


    /**
     *
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFailStampLogo(e) {
        this.handleError(e);
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        
        var dfd = $.Deferred();
        if(this.mode == Screen.SCREEN_MODE_UPDATE){
            this.webRequestVendor.getVendor(this.id).done((res)=>{

                if(res.id){
                    //logo
                    this.logo(res.logo)
                    this.logoFileName(res.logo && res.logo.fileName);
                    var fileLogo = new FileUploadModel();
                    fileLogo.name = res.logo && res.logo.fileName;
                    fileLogo.extension = "png";
                    fileLogo.imageWidth = this.logoSuggestionWidth;
                    fileLogo.imageHeight = this.logoSuggestionHeight;
                    this.logoSelectedFile(fileLogo);

                    //stamp logo
                    this.stampLogo(res.stampLogo)
                    this.stampLogoFileName(res.stampLogo && res.stampLogo.fileName);
                    var fileStampLogo = new FileUploadModel();
                    fileStampLogo.name = res.stampLogo && res.stampLogo.fileName;
                    fileStampLogo.extension = "png";
                    fileStampLogo.imageWidth = this.stampLogoSuggestionWidth;
                    fileStampLogo.imageHeight = this.stampLogoSuggestionHeight;
                    this.stampLogoSelectedFile(fileStampLogo);

                    this.vendorId(res.vendorId);
                    this.code(res.code);
                    this.name(res.name);
                    this.masterFileUserName(res.masterFileUsername);
                    this.masterFilePassword(res.masterFilePassword);
                    this.gpsTransactionUserName(res.gpsTransactionUsername);
                    this.gpsTransactionPassword(res.gpsTransactionPassword);
                    this.companyName(res.companyName);
                    this.prefixLetter(res.prefixLetter);
                    this.address(res.address);
                    this.signatures(res.vendorSignatureInfos);
                    this._originalSignatures = _.cloneDeep(res.vendorSignatureInfos);
                }
                dfd.resolve();
            }).fail((e) => {
                dfd.reject();
            });
        }else{
            dfd.resolve();
        }
        

        return dfd;
    }

   
    setupExtend() {

        this.vendorId.extend({trackChange: true});
        this.logo.extend({ trackChange: true });
        this.stampLogo.extend({ trackChange: true });
        this.code.extend({ trackChange: true });
        this.name.extend({ trackChange: true });
        this.masterFileUserName.extend({ trackChange: true });
        this.masterFilePassword.extend({ trackChange: true });
        this.gpsTransactionUserName.extend({ trackChange: true });
        this.gpsTransactionPassword.extend({ trackChange: true });
        this.companyName.extend({ trackChange: true });
        this.prefixLetter.extend({ trackChange: true });
        this.address.extend({ trackChange: true });
        this.signatures.extend({
            trackArrayChange: {
                uniqueFromPropertyName: "id"
            }
        });

        this.logoSelectedFile.extend({
            fileRequired: true,
            fileExtension: ['jpg', 'png']
        });
        this.stampLogoSelectedFile.extend({
            fileRequired: true,
            fileExtension: ['jpg', 'png']
        });
        this.code.extend({ 
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.name.extend({ 
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
         });

         this.vendorId.extend({ 
            required: true,
            serverValidate: {
                params: "VendorId",
                message: this.i18n("M010")()
            }
         });

        this.masterFileUserName.extend({ required: true });
        this.masterFilePassword.extend({ required: true });
        this.gpsTransactionUserName.extend({ required: true });
        this.gpsTransactionPassword.extend({ required: true });
        this.companyName.extend({ required: true });
        this.prefixLetter.extend({ required: true });
        this.address.extend({ required: true });
        this.signatures.extend({
            arrayRequired: true
        });

        this.validationModel = ko.validatedObservable({
            logoSelectedFile: this.logoSelectedFile,
            stampLogoSelectedFile: this.stampLogoSelectedFile,
            logo: this.logo,
            stampLogo: this.stampLogo,
            code: this.code,
            name: this.name,
            masterFileUserName: this.masterFileUserName,
            masterFilePassword: this.masterFilePassword,
            gpsTransactionUserName: this.gpsTransactionUserName,
            gpsTransactionPassword: this.gpsTransactionPassword,
            companyName: this.companyName,
            prefixLetter: this.prefixLetter,
            address: this.address,
            signatures: this.signatures,
            vendorId: this.vendorId
        });
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var listSignatures = [];
        var model = {
            id: this.id,
            vendorId: this.vendorId(),
            logo: this.logo(),
            stampLogo: this.stampLogo(),
            code: this.code(),
            name: this.name(),
            masterFileUserName: this.masterFileUserName(),
            masterFilePassword: this.masterFilePassword(),
            gpsTransactionUserName: this.gpsTransactionUserName(),
            gpsTransactionPassword: this.gpsTransactionPassword(),
            companyName: this.companyName(),
            prefixLetter: this.prefixLetter(),
            address: this.address(),
            vendorSignatureInfos : this.signatures()
        };



        if(this.mode === Screen.SCREEN_MODE_UPDATE ) {

            this.signatures().forEach((v)=>{
                listSignatures.push(v);
            });

            this._originalSignatures.forEach((o)=>{
                let findItem = $.grep(this.signatures() , (n,m) => {
                    return o.id == n.id ;
                });
                if(findItem.length == 0){
                    o.infoStatus = Enums.InfoStatus.Delete;
                    listSignatures.push(o);
                }
            });
            
            model.vendorSignatureInfos = listSignatures;
        }

        return model;
    }

    addSignature() {
        this.navigate("bo-ticket-manage-vendor-manage-signature", { 
            data : this.generateModel(),
            mode: 'create',
            dataOnDt: this.signatures()
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(this.mode == Screen.SCREEN_MODE_UPDATE){
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestVendor.createVendor(model).done((response) => {
                        this.publishMessage("bo-ticket-vendor-list-refresh");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestVendor.updateVendor(model).done((response) => {
                        this.publishMessage("bo-ticket-vendor-list-refresh");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {

            case "cmdDelete":
                this.showMessageBox(null, "Are you sure you want to delete Vendor?", BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestVendor.deleteVendor(this.id).done(() => {
                                this.isBusy(false);
                                this.publishMessage("bo-ticket-vendor-list-refresh");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                        break;
                    }
                });
            break;
        }
    }
}
export default {
viewModel: ScreenBase.createFactory(TicketVendorsManageScreen),
    template: templateMarkup
};