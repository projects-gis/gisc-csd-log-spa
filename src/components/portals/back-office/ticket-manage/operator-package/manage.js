﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants, Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";


class OperatorPackageManageScrren extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Assets_CreateOperatorPackage")());
        this.bladeSize = BladeSize.Small;

        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Assets_CreateOperatorPackage")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Assets_UpdateOperatorPackage")());
        }
        
        this.operatorId = this.ensureNonObservable(params.id, -1);
        this.operator = ko.observable();
        this.promotion = ko.observable();
        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.apn = ko.observable();
        this.apnUsername = ko.observable();
        this.apnPassword = ko.observable();
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
    }

     /**
     * Get WebRequest specific for Operator Package in Tracking Web API access.
     * @readonly
     */
    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }

    
    /**
     * 
     * @param {any} isFirstLoad
     * @returns
     * @memberOf OperatorPackageManageScrren
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();

    
        var d1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestOperatorPackage.getOperatorPackage(this.operatorId) : null;

        $.when(d1).done((operator) => {

            if(operator){
                this.operator(operator.operator)
                this.promotion(operator.promotion);
                this.startDate(operator.startDate);
                this.endDate(operator.endDate);
                this.apn(operator.apn);
                this.apnUsername(operator.apnUsername);
                this.apnPassword(operator.apnPassword);
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    
    /**
     * Setup validation and trackChange.
     * @memberOf OperatorPackageManageScrren
     */
    setupExtend() {
        //Track change
        this.operator.extend({
            trackChange: true
        });
        
        this.promotion.extend({
            trackChange: true
        });

        this.startDate.extend({
            trackChange: true
        });

        this.endDate.extend({
            trackChange: true
        });

        this.apn.extend({
            trackChange: true
        });

        this.apnUsername.extend({
            trackChange: true
        });

        this.apnPassword.extend({
            trackChange: true
        });

         // Validation
        this.operator.extend({
            required: true
        });

        this.promotion.extend({
            required: true
        });

        this.startDate.extend({
            required: true
        });

        this.endDate.extend({
            required: true
        });

        this.apn.extend({
            required: true,
        });

         this.validationModel = ko.validatedObservable({
            operator: this.operator,
            promotion: this.promotion,
            startDate: this.startDate,
            endDate: this.endDate,
            apn: this.apn
        });
    }

     generateModel() {

        var model = {
            operator: this.operator(),
            promotion: this.promotion(),
            startDate: this.startDate(),
            endDate: this.endDate(),
            apn: this.apn(),
            apnUsername: this.apnUsername(),
            apnPassword: this.apnPassword(),
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.operatorId
        };
        return model;
    }

     /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

        switch(this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    if (WebConfig.userSession.hasPermission(Constants.Permission.OperatorPackage)) {
                        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                         actions.push(this.createActionCancel());
                    }
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    if (WebConfig.userSession.hasPermission(Constants.Permission.OperatorPackage)) {
                        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                         actions.push(this.createActionCancel());
                    }
                    break;
        }
       
    }

     /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }


            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestOperatorPackage.createOperatorPackage(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-asset-operator-package-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestOperatorPackage.updateOperatorPackage(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-asset-operator-package-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }

      /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if (WebConfig.userSession.hasPermission(Constants.Permission.OperatorPackage)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M116")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch(button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);

                        this.webRequestOperatorPackage.deleteOperatorPackage(this.operatorId).done(() => {
                            this.isBusy(false);

                            this.publishMessage("bo-asset-operator-package-changed", this.operatorId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);

                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }
    
    
    /**
     * 
     * @memberOf OperatorPackageManageScrren
     */
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(OperatorPackageManageScrren),
    template: templateMarkup
};
