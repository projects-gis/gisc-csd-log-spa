﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";


class OperatorPackageListScrren extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Assets_OperatorPackages")());
        this.bladeSize = BladeSize.Large;
        
        this.operatorList = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.selectedOperator = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "asc" ]]);

        this.dateFormat = WebConfig.companySettings.shortDateFormat;


        this._selectingRowHandler = (operator) => {
            if(operator && WebConfig.userSession.hasPermission(Constants.Permission.OperatorPackage)) {
                return this.navigate("bo-ticket-manage-package-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: operator.id });
            }
            return false;
        };


        this.subscribeMessage("bo-asset-operator-package-changed", (operatorId) => {
            // Refresh entire datasource
            this.isBusy(true);
             this.webRequestOperatorPackage.listOperatorPackage().done((response)=> {
               var operator =  response["items"];
    
                this.operatorList(operator);
                this.recentChangedRowIds.replaceAll([operatorId]);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });
    }

    /**
     * Get WebRequest specific for Operator Package in Tracking Web API access.
     * @readonly
     */
    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }

     /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
      
        this.webRequestOperatorPackage.listOperatorPackage().done((response)=> {
            var operator =  response["items"];
            
           this.operatorList(operator);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

     /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.OperatorPackage)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

     /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-ticket-manage-package-manage", { mode: Screen.SCREEN_MODE_CREATE });
            this.selectedOperator(null);
            this.recentChangedRowIds.removeAll();
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedOperator(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(OperatorPackageListScrren),
    template: templateMarkup
};
