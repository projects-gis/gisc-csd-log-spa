﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import webRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import webRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import UIConstants from "../../../../../app/frameworks/constant/uiConstant";
import WebRequestSimStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestSimStock";

class SimManageManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        this.mode = this.ensureNonObservable(params.mode);
        this.SimId = this.ensureNonObservable(params.SimId);
        this.actDate = this.ensureNonObservable(params.actDate);
        this.id = this.ensureNonObservable(params.id, 0);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Ticket_CreateSim")());
            this.startDate = ko.observable(new Date());
        }
        else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Ticket_UpdateSim")());
            this.startDate = ko.observable(this.actDate);
        }

        this.OperatorList = ko.observableArray([]);
        this.statusList = ko.observableArray();
        this.enableDataSource = ScreenHelper.createYesNoObservableArray();
        this.selectedOperator = ko.observable();
        this.selectedStatus = ko.observable();
        this.txtMoblieNo = ko.observable("66");
        this.txtRemark = ko.observable();
        this.txtFreeDuration = ko.observable();
        this.iccId = ko.observable();

        //datetime
        this.checkOperator = true;
        this.checkFreeDuration = true;
        this.checkDateTime = true;
        this.checkMoblieNo = true;
        this.disabled = ko.observable();
        this.SelectcheckStatus = ko.observable();

        this.simType = ko.observableArray([]);
        this.selectedSimType = ko.observable();

        this.firstLoadSimStock = [];
        this.simStockInfos = ko.observableArray([]);
        this.selectedSimStock = ko.observable();

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            var dfd = $.Deferred();
            this.status = ko.observable();
            var d1 = this.webRequestTicket.getSimManagement(this.SimId);
            var d2 = this.webRequestOperatorPackage.listOperatorPackage();
            var d3 = this.webRequestSimStock.listSimStockSummary({});

            $.when(d1, d2, d3).done((sim, response, simStock) => {
                this.OperatorList(response.items);
                this.simStockInfos(simStock["items"]);
                this.firstLoadSimStock = simStock["items"];
                if (sim) {
                    this.txtMoblieNo(sim["mobileNo"]);
                    this.txtFreeDuration(sim["freeDuration"]);
                    this.txtRemark(sim["remark"]);
                    var OparatorObj = ScreenHelper.findOptionByProperty(this.OperatorList, "id", sim.operatorId);
                    this.status(sim["status"]);
                    this.selectedOperator(OparatorObj);
                    this.selectedSimStock(ScreenHelper.findOptionByProperty(this.simStockInfos, "id", sim.simStockId));
                    this.iccId(sim["iccId"]);
                }
            });
            this.checkOperator = ko.pureComputed(() => {
                if (this.status() == Enums.ModelData.SimStatus.New) {
                    return true;
                }
                else {
                    return _.isEmpty(params);
                }
            });

            this.checkFreeDuration = ko.pureComputed(() => {
                if (this.status() == Enums.ModelData.SimStatus.New || this.status() == Enums.ModelData.SimStatus.Active) {
                    return true;
                }
                else {
                    return _.isEmpty(params);
                }
            });

            this.checkDateTime = ko.pureComputed(() => {
                if (this.status() == Enums.ModelData.SimStatus.New || this.status() == Enums.ModelData.SimStatus.Active) {
                    return true;
                }
                else {
                    return _.isEmpty(params);
                }
            });

            this.checkMoblieNo = ko.pureComputed(() => {
                if (this.status() == Enums.ModelData.SimStatus.New) {
                    return true;
                }
                else {
                    return _.isEmpty(params);
                }
            })

            $.when(dfd).done(() => {
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
        }

    }

    get webRequestEnumResource() {
        return webRequestEnumResource.getInstance();
    }
    get webRequestOperatorPackage() {
        return webRequestOperatorPackage.getInstance();
    }
    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }
    get webRequestSimStock(){
        return WebRequestSimStock.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.disabled = ko.pureComputed(() => {
            this.result = this.selectedStatus() && this.selectedStatus().value === Enums.ModelData.SimStatus.Active;
            if(this.result){
                this.simStockInfos(this.firstLoadSimStock);
            }
            else{
                this.selectedSimStock(null);
                this.simStockInfos([]);
            }
            
            return this.result;
        });

        var StatusNew = {
            types: [Enums.ModelData.EnumResourceType.SimStatus],
            values: [
                Enums.ModelData.SimStatus.New,
                Enums.ModelData.SimStatus.Active,
            ]
        };
        var StatusInuse = {
            types: [Enums.ModelData.EnumResourceType.SimStatus],
            values: [Enums.ModelData.SimStatus.InUse,
            Enums.ModelData.SimStatus.Pause,
            ]
        };
        var StatusActive = {
            types: [Enums.ModelData.EnumResourceType.SimStatus],
            values: [Enums.ModelData.SimStatus.Active,
            Enums.ModelData.SimStatus.PrepareToClose,
            Enums.ModelData.SimStatus.Close,
            ]
        };
        var StatusPrepare = {
            types: [Enums.ModelData.EnumResourceType.SimStatus],
            values: [Enums.ModelData.SimStatus.PrepareToClose,
            Enums.ModelData.SimStatus.Close,
            ]
        };
        var StatusPause = {
            types: [Enums.ModelData.EnumResourceType.SimStatus],
            values: [Enums.ModelData.SimStatus.Pause,
            Enums.ModelData.SimStatus.PrepareToClose,
            Enums.ModelData.SimStatus.InUse,
            ]
        };
        this.simType(UIConstants.SimType);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                var a1 = this.webRequestEnumResource.listEnumResource(StatusNew);
                var a2 = this.webRequestOperatorPackage.listOperatorPackage();
                var a3 = this.webRequestSimStock.listSimStockSummary({});
                $.when(a1, a2, a3).done((r1, r2, r3) => {
                    this.statusList(r1["items"]);
                    this.OperatorList(r2["items"]);
                    this.simStockInfos(r3["items"]);
                    this.firstLoadSimStock = r3["items"];
                });
                break;

            case Screen.SCREEN_MODE_UPDATE:
                this.status = ko.observable();
                var b1 = this.webRequestTicket.getSimManagement(this.SimId);
                var b2 = this.webRequestEnumResource.listEnumResource(StatusNew);
                var b3 = this.webRequestEnumResource.listEnumResource(StatusInuse);
                var b4 = this.webRequestEnumResource.listEnumResource(StatusPrepare);
                var b5 = this.webRequestEnumResource.listEnumResource(StatusPause);
                var b6 = this.webRequestEnumResource.listEnumResource(StatusActive);
                $.when(b1, b2, b3, b4, b5, b6).done((r1, r2, r3, r4, r5, r6) => {
                    this.status(r1["status"]);
                    if (this.status() == Enums.ModelData.SimStatus.New) {
                        this.statusList(r2["items"]);
                    }
                    if (this.status() == Enums.ModelData.SimStatus.Active) {
                        this.statusList(r6["items"]);
                    }
                    if (this.status() == Enums.ModelData.SimStatus.InUse) {
                        this.statusList(r3["items"]);
                    }
                    if (this.status() == Enums.ModelData.SimStatus.Pause) {
                        this.statusList(r5["items"]);
                        var statusObj = ScreenHelper.findOptionByProperty(this.statusList, "displayName", r1["statusDisplayName"]);
                        this.selectedStatus(statusObj);
                    }
                    if (this.status() == Enums.ModelData.SimStatus.PrepareToClose) {
                        this.statusList(r4["items"]);
                    }

                    this.selectedSimType(ScreenHelper.findOptionByProperty(this.simType, "id", r1["simType"]));

                });
                break;
        }
    }

    generateModel() {
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            var model = {
                status: this.selectedStatus().value,
                mobileNo: this.txtMoblieNo(),
                operatorId: this.selectedOperator().id,
                freeDuration: this.txtFreeDuration(),
                activateDate: this.startDate(),
                remark: this.txtRemark ? this.txtRemark() : null,
                simStockId: this.selectedSimStock() ? this.selectedSimStock().id : null,
                iccId: this.iccId(),
            };
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            var model = {
                id: this.SimId,
                status: this.selectedStatus().value,
                mobileNo: this.txtMoblieNo(),
                operatorId: this.selectedOperator().id,
                freeDuration: this.txtFreeDuration(),
                activateDate: this.startDate(),
                remark: this.txtRemark ? this.txtRemark() : null,
                simStockId: this.selectedSimStock() ? this.selectedSimStock().id : null,                
                iccId: this.iccId(),
            };
        }
        model.simType = this.selectedSimType().id;
        return model;

    }
    setupExtend() {
        var self = this;

        this.txtMoblieNo.extend({ trackChange: true });
        this.txtRemark.extend({ trackChange: true });
        this.startDate.extend({ trackChange: true });
        this.txtFreeDuration.extend({ trackChange: true });
        
        this.selectedOperator.extend({ required: true });
        this.selectedStatus.extend({ required: true });
        this.txtMoblieNo.extend({ required: true });
        this.startDate.extend({
            required: {
                onlyIf: () => {
                    return self.selectedStatus() && self.selectedStatus().value == Enums.ModelData.SimStatus.Active;
                }
            }
        });

        this.txtMoblieNo.extend({
            required: true,
            validation: {
                validator: (val) => {
                    var a1 = this.txtMoblieNo().toString().length;
                    var a2 = this.txtMoblieNo().toString().substring(0, 2);
                    var obj = a1 == 11 && a2 == 66;
                    return obj;
                },
                message: this.i18n("M175")()
            }
        });

        this.iccId.extend({
            serverValidate: {
                params: "ICCId",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            selectedStatus: this.selectedStatus,
            selectedOperator: this.selectedOperator,
            txtMobileNo: this.txtMoblieNo,
            startDate: this.startDate,
        });
    }


    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.isBusy(true);
                    this.webRequestTicket.createSimManagement(model).done((response) => {
                        this.publishMessage("bo-ticket-sim-management-changed");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.isBusy(true);
                    this.webRequestTicket.updateSimManagement(model).done((response) => {
                        this.publishMessage("bo-ticket-sim-management-changed", this.SimId);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }
}
export default {
    viewModel: ScreenBase.createFactory(SimManageManageScreen),
    template: templateMarkup
};