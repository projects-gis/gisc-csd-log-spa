﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import UIConstants from "../../../../../app/frameworks/constant/uiConstant";
class SimManageViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Ticket_SimView")());
        this.bladeSize = BladeSize.Small;

        this.simId = this.ensureNonObservable(params.simId, -1);
        this.id = this.ensureNonObservable(params.simId);
        this.actDate = this.ensureNonObservable(params.actDate);
        this.operator = ko.observable();
        this.status = ko.observable();
        this.mobileNo = ko.observable();
        this.remark = ko.observable();
        this.freeDuration = ko.observable();
        this.createDate = ko.observable();
        this.createBy = ko.observable();
        this.activateDate = ko.observable();
        this.activateBy = ko.observable();
        this.inUserDate = ko.observable();
        this.inUserBy = ko.observable();
        this.ptcDate = ko.observable();
        this.ptcBy = ko.observable();
        this.closeDate = ko.observable();
        this.closeBy = ko.observable();
        this.simType = ko.observable();
        this.simStock = ko.observable();
        this.iccId = ko.observable();

        this.subscribeMessage("bo-ticket-sim-management-changed",(data) => { 
            if(data)
            {
                this.close(true);
            }
        });
    }
     
    get webRequestTicket() {
        return webRequestTicket.getInstance();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        this.webRequestTicket.getSimManagement(this.id).done((result) => {
            if (result) {
                this.status(result["statusDisplayName"]);
                this.mobileNo(result["mobileNo"]);
                this.operator(result["operatorName"]);
                this.freeDuration(result["freeDuration"]);
                this.remark(result["remark"]);
                this.createDate(result["formatCreateDate"]);
                this.createBy(result["createBy"]);
                this.activateDate(result["formatActivateDate"]);
                this.activateBy(result["activateBy"]);
                this.inUserDate(result["formatInUseDate"]);
                this.inUserBy(result["inUseBy"]);
                this.ptcDate(result["formatPreCloseDate"]);
                this.ptcBy(result["preCloseBy"]);
                this.closeDate(result["formatCloseDate"]);
                this.closeBy(result["closeBy"]);
                this.iccId(result["iccId"]);

                let simType = null;
                if(result["simType"]){
                    simType = UIConstants.SimType[result["simType"] - 1].name;
                }
                this.simType(simType);
                this.simStock(result["simStockName"]);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
       
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        var enable = (this.status() == "Close") ? false : true ;
        var enableEdit = (this.status() == "In Use" || this.status() == "Prepare To Close" || this.status() == "Close" || this.status() == "Pause") ?  false: true ;
        commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit",enable ));
        commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete", enableEdit ));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("bo-ticket-manage-sim-manage",{ mode: 'update', SimId: this.id, actDate: this.actDate });
                break;

            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M163")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestTicket.deleteSimManagement(this.id).done(() => {
                                this.publishMessage("bo-ticket-sim-management-changed");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                            }).always(()=>{
                                this.isBusy(false);
                            });
                            break;
                    }
                });
        }
    }

    
}

export default {
viewModel: ScreenBase.createFactory(SimManageViewScreen),
    template: templateMarkup
};