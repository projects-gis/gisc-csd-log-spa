﻿import ko from "knockout";
import templateMarkup from "text!./asset-create.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";

class TicketCreateScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Create")());
        this.itemCreateTickets = (Object.keys(params.items).length > 0) ? params.items : []; 
        this.returnFilter = (Object.keys(params.filter).length > 0) ? params.filter : {noSignalDuration:30};
        this.serviceTypeOptions = ko.observableArray([]); 
        this.AssignOptions = ko.observableArray([]);
        this.items = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this.selectedServiceType = ko.observable();
        this.selectedAssign = ko.observable();
        this.ticketRemark = ko.observable();
        this.userOwner = ko.observable(WebConfig.userSession.fullname); 

        this.isQC = ko.observable(false);
        this.isChangeBoxModel = ko.observable(false);        
        this.isMoveDefaultDriver = ko.observable(false);

        this.moveDefaultDriverVisible = ko.pureComputed(() => {
            let serviceType = this.selectedServiceType();
            if(serviceType){
                let serviceTypeName = serviceType.name == 'Relocate' ? true : false;
                return serviceTypeName;
            }
        });
    }
    
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * create ticket.
     * @readonly
     */
    get webRequestTicket() {
        return WebRequestTicket.getInstance();
    }

    


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        this.items(this.itemCreateTickets);



        // var serviceTypeFilter = {
        //     types: [Enums.ModelData.EnumResourceType.ServiceType],
        //     values: [],
        //     sortingColumns: DefaultSorting.EnumResource
        // };

        var userFilter = {
            groupIds:[Enums.UserGroup.Administrator,
                      Enums.UserGroup.SystemAdmin
            ],
            sortingColumns: DefaultSorting.User,
            enable:true
        };


        var d1 = this.webRequestTicket.listServiceTypeSummary();
        var d2 = this.webRequestUser.listUserSummary(userFilter);


        $.when(d1, d2).done((r1, r2) => {
            this.serviceTypeOptions(r1.items);
            // if fullname is null replace fullname is username
            r2.items.forEach(function(v){
                v.displayName = (v.fullName == null || v.fullName.trim() == "" ) ? v.username : v.fullName
            });
            this.AssignOptions(r2.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

        var enable = (Object.keys(this.items()).length > 0) ? true : false;
        actions.push(this.createAction("actSave", this.i18n("Common_Save")(), "default", true, enable));
        actions.push(this.createActionCancel());

    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id === "actSave") {
            this.isBusy(true);     
            var modelTicketInsertDetails = [];
            var selectedServiceTypeID = (typeof this.selectedServiceType() == 'undefined') ? null : this.selectedServiceType().id; 
            var selectedAssignID = (typeof this.selectedAssign() == 'undefined') ? null : this.selectedAssign().id;

            this.items().forEach(function(v){
                modelTicketInsertDetails.push({
                    companyId: v.companyId,
                    businessUnitId: v.businessUnitId,
                    vehicleId: v.licenseId,
                    boxId: v.boxId,
                    boxTemplateId: v.templateId
                });
            });
            
            
            var modelCreateTicket = {
                serviceType: selectedServiceTypeID,
                assignToId: selectedAssignID,
                remark: this.ticketRemark(),
                ticketInsertDetails: modelTicketInsertDetails,
                isQC: this.isQC(),
                isReplaceBoxWithOtherModel: this.isChangeBoxModel(),
                IsMoveDefaultDriver: this.moveDefaultDriverVisible() == true ? this.isMoveDefaultDriver() : null
            };

            this.webRequestTicket.createTicket(modelCreateTicket).done((response) => {
                this.close(true);

                this.publishMessage("add-items-asset",this.returnFilter);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });

        }
        
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketCreateScreen),
    template: templateMarkup
};