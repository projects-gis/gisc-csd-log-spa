﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import {Constants, Enums, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";

class ConfigurationPoiIconListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_POIIcons")());
        this.bladeSize = BladeSize.Medium;

        this.poiIcons = ko.observableArray();

        this.filterText = ko.observable();
        this.selectedIcon = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 1, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (icon) => {
            if (icon) {
                return this.navigate("bo-configuration-poi-icon-manage", {
                    mode: "update",
                    iconId: icon.id
                });
            }
            return false;
        };
        
        this.subscribeMessage("bo-configuration-poi-iccon-change", (iconId) => {
            // debugger
            this.isBusy(true);
            var filter = {includeAssociationNames:[EntityAssociation.PoiIcon.Image]};
            this.webRequestPoiIcon.listPoiIconSummary(filter).done((r)=> {
                 this.poiIcons.replaceAll(r["items"]);
                 this.recentChangedRowIds.replaceAll([iconId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.isBusy(false);
            });
        });

    }

    /**
     * Get WebRequest specific for Feature module in Tracking Web API access.
     * @readonly
     */
    get webRequestPoiIcon() {
        return WebRequestPoiIcon.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
        var filter = {includeAssociationNames:[EntityAssociation.PoiIcon.Image]};
        this.webRequestPoiIcon.listPoiIconSummary(filter).done((response) => {
            var poiIcons = response["items"];
            this.poiIcons(poiIcons);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}


    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIcon(null);
    }

    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-configuration-poi-icon-manage", {mode: "create"});
            this.selectedIcon(null);
        }
    }


    renderIconColumn(data, type, row) {
        if (data) {
            let node = '<img src="' + data +'"/>';
            return node;
        }
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationPoiIconListScreen),
    template: templateMarkup
};