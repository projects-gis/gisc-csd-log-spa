﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";

const width = 32;
const height = 32;

class ConfigurationPoiIconManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_CreatePOIIcon")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Configurations_UpdatePOIIcon")());
                break;
        }

        this.id = this.ensureNonObservable(params.iconId, 0);
        this.name = ko.observable("");
        this.offsetX = ko.observable("");
        this.offsetY = ko.observable("");

        this.icon = ko.observable();
        this.iconId = ko.observable();
        this.iconName = ko.observable();
        this.iconSelectedFile = ko.observable();

        this.iconUrl = ko.pureComputed(() => {
            if (this.icon() && this.icon().infoStatus !== Enums.InfoStatus.Delete) {
                return this.icon().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        this.isUpload = ko.pureComputed(() => {
            return this.icon() && this.icon().infoStatus !== Enums.InfoStatus.Delete;
        });
    }

    get webRequestPoiIcon() {
        return WebRequestPoiIcon.getInstance();
    }

    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload(isValidToSubmit) {
        if(!isValidToSubmit){
            this.icon(null);
            this.iconName(null);
        }
    }

    
    /**
     * To remove upload icon.
     * 
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onRemoveFile(){
        this.icon(null);
    }

    /**
     * Do something when fileupload fail.
     * 
     * @param {any} e
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadFail(e) {
        this.handleError(e);
    }

    
    /**
     * Do something when fileupload success.
     * 
     * @param {any} data
     * 
     * @memberOf ConfigurationPoiIconManageScreen
     */
    onUploadSuccess(data) {
        if (data && data.length > 0) {
            var newIcon = data[0];

            newIcon.infoStatus = this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update;
            newIcon.id =  this.iconId() || 0;
            this.icon(newIcon);
        }
    }

    /**
     * 
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        // var dfd = $.Deferred();
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                dfd.resolve();
                break;
            case Screen.SCREEN_MODE_UPDATE:
                //Get Service here.

                this.webRequestPoiIcon.getPoiIcon(this.id, [EntityAssociation.PoiIcon.Image]).done((poiIcon) => {
                    if (poiIcon) {

                        this.name(poiIcon.name);
                        this.icon(poiIcon.image);
                        this.iconId(poiIcon.image.id);
                        this.iconName(poiIcon.image.fileName);
                        this.offsetX(poiIcon.offsetX || "");
                        this.offsetY(poiIcon.offsetY || "");
                        var file = new FileUploadModel();
                        file.name = poiIcon.image.fileName;
                        file.extension = "png";
                        file.imageWidth = poiIcon.width;
                        file.imageHeight = poiIcon.height;
                        this.iconSelectedFile(file);
                    }
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

                break;
        }
        return dfd;
    }

    setupExtend() {

        //trackChange
        this.icon.extend({
            trackChange: true
        });
        this.name.extend({
            trackChange: true
        });
        this.offsetX.extend({
            trackChange: true
        });
        this.offsetY.extend({
            trackChange: true
        });

        //validation
        this.iconSelectedFile.extend({
            fileRequired: true,
            fileExtension: ['png'],
            fileSize: 4,
            fileImageDimension: [width, height, 'equal']
        });

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            iconSelectedFile:this.iconSelectedFile,
            name: this.name
        });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        }

    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            name: this.name(),
            image: this.icon(),
            offsetX: this.offsetX(),
            offsetY: this.offsetY(),
            width: width,
            height: height,
            companyId: this.companyId,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.id,
        };

        return model;
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);


        if (sender.id == "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
        
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestPoiIcon.createPoiIcon(model, true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-poi-iccon-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestPoiIcon.updatePoiIcon(model, true).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("bo-configuration-poi-iccon-change", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
            }
        }
    }

    buildCommandBar(commands) {
        if (this.mode === "update") {
            if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_BackOffice)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        }
    }

    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M121")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestPoiIcon.deletePoiIcon(this.id).done(() => {
                            this.isBusy(false);
                            this.publishMessage("bo-configuration-poi-iccon-change", this.id);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);
                            var errorObj = e.responseJSON;
                            if (errorObj) {
                                this.displaySubmitError(errorObj);
                            }
                        });
                        break;
                }
            });
        }
    }

    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationPoiIconManageScreen),
    template: templateMarkup
};