﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import WebRequestSystemConfiguration from "../../../../../app/frameworks/data/apicore/webRequestSystemConfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";

class SystemSettingManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_UpdateSetting")());

        this.systemSettingId = this.ensureNonObservable(params.systemSettingId, -1);

        // This mode is parse from paras, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);

         // These properties will be populated in onLoad
        this.name = ko.observable('');
        this.value = ko.observable('');
    }
     /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup track change.
        this.name.extend({
            trackChange: true
        });
        this.value.extend({
            trackChange: true
        });

        // Manual setup validation rules
        this.name.extend({
            required: true
        });
        this.value.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            value: this.value,
        });
    }

    /**
     * Get WebRequest specific for System Configuration module in Web API access.
     * @readonly
     */
    get webRequestSystemConfiguration() {
        return WebRequestSystemConfiguration.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
        this.webRequestSystemConfiguration.getSystemConfiguration(this.systemSettingId).done((systemSetting)=> {
            if(systemSetting){
                this.name(systemSetting.name);
                this.value(systemSetting.value);
            }
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
     /**
     * Generate System Configuration model from View Model
     * 
     * @returns System Configuration model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.systemSettingId,
            name: this.name(),
            value: this.value(),
            type: Constants.SystemConfigurationType.General
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            this.webRequestSystemConfiguration.updateSystemConfiguration(model).done((response) => {
                this.isBusy(false);

                this.publishMessage("bo-configuration-system-setting-changed", this.systemSettingId);
                this.close(true);
            }).fail((e)=> {
                this.isBusy(false);

                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(SystemSettingManageScreen),
    template: templateMarkup
};