﻿import ko from "knockout";
import templateMarkup from "text!./language-add.html";
import ScreenBase from "../../../screenbase";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";

class TranslationLanguageAddScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_AddLanguage")());
        
        this.selectedLanguage = ko.observable();
        this.languages = ko.observableArray([]);
    }
    /**
     * Get WebRequest specific for Language module in Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestLanguage.getAllLanguages().done((r)=> {
           this.languages(r);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
      /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup track change.
        this.selectedLanguage.extend({
            trackChange: true
        });

        // Manual setup validation rules
        this.selectedLanguage.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            selectedLanguage: this.selectedLanguage
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate language model from View Model
     * 
     * @returns language model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            code: this.selectedLanguage().code,
            name: this.selectedLanguage().name,
            enable: false
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
             if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.publishMessage("bo-language-added", this.generateModel());
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TranslationLanguageAddScreen),
    template: templateMarkup
};