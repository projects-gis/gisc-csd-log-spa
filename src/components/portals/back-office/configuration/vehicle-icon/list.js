﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestVehicleIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleIcon";
import {Constants, Enums, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";


class ConfigurationVehicleIconListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_VehicleIcons")());
        this.bladeSize = BladeSize.Medium;

        this.vehicleIcon = ko.observableArray();

        this.filterText = ko.observable();
        this.selectedIcon = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 1, "asc" ]]);
        
         // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (vehicleIcon) => {
            if(vehicleIcon) {
                return this.navigate("bo-configuration-vehicle-icon-manage", {
                    mode: "update",
                    vehicleIconId: vehicleIcon.id
                });
            }
            return false;
        };

        this.subscribeMessage("bo-configuration-vehicle-iccon-change", (vehicleIconId) => {
            // debugger
            this.isBusy(true);
            var filter = {includeAssociationNames:[EntityAssociation.VehicleIcon.PreviewImage]};
            this.webRequestVehicleIcon.listVehicleIconSummary(filter).done((r)=> {
                 this.vehicleIcon.replaceAll(r["items"]);
                 this.recentChangedRowIds.replaceAll([vehicleIconId]);
                 this.isBusy(false);
            }).fail((e)=> {
                this.isBusy(false);
            });
        });


        this.renderIconColumn = (data, type, row) => {
            let node = "";
            if(data) {
                node = '<img src="' + data.fileUrl +'"/>';
            }
            return node;
        };
    }

    get webRequestVehicleIcon() {
        return WebRequestVehicleIcon.getInstance();
    }

    onLoad(isFirstLoad){
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var filter = {includeAssociationNames:[EntityAssociation.VehicleIcon.PreviewImage]};
        this.webRequestVehicleIcon.listVehicleIconSummary(filter).done((response) => {
            var vehicleIcon = response["items"];
            // debugger
            this.vehicleIcon(vehicleIcon);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;

    }

      /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIcon(null);
    }

    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.VehicleIconsManagement)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("bo-configuration-vehicle-icon-manage", {
                mode: "create"
            });
            this.selectedIcon(null);
        }
    }

    onUnload() {
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationVehicleIconListScreen),
    template: templateMarkup
};