﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDatabaseConnection from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";
import WebRequestDatabaseServer from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseServer";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/WebRequestCompany";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/WebRequestBusinessUnit";
import { Constants, Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestMailConfiguration from "../../../../../../app/frameworks/data/apicore/WebRequestMailConfiguration";
import WebRequestSqlTemplate from "../../../../../../app/frameworks/data/apicore/webRequestSqlTemplate";



/**
 * 
 * 
 * @class TrackingDatabasesManageScreen
 * @extends {ScreenBase}
 */
class MailConfigManageScreen extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.mailconfigId);
        this.compayIdfromlist  =  this.ensureNonObservable(params.companyId);
        this.buIdfromlist  = this.ensureNonObservable(params.buId);
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("AutoMail_CreateMailConfig")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("AutoMail_UpdateMailConfig")());
                break;
        }
        this.mailconfigId = this.ensureNonObservable(params.mailconfigId, -1);
        this.testCheck = ko.observable();
        this.trackingDBId = this.ensureNonObservable(params.trackingDBId, -1);
        this.ListCompany = ko.observableArray([]);
        //this.sqlTempName = ko.observable("");
        //this.sqlStatementName = ko.observable("");
        this.version = ko.observable(1);
        this.title = ko.observable("");
        this.companyId = ko.observable();
        this.buId = ko.observable();
        this.companyname = ko.observable();
        this.Buname = ko.observable();
        this.selectedcompany = ko.observable();
        this.status = ko.observableArray([
            { status: 'Enable' },
            { status: 'Disable' }
        ]);
        this.BuList = ko.observableArray([]);
        this.selectedBU = ko.observable();
        this.isBusinessUnit = ko.observable(false);
        this.requestby = ko.observable("");
        this.selectedStatus = ko.observable();
        this.sqlTemplate = ko.observableArray([]);
        this.selectedsqltemplate = ko.observable();
        this.sqlstatement = ko.observable("");
        this.mailsubject = ko.observable("");
        this.mailto = ko.observable("");
        this.mailbody = ko.observable("");
        this.selectedSchedule = ko.observable();
        this.selectedConfigType = ko.observable();

         this.radiodate = ko.observable();
        this.Schedule = ko.observableArray([
            { frequency: "Daily" },
            { frequency: "Weekly" },
            { frequency: "Monthly" },
        ]);
        this.ConfigType = ko.observableArray([
            { Type: "SQLStatement" },
            { Type: "WebService" }
        ]);
        this.MethodWS = ko.observableArray([
            { method: "GET" },
            { method: "POST" }

        ]);

        this.isValueOcat = ko.observable("Occurat");
        this.isValueOcever = ko.observable("Occurevery");
        this.RadEnddate = ko.observable("Enddate");
        //this.RadNoEnddate = ko.observable("4");
        this.RadMon = ko.observable("Monday");
        this.RadTue = ko.observable("Tuesday");
        this.RadWed = ko.observable("Wednesday");
        this.RadThu = ko.observable("Thursday");
        this.RadFri = ko.observable("Friday");
        this.RadSat = ko.observable("Saturday");
        this.RadSun = ko.observable("Sunday");
        this.RadNoEnddate = ko.observable("NoEnddate");
        this.isRadio = ko.observable();
        this.isRadioWeek = ko.observable("Occurat");
        this.isRadioMonth = ko.observable("Occurat");
        this.isRadioDate = ko.observable("NoEnddate");
        //this.isRadioDate();
        this.selectedSchedule = ko.observable("Daily");
        this.TimeAt = ko.observable("01:00");
        this.TimeEvery = ko.observable();
        this.ValueDay = ko.observable("Monday");
        this.AuthenURL = ko.observable();
        this.AuthenRawData = ko.observable();
        this.WSurl = ko.observable();
        this.selectedmethodWS = ko.observable();
        this.WSRawData = ko.observable();
        this.ReferenceUrl = ko.observable();
        this.enableOccurevery = ko.observable();
        this.enableOccurat=ko.observable();
        this.enableEndDate = ko.observable();
        this.isVisibleEndDate = ko.observable();
        this.isVisibleNoEndDate = ko.observable();
        this.isVisibleOccurevery = ko.observable();
        this.isVisibleOccurat = ko.observable();
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        this.startDate = ko.observable(new Date());
        this.endDate = ko.observable(new Date());
        this.maxDateStart = ko.observable(new Date());
        this.minDate = ko.observable(0);
        this.noEndDate = ko.observable();
        this.TimeEvery1=ko.observable();
        this.TimeAt1 = ko.observable();

        this.DailyVisible = ko.pureComputed(() => {
            this.resultDaily = this.selectedSchedule() && this.selectedSchedule().frequency === "Daily";
            return this.resultDaily;
        });
        this.WeeklyVisible = ko.pureComputed(() => {
            this.resultWeekly = this.selectedSchedule() && this.selectedSchedule().frequency === "Weekly";
            return this.resultWeekly;
        });
        this.MonthlyVisible = ko.pureComputed(() => {
            this.resultMonthly = this.selectedSchedule() && this.selectedSchedule().frequency === "Monthly";
            return this.resultMonthly;
        });

        this.SQLVisible = ko.pureComputed(() => {
            this.resultConfigTypeSQL = this.selectedConfigType() && this.selectedConfigType().Type === "SQLStatement";
            return this.resultConfigTypeSQL;
        });
        this.WSVisible = ko.pureComputed(() => {
            this.resultConfigTypeWS = this.selectedConfigType() && this.selectedConfigType().Type === "WebService";
            return this.resultConfigTypeWS;
        });
       
        this.IsWebMethod = ko.pureComputed(() => {
            return this.selectedmethodWS() && this.selectedmethodWS().method == "POST";
       });

        if(this.mode === Screen.SCREEN_MODE_CREATE)
        {
            this.selectedcompany.subscribe(val => {
                let id = (val)?val.id:null;
                this.WebRequestBusinessUnit.listBusinessUnitSummary({ companyId: id }).done((response) => {
                    //Call API BU
                    if(response.items.length > 0){
                        this.BuList(response.items);
                    }
                    else{
                        this.BuList([]);
                    }
                }); 
            });

        }
        
        this.disabled = this.mode === Screen.SCREEN_MODE_UPDATE ? false : true;
        this.visibleCompanies = this.mode === Screen.SCREEN_MODE_UPDATE ? true : false;
        this.checkaction = this.mode === Screen.SCREEN_MODE_CREATE ? true : false;
        this.captiontext = this.mode === Screen.SCREEN_MODE_CREATE ? '': null;

        this.isopen = ko.pureComputed(function () {
            return true;
        }, this);

        //this.enableEndDate = ko.pureComputed(() => {
        //    this.isRadioDateCheck = this.isRadioDate() != "NoEnddate";
        //    return this.isRadioDateCheck;
        //});
        
       
        this.isRadio.subscribe(val =>{
            if(val == "Occurat"){
                let checkTimeAt = (this.TimeAt() != null)?this.TimeAt():null;
                this.enableOccurat(true);
                this.enableOccurevery(false);
                this.TimeAt(checkTimeAt);
                this.TimeEvery(null);
            }
            else if(val == "Occurevery"){
                let checkTimeEvery = (this.TimeEvery() != null)?this.TimeEvery():null;
                this.enableOccurevery(true);
                this.enableOccurat(false);
                this.TimeAt(null);
                this.TimeEvery(checkTimeEvery);
            }
            
        });
        this.isRadioDate.subscribe(val=>{
            if(val=="NoEnddate"){
                this.endDate(null);
                this.noEndDate(null);
                //this.enableEndDate(false);
            }
            else if(val=="Enddate"){
                this.endDate(null);
                this.noEndDate(null);
            }
        });
     
        this.isVisibleEndDate = ko.pureComputed(()=>{
            this.resultVisible = this.isRadioDate()&&this.isRadioDate() == "Enddate";
            return this.resultVisible;
        });

        this.isVisibleNoEndDate = ko.pureComputed(()=>{
            this.resultVisibleNoenddate = this.isRadioDate()&&this.isRadioDate() == "NoEnddate";
            return this.resultVisibleNoenddate;
        });

        this.isVisibleOccurat = ko.pureComputed(()=>{
            this.resultVisibleOccurat = this.isRadio()&&this.isRadio() == "Occurat";
            return this.resultVisibleOccurat;   
        });

        this.isVisibleOccurevery = ko.pureComputed(()=>{
            this.resultVisibleOccurevery = this.isRadio()&&this.isRadio() == "Occurevery";
            return this.resultVisibleOccurevery;   
        });
       
        this.selectedsqltemplate.subscribe(val=>{
            let sqlStatement = (val)?val.sqlStatement : null ;
                this.sqlstatement(sqlStatement);
        });
       
    }


    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){

        
        //Track Changed
        this.title.extend({
            trackChange: true
        });

        if(this.visibleCompanies){
            this.requestby.extend({trackChange: true});
            this.sqlstatement.extend({trackChange: true});
            this.mailsubject.extend({trackChange: true});
            this.mailbody.extend({trackChange: true});
            this.mailto.extend({trackChange: true});
            this.startDate.extend({trackChange: true});
            this.endDate.extend({trackChange: true});
            this.TimeAt.extend({trackChange: true});
            this.TimeEvery.extend({trackChange: true});  
            this.selectedSchedule.extend({trackChange:true});
        }
        else{
            this.requestby.extend({trackChange: true});
            this.sqlstatement.extend({trackChange: true});
            this.mailsubject.extend({trackChange: true});
            this.mailbody.extend({trackChange: true});
            this.mailto.extend({trackChange: true});
            this.startDate.extend({trackChange: true});
            this.endDate.extend({trackChange: true});
            this.TimeAt.extend({trackChange: true});
            this.TimeEvery.extend({trackChange: true});  
            this.selectedcompany.extend({trackChange: true});
        }

        this.mailto.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("M064")()
            },
            trackChange:true
        });

        this.title.extend({ 
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.selectedcompany.extend({ required: true });
        this.requestby.extend({ required: true });
        this.mailsubject.extend({ required: true });
        this.mailto.extend({ required: true });
        this.mailbody.extend({ required: true });
        this.startDate.extend({ required: true });



        this.validationModel = ko.validatedObservable({
            Title: this.title,
            companyId: this.selectedcompany,
            requestBy: this.requestby,
            mailSubject: this.mailsubject,
            mailTo: this.mailto,
            mailBody: this.mailbody,
            startDate:this.startDate,
        });


        this.validationModelUpdate = ko.validatedObservable({
            Title: this.title,
            requestBy: this.requestby,
            mailSubject: this.mailsubject,
            mailTo: this.mailto,
            mailBody: this.mailbody,
            startDate:this.startDate,
        });


        //validation

            this.sqlstatement.extend({ required:true});

            this.validationModelsql = ko.validatedObservable({
                sqlstatement:this.sqlstatement,
                companyId: this.selectedcompany,
            });

            var self = this;
            self.WSRawData.extend({
                required: {           
                    onlyIf: function () { 
                        return self.IsWebMethod(); 
                    }
                }
            });
            this.AuthenURL.extend({required:true});
            this.AuthenRawData.extend({required:true});
            this.WSurl.extend({required:true});
            this.selectedmethodWS.extend({required:true});

            this.validationModelWS = ko.validatedObservable({
                authenticationUrl: this.AuthenURL,
                authenticationRawData: this.AuthenRawData,
                webServiceUrl: this.WSurl,
                webMethod: this.selectedmethodWS,
                webServiceRawData : self.WSRawData
            });

            this.endDate.extend({required: true});
            this.validationModelEnddate = ko.validatedObservable({
                  endDate:this.endDate  
            });

            this.TimeAt.extend({required:true});
            this.validationModelOccurat = ko.validatedObservable({
                    TimeAt:this.TimeAt
            });
            this.TimeEvery.extend({required:true});
            this.validationModelOccurevery = ko.validatedObservable({
                 TimeEvery:this.TimeEvery
            });
           
    }

    /**
     * 
     * Get WebRequest for DatabaseConnection in Web API access.
     * @readonly
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    get webRequestDatabaseConnection() {
        return WebRequestDatabaseConnection.getInstance();
    }

    get WebRequestMailConfiguration() {
        return WebRequestMailConfiguration.getInstance();
    }

    get webRequestSqlTemplate() {
        return WebRequestSqlTemplate.getInstance();
    }
    /**
     * 
     * Get WebRequest for DatabaseServer in Web API access.
     * @readonly
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    get webRequestDatabaseServer() {
        return WebRequestDatabaseServer.getInstance();
    }
    get WebRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get WebRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */



    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();


        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.WebRequestCompany.listCompany({ ids: [] }).done((response) => {
                    this.ListCompany(response.items);
                    dfd.resolve();
                });
                var filter = { templateNames: ""};
                this.webRequestSqlTemplate.listSqlTemplateSummary(filter).done((response) => {
                    var sqlTemplates = response["items"];
                    this.sqlTemplate(sqlTemplates);
                });
                this.isRadio("Occurat");
                this.enableOccurat(true);
                this.isRadioDate("NoEnddate");
                this.endDate(this.noEndDate);
              
                break;
            case Screen.SCREEN_MODE_UPDATE:

                var getmailc =  this.WebRequestMailConfiguration.getMailConfiguration(this.id);
                var bu   =  this.WebRequestBusinessUnit.listBusinessUnitSummary({ companyId:this.compayIdfromlist });

               $.when(getmailc,bu).done((getmailconfig,bulist) => {
                    if (getmailconfig) {
                        this.BuList(bulist.items);
                        this.ListCompany({
                            id:getmailconfig.companyId,
                            name:getmailconfig.companyName
                        });
                        this.selectedBU((getmailconfig.businessUnitId)?getmailconfig.businessUnitId.toString():null);
                        this.version(getmailconfig.version+1);
                        this.companyId(getmailconfig.companyId);
                        this.buId(getmailconfig.businessUnitId);
                        this.title(getmailconfig.title);
                        this.companyname(getmailconfig.companyName);
                        this.Buname(getmailconfig.businessName);
                        this.isBusinessUnit(getmailconfig.includeSubBU);
                        this.requestby(getmailconfig.requestBy);
                        this.sqlstatement(getmailconfig.sqlStatement);
                        this.mailbody(getmailconfig.mailBody);
                        this.mailsubject(getmailconfig.mailSubject);
                        this.mailto(getmailconfig.mailTo);
                        this.startDate(getmailconfig.startDate);
                        this.AuthenRawData(getmailconfig.authenticationRawData);
                        this.AuthenURL(getmailconfig.authenticationUrl);
                        this.WSurl(getmailconfig.webServiceUrl);
                        this.WSRawData(getmailconfig.webServiceRawData);
                        this.endDate(getmailconfig.endDate);
                        this.TimeAt(getmailconfig.occursAt);
                        this.ValueDay(getmailconfig.weekName);
                        this.TimeEvery(getmailconfig.occursEvery);
                        this.ReferenceUrl(getmailconfig.referenceUrl);
                        
                        let weekname = (this.ValueDay()!=null)?getmailconfig.weekName:"Monday";
                        let RadioDate = (this.endDate()!= null)?"Enddate":"NoEnddate";
                        let RadioOccur = (this.TimeAt()!=null)?"Occurat":"Occurevery";
                        let Isstatus = ScreenHelper.findOptionByProperty(this.status,"status",getmailconfig.status==1?"Enable":"Disable");
                        let Schedule = ScreenHelper.findOptionByProperty(this.Schedule,"frequency",getmailconfig.scheduleFrequency);
                        let ConfigType = ScreenHelper.findOptionByProperty(this.ConfigType,"Type",getmailconfig.configurationType);
                        let Method = ScreenHelper.findOptionByProperty(this.MethodWS,"method",getmailconfig.webMethod);

                        this.selectedSchedule(Schedule);
                        this.selectedConfigType(ConfigType);
                        this.selectedmethodWS(Method);
                        this.isRadioDate(RadioDate);
                        this.isRadio(RadioOccur);
                        this.selectedStatus(Isstatus);
                        this.ValueDay(weekname);
                        this.endDate(getmailconfig.endDate);
                    }
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

                break;
        }
        return dfd;
    }

    /**
     * 
     * 
     * @param {any} actions
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildActionBar(actions) {

        

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
           
                actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                actions.push(this.createActionCancel());
        }
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                actions.push(this.createActionCancel());
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            if(this.mode===Screen.SCREEN_MODE_CREATE){
                if(!this.validationModel.isValid()){
                    this.validationModel.errors.showAllMessages();
                    return ;
                }
            }

            if(this.mode===Screen.SCREEN_MODE_UPDATE){
                if(!this.validationModelUpdate.isValid()){
                    this.validationModelUpdate.errors.showAllMessages();
                    return ;
                } 
            }

            if(this.resultConfigTypeSQL){
                if(!this.validationModelsql.isValid()){
                    this.validationModelsql.errors.showAllMessages();
                    return false;
                }
                
            }

            if(this.resultConfigTypeWS){
                if(!this.validationModelWS.isValid()){
                    this.validationModelWS.errors.showAllMessages();
                    return ;
                }
            }

            if(this.isRadioDate()=="Enddate"){
                if(!this.validationModelEnddate.isValid()){
                    this.validationModelEnddate.errors.showAllMessages();
                    return ;
                }
            }
        
            if(this.resultDaily)
            {
                if(this.isRadio()=="Occurat"){
                    if(!this.validationModelOccurat.isValid()){
                        this.validationModelOccurat.errors.showAllMessages();
                        return;
                    }
                }
                if(this.isRadio()=="Occurevery"){
                    if(!this.validationModelOccurevery.isValid()){
                        this.validationModelOccurevery.errors.showAllMessages();
                        return;
                    }
                }
            }
            if(this.resultWeekly || this.resultMonthly)
            {
                if(!this.validationModelOccurat.isValid()){
                    this.validationModelOccurat.errors.showAllMessages();
                    return;
                }
            }

            this.isBusy(true);
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    console.log(this.selectedBU());
                    var filter = {
                        title: this.title(),
                        version: this.version(),
                        companyId: this.selectedcompany().id,
                        businessUnitId:this.selectedBU(),
                        includeSubBU: this.isBusinessUnit(),
                        requestBy: this.requestby(),
                        status: this.selectedStatus().status=="Enable"?1:0,
                        configurationType: this.selectedConfigType().Type,
                        authenticationUrl: this.AuthenURL(),
                        authenticationRawData: this.AuthenRawData(),
                        webServiceUrl: this.WSurl(),
                        webMethod: this.selectedmethodWS().method,
                        webServiceRawData: this.WSRawData(),
                        sqlStatement: this.sqlstatement(),
                        mailSubject: this.mailsubject(),
                        mailTo: this.mailto(),
                        mailBody: this.mailbody(),
                        scheduleFrequency: this.selectedSchedule().frequency,
                        startDate: this.startDate(),
                        endDate: this.endDate(),
                        occursAt: this.TimeAt(),
                        occursEvery: this.TimeEvery(),
                        weekName: this.ValueDay(),
                        referenceUrl:this.ReferenceUrl(),
                        infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                        userId: WebConfig.userSession.id
                    };
                   
                    this.WebRequestMailConfiguration.createMailConfiguration(filter).done((response) => {
                        this.publishMessage("bo-automail-mailconfig-template-changed",response.id);
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;

                case Screen.SCREEN_MODE_UPDATE:

                    var filter_Update = {
                        Id:this.id,
                        title: this.title(),
                        version: this.version(),
                        companyId: this.companyId(),
                        businessUnitId:this.buId(),
                        includeSubBU: this.isBusinessUnit(),
                        requestBy: this.requestby(),
                        status: this.selectedStatus().status=="Enable"?1:0,
                        configurationType: this.selectedConfigType().Type,
                        authenticationUrl: this.AuthenURL(),
                        authenticationRawData: this.AuthenRawData(),
                        webServiceUrl: this.WSurl(),
                        webMethod: this.selectedmethodWS().method,
                        webServiceRawData: this.WSRawData(),
                        sqlStatement: this.sqlstatement(),
                        mailSubject: this.mailsubject(),
                        mailTo: this.mailto(),
                        mailBody: this.mailbody(),
                        scheduleFrequency: this.selectedSchedule().frequency,
                        startDate: this.startDate(),
                        endDate: this.endDate(),
                        occursAt: this.TimeAt(),
                        occursEvery: this.TimeEvery(),
                        weekName: this.ValueDay(),
                        referenceUrl:this.ReferenceUrl(),
                        infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                        userId: WebConfig.userSession.id
                    };
                    this.WebRequestMailConfiguration.updateMailConfiguration(filter_Update).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("bo-automail-mailconfig-template-changed",response.id);
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
            }
        }
       
    }
    onValidateButtonClick(){
        if(!this.validationModelsql.isValid()){
            this.validationModelsql.errors.showAllMessages();
            return ;
        }
        this.isBusy(true);
        var filter = {
            sqlStatement: this.sqlstatement(),
            companyId: this.selectedcompany().id,
            businessUnitId:this.selectedBU(),
            includeSubBU: this.isBusinessUnit()
        };
        this.WebRequestMailConfiguration.validateSqlStatement(filter).done((r)=> {

            if(r.isValid == true){
                this.showMessageBox(null, this.i18n("M158")(), BladeDialog.DIALOG_OK);
            }
            else{
                this.showMessageBox(null,r.errorMessage, BladeDialog.DIALOG_OK);
            }
            this.isBusy(false);
        });
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M160")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.WebRequestMailConfiguration.deleteMailConfiguration(this.mailconfigId).done(() => {
                            this.publishMessage("bo-automail-mailconfig-template-changed", this.sqlTempId);
                            this.close(true);
                        }).fail((e)=> {
                            this.handleError(e);
                        }).always(()=>{
                            this.isBusy(false);
                        });
                        break;
                }
            });
        }
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
  
    onUnload() {}
}

export default {
viewModel: ScreenBase.createFactory(MailConfigManageScreen),
    template: templateMarkup
};