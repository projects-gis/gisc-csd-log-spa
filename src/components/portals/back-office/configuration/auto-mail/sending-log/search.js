﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/WebRequestCompany";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/WebRequestBusinessUnit";
import WebRequestMailConfiguration from "../../../../../../app/frameworks/data/apicore/WebRequestMailConfiguration";

class SendingLogs extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_SearchSendingLogs")());
        this.bladeSize = BladeSize.Small;
        this.StuList = ko.observableArray([]);
        this.BuList = ko.observableArray([]);
        this.MailList = ko.observableArray([]);
        this.StatusList = ko.observableArray([]);
        this.selectedCompany = ko.observable();
        this.selectedBU = ko.observable();
        this.selectedMailConfig = ko.observable();
        this.selectedStatus = ko.observable();
        this.isIncludeSubBU = ko.observable(false);

        this.selectedCompany.subscribe(val => {
            let companyId = (typeof val == "undefined")? null : val.id;
            this.BuList([]);
            this.WebRequestBusinessUnit.listBusinessUnitSummary({ companyId: companyId }).done((response) => {
                this.BuList(response.items);
            });  //Call API BU
        });

        this.selectedMailConfigBinding = ko.pureComputed(() => {
                this.MailList([]);
                if(this.selectedBU() != null && this.selectedCompany() != null) {
                    this.WebRequestMailConfiguration.summarylistMailConfig({ businessUnitId: this.selectedBU(), includeSubBU: this.isIncludeSubBU()}).done((response) => {
                        response.items.unshift({
                            title: "",
                            id: -1
                        });
                        this.MailList(response.items);
                    }); 
                }//Call API BU
                //this.selectedMailConfig(response.items);
            });
        
        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable(setTimeNow);
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.periodDay = ko.pureComputed(() => {
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {
                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.start() == null) {
                // set end date when clear start date
                if (this.end() == null) {
                    isDate = new Date();
                } else {
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            } else {
                this.minDateEnd(this.start())
            }

            return isDate;
        });
    }

    setupExtend() {
        this.selectedCompany.extend({ required: true });
        this.isIncludeSubBU.extend({ required: true });
        this.selectedStatus.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedCompany: this.selectedCompany,
            isIncludeSubBU: this.BuCheck,
            selectedStatus: this.selectedStatus,
            end:this.end,
            start:this.start,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd

        });
    }

    get WebRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get WebRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get WebRequestMailConfiguration() {
        return WebRequestMailConfiguration.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var filter = { id: [] };
        this.WebRequestCompany.listCompany(filter).done((response) => {
            this.StuList(response.items);
        });
 
        let objStatusList = [{
                displayName: "",
                value: 0
            },
            {
                displayName: "Fail",
                value: 2
            },
            {
                displayName: "Success",
                value: 1
            },
            ];
        this.StatusList(objStatusList);

    }
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("AutoMail_Search")()));
        actions.push(this.createActionCancel());
    }
    onActionClick(sender) {
        super.onActionClick(sender);

        if(!this.validationModel.isValid()){
            this.validationModel.errors.showAllMessages();
            return ;
        }
        
        if (sender.id == "actSearch") {
            var IncludeSubBU = (this.isIncludeSubBU()) ? true : false;
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";
            let MailId = (typeof this.selectedMailConfig() == "undefined")? -1 : this.selectedMailConfig().id
            let businessUnitId = (typeof this.selectedBU() == "undefined")? null : this.selectedBU()

            var filter = {
                mode: "export",
                id: MailId,
                companyId: this.selectedCompany().id,
                businessUnitId: businessUnitId,
                status: this.selectedStatus().value,
                includeSubBU: IncludeSubBU,
                endDate : formatEDate,
                startDate : formatSDate,
            };
            this.WebRequestMailConfiguration.findLogsSummary(this.id).done((response) => {
                this.navigate("bo-configuration-sending-log-manage", filter);
            });
        }
    }
    onChildScreenClosed() {
    }

    onCommandClick(sender) {

    }

    onUnload() {
    }
}

export default {
    viewModel: ScreenBase.createFactory(SendingLogs),
    template: templateMarkup
};