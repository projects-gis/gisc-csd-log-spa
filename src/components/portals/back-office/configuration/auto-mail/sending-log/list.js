﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestMailConfiguration from "../../../../../../app/frameworks/data/apicore/WebRequestMailConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import { Constants, Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * 
 * 
 * @class TrackingDatabasesManageScreen
 * @extends {ScreenBase}
 */
class SendingLogsExport extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_SendingLogs")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.order = ko.observable([[0, "desc"]]);
        this.datalog = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.id = this.ensureNonObservable(params.id);
        this.companyId = this.ensureNonObservable(params.companyId);
        this.businessUnitId = this.ensureNonObservable(params.businessUnitId);
        this.status = this.ensureNonObservable(params.status);
        this.endDate = this.ensureNonObservable(params.endDate);
        this.startDate = this.ensureNonObservable(params.startDate);
        this.includeSubBU = this.ensureNonObservable(params.includeSubBU);

        //filter Export
        this.filterState = ko.observable({
            CompanyId: this.companyId,
            id: this.id,
            businessUnitId: this.businessUnitId,
            status: this.status,
            endDate: this.endDate,
            startDate: this.startDate,
            includeSubBU: this.includeSubBU
        });
        this.renderErrorMessage  = (data, type, row) =>{
            if (data) {
                let node = data.length > 100 ? data.substr( 0, 150 ) +'…' : data;
                return node; 
            }
            else {
                return "";
            }
        }

        this.exportExcel = (data) => {
            var filterFile = { id: data.id };
            this.webRequestMailConfiguration.downloadFileAutoMail(filterFile).done((response) => {
                if (response == null)
                {
                    this.showMessageBox("File Excel Download is not found");
                }
                else
                {
                    ScreenHelper.downloadFile(response.fileUrl);
                }
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }

    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){

    }

    get webRequestMailConfiguration() {
        return WebRequestMailConfiguration.getInstance();
    }

    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var filter = {
            id: this.id,
            startDate: this.startDate,
            endDate: this.endDate,
            companyId: this.companyId,
            businessUnitId: this.businessUnitId,
            includeSubBU:   this.includeSubBU,
            status: this.status,
        };

        var dfd = $.Deferred();
        
        this.webRequestMailConfiguration.findLogsSummary(filter).done((response) => {
                var datalog = response["items"];
                datalog.forEach(function (v) {
                    v.businessUnitName = (v.businessUnitName)?v.businessUnitName:"-";
                    v.download = "../../../../../../images/icon-download-xls.svg";
                });
                this.datalog(datalog);

                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
      
        return dfd;
    }

    /**
     * 
     * 
     * @param {any} sender
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onActionClick(sender) {
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("AutoMail_Export")(), "svg-cmd-export"));
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.filterState().AutoMailLogsFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            this.webRequestMailConfiguration.expoertEntity(this.filterState()).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */


    onUnload() 
    {
    }

   
}

export default {
    viewModel: ScreenBase.createFactory(SendingLogsExport),
    template: templateMarkup
};