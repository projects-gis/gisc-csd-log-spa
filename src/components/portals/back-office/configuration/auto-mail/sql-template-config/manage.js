﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestDatabaseConnection from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseConnection";
import WebRequestDatabaseServer from "../../../../../../app/frameworks/data/apitrackingcore/webRequestDatabaseServer";
import {Constants,Enums,EntityAssociation} from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestSqlTemplate from "../../../../../../app/frameworks/data/apicore/webRequestSqlTemplate";

/**
 * 
 * 
 * @class TrackingDatabasesManageScreen
 * @extends {ScreenBase}
 */
class SqlTemplateManageScreen extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.id = this.ensureNonObservable(params.sqlTempId);

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("AutoMail_CreateSqlTempTitle")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("AutoMail_UpdateSqlTempTitle")());
                break;
        }

        this.sqlTempId = this.ensureNonObservable(params.sqlTempId, -1);
        this.sqlTempName = ko.observable("");
        this.sqlStatement = ko.observable("");
        
        this.serverSelected = ko.observable();
        this.companies = ko.observableArray([]);

        this.disabled = this.mode === Screen.SCREEN_MODE_UPDATE ? false : true;
        this.visibleCompanies = this.mode === Screen.SCREEN_MODE_UPDATE ? true : false;

    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    setupExtend(){

        //Track Changed
        this.sqlTempName.extend({
            trackChange: true
        });
        
        this.sqlStatement.extend({
            trackChange: true
        });


        //validation
        this.sqlTempName.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.sqlStatement.extend({
            required: true,
            serverValidate: {
                params: "SqlStatement",
                //message: this.i18n("M159")()
            }
        });



        this.validationModel = ko.validatedObservable({
            sqlTempName: this.sqlTempName,
            sqlStatement: this.sqlStatement,
        });

    }

    /**
     * 
     * Get WebRequest for DatabaseConnection in Web API access.
     * @readonly
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    get webRequestSqlTemplate() {
        return WebRequestSqlTemplate.getInstance();
    }

    /**
     * 
     * Get WebRequest for DatabaseServer in Web API access.
     * @readonly
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    get webRequestDatabaseServer() {
        return WebRequestDatabaseServer.getInstance();
    }

    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        // var dfd = $.Deferred();
        switch (this.mode) {


            case Screen.SCREEN_MODE_CREATE:
                dfd.resolve();
                break;
            case Screen.SCREEN_MODE_UPDATE:
                //Get Service here.
                //var filter = { templateName: this.sqlTempName() };
                this.webRequestSqlTemplate.getSqlTemplate(this.id).done((sqlTemplate) => {
                   
                    if (sqlTemplate) {

                        this.sqlTempName(sqlTemplate.templateName);
                        this.sqlStatement(sqlTemplate.sqlStatement);
                        //this.sqlTempId(sqlTemplate.id);
                    }
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

                break;
        }
        return dfd;
    }

    /**
     * 
     * 
     * @param {any} actions
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildActionBar(actions) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                actions.push(this.createActionCancel());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                actions.push(this.createActionCancel());
                break;
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {
         
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var filter = {
                templateName: this.sqlTempName(),
                sqlStatement: this.sqlStatement(),
                id : this.sqlTempId,
                infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
                userId:WebConfig.userSession.id
            };
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestSqlTemplate.createSqlTemplate(filter, true).done((response) => {
                        this.publishMessage("bo-automail-sql-template-changed");
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestSqlTemplate.updateSqlTemplate(filter, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("bo-automail-sql-template-changed");
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    onValidateButtonClick(){
        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }
        this.isBusy(true);

        var filter = {
            sqlStatement: this.sqlStatement(),
        };
        this.webRequestSqlTemplate.validateSqlStatement(filter).done((r)=> {
            if(r.isValid == true){
                this.showMessageBox(null, this.i18n("M158")(), BladeDialog.DIALOG_OK);
            }
            else{
                this.showMessageBox(null,r.errorMessage, BladeDialog.DIALOG_OK);
            }
            this.isBusy(false);
        });
    }
    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesManageScreen
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M160")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.webRequestSqlTemplate.deleteSqlTemplate(this.sqlTempId).done(() => {
                            this.publishMessage("bo-automail-sql-template-changed", this.sqlTempId);
                            this.close(true);
                        });
                        break;
                }
            });
        }
    }

    /**
     * 
     * 
     * @returns
     * 
     * @memberOf TrackingDatabasesManageScreen
     */


    onUnload() {}
}

export default {
viewModel: ScreenBase.createFactory(SqlTemplateManageScreen),
    template: templateMarkup
};