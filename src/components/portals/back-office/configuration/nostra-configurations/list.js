﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestNostraConfigurations from "../../../../../app/frameworks/data/apitrackingcore/webRequestNostraConfigurations";

/**
 * 
 * 
 * @class TrackingDatabasesListScreen
 * @extends {ScreenBase}
 */
class NostraConfigurations extends ScreenBase {
    /**
     * Creates an instance of TrackingDatabasesListScreen.
     * 
     * @param {any} params
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Configurations_NostraConfigurations_List")());
        this.bladeSize = BladeSize.Medium;

        this.nostraList = ko.observableArray([]);
        this.selectedRow = ko.observable();

        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([]);
        this.selectedNostra = ko.observable(null);

        this.selectRow = (row) => {
            this.navigate("bo-configuration-nostra-configurations-manage", { data: row });
        }

        this.subscribeMessage("bo-configuration-nostra-configurations-response", info => {
            let id = info != undefined || info != null ? [info] : null
            this.getNostraResponseData(id);            
        });
    }

    /**
     * 
     * 
     * @readonly
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    get webRequestNostraConfigurations() {
        return WebRequestNostraConfigurations.getInstance();
    }

    /**
     * 
     * 
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.getNostraResponseData();
    }

    getNostraResponseData(id){
        var dfd = $.Deferred();
        this.webRequestNostraConfigurations.nostraList().done((r) => {
            this.nostraList.replaceAll(r["items"]);
            if(id) {
                this.recentChangedRowIds.replaceAll(id);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onChildScreenClosed() {
        // this.selectedCategories(null);
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    buildCommandBar(commands) {
            // commands.push(this.createCommand("cmdCreate", this.i18n("Common_Add")(), "svg-cmd-add"));
            //commands.push(this.createCommand("cmdCreate", this.i18n("TestEditPage")(), "svg-cmd-add"));
    }

   

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onUnload() {
    }
}

export default {
    viewModel: ScreenBase.createFactory(NostraConfigurations),
    template: templateMarkup
};