﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../screenbase";
import { SearchFilter } from "./info";


class AnnouncementSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Common_Search")());
        
        this.startDate = this.ensureObservable(params.searchFilter.startDate);
        this.endDate = this.ensureObservable( params.searchFilter.endDate);
    }

     /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if (!isFirstLoad) return;
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

     /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {

            var searchFilter = new SearchFilter( 
                this.startDate() ? this.startDate() : null,
                this.endDate() ? this.endDate() : null,
            );

            this.publishMessage("bo-announcements-search-filter-changed", searchFilter);
        }
    }
    
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id === "cmdClear"){
            this.startDate(null);
            this.endDate(null);
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementSearchScreen),
    template: templateMarkup
};
