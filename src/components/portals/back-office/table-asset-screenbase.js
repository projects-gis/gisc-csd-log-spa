import ko from "knockout";
import ScreenBase from "../screenbase";
import { Enums } from "../../../app/frameworks/constant/apiConstant";
import UIConstants from "../../../app/frameworks/constant/uiConstant";
import { AssetIcons } from "../../../app/frameworks/constant/svg";
import Utility from "../../../app/frameworks/core/utility";
import Logger from "../../../app/frameworks/core/logger";

class TableAssetScreenBase extends ScreenBase {
    constructor (params) {
        super(params);
        this.columns = ko.observableArray([]);
        this.selectedItem = ko.observable(null);
        this.defaultOrderColumnName = ko.observable();//set from screen
        this.defaultOrderDirection = ko.observable();//set from screen
        this.order = ko.observable([]);
        this.items = ko.observableArray([]);
        this.poiIcons = ko.observableArray([]);
        this.vehicleIcons = ko.observableArray([]);
        this.errMode = UIConstants.DataTableErrorMode.None;
        this.autoWidth = false;

        this._errorHandler = (e, settings, techNote, message) => {
            // see references from https://datatables.net/manual/tech-notes/
            switch (techNote) {
                // 1. Warning: Invalid JSON response
                // 2. Warning: Non-table node initialisation
                // 3. Warning: Cannot reinitialise DataTable
                // 4. Warning: Requested unknown parameter
                // 5. Warning: Unknown paging action
                // 6. Warning: Possible column misalignment
                // 7. Warning: Ajax error
                // 8. DataTables Markdown
                // 9. JS Bin
                // 10. Asking for help
                // 11. Editor: Unable to automatically determine field from source.
                // 12. Editor: A system error has occurred.
                // 13. JS/CSS file name structure
                // 14. Editor: Unable to find row identifier
                default:
                    Logger.log('An error has been reported by DataTables: ', message);
            }
        };

        this.renderSmallColumn = (data, type, row) => {
            if (data) {
                return "<div class='col-s truncate' title='" + data + "'>" + data + "</div>";
            }
            else {
                return "";
            }
        };

        this.renderMediumColumn = (data, type, row) => {
            if (data) {
                return "<div class='col-m truncate' title='" + data + "'>" + data + "</div>";
            }
            else {
                return "";
            }
        };

        this.renderLargeColumn = (data, type, row) => {
            if (data) {
                return "<div class='col-l truncate' title='" + data + "'>" + data + "</div>";
            }
            else {
                return "";
            }
        };

        this.renderXLargeColumn = (data, type, row) => {
            if (data) {
                return "<div class='col-xl truncate' title='" + data + "'>" + data + "</div>";
            }
            else {
                return "";
            }
        };

        this.renderMovementColumn = (data, type, row) => {
            if (data) {
                var cssClass = " ";
                switch (data) {
                    case Enums.ModelData.MovementType.Move:
                        cssClass += "move";
                        break;
                    case Enums.ModelData.MovementType.Stop:
                        cssClass += "stop";
                        break;
                    case Enums.ModelData.MovementType.Park:
                        cssClass += "park";
                        break;
                }

                let node = '<div title="' + row.movementDisplayName + '" class="icon-movement' + cssClass + '">' + AssetIcons.IconMovementTemplateMarkup + '</div>';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderDirectionColumn = (data, type, row) => {
            if (data) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (data) {
                    case Enums.ModelData.DirectionType.N:
                        iconTemplateMarkup = AssetIcons.IconDirectionNTemplateMarkup;
                        cssClass += "n";
                        break;
                    case Enums.ModelData.DirectionType.NE:
                        iconTemplateMarkup = AssetIcons.IconDirectionNETemplateMarkup;
                        cssClass += "ne";
                        break;
                    case Enums.ModelData.DirectionType.E:
                        iconTemplateMarkup = AssetIcons.IconDirectionETemplateMarkup;
                        cssClass += "e";
                        break;
                    case Enums.ModelData.DirectionType.SE:
                        iconTemplateMarkup = AssetIcons.IconDirectionSETemplateMarkup;
                        cssClass += "se";
                        break;
                    case Enums.ModelData.DirectionType.S:
                        iconTemplateMarkup = AssetIcons.IconDirectionSTemplateMarkup;
                        cssClass += "s";
                        break;
                    case Enums.ModelData.DirectionType.SW:
                        iconTemplateMarkup = AssetIcons.IconDirectionSWTemplateMarkup;
                        cssClass += "sw";
                        break;
                    case Enums.ModelData.DirectionType.W:
                        iconTemplateMarkup = AssetIcons.IconDirectionWTemplateMarkup;
                        cssClass += "w";
                        break;
                    case Enums.ModelData.DirectionType.NW:
                        iconTemplateMarkup = AssetIcons.IconDirectionNWTemplateMarkup;
                        cssClass += "nw";
                        break;
                }

                let node = '<div title="' + row.directionTypeDisplayName + '" class="icon-direction' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderGsmStatusColumn = (data, type, row) => {
            if (data) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (data) {
                    case Enums.ModelData.GsmStatus.Bad:
                        iconTemplateMarkup = AssetIcons.IconGsmBadTemplateMarkup;
                        cssClass += "bad";
                        break;
                    case Enums.ModelData.GsmStatus.Medium:
                        iconTemplateMarkup = AssetIcons.IconGsmMediumTemplateMarkup;
                        cssClass += "medium";
                        break;
                    case Enums.ModelData.GsmStatus.Good:
                        iconTemplateMarkup = AssetIcons.IconGsmGoodTemplateMarkup;
                        cssClass += "good";
                        break;
                }

                let node = '<div title="' + row.gsmStatusDisplayName + '" class="icon-gsm' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderGpsStatusColumn = (data, type, row) => {
            if (data) {
                var cssClass = " ";
                switch (data) {
                    case Enums.ModelData.GpsStatus.Bad:
                        cssClass += "bad";
                        break;
                    case Enums.ModelData.GpsStatus.Medium:
                        cssClass += "medium";
                        break;
                    case Enums.ModelData.GpsStatus.Good:
                        cssClass += "good";
                        break;
                }

                let node = '<div title="' + row.gpsStatusDisplayName + '" class="icon-gps' + cssClass + '">' + AssetIcons.IconGpsTemplateMarkup + '</div>';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderDltStatusColumn = (data, type, row) => {
            if (data) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (data) {
                    case Enums.ModelData.DltStatus.Success:
                        cssClass += "success";
                        iconTemplateMarkup = AssetIcons.IconDltStatusSuccessTemplateMarkup;
                        break;
                    case Enums.ModelData.DltStatus.Fail:
                        cssClass += "fail";
                        iconTemplateMarkup = AssetIcons.IconDltStatusFailTemplateMarkup;
                        break;
                }

                let node = '<div title="' + row.dltStatusDisplayName + '" class="icon-dltstatus' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderEngineColumn = (data, type, row) => {
            if (!_.isNil(data) && !_.isNil(data.valueDigital)) {
                var cssClass = " ";
                switch (data.valueDigital) {
                    case 0:
                        cssClass += "off";
                        break;
                    case 1:
                        cssClass += "on";
                        break;
                }

                let node = '<div title="' + data.formatValue + '" class="icon-engine' + cssClass + '">' + AssetIcons.IconEngineTemplateMarkup + '</div>';
                return node; 
            }
            else {
                return "";
            }
        };

        this.renderGateColumn = (data, type, row) => {
            if (!_.isNil(data) && !_.isNil(data.valueDigital)) {
                var cssClass = " ";
                var iconTemplateMarkup = null;
                switch (data.valueDigital) {
                    case 0:
                        iconTemplateMarkup = AssetIcons.IconGateOffTemplateMarkup;
                        cssClass += "off";
                        break;
                    case 1:
                        iconTemplateMarkup = AssetIcons.IconGateOnTemplateMarkup;
                        cssClass += "on";
                        break;
                }

                let node = '<div title="' + data.formatValue + '" class="icon-gate' + cssClass + '">' + iconTemplateMarkup + '</div>';
                return node;  
            }
            else {
                return "";
            }
        };

        this.renderSOSColumn = (data, type, row) => {
            if (!_.isNil(data) && !_.isNil(data.valueDigital)) {
                var cssClass = " ";
                switch (data.valueDigital) {
                    case 0:
                        cssClass += "off";
                        break;
                    case 1:
                        cssClass += "on";
                        break;
                }

                let node = '<div title="' + data.formatValue + '" class="icon-sos' + cssClass + '">' + AssetIcons.IconSosTemplateMarkup + '</div>';
                return node;  
            }
            else {
                return "";
            }
        };

        this.renderPowerColumn = (data, type, row) => {
            if (!_.isNil(data) && !_.isNil(data.valueDigital)) {
                var cssClass = " ";
                switch (data.valueDigital) {
                    case 0:
                        cssClass += "off";
                        break;
                    case 1:
                        cssClass += "on";
                        break;
                }

                let node = '<div title="' + data.formatValue + '" class="icon-power' + cssClass + '">' + AssetIcons.IconPowerTemplateMarkup + '</div>';
                return node; 
            }
            else {
                return "";
            }
        }

        this.renderDateTimeColumn = (data, type, row) => {
            if (data) {
                let node = '<div title="' + data + '" class="col-m truncate ' + (row.isOverFleetDelayTime ? "over-fleet-delay-time" : "") + '">' + data + '</div>';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderPoiIconColumn = (data, type, row) => {
            if (data) {
                var poiIcons = this.poiIcons();
                var poiIcon = poiIcons[row.poiIconId];

                let node = '<img class="icon-poi" title="' + data + '" src="' + poiIcon + '" />';
                return node;
            }
            else {
                return "";
            }
        }

        this.renderDelayColumn = (data, type, row) => {
            if (data && data == true) {
                let node = '<div title="' + row.formatIsDelay + '" class="icon-delay">' + AssetIcons.IconDelayTemplateMarkup + '</div>';
                return node; 
            }
            else {
                return "";
            }
        }
    }

    
    
    /**
     * 
     * @param {any} reportTemplateFields
     * @param {boolean} [isAlert=false] //Check if this method is calling by Alert page.
     * @param {integer} startIndex //for case we have additional columns before report temport template fields
     * @returns
     * 
     * @memberOf TableAssetScreenBase
     */
    getColumnDefinitions(reportTemplateFields, isAlert = false, startIndex = 0) {
        var columns = [];
       
        var index = startIndex; //use custom index instead of forEach index because we may skip some column e.g. Job Code, Job Status
        
        columns.push({
            type: "label",
            data: 'id',
            orderable: false,
            sType: "numeric",
            visible: false
        });
        index++;
         
        _.forEach(reportTemplateFields, (field) => {
            var type = "label";
            var data = "";
            var orderable = true;
            var render = null;
            var title = "";
            var sType = "string";
            var skipColumn = false;
            var additionalColumn = null; //for hidden sort column
            var orderData = [index, 0];
            var enumColumnName = null;
            var className = "";

            // if column is feature
            if (field.featureId) {
                data = "trackLocationFeatureValues." + field.featureId + ".formatValue";
                title = field.feature.unitDisplayName ? Utility.stringFormat("{0} ({1})", field.displayName, field.feature.unitDisplayName) : field.displayName;
                orderable = false;//feature cannot be sort by default

                switch (field.feature.code) {
                    case "TrkVwEngine":
                        type = "custom";
                        render = this.renderEngineColumn;
                        data = "trackLocationFeatureValues." + field.featureId;
                        title = '<div class="icon-engine" title="' + title + '">' + AssetIcons.IconEngineTemplateMarkup + '</div>';
                        break;
                    case "TrkVwGate":
                        type = "custom";
                        render = this.renderGateColumn;
                        data = "trackLocationFeatureValues." + field.featureId;
                        title = '<div class="icon-gate" title="' + title + '">' + AssetIcons.IconGateTemplateMarkup + '</div>';
                        break;
                    case "TrkVwFuel":
                        title = '<div class="icon-fuel" title="' + title + '">' + AssetIcons.IconFuelTemplateMarkup + '</div>';
                        className = "dt-body-center";
                        break;
                    case "TrkVwTemp":
                        title = '<div class="icon-temperature" title="' + title + '">' + AssetIcons.IconTemparetureTemplateMarkup + '</div>';
                        className = "dt-body-center";
                        break;
                    case "TrkVwVehicleBattery":
                        title = '<div class="icon-vehiclebattery" title="' + title + '">' + AssetIcons.IconVehicleBatteryTemplateMarkup + '</div>';
                        className = "dt-body-center";
                        break;
                    case "TrkVwDeviceBattery":
                        title = '<div class="icon-devicebattery" title="' + title + '">' + AssetIcons.IconDeviceBatteryTemplateMarkup + '</div>';
                        className = "dt-body-center";
                        break;
                    case "TrkVwSOS":
                        type = "custom";
                        render = this.renderSOSColumn;
                        data = "trackLocationFeatureValues." + field.featureId;
                        title = '<div class="icon-sos" title="' + title + '">' + AssetIcons.IconSosTemplateMarkup + '</div>';
                        break;
                    case "TrkVwPower":
                        type = "custom";
                        render = this.renderPowerColumn;
                        data = "trackLocationFeatureValues." + field.featureId;
                        title = '<div class="icon-power" title="' + title + '">' + AssetIcons.IconPowerTemplateMarkup + '</div>';
                        break;
                    case "TrkVwDRVCardReader":
                        title = '<div class="icon-drivercard" title="' + title + '">' + AssetIcons.IconDriverCardTemplateMarkup + '</div>';
                        break;
                    case "TrkVwPSGRCardReader":
                        title = '<div class="icon-passengercard" title="' + title + '">' + AssetIcons.IconPassengerCardTemplateMarkup + '</div>';
                        break;
                    case "TrkVwMileage":
                        title = '<div class="icon-mileage" title="' + title + '">' + AssetIcons.IconMileageTemplateMarkup + '</div>';
                        className = "dt-body-center";
                        break;
                    default:
                        title = '<div class="icon-customfeature" title="' + title + '">' + AssetIcons.IconCustomFeatureTemplateMarkup + '</div>';
                        // Custom feature which type is ‘Input’ and data type is ‘Digital’, ‘Analog (Temperature, Percent, Speed) also aligns center.
                        if(field.feature.eventType === Enums.ModelData.EventType.Input) {
                            switch(field.feature.eventDataType){
                                case Enums.ModelData.EventDataType.Digital:
                                case Enums.ModelData.EventDataType.Analog:
                                    className = "dt-body-center";
                                    break;
                            }
                        }
                        break;
                }
            }
            // column is not feature
            else {
                data = Utility.toLowerCaseFirstLetter(field.mapPropertyName);
                title = field.unitSymbol ? Utility.stringFormat("{0} ({1})", field.displayName, field.unitSymbol) : field.displayName;
                switch (field.name) {
                    
                    case "License":
                        type = "link";
                        render = this.renderSmallColumn;
                        enumColumnName = Enums.SortingColumnName.VehicleLicense;
                        break;
                    case "ReferenceNo":
                        type = "custom";
                        render = this.renderSmallColumn;
                        enumColumnName = Enums.SortingColumnName.VehicleReferenceNo;
                        break;
                    case "Username":
                        type = "custom";
                        render = this.renderSmallColumn;
                        enumColumnName = Enums.SortingColumnName.Username;
                        break;
                    case "Email":
                        type = "custom";
                        render = this.renderMediumColumn;
                        enumColumnName = Enums.SortingColumnName.Email;
                        break;
                    case "EmployeeID":
                        type = "custom";
                        render = this.renderSmallColumn;
                        enumColumnName = Enums.SortingColumnName.DriverEmployeeId;
                        break;
                    case "Driver":
                        type = "custom";
                        render = this.renderMediumColumn;
                        enumColumnName = Enums.SortingColumnName.DriverName;
                        break;
                    case "BusinessUnit":
                        type = "custom";
                        render = this.renderLargeColumn;
                        enumColumnName = Enums.SortingColumnName.BusinessUnitName;
                        break;
                    case "Radius":
                        enumColumnName = Enums.SortingColumnName.Radius; 
                        className = "dt-body-center";
                        break;
                    case "Movement":
                        type = "custom";
                        render = this.renderMovementColumn;
                        data = "movement";
                        title = '<div class="icon-movement" title="' + title + '">' + AssetIcons.IconMovementTemplateMarkup + '</div>';
                        //need additionalColumn because if has render, datatable will sort from render instead of data
                        orderData = [(index + 1) , 0];
                        enumColumnName = Enums.SortingColumnName.Movement;                        
                        additionalColumn = {
                            type: "label",
                            data: "movement",
                            visible: false,
                            sType: "numeric"
                        };
                        break;
                    case "Speed":
                        title = '<div class="icon-speed" title="' + title + '">' + AssetIcons.IconSpeedTemplateMarkup + '</div>';
                        sType = "numeric";
                        enumColumnName = Enums.SortingColumnName.Speed; 
                        className = "dt-body-center";
                        break;
                    case "Direction":
                        type = "custom";
                        render = this.renderDirectionColumn;
                        data = "directionType";
                        title = '<div class="icon-direction" title="' + title + '">' + AssetIcons.IconDirectionTemplateMarkup + '</div>';
                        //need additionalColumn because if has render, datatable will sort from render instead of data
                        orderData = [(index + 1) , 0];
                        enumColumnName = Enums.SortingColumnName.DirectionType;
                        additionalColumn = {
                            type: "label",
                            data: "directionType",
                            visible: false,
                            sType: "numeric"
                        };
                        break;
                    case "GPSStatus":
                        type = "custom";
                        render = this.renderGpsStatusColumn;
                        data = "gpsStatus";
                        title = '<div class="icon-gps" title="' + title + '">' + AssetIcons.IconGpsTemplateMarkup + '</div>';
                        //need additionalColumn because if has render, datatable will sort from render instead of data
                        orderData = [(index + 1), 0];
                        enumColumnName = Enums.SortingColumnName.GpsStatus;
                        additionalColumn = {
                            type: "label",
                            data: "gpsStatus",
                            visible: false,
                            sType: "numeric"
                        };
                        
                        break;
                    case "GSMStatus":
                        type = "custom";
                        render = this.renderGsmStatusColumn;
                        data = "gsmStatus";
                        title = '<div class="icon-gsm" title="' + title + '">' + AssetIcons.IconGsmTemplateMarkup + '</div>';
                        //need additionalColumn because if has render, datatable will sort from render instead of data
                        orderData = [(index + 1), 0];
                        enumColumnName = Enums.SortingColumnName.GsmStatus;
                        additionalColumn = {
                            type: "label",
                            data: "gsmStatus",
                            visible: false,
                            sType: "numeric"
                        };
                        break;
                    case "Altitude":
                        title = '<div class="icon-altitude" title="' + title + '">' + AssetIcons.IconAltitudeTemplateMarkup + '</div>';
                        sType = "numeric";
                        enumColumnName = Enums.SortingColumnName.Altitude;
                        className = "dt-body-center";
                        break;
                    case "DLTStatus":
                        type = "custom";
                        render = this.renderDltStatusColumn;
                        data = "dltStatus";
                        title = '<div class="icon-dltstatus" title="' + title + '">' + AssetIcons.IconDltStatusTemplateMarkup + '</div>';
                        //need additionalColumn because if has render, datatable will sort from render instead of data
                        orderData = [(index + 1), 0];
                        enumColumnName = Enums.SortingColumnName.DltStatus;
                        additionalColumn = {
                            type: "label",
                            data: "dltStatus",
                            visible: false,
                            sType: "numeric"
                        };
                        break;
                    case "TotalEvent":
                        title = '<div class="icon-event" title="' + title + '">' + AssetIcons.IconEventTemplateMarkup + '</div>';
                        sType = "numeric";
                        enumColumnName = Enums.SortingColumnName.TotalAlerts;
                        className = "dt-body-center";
                        break;
                    case "CoDriver":
                        type = "custom";
                        render = this.renderMediumColumn;
                        orderable = false;
                        break;
                    case "JobCode":
                    case "JobStatus":
                        skipColumn = true;
                        break;
                    case "Datetime":
                        type = "custom";
                        render = this.renderDateTimeColumn;
                        enumColumnName = isAlert ? Enums.SortingColumnName.AlertDateTime : Enums.SortingColumnName.DateTime;
                        orderData = [(index + 1), 0];
                        additionalColumn = {
                            type: "label",
                            data: Utility.toLowerCaseFirstLetter(field.mapPropertyName.replace("Format", "")),
                            visible: false,
                            sType: "numeric"
                        };
                        break;
                    case "POIicon":
                        type = "custom";
                        render = this.renderPoiIconColumn;
                        enumColumnName = Enums.SortingColumnName.PoiIconName;
                        break;
                    case "Location":
                        type = "custom";
                        render = this.renderXLargeColumn;
                        orderable = false;
                        break;
                    case "AlertType":
                        type = "custom";
                        render = this.renderMediumColumn;
                        orderData = [(index + 1), 0];
                        enumColumnName = Enums.SortingColumnName.AlertType;
                        additionalColumn = {
                            type: "label",
                            data: "alertType",
                            visible: false,
                            sType: "numeric"
                        };
                        break;
                    case 'IsDelay':
                        type = "custom";
                        data = "isDelay";
                        render = this.renderDelayColumn;
                        enumColumnName = Enums.SortingColumnName.IsDelay;
                        break;
                    case 'TotalTicket':
                        type = "link";
                        render = this.renderSmallColumn;
                        break;
                        
                }
            }

            if (field.name === this.defaultOrderColumnName()) {
                this.order([[index, this.defaultOrderDirection()]]);
            }

            if (!skipColumn) {
                columns.push({
                    type: type,
                    title: title,
                    data: data,
                    orderable: orderable,
                    render: render,
                    sType: sType,
                    orderData: orderData,
                    enumColumnName: enumColumnName,
                    className: className
                });
                index++;
            }   

            if (additionalColumn) {
                columns.push(additionalColumn);
                index++;
            }         
        });
        return columns;
    }
}

export default TableAssetScreenBase;