﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestDLTReport from "../../../../../app/frameworks/data/apicore/webRequestDLTReport";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";


class DLTSummaryReport extends ScreenBase{
    constructor(params) {
        super(params);
        this.bladeTitle("DLT Summary");
        this.bladeSize = BladeSize.Medium;
        this.bladeIsMaximized = true;
        this.bladeTitle(this.i18n("DLT Summary")());
        this.apiDataSource = ko.observableArray([]);

        this.filter = params ; 
        //console.log(this.filter);
    }

    get webRequestDLTReport() {
        return WebRequestDLTReport.getInstance();
    }
    setupExtend() {
    }

    onLoad(isFirstLoad) {

        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        this.apiDataSource({
            read: this.webRequestDLTReport.listDLTSummary(this.filter)
        });

        dfd.resolve();
        return dfd;
    }

    buildActionBar(actions) {
    }

    onActionClick(sender) {
       
    }

    
    onUnload() {

    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            //var filter = $.extend(true, {}, this.searchFilter);
                            //filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            //filter.sortingColumns = this.generateSortingColumns();
                            this.webRequestDLTReport.exportDLTSummary(this.filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
viewModel: ScreenBase.createFactory(DLTSummaryReport),
    template: templateMarkup
};
