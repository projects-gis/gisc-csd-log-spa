﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import ScreenBase from "../../../screenbase";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDLTSendEmail from "../../../../../app/frameworks/data/apicore/webRequestDLTSendEmail";
import WebRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";
import WebRequestContract from "../../../../../app/frameworks/data/apitrackingcore/webRequestContract";

class DLTLetterScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Search Letters")());

        this.companyOptions = ko.observableArray([]); 
        this.selectedCompany = ko.observable();
        this.businessUnitOptions = ko.observableArray([]);
        this.businessUnitId = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.vehicleOptions = ko.observableArray([]);
        this.vehicleId = ko.observable();

        this.contractNoOptions = ko.observableArray([]);
        this.contractNo = ko.observable();

        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.formatSet = "dd/MM/yyyy";
        this.maxStartDate = ko.observable(new Date());
        this.minStartDate = ko.observable(new Date());

        this.transpoterInfos = ko.observableArray([]);
        this.selectedTranspoter = ko.observable();
        
        this.yearInfos = ko.observableArray([]);
        this.selectedYear = ko.observable();
        
        this.isShowSignature = ko.observable(false);
        this.isBlankForm = ko.observable(false);

        this.selectedCompany.subscribe(value => {
            this.businessUnitOptions([]);
            this.vehicleOptions([]);
            var currentCompanyId = (typeof value == 'undefined') ? null : value.id;
            this.webRequestBusinessUnit.listBusinessUnitSummary({
                companyId: currentCompanyId, 
                sortingColumns: DefaultSorting.BusinessUnit
            }).done((response) => {
                this.businessUnitOptions(response.items);
            });
        });

        // this.periodDay = ko.pureComputed(() => {
        //     // can't select date more than current date
        //     let addDay = 30;
        //     var isDate = Utility.addDays(this.startDate(), addDay);
        //     let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
        //     let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
        //     var calDay = endDate - currentDate;
        //     if (calDay > 0) {
        //         isDate = new Date();
        //     }

        //     // set date when clear start date and end date
        //     if (this.startDate() == null) {
        //         // set end date when clear start date
        //         if (this.endDate() == null) {
        //             isDate = new Date();
        //         } else {
        //             isDate = this.endDate();
        //         }
        //         this.maxStartDate(isDate);
        //         this.minStartDate(isDate);
        //     } else {
        //         this.minStartDate(this.startDate())
        //     }

        //     return isDate;
        // });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.vehicleOptions([]);
            var businessUnitIds = (this.isIncludeSubBU()) ? Utility.includeSubBU(this.businessUnitOptions(),this.businessUnitId()) : this.businessUnitId();
            var currentCompanyId = (typeof this.selectedCompany() == 'undefined') ? null : this.selectedCompany().id;
            if(this.businessUnitId() != null){
                //Vehicle
                this.webRequestVehicle.listVehicleSummary({
                    companyId: currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                     let listVehicle = response.items;
                    // //add displayName "Report_All" and id 0
                    // if(Object.keys(listVehicle).length > 0 && listVehicle[0].id !== 0){
                    //     listVehicle.unshift({
                    //         license: this.i18n('Report_All')(),
                    //         id: 0
                    //     });
                    // }
                    this.vehicleOptions(listVehicle);
                });
            }
            return ' ';
        });
        
        this.selectedCompany.subscribe((val) => {
            if(val){
                this.webRequestContract.listContract({enable: true,companyId: val.id}).done((r) => {
                    this.contractNoOptions(r["items"]);
                });
            }
            else{
                this.contractNoOptions([]);
            }
        });
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

     /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
     * Get WebRequest specific for DLTSendEmail module in Web API access.
     * @readonly
     */
    get webRequestDLTSendEmail() {
        return WebRequestDLTSendEmail.getInstance();
    }
    
    /**
     * Get WebRequest specific for Transporter module in Web API access.
     * @readonly
     */
    get webRequestTransporter() {
        return WebRequestTransporter.getInstance();
    }

    /**
     * Get WebRequest specific for Contract module in Web API access.
     * @readonly
     */
    get webRequestContract() {
        return WebRequestContract.getInstance()
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        var d1 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });
        var d2 = this.webRequestDLTSendEmail.allYear();
        var d3 = this.webRequestTransporter.listTransporter({});

        $.when(d1,d2,d3).done((r1,r2,r3) => {
            this.companyOptions(r1["items"]);

            let arrYear = [];
            for(let item in r2){
                let obj = {
                    id : item,
                    name : r2[item]
                }
               arrYear.push(obj);
            }

            this.yearInfos(arrYear);
            this.transpoterInfos(r3.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        
        if(sender.id == "actSearch"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let filterLetter = this.generateModel();
            this.navigate("bo-report-letter-list",{filter: filterLetter});
        }
    }

    setupExtend() {
        this.selectedCompany.extend({required: true});
        
        let self = this;
        this.startDate.extend({
            required:{
                onlyIf:() =>{
                    return self.endDate()
                }
            }
        });

        this.endDate.extend({
            required:{
                onlyIf:() =>{
                    return self.startDate()
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            selectedCompany: this.selectedCompany,
            startDate: this.startDate,
            endDate: this.endDate
        });       
    } 

    generateModel(){
        return {
            companyId: this.selectedCompany().id,
            contractNumber:this.contractNo() ? this.contractNo().contractNo : null,
            fromInstallDate: this.startDate(),
            toInstallDate: this.endDate(),
            VehicleId: this.vehicleId() ? this.vehicleId().id : null,
            tranSporterId: this.selectedTranspoter() ? this.selectedTranspoter().id : null,
            year: this.selectedYear() ?  parseInt(this.selectedYear().name) : null,
            showSignature: this.isShowSignature(),
            typeFilter: Enums.ModelData.DLTReportType.Letter,
            useTemplate: this.isBlankForm(),
            businessUnitId: this.businessUnitId(),
            includeSubBusinessUnit: this.isIncludeSubBU()
        };
    }
}


export default {
    viewModel: ScreenBase.createFactory(DLTLetterScreen),
    template: templateMarkup
};