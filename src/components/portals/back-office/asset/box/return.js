﻿import ko from "knockout";
import templateMarkup from "text!./return.html";
import ScreenBase from "../../../screenbase";
import {Enums, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestBoxStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxStock";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class BoxReturnScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Assets_ReturnBoxes")());

        this.company = ko.observable();
        this.businessUnit = ko.observable(null);
        this.stock = ko.observable();
        this.rentedBoxes = ko.observableArray([]);
        this.selectedBoxes = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.order = ko.observable([[ 1, "asc" ]]);

        this.findEnable = ko.pureComputed(() => {
            return this.company.isValid() && this.businessUnit.isValid();
        });

        this.companyOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.stockOptions = ko.observableArray([]);
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d0 = $.Deferred();

        var d1 = this.webRequestCompany.listCompanySummary({ sortingColumns: DefaultSorting.CompanyByName });
        var d2 = this.webRequestBoxStock.listBoxStockSummary({ sortingColumns: DefaultSorting.BoxStock });

        $.when(d1, d2).done((r1, r2) => {
            this.companyOptions(r1["items"]);
            this.stockOptions(r2["items"]);
            d0.resolve();
        }).fail((e) => {
            d0.reject(e);
        });

        $.when(d0).done(() => {
            this._changeCompanySubscribe = this.company.subscribe((company) => {
                this.businessUnit(null);
                this.businessUnitOptions.removeAll();
                if (company) {
                    this.isBusy(true);
                    this.webRequestBusinessUnit.listBusinessUnitSummary({ companyId: company.id, includeAssociationNames: [ EntityAssociation.BusinessUnit.ChildBusinessUnits ], sortingColumns: DefaultSorting.BusinessUnit }).done((response) => {
                        this.businessUnitOptions.replaceAll(response["items"]);
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                }
            });
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Box Stock module in Web API access.
     * @readonly
     */
    get webRequestBoxStock() {
        return WebRequestBoxStock.getInstance();
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.company.extend({ trackChange: true });
        this.businessUnit.extend({ trackChange: true });
        this.selectedBoxes.extend({ trackArrayChange: true });
        this.stock.extend({ trackChange: true });

        // validation
        this.company.extend({ required: true });
        this.businessUnit.extend({ required: true });
        this.selectedBoxes.extend({ 
            arrayRequired: { 
                params: true, 
                message: this.i18n("M001")()
            }
        });
        this.stock.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            company: this.company,
            businessUnit: this.businessUnit,
            selectedBoxes: this.selectedBoxes,
            stock: this.stock
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actReturn", this.i18n("Common_Return")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actReturn") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var selectedBoxIds = ko.utils.arrayMap(this.selectedBoxes(), function(selectedBox) {
                return selectedBox.id;
            });

            this.webRequestBox.returnBoxes({ ids: selectedBoxIds, boxStockId: this.stock().id }).done(() => {
                this.isBusy(false);
                this.publishMessage("bo-asset-box-changed");
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    
    /**
     * Hook onclick find rented boxes button
     */
    findRentedBoxes() {
        this.isBusy(true);
        this.selectedBoxes.removeAll();
        this.webRequestBox.listBoxSummary({ companyId: this.company().id, businessUnitIds: [ this.businessUnit() ], isForRent: true, sortingColumns: DefaultSorting.BoxBySerialNo }).done((response) => {
            this.isBusy(false);
            this.rentedBoxes.replaceAll(response["items"]);
        }).fail((e) => {
            this.rentedBoxes.replaceAll([]);
            this.isBusy(false);
            this.handleError(e);
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxReturnScreen),
    template: templateMarkup
};