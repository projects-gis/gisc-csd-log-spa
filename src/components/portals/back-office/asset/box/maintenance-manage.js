﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestBoxMaintenance from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxMaintenance";

class BoxMaintenanceManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_AddBoxMaintenance")());
        this.mode = this.ensureNonObservable(params.mode);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Assets_AddBoxMaintenance")());
        }
        else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Assets_UpdateBoxMaintenance")());
        }

        this.id = this.ensureNonObservable(params.id, -1);
        this.boxId = this.ensureNonObservable(params.boxId);

        this.datetime = ko.observable();
        this.descriptions = ko.observable();
        this.author = ko.observable();
    }

    setupExtend() {

        //Track Change
        this.datetime.extend({ trackChange: true });
        this.descriptions.extend({ trackChange: true });
        this.author.extend({ trackChange: true });

        //Validation
        this.datetime.extend({
            required: true
        });

        this.descriptions.extend({
            required: true
        });

        this.author.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            datetime: this.datetime,
            descriptions: this.descriptions,
            author: this.author,
        });
    }

    /**
     * Get WebRequest specific for Box Maintenance module in Web API access.
     * @readonly
     */
    get webRequestBoxMaintenance() {
        return WebRequestBoxMaintenance.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var d1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestBoxMaintenance.getBoxMaintenance(this.id) : null;

        $.when(d1).done((boxMaintenance) => {
            if(boxMaintenance){
                this.datetime(boxMaintenance.dateTime);
                this.descriptions(boxMaintenance.description);
                this.author(boxMaintenance.author);
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            boxId: this.boxId,
            datetime: this.datetime(),
            description : this.descriptions(),
            author :this.author(),
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            id: this.id
        };

        return model;
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBoxMaintenance.createBoxMaintenance(model, true).done((box) => {
                        this.isBusy(false);
                        this.publishMessage("bo-asset-box-manage-maintenance-manage-changed", box.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBoxMaintenance.updateBoxMaintenance(model, true).done((box) => {
                        this.isBusy(false);
                        this.publishMessage("bo-asset-box-manage-maintenance-manage-changed", box.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M037")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestBoxMaintenance.deleteBoxMaintenance(this.id).done(() => {
                                this.isBusy(false);
                                this.publishMessage("bo-asset-box-manage-maintenance-manage-changed", this.id);
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
            });
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxMaintenanceManageScreen),
    template: templateMarkup
};