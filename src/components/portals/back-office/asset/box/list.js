﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import { Enums, Constants} from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import { BoxFilter } from "./infos";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";

class BoxListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Boxes")());
        this.bladeSize = BladeSize.XLarge;
        this.pageSizeOptions = ko.observableArray([10, 20, 50, 100]);
        this.boxes = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.selectedBox = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.searchFilter = ko.observable(new BoxFilter());
        this.searchFilterDefault = new BoxFilter(); // Use to compare with searchFilter to apply active state
        this.pageFiltering = ko.observable(false); // Use to apply active state for command search
        this.order = ko.observable([[ 1, "asc" ]]);

        
        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (box) => {
            if(box && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBox)) {
                return this.navigate("bo-asset-box-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: box.id });
            }
            return false;
        };

        // Subscribe Message when box search filter apply
        this.subscribeMessage("bo-asset-box-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter(searchFilter);

            this.refreshDataSource();
        });

        // subscribe on searchFilter change to apply filter state
        this._changeSearchFilterSubscribe = this.searchFilter.subscribe((searchFilter) => {
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter(), this.searchFilterDefault));
        });

        // Subscribe Message when company created/updated
        this.subscribeMessage("bo-asset-box-changed", (boxId) => {
            this.refreshDataSource();
        });
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestBox.listBoxSummary(this.generateFilter()).done((response) => {
            this.boxes(response["items"]);      
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }


    onDatasourceRequestRead(gridOption) {
        this.isBusy(true);
        let dfd = $.Deferred();    
        let filter = Object.assign({IncludeCount : true}, gridOption,this.generateFilter());
        this.webRequestBox.listBoxSummary(filter).done((res) => {
            dfd.resolve({
                items: res.items,
                totalRecords: res.totalRecords,
                currentPage: res.currentPage
            });
        }).fail((e)=> {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);
        });


        return dfd;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBox(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBox)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBox) && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBox)){
            commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        }
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateBox)){
            commands.push(this.createCommand("cmdAssign", this.i18n("Common_Assign")(), "svg-cmd-assign"));
            commands.push(this.createCommand("cmdReturn", this.i18n("Common_Return")(), "svg-cmd-return"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    refreshDataSource() {
        this.dispatchEvent("ddBox", "refresh");
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdSearch":
                this.navigate("bo-asset-box-search", { searchFilter: this.searchFilter() });
                break;
            case "cmdCreate":
                this.navigate("bo-asset-box-manage", { mode: "create" });
                break;
            case "cmdImport":
                this.navigate("bo-asset-box-import");
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filter = this.generateFilter();
                            filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            this.webRequestBox.exportBox(filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break; 
            case "cmdAssign":
                this.navigate("bo-asset-box-assign");
                break;
            case "cmdReturn":
                this.navigate("bo-asset-box-return");
                break;
        }
        
        this.selectedBox(null);
        this.recentChangedRowIds([]);
    }

    /**
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
    generateFilter() {

        //var sortingColumns = [{},{}];

        //sortingColumns[0].direction = this.order()[0][1] === "asc" ? 1 : 2 ;

        //switch (this.order()[0][0]) {
        //    case 1:
        //         sortingColumns[0].column = Enums.SortingColumnName.Imei;
        //        break;
        
        //    case 2:
        //         sortingColumns[0].column = Enums.SortingColumnName.SerialNo;
        //        break;
        //    case 3:
        //         sortingColumns[0].column = Enums.SortingColumnName.VendorName;
        //        break;
        
        //    case 4:
        //         sortingColumns[0].column = Enums.SortingColumnName.CompanyName;
        //        break;
        //    case 5:
        //         sortingColumns[0].column = Enums.SortingColumnName.BusinessUnitName;
        //        break;
        
        //    case 6:
        //         sortingColumns[0].column = Enums.SortingColumnName.BoxStatus;
        //        break;
            
        //    case 8:
        //        sortingColumns[0].column = Enums.SortingColumnName.BoxTemplateName;
        //        break;
            
        //    case 9:
        //        sortingColumns[0].column = Enums.SortingColumnName.BoxStockName;
        //        break;
            
        //}

        //sortingColumns[1].column = Enums.SortingColumnName.Id;
        //sortingColumns[1].direction = sortingColumns[0].direction;


        return {
            companyId: this.searchFilter().companyId,
            businessUnitIds: this.searchFilter().businessUnitId ? [this.searchFilter().businessUnitId] : null,
            vendorId: this.searchFilter().vendorId,
            statuses: this.searchFilter().boxStatus ? [this.searchFilter().boxStatus] : null,
            boxTemplateId: this.searchFilter().boxTemplateId,
            boxStockId: this.searchFilter().boxStockId
            //sortingColumns: sortingColumns
        };
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxListScreen),
    template: templateMarkup
};