﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestBoxStock from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxStock";

class BoxStockListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Assets_BoxStocks")());
        this.bladeSize = BladeSize.Medium;

        this.boxStocks = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.selectedBoxStock = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);


        this._selectingRowHandler = (boxStock) => {
            if(boxStock && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxsStock)) {
                return this.navigate("bo-asset-box-stock-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: boxStock.id });
            }
            return false;
        };

         // Observe change about user in this Journey
        this.subscribeMessage("bo-asset-box-stock-changed", (boxStockId) => {
            // Refresh entire datasource
            this.isBusy(true);
             this.webRequestBoxStock.listBoxStockSummary().done((response)=> {
                this.boxStocks(response["items"]);
                this.recentChangedRowIds.replaceAll([boxStockId]);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });
    }

    /**
     * Get WebRequest specific for Box-Stock in Tracking Web API access.
     * @readonly
     */
    get webRequestBoxStock() {
        return WebRequestBoxStock.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
      
        this.webRequestBoxStock.listBoxStockSummary().done((response)=> {
            var boxStocks = response["items"];
            this.boxStocks(boxStocks);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

     /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxsStock)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

     /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-asset-box-stock-manage", { mode: Screen.SCREEN_MODE_CREATE });
            this.selectedBoxStock(null);
            this.recentChangedRowIds.removeAll();
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBoxStock(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxStockListScreen),
    template: templateMarkup
};