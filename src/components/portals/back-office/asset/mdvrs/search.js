import ko from "knockout";
import templateMarkup from "text!./search.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../screenhelper";
import ScreenBase from "../../../screenbase";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestMDVRModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVRModel";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";

class MdvrSearchScreen extends ScreenBase {
    constructor(params){
        super(params);

        this.bladeTitle('MDVR Search');
        this.companyOptions = ko.observableArray([]);
        this.selectedCompany = ko.observable();
        this.buOptions = ko.observableArray([]);
        this.buId = ko.observable();
        this.includeSubBu = ko.observable();
        this.vehicleOptions = ko.observableArray([]);
        this.selectedVehicle = ko.observable();
        this.mdvrModel = ko.observableArray([]);
        this.selectedMdvrModel = ko.observable();
        this.gridFilter = this.ensureNonObservable(params,{});

        this.selectedCompany.subscribe((val) => {
            if(val){
                this.webRequestBusinessUnit.listBusinessUnitSummary({companyId: val.id}).done((r) => {
                    this.buOptions(r.items);
                });
            }
            else{
                this.buOptions([]);
                this.vehicleOptions([]);
            }
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.vehicleOptions([]);
            var businessUnitIds = (this.includeSubBu()) ? Utility.includeSubBU(this.buOptions(),this.buId()) : this.buId();
            var currentCompanyId = (typeof this.selectedCompany() == 'undefined') ? null : this.selectedCompany().id;
            if(this.buId() != null){
                //Vehicle
                this.webRequestVehicle.listVehicleSummary({
                    companyId: currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                    let listVehicle = response.items;
                    // //add displayName "Report_All" and id 0
                    // if(Object.keys(listVehicle).length > 0 && listVehicle[0].id !== 0){
                    //     listVehicle.unshift({
                    //         license: this.i18n('Report_All')(),
                    //         id: 0
                    //     });
                    // }
                    this.vehicleOptions(listVehicle);
                });
            }
            return ' ';
        });
    }

    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestMDVRModel() {
        return WebRequestMDVRModel.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();
        var dfdMDVRModel = this.webRequestMDVRModel.listMDVRModelList();
        var dfdCompany = this.webRequestCompany.listCompanySummary()
        $.when(dfdCompany,dfdMDVRModel).done((r,r2) => {
            this.companyOptions(r.items);
            this.mdvrModel(r2.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id == "actSearch"){
            let filter = {
                companyId: this.selectedCompany() ? this.selectedCompany().id : null,
                businessUnitId: this.buId() ? this.buId() : null,
                vehicleId: this.selectedVehicle() ? this.selectedVehicle().id : null,
                modelId: this.selectedMdvrModel() ? this.selectedMdvrModel().id : null,
                includeSubBusinessUnit: this.includeSubBu()
            }
            // let allFilter = _.assign({},this.gridFilter,filter);

            this.publishMessage("bo-asset-mdvr-search",filter);
        }
    }
}


export default {
    viewModel: ScreenBase.createFactory(MdvrSearchScreen),
    template: templateMarkup
};
