﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../../screenhelper";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestMDVR from "../../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";


class MdvrConfigListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n('MDVR Configuration List')());
        this.listMDVRModel = ko.observableArray([])
        this.bladeSize = BladeSize.Medium;

        this.trackingDBs = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.selectedRow = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([
            [0, "asc"]
        ]);

        this._selectingRowHandler = (operator) => {
            if (operator && WebConfig.userSession.hasPermission(Constants.Permission.UpdateMDVRModel)) {
                return this.navigate("bo-asset-mdvrs-config-update", { mode: Screen.SCREEN_MODE_UPDATE, mdvrConfigId: operator.id });
            }
        };

        this.subscribeMessage("bo-asset-mdvrs-config-changed", (MDVRModelId) => {
            this.genTableList(MDVRModelId);

        });
    }
    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

       
       this.genTableList()

    }

    genTableList(id) {
        var dfd = $.Deferred();
        this.isBusy(true);
        this.webRequestMDVR.listMDVRConfigSummary().done((responMDVRModel) => {
           this.listMDVRModel(responMDVRModel["items"]);
            if (id) {
                this.recentChangedRowIds.replaceAll([id]);
            }
            dfd.resolve();
            this.isBusy(false);
        }).fail((e) => {
            dfd.reject(e);
            this.isBusy(false);
        });
        return dfd;
    }


    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }






    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        //trackChange

    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        // actions.push(this.createAction("actLiveView", this.i18n('Common_Display')()));
        // actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateMDVRModel)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }

    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-asset-mdvrs-config-manage", {
                    mode: Screen.SCREEN_MODE_CREATE
                });
                break;
            case "cmdUpdate":
                this.navigate("bo-asset-mdvrs-config-update", {
                    mode: Screen.SCREEN_MODE_UPDATE
                });
                break;
        }
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actLiveView") {

        }
    }



}

export default {
    viewModel: ScreenBase.createFactory(MdvrConfigListScreen),
    template: templateMarkup
};