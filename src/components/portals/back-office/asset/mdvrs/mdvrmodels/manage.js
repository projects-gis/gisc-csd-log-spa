﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../../screenhelper";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebRequestMDVRModel from "../../../../../../app/frameworks/data/apitrackingcore/webRequestMDVRModel";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import {Constants, Enums} from "../../../../../../app/frameworks/constant/apiConstant";



class MdvrModelsManageScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = params.mode
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n('Assets_Mdvr_Models_Create')());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n('Assets_Mdvr_Models_Update')());
                this.mdvrModel = params.id;
                break;
        }

        this.bladeSize = BladeSize.Medium;

        this.codeMdvr = ko.observable();
        this.modelMdvr = ko.observable();
        this.brandMdvr = ko.observable();

        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.optionMdvr = ko.observable();
        this.validationModel = ko.validatedObservable({});

        this.brandMdvr = ko.observable();
        this.brandOptions = ko.observableArray([]);

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;




        var dfd = $.Deferred();
        var dfdBrand = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.MDVRModel],
            values: [
                Enums.ModelData.MDVRModel.HIKvision,
                Enums.ModelData.MDVRModel.IQTech,
                Enums.ModelData.MDVRModel.DFI
            ]
        });

        var dfdMDVRModel = null;
       // this.minimizeAll(false);
        if(this.mode == Screen.SCREEN_MODE_UPDATE){
            dfdMDVRModel = this.webRequestMDVRModel.getMDVRModel(this.id);
        }

        $.when(dfdBrand, dfdMDVRModel).done((resBrand, responMDVRModel) => {
            if (resBrand){
                this.brandOptions.replaceAll(resBrand.items);
            }

            if (responMDVRModel){
                this.codeMdvr(responMDVRModel["code"]);
                this.brandMdvr(ScreenHelper.findOptionByProperty(this.brandOptions, "value", responMDVRModel["brand"]));
                this.modelMdvr(responMDVRModel["model"]);
                this.optionMdvr(ScreenHelper.findOptionByProperty(this.enableOptions, "value", responMDVRModel["enable"]));
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    } 

    get webRequestMDVRModel() {
        return WebRequestMDVRModel.getInstance();
    }

    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        //trackChange
        this.codeMdvr.extend({
            trackChange: true
        });
        this.brandMdvr.extend({
            trackChange: true
        });
        this.modelMdvr.extend({
            trackChange: true
        });
        this.optionMdvr.extend({
            trackChange: true
        });
       

        // Setup validation properties.
        this.codeMdvr.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        
        this.brandMdvr.extend({
            required: true
        });
        this.modelMdvr.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.optionMdvr.extend({
            required: true
        });

        // Compose validation model.
        this.validationModel({
            code: this.codeMdvr,
            brand: this.brandMdvr,
            model: this.modelMdvr,
            selectedEnable: this.optionMdvr
        });
    }



    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_UPDATE:
            if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRModel)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }  
                break;
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M214")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestMDVRModel.deleteMDVRModel(this.id)
                            .done(()=>{
                                this.publishMessage("bo-asset-mdvr-models-changed", this.id);
                                this.close(true);
                            })
                            .fail((e)=>{
                                this.handleError(e);
                            })
                            .always(()=>{
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        switch(sender.id) {
            case "actSave":
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                this.isBusy(true);

                var filter = {
                    code: this.codeMdvr(),
                    brand: this.brandMdvr().value,//toString(),
                    model: this.modelMdvr(),
                    enable: this.optionMdvr().value
                }

               
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.webRequestMDVRModel.createMDVRModel(filter, true)
                        .done((vehicleModel) => {
                            this.publishMessage("bo-asset-mdvr-models-changed", vehicleModel.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                    filter.id = this.id;
                    this.webRequestMDVRModel.updateMDVRModel(filter)
                        .done(() => {
                            this.publishMessage("bo-asset-mdvr-models-changed", this.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                        break;
                }
        }
    }



    onMapComplete(data) {
        switch (data.command) {
            case "open-liveview":
                this.minimizeAll();
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(MdvrModelsManageScreen),
    template: templateMarkup
};