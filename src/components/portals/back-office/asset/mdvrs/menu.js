﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AssetMenuScreen extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("MDVR")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // This page has only one permission for all sub menu.
        // Do not need extra validation permission.
        if (!isFirstLoad) {
            return;
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateMDVRModel) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateMDVRModel) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRModel) )
        {
            this.items.push({
                text: this.i18n('Assets_Mdvr_Models')(),
                page: 'bo-asset-mdvr-models'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateMDVRs) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateMDVRs) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRs) )
        {
            this.items.push({
                text: this.i18n('MDVR')(),
                page: 'bo-asset-mdvrs'
            });
        }

        if( WebConfig.userSession.hasPermission(Constants.Permission.CreateMDVRsConfig) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateMDVRsConfig) ||
            WebConfig.userSession.hasPermission(Constants.Permission.DeleteMDVRsConfig) )
        {
            this.items.push({
                text: this.i18n('Assets_Mdvr_Config')(),
                page: 'bo-asset-mdvrs-config'
            });
        }
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(AssetMenuScreen),
    template: templateMarkup
};