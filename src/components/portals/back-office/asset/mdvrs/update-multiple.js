﻿import ko from "knockout";
import templateMarkup from "text!./update-multiple.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import WebRequestContract from "../../../../../app/frameworks/data/apitrackingcore/webRequestContract";
import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";

class UpdateMdvrMultipleScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.filter = this.ensureNonObservable(params.filter,{});
        this.bladeTitle(this.i18n("Update Multiple MDVR")());
        this.bladeSize = BladeSize.Small;

        this.itemMdvr = ko.observableArray(params.items,[])
        this.contractNoOptions = ko.observableArray([]);
        this.contractNo = ko.observable();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */

    get webRequestContract() {
        return WebRequestContract.getInstance()
    }

    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }

    onLoad(isFirstLoad) {
        // console.log("onLoad")
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var dfdContract = this.webRequestContract.listContract({"enable":true});

        $.when(dfdContract).done((r) => {
            this.contractNoOptions(r["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
       
        return dfd;

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        // console.log("unload");
    }


    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        this.contractNo.extend({required: true,trackChange: true})
        this.itemMdvr.extend({arrayRequired: true});

        this.validationModel = ko.validatedObservable({
            contractNo: this.contractNo,
            itemMdvr: this.itemMdvr
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {

    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
    }
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let infos = this.generateModel();

            this.isBusy(true);
            this.webRequestMDVR.updateMultipleMDVR(infos).done((r) => {
                this.close(true);
                this.publishMessage("bo-asset-mdvrs",this.filter);
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });
        }
    }

    generateModel() {

        this.itemMdvr().forEach((item) => {
            item.contractId = this.contractNo().id
        });

        return this.itemMdvr();
    }

}

export default {
    viewModel: ScreenBase.createFactory(UpdateMdvrMultipleScreen),
    template: templateMarkup
};



