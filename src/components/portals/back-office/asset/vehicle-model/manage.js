﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {Constants, Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleModel";

/**
 * Manage Vehicle Model Screen.
 * 
 * @class ManageVehicleModelScreen
 * @extends {ScreenBase}
 */
class ManageVehicleModelScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = params.mode;
        this.id = this.ensureNonObservable(params.id, -1);
        this.code = ko.observable("");
        this.brand = ko.observable("");
        this.model = ko.observable("");
        this.selectedEnable = ko.observable(null);
        this.enableDataSource = ScreenHelper.createYesNoObservableArray();
        this.weight = ko.observable("");
        this.loadWeight = ko.observable("");
        this.wheel = ko.observable("");
        this.fuelCapacity = ko.observable("");
        this.fuelMin = ko.observable("");
        this.fuelMax = ko.observable("");
        this.validationModel = ko.validatedObservable({});
        this.selectedSubMenuItem = ko.observable(null);
        this.selectedInverseFeatures = ko.observableArray();
        this._originalInverseFeatures = ko.observableArray([]);
        this.isApplyAssociateVehicle = ko.observable(false);
        this.rpmMultipleValue = ko.observable("");
        this.fuelMultipleValue = ko.observable("");
        
        // Service properties.
        this.webRequestVehicleModel = WebRequestVehicleModel.getInstance();

        // Set page title.
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_CreateVehicleModel")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateVehicleModel")());
                break;
        }

        this.subscribeMessage("bo-accessible-inverse-features-apply", (ids) => {
            this.selectedInverseFeatures(ids);
        });

    }

    onSelectSubMenuItem(index) {
        if (this.journeyCanClose()) {
            this.goToSubPage(index);
        } else {
            this.showMessageBox(null, this.i18n("M100")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.goToSubPage(index);
                        break;
                }
            });
        }
    }

    goToSubPage(index) {
        var page = "";
        var options = new Object();

        switch (index) {
            case 1:
                page = "bo-shared-accessible-inverse-features-select";
                options.selectedInverseFeatures = _.clone(this.selectedInverseFeatures());
                break;
        }

        this.forceNavigate(page, options);
        this.selectedSubMenuItem(index);
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        // Loading user when update mode.
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableDataSource, "value", true));
                // No need to loadd extra service, relase loader.
                dfd.resolve();
                break;
            case Screen.SCREEN_MODE_UPDATE:
                // Load current vehicle model data.
                this.webRequestVehicleModel.getVehicleModel(this.id)
                .done((response) => {
                    this.code(response.code);
                    this.brand(response.brand);
                    this.model(response.model);
                    this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableDataSource, "value", response.enable));
                    this.weight(response.weight);
                    this.loadWeight(response.loadWeight);
                    this.wheel(response.wheel);
                    this.fuelCapacity(response.fuelCapacity);
                    this.fuelMin(response.fuelMin);
                    this.fuelMax(response.fuelMax);
                    this.selectedInverseFeatures(response.inverseFeatureIds);
                    this.rpmMultipleValue(response.multiplyValues);
                    this.fuelMultipleValue(response.fuelMultiplyValues);
                    this._originalInverseFeatures(_.cloneDeep(response.inverseFeatureIds));
                    dfd.resolve();
                })
                .fail((e) => {
                    dfd.reject(e);
                });
                break;
        }

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }

    /**
     * Adding trackchange and validation to properties.
     */
    setupExtend() {
        // Setup trackchange properties.
        this.code.extend({
            trackChange: true
        });
        this.brand.extend({
            trackChange: true
        });
        this.model.extend({
            trackChange: true
        });
        this.selectedEnable.extend({
            trackChange: true
        });
        this.weight.extend({
            trackChange: true
        });
        this.loadWeight.extend({
            trackChange: true
        });
        this.wheel.extend({
            trackChange: true
        });
        this.fuelCapacity.extend({
            trackChange: true
        });
        this.fuelMin.extend({
            trackChange: true
        });
        this.fuelMax.extend({
            trackChange: true
        });

        // Setup validation properties.
        this.code.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.brand.extend({
            required: true
        });
        this.model.extend({
            required: true
        });
        this.selectedEnable.extend({
            required: true
        });

        // Compose validation model.
        this.validationModel({
            code: this.code,
            brand: this.brand,
            model: this.model,
            selectedEnable: this.selectedEnable
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.CreateVehicleModel)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
            case Screen.SCREEN_MODE_UPDATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicleModel)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
        }
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_UPDATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteVehicleModel)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
                break;
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch(sender.id) {
            case "actSave":
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                // Mark start of long operation
                this.isBusy(true);

                // Populate model from screen.
                var vehicleModel = this.generateModel();

                // Calling service for save/update vehicle model.
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.webRequestVehicleModel.createVehicleModel(vehicleModel, true)
                        .done((vehicleModel) => {
                            this.publishMessage("bo-vehicle-model-changed", vehicleModel.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        var dfd = $.Deferred();
                        let isEqual = _.isEqual(_.sortBy(this._originalInverseFeatures()), _.sortBy(this.selectedInverseFeatures()));
                        if(!isEqual){
                            this.showMessageBox(null, "Do you want to apply settings from this Vehicle Model to all associated vehicle(s)?",
                                BladeDialog.DIALOG_YESNO).done((button) => {
                                switch (button) {
                                    case BladeDialog.BUTTON_YES:
                                        // this.isApplyAssociateVehicle(true);
                                        vehicleModel.isApplyAssociateVehicle = true;
                                        dfd.resolve();
                                        break;
                                    case BladeDialog.BUTTON_NO:
                                        // this.isApplyAssociateVehicle(false);
                                        vehicleModel.isApplyAssociateVehicle = false;
                                        dfd.resolve();
                                        break;
                                }
                            });
                        }else{
                            // this.isApplyAssociateVehicle(false);
                            vehicleModel.isApplyAssociateVehicle = false;
                            dfd.resolve();
                        }

                        $.when(dfd).done((res)=>{
                            this.webRequestVehicleModel.updateVehicleModel(vehicleModel)
                                .done(() => {
                                    this.publishMessage("bo-vehicle-model-changed", this.id);
                                    this.close(true);
                                })
                                .fail((e) => {
                                    this.handleError(e);
                                })
                                .always(() => {
                                    this.isBusy(false);
                                });
                        });
                        break;
                }
        }
    }
    /**
     * 
     * Prepare AJAX model from page to REST service.
     */
    generateModel () {
        var model = {
            id: this.id,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            code: this.code(),
            brand: this.brand(),
            model: this.model(),
            enable: this.selectedEnable().value,
            weight: this.weight(),
            loadWeight: this.loadWeight(),
            wheel: this.wheel(),
            fuelCapacity: this.fuelCapacity(),
            fuelMin: this.fuelMin(),
            fuelMax: this.fuelMax(),
            inverseFeatureIds: this.selectedInverseFeatures(),
            isApplyAssociateVehicle: this.isApplyAssociateVehicle(),
            multiplyValues: this.rpmMultipleValue(),
            fuelMultiplyValues: this.fuelMultipleValue()
        };

        return model;
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M118")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestVehicleModel.deleteVehicleModel(this.id)
                            .done(()=>{
                                this.publishMessage("bo-vehicle-model-changed", this.id);
                                this.close(true);
                            })
                            .fail((e)=>{
                                this.handleError(e);
                            })
                            .always(()=>{
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(ManageVehicleModelScreen),
    template: templateMarkup
};