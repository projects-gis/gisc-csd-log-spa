﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import {Constants, Enums} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

/**
 * Box Feature > List
 * 
 * @class BoxFeatureListScreen
 * @extends {ScreenBase}
 */
class BoxFeatureListScreen extends ScreenBase {

    /**
     * Creates an instance of BoxFeatureListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_BoxFeatures")());
        this.bladeSize = BladeSize.Large;

        this.features = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedFeature = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        this._selectingRowHandler = (feature) => {
            if(feature && WebConfig.userSession.hasPermission(Constants.Permission.AssetManagement)) {
                return this.navigate("bo-asset-box-feature-manage", { mode: "update", featureId: feature.id });
            }
            return false;
        };

        // Observe change about user in this Journey
        this.subscribeMessage("bo-asset-feature-changed", (featureId) => {
            // Refresh entire datasource
            this.isBusy(true);

            var filter = {
                sortingColumns: DefaultSorting.BoxFeature
            };
            this.webRequestFeature.listFeatureSummary(filter).done((response)=> {
                var features = response["items"];
                this.features(features);
                this.recentChangedRowIds.replaceAll([featureId]);
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });
    }
    
    /**
     * Get WebRequest specific for Feature module in Tracking Web API access.
     * @readonly
     */
    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();
        var filter = {
            sortingColumns: DefaultSorting.BoxFeature
        };
        this.webRequestFeature.listFeatureSummary(filter).done((response)=> {
            var features = response["items"];
            this.features(features);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedFeature(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxFeature)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id == "cmdCreate") {
            this.navigate("bo-asset-box-feature-manage", { mode: "create" });
            this.selectedFeature(null);
            this.recentChangedRowIds.removeAll();
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxFeatureListScreen),
    template: templateMarkup
};