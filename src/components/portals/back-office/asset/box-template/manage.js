﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {
    Constants,
    EntityAssociation,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestBoxTemplate from "../../../../../app/frameworks/data/apitrackingcore/webrequestBoxTemplate";
import WebRequestBoxModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxModel";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import BoxTemplateFeatureModel from "./boxTemplateFeatureModel";

class ManageBoxTemplateScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = params.mode;
        this.id = this.ensureNonObservable(params.id, -1);
        this.name = ko.observable("");
        this.code = ko.observable("");
        this.boxModelDataSource = ko.observableArray();
        this.selectedBoxModel = ko.observable();
        this.selectedBoxModelSubscribe = null;
        this.hasChangeBoxModel = ko.observable(false);
        this.enableDataSource = ScreenHelper.createYesNoObservableArray();
        this.selectedEnable = ko.observable(null);
        this.availableBoxModels = [];
        this.availableFeatures = [];
        this.selectedFeatures = ko.observableArray();
        this.inputEvents = ko.observableArray();
        this.outputEvents = ko.observableArray();
        this.inputAccessories = ko.observableArray();
        this.outputAccessories = ko.observableArray();
        this.validationModel = ko.validatedObservable({});

        // Service properties.
        this.webRequestBoxTemplate = WebRequestBoxTemplate.getInstance();
        this.webRequestBoxModel = WebRequestBoxModel.getInstance();
        this.webRequestFeature = WebRequestFeature.getInstance();

        // Set page title.
        this.bladeSize = BladeSize.XMedium;
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_CreateBoxTemplate")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateBoxTemplate")());
                break;
        }
        window.bt = this;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var boxFeatureDfd = this.webRequestFeature.listFeature({
            sortingColumns: DefaultSorting.Feature
        });
        var boxModelDfd = this.webRequestBoxModel.listBoxModel({
            includeAssociationNames: [EntityAssociation.BoxModel.PackageType],
            sortingColumns: DefaultSorting.BoxModel
        });

        $.when(boxFeatureDfd, boxModelDfd)
            .done((boxFeatureResponse, boxModelResponse) => {
                // Keep features for available options.
                this.availableFeatures = boxFeatureResponse.items;

                // Prepare box model options.
                this.availableBoxModels = boxModelResponse.items;
                var obServableBoxModelOptions = ScreenHelper.createGenericOptionsObservableArray(boxModelResponse.items, "name", "id");
                this.boxModelDataSource(obServableBoxModelOptions());

                // Populate extra form based on mode.
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableDataSource, "value", true));
                        this.selectedBoxModelSubscribe = this.selectedBoxModel.subscribe(this.onBoxModelChanged, this);
                        // No need to loadd extra service, relase loader.
                        dfd.resolve();
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        // Loading box template data service.
                        this.webRequestBoxTemplate.getBoxTemplate(this.id)
                            .done((response) => {
                                this.name(response.name);
                                this.code(response.code);
                                this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableDataSource, "value", response.enable));
                                this.selectedBoxModel(ScreenHelper.findOptionByProperty(this.boxModelDataSource, "value", response.modelId));
                                this.selectedFeatures.merge(response.features);
                                this.populateModel();
                                this.selectedBoxModelSubscribe = this.selectedBoxModel.subscribe(this.onBoxModelChanged, this);
                                dfd.resolve();
                            })
                            .fail((e) => {
                                dfd.reject(e);
                            });
                        break;
                }
            })
            .fail((e) => {
                dfd.reject(e);
            });

        return dfd;
    }
    /**
     * Adding trackchange and validation to properties.
     */
    setupExtend() {
        // Setup track change.
        this.name.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });
        this.selectedBoxModel.extend({
            trackChange: true
        });
        this.selectedEnable.extend({
            trackChange: true
        });
        this.inputEvents.extend({
            trackChangeByProperty: "isDirty"
        });
        this.outputEvents.extend({
            trackChangeByProperty: "isDirty"
        });
        this.inputAccessories.extend({
            trackChangeByProperty: "isDirty"
        });
        this.outputAccessories.extend({
            trackChangeByProperty: "isDirty"
        });

        // Setup validation object.
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.selectedBoxModel.extend({
            required: true
        });
        this.selectedEnable.extend({
            required: true
        });
        this.inputEvents.extend({
            arrayIsValid: "isValid"
        });
        this.outputEvents.extend({
            arrayIsValid: "isValid"
        });
        this.inputAccessories.extend({
            arrayIsValid: "isValid"
        });
        this.outputAccessories.extend({
            arrayIsValid: "isValid"
        });

        this.validationModel({
            name: this.name,
            code: this.code,
            selectedBoxModel: this.selectedBoxModel,
            selectedEnable: this.selectedEnable,
            inputEvents: this.inputEvents,
            outputEvents: this.outputEvents,
            inputAccessories: this.inputAccessories,
            outputAccessories: this.outputAccessories
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                if (WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxTemplate)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
            case Screen.SCREEN_MODE_UPDATE:
                if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxTemplate)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
        }
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        switch (this.mode) {
            case Screen.SCREEN_MODE_UPDATE:
                if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxTemplate)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
                break;
        }
    }
    /**
     * Pouplate selected feature table from service.
     */
    populateModel() {
        var findFeature = (boxPackageItem) => {
            var feature = null;
            if (this.selectedFeatures()) {
                var featureFilter = _.filter(this.selectedFeatures(), (f) => {
                    return f.event === boxPackageItem.name && f.eventType == boxPackageItem.eventType;
                });
                if (featureFilter.length) {
                    feature = featureFilter[0];
                }
            }
            return feature;
        };

        var selectedBoxModel = this.selectedBoxModel();
        if (selectedBoxModel) {
            var boxModels = _.filter(this.availableBoxModels, (bm) => {
                return bm.id === selectedBoxModel.value;
            });

            if (!_.isEmpty(boxModels)) {
                var boxModel = boxModels[0];
                boxModel.packageType.items.forEach((boxPackageItem) => {
                    var f = findFeature(boxPackageItem);
                    if (f !== null) {
                        var featureModel = new BoxTemplateFeatureModel(
                            f.id, // datatable references.
                            boxPackageItem.name, //f.event, // event name (string).
                            this.filterFeatureByEventType(this.availableFeatures, boxPackageItem.eventType, boxPackageItem.eventDataType),
                            f.featureId, // selected feature.
                            f.note, // typed note.
                            f.infoStatus, // orignial info status.
                            boxPackageItem.eventDataType,
                            boxPackageItem.eventDataTypeDisplayName
                        );
                        featureModel.eventDataType = f.eventDataType;
                        featureModel.isSupportAnalog = f.isSupportAnalog;

                        if (this.checkShowAnaloConvert(boxPackageItem.eventDataType, f.eventDataType, f.isSupportAnalog)) {
                            featureModel.analogValue = f.analogValue ? f.analogValue : 1;
                        }


                        switch (boxPackageItem.eventType) {
                            case Enums.ModelData.BoxFeatureEventType.InputEvent:
                                this.inputEvents.push(featureModel);
                                break;
                            case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                                this.outputEvents.push(featureModel);
                                break;
                            case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                                this.inputAccessories.push(featureModel);
                                break;
                            case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                                this.outputAccessories.push(featureModel);
                                break;
                        }
                    }
                });
            }
        }
    }

    /**
     * Generate model from page to REST api.
     * @return
     */
    generateModel() {
        var boxTemplateModel = {
            id: this.id,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            name: this.name(),
            code: this.code(),
            modelId: this.selectedBoxModel().value,
            enable: this.selectedEnable().value,
            features: []
        };

        // Collect all features from every tables.
        // In create mode we always use Add for every feature.
        // In update mode we need to detect below.
        var effectiveInfoStatus = Enums.InfoStatus.Add;
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            // When changing model ID we always send STATUS Add.
            // When no change model ID we always send STATUS Update.
            if (this.hasChangeBoxModel()) {
                effectiveInfoStatus = Enums.InfoStatus.Add;
            } else {
                effectiveInfoStatus = Enums.InfoStatus.Update;
            }
        }

        // Utility function for finding feature event datatype by feature id.
        var findFeatureEventDataTypeById = (featureId) => {
            var selectedEventDataType = null;

            if (!_.isNil(featureId)) {
                var matchedFeatures = _.filter(this.availableFeatures, (f) => {
                    return f.id === featureId;
                });

                if (!_.isEmpty(matchedFeatures)) {
                    selectedEventDataType = matchedFeatures[0].eventDataType;
                }
            }

            return selectedEventDataType;
        };

        var orderIndex = 1;
        this.inputEvents().forEach(inputEvent => {
            var newInputEvent = {
                eventType: Enums.ModelData.BoxFeatureEventType.InputEvent,
                id: this.mode === Screen.SCREEN_MODE_CREATE ? -1 : inputEvent.id,
                infoStatus: effectiveInfoStatus,
                event: inputEvent.name,
                eventDataType: findFeatureEventDataTypeById(inputEvent.selectedFeature),
                featureId: inputEvent.selectedFeature,
                order: orderIndex,
                note: inputEvent.note,
                analogValue: inputEvent.analogValue
            };
            boxTemplateModel.features.push(newInputEvent);
            orderIndex++;
        });

        orderIndex = 1;
        this.outputEvents().forEach(outputEvent => {
            var newOutputEvent = {
                eventType: Enums.ModelData.BoxFeatureEventType.OutputEvent,
                id: this.mode === Screen.SCREEN_MODE_CREATE ? -1 : outputEvent.id,
                infoStatus: effectiveInfoStatus,
                event: outputEvent.name,
                eventDataType: findFeatureEventDataTypeById(outputEvent.selectedFeature),
                featureId: outputEvent.selectedFeature,
                order: orderIndex,
                note: outputEvent.note
            };
            boxTemplateModel.features.push(newOutputEvent);
            orderIndex++;
        });

        orderIndex = 1;
        this.inputAccessories().forEach(inputAccessory => {
            var newInputAccessory = {
                eventType: Enums.ModelData.BoxFeatureEventType.InputAccessory,
                id: this.mode === Screen.SCREEN_MODE_CREATE ? -1 : inputAccessory.id,
                infoStatus: effectiveInfoStatus,
                event: inputAccessory.name,
                eventDataType: findFeatureEventDataTypeById(inputAccessory.selectedFeature),
                featureId: inputAccessory.selectedFeature,
                order: orderIndex,
                analogValue: inputAccessory.analogValue
            };
            boxTemplateModel.features.push(newInputAccessory);
            orderIndex++;
        });

        orderIndex = 1;
        this.outputAccessories().forEach(outputAccessory => {
            var newOutputAccessory = {
                eventType: Enums.ModelData.BoxFeatureEventType.OutputAccessory,
                id: this.mode === Screen.SCREEN_MODE_CREATE ? -1 : outputAccessory.id,
                infoStatus: effectiveInfoStatus,
                event: outputAccessory.name,
                eventDataType: findFeatureEventDataTypeById(outputAccessory.selectedFeature),
                featureId: outputAccessory.selectedFeature,
                order: orderIndex
            };
            boxTemplateModel.features.push(newOutputAccessory);
            orderIndex++;
        });

        return boxTemplateModel;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch (sender.id) {
            case "actSave":
                // Force observable array notify changes -> force validation evaluate.
                ko.validation.validateObservable(this.inputEvents);
                ko.validation.validateObservable(this.outputEvents);
                ko.validation.validateObservable(this.inputAccessories);
                ko.validation.validateObservable(this.outputAccessories);

                // Validate page before submitting form.
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }
                
                let findError = null;
                if(_.size(this.inputEvents())){
                    // ถ้ามี error ของ textbox support analog จะ return
                    findError = this.findErrorSupportAnalog(this.inputEvents());
                    if(findError) return;
                }
                if(_.size(this.inputAccessories())){
                     // ถ้ามี error ของ textbox support analog จะ return
                     findError = this.findErrorSupportAnalog(this.inputAccessories());
                     if(findError) return;
                }
                // Mark start of long operation
                this.isBusy(true);

                // Populate model from screen.
                var boxTemplate = this.generateModel();
                //console.log(JSON.stringify(boxTemplate));

                // Calling service for save/update box model.
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.webRequestBoxTemplate.createBoxTemplate(boxTemplate, true)
                            .done((boxTemplate) => {
                                this.isBusy(false);
                                this.publishMessage("bo-boxtemplate-changed", boxTemplate.id);
                                this.close(true);
                            })
                            .fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        this.webRequestBoxTemplate.updateBoxTemplate(boxTemplate)
                            .done(() => {
                                this.isBusy(false);
                                this.publishMessage("bo-boxtemplate-changed", this.id);
                                this.close(true);
                            })
                            .fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                        break;
                }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M114")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestBoxTemplate.deleteBoxTemplate(this.id)
                                .done(() => {
                                    this.publishMessage("bo-boxtemplate-changed", this.id);
                                    this.close(true);
                                })
                                .fail((e) => {
                                    this.handleError(e);
                                })
                                .always(() => {
                                    this.isBusy(false);
                                });
                            break;
                    }
                });
                break;
        }
    }
    /**
     * Validate user selected feature doesn't duplciate with other.
     * 
     * @param {any} collection
     */
    _validateFeatureForUniqueSelection(collection) {
        var selectedFeatureIds = [];
        var duplicateFeatureIds = [];
        collection.forEach((f) => {
            var currentFeatureId = f.selectedFeature;

            // Skip null checking ids.
            if (_.isNil(currentFeatureId) || currentFeatureId === "") {
                return;
            }

            // Store unique ids.
            if (selectedFeatureIds.indexOf(currentFeatureId) === -1) {
                selectedFeatureIds.push(currentFeatureId);
            } else {
                duplicateFeatureIds.push(currentFeatureId);
            }
        });

        // Make duplicate id unique.
        duplicateFeatureIds = _.uniq(duplicateFeatureIds);

        // Display for all duplicate rows.
        collection.forEach((f) => {
            if (duplicateFeatureIds.indexOf(f.selectedFeature) !== -1) {
                // Exist display error for that field.
                f.error = this.i18n("M131")();
            } else {
                f.error = "";
            }

            if (_.size(f.features)) {

                let currFeature = _.filter(f.features, (item) => {
                    return item.id == f.selectedFeature;
                }, f);

                if (_.size(currFeature)) {
                    f.eventDataType = currFeature[0].eventDataType;
                    f.isSupportAnalog = currFeature[0].isAnalog;


                    if (this.checkShowAnaloConvert(f.eventModelDataType, f.eventDataType, f.isSupportAnalog)) {
                        let analogVal = parseFloat(f.analogValue);
                        if (f.analogValue == null) {
                            f.analogValue = 1;
                            f.errorAnalogVal = null;
                        } else if (_.isNaN(analogVal)) {
                            f.errorAnalogVal = f.analogValue == "" ? this.i18n("M001")() : this.i18n("M065")(); //invalid
                        } else {
                            if (analogVal > 99.99) {
                                f.analogValue = null;
                                f.errorAnalogVal = this.i18n("M001")(); //required
                            } else {
                                f.errorAnalogVal = null;
                                f.analogValue = _.round(analogVal, 2);
                            }
                        }
                    }else {
                        f.eventDataType = null;
                        f.isSupportAnalog = null;
                        f.analogValue = null;
                    }

                } 
            }

        });
    }
    /**
     * Auto select feature event data type from selected feature id.
     * 
     * @param {any} feature
     */
    _updateFeature(feature) {
        // Update info status.
        if (feature.infoStatus === Enums.InfoStatus.Original) {
            feature.infoStatus = Enums.InfoStatus.Update;
        }
    }
    /**
     * Check for duplicate feature of input event.
     */
    onInputEventDataSourceChanged(feature) {
        this._updateFeature(feature);
        let tmpInputEvent = _.clone(this.inputEvents());
        this.inputEvents([]);
        this._validateFeatureForUniqueSelection(tmpInputEvent);
        this.inputEvents(tmpInputEvent);
    }
    /**
     * Check for duplicate feature of output event.
     */
    onOutEventDataSourceChanged(feature) {
        this._updateFeature(feature);
        this._validateFeatureForUniqueSelection(this.outputEvents());
    }
    /**
     * Check for duplicate feature of input accessories.
     */
    onInputAccessoryDataSourceChanged(feature) {
        this._updateFeature(feature);
        let tmpInputAccessoryEvent = _.clone(this.inputAccessories());
        this.inputAccessories([]);
        this._validateFeatureForUniqueSelection(tmpInputAccessoryEvent);
        this.inputAccessories(tmpInputAccessoryEvent);
    }
    /**
     * Check for duplicate feature of output accessories.
     */
    onOutputAccessoryDataSourceChanged(feature) {
        this._updateFeature(feature);
        this._validateFeatureForUniqueSelection(this.outputAccessories());
    }

    /**
     * Filter features by event type.
     * 
     * @param {any} features
     * @param {any} eventType
     * @returns
     */
    filterFeatureByEventType(features, eventType, eventDataType) {
        return _.filter(features, f => {
            var pickableFeatureForEventType = false;

            switch (eventType) {
                case Enums.ModelData.BoxFeatureEventType.InputEvent:
                    if (eventDataType == Enums.ModelData.EventDataType.Analog) {

                        pickableFeatureForEventType = f.eventType === Enums.ModelData.EventType.Input

                        if (f.eventDataType == Enums.ModelData.EventDataType.Digital) {
                            pickableFeatureForEventType = pickableFeatureForEventType && f.isAnalog
                        }
                        else{
                            pickableFeatureForEventType = pickableFeatureForEventType && f.eventDataType === eventDataType
                        }
                    } else {
                        pickableFeatureForEventType = (
                            f.eventType === Enums.ModelData.EventType.Input &&
                            f.eventDataType === eventDataType
                        );
                    }

                    break;
                case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                    pickableFeatureForEventType = (
                        f.eventType === Enums.ModelData.EventType.Output &&
                        f.eventDataType === eventDataType
                    );
                    break;
                case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                        if (eventDataType == Enums.ModelData.EventDataType.Analog) {

                            pickableFeatureForEventType = f.eventType === Enums.ModelData.EventType.Input && f.isAccessory
    
                            if (f.eventDataType == Enums.ModelData.EventDataType.Digital) {
                                pickableFeatureForEventType = pickableFeatureForEventType && f.isAnalog
                            }
                            else{
                                pickableFeatureForEventType = pickableFeatureForEventType &&
                                                             f.eventDataType === eventDataType
                            }
                        } else {
                            pickableFeatureForEventType = (
                                f.eventType === Enums.ModelData.EventType.Input && f.isAccessory &&
                                f.eventDataType === eventDataType
                            );
                        }
                    
                    break;
                case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                    pickableFeatureForEventType = (
                        f.eventType === Enums.ModelData.EventType.Output && f.isAccessory &&
                        f.eventDataType === eventDataType
                    );
                    break;
            }

            return pickableFeatureForEventType;
        });
    }

    /**
     * Handle then user select box model.
     */
    onBoxModelChanged(selectedBoxModel) {
        // Track user changes.
        this.hasChangeBoxModel(true);

        // If selected pacakge is undefined then clear all other datasource.
        this.inputEvents.removeAll();
        this.outputEvents.removeAll();
        this.inputAccessories.removeAll();
        this.outputAccessories.removeAll();
        
        // Perform update datatable for input/output events and accessories.
        if (selectedBoxModel) {
            var boxModels = _.filter(this.availableBoxModels, (bm) => {
                return bm.id === selectedBoxModel.value;
            });

            if (!_.isEmpty(boxModels)) {
                var boxModel = boxModels[0];
                boxModel.packageType.items.forEach((boxPackageItem) => {
                    var featureModel = new BoxTemplateFeatureModel(
                        boxPackageItem.id,
                        boxPackageItem.name,
                        this.filterFeatureByEventType(this.availableFeatures, boxPackageItem.eventType, boxPackageItem.eventDataType),
                        null,
                        null,
                        Enums.InfoStatus.Add,
                        boxPackageItem.eventDataType,
                        boxPackageItem.eventDataTypeDisplayName
                    );

                    switch (boxPackageItem.eventType) {
                        case Enums.ModelData.BoxFeatureEventType.InputEvent:
                            this.inputEvents.push(featureModel);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                            this.outputEvents.push(featureModel);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                            this.inputAccessories.push(featureModel);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                            this.outputAccessories.push(featureModel);
                            break;
                    }

                });
                
            }
        }
    }

    checkShowAnaloConvert(eventModelDataType, eventDataType, isSupportAnalog) {
        return eventModelDataType == Enums.ModelData.EventDataType.Analog &&
            eventDataType == Enums.ModelData.EventDataType.Digital &&
            isSupportAnalog
    }
    findErrorSupportAnalog(items){
        return _.find(items,(item) => { return item.errorAnalogVal});
    }
}

export default {
    viewModel: ScreenBase.createFactory(ManageBoxTemplateScreen),
    template: templateMarkup
};