import ko from "knockout";

export default class FeatureModel {
    constructor(id, name, features = [], selectedFeature = "", note = "") {
        this.id = id;
        this.name = name;
        this.features = features;
        this.selectedFeature = selectedFeature;
        this.note = "";
        this.error = "";
    }
}