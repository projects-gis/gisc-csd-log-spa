﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestDMSModel from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSModel";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import WebRequestDMSConfiguration from "../../../../../../app/frameworks/data/apitrackingcore/WebRequestDMSConfiguration";


class AssetDMSModelManageScreen extends ScreenBase {

    /**
     * Creates an instance of UserListScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Small;
        this.modelId = this.ensureNonObservable(params.id,null);
        this.validationModel = ko.validatedObservable({});

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("DMS Model Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("DMS Model Update")());
                break;
        }

        this.code = ko.observable();
        this.model = ko.observable();
        this.description = ko.observable();

        this.brandOptions = ko.observableArray([]);
        this.selectedBrand = ko.observable();

        this.configOptions = ko.observableArray([]);
        this.selectedConfig = ko.observable();

        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.selectedEnable = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", true));
    }


    get webRequestDMSModel() {
        return WebRequestDMSModel.getInstance();
    }

    get webRequestDMSConfiguration() {
        return WebRequestDMSConfiguration.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var dfdDMSModel = null;
        var dfdBrand = this.webRequestDMSModel.getDMSBrand();
        var dfdConfig = this.webRequestDMSConfiguration.listDMSConfigSummary();
        if (this.mode == Screen.SCREEN_MODE_UPDATE && this.modelId) {
            dfdDMSModel = this.webRequestDMSModel.getDMSModel(this.modelId);
        }
        $.when(dfdDMSModel,dfdBrand,dfdConfig).done((r,r2,r3) => {
            this.brandOptions(r2);
            this.configOptions(r3.items);
            if (r) {
                this.code(r.code);
                this.model(r.model);
                this.description(r.description);

                this.selectedEnable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", r.enable));
                this.selectedBrand(ScreenHelper.findOptionByProperty(this.brandOptions, "id", r.brandId));
                this.selectedConfig(ScreenHelper.findOptionByProperty(this.configOptions, "id", r.dmsConfigurationId));
            }

            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    setupExtend() {
        this.selectedBrand.extend({ required: true });
        this.model.extend({ required: true });
        this.selectedEnable.extend({ required: true });
        this.selectedConfig.extend({ required: true });

        this.code.extend({ trackChange: true });
        this.selectedBrand.extend({ trackChange: true });
        this.model.extend({ trackChange: true });
        this.selectedEnable.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.selectedConfig.extend({ trackChange: true });


        this.code.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.validationModel({
            code: this.code,
            brand: this.selectedBrand,
            model: this.model,
            enable: this.selectedEnable,
            config: this.selectedConfig
        });
    }

    onUnload() { }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode === "update" && WebConfig.userSession.hasPermission(Constants.Permission.DMSModelDelete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var info = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestDMSModel.createDMSModel(info, true)
                        .done((vehicleModel) => {
                            this.publishMessage("bo-asset-dms-models-changed", vehicleModel.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestDMSModel.updateDMSModel(info)
                        .done(() => {
                            this.publishMessage("bo-asset-dms-models-changed", this.modelId);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                    break;
            }
        }
    }


    generateModel() {
        let filterModel = {
            code: this.code(),
            brandId: (this.selectedBrand()) ? this.selectedBrand().id : 0,
            model: this.model(),
            enable: (this.selectedEnable()) ? this.selectedEnable().value : null,
            dmsConfigurationId: this.selectedConfig() ? this.selectedConfig().id : 0,
            description: this.description(),
            id: this.modelId
        };
        return filterModel;
    }



    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M240")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestDMSModel.deleteDMSModel(this.modelId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("bo-asset-dms-models-changed");
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;

        }

    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetDMSModelManageScreen),
    template: templateMarkup
};