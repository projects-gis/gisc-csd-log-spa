﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestBoxModel from "../../../../../app/frameworks/data/apitrackingcore/webrequestBoxModel";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class BoxModelListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        // Blade configuration.
        this.bladeTitle(this.i18n("Assets_BoxModels")());
        this.bladeSize = BladeSize.Medium;

        // Screen properties.
        this.boxModels = ko.observableArray();
        this.selectedBoxModel = ko.observable();
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 0, "asc" ]]);

        // Service properties.
        this.webRequestBoxModel = WebRequestBoxModel.getInstance();
        this.defaultBoxModelFilter = {
            sortingColumns: DefaultSorting.BoxModel
        };

        // Subscribe for aggregator when user create/update/delete box model.
        this.subscribeMessage("bo-boxmodel-changed", (boxModelId) => {
            this.isBusy(true);
            this.webRequestBoxModel.listBoxModelSummary(this.defaultBoxModelFilter)
            .done((response) => {
                // Update box model datasource and refresh recent UI.
                this.boxModels.replaceAll(response.items);
                this.recentChangedRowIds.replaceAll([boxModelId]);
            })
            .fail((e)=> {
                this.handleError(e);
            })
            .always(()=>{
                this.isBusy(false);
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // Do not load web request when blade restore.
        if (!isFirstLoad) {
            return;
        }

        // Loading availble box model from service and return deferred object.
        var dfd = $.Deferred();
        this.webRequestBoxModel.listBoxModelSummary(this.defaultBoxModelFilter).done((response) => {
            // Update box model datasource.
            this.boxModels(response.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBoxModel(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxModel)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("bo-asset-box-model-manage", { mode: "create" });
                break;
        }

        // Clear selected box model on table.
        this.selectedBoxModel(null);

        // Clear recent changed.
        this.recentChangedRowIds.removeAll();
    }

    /**
     * Handle when user click to each box model.
     * 
     * @param {any} boxModel
     */
    onSelectingRow(boxModel) {
        if(boxModel && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxModel)) {
            // When user has permission to update then open update screen.
            return this.navigate("bo-asset-box-model-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: boxModel.id });
        }
        return false;
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxModelListScreen),
    template: templateMarkup
};