﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {Constants, EntityAssociation, Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestBoxModel from "../../../../../app/frameworks/data/apitrackingcore/webrequestBoxModel";
import WebRequestBoxPackageType from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxPackageType";

/**
 * Manage Box Model Screen.
 * 
 * @class ManageBoxModelScreen
 * @extends {ScreenBase}
 */
class ManageBoxModelScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.uploadUrl = WebConfig.appSettings.uploadUrl;
        this.mode = params.mode;
        this.enableUpdatePackageType = ko.observable(true);
        this.id = this.ensureNonObservable(params.id, -1);
        this.pictureMimeTypes = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();
        this.picture = ko.observable(null);
        this.picturePreviewUrl = ko.pureComputed(this.picturePreviewUrlCompute, this);
        this.selectedPictureFile = ko.observable(null);
        this.selectedPictureFileName = ko.observable(null);
        this.name = ko.observable("");
        this.brand = ko.observable("");
        this.dltModelId = ko.observable("");
        this.selectedPackageType = ko.observable();
        this.selectedPackageTypeSubscribe = this.selectedPackageType.subscribe(this.onPackageTypeChanged, this);
        this.packageTypeDataSource = ko.observable();
        this.minDeviceBatteryLevel = ko.observable();
        this.maxDeviceBatteryLevel = ko.observable();
        this.description = ko.observable("");
        this.attachmentMimeTypes = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png"),
            Utility.getMIMEType("gif"),
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx"),
            Utility.getMIMEType("pdf"),
            Utility.getMIMEType("doc"),
            Utility.getMIMEType("docx"),
            Utility.getMIMEType("ppt"),
            Utility.getMIMEType("pptx"),
            Utility.getMIMEType("txt")
        ].join();
        this.attachment = ko.observable(null);
        this.selectedAttachmentFile = ko.observable(null);
        this.selectedAttachmentFileName = ko.observable(null);
        this.selectedAttachmentFileUrl = ko.pureComputed(this.selectedAttachmentFileUrlCompute, this);
        this.validationModel = ko.validatedObservable({});

        // Temporary items.
        this.orignalPackageTypes = null;
        this.inputEvents = ko.observableArray();
        this.outputEvents = ko.observableArray();
        this.inputAccessories = ko.observableArray();
        this.outputAccessories = ko.observableArray();

        // Service properties.
        this.webRequestBoxModel = WebRequestBoxModel.getInstance();
        this.webRequestBoxPackageType = WebRequestBoxPackageType.getInstance();

        // Set page title.
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Assets_CreateBoxModel")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateBoxModel")());
                this.enableUpdatePackageType(false);
                break;
        }
    }
    /**
     * Calculate dynamic preview picture.
     * 
     * @returns {string} preview url.
     */
    picturePreviewUrlCompute() {
        var preivewPictureUrl = this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
        var currentPicture = this.picture();
        if (currentPicture && currentPicture.infoStatus !== Enums.InfoStatus.Delete) {
            preivewPictureUrl = currentPicture.fileUrl;
        }

        return preivewPictureUrl;
    }
    /**
     * 
     * Calculate dynamic attachment hyperlink.
     */
    selectedAttachmentFileUrlCompute() {
        var attachmentUrl = "";
        var currentAttachment = this.attachment();

        if (currentAttachment && currentAttachment.infoStatus !== Enums.InfoStatus.Delete) {
            attachmentUrl = currentAttachment.fileUrl;
        }
        //console.log('calcuate attachment url', attachmentUrl);
        return attachmentUrl;
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var listPackageTypeFilter = {
            includeAssociationNames: [
                EntityAssociation.BoxPackageType.Items
            ]
        };
        this.webRequestBoxPackageType.listBoxPackageType(listPackageTypeFilter)
        .done((response) => {
            // Prepare package type datasource.
            this.orignalPackageTypes = response.items;
            var packageTypeOptions = ScreenHelper.createGenericOptionsObservableArray(response.items, "name", "id");
            this.packageTypeDataSource(packageTypeOptions());

            // Loading user when update mode.
            switch(this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    // No need to loadd extra service, relase loader.
                    dfd.resolve();
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    // Load current box model data.
                    this.webRequestBoxModel.getBoxModel(this.id)
                    .done((response)=>{
                        // Popuplate user info to page then relase loader.
                        this.picture(response.image);
                        if(response.image) {
                            this.selectedPictureFileName(response.image.fileName);
                        }
                        this.name(response.name);
                        this.brand(response.brand);
                        this.dltModelId(Utility.padDigits(response.dltModelId, 4));
                        this.selectedPackageType(ScreenHelper.findOptionByProperty(this.packageTypeDataSource, "value", response.packageTypeId));
                        this.minDeviceBatteryLevel(response.minDeviceBatteryLevel);
                        this.maxDeviceBatteryLevel(response.maxDeviceBatteryLevel);
                        this.description(response.description);
                        this.attachment(response.attachment);
                        if(response.attachment) {
                            this.selectedAttachmentFileName(response.attachment.fileName);
                        }
                        dfd.resolve();
                    })
                    .fail((e)=>{
                        dfd.reject(e);
                    });
                    break;
            }
        })
        .fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * Adding trackchange and validation to properties.
     */
    setupExtend() {
        // Setup trackchange properties.
        this.picture.extend({
            trackChange: true
        });
        this.name.extend({
            trackChange: true
        });
        this.brand.extend({
            trackChange: true
        });
        this.dltModelId.extend({
            trackChange: true
        });
        this.selectedPackageType.extend({
            trackChange: true
        });
        this.minDeviceBatteryLevel.extend({
            trackChange: true
        });
        this.maxDeviceBatteryLevel.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.attachment.extend({
            trackChange: true
        });

        // Setup validation properties.
        this.selectedPictureFile.extend({
            fileExtension: ['JPG', 'JPEG', 'PNG'],
            fileSize: WebConfig.appSettings.maximumUploadSize
        });
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.brand.extend({
            required: true
        });
        this.dltModelId.extend({
            required: true
        });
        this.selectedPackageType.extend({
            required: true
        });
        this.selectedAttachmentFile.extend({
            fileExtension: ['JPG', 'JPEG', 'PNG', 'GIF', 'XLS', 'XLSX', 'PDF', 'DOC', 'DOCX', 'PPT', 'PPTX', 'TXT'],
            fileSize: WebConfig.appSettings.maximumUploadSize
        });

        // Compose validation model.
        this.validationModel({
            selectedPictureFile: this.selectedPictureFile,
            name: this.name,
            brand: this.brand,
            dltModelId: this.dltModelId,
            selectedPackageType: this.selectedPackageType,
            selectedAttachmentFile: this.selectedAttachmentFile
        });
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.CreateBoxModel)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
            case Screen.SCREEN_MODE_UPDATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxModel)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
        }
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_UPDATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxModel)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
                break;
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch(sender.id) {
            case "actSave":
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                // Mark start of long operation
                this.isBusy(true);

                // Populate model from screen.
                var boxModel = this.generateModel();

                // Calling service for save/update box model.
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.webRequestBoxModel.createBoxModel(boxModel, true)
                        .done((boxModel) => {
                            this.publishMessage("bo-boxmodel-changed", boxModel.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBoxModel.updateBoxModel(boxModel)
                        .done(() => {
                            this.publishMessage("bo-boxmodel-changed", this.id);
                            this.close(true);
                        })
                        .fail((e) => {
                            this.handleError(e);
                        })
                        .always(() => {
                            this.isBusy(false);
                        });
                        break;
                }
        }
    }
    /**
     * 
     * Prepare AJAX model from page to REST service.
     */
    generateModel () {
        var model = {
            id: this.id,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            image: this.picture(),
            name: this.name(),
            brand: this.brand(),
            dltModelId: this.dltModelId(),
            minDeviceBatteryLevel: this.minDeviceBatteryLevel(),
            maxDeviceBatteryLevel: this.maxDeviceBatteryLevel(),
            description: this.description(),
            packageTypeId: this.selectedPackageType().value,
            attachment: this.attachment()
        };

        return model;
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M112")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestBoxModel.deleteBoxModel(this.id)
                            .done(()=>{
                                this.publishMessage("bo-boxmodel-changed", this.id);
                                this.close(true);
                            })
                            .fail((e)=>{
                                this.handleError(e);
                            })
                            .always(()=>{
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }
    /**
     * Handle when user change package type.
     */
    onPackageTypeChanged(selectedPackage) {
        var inputEventsData = [];
        var outputEventsData = [];
        var inputAccessoriesData = [];
        var outputAccessoriesData = [];

        // Perform update datatable for input/output events and accessories.
        if(selectedPackage) {
            var matchPackageTypes = _.filter(this.orignalPackageTypes, (packageType) => {
                return packageType.id === selectedPackage.value;
            });

            if(!_.isEmpty(matchPackageTypes)) {
                var packageType = matchPackageTypes[0];
                packageType.items.forEach((boxPackageType) => {
                    switch(boxPackageType.eventType) {
                        case Enums.ModelData.BoxFeatureEventType.InputEvent:
                            inputEventsData.push(boxPackageType);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                            outputEventsData.push(boxPackageType);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                            inputAccessoriesData.push(boxPackageType);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                            outputAccessoriesData.push(boxPackageType);
                            break;
                    }
                });
            }
        }

        // Update all tables with latest datasource.
        this.inputEvents.replaceAll(inputEventsData);
        this.outputEvents.replaceAll(outputEventsData);
        this.inputAccessories.replaceAll(inputAccessoriesData);
        this.outputAccessories.replaceAll(outputAccessoriesData);
    }
    /**
     * Handle when user upload picture.
     * 
     * @param {any} data
     */
    onPictureUploadSuccess(data) {
        if(data && data.length > 0){
            var newPicture = data[0];
            var currentPicture = this.picture();

            if (currentPicture) {
                // For existing image, we update info status or added.
                switch (currentPicture.infoStatus) {
                    case Enums.InfoStatus.Add:
                        newPicture.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        newPicture.infoStatus = Enums.InfoStatus.Update;
                        newPicture.id = currentPicture.id;
                        break;
                }
            }
            else {
                // For new image, we set it as added status.
                newPicture.infoStatus = Enums.InfoStatus.Add;
            }

            this.picture(newPicture);
        }
    }
    /**
     * Handle when picture upload fail.
     * 
     * @param {any} e
     */
    onPictureUploadFail(e) {
        this.handleError(e);
    }
    /**
     * Handle when remove picture of box model.
     */
    removePicture() {
        if(!_.isNil(this.picture())) {
            switch(this.picture().infoStatus) {
                case Enums.InfoStatus.Add:
                    // Remove temporary uploaded picture.
                    this.picture(null);
                    break;
                default:
                    // Set delete status to existing picture.
                    this.picture().infoStatus = Enums.InfoStatus.Delete;
                    // Force ko change from internal status was updated.
                    this.picture.valueHasMutated();
                    break;
            }
        }

        // Clear picture file upload state.
        this.selectedPictureFile(null);
        this.selectedPictureFileName(null);
    }
    /**
     * Handle when box model's attachment upload is success.
     * 
     * @param {any} data
     * 
     * @memberOf ManageBoxModelScreen
     */
    onAttachmentUploadSuccess(data) {
        if(data && data.length > 0){
            var newAttachment = data[0];
            var currentAttachment = this.attachment();

            if (currentAttachment) {
                // For existing image, we update info status or added.
                switch (currentAttachment.infoStatus) {
                    case Enums.InfoStatus.Add:
                        newAttachment.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        newAttachment.infoStatus = Enums.InfoStatus.Update;
                        newAttachment.id = currentAttachment.id;
                        break;
                }
            }
            else {
                // For new image, we set it as added status.
                newAttachment.infoStatus = Enums.InfoStatus.Add;
            }

            this.attachment(newAttachment);
        }
    }
    /**
     * Handle when upload box model's attachment fail.
     * 
     * @param {any} error
     */
    onAttachmentUploadFail(error) {
        this.handleError(e);
    }
    /**
     * Handle when remove box model's attachment.
     */
    removeAttachment() {
        if(this.attachment()) {
            switch(this.attachment().infoStatus) {
                case Enums.InfoStatus.Add:
                    // Remove temporary uploaded picture.
                    this.attachment(null);
                    break;
                default:
                    // Set delete status to existing picture.
                    this.attachment().infoStatus = Enums.InfoStatus.Delete;
                    this.attachment.valueHasMutated();
                    break;
            }
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(ManageBoxModelScreen),
    template: templateMarkup
};