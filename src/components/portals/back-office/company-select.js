import ko from "knockout";
import templateMarkup from "text!./company-select.html";
import ScreenBase from "../screenbase";
import WebRequestCompany from "../../../app/frameworks/data/apicore/webRequestCompany";
import Utility from "../../../app/frameworks/core/utility";
import WebConfig from "../../../app/frameworks/configuration/webConfiguration";
import {Enums} from "../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../../app/frameworks/constant/bladeMargin";


/**
 * User Menu > User Company Portal
 * 
 * @class CompanySelectScreen
 * @extends {ScreenBase}
 */
class CompanySelectScreen extends ScreenBase {

    /**
     * Creates an instance of CompanySelectScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("User_SelectCompany")());
        this.bladeSize = BladeSize.Medium;
        // this.bladeMargin = BladeMargin.Narrow;

        this.companies = ko.observableArray();
        this.filterText = ko.observable();
        
        // this.selectedCompanyIndex = ko.observable();
        this.selectedCompany = ko.observable(null);

        this.order = ko.observable([[ 0, "asc" ]]);
        this.recentChangedRowIds = ko.observableArray([]);

        this.actOK = this.createAction("actOK", this.i18n("Common_OK")(), "default", true, false);
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        // Create Deferred object then resolve when the webRequest done
        var dfd = $.Deferred();

        var filter = {
            displayStart: 0, // Always use first page.
            displayLength: null, // Used for auto change portal for only one company id.
            includeCount: false, // Used for detect total of accessible companies.
            sortingColumns: [{
                column: Enums.SortingColumnName.Name,
                direction: Enums.SortingDirection.Ascending
            }]
        };
        this.webRequestCompany.listCompanySummary(filter).done((companies) => {
            this.companies(companies.items);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.actOK);
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands){}
    /**
     * @lifecycle Handle when button on ActionBar is clicked
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        switch(sender.id) {
            case "actOK":
                if(this.selectedCompany() !== null) {
                    // Open user portal in new tab with differrent url patterns.
                    var portalUrl = this.resolveUrl(WebConfig.appSettings.companyWorkspaceUrl);
                    // Replace company id parameters to URL.
                    portalUrl = Utility.stringFormat(portalUrl, this.selectedCompany().id);
                    window.open(portalUrl, "GISWorkspaceMode");
                    this.close(true);
                }
                break;
        }
    }
    /**
     * @lifecycle Handle when button on CommandBar is clicked
     * @param {any} sender
     */
    onCommandClick(sender) { }

    /**
     * Notify from list control.
     * @param {any} company
     */
    onCompanySelected(company) {
        if(company !== null) {
            this.actOK.enable(true);
            return Utility.emptyDeferred();
        }
        return false;
    }
}

export default {
    viewModel: ScreenBase.createFactory(CompanySelectScreen),
    template: templateMarkup
};