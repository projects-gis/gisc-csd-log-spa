import ko from "knockout";
import ComponentBase from "../componentBase";
import Navigation from "../controls/gisc-chrome/shell/navigation";
import WidgetNavigation from "../controls/gisc-chrome/shell/widgetNavigation";
import Logger from "../../app/frameworks/core/logger";
import EventAggregator from "../../app/frameworks/core/eventAggregator";
import GC from "../../app/frameworks/core/gc";
import Utility from "../../app/frameworks/core/utility";
import {Router} from "../../app/router";

/**
 * Base class for all screen that render under Blade
 * 
 * @class ScreenBase
 * @extends {ComponentBase}
 */
class ScreenBaseWidget extends ComponentBase {

    /**
     * Creates an instance of ScreenBase.
     * @public
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.title = ko.observable();
        this.navigationService = Navigation.getInstance();
        this.widgetService = WidgetNavigation.getInstance();
        this.widgetInstance = null; // Inject via on model creating.
        this.isBusy = ko.observable(false); // Inject via on model creating.
        // Internal members.
        this._eventAggregator = new EventAggregator();
    }

    /**
     * Handle Track user page with analytics.
     */
    trackAnalyticsPageView() {
        // Track google analytics for first load only.
        if(this.isFirstLoad()) {
            ga("send", "pageview", "widget", {
                title: this.title()
            });
        }
    }

    /**
     * Handle when user clicks custom action.
     * 
     * @param {any} e 
     */
    onCustomActionClick(e) {}

    /**
     * Life cycle: trigger when DOM is redering to page.
     * @private
     * @returns
     */
    onViewAttach() {
        var task = null;

        try {
            var self = this;

            // Perform analytics tracking.
            self.trackAnalyticsPageView();

            // Geting deferred from screen loading.
            task = super.onViewAttach();

            if (task && task.state) {
                // Using promise so waiting sucess from view model response.
                task.done((data) => {
                    //self.buildExtendRules();
                    //self.buildButtons();
                }).fail((data) => {
                    self.throwError(0, "Cannot process view model onLoad method.");
                });
            } else {
                //self.buildExtendRules();
                //self.buildButtons();
            }
        } catch(e) {
            Logger.error(e);
        }

        return task;
    }

    // /**
    //  * Life cycle: trigger when adjacent child (only one level) is closed
    //  * @public
    //  */
    // onChildScreenClosed() {}

    // /**
    //  * Life cycle: for child screen to build available command button collection.
    //  * @public
    //  * @param {any} allCommands
    //  */
    // buildCommandBar(allCommands) {}

    // /**
    //  * Life cycle: for child screen to build available action button collection.
    //  * @public
    //  * @param {any} allButtons
    //  */
    // buildActionBar(allButtons) {}

    /**
     * Navigate to target moduleName on a new Window
     * @public
     * @param {any} moduleName
     * @param {any} [options=null]
     */
    widget(componentName, options = null, widgetOptions = null){
        this.widgetService.widget(componentName, options, widgetOptions);
    }

    /**
     * Guard method to check if the Screen can be closed.
     * 
     * @public
     * @returns {boolean}
     */
    canClose() {
        return !this.isDirty();
    }

    /**
     * Close the current blade.
     * @public
     */
    close(forceClose = false) {
        // Use Journey.tryDeactivate instead of deactivate, to trigger guard function.
        if (this._blade) {
            var journey = this.navigationService.activeItem();
            if(!forceClose) {
                journey.tryDeactivate(this._blade, true);
            } else {
                journey.deactivate(this._blade, true);
            }
        }
    }

    /**
     * Dispatch event to control directly, used this with supported controls.
     * 
     * @param {any} controlId
     * @param {any} eventName
     * @param {any} [message=null]
     */
    dispatchEvent(controlId, eventName, message = null) {
        this._eventAggregator.publish(controlId, { 
            eventName: eventName,
            message: message
        });
    }

    _onViewModelCreating(widget) {
        this.widgetInstance = widget;
        this.isBusy = widget.isBusy;
    }

    /**
     * Internal event when view model is disposing
     * @private
     */
    _onViewModelDisposing() {
        // Dispose all observing subject in EventAggregator
        Logger.log("Widget disposing", this.id);
        this._eventAggregator.destroy();

        // Using GC to cleanup if exists.
        GC.dispose(this);
    }

    displayError(msg){
        Logger.error(msg);
    }

    /**
     * Display error message on ajax request from response error.
     * The expect responseError structure:
     * {
     *      message: "There is an existing item with the same value.",
     *      code: "errorCode",
     *      dataDetails: [
     *          "Name", "Code"
     *      ]
     * }
     * This method will call displayError() internally with appropiate errorCode.
     * 
     * @param {any} ajaxError a jQuery error object from Deferred.
     */
    handleError(ajaxError) {
        if(!_.isNil(ajaxError) && !_.isNil(ajaxError.responseJSON)) {
            var responseError = ajaxError.responseJSON;

            // Check if code is specific message then handle specific error, else call displayError with formatted message.
            if(!_.isNil(responseError.code) && _.indexOf(["M010", "M065", "M084"], responseError.code) > -1) {
                if(!_.isNil(responseError.dataDetails) && (responseError.dataDetails.length > 0)) {
                    // M084 format message with 2 params
                    if (responseError.code === "M084") {
                        var formattedMsg = Utility.stringFormat(responseError.message, responseError.dataDetails[0], responseError.dataDetails[1]);
                        this.displayError(formattedMsg);
                    }
                    // M010/M065 display inline error message below the field
                    else {
                        // Loop through all prop in this VM that has "serverField" attached
                        for(let key in this) {
                            if (this.hasOwnProperty(key) && 
                                ko.isObservable(this[key]) &&
                                !_.isNil(this[key].serverField)) {
                                
                                // Found serverField on observable, check with dataDetails
                                var serverField = this[key].serverField;
                                
                                // Make sure dataDetails is array
                                var dataDetails = responseError.dataDetails;
                                if(typeof dataDetails === "string") {
                                    dataDetails = [dataDetails];
                                }

                                var matchObj = _.find(dataDetails, function(target) { 
                                    return target === serverField;
                                });
                                if(!_.isNil(matchObj)) {
                                    // Found match, trigger error inline on target field.
                                    var serverMessage = !_.isNil(this[key].serverMessage) ? this[key].serverMessage : "Server validation fail.";
                                    
                                    this[key].setError(serverMessage);
                                    this[key].isModified(true);
                                }
                            }
                        }
                    }
                }
            }
            else {
                // Other error code, call displayError
                var formattedErrorMsg = Utility.stringFormat(responseError.message, responseError.dataDetails);
                this.displayError(formattedErrorMsg);
            }
        }
    }

    /**
     * Subscribe to Journal based communication.
     * The callback has value of updated message.
     * @public
     * @param {any} messageId
     * @param {any} callback
     */
    subscribeMessage(messageId, callback) {
        // Communication between blades in the same journey require 
        // a unique topic - this comes from combination of journey's id AND topic
        let activeJourney = this.navigationService.activeItem();
        let journalMessageId = activeJourney.id + "_" + messageId;
        this._eventAggregator.subscribe(journalMessageId, callback);
    }

    /**
     * Publish message to Journal based communication. 
     * @public
     * @param {any} messageId
     * @param {any} messageValue
     */
    publishMessage(messageId, messageValue) {
        let activeJourney = this.navigationService.activeItem();
        let journalMessageId = activeJourney.id + "_" + messageId;
        this._eventAggregator.publish(journalMessageId, messageValue);
    }

    /**
     * Verify current blade is active in journey.
     * @return {boolean}
     */
    isActive() {
        return true;
    }

    /**
     * Minimize all blades in current journey.
     */
    minimizeAll() {
        this.navigationService.minimizeAll(true);
    }

    /**
     * Dispose object release memory.
     */
    onUnload(){
        this._onViewModelDisposing();
    }

    /**
     * Factory method to create View Model
     * @static
     * @public
     * @param {Clazz} Class of target View Model, derive from ScreenBase.
     * @returns ViewModel of target Clazz
     */
    static createFactory(viewModelClass) {
        var factoryObj = {
            createViewModel: function(params, componentInfo) {
                var widget = params.widget;

                if (!widget.componentInstance()) {

                    let vmParameter = params.componentOptions;
                    if(!vmParameter) {
                        vmParameter = {};
                    }

                    let vm = new viewModelClass(vmParameter);
                    widget.componentInstance(vm);
                    vm._onViewModelCreating(widget);
                }

                return widget.componentInstance();
            }
        };
        return factoryObj;
    }
}

export default ScreenBaseWidget;