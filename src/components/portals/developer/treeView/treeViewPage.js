import ko from "knockout";
import templateMarkup from "text!./treeViewPage.html";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ScreenBase from "../../screenbase";
class TreeViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        window.tree = this;
        // Properties
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Treeview (gisc-ui-treeview)");
        this.bladeDescription("src/components/portals/developer/treeview");

        this.selected1 = ko.observableArray();
        this.selected2 = ko.observableArray();
        this.selected3 = ko.observableArray();
        this.selected4 = ko.observableArray();

        this.collapsedNodes1 = ko.observableArray();
        this.collapsedNodes2 = ko.observableArray();
        this.collapsedNodes3 = ko.observableArray();
        this.collapsedNodes4 = ko.observableArray();

        this.items1 = ko.observableArray();
        this.items2 = ko.observableArray();
        this.items3 = ko.observableArray();
        this.items4 = ko.observableArray();

        this.disableCollections = ko.observableArray([5,9]);

        this.result1 = ko.pureComputed(() => {
            return this.selected1().length > 0 ? this.selected1() : "No items selected";
        });

        this.result2 = ko.pureComputed(() => {
            return this.selected2().length > 0 ? this.selected2() : "No items selected";
        });

        this.result3 = ko.pureComputed(() => {
            return this.selected3().length > 0 ? this.selected3() : "No items selected";
        });

        this.result4 = ko.pureComputed(() => {
            return this.selected4().length > 0 ? this.selected4() : "No items selected";
        });
    }
    
    /**
     * For custom build node.
     * 
     * @param {any} node
     * 
     * @memberOf TreeViewScreen
     */
    onBuildNode(node) {
        node.text = node.text + " (" + node.rawData.code + ")";
        node.li_attr.title = node.text;
    }
    
    onLoad(isFirstLoad) {

        var mockData = [{
            id: 1,
            code: "A001",
            name: "Head Office",
            parentNode: null,
            childNodes: [{
                id: 3,
                code: "B001",
                name: "Central",
                parentNode: 1,
                childNodes: [{
                    id: 5,
                    name: "Bangkok",
                    code: "C001",
                    parentNode: 3,
                    childNodes: []
                }, {
                    id: 6,
                    name: "Nonthaburi",
                    code: "C001",
                    parentNode: 3,
                    childNodes: []
                }]
            }, {
                id: 4,
                name: "North",
                code: "B002",
                parentNode: 1,
                childNodes: []
            }]
        },{
            id: 2,
            name: "Regional Office",
            code: "A001",
            parentNode: null,
            childNodes: [{
                id: 7,
                code: "B001",
                name: "Central",
                parentNode: 2,
                childNodes: [{
                    id: 9,
                    name: "Bangkok",
                    code: "C001",
                    parentNode: 7,
                    childNodes: []
                }, {
                    id: 10,
                    name: "Nonthaburi",
                    code: "C001",
                    parentNode: 7,
                    childNodes: []
                }]
            }, {
                id: 8,
                name: "North",
                code: "B002",
                parentNode: 2,
                childNodes: []
            }]
        },{
            id: 11,
            name: "Regional Office",
            code: "A001",
            parentNode: null,
            childNodes: [{
                id: 13,
                code: "B001",
                name: "Central",
                parentNode: 11,
                childNodes: [{
                    id: 14,
                    name: "Bangkok",
                    code: "C001",
                    parentNode: 13,
                    childNodes: [{
                        id: 16,
                        name: "Software park ",
                        code: "C001",
                        parentNode: 14,
                        childNodes: [{
                            id: 18,
                            name: "Floor 3",
                            code: "C001",
                            parentNode: 16,
                            childNodes: []
                        },{
                        id: 19,
                            name: "Floor 10",
                            code: "C001",
                            parentNode: 16,
                            childNodes: []
                        }]
                    },{
                        id: 17,
                        name: "Central ChaengwattanaCentral",
                        code: "C001",
                        parentNode: 14,
                        childNodes: []
                    }]
                }, {
                    id: 15,
                    name: "Nonthaburi",
                    code: "C001",
                    parentNode: 13,
                    childNodes: []
                }]
            }, {
                id: 12,
                name: "North",
                code: "B002",
                parentNode: 11,
                childNodes: []
            }]
        }];


        if (isFirstLoad) {
            this.items1(mockData);
            this.items2(mockData);
            this.items3(mockData);
            this.items4(mockData);
        }

    }
    onUnload() {

    }
}

export default {
    viewModel: ScreenBase.createFactory(TreeViewScreen),
    template: templateMarkup
};