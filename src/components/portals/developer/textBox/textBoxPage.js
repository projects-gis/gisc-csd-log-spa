import templateMarkup from "text!./textBoxPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

/**
 * 
 * 
 * @class TextBoxScreen
 * @extends {ScreenBase}
 */
class TextBoxScreen extends ScreenBase {
    /**
     * Creates an instance of TextBoxScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("TextBox (gisc-ui-textbox)");
        this.bladeDescription("src/components/portals/developer/textBox");

        //Value for view
        this.example = ko.observable("Example");
        this.ex1 = ko.observable("Example1");
        this.ex2 = ko.observable("Example2");
        this.maxlength = ko.observable();
        this.pwd = ko.observable("pwd1234");
        this.numeric = ko.observable();
        this.numeric2 = ko.observable(); //TODO:Remove
        this.alphanumeric = ko.observable();
        this.regex = ko.observable();
        this.isVisible = ko.observable(false);
    }

    onLoad(isFirstLoad){}
    
    toggleIsVisible() {
       this.isVisible(!this.isVisible()); 
    }
}

export default {
    viewModel: ScreenBase.createFactory(TextBoxScreen),
    template: templateMarkup
};