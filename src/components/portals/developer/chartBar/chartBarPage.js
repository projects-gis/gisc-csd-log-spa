import ko from "knockout";
import templateMarkup from "text!./chartBarPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class ChartBarScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Chart (gisc-ui-chartbar)");
        this.bladeDescription("src/components/portals/developer/chartBarPage");

        this.items = [{
                value:2,
                color: "transparent",
            },{
                value: 10,
                color: "#19dd72",
            },{
                value:2,
                color: "transparent",
            },{
                value: 5,
                color: "#ed1c24",
            },{
                value: 5,
                color: "#2e3192",
            }];
    }

    onLoad(){

    }

    onUnload(){}
}

export default {
    viewModel: ScreenBase.createFactory(ChartBarScreen),
    template: templateMarkup
};
