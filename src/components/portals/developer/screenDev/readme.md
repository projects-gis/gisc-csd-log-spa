## Get Started 

Developing a screen in Logistic SPA requires some knowledge on [Knockout][1] However most of the things we used are built on top of Knockout
and other js library. This guide intend to get you started on developing screen in Logistic SPA from start to finish.

## Create a screen

In order to create a screen, you have to perform the following steps:

1. Create View/ViewModel under /src/component/portal.
2. Register ViewModel on /src/components/config.js
3. Register route for screen on /src/app/router.config.js

### 1. Create View/ViewModel

Create new file for View and ViewModel under one of the portals, usually the View and ViewModel use the same name, with suffix
`.html` and `.js` for View and ViewModel respectivly.

You can use snippets to generate View and ViewModel with ease, type the following:
* `gisc-ui-component` on *.html file to scaffolding the View.
* `koScreen` on *.js file to scaffolding the ViewModel.

> Snippets are avialable under /snippets folder, consult [VS Code guide][2] about how to install snippets on your development environment.

View consists of `gisc-ui-xyz` controls and html tags, use Knockout's observable to glue between View and ViewModel.

ViewModel is ES2015 syntax of JavaScript, you can use older syntax (ES5), consult [es6-features.org][3] about the additional features. 

### 2. Register ViewModel

The View/ViewModel pair that you created on step1 are not accessible until it is registered by application registry. 
The registration process is straightforward, open  `/src/components/config.js` and put `name` and `path` of newly created screen.

Example content of config.js

    ...
    group: "back-office-portal-screens",
                items: [
                    // Select Company 
                    { name: "bo-company-select", path: "components/portals/back-office/company-select" },
    ...

The registered component (screen) is `bo-company-select` with path to ViewModel, omit `.js` extensions.

> Under the hood we read this config and register each item as Knockout component, the same config is using with Gulp to create bundle and distributed work product.

### 3. Register Route

After step1 and 2 the application knows there is new screen available, next is tell how to navigate to this screen.

Open `/src/app/router.config.js` and put `url` and `params` for newly created screen.

Example content of router.config.js

    ...
    name: "back-office",
            routes: [
                // Select Company
                { url: 'select-company', params: { page: 'bo-company-select', navLevel: 1 } },
    ...

The registered route's url is `select-company` which target to component name `bo-company-select` from step2.

After you finish all these 3 steps, open browser and put url of the screen, done.

## What's next?

Checkout guide for gisc-ui controls on how to use them on View. Mostly they provide an entry point to bind
observable, which is a property of associated ViewModel.

Checkout guide about screen for specific recipe how to complete a certain task.


[1]: http://knockoutjs.com
[2]: https://code.visualstudio.com/docs/customization/userdefinedsnippets
[3]: http://es6-features.org/#Constants
