import ko from "knockout";
import templateMarkup from "text!./trackChange.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class TrackChangeHowto extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle("Howto: Track Change");
        this.bladeDescription("src/components/portals/developer/screenDev/trackChange");
        this.bladeSize = BladeSize.Medium;

    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TrackChangeHowto),
    template: templateMarkup
};