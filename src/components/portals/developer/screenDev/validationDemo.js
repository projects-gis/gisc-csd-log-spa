import ko from "knockout";
import templateMarkup from "text!./validationDemo.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class ValidationDemo extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle("Demo: Data Validation");
        this.bladeDescription("src/components/portals/developer/screenDev/validationDemo");
        this.bladeSize = BladeSize.Medium;

        this.name = ko.observable("some name");
        this.userName = ko.observable();
        this.email = ko.observable();
        this.numberOnly = ko.observable();
        this.pattern = ko.observable();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }

    setupExtend() {
        // Required field
        this.name.extend({
            required: true
        });

        // Email validation
        this.email.extend({
            email: true
        });

        // Number + min validation
        this.numberOnly.extend({
            number: true,
            min: 0
        });

        // Regex with custom error message.
        this.pattern.extend({ 
            pattern: {
                params: "^[a-z0-9].$",
                message: "This does not match the pattern ^[a-z0-9].$"
            }
        });

        // Required field + server validation
        // params is the expected key from response error object. See WS Core API for details. 
        this.userName.extend({
            required: true,
            serverValidate: {
                params: "UserName",
                message: this.i18n("M010")()
            }
        });

        // Create validation model to be used later when click submit.
        this.validationModel = ko.validatedObservable({
            name: this.name,
            userName: this.userName,
            email: this.email,
            numberOnly: this.numberOnly,
            pattern: this.pattern
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSubmit", "Submit"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actSubmit") {
            this.clearStatusBar();

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Now client validation is completed, go on to form submission
            this.mockWebRequestSubmit(this.userName(), true).done(()=> {
                this.displaySuccess("Submit completed.");
            }).fail((e)=> {
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    mockWebRequestSubmit(userName, isTestInlineMessage = true) {
        // Always reject to demo server validation.
        var delayInSeconds = 0.5 * 1000;
        var dfd = $.Deferred();
        setTimeout(()=> {

            if(isTestInlineMessage) {

                // Test inline message
                if(!_.isNil(userName)) {
                    if(userName === "user1") {
                        // Mock reject message from BL
                        var rejectMessage = {
                            message: "There is an existing item with the same value.",
                            code: "M010",
                            dataDetails: [
                                "UserName",
                                "Code"
                            ]
                        };
                        dfd.reject(rejectMessage);
                    } else {
                        dfd.resolve();
                    }
                } else {
                    // ignore if userName is empty
                    dfd.resolve();
                }

            } else {

                // Test other error message
                var rejectMessage = {
                    message: "Cannot change business unit. {0} is associated with this business unit.",
                    code: "M084",
                    dataDetails: [
                        "Box"
                    ]
                };
                dfd.reject(rejectMessage);
            }

        }, delayInSeconds);
        return dfd;
    }
}

export default {
    viewModel: ScreenBase.createFactory(ValidationDemo),
    template: templateMarkup
};