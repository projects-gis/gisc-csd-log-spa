import ko from "knockout";
import templateMarkup from "text!./buttonPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

/**
 * Sample usage of gisc-ui-button controls
 * 
 * @class ButtonPage
 * @extends {ScreenBase}
 */
class ButtonScreen extends ScreenBase {
    /**
     * Creates an instance of ButtonPage.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Button (gisc-ui-button)");
        this.bladeDescription("src/components/portals/developer/button");
        this.enable = ko.observable(false);
    }
    /**
     * 
     * Test function when button click
     * @param {any} param
     */
    testClick(param) {
        alert(param);
    }
}

export default {
    viewModel: ScreenBase.createFactory(ButtonScreen),
    template: templateMarkup
};