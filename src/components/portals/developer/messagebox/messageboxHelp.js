import ko from "knockout";
import templateMarkup from "text!./messageboxHelp.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

/**
 * Sample usage of gisc-ui-label controls
 * 
 * @class MessageBoxHelpScreen
 * @extends {ScreenBase}
 */
class MessageBoxHelpScreen extends ScreenBase {
    /**
     * Creates an instance of MessageBoxHelpScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Messagebox");
        this.bladeDescription("src/components/portals/developer/messagebox");

        this.responseButton = ko.observable();
    }

    /**
     * Handle on component load.
     * 
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {}

    /**
     * Handle button click.
     * 
     */
    testClick (title, message, type) {
        var self = this;
        this.showMessageBox(title, message, type).done((response) => {
            self.responseButton(response);
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(MessageBoxHelpScreen),
    template: templateMarkup
};