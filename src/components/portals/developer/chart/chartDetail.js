import templateMarkup from "text!./chartDetail.html";
import ScreenBase from "../../screenbase";
import ko from "knockout";

class ChartEndPoint extends ScreenBase {
    constructor(params){
        super(params);

        this.bladeTitle("Selected Item");

        // Passed from previous screen
        this.previousScreenName = params.previousScreenName; //this.ensureObservable();
        this.job = params.job;
    }

    onLoad(isFirstLoad) {
    }
    onUnload(){}
}

export default {
    viewModel: ScreenBase.createFactory(ChartEndPoint),
    template: templateMarkup
};