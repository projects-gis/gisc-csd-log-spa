import ko from "knockout";
import templateMarkup from "text!./imageComboboxPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
/**
 * Sample usage of gisc-ui-image-combobox controls
 * 
 * @class imageComboboxPage
 * @extends {ScreenBase}
 */
class ImageComboboxScreen extends ScreenBase {
    /**
     * Creates an instance of imageComboboxPage.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeTitle("ImageCombobox (gisc-ui-image-combobox)");
        this.bladeDescription("src/components/portals/developer/imageCombobox");
        this.bladeSize = BladeSize.Medium;
        
        this.selectedItem = ko.observable();
       // this.mySelectedValue = ko.observable("Paypal");
     
        this.myData=ko.observableArray([                          
                {image:"../images/ic-calendar.svg",  value:"amex", text:"Amex"},
                {image:"../images/ic-calendar.svg",  value:"Discover", text:"Discover"},
                {image:"../images/ic-calendar.svg", value:"Mastercard", text:"Mastercard"},
                {image:"../images/ic-calendar.svg",  value:"cash", text:"Cash on devlivery"},
                {image:"../images/ic-calendar.svg",  value:"Visa", text:"Visa"},
                {image:"../images/ic-calendar.svg",  value:"Paypal", text:"Paypal"},
                {image:"../images/ic-calendar.svg",  value:"Paypal1", text:"Paypal1"},
                {image:"../images/ic-calendar.svg",  value:"Paypal2", text:"Paypal2"},
                {image:"../images/ic-calendar.svg",  value:"Paypal3", text:"Paypal3"}
        ]);
       
    }
    setNewValue() {
        this.selectedItem({image:"../images/ic-calendar.svg",  value:"Paypal1", text:"Paypal1"});
    }
  
}

export default {
viewModel: ScreenBase.createFactory(ImageComboboxScreen),
    template: templateMarkup
};