import ko from "knockout";
import ScreenBase from "../../screenbase";
import templateMarkup from "text!./readme.html";
import contentMarkdown from "text!./readme.md";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class DataTableReadme extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("DataTable README");
        this.bladeDescription("src/components/portals/developer/datatable/readme");

        this.markdownText = ko.observable(contentMarkdown);
    }
    onLoad(isFirstLoad) {}
}

export default {
    viewModel: ScreenBase.createFactory(DataTableReadme),
    template: templateMarkup
};