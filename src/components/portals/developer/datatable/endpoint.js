import templateMarkup from "text!./endpoint.html";
import ScreenBase from "../../screenbase";
import ko from "knockout";

class DataTableEndPoint extends ScreenBase {
    constructor(params){
        super(params);

        this.bladeTitle("DataTable Output");

        // Passed from previous screen
        this.previousScreenName = params.previousScreenName; //this.ensureObservable();
        this.person = params.person;
    }

    onLoad(isFirstLoad) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableEndPoint),
    template: templateMarkup
};