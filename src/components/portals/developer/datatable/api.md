## Parameters

| Name             | Required | Default Value | Observable | Category |
| ---------------- | -------- | ------------- | ---------- | -------- |
| `id`             | No       | GUID          | No         | Core     |
| `datasource`     | Yes      | None          | Yes (Array)| Core     |
| `columns`        | Yes      | Empty Array   | No         | Core     |
| `order`*         | Yes      | [[0, "asc"]]  | Yes        | Core     |
| `selectedRow`*   | No       | None          | Yes        | Core     |
| `selectedRows`*  | No       | None          | Yes (Array)| Core     |
| `rowId`*         | No       | None          | No         | Core     |
| `removable`*     | No       | null          | No         | Core     |
| `ordering`       | No       | True          | No         | Core     |
| `autoWidth`      | No       | True          | No         | Core     |
| `loadMore`       | No       | False         | No         | Core     |
| `onDatasourceRequest`     | No       | null          | No        | Core    |
| `selectable`     | No       | False         | No         | State    |
| `showFilter`     | No       | False         | No         | State    |
| `showRowSelector`| No       | False         | No         | State    |
| `rowSelectorType`| No       | "multiple"    | No         | State    |
| `filter`*        | No       | None          | Yes        | State    |
| `filterPlaceholder`  | No   | "Filter"      | No         | State    |
| `retainSelectedRowAfterReplaceDataSource` | No       | False          | Yes        | State    |
| `readonly`       | No       | False         | No         | State    |
| `pageSize`     | No       | 20          | Yes        | State    |
| `pageIndex`     | No       | 0          | Yes        | State    |
| `onDataSourceChanged` | No  | None          | No        | Events    |
| `onSelectingRow` | No       | None          | No        | Events    |
| `onLoadMore`     | No       | None          | No        | Events    |
| `serverSide`     | No       | None          | No        | Events    |

__Legends:__
* `order` is not required if `ordering` is false.
* `selectedRow` and `selectedRows` are mutual exclusive.
* `rowId` is not required if there is no data manipulation. 90% of the time you will need it.
* `filter` is not required if `showFilter` is false.
* `removable` is object with structure { visible: boolean }. You can simply set boolean to set removable column, 
or use visible property to do conditional display.

## Parameters Details

### Core Parameters

Core parameters are the basic set of parameters:

* __Required__
    - `datasource` Datasource for DataTable controls, it must be Observable Array of json object. 
    - `columns` Definition of column, define type and data projection.
    - `order` Observable object in form of [order option](https://datatables.net/reference/option/order)
* __Optional__
    - `id` Unique identifier of html element, set to id attribute. If none is specify the controls will generate GUID.
    - `selectedRow` Observable bound to selected record from datasource. It will be updated from controls when row is selected.
    Note that `selectable` must be true in order to allow row selection.
    - `selectedRows` Observable Array bound to selected records from datasource. It will be updated from controls when row is selected.
    Note that `selectable` must be true in order to allow row selection.
    - `removable` is object with structure { visible: boolean }, set to true to add remover for a record.
    - `ordering` boolean, set to true to enable datatable sorting functionality.
    - `autoWidth` boolean, enable or disable automatic column width calculation.
    - `loadMore` boolean, set to true to display load more button at the end of table.
    - `onDatasourceRequest` function, provide jQuery deferred and resolve with (datasource, totalRecord) count after AJAX request complete.

### State Parameters

State parameters determine how DataTable behave, all state parameters are optional:
    
* `selectable` boolean, set to true to allow row selection.
* `showFilter` boolean, set to true to show filter textbox.
* `showRowSelector` boolean, set to true to show row selector.
* `rowSelectorType` enum, possible values are [single, multiple]. Default value is multiple.
* `filter` Observable bound to filter text. Required when `showFilter` is true.
* `filterPlaceholder` place holder text for filter. Default value is "Filter".
* `retainSelectedRowAfterReplaceDataSource` is for retaining selected row, this is used in conjunction with `rowId`.
* `readonly` boolean, set to true to disable row hover style.
* `pageSize` number, use to get/set page size.
* `pageIndex` number, use to get/set page index.

### Event Parameters

Event parameters allow Screen View Model to observe change on DataTable's state with event handler.

* `onDataSourceChanged` event, fire when any object in `datasource` changed.
* `onSelectingRow` event, fire when about to select a row.

#### onDataSourceChanged

This event will trigger when any object in `datasource` changed. Mostly use in conjunction with editable columns like
`checkbox` or `textbox` column.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name' },
        { type: 'checkbox', title: 'Is Enabled', data: 'isEnabled' }
    ],
    order: order,
    rowId: 'id',
    onDataSourceChanged: itemsChanged
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
]);
this.order = ko.observable([[ 0, "asc" ]]);

// Observe changes in datasource when changed come from EditableColumns
this.itemsChanged = (newValue) => { 
    //...
}
```

#### onSelectingRow

This event will trigger when user is about to select a row. It provide an opportunity to cancel the row selection.

__Example:__
``` xml
<!-- View -->
<gisc-ui-datatable params="
    datasource: items,
    columns: [
        { type: 'label', title: 'Name, data: 'name' },
        { type: 'checkbox', title: 'Is Enabled', data: 'isEnabled' }
    ],
    order: order,
    rowId: 'id',
    onSelectingRow: selectingRow
    " />
```
``` js
// View Model
this.items = ko.observableArray([
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
]);
this.order = ko.observable([[ 0, "asc" ]]);

// Determine if selecting action is legal, return false to cancel selecting event.
this.selectingRow = (row) => { 
    if(row.id === 'xyz') return false;
    return true;
}
```

## Dispatch Events

| Name             | Message  | 
| ---------------- | -------- |
| `refresh`        | `data`*  |
| `reset`          | `data`*  |

__Legends:__
* `data` is optional, when send the table will refresh with given data. 

#### Refresh Dispatch Event

Use for refresh datasource of DataTable, but keep other state intact. Example usage:

``` js
// View Model
this.dispatchEvent("table-client-id", "refresh");

var data = [
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
];
this.dispatchEvent("table-client-id", "refresh", data);
``` 

#### Reset Dispatch Event

Use for reset all state of DataTable to the initial. 

``` js
// View Model
this.dispatchEvent("table-client-id", "reset");

var data = [
    { id: 1, name: "Victor", age: 22, isEnabled: true },
    { id: 2, name: "Tiger", age: 33, isEnabled: true },
    { id: 3, name: "Satou", age: 31, isEnabled: false }
];
this.dispatchEvent("table-client-id", "reset", data);
``` 

#### Update Checkbox Select All Dispatch Event

Use for update the checkbox select all of DataTable. 

``` js
// View Model
var data = { columnIndex: 0, checked: false };
this.dispatchEvent("table-client-id", "updateCheckboxSelectAll", data);
``` 