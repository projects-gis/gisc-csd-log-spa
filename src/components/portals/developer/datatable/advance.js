import templateMarkup from "text!./advance.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

class DataTableAdvanceUsage extends ScreenBase {
    constructor(params){
        super(params);
        var self = this;

        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Advance DataTable");
        this.bladeDescription("src/components/portals/developer/datatable/advance");

        // Set datasource to DataTable control
        this.items = ko.observableArray([
            { id: 1, tempId: 4, name: "Victor", position: "Software Engineer", age: 22, isRegistered: true, enable: true },
            { id: 2, tempId: 5, name: "Tiger", position: "System Architect", age: 33, isRegistered: false, enable: true },
            { id: 3, tempId: 6, name: "Satou", position: "Accountant", age: 31, isRegistered: false, enable: false },
            { id: 4, tempId: 7, name: "Kintaro", position: "Accountant", age: 30, isRegistered: true, enable: false }
        ]);
        
        // To preserve state when journey is inactive.
        this.selectedItem = ko.observable(); 
        this.searchText = ko.observable(); // "Test Filter"
        this.order = ko.observable([[ 0, "asc" ]]);

        // Observe changes in DataTable.selectedRow 
        this.selectedItem.subscribe(function(newValue) {
            console.log("Change selected row: ", newValue);
        });

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (user) => {
            // Test ignore second row
            if(user && (user.id !== 2)) {
                return self.navigate("datatableEndPoint", { 
                    previousScreenName: self.bladeTitle(),
                    person: user 
                });
            }
            return false;
        };

        // Demonstrate rendering custom column, see advance.html with column title='custom'
        // You can access the following in this context:
        // - cell data with 'data' param, in this case it is either true or false.
        // - type is jqueryDataTable param, ignore this.
        // - row is TR DOM for this row, you can use jQuery to manipulate it here.
        this.renderCustomColumn = (data, type, row) => {
            if(data) {
                return 'Yes';
            }
            return 'No';
        };

        // Observe changes in datasource when changed come from EditableColumns
        this.itemsChanged = (newValue) => { 
            console.log("adv.js: itemsChanged: ", newValue);
        }

        // Set recent change rows
        this.recentChangedRowIds = ko.observableArray([4]); //ko.observableArray(); 
    }

    onLoad(isFirstLoad) {
    }

    addNewItem() {
        // Simulate add new item to this.items observableArray
        let itemCount = this.items().length;
        let itemId = ++itemCount;
        let itemTempId = itemCount * 2;
        this.items.push({
            id: itemId,
            tempId: itemTempId,
            name: "Item " + itemCount,
            position: "Something",
            age: itemCount,
            isRegistered: false
        });

        // add recent changed state
        this.recentChangedRowIds.replaceAll([itemTempId]);
    }

    refreshTable() {
        this.dispatchEvent("advance-datatable", "refresh");
    }

    refreshTableWithNewDataSet() {
        var newDataSet = [
            { id: 7, tempId: 4, name: "Mary", position: "Biologist", age: 35, isRegistered: true },
            { id: 8, tempId: 5, name: "Bovanof", position: "Clerk", age: 27, isRegistered: true },
            { id: 9, tempId: 6, name: "Kaito", position: "CEO", age: 33, isRegistered: false }
        ];

        this.dispatchEvent("advance-datatable", "refresh", newDataSet);
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableAdvanceUsage),
    template: templateMarkup
};