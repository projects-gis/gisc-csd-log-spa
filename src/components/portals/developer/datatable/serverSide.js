import ko from "knockout";
import templateMarkup from "text!./serverSide.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import Utility from "../../../../app/frameworks/core/utility";

class DataTableServerside extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle("Load More (Serverside)");
        this.bladeDescription("src/components/portals/developer/datatable/serverSide");
        this.bladeSize = BladeSize.Medium;
        this.selectedItem = ko.observable();
        this.searchText = ko.observable();
        this.order = ko.observable([[ 0, "asc" ]]);
        this.pageIndex = ko.observable(0);
        this.pageSize = ko.observable(20);
        this.items = ko.observableArray(Utility.extractParam(params.ds, []));
        this._loadMoreHandler = (pageIndex) => {
            console.log("Screen: on load more = " + pageIndex);
        };
        this.onDatasourceRequest = this.onDatasourceRequest.bind(this);

        // onClick of columnId: 'viewOnMapColumn'
        this.viewMapClick = (data) => {
            alert("Clicked on search icon for " + data.name);
        };
        // Demonstrate rendering custom column, see advance.html with column title='custom'
        // You can access the following in this context:
        // - cell data with 'data' param, in this case it is either true or false.
        // - type is jqueryDataTable param, ignore this.
        // - row is TR DOM for this row, you can use jQuery to manipulate it here.
        this.renderCustomColumn = (data, type, row) => {
            return data + " years";
        };
    }

    onDatasourceRequest() {
        var dfd = $.Deferred();

        this.isBusy(true);
        this.__fetchServer(this.generateFilter()).done((response) => {
            dfd.resolve(response.items, response.totalRecords);
        }).fail((e) => {
            this.handleError(e);
            dfd.fail(e);
        }).always(() => {
            this.isBusy(false);
        });

        return dfd;
    };

    generateFilter() {
        var sortingColumns = [{},{}];
        sortingColumns[0].direction = this.order()[0][1] === "asc" ? 1 : 2 ;

        switch (this.order()[0][0]) {
            case 1:
                sortingColumns[0].column = "id";
                break;
            case 2:
                sortingColumns[0].column = "name";
                break;
            case 3:
                sortingColumns[0].column = "position";
                break;
            case 4:
                sortingColumns[0].column = "age";
                break;
        }

        sortingColumns[1].column = "id";
        sortingColumns[1].direction = sortingColumns[0].direction;

        return {
            keyword: this.searchText(),
            sortingColumns: sortingColumns,
            displayStart: this.pageIndex() * this.pageSize(),
            displayLength: this.pageSize(),
            includeCount: (this.pageIndex() === 0)
        };
    }

    __fetchServer(filter) {
        var dfd = $.Deferred();

        // Simulate sorting and filter from server side.
        // But perform at client side with some delay.
        setTimeout(() => {
            var response = {
                items: [],
                totalRecords: 0
            };

            // Build from source.
            var items = this.items();
            var rawDS = _.slice(items, 0, items.length);

            // Perform search.
            if(!_.isEmpty(filter.keyword)){
                rawDS = rawDS.filter((item) => {
                    return item.name.indexOf(filter.keyword) >= 0 || item.position.indexOf(filter.keyword) >= 0;
                });
            }
            if(filter.includeCount){
                response.totalRecords = rawDS.length;
            }

            // Perform order.
            rawDS.sort((a, b) => {
                var primarySort = filter.sortingColumns[0];
                var aValue = a[primarySort.column];
                var bValue = b[primarySort.column];
                var compareResult = 0;
                if(aValue > bValue){
                    compareResult = 1;
                }
                else if(aValue < bValue){
                    compareResult = -1;
                }

                if(primarySort.direction === 1){
                    return compareResult;
                }
                else {
                    return compareResult * -1;
                }
            });

            // assign to response with paging.
            response.items = _.slice(rawDS, filter.displayStart, filter.displayStart + filter.displayLength);
            console.log("Fetching..", filter, response);
            dfd.resolve(response);
        }, 100);

        return dfd;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableServerside),
    template: templateMarkup
};