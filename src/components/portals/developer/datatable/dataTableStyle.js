import templateMarkup from "text!./dataTableStyle.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

class DataTableStyleUsage extends ScreenBase {
    constructor(params){
        super(params);

        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("DataTable Styles Only");
        this.bladeDescription("src/components/portals/developer/datatable/dataTableStyle");


    }

    onLoad(isFirstLoad) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableStyleUsage),
    template: templateMarkup
};