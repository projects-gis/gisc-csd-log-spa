import templateMarkup from "text!./rowSelector.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

class DataTableRowSelector extends ScreenBase {
    constructor(params){
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("DataTable with Row Selector");
        this.bladeDescription("src/components/portals/developer/datatable/rowSelector");

        this.items = ko.observableArray([
            { id: 1, name: "Victor", position: "Software Engineer", age: 22 },
            { id: 2, name: "Tiger", position: "System Architect", age: 33 },
            { id: 3, name: "Satou", position: "Accountant", age: 31 }
        ]);
        // this.items = ko.observableArray([]);

        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([[ 1, "asc" ]]);

        this.selectedItems.subscribe(function(value) {
            console.log(this.selectedItems());
        }.bind(this));
    }

    onLoad(isFirstLoad) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableRowSelector),
    template: templateMarkup
};