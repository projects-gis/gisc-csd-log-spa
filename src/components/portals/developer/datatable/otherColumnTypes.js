import templateMarkup from "text!./otherColumnTypes.html";
import ScreenBase from "../../screenbase";
import ko from "knockout";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

class DataTableOtherColumnTypes extends ScreenBase {
    constructor(params){
        super(params);

        this.bladeTitle("Other Column Types");
        this.bladeDescription("src/components/portals/developer/datatable/otherColumnTypes");
        this.bladeSize = BladeSize.Medium;

        // Set datasource to DataTable control
        this.items = ko.observableArray([
            { id: 1, name: "Custom POI Category 1", colorHex: "#ff0000", code: "P001", isEnabled: false, viewonmap:'../images/ic-search.svg' },
            { id: 2, name: "Custom POI Category 2", colorHex: "#00ff00", code: "P002", isEnabled: true, viewonmap:'../images/ic-search.svg' },
            { id: 3, name: "Custom POI Category 3", colorHex: "#0000ff", code: "P003", isEnabled: false, viewonmap:'../images/ic-search.svg' }
        ]);

        // To preserve state when journey is inactive.
        this.searchText = ko.observable("");
        this.order = ko.observable([[ 2, "asc" ]]);

        // onClick of columnId: 'viewOnMapColumn'
        this.viewMapClick = (data) => {
            console.log(data);
            alert("Clicked on search icon for " + data.name);
        };
    }

    onLoad(isFirstLoad) {
    }


}

export default {
    viewModel: ScreenBase.createFactory(DataTableOtherColumnTypes),
    template: templateMarkup
};