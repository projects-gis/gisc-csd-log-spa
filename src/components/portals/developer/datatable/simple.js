import templateMarkup from "text!./simple.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

class DataTableSimpleUsage extends ScreenBase {
    constructor(params){
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Simple DataTable");
        this.bladeDescription("src/components/portals/developer/datatable/simple");

        this.items = ko.observableArray([
            { id: 1, name: "Victor", position: "Software Engineer", age: 22 },
            { id: 2, name: "Tiger", position: "System Architect", age: 33 },
            { id: 3, name: "Satou", position: "Accountant", age: 31 }
        ]);

        this.order = ko.observable([[ 2, "desc" ]]);
    }

    onLoad(isFirstLoad) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(DataTableSimpleUsage),
    template: templateMarkup
};