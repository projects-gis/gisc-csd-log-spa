﻿import ko from "knockout";
import templateMarkup from "text!./shipment.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants, Enums, EntityAssociation } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../app/frameworks/core/utility";

import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment"
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource"
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit"
import WebRequestVehicle from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";


class ShipmentServiceTesting extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle("Shipment Service Testing");

        //this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.Large;

        this.lstBU = null;

        this.isBuSuccess = ko.observable(false);
        this.isShipmentStatusSuccess = ko.observable(false);
        this.isVehicleLicenseSuccess = ko.observable(false);
        this.isShipmentCodeSuccess = ko.observable(false);
        this.isShipmentNameSuccess = ko.observable(false);
        this.isDOCodeSuccess = ko.observable(false);
        this.isDONameSuccess = ko.observable(false);
        this.isCreateBySuccess = ko.observable(false);

        this.isBUFieldValid = ko.observable(false);
        this.isShipmentStatusFieldValid = ko.observable(false);
        this.isVehicleLicenseFieldValid = ko.observable(false);
        this.isShipmentCodeFieldValid = ko.observable(false);
        this.isShipmentNameFieldValid = ko.observable(false);
        this.isDOCodeFieldValid = ko.observable(false);
        this.isDONameFieldValid = ko.observable(false);
        this.isCreateByFieldValid = ko.observable(false);
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad)
            return;

        this.refreshServiceStatus();
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
                this.refreshServiceStatus();
                break;
            default:
                break;
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    resetStatus() {
        this.isBuSuccess(false);
        this.isShipmentStatusSuccess(false);
        this.isVehicleLicenseSuccess(false);
        this.isShipmentCodeSuccess(false);
        this.isShipmentNameSuccess(false);
        this.isDOCodeSuccess(false);
        this.isDONameSuccess(false);
        this.isCreateBySuccess(false);

        this.isBUFieldValid(false);
        this.isShipmentStatusFieldValid(false);
        this.isVehicleLicenseFieldValid(false);
        this.isShipmentCodeFieldValid(false);
        this.isShipmentNameFieldValid(false);
        this.isDOCodeFieldValid(false);
        this.isDONameFieldValid(false);
        this.isCreateByFieldValid(false);
    }

    refreshServiceStatus() {
        this.resetStatus();

        this.isBusy(true);

        var dfd = $.Deferred();
        let dfdShipmentCode = null;
        let dfdShipmentName = null;

        let dfdDoCode = null;
        let dfdDoName = null;
        let dfdCreateBy = null;

        /* Process : Business Unit */
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        let dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter).done((response) => {
            this.isBuSuccess(true);

            this.isBUFieldValid(this.checkField(response.items,
                ["childBusinessUnits", "code", "companyId", "createBy", "createDate", "fullCode", "fullPath", "id", "infoStatus",
                    "isAccessible", "name", "pageProperty", "parentBusinessUnitId", "parentBusinessUnitPath", "updateBy", "updateDate"]));

            this.lstBU = response.items.map((item) => {
                return item.id;
            });

            /* Process : Shipment Code */
            let shipmentCodeFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: this.lstBU,
                includeIds: [1],
                shipmentFromDate: '2018-02-01T00:00:00',
                shipmentToDate: '2018-02-20T23:59:59',
                shipmentCode: 'SC1'
            };

            dfdShipmentCode = this.webRequestShipment.autocompleteShipmentCode(shipmentCodeFilter).done((response) => {
                this.isShipmentCodeSuccess(true);

                this.isShipmentCodeFieldValid(response,
                    ["createBy", "createDate", "doCode", "doId", "doName", "id", "infoStatus", "pageProperty", "poNumber",
                        "shipmentCode", "shipmentId", "shipmentName", "updateBy", "updateDate"]);
            });
            /* End Process : Shipment Code */

            /* Process : Shipment Name */
            let shipmentNameFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: this.lstBU,
                includeIds: [1],
                shipmentFromDate: '2018-02-01T00:00:00',
                shipmentToDate: '2018-02-20T23:59:59',
                shipmentName: '1101'
            };

            dfdShipmentName = this.webRequestShipment.autocompleteShipmentName(shipmentNameFilter).done((response) => {
                this.isShipmentNameSuccess(true);

                this.isShipmentNameFieldValid(response,
                    ["createBy", "createDate", "doCode", "doId", "doName", "id", "infoStatus", "pageProperty", "poNumber",
                        "shipmentCode", "shipmentId", "shipmentName", "updateBy", "updateDate"]);
            });

            /* Process : DO Code */
            let doCodeFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: this.lstBU,
                includeIds: [1],
                shipmentFromDate: '2018-02-01T00:00:00',
                shipmentToDate: '2018-02-20T23:59:59',
                doCode: '1101'
            };

            dfdDoCode = this.webRequestShipment.autocompleteShipmentDOCode(doCodeFilter).done((response) => {
                this.isDOCodeSuccess(true);
            });
            /* End Process : DO Code */

            /* Process : DO Name */
            let doNameFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: this.lstBU,
                includeIds: [1],
                shipmentFromDate: '2018-02-01T00:00:00',
                shipmentToDate: '2018-02-20T23:59:59',
                doName: '1101'
            };

            dfdDoName = this.webRequestShipment.autocompleteShipmentDOName(doNameFilter).done((response) => {
                this.isDONameSuccess(true);
            });
            /* End Process : DO Name */

            /* Process : Create By */
            let createByFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: this.lstBU,
                includeIds: [1],
                shipmentFromDate: '2018-02-01T00:00:00',
                shipmentToDate: '2018-02-20T23:59:59',
                createBy: 'OTM'
            };

            dfdCreateBy = this.webRequestShipment.autocompleteShipmentCreateBy(createByFilter).done((response) => {
                this.isCreateBySuccess(true);
            });
            /* End Process : Create by */
        });
        /* End Process : Business Unit */

        /* Process : Shipment Status */
        let shipmentStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobStatus]
        }

        let dfdShipmentStatus = this.webRequestEnumResource.listEnumResource(shipmentStatusFilter).done((response) => {
            this.isShipmentStatusSuccess(true);

            this.isShipmentStatusFieldValid(this.checkField(response.items,
                ["createBy", "createDate", "displayName", "id", "infoStatus", "name", "nameResourceKey", "order",
                    "pageProperty", "resourceType", "type", "updateBy", "updateDate", "value"]));
        });
        /* End Process : Shipment Status */

        /* Process : Vehicle License */
        let vehicleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: [80],
            includeIds: [1]
        };

        let dfdVehicle = this.webRequestVehicle.listVehicleSummary(vehicleFilter).done((response) => {
            this.isVehicleLicenseSuccess(true);

            this.isVehicleLicenseFieldValid(response.items,
                ["boxId", "brand", "businessUnitId", "businessUnitName", "createBy", "createDate", "enable", "formatEnable", "iconId", "id", "infoStatus",
                    "license", "model", "pageProperty", "referenceNo", "updateBy", "updateDate", "vahicleCategoryId", "vehicleType", "vehicleTypeDisplayName"]);
        });
        /* End Process : Vehicle License */

        $.when(dfdBusinessUnit, dfdShipmentStatus, dfdVehicle, dfdShipmentCode, dfdShipmentName, dfdDoCode, dfdDoName, dfdCreateBy).done(() => {
            this.isBusy(false);
        }).fail(() => {
            this.isBusy(false);
        });
    }

    checkField(items, fields) {
        let isValidField = true;

        //for (let attr in items[0]) {
        //    isValidField = isValidField && (fields.indexOf(attr) > -1);
        //}

        for (let ind in fields) {
            isValidField = isValidField && items[0].hasOwnProperty(fields[ind]);
        }

        return isValidField;
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentServiceTesting),
    template: templateMarkup
};