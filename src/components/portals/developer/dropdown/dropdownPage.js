import ko from "knockout";
import templateMarkup from "text!./dropdownPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

/**
 * Sample usage of gisc-ui-dropdown controls
 * 
 * @class DropdownPage
 * @extends {ScreenBase}
 */
class DropdownPage extends ScreenBase {
    /**
     * Creates an instance of DropdownPage.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Dropdown (gisc-ui-dropdown)");
        this.bladeDescription("src/components/portals/developer/dropdown");

        //Observable array initially contains objects
        this.availableCountries = ko.observableArray([
            new Country("UK", 65000000),
            new Country("USA", 320000000),
            new Country("Sweden", 29000000)
        ]);
        //Nothing selected by default
        this.selectedItem = ko.observable();
        this.selectedItem2 = ko.observable();
        this.selectedOptions = ko.observableArray([]);
        this.showItem = ko.observableArray([]);

        this.enable = ko.observable(false);
    }
    
    showSelected() {
        if (this.selectedOptions().length > 0) {
            this.showItem.push(this.selectedOptions()[0].countryName);
        } else {
            this.displayDialog("", "Please selecte item", BladeDialog.DIALOG_OK);
        }
    }
}

/**
 * A DTO for this example
 * 
 * @class Country
 */
class Country {
    /**
     * Creates an instance of Country.
     * 
     * @param {any} name
     * @param {any} population
     */
    constructor(name, population) {
        this.countryName = name;
        this.countryPopulation = population;
    }
}

export default {
    viewModel: ScreenBase.createFactory(DropdownPage),
    template: templateMarkup
};