import ko from "knockout";
import templateMarkup from "text!./mapIndex.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";

class MapIndexScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle("Map Index");
        this.bladeDescription("src/components/portals/developer/map/mapIndex");

        this.bladeMargin = BladeMargin.Narrow;
        this.selectedIndex = ko.observable();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    /**
     * Navigate to another page. 
     * @param {any} data
     */
    goto (data){
        var pageName = data.id;
        this.navigate(pageName);
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(MapIndexScreen),
    template: templateMarkup
};