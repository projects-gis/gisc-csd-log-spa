﻿import ko from "knockout";
import templateMarkup from "text!./mapDrawMultiplePolygonZoom.html";
import ScreenBase from "../../screenbase";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class MapDrawMultiplePolygonZoom extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Draw Multiple Polygon Zoom('draw-multiple-polygon-zoom')");
        this.bladeDescription("src/components/portals/developer/map/mapDrawMultiplePolygonZoom");

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onMapComplete(data) {
        console.log(data);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    invokeCommand() {
        let fillColor = '#6b2593', borderColor = '#0c574d', fillOpacity = 0.5, borderOpacity = 1;
        var mockRings = [
            [
                [100.526750, 13.724927],
                [100.530486, 13.725509],
                [100.528664, 13.722982],
                [100.526750, 13.724927]
            ],
            [
                [100.520953, 13.731480],
                [100.520038, 13.728948],
                [100.524549, 13.729588],
                [100.520953, 13.731480]
            ]
        ];

        MapManager.getInstance().drawMultiplePolygonZoom(this.id, mockRings, fillColor, borderColor, fillOpacity, borderOpacity);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MapDrawMultiplePolygonZoom),
    template: templateMarkup
};