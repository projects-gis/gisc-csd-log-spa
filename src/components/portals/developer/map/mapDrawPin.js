import ko from "knockout";
import templateMarkup from "text!./mapDrawPin.html";
import ScreenBase from "../../screenbase";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class MapDrawPinScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Draw Pin (draw-pin)");
        this.bladeDescription("src/components/portals/developer/map/mapAddPin");

        this.latitude = ko.observable(13.904320);
        this.longitude = ko.observable(100.529803);
        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onMapComplete (data) {
        console.log(data);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clear(this.id);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    /**
     * Add pin to map.
     */
    addPin (){
        MapManager.getInstance().drawPin(this.id, this.latitude(), this.longitude());
    }
}

export default {
    viewModel: ScreenBase.createFactory(MapDrawPinScreen),
    template: templateMarkup
};