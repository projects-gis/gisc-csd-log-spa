﻿import ko from "knockout";
import templateMarkup from "text!./mapEnableDrawPolygon.html";
import ScreenBase from "../../screenbase";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

class MapEnableDrawPolygon extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Enable Draw Polygon ('enable-draw-polygon')");
        this.bladeDescription("src/components/portals/developer/map/mapEnableDrawPolygon");

        this.resultText = ko.observable();

        // Register event subscribe.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onMapComplete(data) {
        console.log(data);

        if(data.command == "enable-draw-polygon" || data.command == "cancel-draw-polygon" )
            this.resultText(JSON.stringify(data))
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    invokeCommand() {
        MapManager.getInstance().enableDrawingPolygon(this.id);
    }

    invokeCancelCommand(){
        MapManager.getInstance().cancelDrawingPolygon(this.id);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MapEnableDrawPolygon),
    template: templateMarkup
};