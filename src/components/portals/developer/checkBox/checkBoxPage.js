import templateMarkup from "text!./checkBoxPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

/**
 * 
 * 
 * @class CheckBoxScreen
 * @extends {ScreenBase}
 */
class CheckBoxScreen extends ScreenBase {

    /**
     * Creates an instance of CheckBoxScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("CheckBox (gisc-ui-checkBox)");
        this.bladeDescription("src/components/portals/developer/checkBox");

        this.isChecked = ko.observable(true);
        this.isNotChecked = ko.observable(false);

        this.isEnable = ko.observable(false);
        this.isCheckedEnable = ko.observable(false);

    }

    onLoad(isFirstLoad) {}

    toggleIsEnable() {
        this.isEnable(!this.isEnable());
    }
}

export default {
    viewModel: ScreenBase.createFactory(CheckBoxScreen),
    template: templateMarkup
};