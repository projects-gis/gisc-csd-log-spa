import ko from "knockout";
import templateMarkup from "text!./dropdownTreePage.html";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ScreenBase from "../../screenbase";

class DropdownTreeScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("dropdownTree (gisc-ui-dropdowntree)");
        this.bladeDescription("src/components/portals/developer/dropdownTree");

        // this.selectedItem = ko.observable(2);
        // this.selectedItem = ko.observableArray([2]);
        this.selectedItem = ko.observable("1");
        this.items = ko.observableArray();
        
        this.selectedText = ko.pureComputed(() => {
            return this.selectedItem() ? this.selectedItem() : "No items selected";
        });

        this.selectedText2 = ko.pureComputed(() => {
            return this.selectedItem2() ? this.selectedItem2() : "No items selected";
        });

        this.disable = ko.observable(true);

        this.disableNodeCollections = ko.observableArray(["3","6"]);

    }

    onclick(){
        // this.selectedItem(null);
        this.disable(!this.disable());
    }

     /**
     * For custom build node.
     * 
     * @param {any} node
     * 
     * @memberOf TreeViewScreen
     */
    // onBuildNode(node) {
    //     node.text = node.text + " (" + node.rawData.code + ")";
    // }

    onLoad() {
         var mockData = [{
            id: 1,
            code: "A001",
            name: "Head OfficeHead",
            parentNode: null,
            childNodes: [{
                id: 3,
                code: "B001",
                name: "Central",
                parentNode: 1,
                childNodes: [{
                    id: 5,
                    name: "Bangkok",
                    code: "C001",
                    parentNode: 3,
                    childNodes: []
                }, {
                    id: 6,
                    name: "Nonthaburi NonthaburiNonthaburiNonthaburiNonthaburiNonthaburiNonthaburiNonthaburiNonthaburi",
                    code: "C001",
                    parentNode: 3,
                    childNodes: []
                }]
            }, {
                id: 4,
                name: "North",
                code: "B002",
                parentNode: 1,
                childNodes: []
            }]
        }, {
            id: 2,
            name: "Regional Office",
            code: "A001",
            parentNode: null,
            childNodes: []
        }];

        this.items(mockData);
    }

    // setupExtend() {
    //     // this.selectedItem.extend({ trackArrayChange: true });
    //     this.selectedItem.extend({ trackChange: true });
    // }

    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(DropdownTreeScreen),
    template: templateMarkup
};