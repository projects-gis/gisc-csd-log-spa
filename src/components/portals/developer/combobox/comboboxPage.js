import ko from "knockout";
import ScreenBase from "../../screenBase";
import templateMarkup from "text!./comboboxPage.html";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";

/**
 * Sample usage of gisc-ui-combobox controls
 * 
 * @class ComboboxPage
 * @extends {ScreenBase}
 */
class ComboboxPage extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle("Combobox (gisc-ui-combobox)");
        this.bladeDescription("src/components/portals/developer/combobox");
        this.bladeSize = BladeSize.Medium;
        
        this.availableCountries = ko.observableArray([
            { name: "UK", value: "_uk" },
            { name: "USA", value: "_us" },
            { name: "Sweden", value: "_swe" }
        ]);
        this.selectedItem = ko.observable();
        this.selectedItem2 = ko.observable();
        this.selectedItem3 = ko.observable();

        this.isSample3Enable = ko.observable(false);

        this.selectedItem.subscribe(()=> {
            console.log(this.selectedItem());
        });
    }

    onLoad(isFirstLoad) {
    }
    onUnload() {
        super.onUnload();
    }
    buildActionBar(actions) {
    }
    buildCommandBar(commands){
    }
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(ComboboxPage), 
    template: templateMarkup
};