import ko from "knockout";
import templateMarkup from "text!./reportViewer.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/BladeSize";

class ReportViewerHelpScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Report Viewer")());
        this.bladeSize = BladeSize.Large;

        this.reportUrl = "http://localhost:40067/api/reports/";
        this.reportSource = {
            report: 'Telerik.Reporting.Examples.CSharp.ReportCatalog,CSharp.ReportLibrary',
            parameters: {}
        };
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(ReportViewerHelpScreen),
    template: templateMarkup
};