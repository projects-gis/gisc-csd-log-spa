import templateMarkup from "text!./tabPage.html";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import ko from "knockout";

class TabScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle("Tab (gisc-ui-tab)");
        this.bladeDescription("src/components/portals/developer/tab");

        // Sample properties.
        this.dateTime1 = new Date();
        this.dateTime2 = ko.observable(new Date());
        this.selectedIndex = ko.observable(1);

        setInterval(() => {
            this.dateTime2(new Date());
        }, 1000);

        // Sample2 properties.
        this.tab2Visible = ko.observable(true);
    }
    onClick() {
        console.log('tab demo click');
    }
    onToggleTab() {
        this.tab2Visible(!this.tab2Visible());
    }
}

export default {
    viewModel: ScreenBase.createFactory(TabScreen),
    template: templateMarkup
};