﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class Dashboard extends ScreenBase {

    /**
     * Creates an instance of ShipmentDashboardScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        //console.log(params);
        this.params = (!_.isEmpty(params)) ? params : null;

        this.filter = ko.observable(params);
        this.bladeTitle(this.i18n("Dashboard_Utilization_Driving_Time_Header")());
        this.barChart = ko.observableArray([]);
        this.bladeSize = BladeSize.Medium;
        //this.bladeIsMaximized = true;
        this.nameChart = ko.observable();
        this.dateFrom = ko.observable(params.dateFromFormat);
        this.dateTo = ko.observable(params.dateToFormat);
        this.isDriver = ko.observable(false);

        if (this.params) {
            if (this.filter().entityType.value == Enums.ModelData.SearchEntityType.Driver) {
                this.isDriver(true)
                this.bladeTitle(this.i18n("Dashboard_Utilization_Details_Driver")());
                this.nameChart(this.i18n("Dashboard_Utilization_Driving_Time_Driver_Qty")());
            } else {
                this.bladeTitle(this.i18n("Dashboard_Utilization_Details_Vehicle")());
                this.nameChart(this.i18n("Dashboard_Utilization_Driving_Time_Vehicle_Qty")());
            }
        }

    }




    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }


        this.minimizeAll(false);

        var dfd = $.Deferred();
        let filter = {}
        filter.businessUnitId = this.filter().businessUnitId
        filter.dateFrom = this.filter().dateFrom;
        filter.dateTo = this.filter().dateTo;
        filter.entityType = this.filter().entityTypeValue;
        filter.dataType = "D";

        let dfdGraph = this.webRequestTruckDriverUtilization.utilizationgraph(filter);
        this.isBusy(true);

        $.when(dfdGraph).done((respontGraph) => {

                let reGraphInfo = this.formatGraphInfo(respontGraph.graphUtilizationInfos);
                
                this.barChart.replaceAll(respontGraph.graphUtilizationInfos);
               
                //this.barChart.push(this.nameChart());
                //console.log("barChart", this.barChart());

                dfd.resolve();

            }).fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
        return dfd;
    }

    formatGraphInfo(graphUtilizationInfos) {
        let reGraphInfo = new Array();

        _.forEach(graphUtilizationInfos, (graphItem) => {
            graphItem.labelText = graphItem.percent + "% <br> (" +  graphItem.value + ")";
            reGraphInfo.push(graphItem);
        });
        
        return reGraphInfo;
    }

    

    barChartCallback(event) {
        let filter = {}
        filter.businessUnitId = this.filter().businessUnitId
        filter.dateFrom = this.filter().dateFrom;
        filter.dateTo = this.filter().dateTo;
        filter.entityType = this.filter().entityType.value;
        filter.graphId = event.item.dataContext.id;

        this.navigate("cw-dashboard-utilization-driving-time-details", filter);
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }

}


export default {
    viewModel: ScreenBase.createFactory(Dashboard),
    template: templateMarkup
};