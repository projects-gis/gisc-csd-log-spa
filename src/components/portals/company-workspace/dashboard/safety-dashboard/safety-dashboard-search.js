﻿import ko from "knockout";
import templateMarkup from "text!./safety-dashboard-search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class SafetyDashboardSearch extends ScreenBase {

    /**
     * Creates an instance of ShipmentDashboardScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Dashboard_Driver_Performance_Score_Header_Search")());
        this.bladeSize = BladeSize.Small;

        this.businessUnitList = ko.observableArray([]);
        this.businessUnit = ko.observable();
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }
        var dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter)

        $.when(dfdBusinessUnit).done((respondBusinessUnit) => {
            this.businessUnitList(respondBusinessUnit.items);

            dfd.resolve();
        })

        return dfd;
    }

    setupExtend() {
        this.businessUnit.extend({
            required: true
        });
        

        var paramsValidation = {
            businessUnit: this.businessUnit
        }
        this.validationModel = ko.validatedObservable(paramsValidation);
    }

    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();

        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Dashboard_Utilization_Time_Preview')()));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        switch (sender.id) {
            case "actPreview":

                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }
                let filter = {};
                filter.businessUnitIds = this.businessUnit();
                this.navigate("cw-dashboard-safety-dashboard-dashboard", filter);
                
                break;
            default:
                break;
        }
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

}


export default {
    viewModel: ScreenBase.createFactory(SafetyDashboardSearch),
    template: templateMarkup
};