﻿import ko from "knockout";
import templateMarkup from "text!./report.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeType from "../../../../../app/frameworks/constant/BladeType";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class SafetyDashboardReport extends ScreenBase {
    /**
     * Creates an instance of DashboardOverall.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Dashboard_Safety_Dashboard_Header")());
        this.bladeSize = BladeSize.XLarge_Report;
        this.bladeIsMaximized = true;
        this.bladeType = BladeType.Compact;
        this.minimizeAll(false);
         // Prepare report configuration.
         this.reportUrl = this.resolveUrl(WebConfig.reportSession.reportUrl); 
         this.reportSource = params.reportSource;
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }


    }



    onDomReady () {

        var self = this;

        window.SPAMapManager = function(lat,lon){

            let clickLocation = {
                lat: lat,
                lon: lon
            },
            symbol = {
                url: self.resolveUrl("~/images/pin_destination.png"),
                width: 35,
                height: 50,
                offset: {
                    x: 0,
                    y: 0
                },
                rotation: 0
            },
            zoom = 17,
            attributes = null

            MapManager.getInstance().drawOnePointZoom(self.id, clickLocation, symbol, zoom, attributes);
            self.minimizeAll();

        }
    }

    onUnload () {
        window.SPAMapManager = null;
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }


}


export default {
    viewModel: ScreenBase.createFactory(SafetyDashboardReport),
    template: templateMarkup
};