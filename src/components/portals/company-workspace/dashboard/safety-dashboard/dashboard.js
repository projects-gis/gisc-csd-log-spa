﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class SafetyDashboard extends ScreenBase {
    /**
     * Creates an instance of DashboardOverall.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.height = $('.app-blade-content')["0"].clientHeight - 170;
        $('.divPerfomanceScore').css("height", this.height + "px");
        this.dgDriverPerformanceScoreHeight = ko.observable((this.height / 3) + 30);

        this.bladeTitle(this.i18n("Dashboard_Safety_Dashboard_Header")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.businessUnits = ko.observableArray(params.businessUnitIds);
        this.donutChartList = ko.observableArray([]);
        this.donutChartData = ko.observable();
        this.donutChartData1 = ko.observable();
        this.centerLabel = ko.observable();
        this.centerLabel1 = ko.observable();
        this.title = ko.observable();
        this.animation = ko.observable();
        this.formatBalloonText = ko.observable('[[name]]: [[percents]]% ([[value]])');
        this.lblLastSync = ko.observable(`${this.i18n("Vehicle_MA_LastSync")()} `)


    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        this.refreshData();

    }



    buildCommandBar(commands) {
        commands.push(
            this.createCommand(
                "cmdRefresh",
                this.i18n("Common_Refresh")(),
                "svg-cmd-refresh"
            )
        );
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
                this.refreshData();
                break
            default:
                break
        }
    }

    onDatasourceRequestRead(gridOption) {

    }
    refreshData() {
        var dfd = $.Deferred();
        var self = this;
        let property = []
        this.donutChartList.replaceAll([])

        let filter = {
            businessUnitIds: this.businessUnits()
        }

        var dfdSafetyList = this.webRequestTruckDriverUtilization.safetyDashboardSummaryList(filter)
        $.when(dfdSafetyList).done((respondSafetyList) => {

            this.lblLastSync(`${this.i18n("Vehicle_MA_LastSync")()}  ${respondSafetyList.formatTimestamp}`)
            if (respondSafetyList.items.length != 0) {
                respondSafetyList.items.forEach(item => {
                    this.donutChartList.push({
                        overtime4hrdfm: item.overtime4Hr ? item.overtime4Hr.graphInfos : null,
                        overtime4hrnondfm: item.overtime4HrNonDfm ? item.overtime4HrNonDfm.graphInfos : null,
                        overtime10hrdfm: item.overtime10Hr ? item.overtime10Hr.graphInfos : null,
                        overtime10hrnondfm: item.overtime10HrNonDfm ? item.overtime10HrNonDfm.graphInfos : null,
                        aotivatingdfm: item.activatingDriving ? item.activatingDriving.graphInfos : null,
                        aotivatingnondfm: item.activatingDrivingNonDfm ? item.activatingDrivingNonDfm.graphInfos : null,
                        rest10hrdfm: item.resting ? item.resting.graphInfos : null,
                        rest10hrnondfm: item.restingNonDfm ? item.restingNonDfm.graphInfos : null,

                        totleover4hrdfm: `${item.overtime4Hr ? item.overtime4Hr.percentage : null}%`,
                        totleover4hrnondfm: `${item.overtime4HrNonDfm ? item.overtime4HrNonDfm.percentage : null}%`,
                        totleover10hrdfm: `${item.overtime10Hr ? item.overtime10Hr.percentage : null}%`,
                        totleover10hrnondfm: `${item.overtime10HrNonDfm ? item.overtime10HrNonDfm.percentage : null}%`,
                        totleaotivatingdfm: `${item.activatingDriving ? item.activatingDriving.percentage : null}%`,
                        totleaotivatingnondfm: `${item.activatingDrivingNonDfm ? item.activatingDrivingNonDfm.percentage : null}%`,
                        totalrest10hrdfm: `${item.resting ? item.resting.percentage : null}%`,
                        totalrest10hrnondfm: `${item.restingNonDfm ? item.restingNonDfm.percentage : null}%`,

                        formatBalloonText: this.formatBalloonText(),
                        animation: this.animation(false),
                        title: item.businessUnitName,
                        propertyGraph: property,
                        buFilter: item.buFilter 
                    });
                })
            } else {
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }
            dfd.resolve();
        })

        return dfd;
    }

    onClickPieChart(type, buFilter, params) {
        let reportName;
        //let key = params.dataItem.dataContext
        switch (type) {
            case "overtime4hr":
                reportName = "Gisc.Csd.Service.Report.Library.SafetyDashboard.Overtime4HrReport,Gisc.Csd.Service.Report.Library";
                break;
            case "overtime10hr":
                reportName = "Gisc.Csd.Service.Report.Library.SafetyDashboard.Overtime10HrReport,Gisc.Csd.Service.Report.Library";

                break;
            case "activatingdriving":
                reportName = "Gisc.Csd.Service.Report.Library.SafetyDashboard.ActivatingDrivingReport,Gisc.Csd.Service.Report.Library";

                break;
            case "restlessthan10hr":
                reportName = "Gisc.Csd.Service.Report.Library.SafetyDashboard.RestLessThan10HrReport,Gisc.Csd.Service.Report.Library";

                break;

            default:
                break;
        }

        this.reports = {
            report: reportName,
            parameters: {
                GraphID: params.dataItem.dataContext.id,
                UserID: WebConfig.userSession.id,
                BuIds: buFilter.toString(),//this.businessUnits().toString(),
                Language: WebConfig.userSession.currentUserLanguage,
                CompanyID: WebConfig.userSession.currentCompanyId,
                PrintBy: WebConfig.userSession.fullname
            }
        };


        this.navigate("cw-dashboard-safety-dashboard-report", {
            reportSource: this.reports
        });
    }
    refreshDataGrid() {
        this.dispatchEvent("dgDriverPerformanceScore", "refresh");
    }

    barChartCallback(event) {

    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }


}


export default {
    viewModel: ScreenBase.createFactory(SafetyDashboard),
    template: templateMarkup
};