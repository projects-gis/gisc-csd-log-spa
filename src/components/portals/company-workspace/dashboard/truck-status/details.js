﻿import ko from "knockout";
import templateMarkup from "text!./details.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import { AssetIcons } from "../../../../../app/frameworks/constant/svg";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class TruckStatusDetail extends ScreenBase {
    /**
     * Creates an instance of TruckStatusDetail.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        //params = {"state":"moving","data":{"businessUnitId":[91],"durationType":"A","jobType":"NM","movementType":1,"graphType":"I"}}

        this.bladeTitle(this.i18n("Dashboard_Truck_Status_Dashboard_Header")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.lblLastSync = ko.observable(`${this.i18n("Vehicle_MA_LastSync")()}`)
        this.lblTitleDetail = ko.observable(null);
        this.filter = this.ensureObservable(params.data, null)
        this.state = this.ensureObservable(params.state, null)

       

        this.svgMovement = "<div class='icon-movement'>" + AssetIcons.IconMovementTemplateMarkup + "</div>";
        this.svgSpeed = "<div class='icon-speed'>" + AssetIcons.IconSpeedTemplateMarkup + "</div>";
        this.svgEngine = "<div class='icon-engine'>" + AssetIcons.IconEngineTemplateMarkup + "</div>";
        this.svgDirection = "<div class='icon-direction'>" + AssetIcons.IconDirectionTemplateMarkup + "</div>";
        this.svgFuel = "<div class='icon-fuel'>" + AssetIcons.IconFuelTemplateMarkup + "</div>";
        this.svgVehicleBattery = "<div class='icon-vehicleBattery'>" + AssetIcons.IconVehicleBatteryTemplateMarkup + "</div>";
        this.svgEvent = "<div class='icon-event'>" + AssetIcons.IconEventTemplateMarkup + "</div>";
        this.svgSos = "<div class='icon-sos'>" + AssetIcons.IconSosTemplateMarkup + "</div>";
        this.svgGate = "<div class='icon-gate'>" + AssetIcons.IconGateTemplateMarkup + "</div>";
        this.svgTempareture = "<div class='icon-tempareture'>" + AssetIcons.IconTemparetureTemplateMarkup + "</div>";
        this.svgPower = "<div class='icon-power'>" + AssetIcons.IconPowerTemplateMarkup + "</div>";
        this.svgMileage = "<div class='icon-mileage'>" + AssetIcons.IconMileageTemplateMarkup + "</div>";
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
    }

    refreshAlertTicketList() { // refresh
        this.dispatchEvent("dgTruckDetailLst", "refresh");  
    }

    onDatasourceRequestRead(gridOption) { 
        this.isBusy(true);
        let dfd = $.Deferred();
        let filter = Object.assign({}, this.filter(), gridOption);
        this.webRequestTruckDriverUtilization.truckStatusList(filter).done((res) => {
            //ต่อ String
            this.lblLastSync(`${this.i18n("Vehicle_MA_LastSync")()} ${res.formatTimestamp}`)
            this.lblTitleDetail(res.itemLabel)
            res.items.forEach((item) => {
                let featureList = item['trackLocationFeatureValues']
                for (let i in featureList) {
                    switch (featureList[i]['featureCode']) {
                        case Enums.AssetFeatures.Engine:
                            item[Enums.AssetFeatures.Engine] = featureList
                            break
                        case Enums.AssetFeatures.Gate:
                            item[Enums.AssetFeatures.Gate] = featureList[i]
                            break
                        case Enums.AssetFeatures.Fuel:
                            item[Enums.AssetFeatures.Fuel] = `${featureList[i]['formatValue']}${featureList[i]['featureUnitSymbol']}`
                            break
                        case Enums.AssetFeatures.VehicleBattery:
                            item[Enums.AssetFeatures.VehicleBattery] = `${featureList[i]['formatValue']}${featureList[i]['featureUnitSymbol']}`
                            break
                        case Enums.AssetFeatures.DeviceBattery:
                            item[Enums.AssetFeatures.DeviceBattery] = `${featureList[i]['formatValue']}${featureList[i]['featureUnitSymbol']}`
                            break
                        case Enums.AssetFeatures.DriverCardReader:
                            item[Enums.AssetFeatures.DriverCardReader] = `${featureList[i]['formatValue']}${featureList[i]['featureUnitSymbol']}`
                            break
                        case Enums.AssetFeatures.Mileage:
                            item[Enums.AssetFeatures.Mileage] = `${featureList[i]['formatValue']}${featureList[i]['featureUnitSymbol']}`
                            break
                        default:
                            break
                    }
                }
                if(!item.jobCode) {
                    item.jobCode = "-"
                }
            });


            dfd.resolve({
                items: res.items,
                totalRecords: res.totalRecords,
                currentPage: res.currentPage
            });
        }).fail((e) => {
            this.handleError(e)
        }).always(() => {
            this.isBusy(false)
        });

        return dfd;
    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.exportExcel();
                break
            default:
                break
        }
    }

    exportExcel() {
        this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                    this.isBusy(true)
                    this.webRequestTruckDriverUtilization.truckStatusExportList(this.filter()).done((res) => {
                        ScreenHelper.downloadFile(res.fileUrl);
                    }).fail((e) => {
                        this.handleError(e)
                    }).always(() => {
                        this.isBusy(false)
                    })
                    break;
                default:
                    break;
            }
        });
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }

    static get constants() {
        return class DetailConstants {
            static get PARKING() {
                return "parking";
            }

            static get MOVING() {
                return "moving";
            }
        }
    }

    renderLocationColumn(data) {
        if (data) {
            var linkCssClass = "location";

            switch (data.locationDescriptionColor) {

                case Enums.ModelData.LocationDescriptionColor.Black:
                    linkCssClass += " system-poi";
                    break;
                case Enums.ModelData.LocationDescriptionColor.Green:
                    linkCssClass += " system-poi-green";
                    break;
                case Enums.ModelData.LocationDescriptionColor.Blue:
                    linkCssClass += " system-poi-blue";
                    break;
            }

            var node = '<span class="' + linkCssClass + '">' + data.currentLocation + '</span>';
            return node;
        }
        return "";
    }

}


export default {
    viewModel: ScreenBase.createFactory(TruckStatusDetail),
    template: templateMarkup
};