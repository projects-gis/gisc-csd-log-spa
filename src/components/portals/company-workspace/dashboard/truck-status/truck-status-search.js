﻿import ko from "knockout";
import templateMarkup from "text!./truck-status-search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class TruckStatusSearch extends ScreenBase {

    /**
     * Creates an instance of TruckStatusSearch.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Dashboard_Truck_Status_Header_Search")());
        this.bladeSize = BladeSize.Small;

        this.businessUnitList = ko.observableArray();
        this.businessUnit = ko.observable();

    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        this.isBusy(true)

        let dfd = $.Deferred();
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        
        let dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitParentSummary(businessUnitFilter);

        $.when(dfdBusinessUnit).done((respondBusinessUnit) => {
            this.businessUnitList(respondBusinessUnit.items);
            dfd.resolve();
        }).fail((e) => {
            this.handleError(e)
        }).always(() => {
            this.isBusy(false)
        });

        
        return dfd;
    }

    setupExtend() {
        this.businessUnit.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            businessUnit: this.businessUnit
        });
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Common_Preview')()));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }

        switch (sender.id) {
            case "actPreview":
                this.navigate("cw-dashboard-truck-status-dashboard-all", {
                    businessUnits: this.businessUnit()
                });
                break;
            default:
                break;
        }
    }

    get webRequestTruckDriverUtilization(){
        return WebRequestTruckDriverUtilization.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
}


export default {
    viewModel: ScreenBase.createFactory(TruckStatusSearch),
    template: templateMarkup
};