﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class ConfigurationMenuScreen extends ScreenBase {

    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Dashboard_DashboardTitlePage")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.TruckStatus)) {
            this.items.push({
                text: this.i18n("Dashboard_Truck_Status_Header")(),
                id: 'cw-dashboard-truck-status'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.DashboardUtilizationDrivingTime)) {
            this.items.push({
                text: this.i18n("Dashboard_Utilization_Driving_Time_Header")(),
                id: 'cw-dashboard-utlization-driving-time'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.DriverPerformanceReport)) {
            this.items.push({
                text: this.i18n("Dashboard_Driver_Performance_Score_Header")(),
                id: 'cw-dashboard-driver-score'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.SafetyDashboard)) {
            this.items.push({
                text: this.i18n("Dashboard_Safety_Dashboard_Header")(),
                id: 'cw-dashboard-safety-dashboard-search'
            });
        }

        //this.items.push({
        //    text: this.i18n("Dashboard_Utilization_Driving_Time_Header")(),
        //    id: 'cw-dashboard-utlization-driving-time'
        //});
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) { }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) { }
}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationMenuScreen),
    template: templateMarkup
};