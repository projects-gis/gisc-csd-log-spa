﻿import ko from "knockout";
import templateMarkup from "text!./details.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import { AssetIcons } from "../../../../../app/frameworks/constant/svg";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class TruckStatusDetail extends ScreenBase {
    /**
     * Creates an instance of TruckStatusDetail.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Dashboard_Driver_Performance_Score_Header_Detail")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.lblLastSync = ko.observable();
        this.lblTitleDetail = ko.observable(null);
        this.columns = ko.observableArray([]);
        this.filter = ko.observable(params.filter);
        this.columnsEvent = params.column;
        
        console.log("eiei",this.columns())


       
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        var columns = [
            { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Driver', data: 'driverName' , width:150,sortable: false,filterable:true,showFilter:true, className: 'dg-body-left'},
            { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Trainer', data: 'trainerName',width:150,sortable: false,filterable:true, className: 'dg-body-left' },
            { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Grade', data: 'grade', sortable: true,filterable:true,width:80, className: 'dg-body-center' },
            { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Score', data: 'score',width:80, sortable: false,columnMenu: false,className: 'dg-body-right'}
        ];

        let newColumns = columns.concat(this.columnsEvent)
        this.columns.replaceAll(newColumns);
        //this.refreshDataGrid();
    }

    refreshDataGrid() {
        this.dispatchEvent("dgDriverPerfomanceDetailLst", "refresh");
    }

    onDatasourceRequestRead(gridOption) {

        let dfd = $.Deferred();

        let filter = Object.assign({}, this.filter(), gridOption);
        let dfdDriverPerfomanceDetails = this.webRequestTruckDriverUtilization.driverPerformanceList(filter);
        $.when(dfdDriverPerfomanceDetails).done((response) => {
            this.lblLastSync(this.i18n("Vehicle_MA_LastSync")() + " " + response.formatTimestamp);
            let reFormatData = new Array();

            _.each(response.items[0].performanceDriverInfos, (item) => {

                let itemData = {
                    "driverName": item.driverName,
                    "trainerName": item.trainerName,
                    "grade": item.grade,
                    "score": item.score
                };
                for (var key in item.alertScoreInfos) {
                    itemData["data_" + key] = item.alertScoreInfos[key];
                }

                reFormatData.push(itemData);
            });

            dfd.resolve({
                items: reFormatData,
                totalRecords: response.totalRecords,
                currentPage: response.currentPage 
            });

        }).fail(e => {
            this.handleError(e);
        })
            .always(() => {
                this.isBusy(false);
                // this.onAutoRefresh(true);
            });

        return dfd;
    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }


    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.exportExcel();
        }
    }

    exportExcel() {

        this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                let dfdExport = this.webRequestTruckDriverUtilization.driverPerformanceDetailExport(this.filter());
                $.when(dfdExport).done((response) => {
                        // Do Somting For Export 
                         ScreenHelper.downloadFile(response.fileUrl);

                })
        
                    break;
                default:
                    break;
            }
        });
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }

    static get constants() {
        return class DetailConstants {
            static get PARKING() {
                return "parking";
            }

            static get MOVING() {
                return "moving";
            }
        }
    }
}


export default {
    viewModel: ScreenBase.createFactory(TruckStatusDetail),
    template: templateMarkup
};