﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestTruckDriverUtilization from "../../../../../app/frameworks/data/apitrackingcore/webRequestTruckDriverUtilization";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class DriverPerformanceDashboard extends ScreenBase {
    /**
     * Creates an instance of DashboardOverall.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.height = $('.app-blade-content')["0"].clientHeight - 170;
        $('.divPerfomanceScore').css("height", this.height + "px");
        this.dgDriverPerformanceScoreHeight = ko.observable((this.height / 3)+30);

        this.bladeTitle(this.i18n("Dashboard_Driver_Performance_Dashboard_Header")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        this.responseBackup = [];
        this.performanceAlertGradeInfos = [];
        this.performanceDriverInfos = [];
        this.businessUnitIds = ko.observableArray(params.businessUnitsIds);
        this.dateFrom = ko.observable(params.startDate);
        this.dateTo = ko.observable(params.endDate);
        this.driverPerformanceRuleId = ko.observable(params.driverPerformanceRuleId);
        this.columsForDetailPage = ko.observableArray([]);



        this.buName = ko.observable(this.i18n('Common_All')());
        this.columns = ko.observableArray([]);

        this.filter = ko.observable({
                "businessUnitIds": this.businessUnitIds(),
                "dateFrom": this.dateFrom(),
                "dateTo": this.dateTo(),
                "driverPerformanceRuleId": this.driverPerformanceRuleId() 
        });

        

        this.apiDataSource = ko.observableArray([]);
        // this.columns = ko.observableArray([
        //     { type: 'text', title: 'Driver', data: 'driverName' },
        //     { type: 'text', title: 'Trainer', data: 'trainerName' },
        //     { type: 'text', title: 'Grade', data: 'grade' },
        //     { type: 'text', title: 'Score', data: 'score' },
        //     { type: 'text', title: 'Overspeed', data: 'overspeed' },
        //     { type: 'text', title: 'Swerve', data: 'swerve' },
        //     { type: 'text', title: 'Acc Rapid', data: 'accrapid' },
        //     { type: 'text', title: 'Dec Rapid', data: 'decrapid' },
        //     { type: 'text', title: 'Bad GPS', data: 'badgps' }
        // ]);

        
        //   this.columns.replaceAll([
        //     { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Driver', data: 'driverName' },
        //     { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Trainer', data: 'trainerName'  },
        //     { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Grade', data: 'grade', sortable: true },
        //     { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Score', data: 'score'}
        //   ]);

        this.barChartMain = ko.observableArray([]);
        this.barChart2 = ko.observableArray([]);
        this.nameChart = ko.observable(this.i18n("Dashboard_Driver_Performance_Score_TableColumn_Score")());
        this.nameChart2 = ko.observable(this.i18n("Dashboard_Driver_Performance_Score_TableColumn_Driver")());

        this.radarChart = ko.observableArray([]);
        this.maximumDisplayColumnMainChart = ko.observable(7);


        this.isShowChartScrollbar = ko.computed(()=>{
            return this.barChartMain().length > this.maximumDisplayColumnMainChart();   //return true or false
        });

        this.fixedColumnWidth = ko.computed(() => {
            return this.barChartMain().length > this.maximumDisplayColumnMainChart() ? null : 60;
        });

        this.valueAxesGuide = ko.observableArray([[{
            value: 60,
            lineColor: "#CC0000",
            lineThickness: 2,
            lineAlpha: 1,
            inside: true,
            //labelRotation: 90,
            label: ""
        }]]);
        // this.businessUnit = params.businessUnits;

        

    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        this.isBusy(true)
        var dfd = $.Deferred();
        this.refreshData().done(()=>{
            dfd.resolve();
        }).fail(e => {
            this.handleError(e);
        }).always(() => {
                this.isBusy(false);
            });
        return dfd;

    }

    // ViewFullDetail=function(){

    // }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
                this.refreshData();
                break;
        }
    }

    onDatasourceRequestRead(gridOption) {
        
        var dfd = $.Deferred();
        // console.log("this.performanceAlertGradeInfos>>",this.performanceAlertGradeInfos);
        // console.log("this.performanceDriverInfos>>>",this.performanceDriverInfos);

        let reFormatData = new Array();
        reFormatData=[];
        
        _.each(this.performanceDriverInfos, (item) => {

            let itemData = {
                "driverName": item.driverName,
                "trainerName": item.trainerName,
                "grade": item.grade,
                "score": item.score
            };
            for (var key in item.alertScoreInfos) {
                itemData["data_" + key] = item.alertScoreInfos[key];
            }

            reFormatData.push(itemData);
        });


        // var mockGridData = [
        //     { "driverName": "Mr. Nutta Ruangraiprom", "trainerName": "Mr. Thongkhum", "grade": "D", "score": 34, "overspeed": 11, "swerve": 0, "accrapid": 4, "decrapid": 3, "badgps": 0 },
        //     { "driverName": "Mr. Ittisawarut Wangbum", "trainerName": "Mr. Thongkhum", "grade": "D", "score": 34, "overspeed": 11, "swerve": 0, "accrapid": 4, "decrapid": 3, "badgps": 0 },
        //     { "driverName": "Mr. Chattharn Priparmuk", "trainerName": "Mr. Thongkhum", "grade": "D", "score": 34, "overspeed": 11, "swerve": 0, "accrapid": 4, "decrapid": 3, "badgps": 0 },
        //     { "driverName": "Mr. Anatta Wongtieng", "trainerName": "Mr. Thongkhum", "grade": "D", "score": 34, "overspeed": 11, "swerve": 0, "accrapid": 4, "decrapid": 3, "badgps": 0 },

        //     { "driverName": "Mr. Nutta Ruangraiprom", "trainerName": "Mr. Thongkhum", "grade": "D", "score": 34, "overspeed": 11, "swerve": 0, "accrapid": 4, "decrapid": 3, "badgps": 0 }
        // ];

        //console.log("respontDetails", respontDetails);

      
        dfd.resolve({
            items: reFormatData,
            totalRecords: reFormatData.length,
            currentPage: 1
        });

        return dfd;
    }
    refreshData(){
        this.isBusy(true)

        let dfd = $.Deferred();
        //Test Mock Call Services
        console.log("Filter>>",this.filter())
        let dfdGraph = this.webRequestTruckDriverUtilization.driverPerformanceAll(this.filter());
        $.when(dfdGraph).done((response) => {
            //console.log("response>>>",response)

            if (response.items.length != 0) {

                this.barChartMain.replaceAll(response.items[0].averageGraphInfos);
                this.barChart2.replaceAll(response.items[0].averageDetailInfos["0"].gradeInfos);

                this.radarChart.replaceAll(response.items[0].averageDetailInfos["0"].radarInfos);
                this.responseBackup = response.items[0];

                this.performanceDriverInfos = response.items[0].averageDetailInfos["0"].performanceDriverInfos;
                this.performanceAlertGradeInfos = response.items[0].averageDetailInfos["0"].performanceAlertGradeInfos;

                var columns = [
                    { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Driver', data: 'driverName' , width:150, className: 'dg-body-left'},
                    { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Trainer', data: 'trainerName',width:150, className: 'dg-body-left' },
                    { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Grade', data: 'grade', sortable: true,width:80, className: 'dg-body-center' },
                    { type: 'text', title: 'i18n:Dashboard_Driver_Performance_Score_TableColumn_Score', data: 'score',width:80 ,className: 'dg-body-right'}
                ];
                
                var columsForDetailPage = [];
                var testCount = 0;
                _.each(this.performanceAlertGradeInfos, (item) => {
                    columns.push({ type: 'text', title: item.alertDescription, data: "data_" + item.alertConfigurationId ,width:150, className: 'dg-body-right',sortable: false});
                    columsForDetailPage.push({ type: 'text', title: item.alertDescription, data: "data_" + item.alertConfigurationId ,width:150, className: 'dg-body-right',sortable: false,columnMenu: false});
                });
              
                this.columsForDetailPage.replaceAll(columsForDetailPage)
                this.columns.replaceAll(columns);
            } 
          
            this.refreshDataGrid();
            dfd.resolve();

        }).fail(e => {
            this.handleError(e);
        }).always(() => {
                this.isBusy(false);
            });

        return dfd;
    }


    refreshDataGrid() {
        this.dispatchEvent("dgDriverPerformanceScore", "refresh");
    }
    viewFullReport(evn){
        
                this.navigate("cw-dashboard-driver-score-details", {
                    "filter":this.filter(),
                    "column":this.columsForDetailPage()
                });
            
    }

    barChartCallback(event) {

        let graphId = event.item.dataContext.id;
        // this.businessUnitIds(graphId)
        this.buName(event.item.dataContext.name);
        this.radarChart.replaceAll(this.responseBackup.averageDetailInfos[graphId].radarInfos);

        this.barChart2.replaceAll(this.responseBackup.averageDetailInfos[graphId].gradeInfos);
        this.performanceDriverInfos = this.responseBackup.averageDetailInfos[graphId].performanceDriverInfos;
        this.refreshDataGrid();

        // this.filter({
        //     "businessUnitIds": this.businessUnitIds(),
        //     "dateFrom": this.dateFrom(),
        //     "dateTo": this.dateTo(),
        //     "driverPerformanceRuleId": this.driverPerformanceRuleId() 
        // })
 
    }

    get webRequestTruckDriverUtilization() {
        return WebRequestTruckDriverUtilization.getInstance();
    }


}


export default {
    viewModel: ScreenBase.createFactory(DriverPerformanceDashboard),
    template: templateMarkup
};