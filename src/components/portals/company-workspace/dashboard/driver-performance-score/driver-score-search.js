﻿import ko from "knockout";
import templateMarkup from "text!./driver-score-search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class DriverScoreSearch extends ScreenBase {

    /**
     * Creates an instance of ShipmentDashboardScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Dashboard_Driver_Performance_Score_Header_Search")());
        this.bladeSize = BladeSize.Small;

        this.startDate = this.ensureObservable();
        this.endDate = this.ensureObservable();
        this.businessUnitList = ko.observableArray([]);
        this.businessUnit = ko.observable();
        this.valueRadio = ko.observable('');

        this.performanceRuleOptions = ko.observableArray([]);
        this.performanceRule = ko.observable();



        this.valueRadio.subscribe((res) => {
            var startDate = new Date();
            var endDate = new Date();

            switch (res) {
                case 'Today':
                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));
                    break;
                case 'Yesterday':
                    startDate.setDate(startDate.getDate() - 1)
                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(startDate));

                    break;
                case 'ThisWeek':
                    let startWeek = parseInt(startDate.getDate() - (startDate.getDay() - 1));
                    let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    startDate.setDate(startWeek)
                    endDate.setDate(endWeek)

                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));

                    break;
                case 'LastWeek':
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek)
                    endDate.setDate(LastendWeek - 1)

                    this.startDate(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));

                    break;
                case 'ThisMonth':
                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);


                    this.startDate(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));
                    this.endDate(this.setFormatDateTime({
                        data: lastDay,
                        end: true
                    }));
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(firstDay));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(lastDay));
                    break;
                case 'LastMonth':
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.startDate(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.endDate(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))
                    $("#searchStartDate").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#searchEndDate").val(this.setFormatDisplayDateTime(LastlastDay));
                    break;
                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });

         this.isDateTimeEnabled = ko.computed(() => {
             return this.valueRadio() == 'Custom';
        });
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }
        let dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitParentSummary(businessUnitFilter);
        let driveRuleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            enable: true
        };
        let dfdDriveRule = this.webRequestDriveRule.listDriveRuleSummary(driveRuleFilter);

        $.when(dfdBusinessUnit, dfdDriveRule).done((responseBusinessUnit, responseDriveRule ) => {
            this.businessUnitList(responseBusinessUnit.items);
            this.performanceRuleOptions(responseDriveRule.items);
            dfd.resolve();
        });
        return dfd;
    }

    setupExtend() {
        this.businessUnit.extend({
            required: true
        });
        this.startDate.extend({
            required: true
        });
        this.endDate.extend({
            required: true
        });
        this.performanceRule.extend({
            required: true
        });

        var paramsValidation = {
            businessUnit: this.businessUnit,
            startDate: this.startDate,
            endDate: this.endDate,
            performanceRule: this.performanceRule
        }
        this.validationModel = ko.validatedObservable(paramsValidation);
    }

    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();

        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Dashboard_Utilization_Time_Preview')()));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        switch (sender.id) {
            case "actPreview":

                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                let filter = {
                    businessUnitsIds: this.businessUnit(),
                    startDate: this.startDate(),
                    endDate: this.endDate(),
                    driverPerformanceRuleId: this.performanceRule().id
                }
                this.navigate("cw-dashboard-driver-score-dashboard",filter );
                break;
            default:
                break;
        }
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }
    // get webRequestEnumResource() {
    //     return WebRequestEnumResource.getInstance();
    // }

}


export default {
    viewModel: ScreenBase.createFactory(DriverScoreSearch),
    template: templateMarkup
};