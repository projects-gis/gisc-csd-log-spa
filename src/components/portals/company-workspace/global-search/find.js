import ko from "knockout";
import templateMarkup from "text!./find.html";
import ScreenBase from "../../screenbase";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";
import FindCategory from "./findCategory";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import WebRequestGlobalSearch from "../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";
import GeoLocation from "../../../../app/frameworks/core/geoLocation";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

/**
 * Global search finding form.
 */
class GlobalSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        this.bladeMargin = BladeMargin.Narrow;
        this.searchType = this.ensureNonObservable(params.searchType, Enums.ModelData.GlobalSearchType.Logistics);
        this.categories = [];
        this.selectedCategory = ko.observable(null);
        this.keyword = ko.observable("");
        this.tmpNameTown = null;

        this.enableFind = ko.pureComputed(() => {
            // Verify with category and extra logic of
            // • หากไม่มีเครื่องหมายคำพูด (”) ประกบ จะต้องมี 3 ตัวอักษรขึ้นไป ระบบจะค้นหาข้อมูลที่ใกล้เคียง Keyword โดยไม่รวมเว้นวรรค
            // • หากมีเครื่องหมายคำพูด (“) ประกบ ระบบจะค้นหาข้อมูลที่เหมือน Keyword เท่านั้น
            var userKeyword = this.keyword();
            //var isValidLength = /[^\s]{3,}/.test(userKeyword);
            //var isValidKeywordQuote = /^"(.*)"$/.test(userKeyword); //comment ไว้แล้ว มาเช็คที่ checkValidLength
            var isValidCategory = !_.isEmpty(this.selectedCategory());
            var isValidQuery = false;
            // ถ้าเป็น search ตำบล จะยอมให้ กรอกได้ 2 ตัว เพราะมี ตำบล ปอ จ.จังหวัดเชียงราย
            var checkForTown = this.selectedCategory() == "TAM_NAMT,TAM_NAME"; 

            var checkValidLength = false;
            if (_.startsWith(userKeyword.trim(), "\"")){
                //var checkValidLength = _.replace(_.replace(userKeyword, /"/g, "").trim(), / /g, "").length >= 3; // replaceในสุด replace " (double quote) , replaceนอกสุด replace ช่องว่าง(space) แล้ว เช็คว่า ต้องมี 3 ตัว ขึ้นไป
                let arrKeyword = _.split(userKeyword, /"/g); // แยกเป็น Array ด้วย " (double quote)
                //console.log(arrKeyword);
                if(checkForTown){
                    checkValidLength = _.filter(arrKeyword, (item) => { return item.trim().length >= 2 }).length > 0; // หา array ที่ มี length >= 3
                }
                else{
                    checkValidLength = _.filter(arrKeyword, (item) => { return item.trim().length >= 3 }).length > 0; // หา array ที่ มี length >= 3
                }
            }
            else {
                if(checkForTown){
                    checkValidLength = /[^\s]{2,}/.test(userKeyword);
                }
                else{
                    checkValidLength = /[^\s]{3,}/.test(userKeyword);
                }
                
            }

            switch(this.searchType) {
                case Enums.ModelData.GlobalSearchType.Logistics:
                case Enums.ModelData.GlobalSearchType.Map:
                    isValidQuery = isValidCategory || checkValidLength;//isValidKeywordQuote || isValidLength;
                    break;
                case Enums.ModelData.GlobalSearchType.AdminPoly:
                case Enums.ModelData.GlobalSearchType.Street:
                    isValidQuery = checkValidLength;//isValidKeywordQuote || isValidLength;
                    break;
            }

            return isValidQuery;
        });

        // Update title based on type.
        switch(this.searchType) {
            case Enums.ModelData.GlobalSearchType.Logistics:
                this.bladeTitle(this.i18n("GlobalSearchType_Logistics")());
                break;
            case Enums.ModelData.GlobalSearchType.Map:
                this.bladeTitle(this.i18n("GlobalSearchType_Map")());
                break;
            case Enums.ModelData.GlobalSearchType.AdminPoly:
                this.bladeTitle(this.i18n("GlobalSearchType_AdminPoly")());
                break;
            case Enums.ModelData.GlobalSearchType.Street:
                this.bladeTitle(this.i18n("GlobalSearchType_Street")());
                break;
        }

        // Service properties.
        this.webRequestGlobalSearch = WebRequestGlobalSearch.getInstance();
        this.geoLocation = GeoLocation.getInstance();

        this.onDatasourceRequestSearch = (request, response) => {         
            if(this.selectedCategory() != null) {

                switch(this.searchType) {
                    case Enums.ModelData.GlobalSearchType.Logistics:
                        var filter = {
                            companyId: WebConfig.userSession.currentCompanyId,
                            type: this.searchType,
                            keyword: request.term,
                            catagories: this.selectedCategory(),
                            latitude: null,
                            longitude: null
                        }
                        this.webRequestGlobalSearch.autocomplete(filter).done((data) => {
                            response( $.map( data, (item) => {
                                return {
                                    label: item,
                                    value: '"' + item + '"'
                                };
                            }));
                        });
                        break;
                    default:
                        response(null);
                        break;
                }   
            }else{
                response(null);
            }                     
        }
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfdLoadCategories = $.Deferred();

        this.webRequestGlobalSearch.getCategory(this.searchType)
        .done((categories) => {
            // Dynamic creaate icon names.
            categories.forEach((c) => {
                var findCategory = new FindCategory(
                    c.name,
                    c.displayName,
                    FindCategory.getIconName(this.searchType, c.name)
                );
                this.categories.push(findCategory);
            });
            dfdLoadCategories.resolve();
        })
        .fail((e) => {
            dfdLoadCategories.reject(e);
        });

        return dfdLoadCategories;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {}

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    /**
     * Handle when user perform finding object.
     */
    onFindClick(data, event) 
    {
        let keyword = this.keyword();
        if((event.keyCode === 13 || event.type === 'click') && (_.size(keyword) >= 3 || this.selectedCategory())){
            var isValid = true;
            var model = {
                searchType: this.searchType,
                keyword: keyword,
                category: this.selectedCategory(),
                latitude: null,
                longitude: null
            };
            if(isValid) {
                // Get current location from browser navigation.
                // See https://www.w3schools.com/html/html5_geolocation.asp
                var dfdGeoLocation = $.Deferred();

                // Set this page as busy.
                this.isBusy(true);

                // Check for geolocation when user perform map search and browser has already suppoted.
                if (model.searchType === Enums.ModelData.GlobalSearchType.Map) {
                    this.geoLocation.getCurrentPosition()
                    .done((lat, lon) => {
                        model.latitude = lat;
                        model.longitude = lon;
                        dfdGeoLocation.resolve();
                    })
                    .fail(() => {
                        dfdGeoLocation.resolve();
                    });
                } else { 
                    dfdGeoLocation.resolve();
                }

                // Go to search result when gps is ready.
                $.when(dfdGeoLocation).always(() => {
                    this.navigate("cw-global-search-result", model);
                    this.isBusy(false);
                });
            }
        }
        return true;
    }

    /**
     * Handle when user click on category.
     * 
     * @param {any} category
     */
    onCategoryClick(category) {
        this.categories.forEach((c) => {
            if(c.id === category.id) {
                // Found matching object.
                // Toggle selected state.
                c.selected(!c.selected());

                // Update state.
                if(c.selected()){
                    // Update selected state.
                    this.selectedCategory(c.id);
                }
                else {
                    // Remove selected state.
                    this.selectedCategory(null);
                }
            }
            else {
                // Toggle others.
                c.selected(false);
            }
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(GlobalSearchScreen),
    template: templateMarkup
};