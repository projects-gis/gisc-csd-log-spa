import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class GlobalSearchMenuScreen
 * @extends {ScreenBase}
 */
class GlobalSearchMenuScreen extends ScreenBase {
    /**
     * Creates an instance of GlobalSearchMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("GlobalSearch_GlobalSearchTitle")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }


        // Internal Search.
        if(WebConfig.userSession.hasPermission(Constants.Permission.Logistics)){
            this.items.push({
                text: this.i18n("GlobalSearchType_Logistics")(),
                page: "cw-global-search-find",
                pageOption: Enums.ModelData.GlobalSearchType.Logistics
            });
        }
        

        // Location Search.
        if(WebConfig.userSession.hasPermission(Constants.Permission.Map)){
            this.items.push({
                text: this.i18n("GlobalSearchType_Map")(),
                page: "cw-global-search-find",
                pageOption: Enums.ModelData.GlobalSearchType.Map
            });
        }
        

        // Admin Poly Search.
        if(WebConfig.userSession.hasPermission(Constants.Permission.AdminPoly)){
            this.items.push({
                text: this.i18n("GlobalSearchType_AdminPoly")(),
                page: "cw-global-search-find",
                pageOption: Enums.ModelData.GlobalSearchType.AdminPoly
            });
        }
       

        // Street Search.
        if(WebConfig.userSession.hasPermission(Constants.Permission.Street)){
            this.items.push({
                text: this.i18n("GlobalSearchType_Street")(),
                page: "cw-global-search-find",
                pageOption: Enums.ModelData.GlobalSearchType.Street
            });
        }
        
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    /**
     * Open next page when user choose menu.
     * 
     * @param {any} item
     */
    selectingRowHandler(item) {
        if (item) {
            return this.navigate(item.page, {searchType: item.pageOption});
        }
        return false;
    }
}

export default {
    viewModel: ScreenBase.createFactory(GlobalSearchMenuScreen),
    template: templateMarkup
};