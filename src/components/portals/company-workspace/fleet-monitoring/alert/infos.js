import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class AlertFilter {
    constructor (businessUnitIds = null, 
    alertTypeId = null, 
    alertConfigurationId = null,
    startDate = null,
    endDate = null,
    entityType = null,
    selectedEntityId = null,
    keyword = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.businessUnitIds = businessUnitIds;
        this.alertTypeIds = alertTypeId ? [alertTypeId] : [],
        this.alertConfigurationIds = alertConfigurationId ? [alertConfigurationId] : null,
        this.startDate = startDate;
        this.endDate = endDate;
        this.searchEntityType = entityType;
        this.searchEntityIds = selectedEntityId ? [selectedEntityId] : null;
        this.sortingColumns = DefaultSorting.Alert;
        this.keyword = keyword;
    }
}
export { AlertFilter };