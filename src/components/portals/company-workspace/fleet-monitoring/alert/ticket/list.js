﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../../app/frameworks/core/utility";
import { Constants, Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import svgMovement from "text!../../../../../../components/svgs/icon-movement.html";
import { AssetIcons } from "../../../../../../app/frameworks/constant/svg";
import WebRequestAlert from "../../../../../../app/frameworks/data/apitrackingcore/webrequestAlert";
import MapManager from "../../../../../controls/gisc-chrome/map/mapManager";
import IQTechUtility from "../../../../../../app/frameworks/core/iqtechUtility";
import HikvisionUtility from "../../../../../../app/frameworks/core/hikvisionUtility";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import EncryptUtility from "../../../../../../app/frameworks/core/encryptUtility"
import WebRequestGlobalSearch from "../../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";
import QuickSearchScreenBase from "../../../../quick-search-screenbase";
import AssetUtility from "../../../asset-utility";

class TicketListScreen extends QuickSearchScreenBase {
    constructor(params) {
        super(params);
        this.bladeEnableQuickSearch = true;
        this.statusId = this.ensureObservable(params.statusId);
        this.keyword = this.ensureObservable(params.keyword);
        this.quickSearchKeyword(this.keyword() ? this.keyword() : null);
        this.bladeTitle(this.i18n("FleetMonitoring_Alert_Ticket")());
        this.bladeSize = BladeSize.XLarge_A0;
        this.bladeIsMaximized = true;
        this.mdvrModelCompany = ko.observable('');
        this.lstData = [];        
        this.isManage = WebConfig.userSession.hasPermission(Constants.Permission.Manage_Ticket) > 0;
        this.svgMovement = "<div class='icon-movement'>" + AssetIcons.IconMovementTemplateMarkup + "</div>";
        this.svgSpeed = "<div class='icon-speed'>" + AssetIcons.IconSpeedTemplateMarkup + "</div>";
        this.classCssLocation = ko.observable();

        this.filter = ko.observable({
            startDate: this.setObjDate({ objDate: new Date(), start: true }),
            endDate: this.setObjDate({ objDate: new Date(), end: true })
        });

        this.svgs = {
            "movement": AssetIcons.IconMovementTemplateMarkup,
            "speed": AssetIcons.IconSpeedTemplateMarkup,
            "gps": AssetIcons.IconGpsTemplateMarkup,
            "fuel" : AssetIcons.IconFuelTemplateMarkup,
            "engine" : AssetIcons.IconEngineTemplateMarkup
        };

        this.checkChanged = (item) => {
            if (item.length == 1) {
                this.lstData.map((val, index) => {
                    if (item[0].id == val.id) {
                        this.lstData[index].isCheck = item[0].value;
                    } else {                     
                        this.lstData[index].isCheck = val.isCheck;
                    }
                })
            } else {
                item.map((val, index) => {
                    this.lstData[index].isCheck = val.value;
                });
            }
        }

        this.gotoAction = (data) => {
            switch(parseInt(data.alertTicketStatus)) {
                case Enums.ModelData.AlertTicketStatus.New:
                case Enums.ModelData.AlertTicketStatus.Inprogress:                
                    this.navigate("cw-fleet-monitoring-alert-ticket-action", { id: data.id, alertTicketStatus: data.alertTicketStatus });
                    break;
                case Enums.ModelData.AlertTicketStatus.Close:
                    break;
                default:
                    break;
            }
        };

        this.gotoViewAction = (data) => {
            this.navigate("cw-fleet-monitoring-alert-ticket-action", { id: data.id, view: true });
        }

        this.viewAlertPoint = (data) => {
            if(data) {
                //จะ Zoom ไปยังตำแหน่งของจุดที่เกิดเหตุการณ์
                this.drawAlertOnMap(data.latitude, data.longitude, data);
                this.minimizeAll();
                //return empty deferred because if not return deferred, selected row does not work properly.
                return Utility.emptyDeferred();
            }
            return false;
        };

        this.setDateFilter = ((selectedDate) => {
            let objDate = new Date();
            let filter = {};
            let start, end = null;
            switch(selectedDate) {
                case "today":
                    start = objDate;
                    end = objDate;                    
                    break;
                case "this-week":
                    start = this.setObjDateWeek({ objDate, start: true });
                    end = this.setObjDateWeek({ objDate, end: true });
                    break;
                case "this-month":
                    start = this.setObjDateMonth({ objDate, start: true });
                    end = this.setObjDateMonth({ objDate, end: true });
                    break;
                case "last-month":     
                    objDate.setDate(1);
                    start = this.setObjDateMonth({ objDate: Utility.minusMonths(objDate, 1), start: true });
                    end = this.setObjDateMonth({ objDate: Utility.minusMonths(objDate, 1), end: true }); 
                    break;                   
                default:
                    break;
            }

            filter.startDate = this.setObjDate({ objDate: start, start: true });
            filter.endDate = this.setObjDate({ objDate: end, end: true });
            this.filter(filter);
            this.refreshAlertTicketList();
        });

        this.subscribeMessage("cw-fleet-monitoring-alert-ticket-filter-search", (filter)=> {
            this.filter(filter);
            this.statusId(filter.alertTicketStatusIds);
            this.refreshAlertTicketList();
        });

        this.subscribeMessage("cw-fleet-monitoring-alert-ticket-action-close", (obj)=> {
            this.refreshAlertTicketList();
        });

        this.subscribeMessage("cw-fleet-monitoring-alert-ticket-action-create-log", (data)=> {
            this.refreshAlertTicketList();
        });


        this.isPermissionVideo = true; //no permission set hidden to true
        if (WebConfig.userSession.hasPermission(Constants.Permission.EventVideo)) {
            this.isPermissionVideo = false; // has permission set hidden to false
        }
        this.isNewWidget = false;

        var self = this;
        this.onPlayVideo = (params) => {

            let filter;

            if(params.alertDisplayType == Enums.ModelData.AlertDisplayType.Video){
                let driverName = params.driverName == null || params.driverName == " " ? "-" :  params.driverName;
                let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;
                
                let videoWidget = $('#cw-fleet-monitoring-common-video-widget');
                if(videoWidget.length){
                    videoWidget.data("kendoWindow").close();
                    setTimeout(()=>{
                        this.widget('cw-fleet-monitoring-common-video-widget', params, {
                            title: `${vehicleLicense}, ${driverName}`,
                            modal: false,
                            resizable: false,
                            minimize: false,
                            target: "cw-fleet-monitoring-common-video-widget",
                            width: "480",
                            height: "300",
                            id: 'cw-fleet-monitoring-common-video-widget',
                            left: "30%",
                            bottom: "20%"
                        });
                    },500);
                }else{
                    this.widget('cw-fleet-monitoring-common-video-widget', params, {
                        title: `${vehicleLicense}, ${driverName}`,
                        modal: false,
                        resizable: false,
                        minimize: false,
                        target: "cw-fleet-monitoring-common-video-widget",
                        width: "480",
                        height: "300",
                        id: 'cw-fleet-monitoring-common-video-widget',
                        left: "30%",
                        bottom: "20%"
                    });
                }

            }else if(params.alertDisplayType == Enums.ModelData.AlertDisplayType.Picture){
                let driverName = params.driverName == null || params.driverName == " " ? "-" :  params.driverName;
                let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;
                
                let pictureWidget = $('#cw-fleet-monitoring-common-picture-widget');
                if(pictureWidget.length){
                    pictureWidget.data("kendoWindow").close();
                    setTimeout(()=>{
                        this.widget('cw-fleet-monitoring-common-picture-widget', params, {
                            title: `${vehicleLicense}, ${driverName}`,
                            modal: false,
                            resizable: true,
                            minimize: false,
                            target: "cw-fleet-monitoring-common-picture-widget",
                            width: "480",
                            height: "300",
                            id: 'cw-fleet-monitoring-common-picture-widget',
                            left: "30%",
                            bottom: "20%"
                        });
                    },500);
                }else{
                    this.widget('cw-fleet-monitoring-common-picture-widget', params, {
                        title: `${vehicleLicense}, ${driverName}`,
                        modal: false,
                        resizable: true,
                        minimize: false,
                        target: "cw-fleet-monitoring-common-picture-widget",
                        width: "480",
                        height: "300",
                        id: 'cw-fleet-monitoring-common-picture-widget',
                        left: "30%",
                        bottom: "20%"
                    });
                }

            }else if (params.mdvrBrand == Enums.ModelData.MDVRModel.HIKvision) {
                var dfd = $.Deferred();

                let vehicleInfos = { //ข้อมูลรถและคนขับสำหรับแสดงบน Title Video

                    "vehicleLicense": params.vehicleLicense,
                    "alertTypeDisplayName": params.alertTypeDisplayName,
                    "formatAlertDateTime": params.formatAlertDateTime,
                    "driverName": params.driverName

                }
               // console.log("Onplay Video HikVision", params);

                localStorage.removeItem("cameraInfos");
                localStorage.removeItem("vehicleInfos");

                filter = {
                    "deviceId": params.deviceId,
                    "startTime": params.startPreview,
                    "endTime": params.endPreview,
                    "recordType": 1 //Device
                }


                self.hikvisionUtility.playBack(filter).done((res) => {

                    localStorage.cameraInfos = JSON.stringify(res.items);
                    localStorage.vehicleInfos = JSON.stringify(vehicleInfos);

                    dfd.resolve();

                }).fail((e) => {
                    console.log(e);
                })

                window.open(WebConfig.appSettings.eventView, "EventviewPage");
                return dfd;


            } else if (params.mdvrBrand == Enums.ModelData.MDVRModel.IQTech) { //brand=3

                var dfdEventVideo = $.Deferred();

                let startPreview = new Date(params.startPreview);
                let endPreview = new Date(params.endPreview);

                //decrypt Key
                 let cameraConig = this.encryptUtility.decryption(params.mdvrServerKey)
                // let result = config.split("|")
                
                self.iqtechUtility.getEventVideo(params.deviceId, startPreview, endPreview, cameraConig).done((resVideo) => {

                    if (self.isNewWidget) {
                        self.closeWidget();
                    }

                    resVideo.videoInfos = params;
                    resVideo.cameraConfig = cameraConig;

                    let driverName = params.driverName.driverName == null || params.driverName.driverName == " " ? "-" :  params.driverName;
                    let vehicleLicense = params.vehicleLicense == null || params.vehicleLicense == " " ? "-" :  params.vehicleLicense;

                    self.widget('cw-fleet-monitoring-eventview-widget', resVideo, {
                        // title: this.i18n("FleetMonitoring_Playback_Timeline_Widget")(),
                        title: `${vehicleLicense}, ${driverName}`,
                        modal: false,
                        resizable: false,
                        target: "cw-fleet-monitoring-eventview-widget",
                        width: "500",
                        height: "520",
                        id: 'cw-fleet-monitoring-eventview-widget',
                        left: "25%",
                        bottom: "10%"
                    });

                    self.isNewWidget = true;

                    dfdEventVideo.resolve();
                    // this.isBusy(false);

                }).fail((e) => {
                    console.log(e);
                })
            }

        }
        
        this.renderTooltip = (res)=> {
            let data = res.alertTicketNote == null || res.alertTicketNote == undefined? "-":res.alertTicketNote;
            let dataReturn = "";
            let displayTooltip = data.length > 13? data.substring(0, 13) + '...':data
                dataReturn = `<div title="${data}">${displayTooltip}</div>`;

            return dataReturn;
        }

    }

    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return
        }

     
        var dfd = $.Deferred();

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
        $.when(dfdCompanySettings).done((res) => {

            this.mdvrModelCompany(res.mdvrModel) //for check create button
           // console.log("dfdCompanySettings",res);

            dfd.resolve();
        });

        return dfd;


    }


    /**
     * @lifecycle Handle when search on text box is clicked or enter.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        let keyword = this.quickSearchKeyword();
        if( 
            (event.keyCode === 13 || event.type === 'click') && 
            (_.size(keyword) >= data.minLength || _.size(keyword) === 0) 
        ){

            //set keyword for refresh (optional)
            this.keyword(keyword);

            //call dashboard with kyword
            this.refreshAlertTicketList();
        }
        return true;
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: request.term,
        }
        this.webRequestGlobalSearch.autocompleteDriverVehicle(filter).done((data) => {
            response( $.map( data, (item) => {
                return {
                    label: item,
                    value: '"' + item + '"'
                };
            }));
        });      
    }

    refreshAlertTicketList() {
        this.dispatchEvent("dgAlertTicketLst", "refresh");
    }


    renderLocationColumn(data) {
        if (data) {
            var linkCssClass = "location";

            switch (data.locationDescriptionColor) {

                case Enums.ModelData.LocationDescriptionColor.Black:
                    linkCssClass += " system-poi";
                    break;
                case Enums.ModelData.LocationDescriptionColor.Green:
                    linkCssClass += " system-poi-green";
                    break;
                case Enums.ModelData.LocationDescriptionColor.Blue:
                    linkCssClass += " system-poi-blue";
                    break;
            }

            var node = '<span class="' + linkCssClass + '">' + data.location + '</span>';
            return node;
        }
        return "";
    }



    onDatasourceRequestRead(gridOption) {
        this.isBusy(true);
        var dfd = $.Deferred();
        var filter = Object.assign({}, this.filter(), gridOption);
        filter.alertTicketStatusIds = this.statusId();
        filter.keyword = this.keyword();
        filter.companyId = WebConfig.userSession.currentCompanyId;
        this.webRequestAlert.alertTicketList(filter).done((res) => {

            _.forEach(res.items, (item) => { //add or edit property in json

                let deviceObject = {deviceId:item.deviceId, deviceCode:item.deviceCode}
                let tempObject = { driverName: item.driverName, driverMobile: item.driverMobile };
                item.driverName = tempObject; 

                item.deviceInfo = deviceObject;
            });
            this.lstData = res.items;

            dfd.resolve({
                items: res.items,
                totalRecords: res.totalRecords,
                currentPage: !res.currentPage ? 1 : res.currentPage
            });
        }).fail((e)=> {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);
        });

        return dfd;
    }


    setupExtend() {}
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clear();
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search"));

        if(this.isManage) {
            commands.push(this.createCommand("cmdCloseTicket", this.i18n("FleetMonitoring_Alert_Ticke_Close_Ticket")(), "svg-cmd-clear"));
        }  

        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));

        
        if (this.mdvrModelCompany() === Enums.ModelData.MDVRModel.All) {

            commands.push(this.createCommand("cmdDownloadUserGuideIQTech", `${this.i18n("Download_User_Guide")()} (IQTech)`, "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadUserGuideHikvision", `${this.i18n("Download_User_Guide")()} (Hikvision)`, "svg-icon-load-guide"));

            commands.push(this.createCommand("cmdDownloadPluginIQTech", `${this.i18n("Download_Plugin")()} (IQTech)`, "svg-icon-load-plugin"));
            commands.push(this.createCommand("cmdDownloadPluginHikvision", `${this.i18n("Download_Plugin")()} (Hikvision)`, "svg-icon-load-plugin"));

            commands.push(this.createCommand("cmdStartPlugin", this.i18n("Start_Plugin")(), "svg-icon-start-plugin"));

        } else if (this.mdvrModelCompany() === Enums.ModelData.MDVRModel.IQTech) {

            commands.push(this.createCommand("cmdDownloadUserGuideIQTech", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadPluginIQTech", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));

        } else if (this.mdvrModelCompany() === Enums.ModelData.MDVRModel.HIKvision) {

            commands.push(this.createCommand("cmdDownloadUserGuideHikvision", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadPluginHikvision", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));

        }
        // commands.push(this.createCommand("cmdWidget",this.i18n("widget")(),"svg-icon-start-plugin"));
        // commands.push(this.createCommand("cmdHikvision",this.i18n("Hikvision")(),"svg-icon-start-plugin"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actOK") {

        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {


            case "cmdDownloadUserGuideIQTech":

                ScreenHelper.downloadFile(this.resolveUrl('~/vendors/iqtech/LiveviewEventviewUserGuide_iqtech.pdf'));

                break;
            case "cmdDownloadUserGuideHikvision":

                ScreenHelper.downloadFile(this.resolveUrl('~/vendors/hikvision/LiveviewEventviewUserGuide.pdf'));

                break;
            case "cmdDownloadPluginIQTech":

                window.open('https://get.adobe.com/flashplayer/')

                break;

            case "cmdDownloadPluginHikvision":

                ScreenHelper.downloadFile(this.resolveUrl('~/vendors/hikvision/PlatformSDKWebControlPlugin.zip'));

                break;

            case "cmdStartPlugin":

                this.hikvisionUtility.startPlugin();

                break;
         
            case "cmdSearch":
                this.navigate("cw-fleet-monitoring-alert-ticket-search", {keyword: this.keyword()});
                break;
            case "cmdCloseTicket":                 
                this.closeTicket();
                break;
            case "cmdExport":
                this.exportExcel();
        }
    }

    drawAlertOnMap(lat, lon, data) {

        data["svgIcon"] = this.svgs;

        MapManager.getInstance().drawAlertPoint(this.id, {
                lat: lat,
                lon: lon
            }, {
                url: this.resolveUrl("~/images/pin_alerts.png"),
                width: WebConfig.mapSettings.symbolWidth,
                height: WebConfig.mapSettings.symbolHeight,
                offset: {
                    x: WebConfig.mapSettings.symbolOffsetX,
                    y: WebConfig.mapSettings.symbolOffsetY
                },
                rotation: WebConfig.mapSettings.symbolRotation
            },
            WebConfig.mapSettings.defaultZoom,
            data);
    }

    closeTicket() {
        let lst = [];
        this.lstData.filter((item) => {
            if (item.isCheck && item.alertTicketStatus != Enums.ModelData.AlertTicketStatus.Close) {
                lst.push({
                    alertTicketId: item.id
                });
            }
        })

        if (lst['length'] != 0) {
            this.isBusy(true);
            this.webRequestAlert.alertTicketCloseTicket(lst).done((res) => {
                this.refreshAlertTicketList();
                this.publishMessage("cw-fleet-monitoring-alert-ticket-multi-close-alert-ticket");
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });
        }
    }

    exportExcel() {
        this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                this.isBusy(true);
                    let filter = this.filter();
                    filter.alertTicketStatusIds = this.statusId();
                    filter.keyword = this.keyword();
                    filter.companyId = WebConfig.userSession.currentCompanyId;
                    this.webRequestAlert.alertTicketExport(filter).done((res)=> {
                        ScreenHelper.downloadFile(res.fileUrl);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=> {
                        this.isBusy(false);
                    });
                break;
                default:
                break;
            }
        });
    }

    setObjDate(params) {
        let obj = null;
        let d = params.objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        let hour = d.getHours().toString();
        let minute = d.getMinutes().toString();
        let sec = "00";

        if(params.start) {
            hour = "00";
            minute = "00";
        } else if(params.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;  
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        obj = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;      

        return obj;
    }

    setObjDateWeek(params) {
        let obj = null;
        let d = params.objDate;
        let startWeek = parseInt(d.getDate() - (d.getDay() - 1));
        let endWeek = parseInt(d.getDate() + (7 - d.getDay()));
        
        if(params.start) {
            d.setDate(startWeek);
        } else if(params.end) {
            d.setDate(endWeek);
        } else { }        

        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        let hour = d.getHours().toString();
        let minute = d.getMinutes().toString();
        let sec = "00";
        
        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;  
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        obj = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;   
        obj = new Date(obj);

        return obj;
    }

    setObjDateMonth(params) {
        let obj = null;
        let d = params.objDate;
        
        if(params.start) {
            d.setDate(1);
        } else if(params.end) {
            d = Utility.addMonths(d, 1);
            d.setDate(1);
            d.setDate(parseInt(d.getDate() - 1));
        } else { }        

        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        let hour = d.getHours().toString();
        let minute = d.getMinutes().toString();
        let sec = "00";
        
        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;  
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        obj = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;   
        obj = new Date(obj);

        return obj;
    }

    get webRequestAlert() {
        return WebRequestAlert.getInstance();
    }
    get iqtechUtility() {
        return IQTechUtility.getInstance();
    }
    get hikvisionUtility() {
        return HikvisionUtility.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get encryptUtility() {
        return EncryptUtility.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketListScreen),
    template: templateMarkup
};