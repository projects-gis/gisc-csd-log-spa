﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenHelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAlertType from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestAlertConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import { AlertFilter } from "./infos";
import AssetInfo from "../../asset-info";

class AlertSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));

        this.alertType = ko.observable();
        this.alertConfiguration = ko.observable();
        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.businessUnitId = ko.observable(null);
        this.entityType = ko.observable();
        this.selectedEntity = ko.observable();
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnitId());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.alertTypeOptions = ko.observableArray([]);
        this.alertConfigurationOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.entityTypeOptions = ko.observableArray([]);
        this.selectedEntityOptions = ko.observableArray([]);
    }

    /**
     * Get WebRequest specific for Alert Type module in Web API access.
     * @readonly
     */
    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webrequestAlertConfiguration() {
        return WebrequestAlertConfiguration.getInstance();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.startDate(this.searchFilter.startDate);
        this.endDate(this.searchFilter.endDate);

        var dfdAlertType = this.webRequestAlertType.listAlertTypeByAlertConfig({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.AlertType],
            excludeAlertTypes: [
                Enums.ModelData.AlertType.InvalidDriver,
                Enums.ModelData.AlertType.TemperatureError]
        }).done((response) => {
            this.alertTypeOptions(response.items);
            if (this.searchFilter.alertTypeIds && this.searchFilter.alertTypeIds.length > 0) {
                this.alertType(ScreenHelper.findOptionByProperty(this.alertTypeOptions, "id", this.searchFilter.alertTypeIds[0]));
            }
        });

        var dfdAlertConfiguration = $.Deferred();
        $.when(dfdAlertType).done(() => {
            if (this.alertType()) {
                this.getListAlertConfiguration().done(() => {
                    if (this.searchFilter.alertConfigurationIds && this.searchFilter.alertConfigurationIds.length > 0) {
                        this.alertConfiguration(ScreenHelper.findOptionByProperty(this.alertConfigurationOptions, "id", this.searchFilter.alertConfigurationIds[0]));
                    }
                    dfdAlertConfiguration.resolve();
                }).fail((e) => {
                    dfdAlertConfiguration.reject(e);
                });
            }
            else {
                dfdAlertConfiguration.resolve();
            }
        });

        var dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary({ 
            companyId: WebConfig.userSession.currentCompanyId, 
            includeAssociationNames: [ EntityAssociation.BusinessUnit.ChildBusinessUnits ], 
            sortingColumns: DefaultSorting.BusinessUnit 
        }).done((response) => {
                this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(response.items));
                this.businessUnitOptions(response.items);
                if (this.searchFilter.businessUnitIds && this.searchFilter.businessUnitIds.length > 0) {
                    this.businessUnitId(this.searchFilter.businessUnitIds[0].toString());
                }
        });

        var dfdEntityType = this.webRequestEnumResource.listEnumResource({ 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: [ Enums.ModelData.EnumResourceType.SearchEntityType ], 
            sortingColumns: DefaultSorting.EnumResource 
        }).done((response) => {
            var entityTypeOptions = [];
            _.forEach(response.items, (o) => {
                //display only vehicle and deliveryman
                if (o.value === Enums.ModelData.SearchEntityType.Vehicle || o.value === Enums.ModelData.SearchEntityType.DeliveryMan) {
                    entityTypeOptions.push(o);
                }
            });
            this.entityTypeOptions(entityTypeOptions);
            if (this.searchFilter.searchEntityType) {
                this.entityType(ScreenHelper.findOptionByProperty(this.entityTypeOptions, "value", this.searchFilter.searchEntityType));
            }
        });

        var dfdSelectedEntity = $.Deferred();
        $.when(dfdBusinessUnit).done(() => {
            //get selectedEntity list after businessUnitId is done
            if (this.searchFilter.searchEntityType) {
                switch (this.searchFilter.searchEntityType) {
                    case Enums.ModelData.SearchEntityType.Vehicle:
                        this.getListVehicleSummary().done(() => {
                            if (this.searchFilter.searchEntityIds && this.searchFilter.searchEntityIds.length > 0) {
                                var selectedEntityId = this.searchFilter.searchEntityIds[0];
                                this.selectedEntity(ScreenHelper.findOptionByProperty(this.selectedEntityOptions, "id", selectedEntityId));
                            }
                            dfdSelectedEntity.resolve();
                        }).fail((e) => {
                            dfdSelectedEntity.reject(e);
                        });
                        break;
                    case Enums.ModelData.SearchEntityType.DeliveryMan:
                        this.getListDeliveryManSummary().done(() => {
                            if (this.searchFilter.searchEntityIds && this.searchFilter.searchEntityIds.length > 0) {
                                var selectedEntityId = this.searchFilter.searchEntityIds[0];
                                this.selectedEntity(ScreenHelper.findOptionByProperty(this.selectedEntityOptions, "id", selectedEntityId));
                            }
                            dfdSelectedEntity.resolve();
                        }).fail((e) => {
                            dfdSelectedEntity.reject(e);
                        });
                        break;
                    default:
                        dfdSelectedEntity.resolve();
                        break;
                }
            }
            else {
                dfdSelectedEntity.resolve();
            }
        });

        $.when(dfdAlertType, dfdAlertConfiguration, dfdBusinessUnit, dfdEntityType, dfdSelectedEntity).done(() => {
            this._alertTypeSubscribeRef = this.alertType.subscribe((alertType) => {
                if (alertType) {
                    this.isBusy(true);
                    this.getListAlertConfiguration().done(() => {
                        this.isBusy(false);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                }
                else {
                    this.alertConfigurationOptions([]);
                    this.alertConfiguration(null);
                }
            });

            this._entityTypeSubscribeRef = this.entityType.subscribe((entityType) => {
                if (entityType) {
                    this.getSelectedEntityOptions(entityType);
                }
                else {
                    this.selectedEntityOptions([]);
                }
            });

            this._businessUnitIdSubscribeRef = this.businessUnitId.subscribe((businessUnitId) => {
                if (this.entityType()) {
                    this.getSelectedEntityOptions(this.entityType());
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {

        this.alertType.extend({ trackChange: true });;
        this.alertConfiguration.extend({ trackChange: true });
        this.startDate.extend({ trackChange: true });
        this.endDate.extend({ trackChange: true });;
        this.businessUnitId.extend({ trackChange: true });
        this.entityType.extend({ trackChange: true });
        this.selectedEntity.extend({ trackChange: true });

        this.startDate.extend({ required: true });
        this.endDate.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            startDate : this.startDate,
            endDate : this.endDate
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {

            if(!this.validationModel.isValid()){
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.searchFilter = new AlertFilter(
                null,
                this.alertType() ? this.alertType().id : null,
                this.alertConfiguration() ? this.alertConfiguration().id : null,
                this.startDate(),
                this.endDate(),
                this.entityType() ? this.entityType().value : null,
                this.selectedEntity() ? this.selectedEntity().id : null
            );

             // Default selection is single.
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.businessUnitId());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    this.searchFilter.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    this.searchFilter.businessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.publishMessage("cw-fleet-monitoring-alert-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.alertType(null);
            this.alertConfiguration(null);
            this.startDate(null);
            this.endDate(null);
            this.businessUnitId(null);
            this.entityType(null);
            this.selectedEntity(null);
        }
    }

    /**
     * Call web request to get list of alert configuration
     * @returns
     */
    getListAlertConfiguration() {
        return this.webrequestAlertConfiguration.listAlertConfiguration({
            companyId: WebConfig.userSession.currentCompanyId,
            alertTypeIds: [this.alertType().id],
            includeAssociationNames: [
                EntityAssociation.AlertConfiguration.CustomPOI,
                EntityAssociation.AlertConfiguration.CustomPOICategory,
                EntityAssociation.AlertConfiguration.CustomArea,
                EntityAssociation.AlertConfiguration.CustomAreaCategory,
                EntityAssociation.AlertConfiguration.CustomAreaType,
                EntityAssociation.AlertConfiguration.Feature
            ],
            sortingColumns: DefaultSorting.AlertConfiguration
        }).done((response) => {
            this.alertConfigurationOptions(response.items);
        });
    }
    
    /**
     * Call web request to get list of vehicle
     * @returns
     */
    getListVehicleSummary() {

        var selectedBusinessUnitId = parseInt(this.businessUnitId());
        if(this.includeSubBusinessUnitEnable()){
            // If user included sub children then return all business unit ids.
            if(this.includeSubBusinessUnit()){
                selectedBusinessUnitId = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
            }
            else {
                // Single selection for business unit.
                selectedBusinessUnitId = [selectedBusinessUnitId];
            }
        }

        return this.webRequestVehicle.listVehicleSummary({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.businessUnitId() ? selectedBusinessUnitId : [],
            //enable: true,
            isAssociatedWithFleetService: true,
            sortingColumns: DefaultSorting.Vehicle
        }).done((response) => {
            Utility.applyFormattedPropertyToCollection(response.items, "displayText", "{0}", "license");
            this.selectedEntityOptions(response.items);
        });
    }

    
    /**
     * Call web request to get list of deliveryMan
     * @returns
     */
    getListDeliveryManSummary() {

        var selectedBusinessUnitId = parseInt(this.businessUnitId());
        if(this.includeSubBusinessUnitEnable()){
            // If user included sub children then return all business unit ids.
            if(this.includeSubBusinessUnit()){
                selectedBusinessUnitId = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
            }
            else {
                // Single selection for business unit.
                selectedBusinessUnitId = [selectedBusinessUnitId];
            }
        }

        return this.webRequestUser.listUserSummary({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.businessUnitId() ? selectedBusinessUnitId : [],
            //enable: true,
            userType: Enums.UserType.Company,
            isAssociatedWithMobileService:true,
            sortingColumns: DefaultSorting.User
        }).done((response) => {
            Utility.applyFormattedPropertyToCollection(response.items, "displayText", "{0}", "username");
            this.selectedEntityOptions(response.items);
        });
    }
    
    /**
     * Get selectedEntityOptions by entityType
     * @param {any} entityType
     */
    getSelectedEntityOptions(entityType) {
        switch (entityType.value) {
            case Enums.ModelData.SearchEntityType.Vehicle:
                this.isBusy(true);
                this.getListVehicleSummary().done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
            case Enums.ModelData.SearchEntityType.DeliveryMan:
                this.getListDeliveryManSummary().done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
            case Enums.ModelData.SearchEntityType.Driver:
                this.getListDriverSummary().done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertSearchScreen),
    template: templateMarkup
};