﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestAlert from "../../../../../app/frameworks/data/apitrackingcore/webrequestAlert";
import Utility from "../../../../../app/frameworks/core/utility";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestGlobalSearch from "../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";
import QuickSearchScreenBase from "../../../quick-search-screenbase";

class AlertDashboardScreen extends QuickSearchScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("FleetMonitoring_AlertDashboard")());
        this.bladeSize = BladeSize.Medium;
        this.bladeEnableQuickSearch = true;

        this.lastSync = ko.pureComputed(() => {
            return WebConfig.fleetMonitoring.alertDashboardLastSync();
        });

        this.items = ko.pureComputed(() => {
            return WebConfig.fleetMonitoring.alertDashboardItems();
        });

        this.displayTicket = ko.observable(WebConfig.userSession.hasPermission([
            Constants.Permission.Manage_Ticket,
            Constants.Permission.Ticket_View_Only
        ]) > 0);
        this.ticketNew = ko.observable(null);
        this.ticketInprogress = ko.observable(null);
        this.ticketClosed = ko.observable(null);
        this.ticketAll = ko.observable(null);
        this.keyword = ko.observable(null);

        this.subscribeMessage("cw-fleet-monitoring-alert-ticket-action-close", (obj) => {
            if (this.displayTicket()) {
                this.getAlertTicketDashboardData();
            }
        });

        this.subscribeMessage("cw-fleet-monitoring-alert-ticket-action-create-log", (data) => {
            if (this.displayTicket()) {
                this.getAlertTicketDashboardData();
            }
        });

        this.subscribeMessage("cw-fleet-monitoring-alert-ticket-multi-close-alert-ticket", () => {
            if (this.displayTicket()) {
                this.getAlertTicketDashboardData();
            }
        });
    }

    gotoAlertTicketList(status) {
        let statusId = [];
        switch (status) {
            case "new":
                statusId.push(Enums.ModelData.AlertTicketStatus.New);
                break;
            case "inprogress":
                statusId.push(Enums.ModelData.AlertTicketStatus.Inprogress);
                break;
            case "closed":
                statusId.push(Enums.ModelData.AlertTicketStatus.Close);
                break;
            case "all":
                statusId = null;
                break;
            default:
                break;
        }
        this.navigate("cw-fleet-monitoring-alert-ticket", {
            statusId: statusId,
            keyword: this.keyword()
        });
    }

    /**
     * Get WebRequest specific for alert module in Web API access.
     * @readonly
     */
    get webRequestAlert() {
        return WebRequestAlert.getInstance();
    }

    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        this.minimizeAll(false);

        if (this.displayTicket())
            this.getAlertTicketDashboardData();

        var dfd = $.Deferred();

        //always sync alert dashboard when user open screen
        WebConfig.fleetMonitoring.syncAlertDashboard(this.webRequestAlert).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    getAlertTicketDashboardData(keyword) {
        this.isBusy(true);
        this.webRequestAlert.getAlertTicketDashboard(keyword).done((res) => {
            this.ticketNew(res.formatNewAlertTickets);
            this.ticketInprogress(res.formatInproAlertTickets);
            this.ticketClosed(res.formatCloseAlertTickets);
            this.ticketAll(res.formatTotalAlertTickets);
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    getAlertTicketDashboardDataWithKeyword(keyword) {
        this.isBusy(true);
        this.webRequestAlert.getAlertTicketDashboardWithKeyword(keyword).done((res) => {
            this.ticketNew(res.formatNewAlertTickets);
            this.ticketInprogress(res.formatInproAlertTickets);
            this.ticketClosed(res.formatCloseAlertTickets);
            this.ticketAll(res.formatTotalAlertTickets);
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
        //commands.push(this.createCommand("cmdTicket", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
                this.isBusy(true);
                let keyword = this.keyword();

                //set keyword current search
                this.quickSearchKeyword(keyword);

                if (this.displayTicket()) {
                    this.getAlertTicketDashboardData(keyword);
                }

                WebConfig.fleetMonitoring.syncAlertDashboard(this.webRequestAlert, keyword).done(() => {
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                    this.handleError(e);
                });
                break;
            case "cmdTicket":
                this.navigate("cw-fleet-monitoring-alert-ticket");
                break;
        }
    }

    /**
     * @lifecycle Handle when search on text box is clicked or enter.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        let keyword = this.quickSearchKeyword();
        if( 
            (event.keyCode === 13 || event.type === 'click') && 
            (_.size(keyword) >= data.minLength || _.size(keyword) === 0) 
        ){
            this.isBusy(true);

            //set keyword for refresh (optional)
            this.keyword(keyword);

            //call dashboard with kyword
            if(this.displayTicket()){
                this.getAlertTicketDashboardData(keyword);
            }
            WebConfig.fleetMonitoring.syncAlertDashboard(this.webRequestAlert, keyword).done(() => {
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
            });
        }
        return true;
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: request.term,
        }
        this.webRequestGlobalSearch.autocompleteDriverVehicle(filter).done((data) => {
            response( $.map( data, (item) => {
                return {
                    label: item,
                    value: '"' + item + '"'
                };
            }));
        });      
    }

    /**
     * 
     * Hook on alert item selected
     * @param {any} alertTypeId
     * 
     */
    onItemSelected(alertTypeId) {
        var datetime = WebConfig.fleetMonitoring.alertDashboardLastSyncDatetime();
        this.navigate("cw-fleet-monitoring-alert-list", {
            alertTypeId: alertTypeId,
            datetime: datetime ? Utility.resetTime(datetime) : null,
            keyword: this.keyword()
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertDashboardScreen),
    template: templateMarkup
};