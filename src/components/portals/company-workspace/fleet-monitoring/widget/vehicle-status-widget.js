﻿import ko from "knockout";
import ScreenBaseWidget from "../../../screenbasewidget";
import templateMarkup from "text!./vehicle-status-widget.html";

class VehicleStatusWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        this.items = ko.observableArray([]);

        this.renderMovementColumn = (data) =>{
            let node = null;
            if(data){
                let img = '<img src="' + data.icon + '" style="height: 28px;" />';
                node = '<div title="' + data.description + '" style="text-align:center;" >' + img + '</div>';
            }
            return node;
        }
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

        let items = new Array();
        items = [
            {
                id: 1,
                name: "Move",
                icon: this.resolveUrl("~/images/icon_nearest_assets_movement_move@3x.png"),
                description: this.i18n("Vehicle_Status_Description_Move")(),
                detail: this.i18n("Vehicle_Status_Detail_Move")()
            },
            {
                id: 2,
                name: "Move with Trip time over 3.5 hrs",
                icon: this.resolveUrl("~/images/icon_vehicle_over3.5.png"),
                description: this.i18n("Vehicle_Status_Description_Move_Over3_5")(),
                detail: this.i18n("Vehicle_Status_Detail_Move_Over3_5")()
            },
            {
                id: 3,
                name: "Move with Trip time over 4 hrs",
                icon: this.resolveUrl("~/images/icon_vehicle_over4.png"),
                description: this.i18n("Vehicle_Status_Description_Move_Over4")(),
                detail: this.i18n("Vehicle_Status_Detail_Move_Over4")()
            },
            {
                id: 4,
                name: "Stop",
                icon: this.resolveUrl("~/images/icon_nearest_assets_movement_stop@3x.png"),
                description: this.i18n("Vehicle_Status_Description_Stop")(),
                detail: this.i18n("Vehicle_Status_Detail_Stop")()
            },
            {
                id: 5,
                name: "Park",
                icon: this.resolveUrl("~/images/icon_nearest_assets_movement_park@3x.png"),
                description: this.i18n("Vehicle_Status_Description_Park")(),
                detail: this.i18n("Vehicle_Status_Detail_Park")()
            },
            {
                id: 6,
                name: "Park Engine On",
                icon: this.resolveUrl("~/images/icon_nearest_assets_movement_idle@3x.png"),
                description: this.i18n("Vehicle_Status_Description_ParkEngineOn")(),
                detail: this.i18n("Vehicle_Status_Detail_ParkEngineOn")()
            }
        ];

        this.items(items);

    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = this.items();
        var totalItems = items.length;
        dfd.resolve({items: items, totalRecords: totalItems, currentPage: 1});
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }


}

export default {
    viewModel: ScreenBaseWidget.createFactory(VehicleStatusWidget),
    template: templateMarkup
};