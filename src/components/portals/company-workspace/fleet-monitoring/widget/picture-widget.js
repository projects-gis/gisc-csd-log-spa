﻿import ko from "knockout";
import ScreenBaseWidget from "../../../screenbasewidget";
import templateMarkup from "text!./picture-widget.html";

class FleetCommonPictureWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        this.params = this.ensureNonObservable(params);
        this.src = ko.observable();
        this.alertDateTime = ko.observable();
        this.alertTypeDisplayName = ko.observable();

        this.src(params.videoUrl);
        this.alertDateTime(params.formatAlertDateTime);
        this.alertTypeDisplayName(params.alertTypeDisplayName);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

    }

    /**
     * Handle when user clicks custom action.
     * 
     * @param {any} e 
     */
    onCustomActionClick(e) {
        console.log(e);
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.params = null;
        this.src = null;
    }


}

export default {
    viewModel: ScreenBaseWidget.createFactory(FleetCommonPictureWidget),
    template: templateMarkup
};