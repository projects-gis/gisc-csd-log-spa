﻿import ko from "knockout";
import templateMarkup from "text!./liveview.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import Utility from "../../../../../app/frameworks/core/utility";
import HikvisionUtility from "../../../../../app/frameworks/core/hikvisionUtility";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestUserApiCore from "../../../../../app/frameworks/data/apicore/webRequestUser";
import {
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import {
    BusinessUnitFilter
} from "../../businessUnitFilter";
import AssetInfo from "../../asset-info";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";

import WebRequestMDVR from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVR";
import IQTechUtility from "../../../../../app/frameworks/core/iqtechUtility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import {
    WebControl
} from "hikvision";
import EncryptUtility from "../../../../../app/frameworks/core/encryptUtility";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";


class LiveViewScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n('FleetMonitoring_Live_Video')());
        this.bladeSize = BladeSize.Medium;
        this.keyword = ko.observable();


        //BU
        this.businessUnit = ko.observable(null);
        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {
            return !_.isEmpty(this.businessUnit());
        });
        this.includeSubBusinessUnit = ko.observable(false);
        this.businessUnitFilter = new BusinessUnitFilter();

        this.findEnable = ko.pureComputed(() => {
            return this.businessUnit.isValid();
        });


        //DT Vehicle
        this.vehicles = ko.observableArray([]);
        // this.selectedVehicles = ko.observableArray([]);
        this.orderVehicle = ko.observable([
            [2, "asc"]
        ]);


        this.selectedAssets = ko.observableArray();

        this.isVehicle = false;
        this.isDeliveryMan = false;
        this.selectedFromMap = ko.observable();

        this.data = ko.observableArray([]);

        this.mdvrConfig = ko.observable();
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        this.tempViewVehicleIds = ko.observableArray([]);
        this.tempViewDeliveryManIds = ko.observableArray([]);
        this.tempFollowVehicleIds = ko.observableArray([]);
        this.tempFollowDeliveryManIds = ko.observableArray([]);
        this.tempTrackLocationFilter = ko.observable();

        this.assetsChanged = (newValue, targetField) => { 
            

            let fData = _.filter(this.selectedAssets(), (id)=>{
                return id == newValue.id
            });

       
            // if View is unchecked, Follow need to be unchecked.
            if (targetField === "isView" && newValue.isView) {
                if(!_.size(fData)){
                    this.selectedAssets.push(newValue.id);
                }
                newValue.isFollow = false;
            }

            // if View is unchecked, Follow need to be unchecked.
            if (targetField === "isView" && !newValue.isView) {
                let newIds = _.remove(this.selectedAssets(), (id) => {
                    return id != newValue.id;
                });
                this.selectedAssets(_.clone(newIds));
                newValue.isFollow = false;
            }

            // if Follow is checked, View need to be checked.
            if (targetField === "isFollow" && newValue.isFollow) {
                if(!_.size(fData)){
                    this.selectedAssets.push(newValue.id);
                }
                newValue.isView = true;
            }
        
        }

    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.minimizeAll(false);

        var dfd = $.Deferred();




        var d0; //= null;

        if (this.id) {
            var str = this.id;
            var prefix = str.charAt(0);
            this.id = str.replace(prefix + '-', '');

            switch (prefix) {
                case "V":
                    d0 = this.webRequestVehicle.getVehicle(this.id);
                    this.isVehicle = true;
                    break;
            }

        } else {
            d0 = null;
        }

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
       
        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.businessUnitFilter);

        $.when(d0, d1,dfdCompanySettings).done((r0, r1,resCompanySettings) => {
            var dfdFindResult = null;
            var bu = r1["items"];
            this.mdvrConfig(resCompanySettings)
            this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(bu));
            this.businessUnitOptions(bu);

            if (r0) {
                this.businessUnit(r0.businessUnitId.toString());

                this.selectedFromMap(r0.id);
                dfdFindResult = this.findResult();
            }

            $.when(dfdFindResult).done(() => {
                dfd.resolve();
            });


        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().closeLiveView();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    get webRequestMDVR() {
        return WebRequestMDVR.getInstance();
    }
    get hikvisionUtility() {
        return HikvisionUtility.getInstance();
    }

    get iqtechUtility() {
        return IQTechUtility.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    get encryptUtility() {
        return EncryptUtility.getInstance();
    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUserApiCore() {
        return WebRequestUserApiCore.getInstance();
    }

    

    /**
     * Register observable property for change detection, validatiors.
     */
    setupExtend() {
        var self = this;
        //trackChange
        // this.keyword.extend({
        //     trackChange: true
        // });
        // this.businessUnit.extend({
        //     trackChange: true
        // });
        // this.vehicles.extend({
        //     trackArrayChange: true
        // });

        //validation
        this.businessUnit.extend({
            required: true
        });
        this.selectedAssets.extend({
            // maxArrayLength: {
            //     params: 6,
            //     message: this.i18n("M253")()
            // },
            maxLength: {
                message: this.i18n("M253")(),
                onlyIf: function () {
                    return _.size(self.selectedAssets()) > 6;
                },
                params: 6
            },
            arrayRequired: {
                params: true,
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            businessUnit: this.businessUnit,
            selectedAssets: this.selectedAssets
        });
    }

    /**
     * Find all selected business unit ids include child.
     * @returns 
     */
    findBusinessUnitIDs() {
        var result = [];

        // Default selection is single.
        if (this.includeSubBusinessUnitEnable()) {
            var selectedBusinessUnitId = parseInt(this.businessUnit());

            // If user included sub children then return all business unit ids.
            if (this.includeSubBusinessUnit()) {
                result = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
            } else {
                // Single selection for business unit.
                result = [selectedBusinessUnitId];
            }
        }

        return result;
    }

    /**
     * Find drivers and vehicles list.
     * 
     * @returns
     * 
     * @memberOf PlaybackScreen
     */
    findResult() {

        this.isBusy(true);
        // this.selectedVehicles.removeAll();
        var dfd = $.Deferred();

        // var vehicleFilter = {
        //     companyId: this.businessUnitFilter.companyId,
        //     businessUnitIds: this.findBusinessUnitIDs(),
        //     enable: true,
        //     isAssociatedWithFleetService: true,
        //     keyword: this.keyword()
        // };
        let filter = {
            // "businessUnitId": 1,
            // "includeSubBusinessUnitId": true
            businessUnitId: this.businessUnit(),
            includeSubBusinessUnitId: this.includeSubBusinessUnit(),
            keyword: this.keyword()


        }

        //var d1 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);
        var d2 = this.webRequestMDVR.listMDVRLiveVdo(filter);

        $.when(d2).done((r2) => {
            this.isBusy(false);
            //var vehicles = this.getVehicleDatasource(r1["items"]);
            var vehicles = this.getVehicleDatasource(r2);
            // var id = this.selectedFromMap();



            // if (this.isVehicle) {
            //     _.each(vehicles, (v) => {
            //         if (v.id === id) {
            //             this.selectedVehicles.push(v);
            //         }
            //     });

            // }

   
            this.vehicles(vehicles);
            // this.isVehicle = false;

            dfd.resolve();
        }).fail((e) => {
            this.isBusy(false);
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Format displayable vehicle.
     * @param {any} items
     * @returns
     */
    getVehicleDatasource(items) {
        Utility.applyFormattedPropertyToCollection(items, "vehicleModelDisplayText", "{0} ({1})", "license", "driverName");
        return items;
    }

    /**
     * update track location
     * if live view is true then replace live view to track loc
     * if live view is false then replace track loc to live view
     */
    replaceTrackLocation(liveView = true){
        var dfd = $.Deferred();
        if(liveView){

            this.tempViewVehicleIds(_.clone(WebConfig.fleetMonitoring.trackLocationViewVehicleIds()));
            this.tempViewDeliveryManIds(_.clone(WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds()));
            this.tempFollowVehicleIds(_.clone(WebConfig.fleetMonitoring.trackLocationFollowVehicleIds()));
            this.tempFollowDeliveryManIds(_.clone(WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds()));
            this.tempTrackLocationFilter(_.clone(WebConfig.fleetMonitoring.trackLocationFilter()));
   
            let newViewVehicleIds = new Array();
            let newFollowVehicleIds = new Array();
            _.forEach(this.vehicles(), (item)=>{
                if(item.isView){
                    newViewVehicleIds.push(item.id);
                }
                if(item.isFollow){
                    newFollowVehicleIds.push(item.id);
                }
                
            });
    
            // WebConfig.fleetMonitoring.trackLocationViewVehicleIds(newViewVehicleIds);
            // WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds([]);
            // WebConfig.fleetMonitoring.trackLocationFollowVehicleIds(newFollowVehicleIds);
            // WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds([]);
            WebConfig.fleetMonitoring.trackLocationFilter({
                deliveryManIds: [],
                // displayLength: 0,
                vehicleIds: []
            });
            this._updateTrackLocationAssetIds(
                //viewVehicleIds
                newViewVehicleIds,
                //viewDeliveryManIds
                [],
                //followVehicleIds
                newFollowVehicleIds,
                //followDeliveryManIds
                []
            ).done(()=>{
                dfd.resolve();
            });
            

        }else{
 

            // WebConfig.fleetMonitoring.trackLocationViewVehicleIds(this.tempViewVehicleIds());
            // WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds(this.tempViewDeliveryManIds());
            // WebConfig.fleetMonitoring.trackLocationFollowVehicleIds(this.tempFollowVehicleIds());
            // WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds(this.tempFollowDeliveryManIds());
            WebConfig.fleetMonitoring.trackLocationFilter(this.tempTrackLocationFilter());
            this._updateTrackLocationAssetIds(
                //viewVehicleIds
                this.tempViewVehicleIds(),
                //viewDeliveryManIds
                this.tempViewDeliveryManIds(),
                //followVehicleIds
                this.tempFollowVehicleIds(),
                //followDeliveryManIds
                this.tempFollowDeliveryManIds()
            ).done(()=>{
                dfd.resolve();
            })
            
        }

        return dfd;

    }

    /**
     * Update Track Location Asset Ids
     */
    _updateTrackLocationAssetIds(viewVehicleIds, viewDeliveryManIds, followVehicleIds, followDeliveryManIds) {
        return WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
            this.webRequestFleetMonitoring,
            WebConfig.userSession,
            viewVehicleIds,
            viewDeliveryManIds,
            followVehicleIds,
            followDeliveryManIds
        ).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    rememberVehicles(){

        
        

        // Loading preferences from server.
        var preferenceFilter = {keys: [Enums.ModelData.UserPreferenceKey.DataGridState]};
        var dfdPreferenceRequest  = this.webRequestUser.getUserPreferences(preferenceFilter);
        dfdPreferenceRequest.done((res)=>{
            
            let liveViewData = {
                viewVehicleIds: _.clone(WebConfig.fleetMonitoring.trackLocationViewVehicleIds()),
                followVehicleIds: _.clone(WebConfig.fleetMonitoring.trackLocationFollowVehicleIds()),
                businessUnitId: this.businessUnit(),
                includeSubBusinessUnitId: this.includeSubBusinessUnit()
            }
       
            let jdata = (_.size(res)) ? res[0].value : "[]";
            let data = JSON.parse(jdata);
            let liveViewId = "MDVR-LiveView";

            let fData = _.filter(data, (o)=>{
                return o.id == liveViewId
            });

            if(_.size(fData)){
                _.forEach(data, (item)=>{
                    if(item.id == liveViewId){
                        item.data = liveViewData
                    }
                })
            }else{
                data.push({
                    id: liveViewId,
                    data: liveViewData
                });
            }
      
            var model =
            [
                {
                    //UserPreferenceKey DataGridState (Key 8) approve by p' Eed because this key is small data
                    key: Enums.ModelData.UserPreferenceKey.DataGridState,
                    value: JSON.stringify(data),
                    infoStatus: Enums.InfoStatus.Update
                }
            ];
      
            this.webRequestUserApiCore.updateUserPreferences(model)
            .done((res)=>{
            })
            .fail((e) => {
                this.handleError(e);
            });

        });
        
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actLiveView", this.i18n('Common_Display')()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        // switch (this.mdvrConfig().mdvrModel) {
        //     case Enums.ModelData.MDVRModel.All:
        //         commands.push(this.createCommand("cmdDownloadUserGuideIQTech", this.i18n("Download_User_Guide")()+" (IQTech)", "svg-icon-load-guide"));
        //         commands.push(this.createCommand("cmdDownloadUserGuideHikvision", this.i18n("Download_User_Guide")()+" (Hikvision)", "svg-icon-start-guide"));
        //         commands.push(this.createCommand("cmdDownloadPluginIQTech", this.i18n("Download_Plugin")()+" (IQTech)", "svg-icon-load-plugin"));
        //         commands.push(this.createCommand("cmdDownloadUserGuide", this.i18n("Download_Plugin")()+" (Hikvision)", "svg-icon-load-plugin"));
        //         commands.push(this.createCommand("cmdDownloadPlugin", this.i18n("Start_Plugin")(), "svg-icon-start-plugin"));
        //         break;
        
        //     case Enums.ModelData.MDVRModel.HIKvision:
        //         commands.push(this.createCommand("cmdDownloadUserGuideHikvision", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
        //         commands.push(this.createCommand("cmdDownloadPlugin", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));
        //         commands.push(this.createCommand("cmdStartPlugin", this.i18n("Start_Plugin")(), "svg-icon-start-plugin"));
        //         break;
        //     case Enums.ModelData.MDVRModel.IQTech:
        //         commands.push(this.createCommand("cmdDownloadUserGuideIQTech", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
        //         commands.push(this.createCommand("cmdDownloadPluginIQTech", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));
        //         break;
        // }
        
        commands.push(this.createCommand("cmdClearViewAll", this.i18n("Common_ClearAll")(), "svg-cmd-clear-all"));
        commands.push(this.createCommand("cmdClearFollow", this.i18n("FleetMonitoring_ClearFollow")(), "svg-cmd-clear-follow"));

       // commands.push(this.createCommand("cmdStartIQTech", this.i18n("IQTech")(), "svg-icon-start-plugin"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onCommandClick(sender) {
        switch (sender.id) {
            // case "cmdDownloadPlugin":
            //     ScreenHelper.downloadFile(this.resolveUrl('~/vendors/hikvision/PlatformSDKWebControlPlugin.zip'));
            //     break;
            // case "cmdDownloadUserGuideHikvision":
            //     ScreenHelper.downloadFile(this.resolveUrl('~/vendors/hikvision/LiveviewEventviewUserGuide.pdf'));
            //     break;
            // case "cmdStartPlugin":
            //     this.hikvisionUtility.startPlugin();
            //     break;
            // case "cmdDownloadUserGuideIQTech":
            //     ScreenHelper.downloadFile(this.resolveUrl('~/vendors/iqtech/LiveviewEventviewUserGuide_iqtech.pdf'));
            //     break;
            // case "cmdDownloadPluginIQTech":
            //     window.open('https://get.adobe.com/flashplayer/')
            //     break;
            case "cmdClearViewAll":
                    let clearViewAll = _.map(this.vehicles(), (item)=>{
                        item.isView = false;
                        item.isFollow = false;
                        return item;
                    });
                    this.selectedAssets.removeAll();
                    this.vehicles.replaceAll(clearViewAll);
                    // this.dispatchEvent("dtFleetMonitoringLiveView", "refresh");
                break;
            case "cmdClearFollow":
                    let clearFollow = _.map(this.vehicles(), (item)=>{
                        item.isFollow = false;
                        return item;
                    });
                    this.vehicles.replaceAll(clearFollow);
                    // this.dispatchEvent("dtFleetMonitoringLiveView", "refresh");
                break;
        }
    }
    onActionClick(sender) {

        super.onActionClick(sender);
        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }

        if (sender.id === "actLiveView") {
            localStorage.removeItem("dataMDVR");
            localStorage.removeItem("dataIQTech");
            let windowWidget = $('#cw-fleet-monitoring-liveview-widget');
            if(windowWidget.length > 0){
                $('#cw-fleet-monitoring-liveview-widget').data("kendoWindow").close();
            }
            
            var vehicleInfo = new Array();
            var promises = new Array();
            
            _.forEach(this.vehicles(), (item) => {

                if(item.isView){
                
                    //let vehicle = (item.brand == Enums.ModelData.MDVRModel.IQTech) ? this.iqtechUtility.getDevIdNo(item.deviceId.toString(),this.encryptUtility.decryption(item.mdvrServerKey)) :  this.hikvisionUtility.liveView(item.deviceId.toString());
                    let vehicle;

                    if(item.brand == Enums.ModelData.MDVRModel.IQTech){
                        vehicle = this.iqtechUtility.getDevIdNo(item.deviceId.toString(),this.encryptUtility.decryption(item.mdvrServerKey));
                    }else if(item.brand == Enums.ModelData.MDVRModel.DFI){
                        vehicle = this.iqtechUtility.getDevIdNo(item.deviceId.toString(),this.encryptUtility.decryption(item.mdvrServerKey));
                    }else{
                        vehicle =  this.hikvisionUtility.liveView(item.deviceId.toString());
                    }

                    
                    let info = vehicle.done((res)=>{
                        return new Promise((resolve, reject) =>{
                            item.cameraConfig = this.encryptUtility.decryption(item.mdvrServerKey);
                            let obj = Object.assign({}, item, res);
                            vehicleInfo.push(obj);
                            resolve();
                        });
                    }).fail((e)=>{
                        console.log(e);
                        reject(e);
                        this.handleError(e);
                    });
                    promises.push(info);
                }
            });
      
            Promise.all(promises).then((res) => {
                this.isBusy(true);

                this.replaceTrackLocation().done(()=>{

                    WebConfig.fleetMonitoring.syncTrackLocation(
                        WebRequestFleetMonitoring.getInstance(),
                        WebConfig.userSession,
                        WebConfig.fleetMonitoring.trackLocationFilter())
                    .done((resultTrackLocation) => {
                        let vehicles = WebConfig.fleetMonitoring.trackLocationItems();
                      
                        _.forEach(vehicleInfo, (item)=>{
                            let fData = _.find(vehicles, (v)=>{
                                return v.vehicleId == item.id;
                            });
                            if(_.size(fData)){
                                item.location = ko.observable(fData.location);
                                item.latitude = ko.observable(fData.latitude);
                                item.longitude = ko.observable(fData.longitude);
                                item.businessUnitName = fData.businessUnitName;
                            }else{
                                item.location = ko.observable("");
                                item.latitude = ko.observable("");
                                item.longitude = ko.observable("");
                                item.businessUnitName = "";
                            }

                        });
                        this.isBusy(false);
             
                        this.rememberVehicles();
                        let width = $( window ).width() - 604;
                        width = width > 604 ? width : 0;

                          
                            this.widget('cw-fleet-monitoring-liveview-widget', vehicleInfo, {
                                title: this.i18n("FleetMonitoring_Live_Video")(),
                                modal: false,
                                resizable: true,
                                minimize: false,
                                maximize: true,
                                target: "cw-fleet-monitoring-liveview-widget",
                                width: "640",
                                height: $( window ).height() - 110,
                                id: 'cw-fleet-monitoring-liveview-widget',
                                left: width - 90,
                                bottom: "1%",
                                onClose: () => {
                                    this.isBusy(true);
                                    MapManager.getInstance().clear();
                
                                    this.replaceTrackLocation(false).done(()=>{
                                        this.isBusy(false);
                                    });
                                }
                            });

                    });
                });
                
            }).catch((e)=>{
                console.log(e);
                this.isBusy(false);
            });
        }
    }


    formatData(ids, data) {
        var obj = [];
        var key = {};
        //make new object 
        ids.map((item, index) => {
            key[item.deviceId] = index;
            obj[key[item.deviceId]] = {
                id: item.deviceId,
                vehicleLicense: `${item.license} (${item.driverName})`,
                data: []
            }

        });

        // data.map((res, index) => {
        //     obj[key[res.deviceId]].data.push(res)
        // })
        //filter by id
        obj.forEach((element, index) => {
            data.filter((res) => {
                if (res.deviceId == element.id) {
                    obj[index].data.push(res)
                }
            })
        });



        return obj;
    }


    onMapComplete(data) {
        switch (data.command) {
            case "open-liveview":
                this.minimizeAll();
                break;
            case "open-live-view-iqtech":
                window.open(WebConfig.appSettings.liveView2, "liveViewPage");
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(LiveViewScreen),
    template: templateMarkup
};