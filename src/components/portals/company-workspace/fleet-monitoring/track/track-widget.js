﻿import ko from "knockout";
import ScreenBaseWidget from "../../../screenbasewidget";
import templateMarkup from "text!./track-widget.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestReportTemplate from "../../../../../app/frameworks/data/apitrackingcore/webrequestReportTemplate";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import Logger from "../../../../../app/frameworks/core/logger";
import IconFollowTemplateMarkup from "text!./../../../../svgs/icon-follow.html";
import AssetUtility from "../../asset-utility";
import WebRequestGlobalSearch from "../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";

class TrackWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);

        var self = this;
        this.quickSearchKeyword = ko.observable();
        this.keyword = ko.observable();
        // Turn on or of automatic update to datatable when SignalR changed.
        this.automaticUpdate = true;
        this.columns = [];
        this.onRemoveTrackLocationClick = (data, e) => {
            Logger.info("onRemoveTrackLocationClick");
            var viewVehicleIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
            var viewDeliveryManIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
            var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
            var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
            var datasource = WebConfig.fleetMonitoring.trackLocationItems().slice();
            var list = datasource;

            if (data.vehicleId) {
                let vId = _.find(viewVehicleIds, function(o) { return o == data.vehicleId; });
                _.remove(viewVehicleIds, (o) => {
                    return o == data.vehicleId;
                });
                _.remove(followVehicleIds, (o) => {
                    return o == data.vehicleId;
                });
                //set new datasource
                if(!vId){
                    _.remove(datasource, function(o) {
                        return o.vehicleId == data.vehicleId;
                    });
                    list = datasource;
                }
            }
            else {
                let dId = _.find(viewDeliveryManIds, function(o) { return o == data.deliveryManId; });
                _.remove(viewDeliveryManIds, (o) => {
                    return o == data.deliveryManId;
                });
                _.remove(followDeliveryManIds, (o) => {
                    return o == data.deliveryManId;
                });
                //set new datasource
                if(!dId){
                    _.remove(datasource, function(o) {
                        return o.deliveryManId == data.deliveryManId;
                    });
                    list = datasource;
                }
            }

            WebConfig.fleetMonitoring.trackLocationItems(list);

            this._updateTrackLocationAssetIds(
                viewVehicleIds,
                viewDeliveryManIds,
                followVehicleIds,
                followDeliveryManIds
            );
        };
        this.onRemoveAllTrackLocationClick = (items, e) => {
            Logger.info("onRemoveAllTrackLocationClick");
            if(!_.isEmpty(items) && _.isArray(items)){
                var viewVehicleIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
                var viewDeliveryManIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
                var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
                var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
                var datasource = WebConfig.fleetMonitoring.trackLocationItems().slice();
                var list = datasource;
                 _.forEach(items, (item) => {
                   var target = _.find(datasource, function(o) { return o.id == item.id; });
                   if(target){
                       if(target.vehicleId){
                            let vId = _.find(viewVehicleIds, function(o) { return o == target.vehicleId; });
                            _.remove(viewVehicleIds, (o) => {
                                return o === target.vehicleId;
                            });
                            _.remove(followVehicleIds, (o) => {
                                return o == target.deliveryManId;
                            });
                            //set new datasource
                            if(!vId){
                                _.remove(datasource, function(o) {
                                    return o.vehicleId == target.vehicleId;
                                });
                                list = datasource;
                            }
                       }
                       else {
                            let dId = _.find(viewDeliveryManIds, function(o) { return o == target.deliveryManId; });
                             _.remove(viewDeliveryManIds, (o) => {
                                return o === target.deliveryManId;
                            });
                            _.remove(followDeliveryManIds, (o) => {
                                return o == target.deliveryManId;
                            });
                            //set new datasource
                            if(!dId){
                                _.remove(datasource, function(o) {
                                    return o.deliveryManId == target.deliveryManId;
                                });
                                list = datasource;
                            }
                        }
                   }
                });

                WebConfig.fleetMonitoring.trackLocationItems(list);

                this._updateTrackLocationAssetIds(
                    viewVehicleIds,
                    viewDeliveryManIds,
                    followVehicleIds,
                    followDeliveryManIds
                );

            }
        };
        this.onFollowCheckboxDataChanged = (items, e) => {
            Logger.info("onFollowCheckboxDataChanged");
            if(!_.isEmpty(items) && _.isArray(items)){
                var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
                var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();

                var datasource = WebConfig.fleetMonitoring.trackLocationItems().slice();
                
                _.forEach(items, (item) => {
                   var target = _.find(datasource, function(o) { return o.id == item.id; });
                   if(target){
                       if(target.vehicleId){
                           if (item.value) {
                                followVehicleIds.push(target.vehicleId);
                            }
                            else {
                                _.remove(followVehicleIds, (o) => {
                                    return o === target.vehicleId;
                                });
                            }
                       }
                       else {
                            if (item.value) {
                                followDeliveryManIds.push(target.deliveryManId);
                            }
                            else {
                                _.remove(followDeliveryManIds, (o) => {
                                    return o === target.deliveryManId;
                                });
                            }
                        }
                   }
                });

                this._updateTrackLocationAssetIds(
                    WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice(),
                    WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice(),
                    followVehicleIds.slice(),
                    followDeliveryManIds.slice()
                );
            }
        };
        this.onSelectingRow = (trackLocation, e) => {
            Logger.info("onSelectingRow");
            if(trackLocation) {
                //แผนที่จะ pan ไปยังจุดของยานพาหนะ หรือ คนส่งของ
                self.mapManager.zoomPoint(self.id, trackLocation.latitude, trackLocation.longitude, WebConfig.mapSettings.defaultZoom);
                self.minimizeAll();
            }
        };

        // Subscribe when track location items changed
        this._trackLocationItemsRef = WebConfig.fleetMonitoring.trackLocationItems.ignorePokeSubscribe(() => {
            if(this.automaticUpdate) {
                Logger.log("Update track location by signalR");
                this.dispatchEvent("trackWidgetMonitoring", "refresh");
            }
        });
    }

    /**
     * 
     * Get MapManager
     * @readonly
     */
    get mapManager() {
        return MapManager.getInstance();
    }

    /**
     * Get WebRequest specific for ReportTemplate module in Web API access.
     * @readonly
     */
    get webRequestReportTemplate() {
        return WebRequestReportTemplate.getInstance();
    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * Get WebRequest specific for GlobalSearch module in Web API access.
     * @readonly
     */
    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
 
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var dfdReportTemplate = this.getReportTemplate();

        $.when(dfdReportTemplate).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        let keyword = this.quickSearchKeyword();
        if( 
            (event.keyCode === 13 || event.type === 'click') && 
            (_.size(keyword) >= data.minLength || _.size(keyword) === 0) 
        ){
            this.keyword(keyword);
            this.isBusy(true);
            this.refreshTrackLocation().done(() => {
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
        return true;
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: request.term,
        }
        this.webRequestGlobalSearch.autocompleteDriverVehicle(filter).done((data) => {
            response( $.map( data, (item) => {
                return {
                    label: item,
                    value: '"' + item + '"'
                };
            }));
        });      
    }

    /**
     * Handle when user clicks custom action.
     * 
     * @param {any} e 
     */
    onCustomActionClick(e) {
        let showAll = $(e.target).hasClass('k-i-all');
        let removeAll = $(e.target).hasClass('k-i-delete');
        if(showAll){
            this.onViewAllButtonClick();
        }
        if(removeAll){
            this.removeAllTrackLocation();
        }
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = WebConfig.fleetMonitoring.trackLocationItems().slice();
        //console.log("onDatasourceRequestRead: ", items);
        var totalItems = items.length;

        if(_.isEmpty(gridOption.sort)){
            gridOption.sort = [{field: "dateTime", dir: "desc" }];
        }
        var columnDefinition = _.find(this.columns, {data: gridOption.sort[0].field});

        // Required map back from grid to web service with enum id.
        if(columnDefinition){  
            gridOption.sort[0].field = columnDefinition.sortData;
        } 

        var data = AssetUtility.query(items, gridOption.take, gridOption.skip, gridOption.sort);
        
        let currPage = AssetUtility.currentPage(items, gridOption.page, gridOption.pageSize);

        dfd.resolve({items: data, totalRecords: totalItems, currentPage: currPage});

        return dfd;
    }

    /**
     * Get report template fields
     */
    getReportTemplate() {
        return this.webRequestReportTemplate.listReportTemplate({ 
            reportTemplateType: Enums.ModelData.ReportTemplateType.TrackMonitoring, 
            includeAssociationNames: [ EntityAssociation.ReportTemplate.ReportTemplateFields, EntityAssociation.ReportTemplateField.Feature ] 
        }).done((response) => {

            var reportTemplateFields = response.items["0"].reportTemplateFields;
            var widthIcon = '45px';
            var columns = AssetUtility.getColumnDefinitions(reportTemplateFields, false, 2, Enums.SortingColumnName.VehicleLicense);
            var headerTemplateIcon = AssetUtility.headerTemplateIcon();

            //find column location to set custom render method
            var locationIndex = _.findIndex(columns, function(o) { return o.data === 'location'; });
            columns.unshift({
                type: 'checkbox',
                title: '',
                // headerTemplate: headerTemplateIcon({ 'iconCssClass': 'icon-follow', 'title': this.i18n("FleetMonitoring_Follow")(), icon: null }),
                headerTemplate: null,
                data: 'isFollow',
                width: widthIcon,
                columnMenu: false,
                columnMenuItem: false,
                sortable: false,
                resizable: false,
                enableSelectAll: true,
                onDataChanged: this.onFollowCheckboxDataChanged
            });

            columns.unshift({
                type: 'text',
                title: '',
                data: 'id',
                headerTemplate: AssetUtility.renderRemoveColumn,
                onHeaderClick: this.onRemoveAllTrackLocationClick,
                template: AssetUtility.renderRemoveColumn,
                onClick: this.onRemoveTrackLocationClick,
                width: widthIcon,
                columnMenu: false,
                columnMenuItem: false,
                sortable: false,
                resizable: false
            });

            this.columns = columns;
        });
    }

    /**
     * Hook on View All button click
     */
    onViewAllButtonClick(){
        var dfd = $.Deferred();

        var vehicleViewIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
        var vehicleFollowIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();

        this.isBusy(true);

        // Calling service for get all Vehicles
        this.webRequestFleetMonitoring.listAsset({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: [],
            searchEntityType: Enums.ModelData.SearchEntityType.Vehicle
        }).done((response) => {
            this.isBusy(true);
            var vehicleViewIdsAdded = [];
            _.forEach(response, (item) => {
                _.forEach(item.assets, (asset) => {
                    vehicleViewIdsAdded.push(asset.vehicleId);
                });
            });

            var currentVehicleViewIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
            var currentVehicleFollowIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
            var currentDeliveryManViewIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
            var currentDeliveryManFollowIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();

            var effectiveVehicleViewIds = [];

            effectiveVehicleViewIds = _.union(currentVehicleViewIds, vehicleViewIdsAdded);

            // Update fleet monitoring global items.
            WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
                this.webRequestFleetMonitoring,
                WebConfig.userSession,
                effectiveVehicleViewIds,
                currentDeliveryManViewIds,
                currentVehicleFollowIds,
                currentDeliveryManFollowIds
            )
            .done(() => {
                dfd.resolve();
            })
            .fail((e) => {
                dfd.reject(e);
            })
            .always(()=> {
                this.isBusy(false);
            });

        }).fail((e) => {
            dfd.reject(e);
        })
        .always(()=> {
            this.isBusy(false);
        });
    }

    /**
     * Update Track Location Asset Ids
     */
    _updateTrackLocationAssetIds(viewVehicleIds, viewDeliveryManIds, followVehicleIds, followDeliveryManIds) {
        this.isBusy(true);
        return WebConfig.fleetMonitoring.updateTrackLocationAssetIds(
            this.webRequestFleetMonitoring,
            WebConfig.userSession,
            viewVehicleIds,
            viewDeliveryManIds,
            followVehicleIds,
            followDeliveryManIds
        ).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    /**
     * Refresh track location manually
     */
    refreshTrackLocation() {
        let filter = {
            keyword: this.keyword()
        }
        return WebConfig.fleetMonitoring.syncTrackLocation(this.webRequestFleetMonitoring, WebConfig.userSession, filter).done(() => {
            Logger.log("Update track location by refresh manually.");
            this._syncTrackLocation();
        });
    }

    /**
     * Sync track location
     */
    _syncTrackLocation() {
        this.dispatchEvent("trackWidgetMonitoring", "refresh");
    }

    /**
     * Remove all track location
     */
    removeAllTrackLocation(){
        Logger.info("onRemoveAllTrackLocation");
        this.isBusy(true);
        var viewVehicleIds = WebConfig.fleetMonitoring.trackLocationViewVehicleIds().slice();
        var viewDeliveryManIds = WebConfig.fleetMonitoring.trackLocationViewDeliveryManIds().slice();
        var followVehicleIds = WebConfig.fleetMonitoring.trackLocationFollowVehicleIds().slice();
        var followDeliveryManIds = WebConfig.fleetMonitoring.trackLocationFollowDeliveryManIds().slice();
        // Calling service for get all Vehicles
        this.webRequestFleetMonitoring.listTrackLocation({
            companyId: WebConfig.userSession.currentCompanyId,
            vehicleIds: viewVehicleIds
        }).done((response) => {
            if(_.size(response.items)){
                _.forEach(response.items, (item) => {
                    if(_.size(item)){
                        if(item.vehicleId){
                                _.remove(viewVehicleIds, (o) => {
                                    return o == item.vehicleId;
                                });
                                _.remove(followVehicleIds, (o) => {
                                    return o == item.deliveryManId;
                                });
                        }
                        else {
                                _.remove(viewDeliveryManIds, (o) => {
                                    return o == item.deliveryManId;
                                });
                                _.remove(followDeliveryManIds, (o) => {
                                    return o == item.deliveryManId;
                                });
                            }
                    }
                });

                this._updateTrackLocationAssetIds(
                    viewVehicleIds,
                    viewDeliveryManIds,
                    followVehicleIds,
                    followDeliveryManIds
                );
                
            }
            this.isBusy(false);
        });
    };

}

export default {
    viewModel: ScreenBaseWidget.createFactory(TrackWidget),
    template: templateMarkup
};