﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {
    Enums,
    Constants
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class DebriefSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());
        this.txtShipmentName = ko.observable();
        this.txtShipmentCode = ko.observable();
        this.relateShipment = ko.observable();
        this.isDateTimeEnabled = ko.observable(true);
        this.selectedReportType = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedDriverPerformance = ko.observable();
        this.selectedEntityType = ko.observable();
        this.categoryVehicle = ko.observable();
        this.shippingType = ko.observable();
        this.isOnlyJob = ko.observable(false);

        this.selectedEntity = ko.observable();
        this.selectedBackwardDuration = ko.observable();
        this.selectedEngineType = ko.observable();
        this.parkingEngineOnOver = ko.observable(10);
        this.selectedGroupByType = ko.observable();
        this.isGroup = ko.observable(false);

        this.BURequire = ko.observable(true);
        this.entityTypeRequire = ko.observable(true);
        this.selectEntityRequire = ko.observable(true);

        this.isIncludeFormerDriver = ko.observable(false);
        this.isIncludeResignedDriver = ko.observable(false);

        this.reportTypes = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.entityTypes = ko.observableArray([]);
        this.categoryVehicleOptions = ko.observableArray([]);
        this.shippingTypeOptions = ko.observableArray([]);
        this.selectEntity = ko.observableArray([]);
        this.driverPerformances = ko.observableArray([]);
        this.backwardDurations = ko.observableArray([]);
        this.engineType = ko.observableArray([]);
        this.groupByTypes = ko.observableArray([]);
        this.valueRadio = ko.observable("Date");
        this.visibleShipment = ko.observable(false);
        this.visibleDate = ko.observable(true);
        this.enableCriteria = ko.observable(true);

        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 5;
        var addMonth = 3;
        var dateTime = Utility.addDays(new Date(), 0);
        var setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(dateTime);
        this.end = ko.observable(dateTime);
        this.startTime = ko.observable("00:00");
        this.endTime = ko.observable("23:59");

        this.maxDateStart = ko.observable(dateTime);
        this.minDateEnd = ko.observable(dateTime);
        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(), addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(), addMonth));

        //Auto Complete Shipment Code
        this.onDatasourceRequestShipmentCode = (request, response) => {
            var filter = {
                shipmentName: this.txtShipmentName(),
                shipmentCode: request.term
            }
            this.webRequestShipment.autocompleteShipmentCode(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.shipmentCode,
                        value: item.shipmentCode
                    };
                }));
            });
        }
        /////////////////////////////////////////

        //for date backward
        this.enable = false;
        this.startBackward = ko.observable(new Date());
        //set end date backward
        this.endBackward = ko.pureComputed(() => {
            var setEndBackward = new Date(this.startBackward());
            this.selectedStartBackward = this.selectedBackwardDuration() && this.selectedBackwardDuration().value;
            if (this.selectedStartBackward === Enums.ModelData.BackwardDurationType.PreviousThreeMonths) {
                setEndBackward.setDate(1); // set Date = First day of Month
                setEndBackward.setMonth(setEndBackward.getMonth() - 2); // set start date backward month - 2 month
            } else if (this.selectedStartBackward === Enums.ModelData.BackwardDurationType.PreviousFourWeeks) {
                setEndBackward.setDate(setEndBackward.getDate() - 27);
            } else if (this.selectedStartBackward === Enums.ModelData.BackwardDurationType.PreviousSeventDays) {
                setEndBackward.setDate(setEndBackward.getDate() - 6);
            } else {
                setEndBackward = setEndBackward;
            }
            return setEndBackward;
        });
        // Enums.ModelData.SummaryReportType.VehicleUsageSummary;TravelDistanceTime
        this.sectionVisible = ko.pureComputed(() => {
            this.isSectionVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.SummaryReport) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.MonthlySummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DailySummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
            return this.isSectionVisible;
        });
        this.isIncludeSubBUVisible = ko.observable(true);

        this.driverVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;
        });

        this.driverPerformanceVisible = ko.pureComputed(() => {
            this.isDriverPerformanceVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore);
            return this.isDriverPerformanceVisible;
        });

        this.txtDateStart = ko.pureComputed(() => {
            var textDateStart = this.i18n('Companies_From')();
            if ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary)) {
                textDateStart = this.i18n('Report_ShipmentFrom')();
            }
            return textDateStart;
        });

        this.txtDateEnd = ko.pureComputed(() => {
            var textDateEnd = this.i18n('Companies_To')();
            if ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary)) {
                textDateEnd = this.i18n('Report_ShipmentTo')();
            }
            return textDateEnd;
        });

        this.categoryVehicleVisible = ko.pureComputed(() => {
            var isCategoryVehicleVisible = ((Object.keys(this.categoryVehicleOptions()).length > 1) ? true : false) && (
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary)
            );
            return isCategoryVehicleVisible;
        });

        this.shippingTypeVisible = ko.pureComputed(() => {
            var isShippingTypeVisible;
            isShippingTypeVisible = (Object.keys(this.shippingTypeOptions()).length > 1) ? true : false;
            isShippingTypeVisible = ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime) || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore) || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore)) ? false : true;

            return isShippingTypeVisible;
        });

        this.parkingEngineOnOverVisible = ko.pureComputed(() => {
            this.isParkingEngineOnOverVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary;
            return this.isParkingEngineOnOverVisible;
        });

        this.backwardDurationVisible = ko.pureComputed(() => {
            this.isBackwardDurationVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.BackwardSummary;
            return this.isBackwardDurationVisible;
        });

        this.isOnlyJobVisible = ko.pureComputed(() => {
            var isCheckJobVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration;
            return isCheckJobVisible;
        });

        

        this.DufaultDateInVisible = ko.pureComputed(() => {
            this.isDufaultDateInVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.BackwardSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.POIParkingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
            return !this.isDufaultDateInVisible;
        });

        this.DateTimeVisible = ko.pureComputed(() => {
            this.isDateTimeVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.POIParkingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
            return this.isDateTimeVisible;
        });

        this.DrivingSummaryReportTimeDurationVisible = ko.pureComputed(() => {
            this.isDrivingSummaryReportTimeDurationVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration);
            // // if report type not equal DrivingSummaryReportTimeDuration set IncludeSubBU is false
            // if(!this.isDrivingSummaryReportTimeDurationVisible){
            //     this.isIncludeSubBU(false);
            // }
            return this.isDrivingSummaryReportTimeDurationVisible;
        });

        this.vihecleUsageVisible = ko.pureComputed(() => {
            this.isVihecleUsageVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary;
            return false;
        });

        this.periodDay = ko.pureComputed(() => {

            // can't select date more than current date
            if (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.WorkingTimeSummary) {
                addDay = 6;
            }

            var isDate = Utility.addDays(this.start(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {

                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.start() == null) {
                // set end date when clear start date
                if (this.end() == null) {

                    isDate = new Date();

                } else {

                    isDate = this.end();

                }

                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            } else {

                this.minDateEnd(this.start());
            }

            if (this.isSelectedReportType) {

                if (this.selectedReportType().value == Enums.ModelData.SummaryReportType.TravelDistanceTime || this.selectedReportType().value == Enums.ModelData.SummaryReportType.VehicleUsageSummary) {
                    //     if(new Date().getDay()==1){ //ถ้าวันจันทร์
                    //         isDate = Utility.addDays(isDate,-1);
                    //     }else{

                    //         isDate = Utility.addDays(isDate,-1);
                    //     }

                    // }else{
                    //     isDate = Utility.addDays(isDate,-1);
                    // }

                    let endDate = new Date(this.end());
                    let startDate = new Date(this.start());


                    if (endDate.getDate() == startDate.getDate()) {

                        isDate = Utility.addDays(isDate, 0);

                    } else {

                        isDate = Utility.addDays(isDate, -1);
                    }
                }

            }

            return isDate;
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.isRAGScore = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore;
            this.isAdvanceScore = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore;
            this.isVehicleUsage = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary;
            this.isTravelDistanceTime = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime;
            this.isMonthlySummaryVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.MonthlySummary;
            this.isDailySummaryVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DailySummary;
            var vihecleTypeOnly = this.isTravelDistanceTime || this.isParkingEngineOnOverVisible || this.isMonthlySummaryVisible || this.isDailySummaryVisible; //|| this.isDrivingSummaryReportTimeDurationVisible;
            var driverTypeOnly = this.isRAGScore || this.isAdvanceScore;
            var vihecleTypeAndDriverType = this.isDriverPerformanceVisible;
            var businessUnitIds = this.selectedBusinessUnit();
            this.selectedEntityType();
            var result = this.i18n('Common_PleaseSelect')();
            this.selectEntity([]);
            //if check include Sub BU
            if (this.isIncludeSubBU() && businessUnitIds != null) {
                businessUnitIds = Utility.includeSubBU(this.businessUnits(), businessUnitIds);
            }
         
            //set vihecleType Only or driver only
            if (vihecleTypeOnly) {
                this.entityTypes(this.tempEntityTypes()[0])
            } else if (driverTypeOnly) {
                this.entityTypes(this.tempEntityTypes()[1])
            } else {
                this.entityTypes(this.tempEntityTypes())
            }
            // this.entityTypes((vihecleTypeOnly)?this.tempEntityTypes()[0]:this.tempEntityTypes());

            if (this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null) {

                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle,
                    vehicleCategoryId: this.categoryVehicle().id
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if (driverTypeOnly || vihecleTypeOnly || vihecleTypeAndDriverType || this.isDrivingSummaryReportTimeDurationVisible || this.isVehicleUsage || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary)) {
                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0) {
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });

                        }
                    } else {
                        //remove displayName "Report_All" and id 0
                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0) {
                            listSelectEntity.shift();
                        }
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            } else if (this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null) {
                let includeFormerDriver = this.isIncludeFormerDriver() ? true : null;
                let includeResignedDriver = this.isIncludeResignedDriver() ? null : true;

                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver,
                    IncludeFormerInBusinessUnit : this.isIncludeFormerDriver(),
                    isEnable: this.isIncludeResignedDriver() ? null : true ,
                    
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if (driverTypeOnly || vihecleTypeOnly || vihecleTypeAndDriverType || this.isDrivingSummaryReportTimeDurationVisible || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary)) {

                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0) {

                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    } else {

                        //remove displayName "Report_All" and id 0
                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0) {

                            listSelectEntity.shift();
                        }
                    }

                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            } else {

                this.selectEntity([]);
            }
            if (this.isVehicleUsage || this.isTravelDistanceTime || this.isAdvanceScore || this.isRAGScore) {

                result = "";
            }
            
            return result;
        });

        this.valueRadio.subscribe((data) => {
            if (data == "Date") {
                this.visibleDate(true);
                this.visibleShipment(false);
                this.enableCriteria(true);
            }
            else {
                this.visibleDate(false);
                this.visibleShipment(true);
                this.enableCriteria(false);
            }
        });
    }

    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else {}

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;



        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }


    setupExtend() {
        var self = this;
        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        
        this.selectedDriverPerformance.extend({required: true});

        this.selectedBusinessUnit.extend({
            required: {
                onlyIf: () => {
                    return self.enableCriteria();
                }
            }
        });
        //Entity Type
        this.selectedEntityType.extend({
            required: {
                onlyIf: () => {
                    return self.enableCriteria();
                }
            }
        });
        this.selectedEntity.extend({
            required: {
                onlyIf: () => {
                    return self.enableCriteria();
                }
            }
        });

        this.start.extend({
            required: {
                onlyIf: () => {
                    return self.visibleDate();
                }
            }
        });
        this.end.extend({
            required: {
                onlyIf: () => {
                    return self.visibleDate();
                }
            }
        });

        this.startTime.extend({
            required: {
                onlyIf: () => {
                    return self.visibleDate();
                }
            }
        });

        this.endTime.extend({
            required: {
                onlyIf: () => {
                    return self.visibleDate();
                }
            }
        });

        this.txtShipmentCode.extend({
            required: {
                onlyIf: () => {
                    return self.visibleShipment();
                }
            }
        })

        this.validationModel = ko.validatedObservable({
         
            end: this.end,
            start: this.start,
            startTime: this.startTime,
            endTime: this.endTime,
            txtShipmentCode: this.txtShipmentCode,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedEntity:this.selectedEntity,
            selectedDriverPerformance:this.selectedDriverPerformance,
            selectedEntityType:this.selectedEntityType
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    getReportTemplateType() {
        var reportType = new Array();
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_TravelDistance)) {
            reportType.push(Enums.ModelData.SummaryReportType.TravelDistanceTime);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_POIParking)) {
            reportType.push(Enums.ModelData.SummaryReportType.POIParkingSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_ParingOvertime)) {
            reportType.push(Enums.ModelData.SummaryReportType.OverTimeIdlingSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_DriverPerformance)) {
            reportType.push(Enums.ModelData.SummaryReportType.DriverPerformanceSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Overall)) {
            reportType.push(Enums.ModelData.SummaryReportType.OverallSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Backward)) {
            reportType.push(Enums.ModelData.SummaryReportType.BackwardSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Monthly)) {
            reportType.push(Enums.ModelData.SummaryReportType.MonthlySummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Daily)) {
            reportType.push(Enums.ModelData.SummaryReportType.DailySummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Driving)) {
            reportType.push(Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_VehicleUsage)) {
            reportType.push(Enums.ModelData.SummaryReportType.VehicleUsageSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Shipment)) {
            reportType.push(Enums.ModelData.SummaryReportType.ShipmentSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_AvtivatingDriving)) {
            reportType.push(Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_WoringTime)) {
            reportType.push(Enums.ModelData.SummaryReportType.WorkingTimeSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_RAGScore)) {
            reportType.push(Enums.ModelData.SummaryReportType.RAGScore);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_AdvanceScore)) {
            reportType.push(Enums.ModelData.SummaryReportType.AdvanceScore);
        }
        //Mock With Out Permission//
        // reportType.push(Enums.ModelData.SummaryReportType.RAGScore);
        // reportType.push(Enums.ModelData.SummaryReportType.AdvanceScore);
        
        
        
        
        if (reportType.length < 1) {
            reportType = [0];
        }
        return reportType;
    }


    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SummaryReportType],
            values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        

        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var filterCategoryVehicle = {
            categoryId: Enums.ModelData.CategoryType.Vehicle,
        };

        var filterShippingType = {
            categoryId: Enums.ModelData.CategoryType.ShippingType,
        };

        var driverRuleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            enable: true
        }

        var enumBackwardDurationFilter = {
            types: [Enums.ModelData.EnumResourceType.BackwardDurationType],
            values: [
                Enums.ModelData.BackwardDurationType.PreviousThreeMonths,
                Enums.ModelData.BackwardDurationType.PreviousFourWeeks,
                Enums.ModelData.BackwardDurationType.PreviousSeventDays
            ]
        };

        var enumEngineTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.EngineType],
            values: []
        };

        var enumGroupByTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.GroupByType],
            values: [
                Enums.ModelData.GroupByType.DateTime,
                Enums.ModelData.GroupByType.Vehicle
            ]
        };

        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);
        var d4 = this.webRequestDriveRule.listDriveRuleSummary(driverRuleFilter);
        var d5 = this.webRequestEnumResource.listEnumResource(enumBackwardDurationFilter);
        var d6 = this.webRequestEnumResource.listEnumResource(enumEngineTypeFilter);
        var d7 = this.webRequestEnumResource.listEnumResource(enumGroupByTypeFilter);

        var d8 = this.webRequestCategory.listCategory(filterCategoryVehicle);
        var d9 = this.webRequestCategory.listCategory(filterShippingType);



        $.when(d1, d2, d3, d4, d5, d6, d7, d8, d9).done((r1, r2, r3, r4, r5, r6, r7, r8, r9) => {


            r8.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });

            r9.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });
 
            // this.reportTypes(r1.items);
            this.businessUnits(r2.items);
            this.tempEntityTypes(r3.items);
            this.driverPerformances(r4.items);
            this.backwardDurations(r5.items);
            this.engineType(r6.items);
            this.groupByTypes(r7.items);
            this.categoryVehicleOptions(r8.items);
            this.shippingTypeOptions(r9.items);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }
  

    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if (sender.id === "actPreview") {
            // ////////////////////////////// MOCK //////////////////////////////////////
            // var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + "T" + this.startTime() + ":00";
            // var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + "T" + this.endTime() + ":00";
                // let filter = {   
                //                 // includeSubBU:this.isIncludeSubBU(),
                //                  // relateShipment:this.relateShipment(),
                //                 jobCode:this.txtShipmentCode(),
                //                 entityType:(this.selectedEntityType())?this.selectedEntityType().value:null,
                //                 selectedEntity:(this.selectedEntity())?this.selectedEntity().id:null,
                //                 drivingPerformanceRule:(this.selectedDriverPerformance())?this.selectedDriverPerformance().id:null,
                //                 fromDate: formatSDate,
		        //                 toDate: formatEDate

                // }
           // this.navigate("cw-fleet-monitoring-debrief-view",filter);
            ///////////////////////////////// END MOCK //////////////////////////////////////////////


            // var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + "T" + this.startTime() + ":00";
            // var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + "T" + this.endTime() + ":00";
            // console.log("eiei")
            // console.log("this.validationModel.isValid()",this.validationModel.isValid())

            // console.log("start",this.start());
            // console.log("end",this.end());
            // console.log("startTime",this.startTime());
            // console.log("endTime",this.endTime());
            // console.log("formatSDate",formatSDate);
            // console.log("formatEDate",formatEDate);
            // console.log("this.selectedEntity()",this.selectedEntity())

            if(this.validationModel.isValid()) {
            //     let filter = {   
            //                     // includeSubBU:this.isIncludeSubBU(),
            //                      // relateShipment:this.relateShipment(),
            //                     entityType:this.selectedEntityType().value,
            //                     selectedEntity:this.selectedEntity().id,
            //                     drivingPerformanceRule:this.selectedDriverPerformance().id,
            //                     fromDate: formatSDate,
		    //                     toDate: formatEDate

            //     }
            // this.navigate("cw-fleet-monitoring-debrief-view",filter);
            ///////////////////////////////// END MOCK //////////////////////////////////////////////

                let formatStartTime = this.startTime() ? this.startTime() : "00:00";
                let formatEndTime = this.endTime() ? this.endTime() : "23:59";
                var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + "T" + formatStartTime + ":00";
                var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + "T" + formatEndTime + ":00";

          // if(this.validationModel.isValid()) {
            //   console.log("this.txtShipmentCode()",this.txtShipmentCode());
                let filter = {

                    includeSubBU: this.isIncludeSubBU(),
                    relateShipment: this.relateShipment(),
                    entityType: this.selectedEntityType() ? this.selectedEntityType().value : null,
                    selectedEntity: this.selectedEntity() ? this.selectedEntity().id : null,
                    drivingPerformanceRule: this.selectedDriverPerformance().id,
                    fromDate: formatSDate,
                    toDate: formatEDate,
                    jobCode: this.visibleDate() ? null : this.txtShipmentCode(),
                    IncludeFormerInBusinessUnit: this.isIncludeFormerDriver() ? 1 : 0 ,
                    Enable: this.isIncludeResignedDriver() ? 1 : 0 ,
                };

                // let filter = {
                
                //     "fromDate": "2018-11-22T00:00:00.5220083+07:00",
                //     "toDate": "2018-11-22T23:59:59.525915+07:00",
                //     "drivingPerformanceRule":3,
                //     "entityType":1,
                //     "selectedEntity":22023
                
                // }

                this.navigate("cw-fleet-monitoring-debrief-view", filter);

            }else{      
                this.validationModel.errors.showAllMessages();
            }

        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(DebriefSearchScreen),
    template: templateMarkup
};