﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebRequestDebrief from "../../../../../app/frameworks/data/apitrackingcore/webRequestDebrief";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {
    Enums,
    Constants
} from "../../../../../app/frameworks/constant/apiConstant";
import AssetInfo from "../../asset-info";
import WorkingMapManager from "../../../../../components/controls/gisc-chrome/map/workingMapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";

import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class DebriefViewScreen extends ScreenBase {
    constructor(params) {
        super(params);

        // JSON.stringify(params)
        this.bladeSize = BladeSize.Large;
        this.bladeTitle(this.i18n('FleetMonitoring_Debrief')());
        this.filter = ko.observable(params)
        this.vehicleSummaryVisible = ko.observable((params.entityType == 1 ? true : false));
        this.driverSummaryVisible = ko.observable((params.entityType == 3 ? true : false));
        this.dateFromDisplay = ko.observable();
        this.dateToDisplay = ko.observable();
        this.vehicleLicense = ko.observable();
        this.relateShipment = ko.observable();
        this.driverName = ko.observable();
        this.driverPerfermanceRuleName = ko.observable();
        this.formatFuelPlan = ko.observable();
        this.formatFuel = ko.observable();
        this.formatDistance = ko.observable();
        this.formatDriverOverTime4HrTimes = ko.observable();
        this.formatStopOver30MinTimes = ko.observable();
        this.averageSpeed = ko.observable();
        this.formatDisplayLevel1 = ko.observable();
        this.formatDisplayLevel2 = ko.observable();
        this.formatDisplayLevel3 = ko.observable();
        this.formatDisplayLevel4 = ko.observable();
        this.formatDisplayLevel5 = ko.observable();
        this.formatDisplayLevel6 = ko.observable();
        this.formatDisplayLevel7 = ko.observable();
        this.formatTotalTime = ko.observable();
        this.formatTotalIdle = ko.observable();
        this.formatTotalPark = ko.observable();
        this.formatTotalStop = ko.observable();
        this.formatTotalMove = ko.observable();
        this.formatTotalEvent = ko.observable();
        this.events = ko.observableArray([]);
        this.eventTimes = ko.observableArray([]);
        this.alert = ko.observableArray([]);
        this.vehicleSummaryVisible = ko.observable((params.entityType == 1 ? true : false));
        this.driverSummaryVisible = ko.observable((params.entityType == 3 ? true : false));
        this.routeData = ko.observable(null);
        this.resultPoltgon = ko.observable(null);
        this.isDataNotFound = ko.observable(false);

        
        this.txtOnTime = ko.observable("(" + this.i18n("Transportation_Mngm_Dashboard_On_Time")() + ")");

        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        this.alertIcons = ko.observable();
        this.alertConfig = ko.observable();

        this.timesTxt = ko.observable(this.i18n('FleetMonitoring_Playback_Times')());

        // i18n ///////////////
        this.dateTxt = ko.observable(this.i18n('Debrief_Date')());
        this.dateToTxt = ko.observable(this.i18n('Report_To')());
        this.licenseTxt = ko.observable(this.i18n('Common_License')());

            // table head ////////
        this.driverNameCol = ko.observable(this.i18n('Debrief_Table_Head_Drivername')());
        this.testDriver = ko.observable(this.i18n('Transportation_Mngm_Shipment_Template_Travel_Time')())


        ///////////////////////////////////////////////
        this.relateshipmentTxt =this.i18n("Debrief_RelateShipment")()
        this.drivernameTxt = this.i18n("Debrief_Table_Head_Drivername")()
        this.driverPerformanceRulesTxt = this.i18n("DriverPerformanceRules_DriverPerformanceRules")()
        this.totalFuelTxt= this.i18n("Debrief_Head_TotalFuel")()
        this.driver4hoursTxt = this.i18n("Debrief_Head_Drive_Overtime_4hours")()
        this.totalTimeTxt = this.i18n("Debrief_Head_Total_Time")()
        this.moveDurationTxt =this.i18n("Debrief_Head_Move_Duration")()
        this.totalDistanceTxt = this.i18n("FleetMonitoring_Playback_Total_Distance")()
        this.rest30minutesTxt = this.i18n("Debrief_Head_Rest_Over_30minutes")()
        this.totaleventTxt = this.i18n("Common_TotalEvent")()
        this.speedTxt = this.i18n("Common_Speed")()

        this.moveTxt = this.i18n("FleetMonitoring_Playback_Move_Duration")()
        this.parkTxt = this.i18n("FleetMonitoring_Playback_Park_Duration")()
        this.stopTxt = this.i18n("FleetMonitoring_Playback_Stop_Duration")()
        this.idleTxt = this.i18n("FleetMonitoring_Playback_Engine_On_Duration")()

        this.startPointTxt = ko.observable(this.i18n("Debrief_Start")())
        this.endPointTxt = ko.observable(this.i18n("Debrief_End")())
        this.engineOffTxt = ko.observable(this.i18n("Debrief_Engine_Off")())

        this.hTableDateTime = this.i18n("Debrief_Table_Head_Date_Time")();
        this.hTableDuration = this.i18n("Debrief_Table_Head_Duration_Time")();
        this.hTableRelateShipment = this.i18n("Debrief_RelateShipment")();
        this.hTableDiverName = this.i18n("Debrief_Table_Head_Drivername")() +"<br>"+ this.i18n("Debrief_Table_Head_Employee_Id")() ;
        this.hTableEvent = this.i18n("Debrief_Table_Head_Event")();
        this.hTableDescription = this.i18n("Debrief_Table_Head_Event")() +"<br>"+ this.i18n("Debrief_Table_Head_Description")();
        this.hTableLaLong = this.i18n("Debrief_Table_Head_Lat_Long")() +"<br>"+ this.i18n("Debrief_Table_Head_Longitude")();
        this.hTableLocation = this.i18n("Debrief_Table_Head_Location")();
        this.driverDebriefHeadTxt = ko.observable(this.i18n("FleetMonitoring_Debrief")()) ; 

    }




    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestDebrief() {
        return WebRequestDebrief.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        // let pathImages = {
        // picTotalFuel: "../../../../images/icon_nearest_assets_fuel@3x.png",
        //     picTotalWorkingTime: "../../../../images/icon_nearest_assets_engine@3x.png",
        //     picAverageSpeed: "../../../../images/icon_nearest_assets_speed@3x.png",
        //     picTotalMove: "../../../../images/icon_nearest_assets_movement_move@3x.png",
        //     picTotalStop: "../../../../images/icon_nearest_assets_movement_stop@3x.png",
        //     picTotalPark: "../../../../images/icon_nearest_assets_movement_park@3x.png",
        //     picTotalDistance: "../../../../images/icon_nearest_assets_mileage@3x.png",
        //     picTotalenginneon: "../../../../images/icon_nearest_assets_movement_idle@3x.png"
        // }

        // Mock Data //
        // this.filter({

        //     "fromDate": "2018-11-22T00:00:00.5220083+07:00",
        //     "toDate": "2018-11-22T23:59:59.525915+07:00",
        //     "drivingPerformanceRule": 3,
        //     "entityType": 1,
        //     "selectedEntity": 22023

        // });

        ///////////////////////////////////////////////////////////////////////////////
        localStorage.removeItem('debriefFilter');
        localStorage.removeItem('debriefI18nTxt');
        localStorage.removeItem('debriefReadyData');
        localStorage.removeItem('companydata');

        
        
        let companydata = {
            UserLanguage:WebConfig.userSession._prefUserLanguage,
            CompanyId:WebConfig.userSession._userSession.currentCompanyId
        }
        localStorage.setItem("companydata",JSON.stringify(companydata))

        var dfd = $.Deferred();
        var debriefRequest = this.webRequestDebrief.debrief(this.filter());

        $.when(debriefRequest).done((res) => {


            // if(res.items[0].length == 0 || res.items[0] == null){
            //     this.isDataNotFound(true);
            // }
            // this.i18n('Common_DataNotFound')()

            
            let result = res.items[0];

            let alertIcons = _.isEmpty(res["alertIcon"]) ? null : res["alertIcon"];
            var alertConfig = res["alertIcon"];
            this.alertIcons(alertIcons)
            this.alertConfig(alertConfig)

            let data = this.setDataRoute(res.items[0].locationInfos)
            
           // localStorage.setItem("Route",JSON.stringify(data))

            this.routeData(data)

            this.dateFromDisplay(res.formatFromDate);
            this.dateToDisplay(res.formatToDate);
            this.relateShipment(result.jobCode);
            this.vehicleLicense(result.vehicleLicense);
            this.driverName(result.driverName);
            this.driverPerfermanceRuleName(result.drivingPerformanceRuleName);

            this.formatFuelPlan(result.fuelInfos.formatDisplayFuel = this.i18n("FleetMonitoring_Playback_Fuel_Plan")() +
                " : " + result.fuelInfos.formatPlanFuel +
                " " + this.i18n("FleetMonitoring_Playback_Litre")() +
                " (" + result.fuelInfos.formatFuelRate +
                " " + this.i18n("FleetMonitoring_Playback_Fuel_Km/L")() + ")")
            this.formatFuel(result.fuelInfos.formatTotalFuel + " " + this.i18n('FleetMonitoring_Playback_Litre')());
            this.formatDistance(result.formatTotalDistance + " " + this.i18n('FleetMonitoring_Playback_Km')());
            this.formatDriverOverTime4HrTimes(result.formatDriverOverTime4HrTimes + " " + this.i18n("FleetMonitoring_Playback_Times")());
            this.formatStopOver30MinTimes(result.formatStopOver30MinTimes + " " + this.i18n("FleetMonitoring_Playback_Times")());
            this.averageSpeed(result.speedInfos.averageSpeed);

            this.formatDisplayLevel1("• 1-20 : " + result.speedInfos.formatLevel1 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel1 + "%)")
            this.formatDisplayLevel2("• 21-40 : " + result.speedInfos.formatLevel2 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel2 + "%)")
            this.formatDisplayLevel3("• 41-60 : " + result.speedInfos.formatLevel3 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel3 + "%)")
            this.formatDisplayLevel4("• 61-80 : " + result.speedInfos.formatLevel4 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel4 + "%)")
            this.formatDisplayLevel5("• 81-100 : " + result.speedInfos.formatLevel5 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel5 + "%)")
            this.formatDisplayLevel6("• 101-120 : " + result.speedInfos.formatLevel6 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel6 + "%)")
            this.formatDisplayLevel7("• >120 : " + result.speedInfos.formatLevel7 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel7 + "%)")

            this.formatTotalTime(this.calculatMins(result.totalTime));
            this.formatTotalIdle(this.calculatMins(result.totalParkEngineOn));
            this.formatTotalPark(this.calculatMins(result.totalPark));
            this.formatTotalStop(this.calculatMins(result.totalStop));
            this.formatTotalMove(this.calculatMins(result.totalMove));

            this.formatTotalEvent(result.events.length + " " + this.i18n("FleetMonitoring_Playback_Times")());

            this.eventTimes(result.eventTimes);
            this.events(result.events);


            let alertIconList = this.generateAlertIconsList(result.eventTimes,res.alertIcon);
            // console.log("alertIconList",alertIconList)
            this.alert(alertIconList);

            ////////////////////Set Local Storage//////////////////////////////
            let debriefReadyData = {

                dateFromDisplay: this.dateFromDisplay(),
                dateToDisplay: this.dateToDisplay(),
                relateShipment: this.relateShipment(),
                vehicleLicense: this.vehicleLicense(),
                driverName: this.driverName(),
                driverPerfermanceRuleName: this.driverPerfermanceRuleName(),
                formatFuel: this.formatFuel(),
                formatFuelPlan: this.formatFuelPlan(),
                formatDistance: this.formatDistance(),
                formatDriverOverTime4HrTimes: this.formatDriverOverTime4HrTimes(),
                formatStopOver30MinTimes: this.formatStopOver30MinTimes(),
                averageSpeed: this.averageSpeed(),
                formatDisplayLevel1: this.formatDisplayLevel1(),
                formatDisplayLevel2: this.formatDisplayLevel2(),
                formatDisplayLevel3: this.formatDisplayLevel3(),
                formatDisplayLevel4: this.formatDisplayLevel4(),
                formatDisplayLevel5: this.formatDisplayLevel5(),
                formatDisplayLevel6: this.formatDisplayLevel6(),
                formatDisplayLevel7: this.formatDisplayLevel7(),
                formatTotalTime: this.formatTotalTime(),
                formatTotalIdle: this.formatTotalIdle(),
                formatTotalPark: this.formatTotalPark(),
                formatTotalStop: this.formatTotalStop(),
                formatTotalMove: this.formatTotalMove(),
                formatTotalEvent: this.formatTotalEvent(),
                eventTimes: this.eventTimes(),
                events: this.events(),
                alertIcon: this.alert()

            }
            let debriefI18nTxt = {

                timesTxt:this.timesTxt(),
                driver:this.i18n('Debrief_Driver')(),
                driverManager:this.i18n('Debrief_Transport_Manager_Driver_Manager')(),
                heardReport:this.i18n('FleetMonitoring_Debrief')(),
                startPointTxt: this.startPointTxt() ,
                endPointTxt:   this.endPointTxt(),
                engineOffTxt:  this.engineOffTxt(), 
                dateTxt: this.dateTxt(),
                dateToTxt: this.dateToTxt(),
                licenseTxt: this.licenseTxt(),
                relateshipment : this.i18n("Debrief_RelateShipment")(),
                drivername : this.i18n("Debrief_Table_Head_Drivername")(),
                driverPerformanceRules : this.i18n("DriverPerformanceRules_DriverPerformanceRules")(),
                totalFuel : this.i18n("Debrief_Head_TotalFuel")(),
                driver4hours : this.i18n("Debrief_Head_Drive_Overtime_4hours")(),
                totalTime : this.i18n("Debrief_Head_Total_Time")(),
                moveDuration : this.i18n("Debrief_Head_Move_Duration")(),
                totalDistance : this.i18n("FleetMonitoring_Playback_Total_Distance")(),
                rest30minutes : this.i18n("Debrief_Head_Rest_Over_30minutes")(),
                totalevent : this.i18n("Common_TotalEvent")(),
                speed : this.i18n("Common_Speed")(),

                move: this.i18n("FleetMonitoring_Playback_Move_Duration")(),
                park: this.i18n("FleetMonitoring_Playback_Park_Duration")(),
                stop: this.i18n("FleetMonitoring_Playback_Stop_Duration")(),
                idle: this.i18n("FleetMonitoring_Playback_Engine_On_Duration")(),

                

                hTableDateTime : this.i18n("Debrief_Table_Head_Date_Time")(),
                hTableDuration : this.i18n("Debrief_Table_Head_Duration_Time")(),
                hTableRelateShipment : this.i18n("Debrief_RelateShipment")(),
                hTableDiverName : this.i18n("Debrief_Table_Head_Drivername")() +"<br>"+ this.i18n("Debrief_Table_Head_Employee_Id")() ,
                hTableEvent : this.i18n("Debrief_Table_Head_Event")(),
                hTableDescription: this.i18n("Debrief_Table_Head_Event")() +"<br>" + this.i18n("Debrief_Table_Head_Description")(),
                hTableLaLong : this.i18n("Debrief_Table_Head_Lat_Long")() +"<br>"+ this.i18n("Debrief_Table_Head_Longitude")(),
                hTableLocation : this.i18n("Debrief_Table_Head_Location")()
                
            }
            localStorage.setItem("debriefReadyData", JSON.stringify(debriefReadyData));
            localStorage.setItem("debriefFilter", JSON.stringify(this.filter()));
            localStorage.setItem("debriefI18nTxt", JSON.stringify(debriefI18nTxt));

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        //$.when(debriefRequest).done((res) => {
        //    console.log("res>>",res)
        //    let data =  this.setDataRoute(res.items[0].locationInfos)
        //    this.routeData(data)
        //    dfd.resolve();
        //}).fail((e) => {
        //    dfd.reject(e);
        //});
        return dfd;
    }

    generateAlertIconsList(eventTimes,alertIcon){

        let alertList = [];
        eventTimes.forEach((items)=>{

            for (var key in alertIcon) {
                if(key == items.eventId){
                    alertList.push(alertIcon[key]);
                }              
            }
        })

        alertList.forEach(items => {
            items.formatImageUrl = this.resolveUrl('../../../../..' + items.imageUrl);
            items.resolveImageUrl = this.resolveUrl(items.imageUrl);
            // items.formatImageUrl =  Utility.resolveUrl('../../../../..'+items.imageUrl);
            // items.formatImageUrl =  Utility.resolveUrl(items.imageUrl);
        });


        return alertList;


    }

    calculatMins(mins) {
        var calParkTime = "";
        var parkDuration = mins;
        var dayInMinutes = 60 * 24;
        var day = Math.floor(parkDuration / dayInMinutes);
        calParkTime += day == 0 ? " " : day+this.i18n('Debrief_Day')()+" ";
        // calParkTime += "d"
        // calParkTime += " "
        var hr = Math.floor((parkDuration % dayInMinutes) / 60);
        calParkTime += hr == 0 ? " " : hr+this.i18n('Debrief_Hr')()+" ";
        // calParkTime += "h"
        // calParkTime += " "
        var minutes = Math.floor((parkDuration % dayInMinutes) % 60);
        calParkTime += minutes == 0 ? " " : minutes+this.i18n('Debrief_Mins')();
        // calParkTime += "m"


        // calParkTime = "22 Day 24 Hr 60 Min"
        return calParkTime
    }


    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        // this.selectedBusinessUnit.extend({ required: true });
        // //Entity Type
        // this.selectedEntityType.extend({ required: true });
        // this.selectedEntity.extend({ required: true });
        // this.selectedDriverPerformance.extend({required: true});
        // this.selectedBackwardDuration.extend({
        //     required: true
        // });
        // this.start.extend({
        //     required: true
        // });
        // this.end.extend({
        //     required: true
        // });
        // this.selectedEngineType.extend({
        //     required: true
        // });
        // this.parkingEngineOnOver.extend({
        //     required: true,
        //     number: true,
        //     min: 0
        // });

        // this.selectedEntity.extend({
        //     required: {
        //         onlyIf: () => {
        //             return (this.selectEntityRequire()); //require เมื่อเป็น true
        //         }
        //     }
        // });

        // this.validationModel = ko.validatedObservable({

        //     end: this.end,
        //     start: this.start,
        //     selectedBusinessUnit: this.selectedBusinessUnit,
        //     selectedEntity:this.selectedEntity,
        //     selectedDriverPerformance:this.selectedDriverPerformance,
        //     selectedEntityType:this.selectedEntityType
        // });

    }
    createPolygon() {
        WorkingMapManager.getInstance().drawPolygonFromPolyLine();
    }


    buildActionBar(actions) {
        // actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    buildCommandBar(commands) {


        commands.push(this.createCommand("cmdReport", this.i18n("Common_Export")(), "svg-cmd-export"));
        // commands.push(this.createCommand("cmdCancel", "Drow Polygon", "svg-cmd-clear"));
    }
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdReport":
            
                window.open(WebConfig.appSettings.driverdebrief, "debriefreport")
                // window.open(WebConfig.appSettings.driverdebrief, "debriefreport")
                break;
            case "cmdCancel":
                this.createPolygon();
                break;

        }
    }

    setDataRoute(items) {

     //   console.log("items",items)

        var mapObj = {
            mode: "ALL",
            playback: [],
            alert: []
        };

        var paths = [];

        var sortItems = _.sortBy(items, ['dateTime', 'id']);

        _.forEach(sortItems, (data) => {
            var itemsID = data.vehicleId ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId;
            var obj = {
                id: itemsID,
                lat: data.latitude,
                lon: data.longitude,
                rotation: data.direction,
                attributes: this.generateAttributes(data),
            };

            // var obj = [data.longitude, data.latitude];

            if (data.vehicleIconId) {
                obj.pic = this.generateVehicleIconImageUrl(this.alertIcons()[data.vehicleIconId], data.movement);
            } else {
                obj.pic = this.generateDeliveryIconImageUrl(data.movement);
            }

            if (data.movement) {
                obj.movement = this.generateMovement(data.movement);
            }

            //paths.push(obj);

        mapObj.playback.push(obj);
            if (data.alertConfigurationId != null) {
                mapObj.alert.push(this.generateAlertItem(this.alertConfig(), data, obj.attributes.SHIPMENTINFO, obj.attributes.TITLE));
            }


        })
       // mapObj.playback.push(paths);
        return mapObj;
    }
    onMapComplete(data) {

        switch (data.command) {
            case "wm-map-api-ready":
                WorkingMapManager.getInstance().setStartupResource(WebConfig.userSession);
                break;

            case "wm-set-startup-resources":
                WorkingMapManager.getInstance().drawRoute(this.routeData());
                //  WorkingMapManager.getInstance().drawPolygonFromPolyLine();
                break;
            default:
                break;
        }
        // actions.push(this.createAction("actPreview", this.i18n('Report_Preview')() ));

    }

    onActionClick(sender) {
        if (sender.id === "actPreview") {
            window.open("DebriefReport.html", "debriefreport")

            // if(this.validationModel.isValid()) {

            //     // this.navigate("bo-shared-accessible-users-select", {
            //     //     mode: targetMode,
            //     //     selectedUserIds: selectedUserIds,
            //     //     teamLeaderId: teamLeaderId,
            //     //     technicianTeamId : technicianTeamId
            //     // });

            // }else{
            //     this.validationModel.errors.showAllMessages();
            // }

        }
    }

    generateAlertItem(alertConfig, playbackItem, info, title) {
        var alertItem = {};

        alertItem.latitude = playbackItem.latitude;
        alertItem.longitude = playbackItem.longitude;


        if (alertConfig[playbackItem.alertConfigurationId] != undefined) {
            alertItem.attributes = _.clone(alertConfig[playbackItem.alertConfigurationId]);
        } else {
            alertItem.attributes = {
                imageUrl: "",
                description: ""
            }
        }
        alertItem.attributes.imageUrl = this.resolveUrl(alertItem.attributes.imageUrl);
        alertItem.attributes.TITLE = title;


        alertItem.attributes.SHIPMENTINFO = [{
            key: "alertdesc",
            title: this.i18n('wg-shipmentlegend-alertdesc')(),
            text: alertItem.attributes.description
        }].concat(_.clone(info));

        return alertItem;
    }

    generateAttributes(data) {
        var isVehicle = data.vehicleId ? true : false;
        var datID = isVehicle ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId + "-PB";
        var title = isVehicle ? data.vehicleLicense : data.username;
        var businessUnitName = "";

        // Detect business unit name based on vehicle or delivery men.
        if (isVehicle) {
            businessUnitName = _.map(_.filter(this.selectedVehicles, {
                id: data.vehicleId
            }), "businessUnitName");
        } else {
            businessUnitName = _.map(_.filter(this.selectedDeliveryMen, {
                id: data.deliveryManId
            }), "businessUnitName");
        }

        // Compose map attribute object.
        var attr = {
            ID: datID,
            TITLE: title,
            BOX_ID: data.vehicleId ? data.boxSerialNo : data.email,
            DRIVER_NAME: data.driverName,
            DEPT: businessUnitName,
            DATE: data.formatDateTime,
            TIME: Utility.getOnlyTime(data.dateTime, "HH:mm"),
            PARK_TIME: data.parkDuration,
            PARK_IDLE_TIME: data.idleTime,
            LOCATION: data.location,
            RAW: data,
            STATUS: AssetInfo.getStatuses(data, WebConfig.userSession),
            IS_DELAY: data.isDelay
        };

        attr.SHIPMENTINFO = AssetInfo.getShipmentInfo(data, attr.STATUS);

        return attr;
    }

    generateVehicleIconImageUrl(data, compareValue) {

        var result = null;

        _.forEach(data, (d) => {
            if (d.movementType === compareValue) {
                result = {
                    N: d.imageUrl + "/n.png",
                    E: d.imageUrl + "/e.png",
                    W: d.imageUrl + "/w.png",
                    S: d.imageUrl + "/s.png",
                    NE: d.imageUrl + "/ne.png",
                    NW: d.imageUrl + "/nw.png",
                    SE: d.imageUrl + "/se.png",
                    SW: d.imageUrl + "/sw.png"
                }
            }
        });

        return result;
    }

    generateDeliveryIconImageUrl(movementType) {
        var movement = "";
        switch (movementType) {
            case Enums.ModelData.MovementType.Move:
                movement = "move";
                break;
            case Enums.ModelData.MovementType.Stop:
                movement = "stop";
                break;
            case Enums.ModelData.MovementType.Park:
                movement = "park";
                break;
            case Enums.ModelData.MovementType.ParkEngineOn:
                movement = "parkengineon";
                break;
        }

        var imageUrl = Utility.resolveUrl("/", "~/images/icon-walk-" + movement + "-direction-");

        return {
            N: imageUrl + "n.png",
            E: imageUrl + "e.png",
            W: imageUrl + "w.png",
            S: imageUrl + "s.png",
            NE: imageUrl + "ne.png",
            NW: imageUrl + "nw.png",
            SE: imageUrl + "se.png",
            SW: imageUrl + "sw.png"
        }
    }

    generateMovement(val) {

        switch (val) {
            case Enums.ModelData.MovementType.ParkEngineOn:
                val = "PARKENGINEON";
                break;
            case Enums.ModelData.MovementType.Move:
                val = "MOVE";
                break;
            case Enums.ModelData.MovementType.Park:
                val = "PARK";
                break;
            case Enums.ModelData.MovementType.ParkEngineOn:
                val = "PARKENGINON";
                break;
            default:
                val = "STOP";
                break;
        }
        return val;
    }
}



export default {
    viewModel: ScreenBase.createFactory(DebriefViewScreen),
    template: templateMarkup
};