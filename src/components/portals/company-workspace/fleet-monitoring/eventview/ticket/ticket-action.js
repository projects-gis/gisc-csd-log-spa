﻿import ko from "knockout";
import templateMarkup from "text!./ticket-action.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../../app/frameworks/core/utility";
import { Constants, Enums } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestTickerResponse from "../../../../../../app/frameworks/data/apitrackingcore/webRequestTickerResponse";
import WebRequestAlert from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlert";

class EventViewTicketActionScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.isPermission = ko.observable((WebConfig.userSession.hasPermission([Constants.Permission.Manage_Ticket]) > 0)); // COM0057
        this.bladeTitle(this.i18n("FleetMonitoring_Alert_Ticket_Action")());
        this.bladeSize = BladeSize.Medium;
        this.alertTicketId = this.ensureNonObservable(params.id);
        this.alertTicketStatus = this.ensureNonObservable(params.alertTicketStatus);
        
        if(params.view) {
            this.isPermission(!params.view);
        } else { }

        this.order = ko.observable([]);
        this.isVisible = ko.observable(true);
        this.detailDate = ko.observable(null);
        this.detailData = ko.observableArray([]);

        this.lstTicketResponseAction = ko.observableArray([]);
        this.ticketResponseAction = ko.observable();
        this.txtRemark = ko.observable();

        this.ticketNumber = ko.observable();
        this.alertType = ko.observable();
        this.vehicleLicense = ko.observable();
        this.driver = ko.observable();

        this.ticketResponseAction.subscribe((val)=> {
            if(val != undefined) {
                this.txtRemark(val.name);
            } else {
                this.txtRemark(null);
            }
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return
        }

        this.getData();
    }

    getData() {
        this.isBusy(true);        
        var dfd = $.Deferred();
        var dfdOptionsTicketResponse = this.webRequestTickerResponse.ticketResponseList();
        var dfdDataLog = this.webRequestAlert.alertTicketDetail(this.alertTicketId);
        
        $.when(dfdOptionsTicketResponse, dfdDataLog).done((resOptionsTicketResponse, resDataLog)=> {
            this.ticketNumber(" : "+resDataLog[0].id);
            this.alertType(" : "+resDataLog[0].alertTypeDisplay);
            this.vehicleLicense(" : "+resDataLog[0].vehicleLicense);
            this.driver(" : "+resDataLog[0].driverName);
            if(resDataLog.length != 0) {
                let resDataLogLst = resDataLog.map((item, index)=>{
                    item.noteLst = []
                    item.formatDate = this.setDisplayFormatDate(item['date'])
                    item.noteDetail.map((note)=> {
                        item.noteLst.push({
                            formatDetail: note.note + " by " + note.createBy,
                            formatTime: note.formatTime
                        })
                    })

                    return item;
                });

                this.isVisible(true);
                this.detailData(resDataLogLst);
            } else {
                this.isVisible(false);
                this.detailData([]);
            }

            this.lstTicketResponseAction.replaceAll(resOptionsTicketResponse.items);    
            
            if(this.isPermission()
            && this.alertTicketStatus == Enums.ModelData.AlertTicketStatus.New) {  
                this.publishMessage("cw-fleet-monitoring-event-ticket-action-create-log");
            }
        }).always(()=> {
            this.isBusy(false);            
            dfd.resolve();
        });
        
        return dfd;
    }

    setDisplayFormatDate(date) {
        let formatDate = "";
        let d = new Date(date);
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();        
        
        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;  
        formatDate = day + "/" + month + "/" + year;

        return formatDate;
    }

    setupExtend() {
        this.txtRemark.extend({ trackChange : true});
        this.ticketResponseAction.extend({ trackChange : true });

        this.txtRemark.extend({ required : true });

        this.validationModel = ko.validatedObservable({
            txtRemark : this.txtRemark
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if(this.isPermission()) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createAction("actSaveAndClose", this.i18n("Tick_Action_Save_And_Close")()));
        }
        
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(this.isPermission()) {
            commands.push(this.createCommand("cmdClose", this.i18n("Tick_Action_Close_Tick")(), "svg-action-close"));
        }        
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch (sender.id) {
            case "actSave":
                    if (!this.validationModel.isValid()) {
                        this.validationModel.errors.showAllMessages();
                        return;
                    }
                    
                    this.isBusy(true);
                    let model = {
                        alertTicketId: this.alertTicketId,
                        note: this.txtRemark()
                    };

                    this.webRequestAlert.alertTicketCreateLog(model).done((res)=> {
                        this.getData();
                        this.clearFrom();
                        //this.publishMessage("cw-fleet-monitoring-alert-ticket-action-create-log");
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                break;

            case "actSaveAndClose":

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
                     
                 this.isBusy(true);
                    let filterSAC = [{
                        alertTicketId: this.alertTicketId,
                        note: this.txtRemark()
                    }];

                    this.webRequestAlert.alertTicketCloseTicket(filterSAC).done((res)=> {
                         this.publishMessage("cw-fleet-monitoring-event-ticket-action-close", { data: res });

                        // this.getData();
                        // this.clearFrom();
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                break;
        
            default:
                break;
        }

    
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdClose": {     
                this.isBusy(true);
                let model = [];
                model.push({
                    alertTicketId: this.alertTicketId
                });  

                this.webRequestAlert.alertTicketCloseTicket(model).done((res)=> {
                    this.publishMessage("cw-fleet-monitoring-event-ticket-action-close", { data: res });
                    this.close(true);
                }).fail((e)=> {
                    this.handleError(e);
                }).always(()=> {
                    this.isBusy(false);
                })                
                break;
            }
        }
    }

    clearFrom() {
        this.txtRemark(null);
    }

    get webRequestTickerResponse() {
        return WebRequestTickerResponse.getInstance();
    }

    get webRequestAlert() {
        return WebRequestAlert.getInstance();        
    }
}

export default {
    viewModel: ScreenBase.createFactory(EventViewTicketActionScreen),
    template: templateMarkup
};