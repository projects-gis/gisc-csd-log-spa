﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../../app/frameworks/core/utility";
import {
    Enums
} from "../../../../../../app/frameworks/constant/apiConstant";

import WebRequestAlertCategories from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";
import WebRequestAlertType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";




class TicketSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());
        this.bladeSize = BladeSize.Small;

        //ddl data source
        this.businessUnitList = ko.observableArray([]);
        this.vehicleList = ko.observableArray([]);
        this.alertCategoryList = ko.observableArray([]);
        this.alertTypeList = ko.observableArray([]);
        this.ticketStatusList = ko.observableArray([]);

        //filter variable
        this.businessUnit = ko.observable(null);
        this.includeSubBU = ko.observable(false);
        this.vehicle = ko.observable({});
        this.alertCategory = ko.observable();
        this.alertType = ko.observable();
        this.ticketStatus = ko.observable();

        //date format
        this.dateFormat = WebConfig.companySettings.shortDateFormat;

        //datetime
        var addDay = 30;
        var addMonth = 3;
        this.alertSearchStartDate = ko.observable(new Date());
        this.alertSearchEndDate = ko.observable(new Date());
        this.alertSearchStartTime = ko.observable('00:00');
        this.alertSearchEndTime = ko.observable('23:59');

        this.minDateAlert = ko.observable(Utility.minusMonths(new Date(), addMonth));
        this.maxDateAlert = ko.observable(Utility.addMonths(new Date(), addMonth));

        this.keyword = this.ensureObservable(params.keyword);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return
        }

        var dfd = $.Deferred();
        /* Process : Business Unit */
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        var dfdAlertCategory = this.webRequestAlertCategories.listAlertCategorySummary({})
        var dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter)
        var dfdAlertType = this.webRequestAlertType.listAlertTypeByAlertConfig({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.AlertType],
            excludeAlertTypes: [
                Enums.ModelData.AlertType.InvalidDriver,
                Enums.ModelData.AlertType.TemperatureError]
        })
        var dfdTicketStatus = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            "types": [Enums.ModelData.EnumResourceType.AlertTicketStatus]
        })


        $.when(dfdAlertCategory, dfdAlertType, dfdBusinessUnit, dfdTicketStatus).done((resAlertCategory, resAlertType, resBusinessUnit, resTicketStatus) => {
            this.alertTypeList(resAlertType.items);
            this.businessUnitList(resBusinessUnit.items);
            this.alertCategoryList(resAlertCategory.items);
            this.ticketStatusList(resTicketStatus.items)
            dfd.resolve();
        });

        this.businessUnit.subscribe((idBusiness) => {

            this.includeSubBU(false);
            this.vehicle(null);
            this.alertCategory(null);
            this.alertType(null);
            this.ticketStatus(null);

            this.vehicleList.replaceAll([])

            if (idBusiness) {
                var dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: idBusiness,
                    includeIds: [this.includeSubBU() ? 1 : 0],
                    vehicleTypes: [
                        Enums.ModelData.VehicleType.Car,
                        Enums.ModelData.VehicleType.Truck,
                        Enums.ModelData.VehicleType.Trailer,
                        Enums.ModelData.VehicleType.PassengerCar
                    ]
                })

                $.when(dfdVehicle).done((resVehicle) => {
                    this.vehicleList.replaceAll(resVehicle.items);
                    dfdVehicle.resolve();
                }).fail((e) => {
                    dfdVehicle.reject(e);
                });
            }

        })

        // StartDate condition
        this.alertSearchStartDate.subscribe((dateTime) => {
            let startDate = new Date(dateTime);
            let endDate = new Date(this.alertSearchEndDate());
            if (startDate > endDate) {
                this.setAlertDateMaxMin(dateTime);
                this.setAlertDateMaxMin();
            }

            if ((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.maxDateAlert(Utility.addMonths(dateTime, 1));
                this.setAlertDateMaxMin();
            }
        });

        // EndDate condition
        this.alertSearchEndDate.subscribe((dateTime) => {
            let startDate = new Date(this.alertSearchStartDate());
            let endDate = new Date(dateTime);
            if (endDate < startDate) {
                this.setAlertDateMaxMin(dateTime);
                this.setAlertDateMaxMin();
            }

            if ((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.minDateAlert(Utility.minusMonths(dateTime, 1));
                this.setAlertDateMaxMin();
            }
        });


        return dfd;
    }

    setAlertDateMaxMin(dateTime) {
        if (dateTime == undefined) {
            this.minDateAlert(Utility.minusMonths(new Date(), 3));
            this.maxDateAlert(Utility.addMonths(new Date(), 3));
        } else {
            this.minDateAlert(new Date(dateTime));
            this.maxDateAlert(new Date(dateTime));
        }
    }

    setupExtend() { }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        switch (sender.id) {
            case "actSearch":
                this.submitSearchFilter();
                break;
            default:
                break;
        }
    }

    setFormatDateTime(dateObj, timeObj, defaultTime) {

        var newDateTime = "";

        if (dateObj && timeObj) {

            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + timeObj + ":00";
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = timeObj + ":00";
                newDateTime = newDate + newTime;
            }

        } else if (dateObj && defaultTime) {
            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + defaultTime;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = defaultTime;
                newDateTime = newDate + newTime;
            }
        } else { }

        return newDateTime;
    }


    submitSearchFilter() {
        if (!this.validationModel.isValid()) {
            this.validationModel.errors.showAllMessages();
            return;
        }

        let filter = {};

        filter.businessUnitIds = this.businessUnit() ? this.businessUnit() : null;
        filter.includeSubBU = this.includeSubBU();
        filter.vehicleIds = this.vehicle() ? [this.vehicle().id] : null;
        filter.alertCategoryIds = this.alertCategory() ? [this.alertCategory().id] : null;
        filter.alertTypeIds = this.alertType() ? [this.alertType().id] : null;
        filter.alertTicketStatusIds = this.ticketStatus() ? [this.ticketStatus().value] : null;
        filter.startDate = this.setFormatDateTime(this.alertSearchStartDate(), this.alertSearchStartTime());
        filter.endDate = this.setFormatDateTime(this.alertSearchEndDate(), this.alertSearchEndTime());
        filter.keyword = this.keyword();
        this.publishMessage("cw-fleet-monitoring-event-ticket-filter-search", filter);


    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.businessUnit(null);
            this.includeSubBU(false);
            this.vehicle(null);
            this.alertCategory(null);
            this.alertType(null);
            this.ticketStatus(null);
            this.alertSearchStartDate(new Date());
            this.alertSearchEndDate(new Date());
            this.alertSearchStartTime('00:00');
            this.alertSearchEndTime('23:59');
        }
    }

    setupExtend() {

        this.alertSearchStartDate.extend({
            required: true
        })
        this.alertSearchEndDate.extend({
            required: true
        })

        this.alertSearchStartTime.extend({
            required: true
        })
        this.alertSearchEndTime.extend({
            required: true
        })

        this.validationModel = ko.validatedObservable({
            startDate: this.alertSearchStartDate,
            endDate: this.alertSearchEndDate,
            startime: this.alertSearchStartTime,
            endtime: this.alertSearchEndTime
        });

    }


    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }
    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }
    get webRequestAlertCategories() {
        return WebRequestAlertCategories.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(TicketSearchScreen),
    template: templateMarkup
};