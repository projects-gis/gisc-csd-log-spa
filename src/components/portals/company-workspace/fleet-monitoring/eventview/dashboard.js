﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestMDVRDashboard from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVRDashboard";
import WebRequestAlert from "../../../../../app/frameworks/data/apitrackingcore/webrequestAlert";
import Utility from "../../../../../app/frameworks/core/utility";

import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestGlobalSearch from "../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";
import QuickSearchScreenBase from "../../../quick-search-screenbase";

class EventViewDashboardScreen extends QuickSearchScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("FleetMonitoring_Event_Video_Dashboard")());
        this.bladeSize = BladeSize.Medium;
        this.bladeEnableQuickSearch = true;

        this.items = ko.observableArray([]);
        this.lastSync = ko.observable();
        // this.lastSync = ko.pureComputed(() => {
        //     return WebConfig.fleetMonitoring.alertDashboardLastSync();
        // });

        // this.items = ko.pureComputed(() => {
        //     return 0;//WebConfig.fleetMonitoring.alertDashboardItems();
        // });
        this.displayTicket = ko.observable(WebConfig.userSession.hasPermission([
            Constants.Permission.Manage_Ticket,
            Constants.Permission.Ticket_View_Only
        ]) > 0);

        this.ticketNew = ko.observable(null);
        this.ticketInprogress = ko.observable(null);
        this.ticketClosed = ko.observable(null);
        this.ticketAll = ko.observable(null);
        this.keyword = ko.observable(null);
        //this.subscribeMessage("cw-fleet-monitoring-alert-ticket-action-close", (obj) => {
        //    if (this.displayTicket()) {
        //        this.getAlertTicketDashboardData();
        //    }
        //});

        //this.subscribeMessage("cw-fleet-monitoring-alert-ticket-action-create-log", (data) => {
        //    if (this.displayTicket()) {
        //        this.getAlertTicketDashboardData();
        //    }
        //});

        //this.subscribeMessage("cw-fleet-monitoring-alert-ticket-multi-close-alert-ticket", () => {
        //    if (this.displayTicket()) {
        //        this.getAlertTicketDashboardData();
        //    }
        //});
    }

    get webRequestMDVRDashboard() {
        return WebRequestMDVRDashboard.getInstance();
    }

    get webRequestAlert() {
       return WebRequestAlert.getInstance();
    }

    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }


    gotoAlertTicketList(status) {
        let statusId = [];
        switch (status) {
            case "new":
                statusId.push(Enums.ModelData.AlertTicketStatus.New);
                break;
            case "inprogress":
                statusId.push(Enums.ModelData.AlertTicketStatus.Inprogress);
                break;
            case "closed":
                statusId.push(Enums.ModelData.AlertTicketStatus.Close);
                break;
            case "all":
                statusId = null;
                break;
            default:
                break;
        }
        this.navigate("cw-fleet-monitoring-eventview-ticket", {statusId: statusId, keyword: this.keyword()});
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        this.isBusy(true)
        var dfd = $.Deferred();
        this.refreshData().done(() => {
            dfd.resolve();
        }).fail(e => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
        return dfd;

    }

    /**
     * @lifecycle Handle when search on text box is clicked or enter.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        let keyword = this.quickSearchKeyword();
        if( 
            (event.keyCode === 13 || event.type === 'click') && 
            (_.size(keyword) >= data.minLength || _.size(keyword) === 0) 
        ){
            this.isBusy(true);

            //set keyword for refresh (optional)
            this.keyword(keyword);
            //call dashboard with kyword
            this.refreshData(keyword).done(() => {
                this.isBusy(false);
            }).fail(e => {
                this.isBusy(false);
            }).always(() => {
                this.isBusy(false);
            });
        }
        return true;
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: request.term,
        }
        this.webRequestGlobalSearch.autocompleteDriverVehicle(filter).done((data) => {
            response( $.map( data, (item) => {
                return {
                    label: item,
                    value: '"' + item + '"'
                };
            }));
        });      
    }

    getAlertTicketDashboardData(keyword) {
        this.isBusy(true);
        this.webRequestMDVRDashboard.getAlertTicket(keyword).done((res) => {
            this.ticketNew(res.formatNewAlertTickets);
            this.ticketInprogress(res.formatInproAlertTickets);
            this.ticketClosed(res.formatCloseAlertTickets);
            this.ticketAll(res.formatTotalAlertTickets);
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }
    onItemSelected(alertTypeId) {
        let currentDateTime = new Date();
        let endDate = Utility.resetEndTime(currentDateTime);
        let startDate = Utility.resetTime(currentDateTime);

        // let endDate= "2018-08-31T00:00:00";
        // let startDate = "2018-08-1T00:00:00";

        let companyId = WebConfig.fleetMonitoring.companyId;
        this.navigate("cw-fleet-monitoring-eventview-list", {
            alertTypeId: alertTypeId,
            companyId: companyId,
            startDate: startDate,
            endDate: endDate,
            keyword: this.keyword()
        });
    }

    refreshData(keyword="") {
        // if (this.displayTicket())
        //call dashboard with kyword
        if(this.displayTicket()){
            this.getAlertTicketDashboardData(keyword);
        }
        //this.getAlertTicketDashboardData(keyword);
        this.minimizeAll(false);
        var dfd = $.Deferred();

        let dfdGraph = this.webRequestMDVRDashboard.getAlert(WebConfig.fleetMonitoring.companyId, null, keyword);
        $.when(dfdGraph).done((response) => {
            this.items(response.items)
            this.lastSync(response.formatTimestamp)
            dfd.resolve();

        }).fail(e => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });

        return dfd;

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
       
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
                let keyword = this.keyword();
                //set keyword current search
                this.quickSearchKeyword(keyword);
                this.refreshData(keyword);
                break;
        }
    }

    ajaxPost(url, data, successCallback) {
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: successCallback
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(EventViewDashboardScreen),
    template: templateMarkup
};