﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import WebRequestMDVRDashboard from "../../../../../app/frameworks/data/apitrackingcore/webRequestMDVRDashboard";
import WebRequestReportTemplate from "../../../../../app/frameworks/data/apitrackingcore/webrequestReportTemplate";
import HikvisionUtility from "../../../../../app/frameworks/core/hikvisionUtility";
import IQTechUtility from "../../../../../app/frameworks/core/iqtechUtility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";

import {
    AlertFilter
} from "./infos";
import AssetUtility from "../../asset-utility";
import {
    AssetIcons
} from "../../../../../app/frameworks/constant/svg";

import WebRequestGlobalSearch from "../../../../../app/frameworks/data/apitrackingcore/webrequestGlobalSearch";
import QuickSearchScreenBase from "../../../quick-search-screenbase";

class EventViewListScreen extends QuickSearchScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("FleetMonitoring_ViewEventView")());
        this.bladeSize = BladeSize.XLarge;
        this.mdvrModelCompany = ko.observable('');
        this.bladeEnableQuickSearch = true;
        this.columns = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);

        let alertTypeId = this.ensureNonObservable(params.alertTypeId);
        let startDate = this.ensureNonObservable(params.startDate);
        let endDate = startDate;
        let keyword = this.ensureNonObservable(params.keyword);
        
        this.searchFilter = ko.observable(new AlertFilter(null, alertTypeId, null, startDate, endDate, null, null, keyword));
        this.searchFilterDefault = ko.observable(new AlertFilter()); // Use to compare with searchFilter to apply active state
        this.keyword = ko.observable("");
        this.quickSearchKeyword(keyword ? keyword : null);
        this.pageFiltering = ko.observable(false); // Use to apply active state for command search
        this.lastRecentlyUsedSortingColumns = null;
        this._selectingRowHandler = (alert,col) => {
           
            if(col != "playVideoDisplayName"){
                if (alert) {
                //จะ Zoom ไปยังตำแหน่งของจุดที่เกิดเหตุการณ์
                this.drawAlertOnMap(alert.latitude, alert.longitude, alert);
                this.minimizeAll();
                //return empty deferred because if not return deferred, selected row does not work properly.
                return Utility.emptyDeferred();
                }
                return false;
            }
        };

        this.svgs = {
            "movement": AssetIcons.IconMovementTemplateMarkup,
            "speed": AssetIcons.IconSpeedTemplateMarkup,
            "gps": AssetIcons.IconGpsTemplateMarkup,
            "fuel": AssetIcons.IconFuelTemplateMarkup,
            "engine": AssetIcons.IconEngineTemplateMarkup
        };

        this.filter = ko.observable({
            "alertTypeIds": params.alertTypeId,
            // "startDate": "2018-07-01T14:23:00",   //MOCK// Require??
            // "endDate": "2018-07-30T14:23:00",     //MOCK// Require??
            "companyId": params.companyId,
            "startDate": params.startDate,
            "endDate": params.endDate
        });

        this.isNewWidget = false;
    }


    /**
     * 
     * Get MapManager
     * @readonly
     */
    get mapManager() {
        return MapManager.getInstance();
    }

    /**
     * Get WebRequest specific for alert module in Web API access.
     * @readonly
     */
    get webRequestAlert() {
        return WebRequestAlert.getInstance();
    }

    get webRequestMDVRDashboard() {
        return WebRequestMDVRDashboard.getInstance()
    }

    get hikvisionUtility() {
        return HikvisionUtility.getInstance();
    }
    
    get iqtechUtility() {
        return IQTechUtility.getInstance();
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for GlobalSearch module in Web API access.
     * @readonly
     */
    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }

    /**
     * Get WebRequest specific for ReportTemplate module in Web API access.
     * @readonly
     */
    get webRequestReportTemplate() {
        return WebRequestReportTemplate.getInstance();
    }

    drawAlertOnMap(lat, lon, data) {

        data["svgIcon"] = this.svgs;

        this.mapManager.drawAlertPoint(this.id, {
                lat: lat,
                lon: lon
            }, {
                url: this.resolveUrl("~/images/pin_alerts.png"),
                width: WebConfig.mapSettings.symbolWidth,
                height: WebConfig.mapSettings.symbolHeight,
                offset: {
                    x: WebConfig.mapSettings.symbolOffsetX,
                    y: WebConfig.mapSettings.symbolOffsetY
                },
                rotation: WebConfig.mapSettings.symbolRotation
            },
            WebConfig.mapSettings.defaultZoom,
            data);
    }

    /**
     * Provide datagrid datasource with paginate.
     * 
     * @param {any} gridOption
     */
    onDatasourceRequestRead(gridOption) {

        var dfd = $.Deferred();

        var filter = Object.assign({}, this.filter(), gridOption);
        this.isBusy(true);

        this.webRequestMDVRDashboard
            .alertSummaryList(this.generateFilter(gridOption))
            .done(response => {

                AssetUtility.updateIconResult(response.vehicleIcons, response.poiIcons);
                let reformData = [];

                dfd.resolve({
                    items: response.items,
                    totalRecords: response.totalRecords, //hardcode
                    currentPage: (response.currentPage != null) ? response.currentPage :  1
                    //response.currentPage != null ? response.currentPage :  1
                    // totalRecords: response["totalRecords"],
                    // currentPage: response["currentPage"]
                });
            })
            .fail(e => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
                // this.onAutoRefresh(true);
            });

        return dfd;
    }
    formatData(items) {
        let reformData = [];

        for (let i = 0; i < items.length; i++) {
            var dataItem = items[i];
            dataItem.alertTypeDisplayName = dataItem.alertTypeDisplayName == null ? "-" : dataItem.alertTypeDisplayName;
            dataItem.vehicleLicense = dataItem.vehicleLicense == null ? "-" : dataItem.vehicleLicense;
            dataItem.driverEmployeeId = dataItem.driverEmployeeId == null ? "-" : dataItem.driverEmployeeId;
            dataItem.driverName = dataItem.driverName == null ? "-" : dataItem.driverName;

            dataItem.coDrivers = dataItem.coDrivers == null || dataItem.coDrivers == "" ? "-" : dataItem.coDrivers;
            dataItem.businessUnitName = dataItem.businessUnitName == null ? "-" : dataItem.businessUnitName;
            dataItem.formatAlertDateTime = dataItem.formatAlertDateTime == null ? "-" : dataItem.formatAlertDateTime;

            dataItem.iconUrl = {
                iconUrl: this.resolveUrl('~/images/icon_eventview_playvideo.png')
            };

            reformData.push(dataItem);
        }

        return reformData;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var self = this;

        var dfd = $.Deferred();

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
        $.when(dfdCompanySettings).done((res) => {
            this.mdvrModelCompany(res.mdvrModel) //for check create button
            // console.log("dfdCompanySettings",res);
        });
        // Calling web service for dynamic column definitions.
        this.webRequestReportTemplate.listReportTemplate({
                reportTemplateType: Enums.ModelData.ReportTemplateType.Alert,
                includeAssociationNames: [
                    EntityAssociation.ReportTemplate.ReportTemplateFields,
                    EntityAssociation.ReportTemplateField.Feature
                ]
            })
            .done((response) => {
                var reportTemplateFields = response.items["0"].reportTemplateFields;

                var columnVideo = {
                    name: "PlayVideo",
                    displayName: "Video",
                    id: 34,
                    mapPropertyName: "PlayVideoDisplayName",
                    order: 34,
                    reportTemplateId: 1
                }
                reportTemplateFields.splice(0, 0, columnVideo);

                var columns = AssetUtility.getColumnDefinitions(reportTemplateFields, true, 0, Enums.SortingColumnName.AlertType, this);

                this.columns(columns);
                dfd.resolve();
            })
            .fail((e) => {
                dfd.reject(e);
            });

        return dfd;

    }

    /**
     * @lifecycle Handle when search on text box is clicked or enter.
     * @param {any} data
     * @param {any} event
     */
    onQuickSearchClick(data, event) {
        let keyword = this.quickSearchKeyword();
        let filter = this.searchFilter();
        if( 
            (event.keyCode === 13 || event.type === 'click') && 
            (_.size(keyword) >= data.minLength || _.size(keyword) === 0) 
        ){
            this.quickSearchKeyword(keyword);
            this.keyword(keyword);
            filter.keyword = this.keyword();
            this.searchFilter(filter);
            this.dispatchEvent("dgEventVideoList", "refresh");

        }
        return true;
    }

    /**
     * @lifecycle web request global search autocomplete.
     * @param {any} request
     * @param {promise} response
     */
    onDataSourceQuickSearch(request, response) {
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            keyword: request.term,
        }
        this.webRequestGlobalSearch.autocompleteDriverVehicle(filter).done((data) => {
            response( $.map( data, (item) => {
                return {
                    label: item,
                    value: '"' + item + '"'
                };
            }));
        });          
    }

    /**
     * Prepare API filter with datatable parameters.
     */
    generateFilter(gridOption) {
        // Clone filter because export option does not require paginate.
        var filter = _.cloneDeep(this.searchFilter());

        // Update standard filter with sorting, paginate parameters.

        if (!_.isEmpty(gridOption.sort)) {
            var sortDir = (gridOption.sort[0].dir === "asc") ?
                Enums.SortingDirection.Ascending :
                Enums.SortingDirection.Descending;

            // Required map back from grid to web service with enum id.
            var sortColumnName = "";
            var columnDefinition = _.find(this.columns(), {
                data: gridOption.sort[0].field
            });
            if (columnDefinition) {
                sortColumnName = columnDefinition.enumColumnName;
            }

            // Apply sorting from user then id.
            filter.sortingColumns = [{
                    column: sortColumnName,
                    direction: sortDir
                },
                {
                    column: Enums.SortingColumnName.Id,
                    direction: sortDir
                }
            ];
        } else {
            // Grid doesn't send any sorting information so using default.
            filter.sortingColumns = [{
                    column: Enums.SortingColumnName.AlertDateTime,
                    direction: Enums.SortingDirection.Descending
                },
                {
                    column: Enums.SortingColumnName.Id,
                    direction: Enums.SortingDirection.Ascending
                }
            ];
        }

        // Store sotring filter for export function.
        this.lastRecentlyUsedSortingColumns = filter.sortingColumns;
        filter.displayStart = gridOption.skip;
        filter.displayLength = gridOption.pageSize;
        filter.includeCount = true;

        var filterWithGrid = Object.assign({}, filter, gridOption);
        
        return filterWithGrid;
    }


    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.mapManager.clear();
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        //commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));

    

        if(this.mdvrModelCompany() === Enums.ModelData.MDVRModel.All){

            commands.push(this.createCommand("cmdDownloadUserGuideIQTech", `${this.i18n("Download_User_Guide")()} (IQTech)`, "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadUserGuideHikvision", `${this.i18n("Download_User_Guide")()} (Hikvision)`, "svg-icon-load-guide"));
            
            commands.push(this.createCommand("cmdDownloadPluginIQTech", `${this.i18n("Download_Plugin")()} (IQTech)`, "svg-icon-load-plugin"));
            commands.push(this.createCommand("cmdDownloadPluginHikvision", `${this.i18n("Download_Plugin")()} (Hikvision)`, "svg-icon-load-plugin"));

            commands.push(this.createCommand("cmdStartPlugin", this.i18n("Start_Plugin")(), "svg-icon-start-plugin"));

        }else if(this.mdvrModelCompany() === Enums.ModelData.MDVRModel.IQTech) {

            commands.push(this.createCommand("cmdDownloadUserGuideIQTech", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadPluginIQTech", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));

        }else if(this.mdvrModelCompany() === Enums.ModelData.MDVRModel.DFI) {

            commands.push(this.createCommand("cmdDownloadUserGuideIQTech", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadPluginIQTech", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));

        }else if(this.mdvrModelCompany() === Enums.ModelData.MDVRModel.HIKvision){

            commands.push(this.createCommand("cmdDownloadUserGuideHikvision", this.i18n("Download_User_Guide")(), "svg-icon-load-guide"));
            commands.push(this.createCommand("cmdDownloadPluginHikvision", this.i18n("Download_Plugin")(), "svg-icon-load-plugin"));

        }

        //commands.push(this.createCommand("testPlayVideo", this.i18n("Test Play Video")(), "svg-icon-start-plugin"));
        //commands.push(this.createCommand("cmdWidget",this.i18n("widget")(),"svg-icon-start-plugin"));
        // commands.push(this.createCommand("testPlayback", this.i18n("PlayVideoTest")(), "svg-cmd-export"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {

            case "cmdDownloadUserGuideIQTech":

                    ScreenHelper.downloadFile(this.resolveUrl('~/vendors/iqtech/LiveviewEventviewUserGuide_iqtech.pdf'));

                break;
            case "cmdDownloadUserGuideHikvision":

                    ScreenHelper.downloadFile(this.resolveUrl('~/vendors/hikvision/LiveviewEventviewUserGuide.pdf'));

                break;
            case "cmdDownloadPluginIQTech":

                    window.open('https://get.adobe.com/flashplayer/')

                break;
            
            case "cmdDownloadPluginHikvision":

                    ScreenHelper.downloadFile(this.resolveUrl('~/vendors/hikvision/PlatformSDKWebControlPlugin.zip'));

                break;

            case "cmdStartPlugin":
            
                    this.hikvisionUtility.startPlugin();

                break;
            
            case "cmdSearch":
                
    
                break;

            case "cmdExport":

                // this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                //     switch (button) {
                //         case BladeDialog.BUTTON_YES:
                //             this.isBusy(true);
                //             var filter = this.searchFilter();
                //             filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                //             filter.sortingColumns = this.lastRecentlyUsedSortingColumns;
                //             this.webRequestAlert.exportAlert(filter)
                //                 .done((response) => {
                //                     this.isBusy(false);
                //                     ScreenHelper.downloadFile(response.fileUrl);
                //                 }).fail((e) => {
                //                     this.isBusy(false);
                //                     this.handleError(e);
                //                 });
                //             break;
                //     }
                // });
                break;
        }
    }



    ajaxPost(url, data, successCallback) {
        $.ajax({
            url: url,
            data: data,
            type: 'post',
            success: successCallback
        });
    }

}

export default {
    viewModel: ScreenBase.createFactory(EventViewListScreen),
    template: templateMarkup
};