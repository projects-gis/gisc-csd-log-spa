﻿import ko from "knockout";
import templateMarkup from "text!./eventview-widget.html";
import ScreenBaseWidget from "../../../screenbasewidget";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import 'iqtech-swf';


class PlaybackInfoWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);
        // Sample properties.
        this.videoInfos = params.videoInfos;
        this.dataEventVideo = params.eventvideo;
        this.cameraConfig = params.cameraConfig;
        this.isInitFinished = false;
        this.ChannelCount = this.ensureObservable(params.eventvideo.length, 4);

        this.DisplayChannel = ko.pureComputed(() => {
            let ChannelCountIndex = [];
            let ChannelCountRun = 0;
            params.eventvideo.forEach(function(item, index) {
                ChannelCountIndex.push(+item.channel);
            });
            ChannelCountRun = Math.max(...ChannelCountIndex);
            ChannelCountRun++;
        
            return ChannelCountRun;
        });
    }

    afterRender() {
        
        setTimeout(()=>{
            //let videoInfos = this.videoInfos;
            let mdvrBrand = this.videoInfos.mdvrBrand;
            //let mdvrBrand = 4; //demo
            if(mdvrBrand == 4){
                this.initVideoJS(); 
            }else{
                this.initPlugin();
            }
        }, 500);
    }

    initPlugin() {
        var self = this;
        let allChannel = this.DisplayChannel();

        try {
            //Video plug-in init param
            var params = {
                allowFullscreen: "true",
                allowScriptAccess: "always",
                bgcolor: "#FFFFFF",
                wmode: "transparent"
            };
            //Init flash
            swfobject.embedSWF(WebConfig.appSettings.iqTechSWFObjectUrl, self.id, 490, 480, "9.0.0", null, null, params, null);

            switch ( allChannel ) {
                case 1:
                    var ObjChannel = 1; break;
                case 2:
                case 3:
                case 4:
                    var ObjChannel = 4; break;
                case 5:
                case 6:
                    var ObjChannel = 6; break;
                case 7:
                case 8:
                    var ObjChannel = 8; break;
                default:
                    var ObjChannel = 9; break;
            }


            setTimeout(function () {
            
                let iqTechIpConfig = self.cameraConfig;//WebConfig.appSettings.iqTechConfig.split("|");

                //Setting the language video widget
                swfobject.getObjectById(self.id).setLanguage(WebConfig.appSettings.iqTechLangUrl);
                swfobject.getObjectById(self.id).setWindowNum(36);
                //Re-configure the current number of windows
                swfobject.getObjectById(self.id).setWindowNum(ObjChannel);
                //Set the video plug-in server
                swfobject.getObjectById(self.id).setServerInfo(iqTechIpConfig[2], "6605");
                self.isInitFinished = true;

                self.startPlayback(self.id);
            }, 1000)

        } catch (ex) {
            console.log(ex);
        }
    }

    startPlayback(id) {
        let Channel_target = [];
        let Channel_target_date = [];
        let playbackUrl = this._findVideo(this.dataEventVideo);
        let videoInfos = this.videoInfos;
        let allChannel = this.dataEventVideo.length;
        this.dataEventVideo = playbackUrl;
        // console.log(this.dataEventVideo);
        // console.log("playbackUrl",playbackUrl)
        // console.log("videoInfos",videoInfos)
        // console.log("allChannel",allChannel)
        
        //let videoUrlFixChn = []; //not use
    
        $('#alertName').text(videoInfos.alertTypeDisplayName);
        $('#alertTime').text(videoInfos.formatAlertDateTime);
        $('.k-grid-height').css("height", "90%")
        if (!this.isInitFinished) {
            return;
        } else {

            if(allChannel > 0){
                swfobject.getObjectById(id).stopVideo(0);
                for (let n = 0; n < allChannel; n++) {
                    let channel = this.videoInfos.alertMediaInfo[n].channelId;   
                        channel = +channel; 
                    if(typeof Channel_target[channel] === 'undefined') {
                        Channel_target[channel] = this.videoInfos.alertMediaInfo[n].fileName; 
                    } 
                }
            
                Channel_target.forEach(function(item, index) {
                    //Play Video
                    swfobject.getObjectById(id).startVod(index , item );
                 });
            }else{
                //Common_NotFoundInstruction
                //console.log(100);
            }
        }
    }

    _findVideo(items){
        var nFiles = new Array();
        let alertDateTime = this.videoInfos.alertDateTime;
        let alertTime = this._convertTimeFormat(alertDateTime);
        let files = _.filter(items, (item)=>{
            return item.beg <= alertTime;
        });
        let oFiles = _.orderBy(files, ['beg'], ['desc']);
        // _.forEach(oFiles, (item)=>{
        //     let fData = _.filter(nFiles, (o)=>{
        //         return o.channel === item.channel
        //     });
        //     if(!_.size(fData)){
        //         nFiles.push(item);
        //     }
        // });
        //return nFiles;
        return oFiles;
    }

    _convertTimeFormat(paramsDate) {
        let dt = new Date(paramsDate);
        var retFormat = "";
        if (paramsDate) {
            retFormat = (dt.getHours() * 3600) + (dt.getMinutes() * 60) + (dt.getSeconds());
        }
        return retFormat.toString();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
    }

    initVideoJS(){
        var self = this;
        let allChannel = self.videoInfos.alertMediaInfo.length;
     
        try {
            //Video plug-in init param
            var params = {
                allowFullscreen: "true",
                allowScriptAccess: "always",
                bgcolor: "#FFFFFF",
                wmode: "transparent"
            };
            //Init flash
            swfobject.embedSWF(WebConfig.appSettings.iqTechSWFObjectUrl, self.id, 490, 480, "9.0.0", null, null, params, null);

            switch ( allChannel ) {
                case 1:
                    var ObjChannel = 1; break;
                case 2:
                case 3:
                case 4:
                    var ObjChannel = 4; break;
                case 5:
                case 6:
                    var ObjChannel = 6; break;
                case 7:
                case 8:
                    var ObjChannel = 8; break;
                default:
                    var ObjChannel = 9; break;
            }
 

            setTimeout(function () {
            
                let dfiIpConfig = self.cameraConfig;
       
                //Setting the language video widget
                swfobject.getObjectById(self.id).setLanguage(WebConfig.appSettings.iqTechLangUrl);
                swfobject.getObjectById(self.id).setWindowNum(36);
                //Re-configure the current number of windows
                swfobject.getObjectById(self.id).setWindowNum(ObjChannel);
                //Set the video plug-in server
                swfobject.getObjectById(self.id).setServerInfo(dfiIpConfig[2], "6605");
    
                self.isInitFinished = true;
          
                self.startPlayback(self.id);
            }, 1000)

        } catch (ex) {
            console.log(ex);
        }
    }
}

export default {
    viewModel: ScreenBaseWidget.createFactory(PlaybackInfoWidget),
    template: templateMarkup
};