﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import {Enums,Constants} from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Navigation from "../../../../controls/gisc-chrome/shell/navigation";

class TripAnalysisSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('TripAnalysis_TripAnalysisFilter')());
        this.dataToSender = ko.observable();
        this.businessUnits = ko.observableArray([]);
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.entityTypes = ko.observableArray([]);
        this.selectedEntityType = ko.observable();
        this.selectEntity = ko.observableArray([]);
        this.selectedEntity = ko.observable();
        this.radEndTrip = ko.observable(1);
        this.valuePeriod = ko.observable("Today");
        this.isDateTimeEnabled = ko.observable(false);
        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);

        this.isIncludeFormerDriver = ko.observable(false);
        this.isIncludeResignedDriver = ko.observable(false);

        //datetime
        this.formatSet = "dd/MM/yyyy";//WebConfig.companySettings.shortDateFormat;
        var addDay = 30;
        var addMonth = 3;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(), addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(), addMonth));

        this.driverVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;
        });


        this.isAutoMailOpen = ko.observable(true)


        this.visiblePoitoPoi = ko.pureComputed(() => {
            var visible = this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle;
            let selectedDriver = this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;

            if (selectedDriver && this.radEndTrip() == Enums.ModelData.EndTripCriteria.POItoPOI) {
                this.radEndTrip(Enums.ModelData.EndTripCriteria.Parking);
            }
            return visible;
        });
        this.selectedEntityBinding = ko.pureComputed(() => {
            var businessUnitIds = this.selectedBusinessUnit();
            var result = this.i18n('Report_All')();//this.i18n('Common_PleaseSelect')();
            this.selectEntity([]);
            //if check include Sub BU
            if (this.isIncludeSubBU() && businessUnitIds != null) {
                businessUnitIds = Utility.includeSubBU(this.businessUnits(), businessUnitIds);
            }

            this.entityTypes(this.tempEntityTypes())

            if ((this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle) && businessUnitIds != null) {

                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;

                    //add displayName "Report_All" and id 0
                    //listSelectEntity.unshift({
                    //    displayName: this.i18n('Report_All')(),
                    //    id: 0
                    //});

                    this.selectEntity(listSelectEntity);
                    //var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    //this.selectedEntity(defaultSelectEntity);
                });
            } else if ((this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver) && businessUnitIds != null) {
                let includeFormerDriver = this.isIncludeFormerDriver() ? true : null;
                let includeResignedDriver = this.isIncludeResignedDriver() ? null : true;

                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver,
                    IncludeFormerInBusinessUnit : this.isIncludeFormerDriver(),
                    isEnable: this.isIncludeResignedDriver() ? null : true ,

                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;

                    //add displayName "Report_All" and id 0
                    //listSelectEntity.unshift({
                    //    displayName: this.i18n('Report_All')(),
                    //    id: 0
                    //});

                    this.selectEntity(listSelectEntity);
                    //var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    //this.selectedEntity(defaultSelectEntity);
                });
            } else {
                this.selectEntity().unshift({
                    displayName: this.i18n('Report_All')(),
                    id: 0
                });
                var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                this.selectedEntity(defaultSelectEntity);
            }

            
            return result;
        });


        this.periodDay = ko.pureComputed((e) => {
            addDay = 30;
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if (calDay > 0) {
                isDate = new Date();
            }

            // set date when clear start date and end date
            if (this.start() === null) {
                // set end date when clear start date
                if (this.end() === null) {

                    isDate = new Date();

                } else {

                    isDate = this.end();
                }

                this.maxDateStart(isDate);
                this.minDateEnd(isDate);

            } else {

                this.minDateEnd(this.start())

            }

            return isDate;
        });

        // Date Time Criteria
        this.valuePeriod.subscribe((res) => {
            let currentDate = new Date();
            var startDate = new Date();
            var endDate = new Date();

            switch (res) {
                case 'Today':
                    this.isDateTimeEnabled(false);
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }));
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;

                case 'Yesterday':

                    this.isDateTimeEnabled(false);

                    startDate.setDate(startDate.getDate() - 1)
                    // console.log("startDate.setDate(startDate.getDate() - 1)",startDate)
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(startDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;

                case 'ThisWeek':
                    this.isDateTimeEnabled(false);

                    let startWeek;

                    //setStartDate
                    if (currentDate.getDay() == 0) { //ถ้าเป็นวันอาทิตย์ ( 0 คือวันอาทิตย์ )

                        startWeek = parseInt((startDate.getDate() - 6));

                    } else if (currentDate.getDay() == 1) { //วันจันทร์

                        startWeek = parseInt((startDate.getDate()));

                    }
                    else {

                        startWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    }

                    startDate.setDate(startWeek)

                    //setEndDate
                    if (currentDate.getDay() == 1) { //ถ้าเป็นวันจันทร์ ( 1 คือวันจันทร์ )
                        endDate.setDate(currentDate.getDate());
                        this.maxDateStart(new Date(), 0);
                    }
                    else {
                        endDate.setDate(currentDate.getDate());
                    }

                    // startDate.setDate(startWeek)
                    // endDate.setDate(endWeek)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))



                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;

                case 'LastWeek':

                    this.isDateTimeEnabled(false);
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek)
                    endDate.setDate(LastendWeek - 1)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));

                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;
                case 'ThisMonth':
                    this.isDateTimeEnabled(false);
                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);


                    this.start(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));
                    $("#from").val(this.setFormatDisplayDateTime(firstDay));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(firstDay));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(lastDay));
                    break;
                case 'LastMonth':
                    this.isDateTimeEnabled(false);
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.start(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#to").val(this.setFormatDisplayDateTime(LastlastDay));

                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;
                case 'Custom':
                    this.isDateTimeEnabled(true);
                    break;

                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });

        this.selectedEntityType.subscribe((val) => {
           
        });
        this.subscribeMessage("cw-automail-manage-close", (data) => {
            if(data){
                this.isAutoMailOpen(data.status)

                if(this.valuePeriod() === "Custom"){
                    this.isDateTimeEnabled(true)
                }
            }
        });

    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for EnumResource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
     * Get WebRequest specific for Driver module in Web API access.
     * @readonly
     */
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.VIEWAUTOMAILREPORT)){
            commands.push(this.createCommand("cmdViewAutoMail", this.i18n('Report_Manage_Automail')(), "svg-ic-mt-report"))
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.CREATAUTOMAILREPORT)){
            commands.push(this.createCommand("cmdCreateAutoMail", this.i18n('Report_Create_Automail')(), "svg-cmd-add"))
        }
    }
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdViewAutoMail":
                this.closeAll();
                Navigation.getInstance().navigate("cw-automail-report-manage-list", {}); 
                break;
            case "cmdCreateAutoMail":
                    if (this.validationModel.isValid()) {

                        let formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
                        let formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";
        
                        let reportNamespace = "";
        
                        if (this.radEndTrip() == Enums.ModelData.EndTripCriteria.POItoPOI) {
                            reportNamespace = "Gisc.Csd.Service.Report.Library.TripAnalysis.TripAnalysisPOIReport, Gisc.Csd.Service.Report.Library";
                        }
                        else {
                            reportNamespace = "Gisc.Csd.Service.Report.Library.TripAnalysis.TripAnalysisReport, Gisc.Csd.Service.Report.Library";
                        }
        
                        let repportParameters = {
                            userId: WebConfig.userSession.id,
                            companyId: WebConfig.userSession.currentCompanyId,
                            startDate: formatSDate,
                            endDate: formatEDate,
                            excel: true,
                            businessUnitId: this.selectedBusinessUnit() == null ? 0 : this.selectedBusinessUnit(),
                            entityTypeId: this.selectedEntityType().value,
                            selectedEntityId: this.selectedEntity() == null ? 0 : this.selectedEntity().id,
                            includeSubBusinessUnit: this.isIncludeSubBU(),
                            tripCriteriaId: this.radEndTrip(),
                            printBy: WebConfig.userSession.fullname,
                            language: WebConfig.userSession.currentUserLanguage,
                            reportTypeId:this.radEndTrip(),
                            reportCategory:7
                        };
        
                        //console.log(repportParameters, reportNamespace);
        
                        this.reports = {
                            report: reportNamespace,
                            parameters: repportParameters
                        };
        
                        let reportExport = { PDF: false, Word: false, CSV: false };
                        // this.navigate("cw-fleet-monitoring-tripanalysis-report", 
                        // {   reportSource: this.reports, 
                        //     reportName: this.i18n('Report_TripAnalysis')(), 
                        //     reportExport: reportExport 
                        // });
                        this.navigate("cw-automail-report-manage-create", {
                            dataToSender:{
                                reportSource: this.reports, 
                                reportName: this.i18n('Report_TripAnalysis')(), 
                                reportExport: reportExport 
                            },
                            mode:'create'
                        });
                        this.isAutoMailOpen(false)
                        this.isDateTimeEnabled(false)
        
                    }
                    else {
                        this.validationModel.errors.showAllMessages();
                    }
                // if(dataToSender){
                //     this.navigate("cw-automail-report-manage-create", {
                //         dataToSender:{
                //             reportSource: this.reports, 
                //             reportName: this.i18n('Report_TripAnalysis')(), 
                //             reportExport: reportExport 
                //         },
                //         mode:'create'
                //     });
                // }
            break;
            default:
                break;
        }
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);

        $.when(d1,d2).done((r1,r2) => {

            this.businessUnits(r1.items);
            this.tempEntityTypes(r2.items);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    setupExtend() {
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            end: this.end,
            start: this.start,
            timeStart: this.timeStart,
            timeEnd: this.timeEnd
        });
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if (sender.id === "actPreview") {

            if (this.validationModel.isValid()) {

                let formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
                let formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";

                let reportNamespace = "";

                if (this.radEndTrip() == Enums.ModelData.EndTripCriteria.POItoPOI) {
                    reportNamespace = "Gisc.Csd.Service.Report.Library.TripAnalysis.TripAnalysisPOIReport, Gisc.Csd.Service.Report.Library";
                }
                else {
                    reportNamespace = "Gisc.Csd.Service.Report.Library.TripAnalysis.TripAnalysisReport, Gisc.Csd.Service.Report.Library";
                }

                let repportParameters = {
                    UserID: WebConfig.userSession.id,
                    CompanyID: WebConfig.userSession.currentCompanyId,
                    StartDate: formatSDate,
                    EndDate: formatEDate,
                    Excel: true,
                    BusinessUnitID: this.selectedBusinessUnit() == null ? 0 : this.selectedBusinessUnit(),
                    EntityTypeID: this.selectedEntityType().value,
                    SelectedEntityID: this.selectedEntity() == null ? 0 : this.selectedEntity().id,
                    IncludeSubBusinessUnit: this.isIncludeSubBU(),
                    TripCriteriaID: this.radEndTrip(),
                    PrintBy: WebConfig.userSession.fullname,
                    Language: WebConfig.userSession.currentUserLanguage,
                    IncludeFormerInBusinessUnit: this.isIncludeFormerDriver() ? 1 : 0 ,
                    Enable: this.isIncludeResignedDriver() ? 1 : 0 ,
                };

                //console.log(repportParameters, reportNamespace);

                this.reports = {
                    report: reportNamespace,
                    parameters: repportParameters
                };

                let reportExport = { PDF: false, Word: false, CSV: false };
                this.navigate("cw-fleet-monitoring-tripanalysis-report", { reportSource: this.reports, reportName: this.i18n('Report_TripAnalysis')(), reportExport: reportExport });

            }
            else {
                this.validationModel.errors.showAllMessages();
            }
        }
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }

    setFormatDateTime(dateObj) {
        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

        return newDateTime;
    }
}


export default {
    viewModel: ScreenBase.createFactory(TripAnalysisSearchScreen),
    template: templateMarkup
};