﻿import ko from "knockout";
import templateMarkup from "text!./timeline.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import AssetInfo from "../../asset-info";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";

class PlaybackTimelineScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Timeline")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;

        // this.businessUnitIds = this.ensureNonObservable(params.businessUnitIds);
        // this.businessUnitName = this.ensureObservable(params.businessUnitName);
        // this.vehicleIds = this.ensureObservable(params.vehicleIds);
        // this.deliveryManIds = this.ensureObservable(params.deliveryManIds);
        //this.startDate = this.ensureObservable(params.startDate);
        //this.endDate = this.ensureObservable(params.endDate);
        this.selectedVehicles = this.ensureNonObservable(params.selectedVehicles);
        this.selectedDeliveryMen = this.ensureNonObservable(params.selectedDeliveryMen);
        this.displayType = this.ensureObservable(params.displayType);
        this.type = this.ensureObservable(params.type);
        this.duration = ko.observable();
        this.timeLine = ko.observableArray([]);
        this.isNoData = ko.observable(false);
        this.isLoading = ko.observable(false);
        this.avgToolTip = ko.observable();
        this.txtvehicleLicense = ko.observableArray(params.selectedVehicles);

        // Componse filter from playback selection.
        this.filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.businessUnitIds,
            vehicleIds: _.map(this.selectedVehicles, "id"),
            deliveryManIds: _.map(this.selectedDeliveryMen, "id"),
            startDate: this.ensureNonObservable(params.startDate),
            endDate: this.ensureNonObservable(params.endDate),
            driverRuleId : this.ensureNonObservable(params.drivingRuleId),
            displayMode: this.ensureNonObservable(params.displayMode)
        };

        // Subscribe event.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    onClickEvent(data) {
        var locationInfoStatus = AssetInfo.getLocationInfoStatus(data.locationInfo);
        var shipmentInfo = AssetInfo.getShipmentInfo(data.locationInfo, locationInfoStatus);
        var alertParam = {
            imageUrl: this.resolveUrl(data.imagePath),
            latitude: data.locationInfo.latitude,
            longitude: data.locationInfo.longitude,
            attributes: {
                TITLE: data.locationInfo.vehicleLicense,
                SHIPMENTINFO: [{
                    key: "alertdesc",
                    title: this.i18n('wg-shipmentlegend-alertdesc')(),
                    text: _.isNil(data.descriptionText) ? "-" : data.descriptionText
                }].concat(shipmentInfo)
            }
        };

        MapManager.getInstance().playbackDrawAlertChart(alertParam);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var myRegex = new RegExp("iPhone|iPad", "i");;
        var isdevice = myRegex.test(navigator.userAgent.toLowerCase());
        let pathImages = {
            picTotalFuel: "../../../../images/icon_nearest_assets_fuel@3x.png",
            picTotalWorkingTime: "../../../../images/icon_nearest_assets_engine@3x.png",
            picAverageSpeed: "../../../../images/icon_nearest_assets_speed@3x.png",
            picTotalMove: "../../../../images/icon_nearest_assets_movement_move@3x.png",
            picTotalStop: "../../../../images/icon_nearest_assets_movement_stop@3x.png",
            picTotalPark: "../../../../images/icon_nearest_assets_movement_park@3x.png",
            picTotalDistance: "../../../../images/icon_nearest_assets_mileage@3x.png",
            picTotalenginneon: "../../../../images/icon_nearest_assets_movement_idle@3x.png"
        }

        var dfd = $.Deferred();
        var d1 = this.webRequestFleetMonitoring.findPlaybackTimelines(this.filter);

        $.when(d1).done((result) => {
            this.duration(result.formatFromDate + " - " + result.formatToDate);
            _.forEach(result.items, (r) => {
                let result = this.modifyData(r);
                result.timelineData = {
                    graphs: result.graphs,
                    events: result.events,
                    displayTitle: result.displayTitle,
                    isIos : isdevice
                }

                if (result.graphs["length"] == 0) {
                    result.empty = true;
                    result.textDataNotFound = this.i18n("Common_DataNotFound")();
                    //result.isNoData = true;
                } else {
                    result.empty = false;
                    result.textDataNotFound = "";
                    //result.isNoData = false;
                }

                result.displayVehicleBrand = "(" + result.vehicleBrand + "-" + result.vehicleModel + ") - " +
                    this.i18n("FleetMonitoring_Playback_Fuel_Consumption")() +
                    " " + result.fuelInfos.formatFuelConsumption +
                    " " + this.i18n("FleetMonitoring_Playback_Fuel_Km/L")()

                result.picTotalFuel = pathImages.picTotalFuel;
                result.picTotalWorkingTime = pathImages.picTotalWorkingTime;
                result.picAverageSpeed = pathImages.picAverageSpeed;
                result.picTotalMove = pathImages.picTotalMove;
                result.picTotalStop = pathImages.picTotalStop;
                result.picTotalPark = pathImages.picTotalPark;
                result.picTotalDistance = pathImages.picTotalDistance;
                result.picTotalenginneon = pathImages.picTotalenginneon;

                result.formatDisplayFuel = this.i18n("FleetMonitoring_Playback_Fuel_Plan")() +
                    " : " + result.fuelInfos.formatPlanFuel +
                    " " + this.i18n("FleetMonitoring_Playback_Litre")() +
                    " (" + result.fuelInfos.formatFuelRate +
                    " " + this.i18n("FleetMonitoring_Playback_Fuel_Km/L")() + ")"

                result.formatDisplayLevel1 = "• 1-20 : " + result.speedInfos.formatLevel1 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel1 + "%)"
                result.formatDisplayLevel2 = "• 21-40 : " + result.speedInfos.formatLevel2 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel2 + "%)"
                result.formatDisplayLevel3 = "• 41-60 : " + result.speedInfos.formatLevel3 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel3 + "%)"
                result.formatDisplayLevel4 = "• 61-80 : " + result.speedInfos.formatLevel4 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel4 + "%)"
                result.formatDisplayLevel5 = "• 81-100 : " + result.speedInfos.formatLevel5 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel5 + "%)"
                result.formatDisplayLevel6 = "• 101-120 : " + result.speedInfos.formatLevel6 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel6 + "%)"
                result.formatDisplayLevel7 = "• >120 : " + result.speedInfos.formatLevel7 + " " + this.i18n("FleetMonitoring_Playback_Times")() + " (" + result.speedInfos.formatPercentageLevel7 + "%)"

                result.fuelInfos.formatTotalFuel += " " + this.i18n('FleetMonitoring_Playback_Litre')();
                // result.speedInfos.averageSpeed += " (" + this.i18n('FleetMonitoring_Playback_Km_Speed')() + ")";
                result.totalMove = " " + this.calculatMins(result.totalMove) ;
                result.totalStop = " " + this.calculatMins(result.totalStop) ;
                result.totalPark = " " + this.calculatMins(result.totalPark) ;
                result.totalParkEngineOn = " " + this.calculatMins(result.totalParkEngineOn) ;
                result.formatTotalDistance += " " + this.i18n('FleetMonitoring_Playback_Km')();
                result.formatTotalWorkingTime = result.workingTime ;

                result.HeardDuration = this.i18n('FleetMonitoring_Playback_Move_Duration_heard')();
                result.TotalFuel = this.i18n('FleetMonitoring_Playback_Total_Fuel')();
                result.TotalWorkingTime = this.i18n('FleetMonitoring_Playback_Total_WorkingTime')();
                result.TotalDistance = this.i18n('FleetMonitoring_Playback_Total_Distance')();
                result.MoveDuration = this.i18n('FleetMonitoring_Playback_Move_Duration')();
                result.StopDuration = this.i18n('FleetMonitoring_Playback_Stop_Duration')();
                result.ParkDuration = this.i18n('FleetMonitoring_Playback_Park_Duration')();
                result.EngineOnDuration = this.i18n('FleetMonitoring_Playback_Engine_On_Duration')();
                result.SpeedInfo = this.i18n('FleetMonitoring_Playback_Speed')() + " (" + this.i18n('FleetMonitoring_Playback_Km_Speed')() + ")";
                result.SpeedUnit = "(" + this.i18n('FleetMonitoring_Playback_Km_Speed')() + ")";
                result.avgToolTip = this.i18n('FleetMonitoring_Playback_Average_Speed')() + " " + result.speedInfos.averageSpeed;

                result.description = this.createFormatDisplayDescription(result.eventTimes);
                result.hasPermissionWorkingTime = WebConfig.userSession.hasPermission(Constants.Permission.PlaybackWorkingTime);



                this.timeLine.push(result);
            });
            //if(this.timeLine().length == 0) {
            //    formatDisplayLevel1
            //}
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }
    calculatMins(mins) {
        var calParkTime = "";
        var parkDuration = mins;
        var dayInMinutes = 60 * 24;
        var day = Math.floor(parkDuration / dayInMinutes);
        calParkTime += day <= 9 ? "0"+day : day;
        calParkTime += ":"
        var hr = Math.floor((parkDuration % dayInMinutes) / 60);
        calParkTime += hr <= 9 ? "0"+hr : hr;
        calParkTime += ":"
        var minutes = Math.floor((parkDuration % dayInMinutes) % 60);
        calParkTime += minutes <= 9 ? "0"+minutes : minutes;
        return calParkTime
    }
    createFormatDisplayDescription(result) {
        let description = result;
        var model = [];
        let formatDescription = "";
        for (let i = 0; i < description.length; i++) {
            formatDescription = "";
            formatDescription += description[i].description + " " + description[i].formatTimes + " " + this.i18n("FleetMonitoring_Playback_Times")();

            model.push({
                displayDescription: formatDescription,
                text: description[i].text,
                style: {
                    backgroundColor: description[i].backgroundColor,
                    fontColor: description[i].fontColor
                }
            });
        }

        return model;
        //return formatDescription;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clear();
        MapManager.getInstance().showAllTrackVehicle();
        MapManager.getInstance().closePlayback();
        if ($("#cw-fleet-monotoring-playback-info-widget")[0] != undefined)
            $("#cw-fleet-monotoring-playback-info-widget").data("kendoWindow").close();
    }


    /**
     * 
     * 
     * @param {any} data
     * @returns
     * @memberOf PlaybackTimelineScreen
     */
    modifyData(data) {
        data.displayTitle = data.vehicleId ? data.vehicleBrand + " - " + data.vehicleModel + " " + data.vehicleLicense : data.username + " (" + data.email + ")";

        data.displayType = this.displayType();
        data.type = this.type();

        _.forEach(data["movements"], (m) => {
            switch (m.movement) {
                case null:
                    m.color = "transparent";
                    break;
                case Enums.ModelData.MovementType.Move:
                    m.color = "#19dd72";
                    break;
                case Enums.ModelData.MovementType.Stop:
                    m.color = "#ed1c24";
                    break;
                case Enums.ModelData.MovementType.Park:
                    m.color = "#2e3192";
                    break;
                case Enums.ModelData.MovementType.ParkEngineOn:
                    m.color = "#eacd3b";
                    break;
            }
        });
        return data;
    }


    /**
     * 
     * @param {any} val
     * 
     * @memberOf PlaybackTimelineScreen
     */
    goToMap(val) {
        this.isLoading(true);
        var playbackTimelineFilter = this.filter;

        if (this.displayType().value === Enums.ModelData.PlaybackDisplayType.Moving) {
            playbackTimelineFilter.vehicleIds = val.vehicleId ? [val.vehicleId] : [];
            playbackTimelineFilter.deliveryManIds = val.deliveryManId ? [val.deliveryManId] : [];
        }
        var dfd = $.Deferred();
        this.webRequestFleetMonitoring.findPlaybackTrackLocations(playbackTimelineFilter).done((result) => {

            var items = result["items"];
            var alertConfig = result["alertIcon"];
            var vehicleIcons = _.isEmpty(result["vehicleIcons"]) ? null : result["vehicleIcons"];
            var mapObj = null;
            var dataWidget = items;

            switch (this.displayType().value) {
                case Enums.ModelData.PlaybackDisplayType.Moving:

                    var itemsID = val.vehicleId ? "V-" + val.vehicleId : "D-" + val.deliveryManId;

                    mapObj = {
                        id: itemsID,
                        playback: [],
                        alert: []
                    };

                    _.forEach(items, (data) => {

                        var obj = {
                            lat: data.latitude,
                            lon: data.longitude,
                            attributes: this.generateAttributes(data),
                        };


                        if (vehicleIcons) {
                            obj.pic = this.generateVehicleIconImageUrl(vehicleIcons[data.vehicleIconId], data.movement);
                        } else {
                            obj.pic = this.generateDeliveryIconImageUrl(data.movement);
                        }

                        mapObj.playback.push(obj);

                        if (data.alertConfigurationId != null) {
                            mapObj.alert.push(this.generateAlertItem(alertConfig, data, obj.attributes.SHIPMENTINFO, obj.attributes.TITLE));
                        }

                    });

                    break;

                case Enums.ModelData.PlaybackDisplayType.AllTrackingTrails:

                    var mode;
                    switch (this.type().value) {
                        case Enums.ModelData.PlaybackTrackingType.AllTracking:
                            mode = "ALL"
                            break;

                        case Enums.ModelData.PlaybackTrackingType.OnlyPark:
                            mode = "STOP"
                            break;
                    }

                    mapObj = {
                        mode: mode,
                        playback: [],
                        alert: []
                    };

                    //Collection of data id, which is vehicleId or deliveryManId.
                    var keyArray = [];
                    _.forEach(this.timeLine(), (t) => {
                        var id = t.vehicleId ? "V-" + t.vehicleId : "D-" + t.deliveryManId;
                        keyArray.push(id);
                    });

                    //Seperate data with id as object.
                    var keyObj = {};
                    _.forEach(items, (data, i) => {
                        var obj = {
                            lat: data.latitude,
                            lon: data.longitude,
                            rotation: data.direction,
                            attributes: this.generateAttributes(data),
                        };
                        if (data.movement) {

                            obj.movement = this.generateMovement(data.movement);
                        }



                        var datID = data.vehicleId ? "V-" + data.vehicleId : "D-" + data.deliveryManId;
                        if (!keyObj.hasOwnProperty(datID)) {
                            keyObj[datID] = [];
                        }

                        keyObj[datID].push(obj);

                        if (data.alertConfigurationId != null) {

                            mapObj.alert.push(this.generateAlertItem(alertConfig, data, obj.attributes.SHIPMENTINFO, obj.attributes.TITLE));
                        }
                    });


                    //TO match keyArray with keyObj and push to playback array.
                    _.forEach(keyArray, (key) => {
                        if (keyObj.hasOwnProperty(key)) {
                            mapObj.playback.push(keyObj[key]);
                        }
                    });

                    break;

                case Enums.ModelData.PlaybackDisplayType.MovingAndTrails:

                    var mapObj = {
                        mode: "ALL",
                        playback: [],
                        alert: []
                    };

                    var sortItems = _.sortBy(items, ['dateTime', 'id']);

                    _.forEach(sortItems, (data) => {

                        var itemsID = data.vehicleId ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId;
                        var obj = {
                            id: itemsID,
                            lat: data.latitude,
                            lon: data.longitude,
                            rotation: data.direction,
                            attributes: this.generateAttributes(data),
                        };
                        if (data.vehicleIconId) {
                            obj.pic = this.generateVehicleIconImageUrl(vehicleIcons[data.vehicleIconId], data.movement);
                        } else {
                            obj.pic = this.generateDeliveryIconImageUrl(data.movement);
                        }
                        if (data.movement) {
                            obj.movement = this.generateMovement(data.movement);
                        }

                        mapObj.playback.push(obj);

                        if (data.alertConfigurationId != null) {
                            mapObj.alert.push(this.generateAlertItem(alertConfig, data, obj.attributes.SHIPMENTINFO, obj.attributes.TITLE));
                        }

                    });
                    break;
            }

            _.forEach(dataWidget, (data, index) => {
                if (data.alertConfigurationId != null) {
                    dataWidget[index].alertURLInfo = alertConfig[data.alertConfigurationId]
                }
            });
            this.sendPlaybackData(mapObj, dataWidget, this.filter.vehicleIds);


            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

    }

    generateAlertItem(alertConfig, playbackItem, info, title) {
        var alertItem = {};

        alertItem.latitude = playbackItem.latitude;
        alertItem.longitude = playbackItem.longitude;


        if (alertConfig[playbackItem.alertConfigurationId] != undefined) {
            alertItem.attributes = _.clone(alertConfig[playbackItem.alertConfigurationId]);
        } else {
            alertItem.attributes = {
                imageUrl: "",
                description: ""
            }
        }
        alertItem.attributes.imageUrl = this.resolveUrl(alertItem.attributes.imageUrl);
        alertItem.attributes.TITLE = title;


        alertItem.attributes.SHIPMENTINFO = [{
            key: "alertdesc",
            title: this.i18n('wg-shipmentlegend-alertdesc')(),
            text: alertItem.attributes.description
        }].concat(_.clone(info));

        return alertItem;
    }

    /**
     * Create attributes object.
     * 
     * @param {any} data
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateAttributes(data) {
        var isVehicle = data.vehicleId ? true : false;
        var datID = isVehicle ? "V-" + data.vehicleId + "-PB" : "D-" + data.deliveryManId + "-PB";
        var title = isVehicle ? data.vehicleLicense : data.username;
        var businessUnitName = "";

        // Detect business unit name based on vehicle or delivery men.
        if (isVehicle) {
            businessUnitName = _.map(_.filter(this.selectedVehicles, {
                id: data.vehicleId
            }), "businessUnitName");
        } else {
            businessUnitName = _.map(_.filter(this.selectedDeliveryMen, {
                id: data.deliveryManId
            }), "businessUnitName");
        }

        // Compose map attribute object.
        var attr = {
            ID: datID,
            TITLE: title,
            BOX_ID: data.vehicleId ? data.boxSerialNo : data.email,
            DRIVER_NAME: data.driverName,
            DEPT: businessUnitName,
            DATE: data.formatDateTime,
            TIME: Utility.getOnlyTime(data.dateTime, "HH:mm"),
            PARK_TIME: data.parkDuration,
            PARK_IDLE_TIME: data.idleTime,
            LOCATION: data.location,
            RAW: data,
            STATUS: AssetInfo.getStatuses(data, WebConfig.userSession),
            IS_DELAY: data.isDelay
        };

        attr.SHIPMENTINFO = AssetInfo.getShipmentInfo(data, attr.STATUS);

        return attr;
    }

    /**
     * Convert movementType data.
     * 
     * @param {any} val
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateMovement(val) {

        switch (val) {
            case Enums.ModelData.MovementType.ParkEngineOn:
                val = "PARKENGINEON";
                break;
            case Enums.ModelData.MovementType.Move:
                val = "MOVE";
                break;
            case Enums.ModelData.MovementType.Park:
                val = "PARK";
                break;
            default:
                val = "STOP";
                break;
        }
        return val;
    }


    /**
     * 
     * 
     * @param {any} data
     * @param {any} compareValue
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateVehicleIconImageUrl(data, compareValue) {

        var result = null;

        _.forEach(data, (d) => {
            if (d.movementType === compareValue) {
                result = {
                    N: d.imageUrl + "/n.png",
                    E: d.imageUrl + "/e.png",
                    W: d.imageUrl + "/w.png",
                    S: d.imageUrl + "/s.png",
                    NE: d.imageUrl + "/ne.png",
                    NW: d.imageUrl + "/nw.png",
                    SE: d.imageUrl + "/se.png",
                    SW: d.imageUrl + "/sw.png"
                }
            }
        });

        return result;
    }


    /**
     * 
     * 
     * @param {any} movementType
     * @returns
     * 
     * @memberOf PlaybackTimelineScreen
     */
    generateDeliveryIconImageUrl(movementType) {
        var movement = "";
        switch (movementType) {
            case Enums.ModelData.MovementType.Move:
                movement = "move";
                break;
            case Enums.ModelData.MovementType.Stop:
                movement = "stop";
                break;
            case Enums.ModelData.MovementType.Park:
                movement = "park";
                break;
        }

        var imageUrl = Utility.resolveUrl("/", "~/images/icon-walk-" + movement + "-direction-");

        return {
            N: imageUrl + "n.png",
            E: imageUrl + "e.png",
            W: imageUrl + "w.png",
            S: imageUrl + "s.png",
            NE: imageUrl + "ne.png",
            NW: imageUrl + "nw.png",
            SE: imageUrl + "se.png",
            SW: imageUrl + "sw.png"
        }
    }


    /**
     * Send json data to map service.
     * 
     * @param {any} data
     * 
     * @memberOf PlaybackTimelineScreen
     */
    sendPlaybackData(data, dataWidget, vehicleIds) {
        switch (this.displayType().value) {
            case Enums.ModelData.PlaybackDisplayType.Moving:
                MapManager.getInstance().playbackAnimate(data);
                break;

            case Enums.ModelData.PlaybackDisplayType.AllTrackingTrails:
                MapManager.getInstance().playbackFootprint(data);
                break;
            case Enums.ModelData.PlaybackDisplayType.MovingAndTrails:
                MapManager.getInstance().playbackFully(data);
                break;
        }
        let vehicleLicense = this.txtvehicleLicense()

        this.widget('cw-fleet-monotoring-playback-info-widget', {
            dataWidget,
            vehicleIds,
            vehicleLicense
        }, {
            title: this.i18n("FleetMonitoring_Playback_Timeline_Widget")(),
            modal: false,
            resizable: false,
            target: "cw-fleet-monotoring-playback-info-widget",
            width: "450px",
            height: "85%",
            id: 'cw-fleet-monotoring-playback-info-widget'
        })

        MapManager.getInstance().hideAllTrackVehicle();

    }

    onMapComplete(data) {
        switch (data.command) {
            case "playback-animate-vehicles":
            case "playback-footprint-vehicles":
            case "playback-fully-vehicles":
            case "playback-draw-alert-chart":
                this.minimizeAll();
                this.isLoading(false);
                break;
            case "close-playback-widget":
                if ($("#cw-fleet-monotoring-playback-info-widget")[0] != undefined)
                    $("#cw-fleet-monotoring-playback-info-widget").data("kendoWindow").close();
                break;
        }
    }

}

export default {
    viewModel: ScreenBase.createFactory(PlaybackTimelineScreen),
    template: templateMarkup
};