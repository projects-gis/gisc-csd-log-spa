import ko from "knockout";
import templateMarkup from "text!./nearest-asset-result.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import {
    Enums,
    EntityAssociation
} from "../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../app/frameworks/core/utility";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestReportTemplate from "../../../../app/frameworks/data/apitrackingcore/webrequestReportTemplate";
import WebRequestFleetMonitoring from "../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import AssetUtility from "../asset-utility";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import FleetMonitoring from "../../../../app/frameworks/configuration/fleetMonitoring";

class NearestAssetScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Map_Result")());
        this.bladeSize = BladeSize.XLarge;
        this.columns = ko.observableArray([]);
        this.selectedItems = ko.observableArray([]);
        this.searchFilter = ko.observable({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: params.businessUnits ? this.ensureNonObservable(params.businessUnits) : null,
            dateTime: this.ensureNonObservable(params.dateTime),
            duration: this.ensureNonObservable(params.duration),
            radius: this.ensureNonObservable(params.radius),
            latitude: this.ensureNonObservable(params.latitude),
            longitude: this.ensureNonObservable(params.longitude)
        });
        this.lastRecentlyUsedSortingColumns = null;
        this.findNearestInfos = [];

        this._selectingRowHandler = (data) => {

            //this.drawResultInMap(this.searchFilter());
            //this.mapManager.trackNearestVehicles(this.id, this.findNearestInfos)

            let clickLocation = {
                    lat: data.latitude,
                    lon: data.longitude
                },
                symbol = {
                    url: this.resolveUrl("~/images/block_hilight.png"),
                    width: 35,
                    height: 35,
                    offset: {
                        x: 0,
                        y: 0
                    },
                    rotation: 0
                },
                zoom = 16,
                attributes = null

            MapManager.getInstance().drawOnePointZoom(this.id, clickLocation, symbol, zoom, attributes);

            this.minimizeAll();
        };

        this.fleetMonitoring = new FleetMonitoring();

    }

    /**
     * Get WebRequest specific for ReportTemplate module in Web API access.
     * @readonly
     */
    get webRequestReportTemplate() {
        return WebRequestReportTemplate.getInstance();
    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    get mapManager() {
        return MapManager.getInstance();
    }
    /**
     * Provide datagrid datasource with paginate.
     * 
     * @param {any} gridOption
     */
    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();

        this.isBusy(true);

        // Update paginate from grid.
        var filter = this.generateFilter(gridOption);

        // Call web service for nearest result.
        this.webRequestFleetMonitoring.listNearestAsset(filter)
            .done((response) => {

                dfd.resolve({
                    items: response.items,
                    totalRecords: response.totalRecords
                });

                this.publishMessage("track-nearest-minimize");
            })
            .fail((e) => {
                this.handleError(e);
                dfd.fail(e);
            })
            .always(() => {
                this.isBusy(false);
            });

        return dfd;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        // Draw Result 
        this.drawResultInMap(this.searchFilter());

        var d1 = this.webRequestReportTemplate.listReportTemplate({
            reportTemplateType: Enums.ModelData.ReportTemplateType.NearestAssets,
            includeAssociationNames: [
                EntityAssociation.ReportTemplate.ReportTemplateFields,
                EntityAssociation.ReportTemplateField.Feature
            ]
        });
        var d2 = this.webRequestFleetMonitoring.listNearestAsset(this.searchFilter());
       
        // Calling web service for dynamic column definitions.
        
        $.when(d1,d2).done((response,r2) => {
                var reportTemplateFields = response.items["0"].reportTemplateFields;
                var columns = AssetUtility.getColumnDefinitions(reportTemplateFields, false, 0, Enums.SortingColumnName.Radius);
                this.columns(columns);

                let data = _.map(r2.items, (item) => {
                    return this.fleetMonitoring._getTrackVehicleItem(item, r2.vehicleIcons, WebConfig.userSession,false);
                });

                this.mapManager.trackNearestVehicles(this.id, data);

                dfd.resolve();
            })
            .fail((e) => {
                dfd.reject(e);
            });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clear();
        MapManager.getInstance().showAllTrackVehicle();
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filter = this.searchFilter();
                            filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            filter.sortingColumns = this.lastRecentlyUsedSortingColumns;
                            this.webRequestFleetMonitoring.exportNearestAsset(filter)
                                .done((response) => {
                                    this.isBusy(false);
                                    ScreenHelper.downloadFile(response.fileUrl);
                                })
                                .fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
                            break;
                    }
                });
                break;
        }
    }

    /**
     * Prepare API filter with datatable parameters.
     * 
     * @param {any} gridOption DataGrid option for datasource request.
     * @returns {any}
     */
    generateFilter(gridOption) {
        // Clone filter because export option does not require paginate.
        var filter = _.cloneDeep(this.searchFilter());

        // Update standard filter with sorting, paginate parameters.
        if (!_.isEmpty(gridOption.sort)) {
            var sortDir = (gridOption.sort[0].dir === "asc") ?
                Enums.SortingDirection.Ascending :
                Enums.SortingDirection.Descending;

            // Required map back from grid to web service with enum id.
            var sortColumnName = "";
            var columnDefinition = _.find(this.columns(), {
                data: gridOption.sort[0].field
            });
            if (columnDefinition) {
                sortColumnName = columnDefinition.enumColumnName;
            }

            // Apply sorting from user then id.
            filter.sortingColumns = [{
                    column: sortColumnName,
                    direction: sortDir
                },
                {
                    column: Enums.SortingColumnName.Id,
                    direction: sortDir
                }
            ];
        } else {
            // Grid doesn't send any sorting information so using default.
            filter.sortingColumns = [{
                    column: Enums.SortingColumnName.Radius,
                    direction: Enums.SortingDirection.Ascending
                },
                {
                    column: Enums.SortingColumnName.Id,
                    direction: Enums.SortingDirection.Ascending
                }
            ];
        }

        // Store sotring filter for export function.
        this.lastRecentlyUsedSortingColumns = filter.sortingColumns;

        filter.displayStart = gridOption.skip;
        filter.displayLength = gridOption.pageSize;
        filter.includeCount = true;

        return filter;
    }

    //มีไว่้เพื่อกรณี clear ghaphic แล้ว ต้องการ zoom ทำให้ต้อง draw ใหม่
    drawResultInMap(info){
        if(info){
            let lon = info.longitude;
            let lat = info.latitude;
            let radius = info.radius;

            this.mapManager.hideAllTrackVehicle();
            //แสดงผลรัศมีการค้นหาในแผนที่ด้วย
            this.mapManager.drawPointBuffer(
                this.id, 
                { lat :lat  , lon :lon  }, 
                radius, 
                "kilometers", 
                WebConfig.mapSettings.fillColor, 
                WebConfig.mapSettings.borderColor, 
                WebConfig.mapSettings.fillOpacity, 
                WebConfig.mapSettings.borderOpacity);
            this.mapManager.zoomPoint(this.id, lat, lon, WebConfig.mapSettings.defaultZoom);
        }
    }


}

export default {
    viewModel: NearestAssetScreen.createFactory(NearestAssetScreen),
    template: templateMarkup
};