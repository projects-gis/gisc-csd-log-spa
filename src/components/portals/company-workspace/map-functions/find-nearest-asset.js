import ko from "knockout";
import templateMarkup from "text!./find-nearest-asset.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestSystemConfiguration from "../../../../app/frameworks/data/apicore/webRequestSystemConfiguration";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Constants,EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../app/frameworks/core/utility";
import { BusinessUnitFilter } from "../businessUnitFilter";
import AssetInfo from "../asset-info";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";

/**
 * Handle find nearest access from specific location.
 * @class MapFunctionsFindNearestScreen
 * @extends {ScreenBase}
 */
class MapFunctionsFindNearestScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Map_FindNearestAsset")());

        this.dateTime = ko.observable();
        this.duration = ko.observable();
        this.radius = ko.observable(); 

        this.latitude = this.ensureObservable(Utility.trimDecimal(params.lat, 6));
        this.longitude = this.ensureObservable(Utility.trimDecimal(params.lon, 6));

        this.businessUnit = ko.observable(null);
        this.businessUnitOptions = ko.observableArray([]);
        this.noneAccessibleBU = ko.observableArray([]);
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.businessUnitFilter = new BusinessUnitFilter();

        // Map
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        this.subscribeMessage("track-nearest-minimize",() => {
            this.minimizeAll(false);
        });
    }

    /**
     * Get MapManager instance.
     * @readonly
     */
    get mapManager() {
        return MapManager.getInstance();
    }

    /**
     * Setup change detection and validators.
     */
    setupExtend() {

        //Track Change
        this.dateTime.extend({
            trackChange: true
        });

        this.duration.extend({
            trackChange: true
        });

        this.radius.extend({
            trackChange: true
        });

        this.latitude.extend({
            trackChange: true
        });

        this.longitude.extend({
            trackChange: true
        });

        this.businessUnit.extend({
            trackChange: true
        });

        //Validation
        this.dateTime.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.duration.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.radius.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.latitude.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });
        
        this.longitude.extend({
            required: true,
            serverValidate: {
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            dateTime: this.dateTime,
            duration: this.duration,
            radius: this.radius,
            latitude: this.latitude,
            longitude: this.longitude
        });

    }

    /**
     * WebRequestBusinessUnit instance.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * WebRequestSystemConfiguration instance.
     * @readonly
     */
    get webRequestSystemConfiguration() {
        return WebRequestSystemConfiguration.getInstance();
    }

    /**
     * Handle when screen is loading.
     * @param {any} isFirstLoad
     * @returns
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var systemConfigurationFilter = {
            names: [
               Constants.SystemConfigurationName.DefaultRadiusFindNearest,
               Constants.SystemConfigurationName.DefaultTimeRangeFindNearest
            ]
        };
        var d1 = this.webRequestSystemConfiguration.listSystemConfiguration(systemConfigurationFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(this.businessUnitFilter);

        $.when(d1,d2).done((r1,r2) => {
            var bu = r2["items"];
            this.businessUnitOptions(bu);
            this.noneAccessibleBU(AssetInfo.getNoneAccessibleBU(bu));
            this.dateTime(r2["timestamp"]);
            var sysConfigdefultValue = r1["items"];

            _.forEach(sysConfigdefultValue, (obj)=> {
                switch (obj.name) {
                    case Constants.SystemConfigurationName.DefaultRadiusFindNearest:
                            this.radius(obj.value);
                        break;
                
                   case Constants.SystemConfigurationName.DefaultTimeRangeFindNearest:
                            this.duration(obj.value);
                        break;
                }
            });
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Handle when buiding action bar buttons.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actFind", this.i18n("Common_Find")()));
        actions.push(this.createActionCancel());
    }

    /**
     * Handle when blade action button clicked. 
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actFind") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // ย้าย ไปไว้หน้า Result 
            // var lat = this.latitude();
            // var lon = this.longitude();
            // var radius = this.radius();

            // this.mapManager.hideAllTrackVehicle();
            // //แสดงผลรัศมีการค้นหาในแผนที่ด้วย
            // this.mapManager.drawPointBuffer(
            //     this.id, 
            //     { lat : lat , lon : lon }, 
            //     radius, 
            //     "kilometers", 
            //     WebConfig.mapSettings.fillColor, 
            //     WebConfig.mapSettings.borderColor, 
            //     WebConfig.mapSettings.fillOpacity, 
            //     WebConfig.mapSettings.borderOpacity);
            // this.mapManager.zoomPoint(this.id, lat, lon, WebConfig.mapSettings.defaultZoom);


            // Default selection is single.
            var selectedBusinessUnitIds = [];
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.businessUnit());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    selectedBusinessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    selectedBusinessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.navigate("cw-map-functions-find-nearest-asset-result", {
                dateTime: this.dateTime(), 
                duration: this.duration(),
                radius: this.radius(),
                latitude: this.latitude(), 
                longitude: this.longitude(), 
                businessUnits: selectedBusinessUnitIds
            });
        }
    }

    /**
     * Handle when screen is unloading.
     */
    onUnload() {
        
    }

    /**
     * Hook on map complete
     * @param {any} data
     */
    onMapComplete(data) {
        //
    }
}

export default {
    viewModel: ScreenBase.createFactory(MapFunctionsFindNearestScreen),
    template: templateMarkup
};