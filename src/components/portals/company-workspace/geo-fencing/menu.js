﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class GeoFencingMenuScreen extends ScreenBase {

    /**
     * Creates an instance of GeoFencingMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("GeoFencing_Title")());

        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.Small;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomPOI))
        {
            this.items.push({
                text: this.i18n('Geofencing_CustomPoi')(),
                page: 'cw-geo-fencing-custom-poi'
            });
        }
        
        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomPOICategory))
        {
            this.items.push({
                text: this.i18n('Geofencing_CustomPoiCategories')(),
                page: 'cw-geo-fencing-custom-poi-category'
            });
        }
        
        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomArea))
        {
            this.items.push({
                text: this.i18n('Geofencing_CustomAreas')(),
                page: 'cw-geo-fencing-custom-area'
            });
        }
       
        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomAreaCategory))
        {
            this.items.push({
                text: this.i18n('Geofencing_CustomAreaCategories')(),
                page: 'cw-geo-fencing-custom-area-category'
            });
        }
        
        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomRoute))
        {
            this.items.push({
                text: this.i18n('Geofencing_CustomRoute')(),
                page: 'cw-geo-fencing-custom-route'
            });

        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomRouteCategory))
        {
            this.items.push({
                text: this.i18n('Geofencing_CustomRouteCategories')(),
                page: 'cw-geo-fencing-custom-route-category'
            });
        }
       

        if(WebConfig.userSession.hasPermission(Constants.Permission.CustomPOISuggestion)) {
            this.items.push({
                text: this.i18n('Geofencing_CustomPoi_Suggestion')(),
                page: 'cw-geo-fencing-custom-poi-suggestion'
            });
        }     

        if(WebConfig.userSession.hasPermission(Constants.Permission.Routes))
        {
            this.items.push({
                text: this.i18n('Geofencing_RouteFromHere')(),
                page: 'cw-geo-fencing-routes-from-here'
            });
        }
        //if (WebConfig.userSession.hasPermission(Constants.Permission.GeoFencing)) {
            
        //    // this.items.push({
        //    //     text: this.i18n('Working_Area_Title')(),
        //    //     page: 'cw-geo-working-area'
        //    // });
        //}
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(GeoFencingMenuScreen),
    template: templateMarkup
};