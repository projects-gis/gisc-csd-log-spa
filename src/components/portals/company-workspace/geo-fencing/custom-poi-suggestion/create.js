﻿import "jquery";
import ko from "knockout";
import _ from "lodash";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./create.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import UIConstant from "../../../../../app/frameworks/constant/uiConstant";
import { Constants, Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import WebRequestPOISuggestion from "../../../../../app/frameworks/data/apitrackingcore/webRequestPOISuggestion";
import WebRequestCountry from "../../../../../app/frameworks/data/apicore/webrequestCountry";
import WebRequestProvince from "../../../../../app/frameworks/data/apicore/webrequestProvince";
import WebRequestCity from "../../../../../app/frameworks/data/apicore/webrequestCity";
import WebRequestTown from "../../../../../app/frameworks/data/apicore/webrequestTown";
import WebRequestPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOICategory";
import WebRequestPOIIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";

class CustomPOISuggestionCreateScreen extends ScreenBase {

    constructor(params) {
        super(params);

        // This mode is parse from params, it can comes from both URL or NavigationService
        //this.mode = this.ensureNonObservable(params.mode);
        this.mode = Screen.SCREEN_MODE_CREATE;
        this.markpoi = this.ensureNonObservable(params.markpoi);
        this.customPoiSuggestionId = this.ensureNonObservable(params.markpoi.id);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("CustomPOI_CreateCustomPOI")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("CustomPOI_UpdateCustomPOI")());
        }
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);
        this.businessUnitDS = ko.observableArray([]);
        this.categoryOptions = ko.observableArray();
        this.iconOptions = ko.observableArray();
        this.iconSelectedItem = ko.observable();
        this.countryOptions = ko.observableArray();
        this.provinceOptions = ko.observableArray();
        this.cityOptions = ko.observableArray();
        this.townOptions = ko.observableArray();
        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.latitude = ko.observable();
        this.longitude = ko.observable();
        this.radius = ko.observable(500);
        this.category = ko.observable();
        this.country = ko.observable();
        this.province = ko.observable();
        this.city = ko.observable();
        this.town = ko.observable();
        this._responseIden = ko.observableArray();
        this._flagIden=false;
        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-geo-fencing-custom-poi-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });
        this.zoom = UIConstant.POISuggestion.ZoomLevel;

        //Subscribe Latitude
        this.latitude.subscribe((lat) => {
            this.idenPoi(this.latitude(),this.longitude(), this.extent());
        });

        //Subscribe Longitude
        this.longitude.subscribe((lon) => {
            this.idenPoi(this.latitude(),this.longitude(), this.extent());
        });

        this.subscribeMessage("cw-geo-fencing-custom-poi-suggestion-closed", () => {
            this.close(true);
        });

        this.subscribeMessage("cw-geo-fencing-custom-poi-suggestion-create-refresh-detail", (data) => {
            this.latitude(data.latitude);
            this.longitude(data.longitude);
            this.customPoiSuggestionId = data.Id;
        });

        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
        MapManager.getInstance().setFollowTracking(false);
    }

    goto(extras) {
        // TODO: Uncomment this block for allow real navigation in next phase.
        this.navigate(extras.id);
    }

    get webRequestPOISuggestion() {
        return WebRequestPOISuggestion.getInstance();
    }

    get webRequestCountry() {
        return WebRequestCountry.getInstance();
    }

    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }

    get webRequestCity() {
        return WebRequestCity.getInstance();
    }

    get webRequestTown() {
        return WebRequestTown.getInstance();
    }

    get webRequestPOICategory() {
        return WebRequestPOICategory.getInstance();
    }

    get webRequestPoiIcon() {
        return WebRequestPOIIcon.getInstance();
    }

    get webRequestMap() {
        return WebRequestMap.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        this.minimizeAll(false);

        var dfd = $.Deferred();
        var d0 = $.Deferred();
        var d1 = $.Deferred();

        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
                EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        };

        var filterPoiIcon = {
            includeAssociationNames: [EntityAssociation.PoiIcon.Image]
        };
        var t1 = this.webRequestPOICategory.listCustomPOICategorySummary({
            "companyId": WebConfig.userSession.currentCompanyId
        });
        var t2 = this.webRequestPoiIcon.listPoiIconSummary(filterPoiIcon);
        var t3 = this.webRequestBusinessUnit.listBusinessUnitSummary(filter);

        $.when(t1, t2, t3).done((responsePoiCategory, responsePoiIcon, responseBu) => {

            if (responseBu) {
                this.businessUnitDS(responseBu["items"]);
            }

            if (responsePoiCategory) {
                var poiCategorys = responsePoiCategory["items"];
                this.categoryOptions.replaceAll(poiCategorys);
            }

            if (responsePoiIcon) {
                var poiIcon = responsePoiIcon["items"];
                var cboDataPoi = [];
                if (poiIcon.length > 0) {
                    ko.utils.arrayForEach(poiIcon, function(value, index) {
                        let obj = {
                            image: value.image.fileUrl,
                            value: value.id,
                            text: value.name
                        };
                        cboDataPoi.push(obj);
                    });
                }
                this.iconOptions(cboDataPoi);
            }

            d1.resolve();
        }).fail((e) => {
            d1.reject(e);
            this.handleError(e);
        }).always(() => {
            
        });

        switch (this.mode) {
            case "create":
                this._originalAccessibleBusinessUnits = [];
                this.webRequestCountry.listCountry({ sortingColumns: DefaultSorting.Country }).done((response) => {
                    var countries = response["items"];
                    this.countryOptions(countries);
                    var defaultCountry = ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true);
                    //this.country(defaultCountry);

                    this.webRequestProvince.listProvince({
                        countryIds: [defaultCountry.id]
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions(provinces);
                        d0.resolve();
                    }).fail((e) => {
                        d0.reject(e);
                        this.handleError(e);
                    }).always(() => {

                    });
                }).fail((e) => {
                    d0.reject(e);
                    this.handleError(e);
                }).always(() => {
                    
                });
                if (this.markpoi != null) {
                    this.latitude(this.markpoi.latitude);
                    this.longitude(this.markpoi.longitude);
                    this.getDataViewOnMap(this.markpoi.id);
                }
                break;
            case "update":

                this.webRequestCustomPOI.getCustomPOI(this.customPoiSuggestionId).done((response) => {
                    if (response) {
                        this.name(response.name);
                        this.latitude(response.latitude);
                        this.longitude(response.longitude);
                        this.code(response.code);
                        this.description(response.description);
                        this.radius(response.radius);
                        this.accessibleBusinessUnits(response.accessibleBusinessUnits);
                        this._originalAccessibleBusinessUnits = _.cloneDeep(response.accessibleBusinessUnits);

                        if (response.categoryId) {
                            this.category(ScreenHelper.findOptionByProperty(this.categoryOptions, "id", response.categoryId));
                        }
                        if (response.iconId) {
                            this.iconSelectedItem(ScreenHelper.findOptionByProperty(this.iconOptions, "value", response.iconId));
                        }

                        var d1 = this.webRequestCountry.listCountry({});
                        var d2 = response.countryId ? this.webRequestProvince.listProvince({
                            countryIds: [response.countryId]
                        }) : null;
                        var d3 = response.provinceId ? this.webRequestCity.listCity({
                            provinceIds: [response.provinceId]
                        }) : null;
                        var d4 = response.cityId ? this.webRequestTown.listTown({
                            cityIds: [response.cityId]
                        }) : null;

                        $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {

                            var countries = r1["items"];
                            this.countryOptions(countries);

                            if (r1) {
                                var countries = r1["items"];
                                this.countryOptions(countries);

                                if (response.countryCode) {
                                    this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "code", response.countryCode));
                                } else {
                                    this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true));
                                }
                            }

                            if (r2) {
                                var provinces = r2["items"];
                                this.provinceOptions(provinces);

                                if (response.provinceCode) {
                                    this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", response.provinceCode));
                                }
                            }

                            if (r3) {
                                var cities = r3["items"];
                                this.cityOptions(cities);

                                if (response.cityCode) {
                                    this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "code", response.cityCode));
                                }
                            }

                            if (r4) {
                                var towns = r4["items"];
                                this.townOptions(towns);

                                if (response.townCode) {
                                    this.town(ScreenHelper.findOptionByProperty(this.townOptions, "", response.townCode));
                                }
                            }

                            d0.resolve();
                        }).fail((e) => {
                            d0.reject(e);
                            this.handleError(e);
                        }).always(() => {

                        });

                    }
                });
                break;
        }

        $.when(d0,d1).done(() => {
      
            this._changeCountrySubscribe = this.country.subscribe((country) => {

                if (country) {
                    this.webRequestProvince.listProvince({
                        countryIds: [country.id]
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions.replaceAll(provinces);

                        setTimeout(() => { 
                            if (this._responseIden) {
                                this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", this._responseIden.adminLevel1Code));

                            }
                        }, 500);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.provinceOptions.replaceAll([]);
                }

                this.publishMessage("cw-geo-fencing-custom-poi-manage-country-changed", country);
            });

            this._changeProvinceSubscribe = this.province.subscribe((province) => {
                if (province) {
                    this.city(null);
                    this.webRequestCity.listCity({
                        provinceIds: [province.id]
                    }).done((response) => {
                        var cities = response["items"];
                        this.cityOptions.replaceAll(cities);
                        setTimeout(() => { 
                            if (this._responseIden) {

                                this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "code", this._responseIden.adminLevel1Code + this._responseIden.adminLevel2Code));

                            }
                        }, 500);

                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.cityOptions.replaceAll([]);
                }
            });

            this._changeCitySubscribe = this.city.subscribe((city) => {

                if (city) {
                    this.town(null);
                    this.isBusy(true);
                    this.webRequestTown.listTown({
                        cityIds: [city.id]
                    }).done((response) => {
                        var towns = response["items"];
                        this.townOptions.replaceAll(towns);

                        setTimeout(() => { 

                            if (this._responseIden) {
                                this.town(ScreenHelper.findOptionByProperty(this.townOptions, "code", this._responseIden.adminLevel1Code + this._responseIden.adminLevel2Code + this._responseIden.adminLevel3Code));
                       
                            }
                        
                        }, 500);
                       

                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                } else {
                    this.townOptions.replaceAll([]);
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        }).always(() => {
            
        });
      
        
        return dfd;
       
    }

    getDataViewOnMap(id) {
        this.isBusy(true);        
        let suggestionId = id;
        this.webRequestPOISuggestion.viewOnMapData(suggestionId).done((res) => {         
            this.drawPOISuggestion({ point: res });
        }).fail((e) => {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);   
        });        
    }

    drawPOISuggestion(params) {
        let points = [];
        let imgUrlSelected = this.resolveUrl("~/images/poi_sugges_selected.png");
        let imgUrl = this.resolveUrl("~/images/poi_sugges_normal.png");
        let mainPoint = {
            location: {
                lon: params.point.longitude,
                lat: params.point.latitude
            },
            symbol: {
                url: imgUrlSelected,
                width: '36',
                height: '46',
                offset: {
                    x: 0,
                    y: 10
                }
            },
            attributes: params.point
        };
        

        for (let index in params.point.near) {
            let data = params.point.near[index];
            points.push({
                location: {
                    lon: data.longitude,
                    lat: data.latitude
                },
                symbol: {
                    url: imgUrl,
                    width: '18',
                    height: '23',
                    offset: {
                        x: 0,
                        y: 5
                    }
                },
                attributes: data
            })
        }

        MapManager.getInstance().drawCustomPOISuggestion(this.id, points, mainPoint, UIConstant.POISuggestion.BufferRadius, UIConstant.POISuggestion.BufferRadiusUnit, this.zoom);
        //MapManager.getInstance().zoomPoint(this.id, params.point.latitude, params.point.longitude, this.zoom);        
    }

    onUnload() {       
        MapManager.getInstance().clear();
    }

    enableClickMap() {

        // Call clear map command.
        //MapManager.getInstance().clear(this.id);

        // Call click command.
        //MapManager.getInstance().enableClickMap(this.customPoiSuggestionId);
        this.minimizeAll();
    }

    onMapComplete(response) {
        //console.log("custom-poi-subscribe",response.command);
        // if (response.command == "enable-click-map") {
        //     this.drawPoint(response.result.location.lat, response.result.location.lon);
        //     this.latitude(response.result.location.lat.toFixed(8));
        //     this.longitude(response.result.location.lon.toFixed(8));
    
        //     //this.idenPoi();

        // }

    }

    // drawPoint(locationLat, locationLon) {
    //     let clickLocation = {
    //         lat: locationLat,
    //         lon: locationLon
    //     },

    //         symbol = {
    //             url: this.resolveUrl("~/images/pin_destination.png"),
    //             width: '35',
    //             height: '50',
    //             offset: {
    //                 x: 0,
    //                 y: 23.5
    //             },
    //             rotation: 0
    //         },
    //         zoom = 17,
    //         attributes = null

    //     //Call clear command.
    //     MapManager.getInstance().drawOnePointZoom(this.customPoiSuggestionId, clickLocation, symbol, zoom, attributes);
    // }

    idenPoi(lat,lon,extent) {

        if (lat && lon && !_.endsWith(lat,'.') && !_.endsWith(lon,'.')) {
            this._flagIden=true;
            var param = {
                lat: lat,
                lon: lon,
                extent: extent
            };
            //this.drawPoint(lat, lon)
            this.isBusy(true);
            this.webRequestMap.identify(param).done((response) => {
                this._responseIden = response;
                var countryCode = this._responseIden.countryCode != null? this._responseIden.countryCode: '';
                this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "code", countryCode));  
                this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", response.adminLevel1Code));
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });

        }
    }

    findCountryCode(code){
        var countryCode;
        switch(code){
        
            case "TH":
                countryCode = Constants.CountryCode.TH;
                break;
            case "SG":
                countryCode = Constants.CountryCode.SG;
                break;
            case "MY":
                countryCode = Constants.CountryCode.MY;
                break;
            case "BN":
                countryCode = Constants.CountryCode.BN;
                break;
            case "VN":
                countryCode = Constants.CountryCode.VN;
                break;
            case "PH":
                countryCode = Constants.CountryCode.PH;
                break;
            case "ID":
                countryCode = Constants.CountryCode.ID;
                break;
            case "MM":
                countryCode = Constants.CountryCode.MM;
                break;
            case "LA":
                countryCode = Constants.CountryCode.LA;
                break;
            case "KH":
                countryCode = Constants.CountryCode.KH;
                break;
        }
     
        return countryCode;
    }
    setupExtend() {

        this.name.extend({
            trackChange: true
        });
        this.latitude.extend({
            trackChange: true,
            rateLimit: 500
        });
        this.longitude.extend({
            trackChange: true,
            rateLimit: 500
        });
        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });

        this.description.extend({
            trackChange: true
        });

        this.code.extend({
            trackChange: true
        });

        this.category.extend({
            trackChange: true
        });

        this.iconSelectedItem.extend({
            trackChange: true
        });

        this.accessibleBusinessUnits.extend({
            trackChange: true
        });

        this.radius.extend({
            trackChange: true
        });

        this.country.extend({
            trackChange: true
        });

        this.province.extend({
            trackChange: true
        });

        this.city.extend({
            trackChange: true
        });

        this.town.extend({
            trackChange: true
        });

     

        // Validation
        this.latitude.extend({
            required: true
        });
        this.longitude.extend({
            required: true
        });
        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });
        this.radius.extend({
            required: true
        });
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.country.extend({
            required: true
        });
        this.province.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            lat: this.latitude,
            lon: this.longitude,
            accessibleBusinessUnits: this.accessibleBusinessUnits,
            country: this.country,
            province: this.province,
            radius: this.radius,
            code: this.code
        });
    }
    /**
     * TODO:Delete selected item
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

    }


    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    generateModel() {
        // concat adminlevelcode by client
        var cityCode = "",
            townCode = "";
        if (this._responseIden.countryCode == "TH") {
            // case NOSTRA's data code 2 digit
            if (this._responseIden.adminLevel2Code.length == 2) {
                cityCode = this._responseIden.adminLevel1Code + this._responseIden.adminLevel2Code;
                townCode = this._responseIden.adminLevel1Code + this._responseIden.adminLevel2Code + this._responseIden.adminLevel3Code;
            } else {
                cityCode = this._responseIden.adminLevel2Code;
                townCode = this._responseIden.adminLevel3Code;
            }
        } else {
            cityCode = this._responseIden.adminLevel2Code;
            townCode = this._responseIden.adminLevel3Code;
        }

        let model = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            Name: this.name(),
            Code: this.code(),
            Description: this.description(),
            Latitude: this.latitude(),
            Longitude: this.longitude(),
            Radius: this.radius() ? this.radius() : null,
            ProvinceCode: this._responseIden.adminLevel1Code,
            CityCode: cityCode,
            TownCode: townCode,
            CountryCode: this._responseIden.countryCode,
            CategoryId: this.category() ? this.category().id : null,
            IconId: this.iconSelectedItem() ? this.iconSelectedItem().value : null,
            Id: this.customPoiSuggestionId
        };
        model.accessibleBusinessUnits = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleBusinessUnits,
            this.accessibleBusinessUnits()
        );

        return model;
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            var dfdDelay = $.Deferred();
            setTimeout(function(){
                dfdDelay.resolve();
            },500); 

            $.when(dfdDelay).done(() => {
            
                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }
      
                var model = this.generateModel();
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.isBusy(true);
                        this.webRequestPOISuggestion.createCustomPOI(model).done(() => {
                            this.publishMessage("cw-geo-fencing-custom-poi-suggestion-changed", this.customPoiSuggestionId);
                    //     //MapManager.getInstance().openCustomPOILayer();
                            this.close(true);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        break;
                    // case Screen.SCREEN_MODE_UPDATE:
                    //     this.isBusy(true);
                    //     this.webRequestCustomPOI.updateCustomPOI(model).done(() => {
                    //         this.publishMessage("cw-geo-fencing-custom-poi-changed", this.customPoiSuggestionId);
                    //         //MapManager.getInstance().openCustomPOILayer();
                    //         this.close(true);
                    //     }).fail((e) => {
                    //         this.handleError(e);
                    //     }).always(() => {
                    //         this.isBusy(false);
                    //     });
                    //     break;
                }
                let location = {
                    lat: this.latitude(),
                    lon: this.longitude()
                },
                    radius = this.radius(),
                    unit = 'meters',
                    fillColor = '#9ECB89',
                    borderColor = '#9ECB89',
                    fillOpacity = 0.8,
                    borderOpacity = 1;
                // Call command.
                //MapManager.getInstance().drawPointBuffer(this.id, location, radius, unit, fillColor, borderColor, fillOpacity, borderOpacity);

            });//

        } else if (sender.id == "actCancel") {
            // Call clear map command.
            //MapManager.getInstance().clear(this.id);

        }
    }

    /**
     * Choose Accessible Business Unit
     */
    addAccessibleBusinessUnit() {
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.businessUnitId;
        });
        this.navigate("cw-geo-fencing-custom-poi-suggestion-create-accessible-business-unit-select", {
            mode: this.mode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }
}

export default {
viewModel: ScreenBase.createFactory(CustomPOISuggestionCreateScreen),
    template: templateMarkup
};