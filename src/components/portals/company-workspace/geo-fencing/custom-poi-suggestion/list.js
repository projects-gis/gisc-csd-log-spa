﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../app/frameworks/core/utility";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import UIConstant from "../../../../../app/frameworks/constant/uiConstant";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import WebRequestPOISuggestion from "../../../../../app/frameworks/data/apitrackingcore/webRequestPOISuggestion";

class CustomPOISuggestionListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Geofencing_CustomPoi_Suggestion")());
        this.bladeSize = BladeSize.XLarge_A1;
        this.bladeIsMaximized = true;
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));    

        this.apiDataSource = ko.observableArray([]);
        this.zoom = UIConstant.POISuggestion.ZoomLevel;
        this.showColCreate = ko.observable(WebConfig.userSession.hasPermission(Constants.Permission.CustomPOISuggestion) > 0);

        this.viewOnMap = (data) => {  
            this.publishMessage("cw-geo-fencing-custom-poi-suggestion-closed");
            this.getDataViewOnMap({ id: data.id, isPOI: !data.isCreate }, true);
        };

        this.openCreatePOISuggestion = (data) => {   
            if(data.isCreate) {
                this.navigate("cw-geo-fencing-custom-poi-suggestion-create", { markpoi: data });
                //this.getDataViewOnMap({ id: data.id }, false);
            }    
        };

        this.clickDeletePOISuggestion = (data) => {
            if(data.isDelete) {
                this.showMessageBox(null, this.i18n("M209")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestPOISuggestion.deletePOISuggestion(data.id).done(()=> {                                
                                this.refreshCustomPOISuggestionList();
                            }).fail((e)=> {
                                this.handleError(e);
                            }).always(()=> {
                                this.isBusy(false);
                            });                   
                            break;
                        case BladeDialog.BUTTON_NO:
                            break;
                        default:
                            break;
                    }
                });  
            }     
        }

        this.subscribeMessage("cw-geo-fencing-custom-poi-suggestion-changed", (selectedBusinessUnits) => {
            MapManager.getInstance().openCustomPOILayer();
            this.restore();
            this.refreshCustomPOISuggestionList();
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(isFirstLoad) {
            return
        }
    }

    refreshCustomPOISuggestionList() {
        this.dispatchEvent("dgPOISuggestionLst", "refresh");
    }

    onDatasourceRequestRead(gridOption) {
        this.isBusy(true);
        var dfd = $.Deferred();    
        var filter = Object.assign({}, gridOption);
        this.webRequestPOISuggestion.summaryList(filter).done((res) => {
            
            let data = res.items.map((item, index)=> {
                item.isCreate = item.customPoiId == null;
                item.isDelete = item.customPoiId == null;
                return item;
            })

            dfd.resolve({
                items: data,
                totalRecords: res.totalRecords
            });
        }).fail((e)=> {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);
        });

        //this.drawPOISuggestion({ point: mockLst });

        return dfd;
    }

    getDataViewOnMap(params, flg) {
        this.isBusy(true);        
        let suggestionId = params.id;
        this.webRequestPOISuggestion.viewOnMapData(suggestionId).done((res) => {
            
            if(flg) {
                this.minimizeAll(flg);  
            }
            
            this.drawPOISuggestion({ point: res, isPOI: params.isPOI });
        }).fail((e) => {
            this.handleError(e);
        }).always(()=> {
            this.isBusy(false);   
        });        
    }

    drawPOISuggestion(params) {
        let points = [];
        let imgUrlSelected = this.resolveUrl("~/images/poi_sugges_selected.png");
        let imgUrl = this.resolveUrl("~/images/poi_sugges_normal.png");
        let mainPoint = {
            location: {
                lon: params.point.longitude,
                lat: params.point.latitude
            },
            symbol: {
                url: imgUrlSelected,
                width: '36',
                height: '46',
                offset: {
                    x: 0,
                    y: 10
                }
            },
            attributes: params.point,
            isPOI: params.isPOI
        };
        

        for (let index in params.point.near) {
            let data = params.point.near[index];
            points.push({
                location: {
                    lon: data.longitude,
                    lat: data.latitude
                },
                symbol: {
                    url: imgUrl,
                    width: '18',
                    height: '23',
                    offset: {
                        x: 0,
                        y: 5
                    }
                },
                attributes: data
            })
        }

        MapManager.getInstance().drawCustomPOISuggestion(this.id, points, mainPoint, UIConstant.POISuggestion.BufferRadius, UIConstant.POISuggestion.BufferRadiusUnit, this.zoom);
        //MapManager.getInstance().zoomPoint(this.id, params.point.latitude, params.point.longitude, this.zoom);        
    }

    onMapComplete(response) {
        switch (response.command) {
            case "suggestion-poi-complete":
                MapManager.getInstance().openCustomPOILayer();
                break;
            case "poi-suggestion":
                this.navigate("cw-geo-fencing-custom-poi-suggestion-create", {
                    markpoi: response.result.attributes
                });
                break;
            case "re-draw-poi-suggestion":
                this.getDataViewOnMap({ id: response.result.id }, false);
                this.publishMessage("cw-geo-fencing-custom-poi-suggestion-create-refresh-detail", response.result);
                break;
            default:
                break;
        }
    }

    setupExtend() {}
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clear();
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actOK") {

        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filter = {};
                            this.webRequestPOISuggestion.exportPOISuggestion(filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }

    get webRequestPOISuggestion() {
        return WebRequestPOISuggestion.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomPOISuggestionListScreen),
    template: templateMarkup
};