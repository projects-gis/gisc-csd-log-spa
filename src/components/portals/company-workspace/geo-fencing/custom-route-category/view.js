﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./view.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestRouteCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRouteCategory";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class ViewRouteCategoriesScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("ViewRouteCategories_Title")());

        this.routeId = this.ensureNonObservable(params.id, -1);
        this.name = ko.observable();
        this.description = ko.observable();
        this.code = ko.observable();
        this.routeColor = ko.observable();

        this.subscribeMessage("cw-geo-fencing-custom-route-category-changed", (id) => {
            this.webRequestRouteCategory.getCustomRouteCategory(id).done((response) => {
                var name = response["name"];
                var description = response["description"];
                var code = response["code"];
                var routeColor = response["color"];
                this.name(name);
                this.description(description);
                this.code(code);
                this.routeColor(routeColor);
            })
        });
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestRouteCategory() {
            return WebRequestRouteCategory.getInstance();
     }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        this.webRequestRouteCategory.getCustomRouteCategory(this.routeId).done((response) => {
            console.log(response);
            var name = response["name"];
            var description = response["description"];
            var code = response["code"];
            var routeColor = response["color"];
            this.name(name);
            this.description(description);
            this.code(code);
            this.routeColor(routeColor);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomRouteCategory)){
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteCustomRouteCategory)){
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
     }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
            super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("cw-geo-fencing-custom-route-category-manage", {
                    mode: "update",
                    id: this.routeId
                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M050")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestRouteCategory.deleteCustomRouteCategory(this.routeId).done(() => {
                                this.publishMessage("cw-geo-fencing-custom-route-category-delete");
                                this.close(true);
                            }).fail((e) => {
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }

}

export default {
    viewModel: ScreenBase.createFactory(ViewRouteCategoriesScreen),
    template: templateMarkup
};