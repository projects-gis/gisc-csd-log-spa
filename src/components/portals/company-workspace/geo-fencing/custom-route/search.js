﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";

/**
 
 * @class CustomRouteListFilterScreen
 * @extends {ScreenBase}
 */
class CustomRouteListFilterScreen extends ScreenBase {

    /**
     * Creates an instance of CustomRouteListFilterScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_Filters_Title")());
        this.dateFormat = ko.observable("yyyy-MM-dd");
        this.createDateFrom = ko.observable();
        this.createDateTo = ko.observable();
        this.createBy = ko.observable();
        this.filterState = Utility.extractParam(params.filterState, null);

        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;
    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var r1 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.PoiType]
        });

        $.when(r1).done((response) => {
            let tmpPoiType = _.filter(response["items"], (x) => {
                if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                    return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                }
                return x;
            });
            this.poiTypeOptions(tmpPoiType);

            dfd.resolve();
        }).fail((ex) => {
            dfd.reject(e);
            this.handleError(e);
        });

        return dfd;
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            this.filterState.FromCreateDate = this.createDateFrom() ? this.createDateFrom() : null;
            this.filterState.ToCreateDate = this.createDateTo() ? this.createDateTo() : null;
            this.filterState.CreateBy = this.createBy() ? this.createBy() : "";
            this.filterState.poiType = this.poiType() ? this.poiType().value : null;
            this.publishMessage("cw-geo-fencing-custom-route-filter-changed", this.filterState);
            //this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

        if (sender.id === "cmdClear") {
            this.createDateFrom("");
            this.createDateTo("");
            this.createBy("");
            this.poiType(null);
        }
    }


}

export default {
    viewModel: ScreenBase.createFactory(CustomRouteListFilterScreen),
    template: templateMarkup
};