﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./list.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestCustomRoute from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRoute";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../app/frameworks/core/utility";
import ScreenHelper from "../../../screenhelper";

class CustomRouteListScreen extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_CustomRoutes")());
        this.bladeSize = BladeSize.XLarge;
        this.customRoutes = ko.observableArray();
        this.filterText = ko.observable();
        this.selectedCustomRoutes = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([
            [1, "asc"]
        ]);
        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (customRoute) => {
            if (customRoute) {
                return this.navigate("cw-geo-fencing-custom-route-view", {
                    customRouteId: customRoute.id
                });
            }
            return false;
        };
        this.filterState = ko.observable({
            CompanyId: WebConfig.userSession.currentCompanyId,
            FromCreateDate: null,
            ToCreateDate: null,
            CreateBy: "",
            poiType: null
        });

        // Observe change about filter
        this.subscribeMessage("cw-geo-fencing-custom-route-filter-changed", (newFilter) => {

            this.filterState(newFilter);

            this.webRequestRoute.listCustomRouteSummary(this.filterState()).done((data) => {

                var customRoute = data["items"];
                var viewonMap = {
                    viewonMap: this.resolveUrl("~/images/ic-search.svg")
                };
                if (customRoute.length > 0) {
                    ko.utils.arrayForEach(customRoute, function(items, index) {
                        items = $.extend(items, viewonMap);
                    });

                }
                this.customRoutes.replaceAll(customRoute);

            });
        });
        // onClick of columnId: 'viewOnMapColumn'
        this.viewMapClick = (data) => {
            
            var self = this;
            this.webRequestRoute.getCustomRoute(data.id).done((response) => {

                if (response) {

                    var param = {
                        routeMode: response.routeOption.routeMode,
                        routeOption: response.routeOption.routeOption,
                        lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L",
                        stops: []
                    };

                    if (response.viaPoints.length >= 2) {
                        ko.utils.arrayForEach(response.viaPoints, function(items, index) {
                            param.stops.push({
                                name: items.name,
                                lat: items.latitude,
                                lon: items.longitude
                            });
                        });

                        this.webRequestMap.solveRoute(param).done((responseSolveRouteString) => {
                            //console.log("boy debug :: responseSolveRoute",responseSolveRouteString);
                            var responseSolveRoute = JSON.parse(responseSolveRouteString);
                            if (responseSolveRoute != null) {
                                if (responseSolveRoute.results != null) {
                                    var pics = [];
                                    ko.utils.arrayForEach(response.viaPoints, function(items, index) {

                                        let urlPic = "~/images/";
                                        if(index===(response.viaPoints.length-1)){
                                            urlPic+="pin_destination.png";
                                
                                        }else{
                                            urlPic+="pin_poi_"+(index+1).toString()+".png";
                                        } 

                                        pics.push(self.resolveUrl(urlPic));
                                    });
                                    self.drawRoute(param.stops, pics, responseSolveRoute.results.route, response.color, undefined, undefined, data);

                                }
                            }
                        this.minimizeAll();
                        }).fail((e) => {

                            var errorObj = e.responseJSON;
                            console.log("errorObj", errorObj);
                        });
                    }
                }

            });
       
        };

        // Observe change about customroute in this Journey
        this.subscribeMessage("cw-geo-fencing-custom-route-changed", (customRouteId) => {
            var self = this;
            this.isBusy(true);
            this.webRequestRoute.listCustomRouteSummary(this.filterState()).done((data) => {
                var customRoute = data["items"];
                if (customRoute.length > 0) {
                    ko.utils.arrayForEach(customRoute, function(items, index) {
                        items = $.extend(items, {
                            viewonMap: self.resolveUrl("~/images/ic-search.svg")
                        });
                    });

                }
                this.customRoutes.replaceAll(customRoute);
                if (customRouteId != null) {
                    this.recentChangedRowIds.replaceAll([customRouteId]);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("cw-geo-fencing-custom-route-deleted", (customRouteId) => {
            this.isBusy(true);
            this.webRequestRoute.listCustomRouteSummary(this.filterState()).done((data) => {

                var customRoute = data["items"];
                var viewonMap = {
                    viewonMap: this.resolveUrl("~/images/ic-search.svg")
                };
                if (customRoute.length > 0) {
                    ko.utils.arrayForEach(customRoute, function(items, index) {
                        items = $.extend(items, viewonMap);
                    });

                }
                this.customRoutes.replaceAll(customRoute);
                if (customRouteId != null) {
                    this.recentChangedRowIds.replaceAll([customRouteId]);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        MapManager.getInstance().setFollowTracking(false);
    }

    listCustomRouteSummary(customRouteId) {

            // Refresh entire datasource
            this.isBusy(true);
            this.webRequestRoute.listCustomRouteSummary(this.filterState()).done((data) => {

                var customRoute = data["items"];
                if (customRoute.length > 0) {
                    ko.utils.arrayForEach(customRoute, function(items, index) {
                        items = $.extend(items, {
                            viewonMap: this.resolveUrl("~/images/ic-search.svg")
                        });
                    });

                }
                this.customRoutes.replaceAll(customRoute);
                if (customRouteId != null) {
                    this.recentChangedRowIds.replaceAll([customRouteId]);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });

     }
    /**
      Draw Route
     */
    drawRoute(stops, pics, path, color = "#7000db", width = 5, opacity = 1, attributes = null) {

            MapManager.getInstance().drawOneRoute(this.id, stops, pics, path, color, width, opacity, attributes);
    }
    getBaseLocation(){

        var url = window.location.href.split("?");
        if (url.length > 1)
        { url.pop(); }

        url = url[0].split("/");

        url.pop();
        var baseLocation = url.join("/");
        return baseLocation;
    }
    /**
     * Get WebRequest specific for customroute module in Web API access.
     * @readonly
     */
    get webRequestRoute() {
            return WebRequestCustomRoute.getInstance();
    }
    /**
     * Get WebRequest specific for map module in Web API access.
     * @readonly
     */
    get webRequestMap() {
            return WebRequestMap.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        var dfd = $.Deferred();

        this.webRequestRoute.listCustomRouteSummary(this.filterState()).done((response) => {

            var customRoute = response["items"];
            var viewonMap = {
                viewonMap: this.resolveUrl("~/images/ic-search.svg")
            };
            if (customRoute.length > 0) {
                ko.utils.arrayForEach(customRoute, function(items, index) {
                    items = $.extend(items, viewonMap);
                });

            }
            this.customRoutes(customRoute);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    onUnload() {

            MapManager.getInstance().clear(this.id);
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedCustomRoutes(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateCustomRoute)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("CustomRoutes_Create")(), "svg-cmd-add"));
        }

        commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdExport", this.i18n("CustomRoutes_Export")(), "svg-cmd-export"));
    }

    /**
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

        switch (sender.id) {

            case "cmdCreate":
                this.navigate("cw-geo-fencing-custom-route-manage", {
                    mode: "create"
                });
                this.selectedCustomRoutes(null);
                break;
            case "cmdFilter":
                this.navigate("cw-geo-fencing-custom-route-search", {
                    filterState: this.filterState()
                });
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.filterState().templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            this.webRequestRoute.exportCustomRoute(this.filterState()).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomRouteListScreen),
    template: templateMarkup
};