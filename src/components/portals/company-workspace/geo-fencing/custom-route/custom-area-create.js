﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./custom-area-create.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import WebRequestCustomRoute from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomRoute";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";

class CustomAreaCreateScreen extends ScreenBase {

    /**
     * Creates an instance of CustomAreaCreateScreen.
     * 
     * @param {any} params
     */
    constructor(params) {

        super(params);

        this.bladeTitle(this.i18n("CustomRoutes_CreateCustomArea")());
        this.mode = this.ensureNonObservable(params.mode);
        this.typeOptions = ko.observableArray();
        this.name = ko.observable();
        this.areaType = ko.observable();
        this.areaDescription = ko.observable();
        this.areaBuffer = ko.observable(50);
        this.generalize = ko.observable(50);
        this.path = this.ensureNonObservable(params.path);
        this.color = this.ensureNonObservable(params.color);
        this.isSave = ko.observable(false);
        this.size = ko.observable();
        this.totalPoints = ko.observable();
        this.bufferPoints = new Array();

        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea) || WebConfig.userSession.userType == Enums.UserType.BackOffice ? true : false;
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;

        // Register event subscribe in constructor.
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
    }

    onMapComplete(response) {
            
            if (response.result) {
                var self = this;
                if (response.command == "draw-route-buffer") {
                    this.size(response.result.area);
                    this.totalPoints(response.result.count);
                    this.bufferPoints = response.result.geometry.rings;
                    if (this.isSave() === true) {

                        var model = this.generateModel(response);
                        
                        this.webRequestRoute.createCustomAreaFromRoutes(model).done((response) => {
                            this.isBusy(false);
                            this.publishMessage("cw-geo-fencing-custom-area-by-route-changed");
                            this.close(true);

                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        this.isSave(false);
                    }
                }
            }
    }
    /**
      * Get WebRequest specific for customroute module in Web API access.
      * @readonly
      */
    get webRequestRoute() {
            return WebRequestCustomRoute.getInstance();
    }

    /**
      * Get WebRequest specific for customArea module in Web API access.
      * @readonly
      */

    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    /**
      * Get WebRequest specific for EnumResource module in Web API access.
      * @readonly
      */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * 
     * @public
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad) return;
        var dfd = $.Deferred();

        var filter = {
            types: [Enums.ModelData.EnumResourceType.PoiType]
        };

        var d1 = this.webRequestCustomArea.customAreaType();
        var d2 = this.webRequestEnumResource.listEnumResource(filter);

        $.when(d1,d2).done((type,resEnums) => {
            if (type) {
                this.typeOptions(type["items"]);
            }

            if (_.size(resEnums["items"]) > 0) {
                let tmpPoiType = resEnums["items"];

                let enumTypePoi = _.filter(tmpPoiType, (x) => {

                    if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                        return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                    }
                    else {
                        if (!WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea)) {
                            return x.value != Enums.ModelData.PoiType.Shared;
                        }
                        return x;
                    }
                });

                this.poiTypeOptions(enumTypePoi);
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
      * Clear reference from DOM for garbage collection.
      * @lifecycle Called when Custom-area-create is unloaded.
      */
    onUnload() {

        MapManager.getInstance().clear(this.id);
    }

    refreshMap() {
       
        let radius = this.areaBuffer();
        let unit = "meters";
        let fillColor = this.rgbTohex(this.areaType().symbolFillColor);
        let borderColor = this.rgbTohex(this.areaType().symbolBorderColor);
        let fillOpacity = 0.3;
        let borderOpacity = 0;
        let generalize = this.generalize();
        let color=this.color==null?"#7000db":this.color;
        MapManager.getInstance().drawRouteBuffer(this.id, this.path, radius, unit, color, color, fillOpacity, borderOpacity, generalize);
    }

    /**
       convert hex format to a rgb color
    */
    rgbTohex(rgb) {

        let objRgb = rgb.split(",");
        return "#" + this.hex(objRgb[0]) + this.hex(objRgb[1]) + this.hex(objRgb[2]);
    }

    hex(x) {
        let hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }

    generateModel(param) {
        let model = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            ViaPoints: JSON.stringify(this.bufferPoints),
            Name: this.name() ? this.name() : null,
            Description: this.areaDescription() ? this.areaDescription() : null,
            TypeId: this.areaType().id ? this.areaType().id : 0,
            TotalPoints: this.totalPoints() ? this.totalPoints() : null,
            Size: this.size() ? this.size() : null,
            MaxLatitude: param.result.maxLatitude ? param.result.maxLatitude : 0.0,
            MinLatitude: param.result.minLatitude ? param.result.minLatitude : 0.0,
            MaxLongitude: param.result.maxLongitude ? param.result.maxLongitude : 0.0,
            MinLongitude: param.result.minLongitude ? param.result.minLongitude : 0.0,
            CentralLatitude: param.result.centralLatitude ? param.result.centralLatitude : 0.0,
            CentralLongitude: param.result.centralLongitude ? param.result.centralLongitude : 0.0,
            poiType: this.poiType() ? this.poiType().value : null
        }
        return model;

    }

    setupExtend() {

        this.name.extend({
            trackChange: true
        });
        this.areaBuffer.extend({
            trackChange: true
        });
        this.areaType.extend({
            trackChange: true
        });
        this.generalize.extend({
            trackChange: true
        });
        // Validation
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.areaBuffer.extend({
            required: true
        });
        this.areaType.extend({
            required: true
        });
        this.generalize.extend({
            required: true
        });
        this.poiType.extend({
            required: true
        });


        this.validationModel = ko.validatedObservable({
            name: this.name,
            areaBuffer: this.areaBuffer,
            areaType: this.areaType,
            generalize: this.generalize,
            poiType: this.poiType 
        });
    }

    /**
     * 
     * Delete data
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    /**
     * TODO:Delete selected item
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", "Save"));
        actions.push(this.createActionCancel());
    }


    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            this.isSave(true);
            this.refreshMap();
        }

    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomAreaCreateScreen),
    template: templateMarkup
};