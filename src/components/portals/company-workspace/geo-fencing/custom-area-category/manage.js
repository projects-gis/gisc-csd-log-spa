﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomAreaCategory";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";


class ManageAreaCategoriesScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode === "create") {
            this.bladeTitle(this.i18n("CreateAreaCategories_Title")());
        } else if (this.mode === "update") {
            this.bladeTitle(this.i18n("UpdateAreaCategories_Title")());
        }
        this.areaId = this.ensureNonObservable(params.id, -1);
        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.color = ko.observable();
        this.typeOptions = ko.observableArray();
        this.type = ko.observable();
        // Set datasource to DataTable control

        // To preserve state when journey is inactive.
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestAreaCategory() {
            return WebRequestAreaCategory.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();

        let dfdAreaType = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.AreaCategoryType]
        });

        let dfdGetCustomAreaCat = this.mode === "update" ? dfdGetCustomAreaCat = this.webRequestAreaCategory.getCustomAreaCategory(this.areaId) : null;

        $.when(dfdAreaType,dfdGetCustomAreaCat).done((rAreaType,rCustomArea) => {

            if(rAreaType){
                this.typeOptions(rAreaType.items);
            }
            if(rCustomArea){
                var name = rCustomArea["name"];
                var description = rCustomArea["description"];
                var code = rCustomArea["code"];
                var color = rCustomArea["color"];

                this.name(name);
                this.description(description);
                this.code(code);
                this.color(color);

                var type = !_.isNil(rCustomArea.areaCategoryType) ? ScreenHelper.findOptionByProperty(this.typeOptions, "value", rCustomArea.areaCategoryType) : null;
                this.type(type);
            }
                
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    onUnload() {

    }

    setupExtend() {
        this.name.extend({
            trackChange: true
        });
        this.color.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });

        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.color.extend({
            required: true
        });
        this.type.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            color: this.color,
            type: this.type
        });
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    generateModel() {
            //if (this.mode === "create") {
            //    var model = {
            //        companyId: WebConfig.userSession.currentCompanyId,
            //        name: this.name() ? this.name() : null,
            //        description: this.description() ? this.description() : null,
            //        code: this.code() ? this.code() : null,
            //        color: this.color() ? this.color() : null,
            //        areaCategoryType:this.type() ? this.type().value  : null
            //    };
            //} else if (this.mode === "update") {
            //    var model = {
            //        id: this.areaId,
            //        companyId: WebConfig.userSession.currentCompanyId,
            //        name: this.name() ? this.name() : null,
            //        description: this.description() ? this.description() : null,
            //        code: this.code() ? this.code() : null,
            //        color: this.color() ? this.color() : null,
            //        areaCategoryType:this.type() ? this.type().value  : null
            //    };
            //}

        var model = {
            id: this.areaId,
            companyId: WebConfig.userSession.currentCompanyId,
            name: this.name() ? this.name() : null,
            description: this.description() ? this.description() : null,
            code: this.code() ? this.code() : null,
            color: this.color() ? this.color() : null,
            areaCategoryType:this.type() ? this.type().value  : null
        };
            return model;
        }
    /**
      * Life cycle: handle command click
      * @public
      * @param {any} sender
      */
    onCommandClick(sender) {

    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();
            switch (this.mode) {
                case "create":
                    this.isBusy(true);
                    this.webRequestAreaCategory.createCustomAreaCategory(model).done((response) => {
                        this.publishMessage("cw-geo-fencing-custom-area-category-changed");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case "update":
                    this.isBusy(true);
                    this.webRequestAreaCategory.updateCustomAreaCategory(model).done((response) => {
                        this.publishMessage("cw-geo-fencing-custom-area-category-changed", this.areaId);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(ManageAreaCategoriesScreen),
    template: templateMarkup
};