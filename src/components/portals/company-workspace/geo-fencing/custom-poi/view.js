﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import {
    EntityAssociation,
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";

/**
 * 
 * 
 * @class CustomPOIDetailScreen
 * @extends {ScreenBase}
 */
class CustomPOIViewScreen extends ScreenBase {
    /**
     * Creates an instance of CustomPOIDetailScreen.
     * 
     * @param {any} params
     * 
     * @memberOf CustomPOIDetailScreen
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomPOI_View")());
        this.bladeSize = BladeSize.Medium;
        this.customPoiId = this.ensureNonObservable(params.customPoiId);
        this.name = ko.observable();
        this.description = ko.observable();
        this.category = ko.observable();
        this.icon = ko.observable();
        this.createdDate = ko.observable();
        this.createdBy = ko.observable();
        this.lastUpdatedDate = ko.observable();
        this.lastUpdatedBy = ko.observable();
        this.latitude = ko.observable();
        this.longitude = ko.observable();
        this.radius = ko.observable();
        this.town = ko.observable();
        this.city = ko.observable();
        this.province = ko.observable();
        this.country = ko.observable();
        this.code = ko.observable();
        this.accessibleBusinessUnits = ko.observableArray();
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);
        this.poiType = ko.observable();
        this.poiUserPermissionType = ko.observable();
        this.poiTypeEnum = ko.observable();

        this.displaySize = ko.observable();
        this.numberofareapoint = ko.observable();
        this.subPOIareaList = ko.observableArray();
        this.subPoiArea = ko.observable();
        this.areaUnit = ko.observable();
        this.areaUnitType = ko.observable();
        this.areaUnitText = ko.observable();
        this.areaPoint = ko.observableArray();
        this.areaPOIList = ko.observableArray();
        this.mainPoint = ko.observableArray();
        this.selectedSubAreaId = ko.observable();
        this.parentPoiArea = ko.observable();
        // this.areaId = this.ensureNonObservable(params.id);

        this.subscribeMessage("cw-geo-fencing-custom-poi-changed", (id) => {
            this.webRequestCustomPOI.getCustomPOI(this.customPoiId).done((response) => {
                let category = response.category != null ? response.category.name : response.category;
                this.name(response.name);
                this.description(response.description);
                this.category(category);
                if(response.icon!=null){
                    this.icon(response.icon.image.fileUrl);
                }
                this.code(response.code)
                this.createdDate(response.formatCreateDate);
                this.createdBy(response.createBy);
                this.lastUpdatedDate(response.formatUpdateDate);
                this.lastUpdatedBy(response.updateBy);
                this.latitude(response.latitude);
                this.longitude(response.longitude);
                this.radius(response.formatRadius);
                this.town(response.townName);
                this.city(response.cityName);
                this.province(response.provinceName);
                this.country(response.countryName);
                this.poiType(response.poiTypeDisplayName);
                this.poiUserPermissionType(response.poiUserPermissionType);
                this.poiTypeEnum(response.poiType);
                var mappedSelectedBUs = response.accessibleBusinessUnits.map((bu) => {
                    let mbu = bu;
                    mbu.businessUnitId = bu.businessUnitId;
                    mbu.businessUnitName = bu.businessUnitName;
                    return mbu;
                });
                this.accessibleBusinessUnits(mappedSelectedBUs);

                this.obj = JSON.parse(response.parentPoiArea.points);
                if(this.obj && this.obj.length > 0){
                    this.mainPoint(this.obj)

                var point = [];
                    ko.utils.arrayForEach(this.obj[0], function(items, index) {
                        
                        point.push({
                            id: (index + 1),
                            point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                        })
                    });
                    point.splice(point.length - 1);
                    this.areaPoint(point);
                }else{
                    this.areaPoint([]);
                }
            this.displaySize(Utility.convertAreaUnit(response.parentPoiArea.size, this.areaUnitType()));
            this.numberofareapoint(response.parentPoiArea.totalPoints)  
            this.subPOIareaList(response.subPoiArea)
            })
        });
        this._selectingRowHandler = (subarea) => {
            this.navigate("cw-geo-fencing-custom-poi-sub-area-view", {
                // mode: "update",
                selectedSubArea: _.cloneDeep(subarea),
                parentPoiArea:_.cloneDeep(this.parentPoiArea()),
                subareaList:_.cloneDeep(this.subPOIareaList())
            });
        }
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestCustomPOI() {
            return WebRequestCustomPOI.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((companySettings)=>{

            if (companySettings) {
                this.areaUnitText(companySettings.areaUnitSymbol);
                this.areaUnit(this.i18n("CustomArea_Size_2", [this.areaUnitText()]));
                this.areaUnitType(companySettings.areaUnit);
            }
        })


        var dfd = $.Deferred();
        this.webRequestCustomPOI.getCustomPOI(this.customPoiId).done((response) => {
            this.parentPoiArea(response.parentPoiArea)
            let category = response.category != null ? response.category.name : response.category;
            this.accessibleBusinessUnits(response.accessibleBusinessUnits);
            this.name(response.name);
            this.description(response.description);
            this.category(category);
            if (response.icon != null) {
                this.icon(response.icon.image.fileUrl);
            }
            this.createdDate(response.formatCreateDate);
            this.createdBy(response.createBy);
            this.lastUpdatedDate(response.formatUpdateDate);
            this.lastUpdatedBy(response.updateBy);
            this.latitude(response.latitude);
            this.longitude(response.longitude);
            this.radius(response.radius);
            this.town(response.townName);
            this.city(response.cityName);
            this.province(response.provinceName);
            this.country(response.countryName);
            this.code(response.code);
            this.poiType(response.poiTypeDisplayName);
            this.poiUserPermissionType(response.poiUserPermissionType);
            this.poiTypeEnum(response.poiType);
            this.obj = JSON.parse(response.parentPoiArea.points);
            if(this.obj && this.obj.length > 0){
                this.mainPoint(this.obj)

                var point = [];
                    ko.utils.arrayForEach(this.obj[0], function(items, index) {
                        
                        point.push({
                            id: (index + 1),
                            point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                        })
                    });
                    point.splice(point.length - 1);
                this.areaPoint.replaceAll(point);
            }
            // this.displaySize(response.parentPoiArea.size)
            this.displaySize(Utility.convertAreaUnit(response.parentPoiArea.size, this.areaUnitType()));
            this.numberofareapoint(response.parentPoiArea.totalPoints)  
            this.subPOIareaList(response.subPoiArea)



            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        }).always(() => {

        });
        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
            MapManager.getInstance().clear(this.id);
        }
    /**
      * @lifecycle Build available action button collection.
      * @param {any} actions
      */
    buildActionBar(actions) {}
    /**
      * @lifecycle Build available commands collection.
      * @param {any} commands
      */
    buildCommandBar(commands) {

        if ( WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomPOI) &&
            (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        )
        {
            if (this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) { // check ว่าเป็น Shared ไหม
                if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI)) { // เช็คว่า มี Permission Shared ไหม
                    commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
                }
            }
            else {
                commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));

            }
        }

        commands.push(this.createCommand("cmdViewOnMap", this.i18n("Common_ViewOnMap")(), "svg-cmd-search"));
        /* wait for implement BL */
        /*
        commands.push(this.createCommand("cmdVerify", this.i18n("Common_Verify")(), "svg-cmd-edit"));
        */

        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteCustomPOI) &&
            (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        )
        {
            if (this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) {
                if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
            }
            else {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));

            }
        }

    }
    /**
      * @lifecycle Handle when button on ActionBar is clicked.
      * @param {any} commands
      */
    onActionClick(sender) {
            super.onActionClick(sender);
    }
    /**
      * @lifecycle Hangle when command on CommandBar is clicked.
      * @param {any} sender
      */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("cw-geo-fencing-custom-poi-manage", {
                    mode: "update",
                    customPoiId: this.customPoiId
                });
                break;
            case "cmdViewOnMap":
                // let imgUrlPath = this.icon() ? this.icon() : this.resolveUrl("~/images/block_hilight.png");
                
                let imgUrlPath = this.resolveUrl("~/images/block_hilight.png")//this.resolveUrl("~/images/pin_destination.png")//;
                let clickLocation = {
                        lat: this.latitude(),
                        lon: this.longitude()
                    },
                    symbol = {
                        url: imgUrlPath,
                        width: 32,
                        height: 32,
                        offset: {
                            x: 0,
                            y: 0
                        },
                        rotation: 0
                    },
                    zoom = 17,
                    attributes = null

                    //-----------------
                    // symbol = {
                    //     url: imgUrlPath,//this.resolveUrl("~/images/pin_destination.png"),
                    //     width: '35',
                    //     height: '50',
                    //     offset: {
                    //         x: 0,
                    //         y: 23.5
                    //     },
                    //     rotation: 0
                    // },
                    // zoom = 17,
                    // attributes = null

                // //Call clear command.

                // //openCustomPOILayer
                MapManager.getInstance().openCustomPOILayer();

                // //d1pz
                // MapManager.getInstance().drawOnePointZoom(this.id, clickLocation, symbol, zoom, attributes);
                this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
                MapManager.getInstance().drawPointPoiArea(this.id, clickLocation, symbol, zoom, attributes);
                
                    if(this.parentPoiArea().name){

                        MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                        let mainArea = JSON.parse(this.parentPoiArea().points) //'#80ff00', '#ff9b00'this.mainPoint()
                        MapManager.getInstance().drawOnePolygonZoomArea(this.id,this.mainPoint() , `rgba(${this.parentPoiArea().type.symbolFillColor})`, `rgba(${this.parentPoiArea().type.symbolFillColor})`, 0.5, 1, null,"draw-polygon-main-area");
                        this.subPOIareaList().map((itm,index)=>{
                            let result = JSON.parse(itm.points)
                            let attributes = {
                                name:itm.name,
                                index:index
                            }
                            MapManager.getInstance().drawOnePolygonZoomArea(this.id, result, `rgba(${itm.type.symbolFillColor})`, `rgba(${itm.type.symbolBorderColor})`, 0.5, 1, attributes,"draw-polygon-sub-area");
                        })
                    }
                
                this.minimizeAll();
                break;
            case "cmdVerify":

                this.navigate("cw-geo-fencing-custom-poi-view-verify", {
                    customPoiId: this.customPoiId
                });
                break;
            case "cmdDelete":

                this.showMessageBox(null, this.i18n("M045")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestCustomPOI.deleteCustomPOI(this.customPoiId).done(() => {
                                MapManager.getInstance().clear(this.id);
                                this.publishMessage("cw-geo-fencing-custom-poi-delete");
                                this.close(true);
                            }).fail((e) => {
                                this.handleError(e);
                            });

                            break;
                    }
                });
                break;
        }
    }

    onMapComplete(response) {
        // console.log("view on map : ", response);

    }

}

export default {
    viewModel: ScreenBase.createFactory(CustomPOIViewScreen),
    template: templateMarkup
};