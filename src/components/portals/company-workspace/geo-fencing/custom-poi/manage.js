﻿import "jquery";
import ko from "knockout";
import _ from "lodash";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import WebRequestCustomPOI from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebRequestCountry from "../../../../../app/frameworks/data/apicore/webrequestCountry";
import WebRequestProvince from "../../../../../app/frameworks/data/apicore/webrequestProvince";
import WebRequestCity from "../../../../../app/frameworks/data/apicore/webrequestCity";
import WebRequestTown from "../../../../../app/frameworks/data/apicore/webrequestTown";
import WebRequestPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOICategory";
import WebRequestPOIIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";

class CustomPOIMangeScreen extends ScreenBase {

    constructor(params) {
        super(params);
        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);
        this.markpoi = this.ensureNonObservable(params.markpoi);
        this.customPoiId = this.ensureNonObservable(params.customPoiId);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("CustomPOI_CreateCustomPOI")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("CustomPOI_UpdateCustomPOI")());
        }
        this.subPOIareaList = ko.observableArray();
        this.subPoiArea = ko.observable();
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);
        this.businessUnitDS = ko.observableArray([]);
        this.categoryOptions = ko.observableArray();
        this.iconOptions = ko.observableArray();
        this.iconSelectedItem = ko.observable();
        this.countryOptions = ko.observableArray();
        this.provinceOptions = ko.observableArray();
        this.cityOptions = ko.observableArray();
        this.townOptions = ko.observableArray();
        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.latitude = ko.observable();
        this.longitude = ko.observable();
        this.radius = ko.observable(100);
        this.category = ko.observable();
        this.country = ko.observable();
        this.province = ko.observable();
        this.city = ko.observable();
        this.town = ko.observable();
        this.size = ko.observable();
        this.displaySize = ko.observable();
        this.numberofareapoint = ko.observable();
        this.areaUnit = ko.observable();
        this.areaUnitType = ko.observable();
        this.areaUnitText = ko.observable();

        this._responseIden = ko.observableArray();
        this._flagIden = false;
        this.isDropdownEnable = ko.observable(false);
        this.enablePoiType = false;
        this.isRadiusEnable = ko.observable(true);

        //Poi Type
        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        this.areaPoint = ko.observableArray();
        this.areaPOIList = ko.observableArray();
        this.areaId = this.ensureNonObservable(params.id);

        this.selectedSubAreaId = ko.observable();
        this.recentChangedRowIds = ko.observableArray();
        this.areaPointToDraw = ko.observableArray()

        this.tmpParentArea = ko.observable();
        this.parentAreaId = ko.observable();
        this.points = ko.observable();
        
        this.updateResult = ko.observable();
        this.areaPointerror = ko.observable();
        this.pointerror = ko.observable();
        this.subAreaError = ko.observable();
        // 'rgba(46,191,36,0.2)', 'rgba(46,191,36,0.4)'
        this.parentFillColor = ko.observable('rgba(46,191,36,0.2)');
        this.parentBorderColor = ko.observable('rgba(46,191,36,0.4)');

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI) || WebConfig.userSession.userType == Enums.UserType.BackOffice ? true : false;
        }
        else {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI) ? true : false;
        }
        
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;

        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-geo-fencing-custom-poi-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        this.subscribeMessage("cw-geo-fencing-custom-sub-poi-area-changed",(data)=>{
            if(data.mode === Screen.SCREEN_MODE_CREATE){
                let last = this.subPOIareaList().length
                data.id = (last === 0) ? last+1:this.subPOIareaList()[last-1].id+1
                this.subPOIareaList.push(data)
                // this.areaPointToDraw().push(data)
            }else if(data.mode === "actCancel"){
                    if(this.areaPoint() && (this.areaPoint().length > 0)){
                        this.drawPolygon()
                    }
            }else{
                    let editdate = _.cloneDeep(this.subPOIareaList())
                    let subAreaList = []
                    let pomis = new Promise(function(resolve){
    
                        ko.utils.arrayForEach(editdate, function(value, index) {
                            if(value.id === data.id){
                                subAreaList.push(data)
                            }else{
                                subAreaList.push(value)
                            }
                        });
                        resolve(subAreaList)
                    })
    
                    pomis.then( (params)=> {
                        this.subPOIareaList(_.cloneDeep(params))
                        // this.areaPointToDraw.replaceAll(_.cloneDeep(params))
                    })
                
            }
            // this.drawPolygon()
        })

        this.subscribeMessage("cw-geo-fencing-custom-sub-poi-close",(data)=>{
            if(this.subPOIareaList().length > 0){
                this.drawPolygon()
            }
        })

        this.extent = ko.observable();

        this.isSettingUpdate = false;

        this.areaPoint.subscribe((area)=>{
            if (!this.isSettingUpdate){
                // console.log(this.areaPOIList())
                // this.drawPolygon(this.areaPOIList())
            }
        })

        this.subPOIareaList.subscribe((area)=>{
                this.drawPolygon()
        })


        //Subscribe Latitude
        this.latitude.subscribe((lat) => {
            if (!this.isSettingUpdate){
                this.idenPoi(this.latitude(), this.longitude(), this.extent());
            }
        });

        //Subscribe Longitude
        this.longitude.subscribe((lon) => {
            if (!this.isSettingUpdate) {
                this.idenPoi(this.latitude(), this.longitude(), this.extent());
            }
        });
        
        this._selectingRowHandler = (subarea) => {
            if (subarea) {
                // MapManager.getInstance().clear();

                let ListSubarea = _.cloneDeep(this.subPOIareaList())
                let subAreaList = []

                // this.subPOIareaList().map((itm,index)=>{
                //     if(itm.id !== subarea.id){
                //         let result = JSON.parse(itm.Points)
                //         let attributes = {
                //             name:itm.Name,
                //             index:index
                //         }
                //         MapManager.getInstance().drawOnePolygonZoomArea(this.id, result, `rgba(${itm.Type.symbolFillColor})`, `rgba(${itm.Type.symbolBorderColor})`, 0.5, 1, attributes,"enable-draw-polygon-area");
                //    //"draw-polygon-sub-area"
                //     }else{
                //         MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                //     }
                    
                // })
                let pomis = new Promise(function(resolve){
                    ko.utils.arrayForEach(ListSubarea, function(value, index) {
                        if(value.id !== subarea.id){
                            subAreaList.push(value)
                        }
                    });
                    resolve(subAreaList)
                })

                pomis.then( (params)=> {
                   //ลบ Sub area ทั้งหมด
                   MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                   //วาด Sub area ตัวอื่นที่ไม่ได้เลือกมาใหม่
                   params.map((itm,index)=>{
                       let result = JSON.parse(itm.points)
                       let attributes = {
                           name:itm.name,
                           index:index
                       }
                       MapManager.getInstance().drawOnePolygonZoomArea(this.id, result, `rgba(${itm.type.symbolFillColor})`, `rgba(${itm.type.symbolBorderColor})`, 0.5, 1, attributes,"draw-polygon-sub-area");
                   })
                })

                this.navigate("cw-geo-fencing-custom-poi-manage-sub-poi-area", {
                    mode: "update",
                    selectedSubArea: _.cloneDeep(subarea)
                });
            }
            return false;
        };

        this.isShowAreaPoi = WebConfig.userSession.hasPermission(Constants.Permission.ManagePOIArea);

        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
        MapManager.getInstance().setFollowTracking(false);
    }

    goto(extras) {
        // TODO: Uncomment this block for allow real navigation in next phase.
        this.navigate(extras.id);
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }

    get webRequestCountry() {
        return WebRequestCountry.getInstance();
    }

    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }

    get webRequestCity() {
        return WebRequestCity.getInstance();
    }

    get webRequestTown() {
        return WebRequestTown.getInstance();
    }

    get webRequestPOICategory() {
        return WebRequestPOICategory.getInstance();
    }

    get webRequestPoiIcon() {
        return WebRequestPOIIcon.getInstance();
    }

    get webRequestMap() {
        return WebRequestMap.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }


    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.minimizeAll(false);

        var dfd = $.Deferred();
        var d0 = $.Deferred();
        var d1 = $.Deferred();
        
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
                EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        };
        
        var filterPoiIcon = {
            includeAssociationNames: [EntityAssociation.PoiIcon.Image]
        };
        var t1 = this.webRequestPOICategory.listCustomPOICategorySummary({
            "companyId": WebConfig.userSession.currentCompanyId
        });
        var t2 = this.webRequestPoiIcon.listPoiIconSummary(filterPoiIcon);
        var t3 = this.webRequestBusinessUnit.listBusinessUnitSummary(filter);
        var t4 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.PoiType]
        });
        var t5 = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
        
        $.when(t1, t2, t3, t4,t5).done((responsePoiCategory, responsePoiIcon, responseBu,responsePoiType,companySettings) => {

            if (responseBu) {
                this.businessUnitDS(responseBu["items"]);
            }

            if (responsePoiCategory) {
                var poiCategorys = responsePoiCategory["items"];
                this.categoryOptions.replaceAll(poiCategorys);
            }

            if (responsePoiIcon) {
                var poiIcon = responsePoiIcon["items"];
                var cboDataPoi = [];
                if (poiIcon.length > 0) {
                    ko.utils.arrayForEach(poiIcon, function (value, index) {
                        let obj = {
                            image: value.image.fileUrl,
                            value: value.id,
                            text: value.name
                        };
                        cboDataPoi.push(obj);
                    });
                }
                this.iconOptions(cboDataPoi);
            }

            if (companySettings) {
                this.areaUnitText(companySettings.areaUnitSymbol);
                this.areaUnit(this.i18n("CustomArea_Size_2", [this.areaUnitText()]));
                this.areaUnitType(companySettings.areaUnit);
            }

            let tmpPoiType = _.filter(responsePoiType["items"], (x) => {
                if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                    return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                }
                else {
                    if (!WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomPOI)) {
                        return x.value != Enums.ModelData.PoiType.Shared;
                    }
                    return x;
                }
                
            });
            this.poiTypeOptions(tmpPoiType);

            d1.resolve();
        }).fail((e) => {
            d1.reject(e);
            this.handleError(e);
        }).always(() => {

        });
        
        switch (this.mode) {
            case "create":
                this.poiType(ScreenHelper.findOptionByProperty(this.poiTypeOptions, "value", WebConfig.userSession.permissionType));

                this._originalAccessibleBusinessUnits = [];
                this.webRequestCountry.listCountry({
                    sortingColumns: DefaultSorting.Country
                }).done((response) => {
                    var countries = response["items"];
                    this.countryOptions(countries);
                    var defaultCountry = ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true);
                    //this.country(defaultCountry);

                    this.webRequestProvince.listProvince({
                        countryIds: [defaultCountry.id]
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions(provinces);
                        d0.resolve();
                    }).fail((e) => {
                        d0.reject(e);
                        this.handleError(e);
                    }).always(() => {

                    });
                }).fail((e) => {
                    d0.reject(e);
                    this.handleError(e);
                }).always(() => {

                });
                if (this.markpoi != null) {
                    this.extent(this.markpoi.extent?this.markpoi.extent:'')
                    this.latitude(this.markpoi.lat);
                    this.longitude(this.markpoi.lon);
                    
                }
                break;
            case "update":
                this.isSettingUpdate = true;
                this.webRequestCustomPOI.getCustomPOI(this.customPoiId).done((response) => {
                    if (response) {
                        this.updateResult(_.cloneDeep(response))
                        this.name(response.name);
                        this.latitude(response.latitude);
                        this.longitude(response.longitude);
                        this.drawPoint(this.latitude(), this.longitude());
                        this.code(response.code);
                        this.description(response.description);
                        this.radius(response.radius);
                        this.accessibleBusinessUnits(response.accessibleBusinessUnits);
                        this._originalAccessibleBusinessUnits = _.cloneDeep(response.accessibleBusinessUnits);


                       
                        this.tmpParentArea(response.parentPoiArea)
                        this.parentAreaId(response.parentPoiArea.id);
                        if (WebConfig.userSession.userType == Enums.UserType.BackOffice) {
                            let filterPoiType = [];
                            switch (response.poiType) {
                                case Enums.ModelData.PoiType.Public:
                                    filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                                    break;

                                case Enums.ModelData.PoiType.Private:
                                    filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                                    break;

                                case Enums.ModelData.PoiType.Shared:
                                    if (response.poiUserPermissionType == 1)  // 1 is Public of PoiUserPermission , 2 is Private of PoiUserPermission
                                    {
                                        filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                                    }
                                    else {
                                        filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                                    }
                                    break;
                            }
                            this.poiTypeOptions(filterPoiType);
                        }
                        
                        this.poiType(ScreenHelper.findOptionByProperty(this.poiTypeOptions, "value", response.poiType));

                        if (response.categoryId) {
                            this.category(ScreenHelper.findOptionByProperty(this.categoryOptions, "id", response.categoryId));
                        }
                        if (response.iconId) {
                            this.iconSelectedItem(ScreenHelper.findOptionByProperty(this.iconOptions, "value", response.iconId));
                        }

                        var d1 = this.webRequestCountry.listCountry({});
                        var d2 = response.countryId ? this.webRequestProvince.listProvince({
                            countryIds: [response.countryId]
                        }) : null;
                        var d3 = response.provinceId ? this.webRequestCity.listCity({
                            provinceIds: [response.provinceId]
                        }) : null;
                        var d4 = response.cityId ? this.webRequestTown.listTown({
                            cityIds: [response.cityId]
                        }) : null;

                        if(response.parentPoiArea){
                            if(response.parentPoiArea.type){
                                this.parentFillColor(`rgba(${response.parentPoiArea.type.symbolFillColor})`);
                                this.parentBorderColor(`rgba(${response.parentPoiArea.type.symbolBorderColor})`);
                            }
                        let pointMain = JSON.parse(response.parentPoiArea.points)
                        var point = [];
                        if(pointMain){
                            ko.utils.arrayForEach(pointMain[0], function(items, index) {
                                point.push({
                                    id: (index + 1),
                                    point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                                })
                            });
                            point.splice(point.length - 1);
                        }
                        this.areaPoint(point)
                        this.areaPointToDraw([{
                            Name:"MainArea",
                            rings:pointMain
                        }])
                        if(pointMain){
                            this.isRadiusEnable(false)
                        }
                        this.size(response.parentPoiArea.size);
                        this.displaySize(Utility.convertAreaUnit(response.parentPoiArea.size, this.areaUnitType()))
                        this.numberofareapoint(response.parentPoiArea.totalPoints);
                        this.points(pointMain);


                        MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                        MapManager.getInstance().drawOnePolygonZoomArea(this.id, pointMain, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");
                       
                    }
                    
                        if(response.subPoiArea){
                            this.subPOIareaList(response.subPoiArea)
                        }


                        $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {

                            // var countries = r1["items"];
                            // this.countryOptions(countries);

                            if (r1) {

                                var countries = r1["items"];
                                this.countryOptions(countries);
                                if (response.countryCode) {
                                    this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "code", response.countryCode));
                                } else {
                                    this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "isDefault", true));

                                }
                            }

                            if (r2) {
                                var provinces = r2["items"];
                                this.provinceOptions(provinces);
                                if (response.provinceCode) {
                                    this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", response.provinceCode));
                                }else{
                                    this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "id", response.provinceId));

                                }
                            }

                            if (r3) {
                                var cities = r3["items"];
                                this.cityOptions(cities);

                                if (response.cityCode) {
                                    this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "code", response.cityCode));
                                }else{
                                    this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "id", response.cityId));

                                }
                            }

                            if (r4) {
                                var towns = r4["items"];
                                this.townOptions(towns);

                                if (response.townCode) {
                                    this.town(ScreenHelper.findOptionByProperty(this.townOptions, "", response.townCode));
                                }else{
                                    this.town(ScreenHelper.findOptionByProperty(this.townOptions, "id", response.townId));

                                }
                            }
                            this.isSettingUpdate = false;
                            d0.resolve();
                        }).fail((e) => {
                            d0.reject(e);
                            this.isSettingUpdate = false;
                            this.handleError(e);
                        }).always(() => {

                        });

                    }
                });
                break;
        }


        $.when(d0, d1).done(() => {

            this._changeCountrySubscribe = this.country.subscribe((country) => {

                if (country) {
                    this.webRequestProvince.listProvince({
                        countryIds: [country.id]
                    }).done((response) => {
                        var provinces = response["items"];
                        this.provinceOptions.replaceAll(provinces);

                        setTimeout(() => {
                            if (this._responseIden) {
                                this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", this._responseIden.adminLevel1Code));
                            }
                        }, 500);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.provinceOptions.replaceAll([]);
                }

                this.publishMessage("cw-geo-fencing-custom-poi-manage-country-changed", country);
            });

            this._changeProvinceSubscribe = this.province.subscribe((province) => {
                if (province) {
                    this.city(null);
                    this.webRequestCity.listCity({
                        provinceIds: [province.id]
                    }).done((response) => {
                        var cities = response["items"];
                        this.cityOptions.replaceAll(cities);
                        setTimeout(() => {
                            if (this._responseIden) {
                                
                                this.city(ScreenHelper.findOptionByProperty(this.cityOptions, "code", this.generateModel().cityCode));

                            }
                        }, 500);

                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {

                    });
                } else {
                    this.cityOptions.replaceAll([]);
                }
            });

            this._changeCitySubscribe = this.city.subscribe((city) => {

                if (city) {
                    this.town(null);
                    this.isBusy(true);
                    this.webRequestTown.listTown({
                        cityIds: [city.id]
                    }).done((response) => {
                        var towns = response["items"];
                        this.townOptions.replaceAll(towns);

                        setTimeout(() => {

                            if (this._responseIden) {

                                this.town(ScreenHelper.findOptionByProperty(this.townOptions, "code", this.generateModel().townCode));

                            }

                        }, 500);


                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                } else {
                    this.townOptions.replaceAll([]);
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        }).always(() => {

        });


        return dfd;

    }

    onUnload() {
        MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
        MapManager.getInstance().clear(this.id);
        MapManager.getInstance().cancelDrawingPolygon(this.id);
    }

    enableClickMap() {

        // Call clear map command.
        MapManager.getInstance().clear(this.id);

        // Call click command.
        MapManager.getInstance().enableClickMap(this.customPoiId);
        this.minimizeAll();
    }
    // drawPOIArea(){
    //     // Call clear map command.
    //     MapManager.getInstance().clear(this.id);

    //     // Call click command.
    //     MapManager.getInstance().drawPOIArea(this.customPoiId);
    //     this.minimizeAll();
    // }
    deletePointArea(){

        this.showMessageBox(null, this.i18n("M045")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                    this.areaPoint([]);
                    this.displaySize("0");
                    this.size('0')
                    this.numberofareapoint("0");
                    this.subPOIareaList([]);
                    this.points([]);
                    this.areaPointerror(false)
                    this.isRadiusEnable(true)

                    //สั่งปิดหน้า SubArea
                    this.publishMessage("cw-geo-fencing-custom-main-area-close-sub-area");

                    //ลบกราฟฟิคบนแผนที่
                    MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
                    MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                    
                    break;
            }
        });
    }
    enableDrawArea(){
        this.minimizeAll(true);
        let companydata = {
            UserLanguage:WebConfig.userSession._prefUserLanguage,
            CompanyId:WebConfig.userSession._userSession.currentCompanyId
        }
        localStorage.setItem("companydata",JSON.stringify(companydata))

        var areaUnit = {
            type: this.areaUnitType(),
            text: this.areaUnit()
        };
        var points = this.points() && this.points().length > 0? this.points():null
        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            MapManager.getInstance().enableDrawingPolygon(this.id, points, areaUnit);
        } else {
            MapManager.getInstance().enableDrawingPolygon(this.id, points, areaUnit);
        }

    }

    onMapComplete(response) {
        if (response.command == "enable-click-map") {
            this.drawPoint(response.result.location.lat, response.result.location.lon);
            this.extent(response.result.location.extent);
            this.latitude(response.result.location.lat.toFixed(8));
            this.longitude(response.result.location.lon.toFixed(8));

            //this.idenPoi();

        }
        // this.latitude = ko.observable();
        // this.longitude = ko.observable();

        if (response.command == "enable-draw-polygon") {

            var point = [];
            ko.utils.arrayForEach(response.result.geometry.rings[0], function(items, index) {
                point.push({
                    id: (index + 1),
                    point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                })
            });
            point.splice(point.length - 1);
            this.areaPOIList([response.result.geometry]);
            response.result.tmp = this.tmpParentArea()
            this.tmpParentArea(response.result)
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                this.areaPoint(point);
                this.areaPointToDraw([{
                    Name:"MainArea",
                    rings:response.result.geometry.rings
                }])
                this.size(response.result.area);
                this.displaySize(Utility.convertAreaUnit(response.result.area, this.areaUnitType()));
                this.numberofareapoint(response.result.count);
                this.points(response.result.geometry.rings);

                MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");                     
                MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");

            }else{
                this.areaPoint(point);
                this.size(response.result.area)
                this.displaySize(Utility.convertAreaUnit(response.result.area, this.areaUnitType()));
                this.numberofareapoint(response.result.count);
                this.areaPointToDraw([{
                    Name:"MainArea",
                    rings:response.result.geometry.rings
                }])
                this.points(response.result.geometry.rings);
                MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");                
                MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");

                if(!this.latitude() && !this.longitude()){
                    this.latitude(response.result.centralLatitude);
                    this.longitude(response.result.centralLongitude);
                }
            }

            if(response.result.geometry.rings){
                this.isRadiusEnable(false)
                this.areaPointerror(true)
            }else{
                this.areaPointerror(false)
                this.isRadiusEnable(true)
            }
            // this.drawMainArea()
            this.drawPolygon()
        } else if (response.command == "close-drawing-polygon") {
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                if(this.areaPointToDraw().length > 0){
                    MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");
                        // MapManager.getInstance().drawOnePolygonZoom(this.id, this.point, this.fillColor(), this.borderColor(), 0.5, 1, { name : this.name()});
                }
            }
        } else if (response.command == "cancel-drawing-polygon") {
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                if(this.areaPointToDraw().length > 0){
                    MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");                
                    MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");
                // MapManager.getInstance().drawOnePolygonZoom(this.id, this.point, this.fillColor(), this.borderColor(), 0.5, 1, { name : this.name()});
                }
            }else{
                if(this.areaPoint().length > 0){
                    if(this.areaPointToDraw().length > 0){
                        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
                        MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");
                    }
                }

            }
        }

        // if(response.command == "clear-graphics-id"){
        //     if(this.mode === Screen.SCREEN_MODE_UPDATE){

        //     }else{
        //         if(this.areaPoint().length > 0){
        //             MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, this.parentFillColor(), this.parentBorderColor(), 0.5, 1, null,"draw-polygon-main-area");
        //         }
        //     }
        // }
    }

    drawPoint(locationLat, locationLon) {
        let clickLocation = {
                lat: locationLat,
                lon: locationLon,
            },

            symbol = {
                url: this.resolveUrl("~/images/pin_destination.png"),
                width: '35',
                height: '50',
                offset: {
                    x: 0,
                    y: 23.5
                },
                rotation: 0
            },
            zoom = 17,
            attributes = null

        //Call clear command
        //backup MapManager.getInstance().drawOnePointZoom(this.customPoiId, clickLocation, symbol, zoom, attributes);
        MapManager.getInstance().drawPointPoiArea(this.customPoiId, clickLocation, symbol, zoom, attributes);
    }

    // drawMainArea(){
    //     // if(this.areaPointToDraw().length > 0){
    //         MapManager.getInstance().clear();
    //         MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, '#80ff00', '#ff9b00', 0.5, 1, null,"draw-polygon-main-area");
    //     // }
    // }

    drawPolygon(){
       if(this.subPOIareaList().length > 0)
       {
            // MapManager.getInstance().clear();
            MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
           this.subPOIareaList().map((itm,index)=>{

                // if(itm.Name === "MainArea"){
                //     MapManager.getInstance().drawOnePolygonZoomArea(this.id, itm.rings, '#80ff00', '#ff9b00', 0.5, 1, null);
                // }else{
                    let result = JSON.parse(itm.points)
                    let attributes = {
                        name:itm.name,
                        index:index
                    }
                    MapManager.getInstance().drawOnePolygonZoomArea(this.id, result, `rgba(${itm.type.symbolFillColor})`, `rgba(${itm.type.symbolBorderColor})`, 0.5, 1, attributes,"draw-polygon-sub-area");
                //    MapManager.getInstance().drawOnePolygonZoomArea(this.id, result, `#08f4fc`, `#08f4fc`, 0.5, 1, {name:itm.Name});
    
                // }
            })
        //MapManager.getInstance().drawMultiplePolygonZoomArea(this.id,this.areaPointToDraw())
       }else{
        MapManager.getInstance().clearGraphicsId("draw-polygon-sub-area");
        MapManager.getInstance().clearGraphicsId("draw-polygon-main-area");
        if(this.areaPointToDraw().length > 0){
            MapManager.getInstance().drawOnePolygonZoomArea(this.id, this.areaPointToDraw()[0].rings, 'rgba(46,191,36,0.2)', 'rgba(46,191,36,0.4)', 0.5, 1, null,"draw-polygon-main-area");
        }
       }

       
    }

    idenPoi(lat, lon,extent) {

        if (lat && lon && !_.endsWith(lat, '.') && !_.endsWith(lon, '.')) {
            this._flagIden = true;
            var param = {
                lat: lat,
                lon: lon,
                extent: extent
            };
            this.drawPoint(lat, lon)
            this.isBusy(true);
            this.webRequestMap.identify(param).done((response) => {
                this._responseIden = response;

                this._responseIden.countryCode != null && this._responseIden.countryCode != "" ? this.isDropdownEnable(false) : this.isDropdownEnable(true)
                var countryCode = this._responseIden.countryCode != null && this._responseIden.countryCode != "" ? this._responseIden.countryCode : '';
                if(countryCode != '')
                {
                    this.country(ScreenHelper.findOptionByProperty(this.countryOptions, "code", countryCode));
                    this.province(ScreenHelper.findOptionByProperty(this.provinceOptions, "code", response.adminLevel1Code));
                }
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });

        }
    }

    findCountryCode(code) {
        var countryCode;
        switch (code) {

            case "TH":
                countryCode = Constants.CountryCode.TH;
                break;
            case "SG":
                countryCode = Constants.CountryCode.SG;
                break;
            case "MY":
                countryCode = Constants.CountryCode.MY;
                break;
            case "BN":
                countryCode = Constants.CountryCode.BN;
                break;
            case "VN":
                countryCode = Constants.CountryCode.VN;
                break;
            case "PH":
                countryCode = Constants.CountryCode.PH;
                break;
            case "ID":
                countryCode = Constants.CountryCode.ID;
                break;
            case "MM":
                countryCode = Constants.CountryCode.MM;
                break;
            case "LA":
                countryCode = Constants.CountryCode.LA;
                break;
            case "KH":
                countryCode = Constants.CountryCode.KH;
                break;
        }

        return countryCode;
    }
    setupExtend() {

        this.name.extend({
            trackChange: true
        });
        this.latitude.extend({
            trackChange: true,
            rateLimit: 500
        });
        this.longitude.extend({
            trackChange: true,
            rateLimit: 500
        });
        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });

        this.description.extend({
            trackChange: true
        });

        this.code.extend({
            trackChange: true
        });

        this.category.extend({
            trackChange: true
        });

        this.iconSelectedItem.extend({
            trackChange: true
        });

        this.accessibleBusinessUnits.extend({
            trackChange: true
        });

        this.radius.extend({
            trackChange: true
        });

        this.country.extend({
            trackChange: true
        });

        this.province.extend({
            trackChange: true
        });

        this.city.extend({
            trackChange: true
        });

        this.town.extend({
            trackChange: true
        });



        // Validation
        this.latitude.extend({
            required: true
        });
        this.longitude.extend({
            required: true
        });
        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });
        this.radius.extend({
            required: true
        });
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            required: true,
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.country.extend({
            required: true
        });
        this.province.extend({
            required: true
        });
        this.city.extend({
            required: true
        });

        this.areaPointerror.extend({
            required: true,
        });
        this.pointerror.extend({
            serverValidate: {
                params: "ParentArea",
                message: this.i18n("M010")()
            }
        });

        this.subAreaError.extend({
            serverValidate: {
                params: "SubArea",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            code: this.code,
            lat: this.latitude,
            lon: this.longitude,
            accessibleBusinessUnits: this.accessibleBusinessUnits,
            country: this.country,
            province: this.province,
            city: this.city,
            radius: this.radius
        });
        this.validationPoints = ko.validatedObservable({
            areaPointerror: this.areaPointerror,
        });
        // this.validationSubArea = ko.validatedObservable({
        //     subArea: this.subArea,
        // });
    }
    /**
     * TODO:Delete selected item
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

    }


    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    generateModel() {
        // concat adminlevelcode by client
        var cityCode = "",
            townCode = "";
        if (this._responseIden.countryCode == "TH") {
            // case NOSTRA's data code 2 digit
            if (this._responseIden.adminLevel2Code.length == 2) {
                cityCode = this._responseIden.adminLevel1Code + this._responseIden.adminLevel2Code;
                townCode = this._responseIden.adminLevel1Code + this._responseIden.adminLevel2Code + this._responseIden.adminLevel3Code;
            } else {
                cityCode = this._responseIden.adminLevel2Code;
                townCode = this._responseIden.adminLevel3Code;
            }
        } else {
            cityCode = this._responseIden.adminLevel2Code;
            townCode = this._responseIden.adminLevel3Code;
        }

        
        let model = {
            companyId: WebConfig.userSession.currentCompanyId,
            name: this.name(),
            code: this.code(),
            description: this.description(),
            latitude: this.latitude(),
            longitude: this.longitude(),
            radius: this.radius() ? this.radius() : null,
            provinceId: this.province() ? this.province().id:null,
            provinceCode: this._responseIden.adminLevel1Code ? this._responseIden.adminLevel1Code : this.getCode(this.province()),
            cityId: this.city() && this.city().id,
            cityCode: cityCode ? cityCode : this.getCode(this.city()),
            townId: this.town() ? this.town().id : null,
            townCode: townCode ? townCode : this.getCode(this.town()),
            countryId: this.country().id,
            countryCode: this._responseIden.countryCode ? this._responseIden.countryCode : this.getCode(this.country()),
            categoryId: this.category() ? this.category().id : null,
            iconId: this.iconSelectedItem() ? this.iconSelectedItem().value : null,
            poiType: this.poiType().value,
            id: this.customPoiId
        };
        model.accessibleBusinessUnits = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleBusinessUnits,
            this.accessibleBusinessUnits()
        );
        // model.parentPoiArea = this.genParentPoiArea()
        if(this.points() && this.points().length !== 0){
            model.parentPoiArea = {
                companyId: WebConfig.userSession.currentCompanyId,
                name: this.name()? this.name() : null,
                code: this.code()? this.code() : null,
                description: this.description()? this.description() : null,
                points: JSON.stringify(this.points()),
                size:this.size()?this.size():null,
                // totalPoints: this.tmpParentArea() ? this.tmpParentArea().count : 0.0,
                centralLatitude: this.tmpParentArea() ? this.tmpParentArea().centralLatitude : 0.0,
                centralLongitude: this.tmpParentArea() ? this.tmpParentArea().centralLongitude : 0.0,
                radius: 0,
                // Type: this.typeArea(),
                mode:this.mode,
                maxLatitude: this.tmpParentArea() ? this.tmpParentArea().maxLatitude : 0.0,
                minLatitude: this.tmpParentArea() ? this.tmpParentArea().minLatitude : 0.0,
                maxLongitude:this.tmpParentArea() ? this.tmpParentArea().maxLongitude : 0.0,
                minLongitude:this.tmpParentArea() ? this.tmpParentArea().minLongitude : 0.0,
                tmpData:this.tmpParentArea() ? this.tmpParentArea():null,
                id:this.parentAreaId() ? this.parentAreaId() : 0,
            }

            if(this.tmpParentArea() && this.tmpParentArea().count){
                model.parentPoiArea.totalPoints = this.tmpParentArea() ? this.tmpParentArea().count : 0.0
            }else if(this.tmpParentArea() && this.tmpParentArea().totalPoints){
                model.parentPoiArea.totalPoints = this.tmpParentArea().totalPoints
            }
        }else{
            model.parentPoiArea = null
        }
        
        //เช็คค่ากรณีที่ เพิ่ม,อัพเดท โดยไม่ได้ใส่ Main Area,Sub Areaมา
        let tmp = _.cloneDeep(this.updateResult())
        let subPOIareaList = _.cloneDeep(this.subPOIareaList())
        if(tmp){

            let subAreaLists = [];
            if(tmp.subPoiArea.length === 0){
                ko.utils.arrayForEach(subPOIareaList, function(items, index) {
                    items.id = 0
                    subAreaLists.push(items)
                });
                model.subPoiArea = subAreaLists
            }else{
                ko.utils.arrayForEach(subPOIareaList, function(items, index) {
                    let check = _.some(tmp.subPoiArea, ['id', items.id]);
                    if(check){
                        subAreaLists.push(items)
                    }else{
                        items.id = 0
                        subAreaLists.push(items)
                    }
                    // ko.utils.arrayForEach(tmp.subPoiArea, function(itemsSubarea, index) {
                    //    console.log("items",items);
                    //    console.log("itemsSubarea",itemsSubarea);
                    //     if(items.id === itemsSubarea.id){
                    //         subAreaLists.push(items)
                    //     }else{
                    //         items.id = 0
                    //         subAreaLists.push(items)
                    //     }
                    // })

                });
                // model.subPoiArea = this.subPOIareaList() 
                model.subPoiArea = subAreaLists
            }
        }else{
            if(this.mode === Screen.SCREEN_MODE_CREATE){
                let subAreaLists = []
                ko.utils.arrayForEach(subPOIareaList, function(items, index) {
                        items.id = 0
                        subAreaLists.push(items)
                })
                model.subPoiArea = subAreaLists            
                // model.subPoiArea = this.subPOIareaList()            
            }
        }

        return model;
    }
    // genParentPoiArea(){
    //     let parentArea = null
    //     parentArea = {
    //         CompanyId: WebConfig.userSession.currentCompanyId,
    //         name: this.name()? this.name() : null,
    //         Code: this.code()? this.code() : null,
    //         Description: this.description()? this.description() : null,
    //         Points: JSON.stringify(this.points()),
    //         Size:this.displaySize()?this.displaySize():null,
    //         TotalPoints: this.tmpParentArea() ? this.tmpParentArea().count : 0.0,
    //         CentralLatitude: this.tmpParentArea() ? this.tmpParentArea().centralLatitude : 0.0,
    //         CentralLongitude: this.tmpParentArea() ? this.tmpParentArea().centralLongitude : 0.0,
    //         Radius: 0,
    //         // Type: this.typeArea(),
    //         mode:this.mode,
    //         MaxLatitude: this.tmpParentArea() ? this.tmpParentArea().maxLatitude : 0.0,
    //         MinLatitude: this.tmpParentArea() ? this.tmpParentArea().minLatitude : 0.0,
    //         MaxLongitude:this.tmpParentArea() ? this.tmpParentArea().maxLongitude : 0.0,
    //         MinLongitude:this.tmpParentArea() ? this.tmpParentArea().minLongitude : 0.0,
    //         tmpData:this.tmpParentArea() ? this.tmpParentArea():null
    //     }
    //     return parentArea
    // }
    getCode(result) {
        let code;
        if (result) {
            code = result.code
        } else {
            code = null
        }
        return code;
    }
    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            var dfdDelay = $.Deferred();
            setTimeout(function () {
                dfdDelay.resolve();
            }, 500);

            $.when(dfdDelay).done(() => {

                if (!this.validationModel.isValid()) {
                    this.validationModel.errors.showAllMessages();
                    return;
                }

                var model = this.generateModel();
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        this.isBusy(true);
                        this.webRequestCustomPOI.createCustomPOI(model).done(() => {
                            this.publishMessage("cw-geo-fencing-custom-poi-changed");
                            MapManager.getInstance().openCustomPOILayer();
                            this.close(true);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        this.isBusy(true);
                        model.infoStatus = 1
                        this.webRequestCustomPOI.updateCustomPOI(model).done(() => {
                            this.publishMessage("cw-geo-fencing-custom-poi-changed", this.customPoiId);
                            MapManager.getInstance().openCustomPOILayer();
                            this.close(true);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        break;
                }


                // let location = {
                //         lat: this.latitude(),
                //         lon: this.longitude()
                //     },
                //     radius = this.radius(),
                //     unit = 'meters',
                //     fillColor = '#9ECB89',
                //     borderColor = '#9ECB89',
                //     fillOpacity = 0.8,
                //     borderOpacity = 1;
                // // Call command.
                // MapManager.getInstance().drawPointBuffer(this.id, location, radius, unit, fillColor, borderColor, fillOpacity, borderOpacity);

            }); //

        } else if (sender.id == "actCancel") {
            // Call clear map command.
            MapManager.getInstance().clear(this.id);

        }
    }

    filterPoiType(data, filterType) {

        var filter = [];

        switch (filterType) {
            case 1:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Private;
                });
                break;
            case 2:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Public;
                });
                break;
            default:
                break;

        }
        return filter;
    }

    /**
     * Choose Accessible Business Unit
     */
    addAccessibleBusinessUnit() {
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.businessUnitId;
        });
        this.navigate("cw-geo-fencing-custom-poi-manage-accessible-business-unit-select", {
            mode: this.mode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }
    addSubPOIArea(){
    if(this.areaPoint().length > 0){
        this.navigate("cw-geo-fencing-custom-poi-manage-sub-poi-area", {
            mode: 'create',
            // selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }else{
            if(!this.areaPointerror()){
                this.areaPointerror(null)
                this.validationPoints.errors.showAllMessages();
            }
    }

    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomPOIMangeScreen),
    template: templateMarkup
};