﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./list.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOICategory";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";

class CustomPOICategoriesScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomPOICategories_Title")());
        this.bladeSize = BladeSize.Medium;
        // Set datasource to DataTable control
        this.items = ko.observableArray();
        this.order = ko.observable([
            [1, "asc"]
        ]);
        // To preserve state when journey is inactive.
        this.filterText = ko.observable();
        this.selectedCustomPOICategory = ko.observable();
        this.recentChangedRowIds = ko.observableArray();

        this._selectingRowHandler = (customPOI) => {
            if (customPOI) {
                return this.navigate("cw-geo-fencing-custom-poi-category-view", {
                    id: customPOI.id
                });
            }
            return false;
        };

        this.subscribeMessage("cw-geo-fencing-custom-poi-category-changed", (id) => {
            var param = {
                "companyId": WebConfig.userSession.currentCompanyId,
                "keyword": null
            };
            this.webRequestPOICategory.listCustomPOICategorySummary(param).done((response) => {
                var items = response["items"];
                this.items.replaceAll(items);

                this.recentChangedRowIds.replaceAll([id]);
            });
        });

        this.subscribeMessage("cw-geo-fencing-custom-poi-category-delete", (id) => {
            var param = {
                "companyId": WebConfig.userSession.currentCompanyId,
                "keyword": null
            };
            this.webRequestPOICategory.listCustomPOICategorySummary(param).done((response) => {
                var items = response["items"];
                this.items.replaceAll(items);

                this.recentChangedRowIds.replaceAll([id]);
            });
        });
    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestPOICategory() {
            return WebRequestPOICategory.getInstance();
        }
    /**
      * @lifecycle Called when View is loaded.
      * @param {boolean} isFirstLoad true if ViewModel is first load
      */
    onLoad(isFirstLoad) {
        var dfd = $.Deferred();
        var param = {
            "CompanyId": WebConfig.userSession.currentCompanyId,
            "Keyword": null
        };
        this.webRequestPOICategory.listCustomPOICategorySummary(param).done((response) => {
            var items = response["items"];
            this.items(items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }

    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateCustomPOICategory)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("CustomPOICategory_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("cw-geo-fencing-custom-poi-category-manage", {
                mode: "create"
            });

            //this.selectedSubscription(null);
        }

    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomPOICategoriesScreen),
    template: templateMarkup
};