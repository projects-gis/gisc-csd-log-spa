﻿import ko from "knockout";
import templateMarkup from "text!./vehicleInfo-widget.html";
import ScreenBaseWidget from "../../screenbasewidget";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

class VehicleInfoWidget extends ScreenBaseWidget
{
    constructor(params) {
        super(params);

        this.isShipmentDesc = ko.observable(true);
        this.shipmentDesc = ko.observable('');

        this.items = ko.observableArray([
            { key: this.i18n('vehicle-info-widget-box-id')(), value: params["RAW"]["boxSerialNo"] || '-' },
            { key: this.i18n('vehicle-info-widget-vehicle-id')(), value: params["RAW"]["vehicleId"] || '-' },
            { key: this.i18n('vehicle-info-widget-vehicle-desc')(), value: params["RAW"]["vehicleDescription"] || '-' },
            { key: this.i18n('Common_Driver')(), value: params["RAW"]["driverName"] || '-' },
            { key: this.i18n('Common_BusinessUnit')(), value: params["RAW"]["businessUnitName"] || '-' },
            { key: this.i18n('Common_JobStatus')(), value: params["RAW"]["jobStatusDisplayName"] || '-' },
            { key: this.i18n('Common_Datetime')(), value: params["RAW"]["formatDateTime"] || '-' },
            { key: this.i18n('Common_Location')(), value: params["RAW"]["location"] || '-' },


            //{ key: "Common_Movement", value: params["RAW"]["movementDisplayName"] || '-' },
            //{ key: "Common_Speed", value: params["RAW"]["speed"] || '-' },
            //{ key: "Common_Engine", value: params["RAW"]["speed"] || '-' },
        ]);

        //console.log("RAW", params["RAW"]);
        //console.log("status",params["STATUS"]);

        _.forEach(params["STATUS"], (itm) => {

            switch (itm.key) {
                case "movementDisplayName":
                case "speed":
                case "TrkVwEngine":
                case "gsmStatusDisplayName":
                    this.items.push({
                        key: itm["title"], value: itm["text"] || '-'
                    });
                    break;
                case "TrkVwGate":
                case "TrkVwFuel":
                case "TrkVwTemp":
                    if (itm["text"] != undefined && itm["text"] != null && itm["text"] != "" && itm["text"] != "-") {
                        this.items.push({
                            key: itm["title"], value: itm["text"] || '-'
                        });
                    }
                    break;
            }


            //switch (itm["cssClass"]) {
            //    case "icon-movement":
            //    case "icon-speed":
            //    case "icon-engine":
            //    case "icon-fuel":



            //this.items.push({
            //    key: itm["title"], value: itm["text"] || '-'
            //});
            //        break;
            //    default: break;
            //}

        });

        //this.items.push({ key: "vehicle-info-widget-park-time", value: params["PARK_TIME"] });
        //this.items.push({ key: "vehicle-info-widget-park-idel-time", value: params["PARK_IDLE_TIME"] });

        if (params["RAW"]["jobCode"])
        { this.items.push({ key: this.i18n('vehicle-info-widget-job-code')(), value: params["RAW"]["jobCode"] }); }

        if (params["RAW"]["jobName"])
        { this.items.push({ key: this.i18n('vehicle-info-widget-job-name')(), value: params["RAW"]["jobName"] }); }

        if (!params["RAW"]["jobDescription"]) {
            this.isShipmentDesc(false);
        }
        else {
            this.isShipmentDesc(true);
            this.shipmentDesc(params["RAW"]["jobDescription"]);
        }
    }
}

export default {
    viewModel: ScreenBaseWidget.createFactory(VehicleInfoWidget),
    template: templateMarkup
};