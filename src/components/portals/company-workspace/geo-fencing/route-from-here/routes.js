﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./routes.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestMap from "../../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";

class RoutesFromHereScreen extends ScreenBase {
    constructor(params) {
        super(params);

        //params
        this.state = this.ensureNonObservable(params.state, 'start');
        this.lat = this.ensureNonObservable(params.lat);
        this.lon = this.ensureNonObservable(params.lon);
        this.extent = this.ensureNonObservable(params.extent);

        this.maximumNode = ko.observable(50);
        
        this.countElement = ko.pureComputed(() => {
            return this.routeData().length < this.maximumNode();
        });

        this.clickIndex = ko.observable();

        this.invalidRouteData = ko.observable(false);
        
        this.routeData = ko.observableArray([
            { index: 0, name: ko.observable(""), lat: null, lon: null },
            { index: 1, name: ko.observable(""), lat: null, lon: null }
        ]);
        let arrOption = [];
        // if (WebConfig.userSession.currentUserLanguage == "en-US")
        // {
        //     arrOption.push({ displayName: "Car", value: 1});
        //     arrOption.push({ displayName: "Motorcycle", value: 2 });
        //     arrOption.push({ displayName: "Pedestrain", value: 3 });
        //     arrOption.push({ displayName: "Truck", value: 4 });
        // }
        // else
        // {
        //     arrOption.push({ displayName: "รถยนต์", value: 1 });
        //     arrOption.push({ displayName: "รถจักรยานยนต์", value: 2 });
        //     arrOption.push({ displayName: "เดินเท้า", value: 3 });
        //     arrOption.push({ displayName: "รถบรรทุก", value: 4 });
        // }

        this.routeOption = ko.observableArray(arrOption);
        this.hasRouteOption = ko.pureComputed(function() {
            return this.routeOption().length > 0
        }, this);

        this.routeMode = ko.observable(1);

        this.routeDirection = ko.observableArray([]);
        this.routeParams = ko.observableArray([]);

        this.path = ko.observableArray();
        this.distance = ko.observable();
        this.driveTime = ko.observable();
        this.displayDistance = ko.observable();
        this.displayDuration = ko.observable();

        this.hasRouteDirection = ko.pureComputed(() => {
            return this.routeDirection().length > 0;
        });

        this.bladeTitle(this.i18n("Geofencing_RouteFromHere_Title")());
        this.bladeSize = BladeSize.Small;

        this.routeParams.subscribe(this.redrawDirectionPin.bind(this));

    }

    /**
     * Get WebRequest specific for Subscription module in Web API access.
     * @readonly
     */
    get webRequestMap() {
        return WebRequestMap.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    redrawDirectionPin()
    {
        MapManager.getInstance().cancelClickMap();

        let self = this;

        //if (!this.hasRouteDirection()) {

            let drawingObj = [];

            ko.utils.arrayForEach(self.routeParams(), function (itm, index) {
                if (!itm) return;
                if (_.isNumber(itm.lat) && _.isNumber(itm.lon)) {

                    let urlPic = "~/images/";
                    if (index === (self.routeParams().length - 1)) {
                        urlPic += "pin_destinationE.png";
                    }
                    else if (index === 0) {
                        urlPic += "pin_destinationB.png";
                    }
                    else {
                        urlPic += "pin_poi_" + (index + 1).toString() + ".png";
                    }

                    let point = {
                        attributes: null,
                        location: { lat: itm.lat, lon: itm.lon },
                        symbol: {
                            url: self.resolveUrl(urlPic),
                            width: 35,
                            height: 50,
                            offset: { x: 0, y: 23.5 }
                        }
                    }

                    drawingObj.push(point);
                }
            });

            if (drawingObj.length > 0)
                MapManager.getInstance().drawMultiplePoint(this.id, drawingObj);
        //}
    }

    onMapComplete(response)
    {
        //console.log("onMapComplete");
        //console.log(response);

        var self = this;
        if (response.command == "enable-click-map") {

            var param = {
                lat: response.result.location.lat,
                lon: response.result.location.lon,
                extent: response.result.location.extent
            };

            console.log("param",param);

            this.webRequestMap.identify(param).done((responseIden) => {
                //console.log("Iden :: ", responseIden);

                let name = WebConfig.userSession.currentUserLanguage == "en-US" ? responseIden["nameEnglish"] : responseIden["nameLocal"];

                let obj = Object.assign(param, { name: ko.observable(name) });

                let newArr = [];
                let clkInd = this.clickIndex();
                ko.utils.arrayForEach(this.routeParams(), function (itm, ind) {
                    if (clkInd == ind)
                        newArr.push(obj);
                    else
                        newArr.push(itm);
                });

                this.routeData.replaceAll(newArr);
            });
        }
        
    }

    createRouteParameter()
    {
        let d = new Date();
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        let hour = d.getHours().toString();
        let minute = d.getMinutes().toString();
        let sec = d.getSeconds().toString();
        let newDateTime = null;

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        sec = sec.length > 1 ? sec : "0" + sec;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

        let param = {
            routeMode: this.routeMode(),
            routeOption: 1,
            lang: WebConfig.userSession.currentUserLanguage == "en-US" ? "E" : "L",
            stops: [],
            startDate: newDateTime
        };

        return param;
    }

    textClick(index, ele)
    {
        //console.log("AA", );
        this.clickIndex($(ele).parents('.grid-stack-item').attr('index'));
        MapManager.getInstance().enableClickMap(this.id);
    }

    addStop()
    {
        let stops = [];
        //console.log(this.routeParams());
        let count = this.routeParams().length;
        ko.utils.arrayForEach(this.routeParams(), function (itm) {
            //console.log(itm.name(), itm.lat, itm.lon);
            stops.push({ index: itm.index, name: itm.name, lat: itm.lat, lon: itm.lon });
        });

        stops.push({ index: count ,name: ko.observable(""), lat: null, lon: null });

        this.routeData.replaceAll(stops);
        //console.log(this.routeData());
    }

    findRoute()
    {
        let validateObj = {};

        ko.utils.arrayForEach(this.routeParams(), function (itm, ind) {
            validateObj[`validateName_${ind}`] = itm.name;
            validateObj[`validateLat_${ind}`] = ko.observable(itm.lat);
            validateObj[`validateLon_${ind}`] = ko.observable(itm.lon);

            validateObj[`validateName_${ind}`].extend({ required: true});
            validateObj[`validateLat_${ind}`].extend({ required: true });
            validateObj[`validateLon_${ind}`].extend({ required: true });
        });

        let validateObserv = ko.validatedObservable(validateObj);

        if (!validateObserv.isValid()) {
            this.invalidRouteData(true);
            return;
        } else {
            this.invalidRouteData(false);
        }

        let stops = [];
        //console.log(this.routeParams());
        ko.utils.arrayForEach(this.routeParams(), function (itm) {
            //console.log(itm.name(), itm.lat, itm.lon);
            stops.push({ name: itm.name(), lat: itm.lat, lon: itm.lon});
        });

        let self = this;
        let option = this.createRouteParameter();

        option.stops = stops;

        this.isBusy(true);

        this.webRequestMap.solveRoute(option).done((response) => {
            let responseSolveRoute = JSON.parse(response);
            if (responseSolveRoute != null) {
                
                self.path(responseSolveRoute.results.route);
                ko.utils.arrayForEach(responseSolveRoute.results.directions, function (items, index) {

                    items = $.extend(items, {
                        order: (index + 1)
                    });
                });

                self.routeDirection.replaceAll(responseSolveRoute.results.directions);
                self.distance(Math.round(responseSolveRoute.results.totalLength));
                self.driveTime(Math.round(responseSolveRoute.results.totalTime));
                self.displayDistance(self.formatDistance(responseSolveRoute.results.totalLength));
                self.displayDuration(self.formatTime(responseSolveRoute.results.totalTime));

                var pics = [];

                ko.utils.arrayForEach(self.routeData(), function (items, index) {
                    let urlPic = "~/images/";
                    if (index === (self.routeData().length - 1)) {
                        urlPic += "pin_destinationE.png";
                    }
                    else if (index === 0) {
                        urlPic += "pin_destinationB.png";
                    }
                    else {
                        urlPic += "pin_poi_" + (index + 1).toString() + ".png";
                    }
                    pics.push(self.resolveUrl(urlPic));
                });

                //console.log(option.stops, pics, self.path());
                self.drawRoute(option.stops, pics, self.path());

            }
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
        
    }

    drawRoute(stops, pics, path, color = "#d91515", width = 5, opacity = 1, attributes = null) {

        MapManager.getInstance().drawOneRoute(this.id, stops, pics, path, color, width, opacity, attributes);
    }

    formatTime(time) {

        let timeHour = time / 60;
        let timeMin = time % 60;
        let textTime = "";
        if (timeHour >= 1) {
            textTime = (Math.floor(timeHour)) + (WebConfig.userSession.currentUserLanguage == "en-US" ? " hr " : " ชม. ");
        }
        textTime += (Math.floor(timeMin)) + (WebConfig.userSession.currentUserLanguage == "en-US" ? " min " : " น. ");

        return textTime;
    }

    formatDistance(distance) {

        let lengthUnit = distance >= 1000 ? WebConfig.userSession.currentUserLanguage == "en-US" ? " km. " : " กม. " : WebConfig.userSession.currentUserLanguage == "en-US" ? " m. " : " ม.";
        let dist = distance > 1000 ? ((distance / 1000).toFixed(2)) : (distance).toFixed(2);

        return dist + lengthUnit;
    }

    selectMode(val)
    {
        $(".ui-route-option-mode").removeClass("active");
        $(".ui-route-option-mode.mode-" + val).addClass("active");

        this.routeMode(val);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        //console.log("loaded", isFirstLoad);

        // Register event subscribe in constructor.
        this._eventAggregator.destroy();
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        let self = this;

        MapManager.getInstance().clear(this.id);

        if (!isFirstLoad) return;

        const filter = {
            CompanyId: WebConfig.userSession.currentCompanyId,
            //types: [Enums.ModelData.EnumResourceType.RouteMode, Enums.ModelData.EnumResourceType.RouteOption]
            types: [Enums.ModelData.EnumResourceType.RouteMode]
        };
        this.webRequestEnumResource.listEnumResource(filter).done((response) => {
            // let routeOption = response["items"].filter((options) => {
            //     if (options.type === Enums.ModelData.EnumResourceType.RouteOption) {
            //         return options;
            //     }
            // });
            // let routeMode = response["items"].filter((mode) => {
            //     if (mode.type === Enums.ModelData.EnumResourceType.RouteMode) {
            //         return mode;
            //     }
            // });

            let routeMode = response["items"].map((mode) => {
                return {
                    displayName: mode["displayName"], value: mode["value"]
                };
            });

            this.routeOption.replaceAll(routeMode);
            //console.log(routeOption, routeMode);
        });

        if (_.isNumber(this.lat) && _.isNumber(this.lon)) {
            const idenOption = {
                lat: this.lat,
                lon: this.lon,
                extent: this.extent
            };

            let tmpData = [];
            if (self.state == 'start') {
                tmpData = [idenOption, { lat: null, lon: null, extent: ""}];
            }
            else {
                tmpData = [{ lat: null, lon: null, extent:""}, idenOption];
            }

            let drawingObj = [];

            ko.utils.arrayForEach(tmpData, function (items, index) {
                if (!items) return;

                let urlPic = "~/images/";
                if (index === (self.routeData().length - 1)) {
                    urlPic += "pin_destinationE.png";
                }
                else if (index === 0) {
                    urlPic += "pin_destinationB.png";
                }
                else {
                    urlPic += "pin_poi_" + (index + 1).toString() + ".png";
                }

                let point = {
                    attributes: null,
                    location: items,
                    symbol: {
                        url: self.resolveUrl(urlPic),
                        width: 35,
                        height: 50,
                        offset: { x: 0, y: 23.5 }
                    }
                }

                drawingObj.push(point);
            });

            MapManager.getInstance().drawMultiplePoint(self.id, drawingObj);

            console.log("idenOption", idenOption);

            this.webRequestMap.identify(idenOption).done((response) => {

                let name = WebConfig.userSession.currentUserLanguage == "en-US" ? response["nameEnglish"] : response["nameLocal"]

                let lstObj = self.routeData().slice();

                //console.log(lstObj);

                if (self.state == 'start') {
                    //self.routeData.replaceAll([
                    //    { name: ko.observable(name), lat: self.lat, lon: self.lon },
                    //    { name: ko.observable("Thai Building"), lat: 13.7195, lon: 100.5571 }
                    //]);
                    lstObj[0] = { name: ko.observable(name), lat: self.lat, lon: self.lon };
                }
                else {
                    //self.routeData.replaceAll([
                    //    { name: ko.observable("Sathon Thani Tower 1"), lat: 13.7227, lon: 100.5309 },
                    //    { name: ko.observable(name), lat: self.lat, lon: self.lon }
                    //]);
                    lstObj[(lstObj.length - 1)] = { name: ko.observable(name), lat: self.lat, lon: self.lon };
                }

                self.routeData.replaceAll(lstObj);
            });
        }
    }

    onUnload() {
        MapManager.getInstance().clear(this.id);
        this._eventAggregator.destroy();
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        //  this.selectedSubscription(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    /**
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {

    }
}

export default {
    viewModel: ScreenBase.createFactory(RoutesFromHereScreen),
    template: templateMarkup
};