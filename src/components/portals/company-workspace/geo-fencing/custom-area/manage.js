﻿import "jquery";
import ko from "knockout";
import _ from "lodash";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import templateMarkup from "text!./manage.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import {
    Constants,
    Enums
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import WebRequestAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomAreaCategory";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";


class CustomAreaManageScreen extends ScreenBase {

    constructor(params) {
        super(params);
        this.maxLatitude = "";
        this.maxLongitude = "";
        this.minLatitude = "";
        this.minLongitude = "";
        this.centralLatitude = "";
        this.centralLongitude = "";
        // This mode is parse from params, it can comes from both URL or NavigationService
        this.mode = this.ensureNonObservable(params.mode);

        if (this.mode === "create") {
            this.bladeTitle(this.i18n("CustomArea_CreateArea")());
        } else if (this.mode === "update") {
            this.bladeTitle(this.i18n("CustomArea_UpdateArea")());
        }

        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);

        this.categoryOptions = ko.observableArray();

        this.typeOptions = ko.observableArray([]);

        this.moveInAreaOptions = ko.observableArray();

        this.moveOutAreaOptions = ko.observableArray();

        this.areaPoint = ko.observableArray();
        this.areaId = this.ensureNonObservable(params.id);
        this.name = ko.observable();
        this.code = ko.observable();
        this.description = ko.observable();
        this.point = ko.observable();
        this.size = ko.observable();
        this.displaySize = ko.observable(0);
        this.numberofareapoint = ko.observable();
        this.type = ko.observable();
        this.moveIn = ko.observable();
        this.moveOut = ko.observable();
        this.categoryId = ko.observable();
        this.fillColor = ko.observable();
        this.borderColor = ko.observable();
        this.areaUnit = ko.observable();
        this.areaUnitType = ko.observable();
        this.areaUnitText = ko.observable();

        //Poi Type
        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea) || WebConfig.userSession.userType == Enums.UserType.BackOffice ? true : false;
        }
        else {
            this.enablePoiType = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea) ? true : false;
        }
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;

        //this._originalAccessibleBusinessUnits = [];

        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-geo-fencing-custom-area-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));
        MapManager.getInstance().setFollowTracking(false);
    }

    /**
 * Get WebRequest specific for Company module in Web API access.
 * @readonly
 */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    get webRequestAreaCategory() {
        return WebRequestAreaCategory.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    goto(extras) {
        // TODO: Uncomment this block for allow real navigation in next phase.
        this.navigate(extras.id);
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        var filter = {
            CompanyId: 1,
            types: [Enums.ModelData.EnumResourceType.EventActionType, Enums.ModelData.EnumResourceType.PoiType]
        };
        var param = {
            "CompanyId": WebConfig.userSession.currentCompanyId,
            "Keyword": ""
        };
        this._originalAccessibleBusinessUnits = [];

        var d1 = this.webRequestCustomArea.customAreaType();
        var d2 = this.webRequestAreaCategory.listCustomAreaCategorySummary(param);
        var d3 = this.webRequestEnumResource.listEnumResource(filter);

        var d4 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestCustomArea.getCustomArea(this.areaId) : null;

        var d5 = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);

        $.when(d1, d2, d3, d4, d5).done((type, list, enumItem, responseInfo, companySettings) => {
            if (_.size(type["items"]) > 0 ) {
                this.typeOptions(type["items"]);
            }
            if (list) {

                this.categoryOptions(list["items"]);
            }
            if (_.size(enumItem["items"]) > 0) {
                let enumTypeEventAction = _.filter(enumItem["items"], (item) => { return item.type == Enums.ModelData.EnumResourceType.EventActionType });
                let tmpPoiType = _.filter(enumItem["items"], (item) => { return item.type == Enums.ModelData.EnumResourceType.PoiType });

                let enumTypePoi = _.filter(tmpPoiType, (x) => {

                    if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                        return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                    }
                    else {
                        if (!WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea)) {
                            return x.value != Enums.ModelData.PoiType.Shared;
                        }
                        return x;
                    }
                });

                this.moveInAreaOptions(enumTypeEventAction);
                this.moveOutAreaOptions(enumTypeEventAction);

                this.poiTypeOptions(enumTypePoi);
            }

            if (companySettings) {
                this.areaUnitText(companySettings.areaUnitSymbol);
                this.areaUnit(this.i18n("CustomArea_Size_2", [this.areaUnitText()]));
                this.areaUnitType(companySettings.areaUnit);
            }


            if (responseInfo) {

                var typeId = !_.isNil(responseInfo.type.id) ? ScreenHelper.findOptionByProperty(this.typeOptions, "id", responseInfo.type.id) : ScreenHelper.findOptionByProperty(this.categoryOptions, "isDefault", true);
                var categoryId = !_.isNil(responseInfo.categoryId) ? ScreenHelper.findOptionByProperty(this.categoryOptions, "id", responseInfo.categoryId) : ScreenHelper.findOptionByProperty(this.categoryOptions, "isDefault", true);
                var moveInId = !_.isNil(responseInfo.insideAction) ? ScreenHelper.findOptionByProperty(this.moveInAreaOptions, "value", responseInfo.insideAction) : ScreenHelper.findOptionByProperty(this.categoryOptions, "isDefault", true);
                var moveOutId = !_.isNil(responseInfo.outsideAction) ? ScreenHelper.findOptionByProperty(this.moveOutAreaOptions, "value", responseInfo.outsideAction) : ScreenHelper.findOptionByProperty(this.categoryOptions, "isDefault", true);

                if (WebConfig.userSession.userType == Enums.UserType.BackOffice) {
                    let filterPoiType = [];
                    switch (responseInfo.poiType) {
                        case Enums.ModelData.PoiType.Public:
                            filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                            break;

                        case Enums.ModelData.PoiType.Private:
                            filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                            break;

                        case Enums.ModelData.PoiType.Shared:
                            if (responseInfo.poiUserPermissionType == 1)  // 1 is Public of PoiUserPermission , 2 is Private of PoiUserPermission
                            {
                                filterPoiType = this.filterPoiType(this.poiTypeOptions(), 1);
                            }
                            else {
                                filterPoiType = this.filterPoiType(this.poiTypeOptions(), 2);
                            }
                            break;
                    }
                    this.poiTypeOptions(filterPoiType);
                }

                var poitype = ScreenHelper.findOptionByProperty(this.poiTypeOptions, "value", responseInfo.poiType);


                this.accessibleBusinessUnits(responseInfo.accessibleBusinessUnits || []);
                this._originalAccessibleBusinessUnits = _.cloneDeep(responseInfo.accessibleBusinessUnits || []);

                this.name(responseInfo.name);
                this.type(typeId);
                this.code(responseInfo.code);
                this.description(responseInfo.description);
                this.categoryId(categoryId);
                this.moveIn(moveInId);
                this.moveOut(moveOutId);
                this.centralLatitude = responseInfo.centralLatitude;
                this.centralLongitude = responseInfo.centralLongitude;
                this.maxLatitude = responseInfo.maxLatitude;
                this.maxLongitude = responseInfo.maxLongitude;
                this.minLatitude = responseInfo.minLatitude;
                this.minLongitude = responseInfo.minLongitude;
                this.fillColor(this.rgbTohex(responseInfo.type.symbolFillColor));
                this.borderColor(this.rgbTohex(responseInfo.type.symbolBorderColor));
                this.point = JSON.parse(responseInfo.points);
                this.poiType(poitype);

                MapManager.getInstance().drawOnePolygonZoom(this.id, this.point, this.fillColor(), this.borderColor(), 0.5, 1, responseInfo);
                var point = [];
                ko.utils.arrayForEach(this.point[0], function(items, index) {
                    point.push({
                        id: (index + 1),
                        point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                    })
                });
                point.splice(point.length - 1);
                this.areaPoint.replaceAll(point);

                this.size(responseInfo.size);
                this.displaySize(Utility.convertAreaUnit(responseInfo.size, this.areaUnitType()));
                this.numberofareapoint(responseInfo.totalPoints);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    onUnload() {
        MapManager.getInstance().clear(this.id);
        MapManager.getInstance().cancelDrawingPolygon(this.id);
    }

    enableDrawArea() {

        this.minimizeAll(true);
        let companydata = {
            UserLanguage:WebConfig.userSession._prefUserLanguage,
            CompanyId:WebConfig.userSession._userSession.currentCompanyId
        }
        localStorage.setItem("companydata",JSON.stringify(companydata))

        var areaUnit = {
            type: this.areaUnitType(),
            text: this.areaUnit()
        };

        if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            MapManager.getInstance().clear(this.id);
            MapManager.getInstance().enableDrawingPolygon(this.id, this.point, areaUnit);
        } else {
            MapManager.getInstance().enableDrawingPolygon(this.id, null, areaUnit);
        }

    }
    /**
       convert hex format to a rgb color
    */
    rgbTohex(rgb) {

        let objRgb = rgb.split(",");
        return "#" + this.hex(objRgb[0]) + this.hex(objRgb[1]) + this.hex(objRgb[2]);
    }

    hex(x) {
        let hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }
    onMapComplete(response) {

        if (response.command == "enable-draw-polygon") {

            var point = [];
            ko.utils.arrayForEach(response.result.geometry.rings[0], function(items, index) {
                point.push({
                    id: (index + 1),
                    point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                })
            });
            point.splice(point.length - 1);
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                this.areaPoint.replaceAll(point);
                this.point = response.result.geometry.rings;
                this.size(response.result.area);
                this.displaySize(Utility.convertAreaUnit(response.result.area, this.areaUnitType()));
                this.numberofareapoint(response.result.count);
                this.centralLatitude = response.result.centralLatitude;
                this.centralLongitude = response.result.centralLongitude;
                this.maxLatitude = response.result.maxLatitude;
                this.maxLongitude = response.result.maxLongitude;
                this.minLatitude = response.result.minLatitude;
                this.minLongitude = response.result.minLongitude;
            } else {
                this.areaPoint.replaceAll(point);
                this.point(response.result.geometry.rings);
                this.size(response.result.area);
                this.displaySize(Utility.convertAreaUnit(response.result.area, this.areaUnitType()));
                this.numberofareapoint(response.result.count);
                this.centralLatitude = response.result.centralLatitude;
                this.centralLongitude = response.result.centralLongitude;
                this.maxLatitude = response.result.maxLatitude;
                this.maxLongitude = response.result.maxLongitude;
                this.minLatitude = response.result.minLatitude;
                this.minLongitude = response.result.minLongitude;
            }
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                MapManager.getInstance().drawOnePolygonZoom(this.id, this.point, this.fillColor(), this.borderColor(), 0.5, 1, { name : this.name()});
            }

        } else if (response.command == "close-drawing-polygon") {
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                MapManager.getInstance().drawOnePolygonZoom(this.id, this.point, this.fillColor(), this.borderColor(), 0.5, 1, { name : this.name()});
            }
        } else if (response.command == "cancel-drawing-polygon") {
            if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                MapManager.getInstance().drawOnePolygonZoom(this.id, this.point, this.fillColor(), this.borderColor(), 0.5, 1, { name : this.name()});
            }
        }
    }

    setupExtend() {
        this.name.extend({
            trackChange: true
        });
        this.type.extend({
            trackChange: true
        });
        this.areaPoint.extend({
            trackChange: true
        });
        this.code.extend({
            trackChange: true
        });
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });
        this.code.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });
        this.type.extend({
            required: true
        });
        this.areaPoint.extend({
            required: true
        });

        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });

        this.accessibleBusinessUnits.extend({
            trackChange: true
        });

        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            accessibleBusinessUnits: this.accessibleBusinessUnits,
            type: this.type,
            areaPoint: this.areaPoint
        });
    }

    /**
     * 
     * Delete data
     * @param {any} commands
     */
    buildCommandBar(commands) {
        //if (this.mode === "update") {
        //    commands.push(this.createCommand("cmdDelete", "Delete", "svg-cmd-delete"));
        //}
    }

    /**
     * TODO:Delete selected item
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    generateModel() {
        if (this.mode === "create") {
            var model = {
                Name: this.name() ? this.name() : null,
                Description: this.description() ? this.description() : null,
                Code: this.code() ? this.code() : null,
                CompanyId: WebConfig.userSession.currentCompanyId,
                Size: this.size() ? this.size() : null,
                Points: JSON.stringify(this.point()),
                TotalPoints: this.numberofareapoint(),
                CentralLatitude: this.centralLatitude ? this.centralLatitude : 0.0,
                CentralLongitude: this.centralLongitude ? this.centralLongitude : 0.0,
                Radius: 0,
                InsideAction: this.moveIn() ? this.moveIn().value : null,
                OutsideAction: this.moveOut() ? this.moveOut().value : null,
                CategoryId: this.categoryId() ? this.categoryId().id : null,
                TypeId: this.type().id ? this.type().id : null,
                MaxLatitude: this.maxLatitude ? this.maxLatitude : 0.0,
                MinLatitude: this.minLatitude ? this.minLatitude : 0.0,
                MaxLongitude: this.maxLongitude ? this.maxLongitude : 0.0,
                MinLongitude: this.minLongitude ? this.minLongitude : 0.0,
                PoiType: this.poiType().value
            };
        } else if (this.mode === "update") {
            var model = {
                id: this.areaId,
                Name: this.name() ? this.name() : null,
                Description: this.description() ? this.description() : null,
                Code: this.code() ? this.code() : null,
                CompanyId: WebConfig.userSession.currentCompanyId,
                Size: this.size() ? this.size() : null,
                Points: JSON.stringify(this.point),
                TotalPoints: this.numberofareapoint(),
                CentralLatitude: this.centralLatitude ? this.centralLatitude : 0.0,
                CentralLongitude: this.centralLongitude ? this.centralLongitude : 0.0,
                Radius: 0,
                InsideAction: this.moveIn() ? this.moveIn().value : null,
                OutsideAction: this.moveOut() ? this.moveOut().value : null,
                CategoryId: this.categoryId() ? this.categoryId().id : null,
                TypeId: this.type().id ? this.type().id : null,
                MaxLatitude: this.maxLatitude ? this.maxLatitude : 0.0,
                MinLatitude: this.minLatitude ? this.minLatitude : 0.0,
                MaxLongitude: this.maxLongitude ? this.maxLongitude : 0.0,
                MinLongitude: this.minLongitude ? this.minLongitude : 0.0,
                PoiType: this.poiType().value
            };
        }

        model.accessibleBusinessUnits = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleBusinessUnits,
            this.accessibleBusinessUnits()
        );

        return model;

    }

    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            //Show dialogBox messages for delete item
            this.displayDialog(null, this.i18n("M011")(), BladeDialog.DIALOG_OKCANCEL, this.deleteItem.bind(this)); //TODO:i18n M011
        }
    }

    deleteItem(button) {
        switch (button) {
            case BladeDialog.BUTTON_OK:
                // TODO: At this stage should use WebRequest to delete entity with WebAPI
                // How to do error handling?

                this.webRequestSubscription.deleteBackOfficeSubscription(this.subscriptionId).done((subscription) => {
                    this.publishMessage("bo-subscription-change");
                    this.close(true); // This close the whole journey, ask loy how to close blade individually?
                }).fail((msg) => {
                    console.log(msg);
                });
                break;
        }
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = this.generateModel();

            switch (this.mode) {
                case "create":
                    this.isBusy(true);
                    this.webRequestCustomArea.createCustomArea(model).done((response) => {
                        this.publishMessage("cw-geo-fencing-custom-area-changed");
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
                case "update":
                    this.isBusy(true);
                    this.webRequestCustomArea.updateCustomArea(model).done((response) => {
                        this.publishMessage("cw-geo-fencing-custom-area-changed", this.areaId);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                    }).always(() => {
                        this.isBusy(false);
                    });
                    break;
            }

        }
    }


    filterPoiType(data, filterType) {

        var filter = [];

        switch (filterType) {
            case 1:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Private;
                });
                break;
            case 2:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Public;
                });
                break;
            default:
                break;

        }
        return filter;
    }


    filterPoiType(data, filterType) {

        var filter = [];

        switch (filterType) {
            case 1:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Private;
                });
                break;
            case 2:
                filter = _.filter(data, (x) => {
                    return x.value != Enums.ModelData.PoiType.Public;
                });
                break;
            default:
                break;

        }
        return filter;
    }
        /**
     * Choose Accessible Business Unit
     */
    addAccessibleBusinessUnit() {
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.businessUnitId;
        });
        this.navigate("cw-geo-fencing-custom-area-manage-accessible-business-unit-select", {
            mode: this.mode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomAreaManageScreen),
    template: templateMarkup
};