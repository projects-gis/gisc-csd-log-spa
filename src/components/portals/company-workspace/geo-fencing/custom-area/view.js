﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../../controls/gisc-chrome/map/mapManager";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";

class CustomAreaViewScreen extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomArea_View")());
        this.bladeSize = BladeSize.Medium;
        this.currentData = null;

        this.areaId = this.ensureNonObservable(params.id);
        this.name = ko.observable();
        this.description = ko.observable();
        this.category = ko.observable();
        this.createdDate = ko.observable();
        this.createdBy = ko.observable();
        this.lastUpdatedDate = ko.observable();
        this.lastUpdatedBy = ko.observable();
        this.size = ko.observable();
        this.numberofareapoint = ko.observable();
        this.moveoutarea = ko.observable();
        this.moveinarea = ko.observable();
        this.code = ko.observable();
        this.type = ko.observable();
        this.obj = ko.observable();
        this.areaPoint = ko.observableArray();
        this.fillColor = ko.observable();
        this.borderColor = ko.observable();
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.areaUnit = ko.observable('');
        this.areaUnitType = ko.observable(1);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);

        this.poiType = ko.observable();
        this.poiUserPermissionType = ko.observable();
        this.poiTypeEnum = ko.observable();

        this.subscribeMessage("cw-geo-fencing-custom-area-changed", (id) => {
            this.webRequestCustomArea.getCustomArea(id).done((response) => {
                this.currentData = response;
                let category = response.category != null ? response.category.name : response.category;
                this.name(response.name);
                this.description(response.description);
                this.category(category);
                this.createdDate(response.formatCreateDate);
                this.createdBy(response.createBy);
                this.lastUpdatedDate(response.formatUpdateDate);
                this.lastUpdatedBy(response.updateBy);
                this.size(Utility.convertAreaUnit(response.size, this.areaUnitType()));
                this.numberofareapoint(response.totalPoints);
                this.moveoutarea(response.outsideActionDisplayName);
                this.moveinarea(response.insideActionDisplayName);
                this.code(response.code);
                this.type(response.type.displayName);
                this.fillColor(this.rgbTohex(response.type.symbolFillColor));
                this.borderColor(this.rgbTohex(response.type.symbolBorderColor));
                this.obj = JSON.parse(response.points);
                this.poiType(response.poiTypeDisplayName);
                this.poiUserPermissionType(response.poiUserPermissionType);
                this.poiTypeEnum(response.poiType);
                var point = [];
                ko.utils.arrayForEach(this.obj[0], function(items, index) {
                    point.push({
                        id: (index + 1),
                        point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                    })
                });
                point.splice(point.length - 1);
                this.areaPoint.replaceAll(point);

                let selectedBU = response.accessibleBusinessUnits || [];
                var mappedSelectedBUs = selectedBU.map((bu) => {
                    let mbu = bu;
                    mbu.businessUnitId = bu.businessUnitId;
                    mbu.businessUnitName = bu.businessUnitName;
                    return mbu;
                });
                this.accessibleBusinessUnits(mappedSelectedBUs);
            })
        });

    }


    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
            if (!isFirstLoad) return;
            var dfd = $.Deferred();


            var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);
            var dfdCustomAreaList = this.webRequestCustomArea.getCustomArea(this.areaId);

            $.when(dfdCompanySettings, dfdCustomAreaList).done((r1,r2) => {
                this.areaUnit(this.i18n("CustomArea_Size_2", [r1.areaUnitSymbol]));
                this.areaUnitType(r1.areaUnit);

                this.currentData = r2;
                let category = r2.category != null ? r2.category.name : r2.category;
                this.name(r2.name);
                this.description(r2.description);
                this.category(category);
                this.createdDate(r2.formatCreateDate);
                this.createdBy(r2.createBy);
                this.lastUpdatedDate(r2.formatUpdateDate);
                this.lastUpdatedBy(r2.updateBy);
                this.size(Utility.convertAreaUnit(r2.size, this.areaUnitType()));
                this.numberofareapoint(r2.totalPoints);
                this.moveoutarea(r2.outsideActionDisplayName);
                this.moveinarea(r2.insideActionDisplayName);
                this.code(r2.code);
                this.type(r2.type.displayName);
                this.fillColor(this.rgbTohex(r2.type.symbolFillColor));
                this.borderColor(this.rgbTohex(r2.type.symbolBorderColor));
                this.obj = JSON.parse(r2.points);
                this.poiType(r2.poiTypeDisplayName);
                this.poiUserPermissionType(r2.poiUserPermissionType);
                this.poiTypeEnum(r2.poiType);
                var point = [];
                ko.utils.arrayForEach(this.obj[0], function (items, index) {
                    point.push({
                        id: (index + 1),
                        point: items[0].toFixed(5) + "," + items[1].toFixed(5)
                    })
                });
                point.splice(point.length - 1);
                this.areaPoint.replaceAll(point);

                let selectedBU = r2.accessibleBusinessUnits || [];
                var mappedSelectedBUs = selectedBU.map((bu) => {
                    let mbu = bu;
                    mbu.businessUnitId = bu.businessUnitId;
                    mbu.businessUnitName = bu.businessUnitName;
                    return mbu;
                });
                this.accessibleBusinessUnits(mappedSelectedBUs);

                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });


            return dfd;
        }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
            MapManager.getInstance().clear(this.id);
            MapManager.getInstance().cancelDrawingPolygon(this.id);
        }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateCustomArea) && 
            (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        )
        {
            if (this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) { // check ว่าเป็น Shared ไหม
                if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea)) { // เช็คว่า มี Permission Shared ไหม
                    commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
                }
            }
            else {
                commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));

            }
        }

        commands.push(this.createCommand("cmdViewOnMap", this.i18n("Common_ViewOnMap")(), "svg-cmd-search"));

        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteCustomArea) &&
            (WebConfig.userSession.userType == Enums.UserType.BackOffice || WebConfig.userSession.permissionType == this.poiUserPermissionType())
        )
        {
            if(this.poiTypeEnum() == Enums.ModelData.PoiType.Shared) {
                if (WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
            }
            else {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));

            }
        }

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
            super.onActionClick(sender);
        }
    /**
      * @lifecycle Hangle when command on CommandBar is clicked.
      * @param {any} sender
      */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("cw-geo-fencing-custom-area-manage", {
                    mode: "update",
                    id: this.areaId
                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M051")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestCustomArea.deleteCustomArea(this.areaId).done(() => {
                                this.publishMessage("cw-geo-fencing-custom-area-delete");
                                this.close(true);
                            }).fail((e) => {
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
            case "cmdViewOnMap":
                MapManager.getInstance().drawOnePolygonZoom(this.id, this.obj, this.fillColor(), this.borderColor(), 0.5, 1, this.currentData);
                this.minimizeAll();
                break;
        }
    }
    printMapAndDirection() {}
    /**
      convert hex format to a rgb color
      */
    rgbTohex(rgb) {

        let objRgb = rgb.split(",");
        return "#" + this.hex(objRgb[0]) + this.hex(objRgb[1]) + this.hex(objRgb[2]);
    }

    hex(x) {
        let hexDigits = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f");
        return isNaN(x) ? "00" : hexDigits[(x - x % 16) / 16] + hexDigits[x % 16];
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomAreaViewScreen),
    template: templateMarkup
};