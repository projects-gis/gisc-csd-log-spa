﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {
    Enums, Constants
} from "../../../../../app/frameworks/constant/apiConstant";

class CustomAreaFilterScreen extends ScreenBase {

    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("CustomArea_Filters_Title")());

        this.typeOptions = ko.observableArray();

        this.dateFormat = ko.observable("yyyy-MM-dd");
        this.createDateFrom = ko.observable();
        this.createDateTo = ko.observable();
        this.createBy = ko.observable();
        this.filterState = Utility.extractParam(params.filterState, null);
        this.type = ko.observable();

        this.poiTypeOptions = ko.observableArray();
        this.poiType = ko.observable();
        this.isPermissionTypePublic = WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;
    }

    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        var r1 = this.webRequestCustomArea.customAreaType()
        var r2 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.PoiType]
        });

        $.when(r1, r2).done((response, response2) => {
            this.typeOptions(response["items"]);

            let tmpPoiType = _.filter(response2["items"], (x) => {
                //let checkPermissionShared = WebConfig.userSession.hasPermission(Constants.Permission.SharedCustomArea);

                if (WebConfig.userSession.userType != Enums.UserType.BackOffice) {
                    return this.isPermissionTypePublic ? x.value != Enums.ModelData.PoiType.Private : x.value != Enums.ModelData.PoiType.Public;
                    //if (this.isPermissionTypePublic) {
                    //    return checkPermissionShared ? x.value != Enums.ModelData.PoiType.Private : x.value == Enums.ModelData.PoiType.Public;
                    //}
                    //else {
                    //    return checkPermissionShared ? x.value != Enums.ModelData.PoiType.Public : x.value == Enums.ModelData.PoiType.Private;
                    //}
                }
                return x;
                //else {
                //    //if (!checkPermissionShared) {
                //    //    return x.value != Enums.ModelData.PoiType.Shared;
                //    //}
                //    return x;
                //}

            });
            this.poiTypeOptions(tmpPoiType);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    onUnload() {}


    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Apply")()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            this.filterState.CustomAreaTypeIds = this.type() ? this.type().id : [];
            this.filterState.FromCreateDate = this.createDateFrom() ? this.createDateFrom() : null;
            this.filterState.ToCreateDate = this.createDateTo() ? this.createDateTo() : null;
            this.filterState.CreateBy = this.createBy() ? this.createBy() : "";
            this.filterState.poiType = this.poiType() ? this.poiType().value : null;
            this.publishMessage("cw-geo-fencing-custom-area-filter-changed", this.filterState);
            //this.close(true);
        }
    }

    onCommandClick(sender) {

        if (sender.id === "cmdClear") {
            this.type(null);
            this.createDateFrom("");
            this.createDateTo("");
            this.createBy("");
            this.poiType(null);
        }
    }


}

export default {
    viewModel: ScreenBase.createFactory(CustomAreaFilterScreen),
    template: templateMarkup
};