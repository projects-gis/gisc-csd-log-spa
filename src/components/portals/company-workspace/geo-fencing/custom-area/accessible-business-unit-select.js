﻿import ko from "knockout";
import templateMarkup from "text!./accessible-business-unit-select.html";
import ScreenBase from "../../../screenBase";
import ScreenHelper from "../../../screenhelper";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {
    Constants,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";

class CustomAreaAccessibleBusinessUnitSelectScreen extends ScreenBase {

    /**
     * Creates an instance of AccessibleBusinessUnitSelectScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);

        this.bladeTitle(this.i18n("User_AccessibleBusinessUnits")());

        this.includeSubBusinessUnitsDS = ScreenHelper.createYesNoObservableArray();
        this.includeSubBusinessUnits = ko.observable();
        this.businessUnits = ko.observableArray();
        this.selectedBusinessUnits = ko.observableArray();
        this.collapsedNodes = ko.observableArray();
        this.disableNodes = ko.observableArray();

        // Internal member
        this._selectedBusinessUnitIds = this.ensureNonObservable(params.selectedBusinessUnitIds, []);
        this._selectedBusinessUnitObjects = [];
        this._selectedBusinessUnitsSubscribe = this.selectedBusinessUnits.subscribe((businessUnitIds) => {
            this._selectedBusinessUnitIds = businessUnitIds;

            var selectedObjects = [];
            _.each(businessUnitIds, (buId) => {
                selectedObjects.push(_.find(this._flattenBusinessUnits, (fbu) => {
                    return buId == fbu.id;
                }));
            });
            this._selectedBusinessUnitObjects = selectedObjects;
        });

        this._flattenBusinessUnits = [];
        this._businessUnitsSubscribe = this.businessUnits.subscribe((bus) => {
            this._flattenBusinessUnits = this._unionBusinessUnits(bus);
            this.disableNodes(this._getNotAccessibleBusinessUnitIds(this._flattenBusinessUnits));
        });
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        // Set default behavior of include sub business units.
        this.includeSubBusinessUnits(ScreenHelper.findOptionByProperty(this.includeSubBusinessUnitsDS, "value", false));

        // Calling web services for available Business Units.
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
                EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        };
        this.webRequestBusinessUnit.listBusinessUnitSummary(filter).done((response) => {
            this.businessUnits(response["items"]);

            // If there is _selectedBusinessUnitIds, set pre-selected value
            _.each(this._selectedBusinessUnitIds, (buId) => {
                let match = _.find(this._flattenBusinessUnits, (bu) => {
                    return bu.id == buId;
                });
                if (match) {
                    this.selectedBusinessUnits.push(match.id);
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        }).always(() => {

        });

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Setup track change.
        this.selectedBusinessUnits.extend({
            trackArrayChange: true
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actChoose", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) { }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actChoose") {
            this.publishMessage("cw-geo-fencing-custom-area-manage-accessible-business-unit-selected", this._selectedBusinessUnitObjects);
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) { }


    /**
     * Flatten BU by recusively add child collection then return as one.
     * 
     * @private
     * @param {any} businessUnits
     * @param {number} [level=0]
     * @returns
     */
    _unionBusinessUnits(businessUnits, level = 0, formattedParentPath = null) {
        var union = [];
        _.each(businessUnits, (bu) => {
            let buItem = {
                code: bu.code,
                companyId: bu.companyId,
                id: bu.id,
                infoStatus: bu.infoStatus,
                name: bu.name,
                parentBusinessUnitId: bu.parentBusinessUnitId,
                parentBusinessUnitPath: bu.parentBusinessUnitPath,
                businessUnitPath: _.isNil(formattedParentPath) ? bu.name : formattedParentPath + " > " + bu.name,
                isAccessible: bu.isAccessible
            };
            union.push(buItem);
            if (bu.childBusinessUnits && (bu.childBusinessUnits.length > 0)) {
                let children = this._unionBusinessUnits(bu.childBusinessUnits, level + 1, buItem.businessUnitPath);
                union = _.union(union, children);
            }
        });
        return union;
    }

    /**
     * Map BU that unable to access by current user
     * 
     * @param {any} flattenBUs
     * @returns
     */
    _getNotAccessibleBusinessUnitIds(flattenBUs) {
        var result = _.filter(flattenBUs, (bu) => {
            return !bu.isAccessible;
        });
        return _.map(result, (bu) => {
            return bu.id;
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(CustomAreaAccessibleBusinessUnitSelectScreen),
    template: templateMarkup
};