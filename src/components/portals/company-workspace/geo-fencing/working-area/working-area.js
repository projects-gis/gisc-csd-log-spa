﻿import ko from "knockout";
import templateMarkup from "text!./working-area.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WorkingMapManager from "../../../../../components/controls/gisc-chrome/map/workingMapManager";
import * as EventAggregatorConstant from "../../../../../app/frameworks/constant/eventAggregator";

class WorkingAreaScreen extends ScreenBase
{
    constructor(params)
    {
        super(params);

        this.bladeTitle(this.i18n("Working_Area_Title")());

        this.bladeMargin = BladeMargin.Normal;
        this.bladeSize = BladeSize.Large;
        this.bladeIsMaximized = true;
        this._eventAggregator.subscribe(EventAggregatorConstant.MAP_COMMAND_COMPLETE, this.onMapComplete.bind(this));

        this.resultPoltgon = ko.observable(null);
    }

    createPolygon() {
        WorkingMapManager.getInstance().drawPolygonFromPolyLine();
    }

    getResultPolygon() {
        WorkingMapManager.getInstance().getResultPolygon();
    }

    displayResultPolygon() {
        console.log('ResultPolygon :: ', this.resultPoltgon());
    }

    onMapComplete(data) {
        
        switch (data.command)
        {
            case "wm-get-result-polygon-completed":
                this.resultPoltgon(data.result);
                break;
            default:
                break;
        }
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) 
    {

    }

    onUnload() {}

    buildActionBar(actions) {
    
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCancel", "Drow Polygon", "svg-cmd-clear"));
        commands.push(this.createCommand("cmdCancel2", "Get Result Polygon", "svg-cmd-clear"));
        commands.push(this.createCommand("cmdCancel3", "Display Result Polygon", "svg-cmd-clear"));
        commands.push(this.createCommand("cmdCancel4", "Clear Graphics", "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdCancel":
                this.createPolygon();
                break;
            case "cmdCancel2":
                this.getResultPolygon();
                break;
            case "cmdCancel3": 
                this.displayResultPolygon();
                break;
            case "cmdCancel4":
                WorkingMapManager.getInstance().clearGraphics();
                break;
            default:
                break;
        }
    }
        
}

export default {
    viewModel: ScreenBase.createFactory(WorkingAreaScreen),
    template: templateMarkup
};