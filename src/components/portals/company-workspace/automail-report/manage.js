﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenHelper from "../../screenhelper";
import ScreenBase from "../../screenbase";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../app/frameworks/core/utility";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

import WebRequestAutoMailReport from "../../../../app/frameworks/data/apitrackingcore/webRequestAutoMailReport"
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import moment from "moment";
import Navigation from "../../../controls/gisc-chrome/shell/navigation";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class AutoMailReportMenuScreen extends ScreenBase {
    constructor(params){
        super(params);
        this.parameters = ko.observable(params.dataToSender.reportSource.parameters)
        this.getId = ko.observable(params.dataToSender.reportSource.parameters.id)
        this.bladeSize = BladeSize.Medium;
        this.bladeIsMaximized = false;
        this.mode = ko.observable(params.mode)
        if(params.mode === "create"){
            this.bladeTitle(this.i18n("Menu_Auto_Mail_Report_Create")());
        }else{
            this.bladeTitle(this.i18n("Menu_Auto_Mail_Report_Update")());
        }

        this.isShowEmailCompany = ko.observable(true);
        this.isSendEmail = ko.observable(false);
        this.isFileServer = ko.observable(false);

        this.isShowEmail = ko.observable(false)
        this.isText = ko.observable(1)

        this.mailSubject = ko.observable();
        this.mailTo = ko.observable();
        this.mailBody = ko.observable();

        this.ddSchedulerlist = ko.observableArray();
        this.scheduler = ko.observable();
        
        this.RadMon = ko.observable("1");
        this.RadTue = ko.observable("2");
        this.RadWed = ko.observable("3");
        this.RadThu = ko.observable("4");
        this.RadFri = ko.observable("5");
        this.RadSat = ko.observable("6");
        this.RadSun = ko.observable("7");

        this.ValueDay = ko.observable("1");

        this.importValidationError = ko.observable("")

        this.currentDate = ko.observable(Utility.addDays(new Date(), 1))

        this.startDate = ko.observable(Utility.addDays(new Date(), 1))
        this.endDate = ko.observable("");

        this.noEndData = ko.observable("noEndData");
        this.radioEndDate = ko.observable("endDate");
        this.valueEndDate = ko.observable("noEndData");
        this.isShowEndDate = ko.observable(false);

        this.valueTypeday = ko.observable("Sameday");
        this.isShowTypeday = ko.observable(true);
        this.isShowTypedayMultipleday = ko.observable(false);

        this.startTime = ko.observable("00:00");
        this.endTime = ko.observable("00:00");
        
        
        this.langEng = ko.observable("en-US");
        this.langTh = ko.observable("th-TH");
        this.valueLang = ko.observable("en-US");

        this.isShowDaily = ko.observable(false);
        this.isShowMonthly = ko.observable(false);
        this.isShowWeekly = ko.observable(false);

        this.firstLoad = ko.observable(false);

        this.startDate.subscribe((stDate)=>{
            if(this.valueEndDate() !== "noEndData"){
                let inDate = new Date(stDate)
                this.endDate(Utility.addDays(inDate, 1))
            }
        })
        this.isSendEmail.subscribe((check)=>{
            this.isShowEmail(check)
        })
        this.valueEndDate.subscribe((enddate)=>{
            this.isShowEndDate(enddate === "noEndData"?false:true)
            if(!this.firstLoad()){
                if(enddate === "noEndData"){
                    this.endDate("")
                }else{
                    this.endDate(Utility.addDays(this.startDate(), 1))
                }
            }
        })
        this.valueTypeday.subscribe((typedate)=>{
            this.isShowTypeday(typedate === "Sameday"?true:false)
            this.isShowTypedayMultipleday(typedate === "Multipleday"?true:false)
            if(typedate === "Sameday"){
                this.startTime("00:00");
                this.endTime("00:00")
            }else{
                this.startTime("12:00");
                this.endTime("12:00")
            }
        })
        this.scheduler.subscribe((scheduler)=>{
            this.isShowDaily(false)
            this.isShowMonthly(false)
            this.isShowWeekly(false)                

            switch (scheduler.value) {
                case 1:
                    if(this.parameters().reportCategory !== 3){
                        this.isShowDaily(true)
                    }
                    break;
                case 2:
                    this.isShowWeekly(true)
                    break;
                case 3:
                default:
                    break;
            }
        })
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        this.firstLoad(isFirstLoad)

        let dfd = $.Deferred();
        let dfdCompanySetting = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);

        let dfdEnumScheduleFrequency = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.ScheduleFrequency]
        });

        $.when(dfdCompanySetting,dfdEnumScheduleFrequency).done((responseCompanySetting,responseScheduleFrequency)=>{
            this.isShowEmailCompany(responseCompanySetting.enableAutomailFTP) 
            if(!responseCompanySetting.enableAutomailFTP){
                this.isSendEmail(true)
            }
            this.ddSchedulerlist.replaceAll(responseScheduleFrequency.items)

           

            if(this.mode() === "update"){
                this.isBusy(true);

                this.webRequestAutoMailReport.getAutoReport(this.getId()).done((res) => {
                    switch (res.outputType) {
                        case "b":
                                this.isSendEmail(true)
                                this.isFileServer(true)
                            break;
                        case "e":
                                this.isSendEmail(true)                    
                                break;
                        case "f":
                                this.isFileServer(true)            
                                break;
                        default:
                            break;
                    }
    
                    if(this.isSendEmail() && this.isShowEmail()){
                        this.mailSubject(res.mailSubject);
                        this.mailTo(res.mailTo);
                        this.mailBody(res.mailBody);
                    }
                    this.scheduler(ScreenHelper.findOptionByProperty(this.ddSchedulerlist, "value", res.scheduleFrequency))
                    this.ValueDay(res.weeklyDayName.toString())

                    this.startDate(new Date(res.startDate))

                    if(!res.noEndDate){
                        this.endDate(new Date(res.endDate))
                    }
                    
                    if(res.scheduleFrequency === 1){
                        this.valueTypeday(res.dailyConditionTime === "y"?"Multipleday":"Sameday")
                        this.startTime(res.dailyStartTime)
                        this.endTime(res.dailyEndTime)
                    }
                    this.valueLang(res.reportLanguage)
                    
                    this.parameters(res)
                    this.valueEndDate(res.noEndDate?"noEndData":"endDate")

                    dfd.resolve();
                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                    this.firstLoad(false)

                });
                
            }else{
                dfd.resolve();
            }
        })

        
       
    return dfd;


    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        this.publishMessage("cw-automail-manage-close", {status:true});        
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
   
    setupExtend() {
        var self = this;

         this.startDate.extend({ trackChange: true });
         this.startTime.extend({ trackChange: true });
         this.mailSubject.extend({ trackChange: true });
         this.mailTo.extend({ trackChange: true });
         this.mailBody.extend({ trackChange: true });


        this.mailSubject.extend({
            required: true     
        })
        this.mailTo.extend({
            required: true,      
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("M064")()
            },   
        })
        this.mailBody.extend({
            required: true,            
        })

        this.isFileServer.extend({
            // required: true,
            validation: {
                validator: function (val) {
                    let isval = true
                    if(self.isShowEmailCompany() && self.isSendEmail() === false){
                        isval = val
                    }
                    return isval;
                },
                message: this.i18n("M001")()
            }
        })

        this.startDate.extend({
            required: true,
        })
        this.endDate.extend({
            // required: true,
            validation: {
                validator: function (val) {
                    let isval = true
                    if(self.valueEndDate() !== "noEndData"){
                        if(!val){
                            isval = false
                        }
                    }
                    return isval;
                },
                message: this.i18n("M001")()
            }
        })
        this.startTime.extend({
            required: true,
        })
        
        this.endTime.extend({
            required: true,
        })
        this.endTime.extend({
            // required: true,
            validation: {
                validator: function (val) {
                    let invalidDiffDate = true;
                    if (self.startTime() != null) {
                        if(val !== "00:00"){  
                            self.importValidationError("");

                            let strStartDateTime = moment(self.startTime(),'HH:mm');
                            let strEndDateTime = moment(val,'HH:mm');
                                if(self.valueTypeday() === "Multipleday"){
                                    strStartDateTime.subtract(1,'day')
                                }
                            let dateEnd = moment(strEndDateTime, "YYYY/MM/DD HH:mm");
                            let dateStart = moment(strStartDateTime, "YYYY/MM/DD HH:mm");
    
                            let diffHours = moment.duration(dateEnd.diff(dateStart)).asHours();
                            self.isText(diffHours)
                            invalidDiffDate = diffHours < 8 || diffHours > 24 ? false : true;
                        }
                    }
                    return invalidDiffDate;
                },
                message: function () {
                    let mess = null
                    if(self.valueTypeday() === "Multipleday"){
                        if(self.isText() < 8){
                            mess = self.i18n("M264")()
                        }else if(self.isText() > 24){
                            mess = self.i18n("M265")()
                        }
                    }else{
                        if(self.isText() < 8 && self.isText() != 0){
                        mess = self.i18n("M264")()
                        }
                    }
                    return mess
                }
            }
        });

        

        this.validationEmail = ko.validatedObservable({
                mailSubject:this.mailSubject,
                mailTo:this.mailTo,
                mailBody:this.mailBody
        })

        this.validationDaily = ko.validatedObservable({
            endTime:this.endTime,
        })

        this.validationCheckboxEmailis = ko.validatedObservable({
            isSendEmail: this.isSendEmail,
            isFileServer: this.isFileServer
        });

        this.validationModel = ko.validatedObservable({
            startDate: this.startDate,
        });
        this.validationEndModel = ko.validatedObservable({
            endDate: this.endDate,
        });
        this.validationTimeModel = ko.validatedObservable({
            startTime: this.startTime,
            endTime: this.endTime
        });
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            if(this.scheduler().value === 1 && this.parameters().reportCategory !== 3){
                if (!this.validationDaily.isValid()) {
                    this.validationDaily.errors.showAllMessages();
                    return;
                }
                
                let strStartDateTime = moment(this.startTime(),'HH:mm');
                let strEndDateTime = moment(this.endTime(),'HH:mm');
                let dateEnd = moment(strEndDateTime, "YYYY/MM/DD HH:mm");
                let dateStart = moment(strStartDateTime, "YYYY/MM/DD HH:mm");
                
                let diffHours = moment.duration(dateEnd.diff(dateStart)).asHours();
                let invalidDiffDate = diffHours < 8 || diffHours > 24 ? false : true;

                if(this.valueTypeday() !== "Multipleday"){
                    if(!invalidDiffDate && diffHours != 0){
                        this.importValidationError(this.i18n("M264")());
                        return;
                    }else{
                        this.importValidationError("");

                    }
                }
                
            }


            if(this.isSendEmail()){
                if (!this.validationEmail.isValid()) {
                    this.validationEmail.errors.showAllMessages();
                    return;
                }
            }
            if(this.isShowEmailCompany()){
                if (!this.validationCheckboxEmailis.isValid()) {
                    this.validationCheckboxEmailis.errors.showAllMessages();
                    return;
                }          
            }

         
            if(this.valueEndDate() !== "noEndData"){
                if (!this.validationEndModel.isValid()) {
                    this.validationEndModel.errors.showAllMessages();
                    return;
                }
            }

            if(this.isShowDaily()){
                if (!this.validationTimeModel.isValid()) {
                    this.validationTimeModel.errors.showAllMessages();
                    return;
                }
            }
            
            let data = this.genDataToSave();

            let dfd = $.Deferred();
            this.isBusy(true);

            if(this.mode() === "create"){
                let dfdDupicate = this.webRequestAutoMailReport.checkDupicate(data)
                $.when(dfdDupicate).done((responseDupicate)=>{
                    if(responseDupicate){
                        this.showMessageBox(null, this.i18n("M258")(), BladeDialog.DIALOG_YESNO).done((button) => {
                            switch (button) {
                                case BladeDialog.BUTTON_YES:
                                    data.id = responseDupicate?responseDupicate:null
                                    this.webRequestAutoMailReport.createAutoReport(data).done((res) => {
                                    this.publishMessage("cw-auto-mail-report", this.parameters().id);
                                     this.close(true);
                                   // this.closeAll();
                                   // Navigation.getInstance().navigate("cw-automail-report-manage-list", {}); 

                                    }).fail((e) => {
                                        this.handleError(e);
                                    }).always(() => {
                                        this.isBusy(false);
                                    });
                                    break;
                            }
                        });
    
                    }else{
                        this.webRequestAutoMailReport.createAutoReport(data).done((res) => {
                            this.publishMessage("cw-auto-mail-report", this.parameters().id);
                            this.close(true);
                            // this.closeAll();
                            // Navigation.getInstance().navigate("cw-automail-report-manage-list", {}); 

                        }).fail((e) => {
                            this.handleError(e);
                            this.isBusy(false);
                        });
                    }
    
                    
                    dfd.resolve();
                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });
            }else{
                data.id = this.parameters().id
                this.webRequestAutoMailReport.updateAutoReport(data).done((res) => {
                    this.publishMessage("cw-auto-mail-report", this.parameters().id);
                    this.close(true);
                    dfd.resolve();
                }).fail((e) => {
                    this.handleError(e);
                }).always(() => {
                    this.isBusy(false);
                });
            }
            
        }
    }
    genDataToSave(){
        let result = {}
            result.companyId = this.parameters().companyId
            result.businessUnitId = this.parameters().businessUnitId
            result.reportCategory = this.parameters().reportCategory//3
            result.reportType = this.parameters().reportTypeId
            result.entityType = this.parameters().entityTypeId
            result.selectedEntity = this.parameters().selectedEntityId

            if(this.isSendEmail()){
                result.mailSubject = this.mailSubject()
                result.mailTo = this.mailTo()
                result.mailBody = this.mailBody()
            }

            result.includeSubBusinessUnit = this.parameters().includeSubBusinessUnit; ///=== 0?false:true
            result.reportLanguage = this.valueLang()//this.parameters().Language
            let outputType = null
            if(this.isSendEmail() && this.isFileServer()){
                outputType = "b"
            }else if(this.isSendEmail()){
                outputType = "e"
            }else if(this.isFileServer()){
                outputType = "f"
            }

            result.outputType = outputType//email isSendEmail isFileServer
            result.scheduleFrequency = this.scheduler().value
            result.startDate = Utility.formatDateTime(this.startDate()) 
            result.endDate = this.valueEndDate() === "noEndData"?null:Utility.formatDateTime(this.endDate())
            result.dailyConditionTime = this.valueTypeday() === "Multipleday"?"y":"T"
            result.dailyStartTime = this.startTime()
            result.dailyEndTime = this.endTime()
            result.weeklyDayName = parseInt(this.ValueDay())//enum 1-7

            if(this.mode() === "update"){
                result.reportCriteria = this.parameters().reportCriteria;
                result.reportType = this.parameters().reportType
                result.entityType = this.parameters().entityType
                result.selectedEntity = this.parameters().selectedEntity
            }else{
                result.reportCriteria = JSON.stringify(this.parameters());
            }
            // result.noEndDate = this.valueEndDate() === "noEndData"? true:false

        return result
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestAutoMailReport(){
        return WebRequestAutoMailReport.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AutoMailReportMenuScreen),
    template: templateMarkup
};
