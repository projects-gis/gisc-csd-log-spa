import ko from "knockout";
import WebConfig from "../../../app/frameworks/configuration/webconfiguration";
import {Constants,EntityAssociation} from "../../../app/frameworks/constant/apiConstant";


class BusinessUnitFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.includeAssociationNames = [
            EntityAssociation.BusinessUnit.ChildBusinessUnits
        ];
    }
}

export {BusinessUnitFilter};
