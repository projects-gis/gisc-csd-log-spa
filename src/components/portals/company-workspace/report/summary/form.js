import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {
    Enums,
    Constants
} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";
import Navigation from "../../../../controls/gisc-chrome/shell/navigation";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class FormOverspeed extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());
        this.iscommands = null
        this.isDateTimeEnabled = ko.observable(true);
        this.selectedReportType = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedDriverPerformance = ko.observable();
        this.selectedEntityType = ko.observable();
        this.categoryVehicle = ko.observable();
        this.shippingType = ko.observable();
        this.isOnlyJob = ko.observable(false);
        this.overSpeedDuration = ko.observable(120);
        this.selectedEntity = ko.observable();
        this.selectedBackwardDuration = ko.observable();
        this.selectedEngineType = ko.observable();
        this.parkingEngineOnOver = ko.observable(10);
        this.selectedGroupByType = ko.observable();
        this.isGroup = ko.observable(false);
        this.selectedHighScore = ko.observable();
        this.amberScoreRangeFrom = ko.observable();
        this.amberScoreRangeTo = ko.observable();

        this.isIncludeFormerDriver = ko.observable(false);
        this.isIncludeResignedDriver = ko.observable(false);

        this.driverVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;
        });

        this.BURequire = ko.observable(true);
        this.entityTypeRequire = ko.observable(true);
        this.selectEntityRequire = ko.observable(true);
        // this.isShowBtnAutoReport = ko.observable(false);

        this.reportTypes = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.entityTypes = ko.observableArray([]);
        this.categoryVehicleOptions = ko.observableArray([]);
        this.shippingTypeOptions = ko.observableArray([]);
        this.selectEntity = ko.observableArray([]);
        this.driverPerformances = ko.observableArray([]);
        this.backwardDurations = ko.observableArray([]);
        this.engineType = ko.observableArray([]);
        this.groupByTypes = ko.observableArray([]);
        this.highScorePointTypes = ko.observableArray([{
                displayName: this.i18n('Report_Red')(),
                value: 'R'
            },
            {
                displayName: this.i18n('Report_Green')(),
                value: 'G'
            }
        ]);
        this.showAssetWithoutDistance = ScreenHelper.createYesNoObservableArray();
        this.selectedShowAsset = ko.observable(ScreenHelper.findOptionByProperty(this.showAssetWithoutDistance, "value", false));
        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var addMonth = 3;
        var dateTime = Utility.addDays(new Date(), 0);
        var setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(dateTime);
        this.end = ko.observable(dateTime);
        this.startTime = ko.observable("00:00");
        this.endTime = ko.observable("23:59");

        this.maxDateStart = ko.observable(dateTime);
        this.minDateEnd = ko.observable(dateTime);
        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(), addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(), addMonth));

        this.isAutoMailOpen = ko.observable(true)
        this.txtBusinessUnit = ko.observable();

        //for date backward
        this.enable = false;
        this.startBackward = ko.observable(new Date());
        //set end date backward
        this.endBackward = ko.pureComputed(() => {
            var setEndBackward = new Date(this.startBackward());
            this.selectedStartBackward = this.selectedBackwardDuration() && this.selectedBackwardDuration().value;
            if (this.selectedStartBackward === Enums.ModelData.BackwardDurationType.PreviousThreeMonths) {
                setEndBackward.setDate(1); // set Date = First day of Month
                setEndBackward.setMonth(setEndBackward.getMonth() - 2); // set start date backward month - 2 month
            } else if (this.selectedStartBackward === Enums.ModelData.BackwardDurationType.PreviousFourWeeks) {
                setEndBackward.setDate(setEndBackward.getDate() - 27);
            } else if (this.selectedStartBackward === Enums.ModelData.BackwardDurationType.PreviousSeventDays) {
                setEndBackward.setDate(setEndBackward.getDate() - 6);
            } else {
                setEndBackward = setEndBackward;
            }
            return setEndBackward;
        });
        // Enums.ModelData.SummaryReportType.VehicleUsageSummary;TravelDistanceTime
        this.sectionVisible = ko.pureComputed(() => {
            this.isSectionVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.SummaryReport) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.MonthlySummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DailySummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
            return this.isSectionVisible;
        });

        this.isVisibleBu = ko.pureComputed(() => {
            let visibleBu = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.SummaryReport) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.MonthlySummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DailySummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverallSummary);

            if(this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverallSummary){
                this.txtBusinessUnit(this.i18n('Common_BusinessUnit'));
            }
            else{
                this.txtBusinessUnit(this.i18n('Report_BusinessUnit'));
            }
            return visibleBu;
        });

        this.isIncludeSubBUVisible = ko.observable(true);

        this.driverPerformanceVisible = ko.pureComputed(() => {
            this.isDriverPerformanceVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary);
            
            return this.isDriverPerformanceVisible;
        });

        this.txtDateStart = ko.pureComputed(() => {
            var textDateStart = this.i18n('Companies_From')();
            if ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary)) {
                textDateStart = this.i18n('Report_ShipmentFrom')();
            }
            return textDateStart;
        });

        this.txtDateEnd = ko.pureComputed(() => {
            var textDateEnd = this.i18n('Companies_To')();
            if ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary)) {
                textDateEnd = this.i18n('Report_ShipmentTo')();
            }
            return textDateEnd;
        });

        this.categoryVehicleVisible = ko.pureComputed(() => {
            var isCategoryVehicleVisible = ((Object.keys(this.categoryVehicleOptions()).length > 1) ? true : false) && (
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary)
            );
            return isCategoryVehicleVisible;
        });

        this.shippingTypeVisible = ko.pureComputed(() => {
            var isShippingTypeVisible;
            isShippingTypeVisible = (Object.keys(this.shippingTypeOptions()).length > 1) ? true : false;
            isShippingTypeVisible = ((this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime) || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore) || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore)) ? false : true;

            return false;//isShippingTypeVisible;
        });

        this.parkingEngineOnOverVisible = ko.pureComputed(() => {
            this.isParkingEngineOnOverVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary;
            return this.isParkingEngineOnOverVisible;
        });

        this.backwardDurationVisible = ko.pureComputed(() => {
            this.isBackwardDurationVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.BackwardSummary;
            return this.isBackwardDurationVisible;
        });

        this.isOnlyJobVisible = ko.pureComputed(() => {
            var isCheckJobVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration;
            return isCheckJobVisible;
        });

        this.isOverSpeedDurationVisible = ko.pureComputed(() => {
            var isOSDurationVisible =
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary)
            return isOSDurationVisible;
        });

        this.DufaultDateInVisible = ko.pureComputed(() => {
            this.isDufaultDateInVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.BackwardSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.POIParkingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
            return !this.isDufaultDateInVisible;
        });

        this.DateTimeVisible = ko.pureComputed(() => {
            this.isDateTimeVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.POIParkingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
            return this.isDateTimeVisible;
        });

        this.DrivingSummaryReportTimeDurationVisible = ko.pureComputed(() => {
            this.isDrivingSummaryReportTimeDurationVisible = (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration);
            // // if report type not equal DrivingSummaryReportTimeDuration set IncludeSubBU is false
            // if(!this.isDrivingSummaryReportTimeDurationVisible){
            //     this.isIncludeSubBU(false);
            // }
            return this.isDrivingSummaryReportTimeDurationVisible;
        });

        this.vihecleUsageVisible = ko.pureComputed(() => {
            this.isVihecleUsageVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary;
            return false;
        });

        this.visibleAdvanceScore = ko.pureComputed(() => {
            this.isVisibleAdvanceScore = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore;
            return this.isVisibleAdvanceScore;
        });

        //radioCriteria/////////////////////////
        this.radioCriteria = ko.observable();
        this.radioLastWeekLastMonth = ko.observable();
        this.radioThisWeek = ko.observable(true);
        this.radioThisMonth = ko.observable(true);
        this.valueRadio = ko.observable('Yesterday');
        /////////////////////////////////////////////////
        // this.isDateTimeRadioVisible = ko.observable(false);
        // this.isDateTimeNormalVisible = ko.observable(false);
        this.selectedReportType.subscribe((res) => {
            // เลือกแสดงปุ่ม AutoMail ตาม Type
            this.isShowBtnAutoReport = (res && res.value === Enums.ModelData.SummaryReportType.OverallSummary) ||
            (res && res.value === Enums.ModelData.SummaryReportType.RAGScore) ||
            (res && res.value === Enums.ModelData.SummaryReportType.AdvanceScore)

            if(this.isShowBtnAutoReport){
                let BTNAutoMail = []
                    if(WebConfig.userSession.hasPermission(Constants.Permission.VIEWAUTOMAILREPORT)){
                        BTNAutoMail.push(this.createCommand("cmdViewAutoMail", this.i18n('Report_Manage_Automail')(), "svg-ic-mt-report"))
                    }

                    if(WebConfig.userSession.hasPermission(Constants.Permission.CREATAUTOMAILREPORT)){
                        BTNAutoMail.push(this.createCommand("cmdCreateAutoMail", this.i18n('Report_Create_Automail')(), "svg-cmd-add"))
                    }
                this.iscommands.replaceAll(BTNAutoMail);
            }else{
                this.iscommands.replaceAll([])
                this.closeByName("cw-automail-report-manage-create")
            }

            this.buildCommandBar()
            //set date - 1
            // Report ที่ เลือกวันปัจจุบันได้คือ Report Driving Summary Time Duration
            var yesterday = Utility.addDays(new Date(), -1);
            var currentDate = Utility.addDays(new Date(), 0);
            // this.isSelectedReportType = (res && res.value === Enums.ModelData.SummaryReportType.TravelDistanceTime) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.OverallSummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.BackwardSummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.MonthlySummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.DailySummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.VehicleUsageSummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.ShipmentSummary) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.RAGScore) ||
            //     (res && res.value === Enums.ModelData.SummaryReportType.AdvanceScore);
            this.isSelectedReportType = (res && res.value != Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration);

            if (this.isSelectedReportType) {
                this.start(yesterday);
                this.end(yesterday);
                this.maxDateStart(yesterday);
                addDay = 31;
            } else {
                this.start(currentDate);
                this.end(currentDate);
                this.maxDateStart(currentDate);
                addDay = 30;
            }

            if (res && res.value === Enums.ModelData.SummaryReportType.RAGScore ||
                res && res.value === Enums.ModelData.SummaryReportType.AdvanceScore
            ) {
                if (res && res.value === Enums.ModelData.SummaryReportType.RAGScore ||
                    res && res.value === Enums.ModelData.SummaryReportType.AdvanceScore) {
                    this.entityTypeRequire(true);
                    this.isIncludeSubBUVisible(true);
                    this.radioLastWeekLastMonth(true);
                } else {
                    this.entityTypeRequire(false);
                    this.isIncludeSubBUVisible(false);
                    this.radioLastWeekLastMonth(false);
                }

                this.selectEntityRequire(false);
                this.BURequire(false);
                this.valueRadio('Yesterday');
                this.radioCriteria(true);
                this.isDateTimeEnabled(false);

               if(res && res.value === Enums.ModelData.SummaryReportType.AdvanceScore){
                    let sDate = new Date();
                    sDate.setDate(sDate.getDate() - 1)
                     this.start(this.setFormatDateTime({
                         data: sDate,
                         start: true
                     }))
                     this.end(this.setFormatDateTime({
                         data: sDate,
                         end: true
                     }))
                     $("#fromDateReport").val(this.setFormatDisplayDateTime(sDate));
                     $("#toDateReport").val(this.setFormatDisplayDateTime(sDate));
 
               }

            }
            else if(res && res.value === Enums.ModelData.SummaryReportType.OverallSummary){
                this.BURequire(false);
                this.selectEntityRequire(false);
                this.valueRadio('Yesterday');
                this.radioCriteria(true);
                this.isDateTimeEnabled(false);
                this.radioLastWeekLastMonth(true);
            }
            else {
                this.selectEntityRequire(true);
                this.entityTypeRequire(true);
                this.BURequire(true);
                this.radioCriteria(false);
                this.isDateTimeEnabled(true);
                this.isIncludeSubBUVisible(true);

            }
        });

        this.periodDay = ko.pureComputed(() => {
            
            // can't select date more than current date
            if (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.WorkingTimeSummary) {
                addDay = 6;
            }

            var isDate = Utility.addDays(this.start(), addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;

            if (calDay > 0) {
                isDate = this.isSelectedReportType ? Utility.addDays(new Date(), -1) : new Date();
            }


            // set date when clear start date and end date
            if (this.start() === null) {
                // set end date when clear start date
                if (this.end() === null) {
                    isDate = this.isSelectedReportType ? Utility.addDays(new Date(), -1) : new Date();
                } else {
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);

            } else {
                this.minDateEnd(this.start());
            }

            // if (this.isSelectedReportType) {
            //     isDate = Utility.addDays(isDate, -1);
            //     if(this.start() == null || this.end() == null){
            //         this.maxDateStart(isDate);
            //     }
            // }
        

            return isDate;
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.isParkingOverTimeSummary = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.OverTimeIdlingSummary;
            this.isActivatingDrivingSummary = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary;
            this.isShipmentSummary = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ShipmentSummary;
            this.isDrivingSummaryReportTimeDuration = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration;
            this.isDriverPerformanceSummary = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DriverPerformanceSummary;
            this.isParkingSummary = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.POIParkingSummary;
            this.isRAGScore = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.RAGScore;
            this.isAdvanceScore = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.AdvanceScore;
            this.isVehicleUsage = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.VehicleUsageSummary;
            this.isTravelDistanceTime = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.TravelDistanceTime;
            this.isMonthlySummaryVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.MonthlySummary;
            this.isDailySummaryVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.DailySummary;

            // สำหรับ report ที่ฟิลด์ vehicle/driver สามารถเลือก All ได้ /////////////////
            var vehicleDriverAddAll =   this.isActivatingDrivingSummary ||
                                        this.isShipmentSummary ||
                                        this.isDrivingSummaryReportTimeDuration ||
                                        this.isDriverPerformanceSummary ||
                                        this.isParkingOverTimeSummary||
                                        this.isRAGScore ||
                                        this.isAdvanceScore ||
                                        this.isVehicleUsage ||
                                        this.isTravelDistanceTime ||
                                        this.isMonthlySummaryVisible ||
                                        this.isDailySummaryVisible;
            /////////////////////////////////////////////////////////////////////
            var vihecleTypeOnly = this.isTravelDistanceTime || this.isParkingEngineOnOverVisible || this.isMonthlySummaryVisible || this.isDailySummaryVisible; //|| this.isDrivingSummaryReportTimeDurationVisible;
            var driverTypeOnly = this.isRAGScore || this.isAdvanceScore;
            var vihecleTypeAndDriverType = this.isDriverPerformanceVisible || this.isVehicleUsage ||  this.isShipmentSummary;
            var businessUnitIds = this.selectedBusinessUnit();
            var result = this.i18n('Common_PleaseSelect')();
            this.selectEntity([]);
            //if check include Sub BU
            if (this.isIncludeSubBU() && businessUnitIds != null) {

                businessUnitIds = Utility.includeSubBU(this.businessUnits(), businessUnitIds);
            }

            //set vihecleType Only or driver only
            if (vihecleTypeOnly) {
                this.entityTypes(this.tempEntityTypes()[0])
            } else if (driverTypeOnly) {
                this.entityTypes(this.tempEntityTypes()[1])
            } else {
                this.entityTypes(this.tempEntityTypes())
            }
            // this.entityTypes((vihecleTypeOnly)?this.tempEntityTypes()[0]:this.tempEntityTypes());

            if (this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null) {

                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle,
                    vehicleCategoryId: this.categoryVehicle().id
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if (driverTypeOnly || vihecleTypeOnly || vihecleTypeAndDriverType || this.isDrivingSummaryReportTimeDurationVisible  || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary)) {
                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0) {
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        } else if (Object.keys(listSelectEntity).length == 0 && (vehicleDriverAddAll)) { //add All กรณีเลือก BUแล้วไม่เจอข้อมูล
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    } else {
                        //remove displayName "Report_All" and id 0
                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0) {
                            listSelectEntity.shift();
                        }
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            } else if (this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null) {

                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver,                    
                    IncludeFormerInBusinessUnit : this.isIncludeFormerDriver(),
                    isEnable: this.isIncludeResignedDriver() ? null : true ,

                }).done((response) => {
                    //set displayName
                    response.items.forEach(function (arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if (driverTypeOnly || vihecleTypeOnly || vihecleTypeAndDriverType || this.isDrivingSummaryReportTimeDurationVisible || (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.SummaryReportType.ActivatingDrivingSummary)) {

                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0) {

                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }else if (Object.keys(listSelectEntity).length == 0 && (vehicleDriverAddAll)) { //add All กรณีเลือก BUแล้วไม่เจอข้อมูล
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    } else {

                        //remove displayName "Report_All" and id 0
                        if (Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0) {

                            listSelectEntity.shift();
                        }
                    }

                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            } else {

                this.selectEntity([]);
            }
            if (this.isVehicleUsage || this.isTravelDistanceTime || this.isAdvanceScore || this.isRAGScore) {

                result = "";
            }

            return result;
        });



        // ForRadio
        this.valueRadio.subscribe((res) => {
            let currentDate = new Date();
            var startDate = new Date();
            var endDate = new Date();
            
            switch (res) {
                case 'Yesterday':
                    this.isDateTimeEnabled(false);
                    startDate.setDate(startDate.getDate() - 1)
                    // console.log("startDate.setDate(startDate.getDate() - 1)",startDate)
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#fromDateReport").val(this.setFormatDisplayDateTime(startDate));
                    $("#toDateReport").val(this.setFormatDisplayDateTime(startDate));

                    break;

                case 'ThisWeek':
                    this.isDateTimeEnabled(false);

                    let startWeek;
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    if (currentDate.getDay() == 0) { //ถ้าเป็นวันอาทิตย์ ( 0 คือวันอาทิตย์ )

                        startWeek = parseInt((startDate.getDate() - 6));

                    } else {
                        startWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    }
                    startDate.setDate(startWeek)



                    if (currentDate.getDay() == 1) { //ถ้าเป็นวันจันทร์ ( 1 คือวันจันทร์ )
                        endDate.setDate(currentDate.getDate());
                        this.maxDateStart(new Date(), 0);

                    } else {
                        endDate.setDate(currentDate.getDate() - 1);
                        this.maxDateStart(Utility.addDays(new Date(), -1));

                    }

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                    $("#fromDateReport").val(this.setFormatDisplayDateTime(startDate));
                    $("#toDateReport").val(this.setFormatDisplayDateTime(endDate));
                    // this.timeStart("00:00");
                    // this.timeEnd("23:59");

                    break;

                case 'LastWeek':
                    this.isDateTimeEnabled(false);
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek)
                    endDate.setDate(LastendWeek - 1)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                    $("#fromDateReport").val(this.setFormatDisplayDateTime(startDate));
                    $("#toDateReport").val(this.setFormatDisplayDateTime(endDate));

                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));

                    break;

                case 'ThisMonth':
                    this.isDateTimeEnabled(false);

                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date();
                    let getCurrentDate = lastDay.getDate() - 1;
                    lastDay.setDate(getCurrentDate);
                    // let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);


                    this.start(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));
                    this.end(this.setFormatDateTime({
                        data: lastDay,
                        end: true
                    }));
                    $("#fromDateReport").val(this.setFormatDisplayDateTime(firstDay));
                    $("#toDateReport").val(this.setFormatDisplayDateTime(lastDay));

                    break;
                case 'LastMonth':
                    this.isDateTimeEnabled(false);
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.start(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))

                    $("#fromDateReport").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#toDateReport").val(this.setFormatDisplayDateTime(LastlastDay));

                    break;
                case 'Custom':
                    this.isDateTimeEnabled(true)
                    break;

                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });

        this.subscribeMessage("cw-automail-manage-close", (data) => {
            if(data){
                this.isAutoMailOpen(data.status)

                if(this.valueRadio() === "Custom"){
                    this.isDateTimeEnabled(true)
                }
            }
        });
    }

    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else {}

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;



        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }


    setupExtend() {
        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedReportType.extend({
            required: true
        });
        // this.selectedBusinessUnit.extend({ required: true });
        //Entity Type
        // this.selectedEntityType.extend({ required: true });
        // this.selectedEntity.extend({ required: true });
        this.selectedDriverPerformance.extend({
            required: true
        });
        this.selectedBackwardDuration.extend({
            required: true
        });
        this.start.extend({
            required: true
        });
        this.end.extend({
            required: true,
        });
        this.selectedEngineType.extend({
            required: true
        });
        this.parkingEngineOnOver.extend({
            required: true,
            number: true,
            min: 0
        });

        // this.entityTypeRequire(true);

        this.selectedEntity.extend({
            required: {
                onlyIf: () => {
                    return (this.selectEntityRequire()); //require เมื่อเป็น true
                }
            }
        });

        this.selectedEntityType.extend({
            required: {
                onlyIf: () => {
                    return (this.entityTypeRequire()); //require เมื่อเป็น true
                }
            }
        });
        this.selectedBusinessUnit.extend({
            required: {
                onlyIf: () => {
                    return (this.BURequire()); //require เมื่อเป็น true
                }
            }
        });

        this.amberScoreRangeFrom.extend({
            required: true,
            validation: {
                validator: (val) => {
                    let isValid = true;
                    if (this.amberScoreRangeTo() && this.amberScoreRangeTo() != "") {
                        isValid = (val > this.amberScoreRangeTo()) ? false : true;
                    }
                    return isValid;
                },
                message: ''
            }
        });
        this.amberScoreRangeTo.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            selectedReportType: this.selectedReportType,
            end: this.end,
            start: this.start
        });

        this.validationSection = ko.validatedObservable({
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedEntityType: this.selectedEntityType,
            selectedEntity: this.selectedEntity,
        });

        this.validationDriverPerformance = ko.validatedObservable({
            selectedDriverPerformance: this.selectedDriverPerformance,
        });

        this.validationParkingEngineOnOver = ko.validatedObservable({
            selectedEngineType: this.selectedEngineType,
            parkingEngineOnOver: this.parkingEngineOnOver
        });

        this.validationBackwardDuration = ko.validatedObservable({
            selectedBackwardDuration: this.selectedBackwardDuration,
        });

        this.validationAdvanceScore = ko.validatedObservable({
            amberScoreRangeFrom: this.amberScoreRangeFrom,
            amberScoreRangeTo: this.amberScoreRangeTo,
            selectedDriverPerformance: this.selectedDriverPerformance
        });

        this.validationBu = ko.validatedObservable({
            selectedBusinessUnit: this.selectedBusinessUnit,
        });
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    getReportTemplateType() {
        var reportType = new Array();
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_TravelDistance)) {
            reportType.push(Enums.ModelData.SummaryReportType.TravelDistanceTime);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_POIParking)) {
            reportType.push(Enums.ModelData.SummaryReportType.POIParkingSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_ParingOvertime)) {
            reportType.push(Enums.ModelData.SummaryReportType.OverTimeIdlingSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_DriverPerformance)) {
            reportType.push(Enums.ModelData.SummaryReportType.DriverPerformanceSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Overall)) {
            reportType.push(Enums.ModelData.SummaryReportType.OverallSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Backward)) {
            reportType.push(Enums.ModelData.SummaryReportType.BackwardSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Monthly)) {
            reportType.push(Enums.ModelData.SummaryReportType.MonthlySummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Daily)) {
            reportType.push(Enums.ModelData.SummaryReportType.DailySummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Driving)) {
            reportType.push(Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_VehicleUsage)) {
            reportType.push(Enums.ModelData.SummaryReportType.VehicleUsageSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_Shipment)) {
            reportType.push(Enums.ModelData.SummaryReportType.ShipmentSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_AvtivatingDriving)) {
            reportType.push(Enums.ModelData.SummaryReportType.ActivatingDrivingSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_WoringTime)) {
            reportType.push(Enums.ModelData.SummaryReportType.WorkingTimeSummary);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_RAGScore)) {
            reportType.push(Enums.ModelData.SummaryReportType.RAGScore);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.SummaryReport_AdvanceScore)) {
            reportType.push(Enums.ModelData.SummaryReportType.AdvanceScore);
        }
        //Mock With Out Permission//
        // reportType.push(Enums.ModelData.SummaryReportType.RAGScore);
        // reportType.push(Enums.ModelData.SummaryReportType.AdvanceScore);




        if (reportType.length < 1) {
            reportType = [0];
        }
        return reportType;
    }


    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        let currentDate = new Date();
        let firstDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1);



        if (currentDate.getDay() == 1) {
            this.radioThisWeek(false);
        }
        if (currentDate.getDate() == firstDay.getDate()) {

            this.radioThisMonth(false);
        }


        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SummaryReportType],
            values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };


        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var filterCategoryVehicle = {
            categoryId: Enums.ModelData.CategoryType.Vehicle,
        };

        var filterShippingType = {
            categoryId: Enums.ModelData.CategoryType.ShippingType,
        };

        var driverRuleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            enable: true
        }

        var enumBackwardDurationFilter = {
            types: [Enums.ModelData.EnumResourceType.BackwardDurationType],
            values: [
                Enums.ModelData.BackwardDurationType.PreviousThreeMonths,
                Enums.ModelData.BackwardDurationType.PreviousFourWeeks,
                Enums.ModelData.BackwardDurationType.PreviousSeventDays
            ]
        };

        var enumEngineTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.EngineType],
            values: []
        };

        var enumGroupByTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.GroupByType],
            values: [
                Enums.ModelData.GroupByType.DateTime,
                Enums.ModelData.GroupByType.Vehicle
            ]
        };

        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);
        var d4 = this.webRequestDriveRule.listDriveRuleSummary(driverRuleFilter);
        var d5 = this.webRequestEnumResource.listEnumResource(enumBackwardDurationFilter);
        var d6 = this.webRequestEnumResource.listEnumResource(enumEngineTypeFilter);
        var d7 = this.webRequestEnumResource.listEnumResource(enumGroupByTypeFilter);

        var d8 = this.webRequestCategory.listCategory(filterCategoryVehicle);
        var d9 = this.webRequestCategory.listCategory(filterShippingType);



        $.when(d1, d2, d3, d4, d5, d6, d7, d8, d9).done((r1, r2, r3, r4, r5, r6, r7, r8, r9) => {


            r8.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });

            r9.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });




            this.reportTypes(r1.items);
            this.businessUnits(r2.items);
            this.tempEntityTypes(r3.items);
            this.driverPerformances(r4.items);
            this.backwardDurations(r5.items);
            this.engineType(r6.items);
            this.groupByTypes(r7.items);
            this.categoryVehicleOptions(r8.items);
            this.shippingTypeOptions(r9.items);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        return dfd;
    }


    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    buildCommandBar(commands) {
        if(commands){
            this.iscommands = commands
        }
    }
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdViewAutoMail":
                this.closeAll();
                Navigation.getInstance().navigate("cw-automail-report-manage-list", {}); 
                break;
            case "cmdCreateAutoMail":
                let dataToSender = this.genDatatoSendPreview()
                if(dataToSender){
                    this.navigate("cw-automail-report-manage-create", {
                        dataToSender:dataToSender,
                        mode:'create'
                    });
                    this.isAutoMailOpen(false)
                    this.isDateTimeEnabled(false)
                }
            break;
            default:
                break;
        }
    }
    genDatatoSendPreview(data){

        var reportNameSpace = null;
        var reportTitle = null;
        var ReportTypeID = null;
        var BusinessUnitID = null;
        var EntityTypeID = null;
        var SelectedEntityID = null;
        var DriverPerformanceID = null;
        var EngineTypeID = null;
        var EngDur = null;
        var BackwardDurationID = null;
        var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.startTime() + ":00";
        //var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + "00:00:00";
        var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.endTime() + ":00";
        var IncludeSubBU = (this.isIncludeSubBU()) ? 1 : 0;
        var IncludeJob = (this.isOnlyJob()) ? 1 : 0;


        var categoryId = (this.categoryVehicle()) ? this.categoryVehicle().id : 0;
        var shippingTypeId = (this.shippingType()) ? this.shippingType().id : 0;
        var overSpeedDurationTime = (this.overSpeedDuration()) ? this.overSpeedDuration() : 0;

        var GroupByType = (this.selectedGroupByType() && this.selectedGroupByType().value) ? this.selectedGroupByType() && this.selectedGroupByType().value : 0;


        //for AdvanceScore Report
        var highScorePointsAre = null;
        var amberScoreRangeFrom = null;
        var amberScoreRangeTo = null;
        var showAssetWithoutDistance = null;
        //

        // if(this.isDateTimeVisible || this.isDrivingSummaryReportTimeDurationVisible){
        //     formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.startTime() + ":00";
        //     formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.endTime() + ":00";
        // }

        // Validate Bu
        if (this.isVisibleBu()) {
            if (this.validationBu.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
            } else {
                this.validationBu.errors.showAllMessages();
                return false;
            }
        }

        //validate section and DriverPerformance rule 
        if (this.isDriverPerformanceVisible) {

            if (this.validationSection.isValid() && this.validationDriverPerformance.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
                EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                DriverPerformanceID = this.selectedDriverPerformance() && this.selectedDriverPerformance().id;
            } else {
                this.validationSection.errors.showAllMessages();
                this.validationDriverPerformance.errors.showAllMessages();
                return false;
            }

            //validate section and ParkingEngineOnOver 
        } else if (this.isParkingEngineOnOverVisible) {

            if (this.validationSection.isValid() && this.validationParkingEngineOnOver.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
                EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                EngineTypeID = this.selectedEngineType() && this.selectedEngineType().value;
                EngDur = this.parkingEngineOnOver();
            } else {
                this.validationSection.errors.showAllMessages();
                this.validationParkingEngineOnOver.errors.showAllMessages();
                return false;
            }

            //validate backward 
        } else if (this.isBackwardDurationVisible) {

            if (this.validationBackwardDuration.isValid()) {
                BackwardDurationID = this.selectedBackwardDuration() && this.selectedBackwardDuration().value;
                var backwardSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.startBackward())) + " " + "00:00:00";
                var backwardEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.endBackward())) + " " + "00:00:00";
                //set for report backward EDate = SDate, SDate = EDate
                formatSDate = backwardEDate;
                formatEDate = backwardSDate;
            } else {
                this.validationBackwardDuration.errors.showAllMessages();
                return false;
            }
            //validate AdvanceScore     
        } else if (this.isVisibleAdvanceScore) {
            if (this.validationSection.isValid() && this.validationAdvanceScore.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
                EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                highScorePointsAre = this.selectedHighScore().value;
                amberScoreRangeFrom = this.amberScoreRangeFrom();
                amberScoreRangeTo = this.amberScoreRangeTo();
                showAssetWithoutDistance = this.selectedShowAsset().value;
                DriverPerformanceID = this.selectedDriverPerformance() && this.selectedDriverPerformance().id;
            } else {
                this.validationSection.errors.showAllMessages();
                this.validationAdvanceScore.errors.showAllMessages();
                return false;
            }
        } else if (this.isSectionVisible) {
            if (this.validationSection.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
                EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
            } else {
                this.validationSection.errors.showAllMessages();
                return false;
            }


        }
        var reportExport = new Object();
        // varidate Model and set param repoprt
        if (this.validationModel.isValid() || this.isBackwardDurationVisible) {
            switch (this.selectedReportType() && this.selectedReportType().value) {
                case Enums.ModelData.SummaryReportType.TravelDistanceTime:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.TravelDistanceTimeSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_TravelDistanceTime";
                    // reportExport.CSV = this.webRequestReportExports.exportCSVTravelDistanceTimeSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.POIParkingSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.POIParkingSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_POIParkingSummary";
                    break;
                case Enums.ModelData.SummaryReportType.OverTimeIdlingSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.ParkingEngineOnOverTimeReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_OverTimeIdlingSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVParkingEngineOnOverTimeReport();
                    break;
                case Enums.ModelData.SummaryReportType.DriverPerformanceSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DriverPerformanceSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_DriverPerformanceSummary";
                    break;
                case Enums.ModelData.SummaryReportType.OverallSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.OverallSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_OverallSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVOverallSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.BackwardSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.BackwardSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_BackwardSummary";
                    break;
                case Enums.ModelData.SummaryReportType.MonthlySummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.MonthlySummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_MonthlySummary";
                    break;
                case Enums.ModelData.SummaryReportType.DailySummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DailySummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_DailySummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVDailySummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DrivingSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_DrivingSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVDrivingTimeDurationSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.VehicleUsageSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.VehicleUsageSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_VehicleUsageSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVVehicleUsageSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.ShipmentSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DriverSummaryShipmentReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_ShipmentSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVShipmentSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.ActivatingDrivingSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.ActivatingDriverLicenseReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_ActivatingDrivingSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVActivatingDrivingSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.WorkingTimeSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.WorkingTimeSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_WorkingTimeSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVWorkingTimeSummaryReport();
                    break;

                case Enums.ModelData.SummaryReportType.RAGScore:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.RAGScoreSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_RAGScoreSummaryReport";
                    // reportExport.CSV = this.webRequestReportExports.exportCSVWorkingTimeSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.AdvanceScore:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.AdvancedScoreSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_AdvancedScoreSummaryReport";
                    // reportExport.CSV = this.webRequestReportExports.exportCSVWorkingTimeSummaryReport();
                    break;


                default:
                    reportNameSpace = null;
                    break;
            }
            var objReport = this.reports = {
                report: reportNameSpace,
                parameters: {
                    userId: WebConfig.userSession.id,
                    companyId: WebConfig.userSession.currentCompanyId,
                    reportTypeId: this.selectedReportType().value,
                    businessUnitId: BusinessUnitID?BusinessUnitID:0,
                    includeSubBusinessUnit: this.isIncludeSubBU(),
                    includeJob: IncludeJob,
                    entityTypeId: EntityTypeID,
                    selectedEntityId: SelectedEntityID,
                    ruleId: DriverPerformanceID,
                    durationTypeId: BackwardDurationID,
                    sDate: formatSDate,
                    eDate: formatEDate,
                    groupBy: GroupByType,
                    printBy: WebConfig.userSession.fullname,
                    language: WebConfig.userSession.currentUserLanguage,
                    categoryId: categoryId,
                    shippingType: shippingTypeId,
                    time: overSpeedDurationTime,

                    //parameter AdvanceScore
                    highScorePointsAre: highScorePointsAre,
                    amberScoreRangeFrom: amberScoreRangeFrom,
                    amberScoreRangeTo: amberScoreRangeTo,
                    showAssetWithoutDistance: showAssetWithoutDistance ? 1 : 0 ,
                    //paramerter for old report
                    departmentId: BusinessUnitID,
                    assetId: SelectedEntityID,
                    engineType: EngineTypeID,
                    engDur: EngDur,
                    group: this.isGroup(),
                    reportCategory:3
                }
            };

            return {
                reportSource: this.reports,
                reportName: this.i18n(reportTitle)(),
                reportExport: reportExport
            }
            // this.navigate("cw-report-reportviewer", {
            //     reportSource: this.reports,
            //     reportName: this.i18n(reportTitle)(),
            //     reportExport: reportExport
            // });

        } else {
            // This is unreachable code, since we disable save button when VM is not valid
            this.validationModel.errors.showAllMessages();

        }
    }
    onActionClick(sender) {
        if (sender.id === "actPreview") {
            // let dataToSender = this.genDatatoSendPreview()
            
            var reportNameSpace = null;
        var reportTitle = null;
        var ReportTypeID = null;
        var BusinessUnitID = null;
        var EntityTypeID = null;
        var SelectedEntityID = null;
        var DriverPerformanceID = null;
        var EngineTypeID = null;
        var EngDur = null;
        var BackwardDurationID = null;
        var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.startTime() + ":00";
        //var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + "00:00:00";
        var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.endTime() + ":00";
        var IncludeSubBU = (this.isIncludeSubBU()) ? 1 : 0;
        var IncludeJob = (this.isOnlyJob()) ? 1 : 0;


        var categoryId = (this.categoryVehicle()) ? this.categoryVehicle().id : 0;
        var shippingTypeId = (this.shippingType()) ? this.shippingType().id : 0;
        var overSpeedDurationTime = (this.overSpeedDuration()) ? this.overSpeedDuration() : 0;

        var GroupByType = (this.selectedGroupByType() && this.selectedGroupByType().value) ? this.selectedGroupByType() && this.selectedGroupByType().value : 0;


        //for AdvanceScore Report
        var highScorePointsAre = null;
        var amberScoreRangeFrom = null;
        var amberScoreRangeTo = null;
        var showAssetWithoutDistance = null;
        //

        // if(this.isDateTimeVisible || this.isDrivingSummaryReportTimeDurationVisible){
        //     formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.startTime() + ":00";
        //     formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.endTime() + ":00";
        // }

            // Validate Bu
            if(this.isVisibleBu()){
                if (this.validationBu.isValid()) {
                    BusinessUnitID = this.selectedBusinessUnit();
                } else {
                    this.validationBu.errors.showAllMessages();
                    return false;
                }
            }

            //validate section and DriverPerformance rule 
            if (this.isDriverPerformanceVisible) {

            if (this.validationSection.isValid() && this.validationDriverPerformance.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
                EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                DriverPerformanceID = this.selectedDriverPerformance() && this.selectedDriverPerformance().id;
            } else {
                this.validationSection.errors.showAllMessages();
                this.validationDriverPerformance.errors.showAllMessages();
                return false;
            }

            //validate section and ParkingEngineOnOver 
        } else if (this.isParkingEngineOnOverVisible) {

            if (this.validationSection.isValid() && this.validationParkingEngineOnOver.isValid()) {
                BusinessUnitID = this.selectedBusinessUnit();
                EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                EngineTypeID = this.selectedEngineType() && this.selectedEngineType().value;
                EngDur = this.parkingEngineOnOver();
            } else {
                this.validationSection.errors.showAllMessages();
                this.validationParkingEngineOnOver.errors.showAllMessages();
                return false;
            }

            //validate backward 
        } else if (this.isBackwardDurationVisible) {

                if (this.validationBackwardDuration.isValid()) {
                    BackwardDurationID = this.selectedBackwardDuration() && this.selectedBackwardDuration().value;
                    var backwardSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.startBackward())) + " " + "00:00:00";
                    var backwardEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.endBackward())) + " " + "00:00:00";
                    //set for report backward EDate = SDate, SDate = EDate
                    formatSDate = backwardEDate;
                    formatEDate = backwardSDate;
                } else {
                    this.validationBackwardDuration.errors.showAllMessages();
                    return false;
                }
                //validate AdvanceScore     
            } else if (this.isVisibleAdvanceScore) {
                if (this.validationSection.isValid() && this.validationAdvanceScore.isValid()) {
                    BusinessUnitID = this.selectedBusinessUnit();
                    EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                    SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                    highScorePointsAre = this.selectedHighScore().value;
                    amberScoreRangeFrom = this.amberScoreRangeFrom();
                    amberScoreRangeTo = this.amberScoreRangeTo();
                    showAssetWithoutDistance = this.selectedShowAsset().value;
                    DriverPerformanceID = this.selectedDriverPerformance() && this.selectedDriverPerformance().id;
                } else {
                    this.validationSection.errors.showAllMessages();
                    this.validationAdvanceScore.errors.showAllMessages();
                    return false;
                }
            } else if (this.isSectionVisible) {
                if (this.validationSection.isValid()) {
                    EntityTypeID = this.selectedEntityType() && this.selectedEntityType().value;
                    SelectedEntityID = this.selectedEntity() && this.selectedEntity().id;
                } else {
                    this.validationSection.errors.showAllMessages();
                    return false;
                }


        }
        var reportExport = new Object();
        // varidate Model and set param repoprt
        if (this.validationModel.isValid() || this.isBackwardDurationVisible) {
            switch (this.selectedReportType() && this.selectedReportType().value) {
                case Enums.ModelData.SummaryReportType.TravelDistanceTime:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.TravelDistanceTimeSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_TravelDistanceTime";
                    // reportExport.CSV = this.webRequestReportExports.exportCSVTravelDistanceTimeSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.POIParkingSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.POIParkingSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_POIParkingSummary";
                    break;
                case Enums.ModelData.SummaryReportType.OverTimeIdlingSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.ParkingEngineOnOverTimeReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_OverTimeIdlingSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVParkingEngineOnOverTimeReport();
                    break;
                case Enums.ModelData.SummaryReportType.DriverPerformanceSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DriverPerformanceSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_DriverPerformanceSummary";
                    break;
                case Enums.ModelData.SummaryReportType.OverallSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.OverallSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_OverallSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVOverallSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.BackwardSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.BackwardSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_BackwardSummary";
                    break;
                case Enums.ModelData.SummaryReportType.MonthlySummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.MonthlySummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_MonthlySummary";
                    break;
                case Enums.ModelData.SummaryReportType.DailySummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DailySummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_DailySummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVDailySummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.DrivingSummaryReportTimeDuration:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DrivingSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_DrivingSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVDrivingTimeDurationSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.VehicleUsageSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.VehicleUsageSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_VehicleUsageSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVVehicleUsageSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.ShipmentSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.DriverSummaryShipmentReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_ShipmentSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVShipmentSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.ActivatingDrivingSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.ActivatingDriverLicenseReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_ActivatingDrivingSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVActivatingDrivingSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.WorkingTimeSummary:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.WorkingTimeSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_WorkingTimeSummary";
                    reportExport.CSV = this.webRequestReportExports.exportCSVWorkingTimeSummaryReport();
                    break;

                case Enums.ModelData.SummaryReportType.RAGScore:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.RAGScoreSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_RAGScoreSummaryReport";
                    // reportExport.CSV = this.webRequestReportExports.exportCSVWorkingTimeSummaryReport();
                    break;
                case Enums.ModelData.SummaryReportType.AdvanceScore:
                    reportNameSpace = "Gisc.Csd.Service.Report.Library.Summary.AdvancedScoreSummaryReport, Gisc.Csd.Service.Report.Library";
                    reportTitle = "SummaryReport_AdvancedScoreSummaryReport";
                    // reportExport.CSV = this.webRequestReportExports.exportCSVWorkingTimeSummaryReport();
                    break;


                default:
                    reportNameSpace = null;
                    break;
            }
            var objReport = this.reports = {
                report: reportNameSpace,
                parameters: {
                    UserID: WebConfig.userSession.id,
                    CompanyID: WebConfig.userSession.currentCompanyId,
                    ReportTypeID: this.selectedReportType().value,
                    BusinessUnitID: BusinessUnitID,
                    IncludeSubBusinessUnit: IncludeSubBU,
                    IncludeJob: IncludeJob,
                    EntityTypeID: EntityTypeID,
                    SelectedEntityID: SelectedEntityID,
                    RuleID: DriverPerformanceID,
                    DurationTypeID: BackwardDurationID,
                    SDate: formatSDate,
                    EDate: formatEDate,
                    GroupBy: GroupByType,
                    PrintBy: WebConfig.userSession.fullname,
                    Language: WebConfig.userSession.currentUserLanguage,
                    CategoryID: categoryId,
                    ShippingType: shippingTypeId,
                    Time: overSpeedDurationTime,
                    IncludeFormerInBusinessUnit: this.isIncludeFormerDriver() ? 1 : 0 ,
                    Enable: this.isIncludeResignedDriver() ? 1 : 0 ,

                    //parameter AdvanceScore
                    HighScorePointsAre: highScorePointsAre,
                    AmberScoreRangeFrom: amberScoreRangeFrom,
                    AmberScoreRangeTo: amberScoreRangeTo,
                    ShowAssetWithoutDistance: showAssetWithoutDistance ? 1 : 0 ,
                    //paramerter for old report
                    DepartmentID: BusinessUnitID,
                    AssetID: SelectedEntityID,
                    EngineType: EngineTypeID,
                    EngDur: EngDur,
                    Group: this.isGroup()
                }
            };


            this.navigate("cw-report-reportviewer", {
                reportSource: this.reports,
                reportName: this.i18n(reportTitle)(),
                reportExport: reportExport
            });

        } else {
            // This is unreachable code, since we disable save button when VM is not valid
            this.validationModel.errors.showAllMessages();

        }
           
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(FormOverspeed),
    template: templateMarkup
};