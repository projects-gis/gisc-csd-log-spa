import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";
import WebRequestAlertType from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";


/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 **/

class FormGeneralReport extends ScreenBase {
    constructor(params) {
        super(params);
        
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        //MultiSelect Bu//
        this.selectedBusinessUnitMultiselect = ko.observable();
        this.businessUnitsMultiselectOption = ko.observableArray([]);
        ////////
        this.isDateTimeEnabled = ko.observable(false);
        this.valueRadio = ko.observable('Today');
        this.selectedReportType = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedEntityType = ko.observable();
        this.selectedEntity = ko.observable();
        this.selectedParkingType = ko.observable();
        this.selectedEventType = ko.observable();
        this.categoryVehicle = ko.observable();
        this.shippingType = ko.observable();

        this.isIncludeFormerDriver = ko.observable(false);
        this.isIncludeResignedDriver = ko.observable(false);

        this.reportTypes = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.entityTypes = ko.observableArray([]);
        this.selectEntity = ko.observableArray([]);
        this.parkingTypes = ko.observableArray([]);
        this.eventTypes = ko.observableArray([]);
        this.alertTypes = ko.observableArray([]);
        this.selectedAlertType = ko.observable();
        this.categoryVehicleOptions = ko.observableArray([]);
        this.shippingTypeOptions = ko.observableArray([]);
        this.customPoiValue = ko.observable();
        this.isLastParking = ko.observable(false);
        this.isGroupParkStatus = ko.observable(true);
        this.isGroup = ko.observable(true);
        this.isLimitedToSelectedTime = ko.observable(false);
        this.overSpeedDuration = ko.observable(120);
        this.reportGroupData = ko.observable(false);
        this.isGraphic = ko.observable(false);
        this.isIncludeDelinquent = ko.observable(false);
        //Temperature
        this.temperature = ko.observableArray([]);
        this.selectedItemTemp = ko.observable();
        this.orderTemp = ko.observable([[ 1, "asc" ]]);

        //Gate
        this.gate = ko.observableArray([]);
        this.selectedItemGate = ko.observable();
        this.orderGate = ko.observable([[ 1, "asc" ]]);
        this.requiredGateVisible = ko.observable(false);

        this.driverPerformancesRuleOption = ko.observableArray([]);
        this.selectedDriverPerformanceRule = ko.observable();

        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);
        //TruckNotUpdate
        this.truckNotDuration = ko.observable(360);

        // for TrackingReport Send Mail
        this.mailTo = ko.observable();
        this.isRequiredMailTo = ko.observable();

         this.parkingTypeVisible = ko.pureComputed(() => {
            this.isParkingTypeSelected = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Stoppage;
            return this.isParkingTypeSelected;
        });

        this.driverVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;
        });

        this.customPoiVisible = ko.pureComputed(() => {
            return this.selectedParkingType() && this.selectedParkingType().value === Enums.ModelData.ParkingType.SpecifiedInsidePOI && 
                   this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Stoppage;
        });

        this.temperatureVisible = ko.pureComputed(() => {
            // return this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Stoppage;
            return false;
        });
        
        this.eventTypeVisible = ko.pureComputed(() => {
			this.isEventTypeVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Driving;
            return this.isEventTypeVisible;
        });

        this.gateVisible = ko.pureComputed(() => {
            this.isGateVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.OpenCloseGate;
            return this.isGateVisible;
        });

        this.groupVisible = ko.pureComputed(() => {
            this.isGroupVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed || 
                                  this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Stoppage ||
                                  this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.ConcreteDischarge;
            return this.isGroupVisible;
        });

        this.graphicVisible = ko.pureComputed(() => {
            this.isGraphicVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.TrackingPEA ||
                                    this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Driving || 
                                    this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Stoppage ||
                                    //ถ้า เป็น Tracking Report แล้ว เลือก FromDate EndDate ต่างกัน เกิน 7 วัน จะไม่แสดง Check Box Grphic
                                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Tracking && this.isRequiredMailTo() == false); 
            return this.isGraphicVisible;
        });

        this.limitedToSelectedTime = ko.pureComputed(() => {
            this.isLimitedToSelectedTimeVisible =   this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Stoppage ||
                                                    this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.OpenCloseGate ||
                                                    this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.BadGPS ||
                                                    this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed
                   
            return this.isLimitedToSelectedTimeVisible;
        });

        this.categoryVehicleVisible = ko.pureComputed(() => {
            var isCategoryVehicleVisible = this.selectedReportType() && (this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed 
                                        || this.selectedReportType().value === Enums.ModelData.GeneralReportType.TruckNotUpdateGPS)
                                        && (Object.keys(this.categoryVehicleOptions()).length > 1)
                                        ? true : false ;
                                       
            return isCategoryVehicleVisible;
        });

        this.categoryVehicleVisible = ko.pureComputed(() => {
            var isCategoryVehicleVisible = this.selectedReportType() && (this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed 
                                        || this.selectedReportType().value === Enums.ModelData.GeneralReportType.TruckNotUpdateGPS)
                                        && (Object.keys(this.categoryVehicleOptions()).length > 1)
                                        ? true : false ;
                                 
            return isCategoryVehicleVisible;
        });

        this.shippingTypeVisible = ko.pureComputed(() => {
            var isShippingTypeVisible = this.selectedReportType() 
                                        && (this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed 
                                        || this.selectedReportType().value === Enums.ModelData.GeneralReportType.TruckNotUpdateGPS)
                                        && (Object.keys(this.shippingTypeOptions()).length > 1) 
                                        ? true : false ;
            return false;//isShippingTypeVisible;
        });

        this.durationVisible = ko.pureComputed(() => {
            var isDurationVisible = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed ;
            return isDurationVisible;
        });

        this.trackingVisibleGroupStatus = ko.pureComputed(() => {
            return this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Tracking ;
        });
        
        this.visibleTruck = ko.pureComputed(() => {
            var isTrackNotUpdate = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.TruckNotUpdateGPS;
            return !isTrackNotUpdate;
        });
        

        this.visibleAlertType = ko.pureComputed(() => {
                     //ถ้า เป็น Tracking Report แล้ว เลือก FromDate EndDate ต่างกัน เกิน 7 วัน จะไม่แสดง Dropdown AlertType
            return (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.Tracking && this.isRequiredMailTo() == false) ||
                (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.TrackingPEA)
        });

        this.driverPerfomanceRuleVisible = ko.pureComputed(()=>{
            
            return  this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail 
                    
        });

        this.includeDeliquentVisible = ko.pureComputed(()=>{
            return  this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail 
        });
        this.businessUnitMultiselectVisible = ko.pureComputed(()=>{
            return  this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail
        });
        this.businessUnitVisible = ko.pureComputed(()=>{
            return  !(this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail)
        })

        //validate gate
        this.itemsChanged = (newValue) => {
            this.isCheckedGate = _.find(this.gate(), (x) => { return x.isChecked; });
            if(this.isCheckedGate === undefined  || this.isGateVisible.isChecked === false){
                this.requiredGateVisible(true);
            }else{
                this.requiredGateVisible(false);
            }
        }

        this.enableMailTo = ko.pureComputed(() => {
            if (this.selectedReportType() && this.selectedReportType().value == Enums.ModelData.GeneralReportType.Tracking) {
                let dayDiff = (Utility.compareDateAsDays(this.end(), this.start())) + 1;

                this.isRequiredMailTo(dayDiff > 7 ? true : false);
                return this.isRequiredMailTo();
            }
            return false;
        });

        //Use For SentMail Trackloc
        this.subscribeMessage("cw-report-general-sentmail-dialog-success", () => {
            this.mailTo(null);
        });

        //datetime
        this.formatSet = "dd/MM/yyyy";//WebConfig.companySettings.shortDateFormat;
        var addDay = 30;
        var addMonth = 3;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(),addMonth));

        //for disable column
        this.renderItemColumn = (data, type, row) => {
            if(data) {
                return 'Yes';
            }
            return 'No';
        };

        this.radioCriteria = ko.computed(()=>{
            var isRadioCriteria;
            if(this.selectedReportType() && (this.selectedReportType().value === Enums.ModelData.GeneralReportType.TruckNotUpdateGPS )){

                this.start(new Date());
                this.end(new Date());
                this.timeStart("00:00");
                this.timeEnd("23:59");
                this.valueRadio('Today');
                isRadioCriteria = false;

            }else{
            
                isRadioCriteria = true;
            }
           
            return isRadioCriteria;

        })

        this.periodDay = ko.pureComputed((e) => {
            // can't select date more than current date
            addDay = 30;
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }
     
            // set date when clear start date and end date
            if(this.start() === null){
                // set end date when clear start date
                if(this.end() === null){
                    
                    isDate = new Date();
                   
                }else{
                 
                    isDate = this.end();
              
                }
            
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
          
            }else{
                
                this.minDateEnd(this.start())
         
            }

            return isDate;
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.isBusy(true);
            this.isEventDetail = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail;
            this.israckingPEASelected = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.TrackingPEA;
            this.isOverSpeedSelected = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.OverSpeed;
            this.isBadGPSSelected = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.BadGPS;
            this.isTruckSelected = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.TruckNotUpdateGPS;
            this.isConcreteDischarge = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.ConcreteDischarge;
            var vihecleTypeOnly = null;
            var vihecleTypeAndDriverType = this.israckingPEASelected || this.isOverSpeedSelected || this.isBadGPSSelected || this.isParkingTypeSelected || this.isTruckSelected || this.isConcreteDischarge ;
            var businessUnitIds = (this.isEventDetail) ? this.selectedBusinessUnitMultiselect():this.selectedBusinessUnit(); //หากเป็นEvent Detail Report ให้ใช้ค่าจาก bu multiselect
            var result = this.i18n('Common_PleaseSelect')();
            this.selectEntity([]);
            //set object businessUnitIds for include Sub BU
            if(this.isIncludeSubBU() && businessUnitIds != null ){
       
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);

            }
           
            if(this.isTruckSelected){
                vihecleTypeOnly = true;
            }
            //set vihecleType Only
            this.entityTypes((vihecleTypeOnly)?this.tempEntityTypes()[0]:this.tempEntityTypes());

            if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null){
                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle,
                    vehicleCategoryId: this.categoryVehicle().id
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;
                    
                    //add displayName "Report_All" and id 0
                    if(vihecleTypeOnly || vihecleTypeAndDriverType){
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                            
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }else if(Object.keys(listSelectEntity).length == 0 && (this.israckingPEASelected || this.isBadGPSSelected || this.isOverSpeedSelected )) {
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    }else{
                        //remove displayName "Report_All" and id 0
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0){       
                            listSelectEntity.shift();
                        }
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                    this.isBusy(false);
                });
            }else if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null){
                let includeFormerDriver = this.isIncludeFormerDriver() ? true : null;
                let includeResignedDriver = this.isIncludeResignedDriver() ? null : true;

                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver,
                    IncludeFormerInBusinessUnit : this.isIncludeFormerDriver(),
                    isEnable: this.isIncludeResignedDriver() ? null : true ,

                    
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if(vihecleTypeOnly || vihecleTypeAndDriverType){
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }else if(Object.keys(listSelectEntity).length == 0 && (this.israckingPEASelected || this.isBadGPSSelected || this.isOverSpeedSelected)) {
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    }else{
                        //remove displayName "Report_All" and id 0
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0){       
                            listSelectEntity.shift();
                        }
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                    this.isBusy(false);
                });
            }else{
                this.selectEntity([]);
                this.isBusy(false);
            }
            if(this.israckingPEASelected){
                result = "";
            }
        
           return result;
        });

        // Date Time Criteria
        this.valueRadio.subscribe((res) => {
            let currentDate = new Date();
            var startDate = new Date();
            var endDate = new Date();

            switch (res) {
                case 'Today':
                this.isDateTimeEnabled(false);
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;

                case 'Yesterday':
                    
                    this.isDateTimeEnabled(false);
    
                    startDate.setDate(startDate.getDate() - 1)
                    // console.log("startDate.setDate(startDate.getDate() - 1)",startDate)
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(startDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;
            
                case 'ThisWeek':
                    this.isDateTimeEnabled(false);
                    // let startWeek = parseInt(startDate.getDate() - (startDate.getDay() - 1));
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    let startWeek;
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    //setStartDate
                    if(currentDate.getDay() == 0){ //ถ้าเป็นวันอาทิตย์ ( 0 คือวันอาทิตย์ )
              
                        startWeek = parseInt((startDate.getDate() - 6));

                    }else if(currentDate.getDay() == 1) { //วันจันทร์
                 
                        startWeek = parseInt((startDate.getDate() ));

                    }
                    else{
                    
                        startWeek = parseInt((startDate.getDate() - (startDate.getDay()-1)));
                    }
                  
                    startDate.setDate(startWeek)

                    //setEndDate
                    if(currentDate.getDay() == 1){ //ถ้าเป็นวันจันทร์ ( 1 คือวันจันทร์ )
                        endDate.setDate(currentDate.getDate());
                        this.maxDateStart(new Date(),0);
                    }
                    else{
                        endDate.setDate(currentDate.getDate());
                    }

                    // startDate.setDate(startWeek)
                    // endDate.setDate(endWeek)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                   

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;

                case 'LastWeek':

                    this.isDateTimeEnabled(false);
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek)
                    endDate.setDate(LastendWeek - 1)

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));

                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(startDate));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;
                
                case 'ThisMonth':
                    this.isDateTimeEnabled(false);
                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);


                    this.start(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));
                    $("#from").val(this.setFormatDisplayDateTime(firstDay));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(firstDay));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(lastDay));
                    break;
                case 'LastMonth':
                    this.isDateTimeEnabled(false);
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.start(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#to").val(this.setFormatDisplayDateTime(LastlastDay));

                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;
                case 'Custom':
                    this.isDateTimeEnabled(true)
                    break;

                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });

    //     this.isDateTimeEnabled = ko.computed(() => {
    //         console.log(this.valueRadio());
    //         return this.valueRadio() == 'Custom';
    //    });
    //    console.log("this.isDateTimeEnabled",this.isDateTimeEnabled());

        // For PEA
        this.isIncludeSubBUVisible = ko.observable(true);
        this.isRequiredEntity = ko.observable(true);
       

        this.selectedReportType.subscribe((data) => {

            if (this.selectedReportType() && data.value === Enums.ModelData.GeneralReportType.TrackingPEA) {

                this.isIncludeSubBUVisible(false);
                this.isRequiredEntity(false);
                // this.selectedBusinessUnit.extend({ required: (!this.isRequiredEntity()) ? false : true });
                
            }
            else{
                this.isIncludeSubBUVisible(true);
                this.isRequiredEntity(true);
            }

            if (this.selectedReportType() && data.value === Enums.ModelData.GeneralReportType.EventDetail) {

                this.isIncludeSubBUVisible(false);    
            }else{
                this.isIncludeSubBUVisible(true);
            }


            if (this.selectedReportType() && data.value === Enums.ModelData.GeneralReportType.Tracking) {
                this.valueRadio("Today");
            }

        })
        
    }
    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data)
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

       

        return newDateTime;
    }
    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }

    setupExtend() {
        var self = this;
        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation
     
        this.selectedReportType.extend({ required: true });
        // this.selectedBusinessUnit.extend({ required: true });
        
        //Entity Type
        this.selectedEntityType.extend({ required: true });
        // this.selectedEntity.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        this.selectedEventType.extend({ required: true });
        //Truck Not Update 
        this.truckNotDuration.extend({required: true});

        //
        // this.selectedDriverPerformanceRule.extend({required: ()});

        // this.selectedParkingType.extend({ required: true });
        // this.end = this.end.extend({
        //     date: true,
        //     min:{
        //         params:this.start,
        //         message:"Please select date greate than or equal to \"From Date\""
        //     }
        // });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });

        this.selectedBusinessUnit.extend({ 
            required: {
                onlyIf: () => {
                    return (this.selectedReportType() && this.selectedReportType().value !== Enums.ModelData.GeneralReportType.EventDetail);
                }
            }
         });

        this.selectedDriverPerformanceRule.extend({ 
            required: {
                onlyIf: () => {
                    return (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail);
                }
            }
         });
         this.selectedBusinessUnitMultiselect.extend({ 
            required: {
                onlyIf: () => {
                    return (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail);
                }
            }
         })

        //PEA
        this.selectedEntity.extend({ 
            required: {
                onlyIf: () => {
                    return (this.isRequiredEntity());
                }
            }
         });

        
        this.mailTo.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!ko.validation.utils.isEmptyVal(val)) {
                        // use the required: true property if you don't want to accept empty values
                        var values = val.split(',');
                        $(values).each(function (index) {
                            isValid = ko.validation.rules['email'].validator($.trim(this), validate);
                            return isValid; // short circuit each loop if invalid
                        });
                    }
                    return isValid;
                },
                message: this.i18n("M064")()
            },
            required: {
                onlyIf: function () {
                    return self.isRequiredMailTo();
                }
            }
        });

        // With custom message
        this.validationModel = ko.validatedObservable({
            selectedReportType: this.selectedReportType,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedEntityType: this.selectedEntityType,
            selectedEntity: this.selectedEntity,
            end:this.end,
            start:this.start,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd,
            truckNotDuration:this.truckNotDuration,
            selectedDriverPerformanceRule:this.selectedDriverPerformanceRule,
            selectedBusinessUnitMultiselect: this.selectedBusinessUnitMultiselect,
            mailTo: this.mailTo
        });

        this.validationEventType = ko.validatedObservable({
            selectedEventType:this.selectedEventType,
        });

        // this.validationParkingType = ko.validatedObservable({
        //     selectedParkingType:this.selectedParkingType
        // });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestDriveRule() {
        return WebRequestDriveRule.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }
    

    getReportTemplateType() {
        var reportType = new Array();
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_Tracking)) {
            reportType.push(Enums.ModelData.GeneralReportType.Tracking);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_Driving)) {
            reportType.push(Enums.ModelData.GeneralReportType.Driving);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_BadGPS)) {
            reportType.push(Enums.ModelData.GeneralReportType.BadGPS);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_Stoppage)) {
            reportType.push(Enums.ModelData.GeneralReportType.Stoppage);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_OpenCloseGate)) {
            reportType.push(Enums.ModelData.GeneralReportType.OpenCloseGate);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_OverSpeed)) {
            reportType.push(Enums.ModelData.GeneralReportType.OverSpeed);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_TruckNotUpateGPS)) {
            reportType.push(Enums.ModelData.GeneralReportType.TruckNotUpdateGPS);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_ConcreteDischarge)) {
            reportType.push(Enums.ModelData.GeneralReportType.ConcreteDischarge);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_TrackingPEA)) {
            reportType.push(Enums.ModelData.GeneralReportType.TrackingPEA);
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_EventReport)) {
            reportType.push(Enums.ModelData.GeneralReportType.EventDetail);
        }
        
        
       
        if(reportType.length < 1)
        {
            reportType = [0];
        }
   
        return reportType;
    }

    setShipmentDateMaxMin(dateTime) {
        if(dateTime == undefined) {
            this.minDateShipment(Utility.minusMonths(new Date(), 3));
            this.maxDateShipment(Utility.addMonths(new Date(), 3));            
        }  else {
            this.minDateShipment(new Date(dateTime));
            this.maxDateShipment(new Date(dateTime));
        }  
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        this.start.subscribe((dateTime) => {
            let startDate = new Date(dateTime);
            let endDate = new Date(this.end());
            if(startDate > endDate) {
                this.setShipmentDateMaxMin(dateTime);
                this.setShipmentDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.maxDateShipment(Utility.addMonths(dateTime, 1));
                this.setShipmentDateMaxMin();
            }
        });

        // EndDate condition
        this.end.subscribe((dateTime) => {
            let startDate = new Date(this.start());
            let endDate = new Date(dateTime);
            if(endDate < startDate) {
                this.setShipmentDateMaxMin(dateTime);
                this.setShipmentDateMaxMin();
            }

            if((startDate.getMonth() - endDate.getMonth()) != 0) {
                this.minDateShipment(Utility.minusMonths(dateTime, 1));
                this.setShipmentDateMaxMin();
            }
        });

        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.GeneralReportType],
            values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, 
                     Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var enumParkingTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.ParkingType],
            values: [Enums.ModelData.ParkingType.InsidePOI, 
                     Enums.ModelData.ParkingType.OutsidePOI, 
                     Enums.ModelData.ParkingType.SpecifiedInsidePOI],
            sortingColumns: DefaultSorting.EnumResource
        };

        var enumEventTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.DrivingReportType],
            values: [
                        Enums.ModelData.DrivingReportType.OverSpeed,
                        Enums.ModelData.DrivingReportType.DecRapidDrive, 
                        Enums.ModelData.DrivingReportType.AccRapidDrive,
                        Enums.ModelData.DrivingReportType.SwerveDrive
                    ],
            sortingColumns: DefaultSorting.EnumResource
        };

        var filterTemperature = {
            unit:2,
            sortingColumns: DefaultSorting.BoxFeature
        };

        var filterGate = {
            unit:4,
            sortingColumns: DefaultSorting.BoxFeature
        };

        var filterCategoryVehicle = {
            categoryId:Enums.ModelData.CategoryType.Vehicle,
        };

        var filterShippingType = {
            categoryId:Enums.ModelData.CategoryType.ShippingType,
        };
        var filterAlertType = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.AlertType],
            excludeAlertTypes: [
                Enums.ModelData.AlertType.InvalidDriver,
                Enums.ModelData.AlertType.TemperatureError]
        };
        var driverRuleFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            enable: true
        }

        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);
        var d4 = this.webRequestEnumResource.listEnumResource(enumParkingTypeFilter);
        var d5 = this.webRequestEnumResource.listEnumResource(enumEventTypeFilter);
        var d6 = this.webRequestFeature.listFeatureSummary(filterTemperature);
        var d7 = this.webRequestFeature.listFeatureSummary(filterGate);
        var d8 = this.webRequestCategory.listCategory(filterCategoryVehicle);
        var d9 = this.webRequestCategory.listCategory(filterShippingType);
        var d10 = this.webRequestAlertType.listAlertTypeByAlertConfig(filterAlertType)
        var d11 = this.webRequestDriveRule.listDriveRuleSummary(driverRuleFilter);
        $.when(d1, d2, d3, d4, d5, d6, d7, d8, d9,d10,d11).done((r1, r2, r3, r4, r5, r6, r7, r8, r9,r10,r11) => {
    

            //for add index, value first item and add id all array list
            if(Object.keys(r6.items).length > 0){
                let i = 1;
                r6.items.forEach(function(arr) {
                    arr.tempId = i++;
                });
                // r6.items[0].isChecked = true;
                // r6.items[0].enable = false;
            }

            if(Object.keys(r7.items).length > 0){
                let i = 1;
                r7.items.forEach(function(arr) {
                    arr.gateId = i++;
                });
                // r5.items[0].isChecked = true;
                // r5.items[0].enable = false;
            }

   
            r8.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });

            r9.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });
            r10.items.unshift({
                //name: this.i18n('Report_All')(),
                displayName : this.i18n('Report_All')(),
                id: 0
            });
            

            let newReportTypesObj;
         
            if (WebConfig.userSession.hasPermission(Constants.Permission.GeneralReport_TrackingPEA)) { //โค้ด if นี้สำหรับสลับตำแหน่ง ให้ TrackingPEA ขึ้นมาอยู่indexที่1

                let trackingPEAReportType = r1.items.filter((items) => {
                    return items.value == 9;
                  })
                newReportTypesObj = r1.items.filter((items) => {
                    return items.value != 9;
                  })
                newReportTypesObj.splice(0, 0, trackingPEAReportType[0]);

                // console.log("newReportTypesObj",newReportTypesObj);
            }else{
                newReportTypesObj = r1.items;
            }
           
            
            this.reportTypes(newReportTypesObj);
            this.businessUnitsMultiselectOption(r2.items) //MultiSelect BU
            this.businessUnits(r2["items"]);
            this.tempEntityTypes(r3["items"]);
            this.parkingTypes(r4["items"]);
            this.eventTypes(r5["items"]);
            //set temperature array 4 list
            this.temperature(r6.items.slice(0, 4));
            this.gate(r7.items);
            this.categoryVehicleOptions(r8.items);
            this.shippingTypeOptions(r9.items);
            this.alertTypes(r10.items);

            this.driverPerformancesRuleOption(r11.items);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;

    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        // console.log("this.selectedBusinessUnitMultiselect()",this.selectedBusinessUnitMultiselect())
        if(sender.id === "actPreview") {

            var objGate = new Array();
            this.gate().forEach(function(arr) {
                objGate.push({
                    id:arr.id,
                    name:arr.name,
                    code:arr.code,
                    isChecked:(arr.isChecked)?true:false
                });
            });
           

            //for validate select type
            var reportTitle = '';
            var reportNamespace = '';
            var reportParam = {};
            var selectedEntityId = null;
            var IncludeSubBU = (this.isIncludeSubBU())?1:0;
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";
            var objGateJSON = JSON.stringify(objGate);
            var validateGate = this.isCheckedGate;

            //validate isChecked Gate
            if(validateGate === undefined  && this.isGateVisible){
                this.requiredGateVisible(true);
                return false;
            }
            
			if(this.isEventTypeVisible){
				if(!this.validationEventType.isValid()){
					this.validationEventType.errors.showAllMessages();
					return false;
				}
			}
			
            if (this.validationModel.isValid()) {
                this.isEventDetail = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.GeneralReportType.EventDetail;
                var repportParameters = {
                    ReportTypeID:this.selectedReportType().value,
                    UserID:WebConfig.userSession.id,
                    CompanyID:WebConfig.userSession.currentCompanyId,
                    BusinessUnitID: (this.isEventDetail)?this.selectedBusinessUnitMultiselect() : this.selectedBusinessUnit(),//โค้ดส่วนนี้อาจจะพังเพราะปัจจุบันตัวReportไม่รับ Bu เป็น Array แต่ EventDetailReport จะส่งBuไปเป็นArray
                    // BusinessUnitID:"91",
                    IncludeSubBusinessUnit:IncludeSubBU,
                    EntityTypeID:this.selectedEntityType().value,
                    SelectedEntityID:(this.selectedEntity()) ? this.selectedEntity().id : 0,
                    SDate:formatSDate,
                    EDate:formatEDate,
                    EventID:(this.selectedEventType() && this.selectedEventType().value)?this.selectedEventType().value:null,
                    ParkingTypeID:(this.selectedParkingType() && this.selectedParkingType().value)?this.selectedParkingType().value:null,
                    CustomPOI:this.customPoiValue(),
                    Gate:objGateJSON,
                    LastParking:(this.isLastParking())?1:0,
                    Group:(this.isGroup())?1:0,
                    LimitedToSelectedTime:(this.isLimitedToSelectedTime())?1:0,
                    Time:this.overSpeedDuration(),
                    IncludeFormerInBusinessUnit: this.isIncludeFormerDriver() ? 1 : 0 ,
                    Enable: this.isIncludeResignedDriver() ? 1 : 0 ,
                    CategoryID: this.categoryVehicle().id,
                    ShippingType: (this.shippingType()) ? this.shippingType().id : 0,
                    TruckDuration : (this.truckNotDuration()) ? this.truckNotDuration() : 0,
                    alertTypeId : this.selectedAlertType()  && this.selectedAlertType().id ? this.selectedAlertType().id : null,
                    drivingPerformanceRule :(this.selectedDriverPerformanceRule())?this.selectedDriverPerformanceRule().id : null,
                    IsIncludeDelinquent: (this.isIncludeDelinquent()) ? 1 : 0,
                    mailTo: this.mailTo(),
                    // IsIncludeDelinquent:true,
                    IsGroup : (this.isGroupParkStatus()) ? 1 : 0,

                    PrintBy:WebConfig.userSession.fullname,
                    Language:WebConfig.userSession.currentUserLanguage
                };
                
                var reportExport = new Object();
                switch (this.selectedReportType() && this.selectedReportType().value) {

                    case Enums.ModelData.GeneralReportType.Tracking:
                        if(this.isGraphic())
                            reportNamespace = 'Gisc.Csd.Service.Report.Library.General.TracklocReport, Gisc.Csd.Service.Report.Library';
                        else
                            reportNamespace = 'Gisc.Csd.Service.Report.Library.General.TracklocReportNoGraphic, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_Trackloc';
                        reportExport.CSV = this.webRequestReportExports.exportCSVTracklocReport();
                        break;
                    case Enums.ModelData.GeneralReportType.Driving:
                        if(this.isGraphic())
                            reportNamespace = 'Gisc.Csd.Service.Report.Library.General.DriveReport, Gisc.Csd.Service.Report.Library';
                        else
                            reportNamespace = 'Gisc.Csd.Service.Report.Library.General.DriveReportNoGraphic, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_Driving';
                        reportExport.CSV = this.webRequestReportExports.exportCSVDrivingReport();
                        break;
                    case Enums.ModelData.GeneralReportType.Stoppage:
                        if(this.isGraphic())
                            reportNamespace = 'Gisc.Csd.Service.Report.Library.General.StoppageReport, Gisc.Csd.Service.Report.Library';
                        else
                            reportNamespace = 'Gisc.Csd.Service.Report.Library.General.StoppageReportNoGraphic, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_Stoppage';
                        //set index Temperature and value
                        this.temperature().forEach(function(arr) {
                            reportParam["Temperature"+arr.tempId] = ((arr.isChecked)?1:0);
                        });
                        reportExport.CSV = this.webRequestReportExports.exportCSVParkingReport();
                        break;
                    case Enums.ModelData.GeneralReportType.OpenCloseGate:
                        reportNamespace = 'Gisc.Csd.Service.Report.Library.General.GateOpenReport, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_GateOpenClose';
                        reportExport.CSV = this.webRequestReportExports.exportCSVOpenCloseReport();
                        break;
                    case Enums.ModelData.GeneralReportType.BadGPS:
                        reportNamespace = 'Gisc.Csd.Service.Report.Library.General.BadGpsReport, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_BadGps';
                        reportExport.CSV = this.webRequestReportExports.exportCSVBadGpsReport();
                        break;
                    case Enums.ModelData.GeneralReportType.OverSpeed:
                        reportNamespace = (this.isGroup()) ? 'Gisc.Csd.Service.Report.Library.General.OverspeedReport, Gisc.Csd.Service.Report.Library' : 'Gisc.Csd.Service.Report.Library.General.OverSpeedReport_UnGroup, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_OverSpeed';
                        reportExport.CSV = this.webRequestReportExports.exportCSVOverSpeedReport();
                        break;
                    case Enums.ModelData.GeneralReportType.ConcreteDischarge:
                        reportNamespace = (this.isGroup()) ? 'Gisc.Csd.Service.Report.Library.General.ConcreteDischargeReport, Gisc.Csd.Service.Report.Library' : 'Gisc.Csd.Service.Report.Library.General.ConcreteDischargeReport_UnGroup, Gisc.Csd.Service.Report.Library' ;
                        reportTitle = 'Report_ConcreteDischargeReport';
                        //reportExport.CSV = this.webRequestReportExports.exportCSVTruckNotUpdateReport();
                        break;
                        
                    case Enums.ModelData.GeneralReportType.TruckNotUpdateGPS:
                        reportNamespace = 'Gisc.Csd.Service.Report.Library.General.TruckNotUpdateReport, Gisc.Csd.Service.Report.Library' ;
                        reportTitle = 'Report_TruckNotUpdateGPS';
                        reportExport.CSV = this.webRequestReportExports.exportCSVTruckNotUpdateReport();
                        break;
                    
                    case Enums.ModelData.GeneralReportType.TrackingPEA:
                    if(this.isGraphic())
                        reportNamespace = 'Gisc.Csd.Service.Report.Library.General.TracklocReport, Gisc.Csd.Service.Report.Library';
                    else
                        reportNamespace = 'Gisc.Csd.Service.Report.Library.General.TracklocReportNoGraphic, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_Trackloc';
                        reportExport.CSV = this.webRequestReportExports.exportCSVTracklocReport();
                    break;

                    case Enums.ModelData.GeneralReportType.EventDetail:
                        reportNamespace = 'Gisc.Csd.Service.Report.Library.General.EventDetailReport, Gisc.Csd.Service.Report.Library';
                        reportTitle = 'Report_Event_Detail';
                        // reportExport.CSV = this.webRequestReportExports.exportCSVTracklocReport();
                    break;
                  
                }
                this.reports = {
                    report:reportNamespace, 
                    parameters: repportParameters
                };

                // console.log("STRINGIFY", JSON.stringify(this.reports.parameters));
                //console.log("this.report", this.reports.parameters);
                if (this.isRequiredMailTo()) { //ถ้า เป็น Tracking Report แล้ว เลือก FromDate EndDate ต่างกัน เกิน 7 วัน จะเข้าสู่ การส่ง mail แทนการ preview report
                    this.widget('cw-report-general-sentmail-dialog', repportParameters, {
                        title: this.i18n('Report_TitleDialogTrackloc'),
                        modal: true,
                        resizable: false,
                        minimize: false,
                        target: "cw-report-general-sentmail-dialog",
                        width: "500",
                        height: "162",
                        id: 'cw-report-general-sentmail-dialog',
                        left: "50%",
                        bottom: "50%"
                    });
                    
                }
                else {
                    this.navigate("cw-report-reportviewer", { reportSource: this.reports, reportName: this.i18n(reportTitle)(), reportExport: reportExport });
                }
                

            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }
        } 
    }
}

export default {
viewModel: ScreenBase.createFactory(FormGeneralReport),
    template: templateMarkup
};