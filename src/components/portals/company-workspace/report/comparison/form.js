import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums, Constants} from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class FormOverspeed extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.selectedReportType = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedEntityType = ko.observable();
        this.selectedEntity = ko.observable();
        this.selectedFuelType = ko.observable();

        this.isIncludeFormerDriver = ko.observable(false);
        this.isIncludeResignedDriver = ko.observable(false);
        
        this.reportTypes = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.entityTypes = ko.observableArray([]);
        this.selectEntity = ko.observableArray([]);
        this.fuelType = ko.observableArray([]);

        //Temperature
        this.temperature = ko.observableArray([]);
        this.selectedItemTemp = ko.observable();
        this.orderTemp = ko.observable([[ 1, "asc" ]]);
        this.isCheckedTempError = ko.observable(false);

        //Gate
        this.gate = ko.observableArray([]);
        this.selectedItemGate = ko.observable();
        this.orderGate = ko.observable([[ 1, "asc" ]]);
        this.isCheckedGateOpen = ko.observable(false);
        this.isCheckedOnlyOverSpeed = ko.observable(false);
        this.isCheckedShowTable = ko.observable(false);
        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        //for disable column
        this.renderItemColumn = (data, type, row) => {
            if(data) {
                return 'Yes';
            }
            return 'No';
        };

        this.vehicleVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle;
        });

        this.driverVisible = ko.pureComputed(() => {
            return this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver;
        });

        this.temperatureVisible = ko.pureComputed(() => {
            let selectedReportType = (
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.ComparisonReportType.FuelTemperature) || 
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.ComparisonReportType.TemperatureSpeed) || 
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.ComparisonReportType.TemperatureGate)
                );
            return selectedReportType;
        });

        this.gateVisible = ko.pureComputed(() => {
            let selectedReportType = this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.ComparisonReportType.TemperatureGate;
            return selectedReportType;
        });

        this.onlyOverSpeedVisible = ko.pureComputed(() => {
            let selectedReportType = (
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.ComparisonReportType.FuelSpeed) || 
                    (this.selectedReportType() && this.selectedReportType().value === Enums.ModelData.ComparisonReportType.TemperatureSpeed)
                );
            return selectedReportType;
        });

        this.periodDay = ko.pureComputed(()=>{
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }

            // set date when clear start date and end date
            if(this.start() == null){
                // set end date when clear start date
                if(this.end() == null){
                    isDate = new Date();
                }else{
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            }else{
                this.minDateEnd(this.start())
            }

            return isDate;
        });
            
        this.selectedEntityBinding = ko.pureComputed(() => {
            var vihecleTypeOnly = null;
            var vihecleTypeAndDriverType = null;
            var businessUnitIds = this.selectedBusinessUnit();
            var result = this.i18n('Common_PleaseSelect')();
            this.selectEntity([]);
            //set object businessUnitIds for include Sub BU
            if(this.isIncludeSubBU() && businessUnitIds != null){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }
            //set vihecleType Only
            this.entityTypes((vihecleTypeOnly)?this.tempEntityTypes()[0]:this.tempEntityTypes());
            
            if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null){
                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if(vihecleTypeOnly || vihecleTypeAndDriverType){
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    }else{
                        //remove displayName "Report_All" and id 0
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0){       
                            listSelectEntity.shift();
                        }
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            }else if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null){
                let includeFormerDriver = this.isIncludeFormerDriver() ? true : null;
                let includeResignedDriver = this.isIncludeResignedDriver() ? null : true;

                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver,
                    IncludeFormerInBusinessUnit : this.isIncludeFormerDriver(),
                    isEnable: this.isIncludeResignedDriver() ? null : true ,

                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if(vihecleTypeOnly || vihecleTypeAndDriverType){
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                            listSelectEntity.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        }
                    }else{
                        //remove displayName "Report_All" and id 0
                        if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id === 0){       
                            listSelectEntity.shift();
                        }
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            }else{
                this.selectEntity([]);
            }

            return result;
        });

    }

    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedReportType.extend({ required: true });
        this.selectedBusinessUnit.extend({ required: true });
        //Entity Type
        this.selectedEntityType.extend({ required: true });
        this.selectedEntity.extend({ required: true });
        this.selectedFuelType.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        // this.end = this.end.extend({
        //     date: true,
        //     min:{
        //         params:this.start,
        //         message:"Please select date greate than or equal to \"From Date\""
        //     }
        // });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedReportType: this.selectedReportType,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedEntityType: this.selectedEntityType,
            selectedEntity: this.selectedEntity,
            selectedFuelType: this.selectedFuelType,
            end:this.end,
            start:this.start,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    getReportTemplateType() {
        var reportType = new Array();
        if (WebConfig.userSession.hasPermission(Constants.Permission.ComparisonReport_FuelTemperature)) {
            reportType.push(Enums.ModelData.ComparisonReportType.FuelTemperature);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ComparisonReport_TemperatureSpeed)) {
            reportType.push(Enums.ModelData.ComparisonReportType.TemperatureSpeed);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ComparisonReport_FuelSpeed)) {
            reportType.push(Enums.ModelData.ComparisonReportType.FuelSpeed);
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ComparisonReport_TemperatureGate)) {
            reportType.push(Enums.ModelData.ComparisonReportType.TemperatureGate);
        }
        if(reportType.length < 1)
        {
            reportType = [0];
        }
        return reportType;
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
 
        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.ComparisonReportType],
            values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var filterTemperature = {
            unit:2,
            sortingColumns: DefaultSorting.BoxFeature
        };

        var filterGate = {
            unit:4,
            sortingColumns: DefaultSorting.BoxFeature
        };

        var enumFuelTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.FuelType],
            values: [],
            sortingColumns: DefaultSorting.EnumResource
        };

        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);
        var d4 = this.webRequestFeature.listFeatureSummary(filterTemperature);
        var d5 = this.webRequestFeature.listFeatureSummary(filterGate);
        var d6 = this.webRequestEnumResource.listEnumResource(enumFuelTypeFilter);

        $.when(d1, d2, d3, d4, d5, d6).done((r1, r2, r3, r4, r5, r6) => {
            
            //for add index, value first item and add id all array list
            if(Object.keys(r4.items).length > 0){
                let i = 1;
                r4.items.forEach(function(arr) {
                    arr.tempId = i++;
                });
                r4.items[0].isChecked = true;
                r4.items[0].enable = false;
            }
            
            if(Object.keys(r5.items).length > 0){
                let i = 1;
                r5.items.forEach(function(arr) {
                    arr.gateId = i++;
                });
                r5.items[0].isChecked = true;
                r5.items[0].enable = false;
            }
            
            this.reportTypes(r1.items);
            this.businessUnits(r2.items);
            this.tempEntityTypes(r3.items);
            this.temperature(r4.items);
            this.gate(r5.items);

            this.fuelType(r6.items);
            //find Litre
            var fuelTypeObj = ScreenHelper.findOptionByProperty(this.fuelType, "value", 2);
            //select Litre is default
            this.selectedFuelType(fuelTypeObj);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {
            // console.log(this.isIncludeFormerDriver());
            var objGate = new Array();
            var objTemperature = new Array();
            var IncludeSubBU = (this.isIncludeSubBU())?1:0;
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";
            this.gate().forEach(function(arr) {
                objGate.push({
                    id:arr.id,
                    name:arr.name,
                    code:arr.code,
                    isChecked:(arr.isChecked)?true:false
                });
            });
            this.temperature().forEach(function(arr) {
                objTemperature.push({
                    id:arr.id,
                    name:arr.name,
                    code:arr.code,
                    isChecked:(arr.isChecked)?true:false
                });
            });
            var objGateJSON = JSON.stringify(objGate);
            var objTemperatureJSON = JSON.stringify(objTemperature);

            // varidate form report and set param repoprt
            if(this.validationModel.isValid()) {

                var objReport = this.reports = {
                    report:null, 
                                    parameters: {
                                                    UserID:WebConfig.userSession.id,
                                                    CompanyID:WebConfig.userSession.currentCompanyId,
                                                    ReportTypeID:this.selectedReportType().value,
                                                    BusinessUnitID:this.selectedBusinessUnit(),
                                                    IncludeSubBusinessUnit:IncludeSubBU,
                                                    EntityTypeID:this.selectedEntityType().value,
                                                    SelectedEntityID:this.selectedEntity().id,
                                                    FuelType:this.selectedFuelType().value,
                                                    Gate:objGateJSON,
                                                    Temperature:objTemperatureJSON,
                                                    SDate:formatSDate,
                                                    EDate:formatEDate,
                                                    IsShowTable:(this.isCheckedShowTable()) ? 1 : 0,
                                                    PrintBy:WebConfig.userSession.fullname,
                                                    Language:WebConfig.userSession.currentUserLanguage,
                                                    IncludeFormerInBusinessUnit: this.isIncludeFormerDriver() ? 1 : 0 ,
                                                    Enable: this.isIncludeResignedDriver() ? 1 : 0 ,
                                                }
                                };

                //switch case for selected report type
                var reportTitle = null;
                var reportExport = new Object();
                switch (this.selectedReportType() && this.selectedReportType().value) {
                    case Enums.ModelData.ComparisonReportType.FuelTemperature:
                        objReport.report = "Gisc.Csd.Service.Report.Library.Comparison.FuelTemperatureReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "ComparisonReport_FuelTemperature";
                        // //set index Temperature and value
                        // this.temperature().forEach(function(arr) {
                        //     objReport.parameters["Temperature"+arr.tempId] = ((arr.isChecked)?1:0);
                        // });
                        // //set index gate and value 0 for gate is hidden
                        // this.gate().forEach(function(arr) {
                        //     objReport.parameters["Gate"+arr.gateId] = 0;
                        // });
                        objReport.parameters.IsAlertTemperature = (this.isCheckedTempError())?1:0;
                        objReport.parameters.IsGateOpen = 0;
                        objReport.parameters.IsOverSpeed = 0;
                        break;
                    case Enums.ModelData.ComparisonReportType.TemperatureSpeed:
                        objReport.report = "Gisc.Csd.Service.Report.Library.Comparison.TemperatureSpeedReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "ComparisonReport_TemperatureSpeed";
                        // //set index Temperature and value
                        // this.temperature().forEach(function(arr) {
                        //     objReport.parameters["Temperature"+arr.tempId] = ((arr.isChecked)?1:0);
                        // });
                        // //set index gate and value 0 for gate is hidden
                        // this.gate().forEach(function(arr) {
                        //     objReport.parameters["Gate"+arr.gateId] = 0;
                        // });
                        objReport.parameters.IsAlertTemperature = (this.isCheckedTempError())?1:0;
                        objReport.parameters.IsGateOpen = 0;
                        objReport.parameters.IsOverSpeed = (this.isCheckedOnlyOverSpeed())?1:0;
                        break;
                    case Enums.ModelData.ComparisonReportType.FuelSpeed:
                        objReport.report = "Gisc.Csd.Service.Report.Library.Comparison.FuelSpeedReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "ComparisonReport_FuelSpeed";
                        // //set index Temperature and value 0 for temperature is hidden
                        // this.temperature().forEach(function(arr) {
                        //     objReport.parameters["Temperature"+arr.tempId] = 0;
                        // });
                        // //set index gate and value 0 for gate is hidden
                        // this.gate().forEach(function(arr) {
                        //     objReport.parameters["Gate"+arr.gateId] = 0;
                        // });
                        objReport.parameters.IsAlertTemperature = 0;
                        objReport.parameters.IsGateOpen = 0;
                        objReport.parameters.IsOverSpeed = (this.isCheckedOnlyOverSpeed())?1:0;
                        break;
                    case Enums.ModelData.ComparisonReportType.TemperatureGate:
                        objReport.report = "Gisc.Csd.Service.Report.Library.Comparison.TemperatureGateReport, Gisc.Csd.Service.Report.Library";
                        reportTitle = "ComparisonReport_TemperatureGate";
                        // //set index Temperature and value
                        // this.temperature().forEach(function(arr) {
                        //     objReport.parameters["Temperature"+arr.tempId] = ((arr.isChecked)?1:0);
                        // });
                        // //set index gate and value
                        // this.gate().forEach(function(arr) {
                        //     objReport.parameters["Gate"+arr.gateId] = ((arr.isChecked)?1:0);
                        // });
                        objReport.parameters.IsAlertTemperature = (this.isCheckedTempError())?1:0;
                        objReport.parameters.IsGateOpen = (this.isCheckedGateOpen())?1:0;
                        objReport.parameters.IsOverSpeed = 0;
                        break;
                    default:
                        break;
                }
                
                reportExport.CSV = this.webRequestReportExports.exportCSVComparisonReport();
                this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n(reportTitle)(), reportExport:reportExport});

            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(FormOverspeed),
    template: templateMarkup
};