﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../screenBase";
import templateMarkup from "text!./reports.html";
import * as BladeSize from "../../../../app/frameworks/constant/BladeSize";
import * as BladeType from "../../../../app/frameworks/constant/BladeType";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";

class SubscriptionsListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(params.reportName);
        this.bladeSize = BladeSize.XLarge_Report;
        this.bladeType = BladeType.Compact;
        this.minimizeAll(false);
        this.bladeCanMaximize = false;
        // Prepare report configuration.
        this.reportUrl = this.resolveUrl(WebConfig.reportSession.reportUrl); 
        this.reportSource = params.reportSource;
        this.reportExport = params.reportExport;

    }


    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

    }


    onDomReady () {

        var self = this;

        window.SPAMapManager = function(lat,lon){

            let clickLocation = {
                lat: lat,
                lon: lon
            },
            symbol = {
                url: self.resolveUrl("~/images/pin_destination.png"),
                width: 35,
                height: 50,
                offset: {
                    x: 0,
                    y: 0
                },
                rotation: 0
            },
            zoom = 17,
            attributes = null

            MapManager.getInstance().drawOnePointZoom(self.id, clickLocation, symbol, zoom, attributes);
            self.minimizeAll();

        }
    }


    onLoad(isFirstLoad){

        if (!isFirstLoad) {
            return;
        }

    }

    onUnload () {
        window.SPAMapManager = null;
    }


}

export default {
    viewModel: ScreenBase.createFactory(SubscriptionsListScreen),
    template: templateMarkup
};