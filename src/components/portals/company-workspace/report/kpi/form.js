﻿import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 **/

class FormKpiReport extends ScreenBase {
    constructor(params) {
        super(params);

        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.businessUnits = ko.observableArray([]);
        this.selectedBusinessUnit = ko.observable(null);

        this.shippingTypeOptions = ko.observableArray([]);
        this.shippingType = ko.observable();
        
        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var dateTime = Utility.addDays(new Date(), -1);
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(dateTime);
        this.end = ko.observable(dateTime);
        this.startTime = ko.observable("00:00");
        this.endTime = ko.observable("23:59");
        this.maxDateStart = ko.observable(dateTime);

        this.minDateEnd = ko.observable(dateTime);

        this.periodDay = ko.pureComputed(()=>{

            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            
            if(calDay > 0){
                isDate = Utility.addDays(new Date(),-1);
            }

            // set date when clear start date and end date
            if(this.start() == null){
                // set end date when clear start date
                if(this.end() == null){
                    isDate = new Date();
                }else{
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            }else{
                this.minDateEnd(this.start());
            }

            if(this.isSelectedReportType){
                isDate = Utility.addDays(isDate,-1);
            }
        
            return isDate;
        });   
    }

    setupExtend() {

        this.selectedBusinessUnit.extend({required: true});
        this.start.extend({ required: true });
        this.end.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedBusinessUnit : this.selectedBusinessUnit,
            start : this.start,
            end : this.end
        });
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }

    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        let dfd = $.Deferred();
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };
        let filterShippingType = {
            categoryId:Enums.ModelData.CategoryType.ShippingType,
        };

        let dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        let dfdShippingType = this.webRequestCategory.listCategory(filterShippingType);

        $.when(dfdBusinessUnit,dfdShippingType).done((responseBu,responseShippingType) =>{

            responseShippingType.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });
            this.businessUnits(responseBu["items"]);
            this.shippingTypeOptions(responseShippingType.items);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;

    }

    buildActionBar(actions) {
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {

        if(sender.id === "actPreview") {
            if(!this.validationModel.isValid()){
                this.validationModel.errors.showAllMessages();
                return false;
            }
            console.log("this.startdate>>",this.start());

            let formatStartDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + "T" + this.startTime() + ":00";
            let formatEndDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + "T" + this.endTime() + ":00";

            let formatStartDateForDisplay = $.datepicker.formatDate("dd/mm/yy", new Date(this.start()));
            let formatEndDateForDisplay = $.datepicker.formatDate("dd/mm/yy", new Date(this.end()));

            console.log("WebConfig.userSession>",WebConfig.userSession.currentCompanyNameOnIcon)
            let shippingTypeId = (this.shippingType().id == 0 ) ? null : this.shippingType().id
            let filter = {
                businessUnitIds: this.selectedBusinessUnit(),
                companyName:WebConfig.userSession.currentCompanyNameOnIcon,
                startDate: formatStartDate,
                endDate: formatEndDate,
                shippingTypeId: shippingTypeId,
                formatStartDateForDisplay: formatStartDateForDisplay,
                formatEndDateForDisplay: formatEndDateForDisplay
            }
            
            this.navigate("cw-report-kpi-dashboard-overall",filter );
        }
    }

}

export default {
    viewModel: ScreenBase.createFactory(FormKpiReport),
    template: templateMarkup
};