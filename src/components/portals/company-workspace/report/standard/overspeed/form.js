import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebrequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";


/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class FormOverspeed extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.selectedBusinessUnit = ko.observable(null);
        this.selectedVehicle = ko.observable();

        this.businessUnits = ko.observableArray([]);
        this.vehicles = ko.observableArray([]);
        

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        this.start = ko.observable(Utility.addDays(new Date(),-1));
        this.end = ko.observable(new Date());

        // //set endDate if selected startDate over endDate 31days then set endDate = startDate+31days
        // this.start.subscribe(value => {
        //     var diffDays = parseInt((new Date(this.end()) - new Date(value)) / (1000 * 60 * 60 * 24));
        //     if(diffDays > addDay ){      
        //         this.end(Utility.addDays(value,addDay));
        //     }
        // });

        //set startDate if selected endDate less startDate then set startDate = endDate
        this.end.subscribe(value => {
            var diffDays = parseInt((new Date(value) - new Date(this.start())) / (1000 * 60 * 60 * 24));
            if(diffDays <= 0 ){      
                this.start(new Date(value));
            }
        });
        
        this.periodDay = ko.pureComputed(()=>{
            return Utility.addDays(this.start(),addDay);
        });
        //Set hidden detail date
        this.showDate = ko.observable(false);
            
        //get Asset by Department
        this.selectedBusinessUnit.subscribe(value => {
            //TO do load call service
            this.webRequestVehicle.listVehicleSummary({
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: value,
                sortingColumns: DefaultSorting.Vehicle
            }).done((response) => {
                if(Object.keys(response.items).length > 0){
                    response.items.unshift({
                        license: this.i18n('Report_All')(),
                        id: 0
                    });
                }
                this.vehicles(response["items"]);
            });
        });
    }

    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedBusinessUnit.extend({ required: true });
        this.selectedVehicle.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
        // this.end = this.end.extend({
        //     date: true,
        //     min:{
        //         params:this.start,
        //         message:"Please select date greate than or equal to \"From Date\""
        //     }
        // });
        // With custom message
        this.validationModel = ko.validatedObservable({

            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedVehicle: this.selectedVehicle,
            end:this.end,
            start:this.start,

        });

    }


    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        this.webRequestBusinessUnit.listBusinessUnitSummary({ 
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit })
            .done((response) => {
                this.businessUnits(response["items"]);
                dfd.resolve();
        });

        return dfd;

    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " "+"00:00:00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " "+"00:00:00";


            if(this.validationModel.isValid()) {

                this.reports = {
                    report:'Gisc.Csd.Service.Report.Library.Summary.DailySummaryReport, Gisc.Csd.Service.Report.Library', 
                    //report:'Gisc.Csd.Service.Report.Library.DailySummary_Report.DailySummary_List, Gisc.Csd.Service.Report.Library', 
                                    parameters: {
                                                    UserID:WebConfig.userSession.id,
                                                    CompanyID:WebConfig.userSession.currentCompanyId,
                                                    // CompanyName:this.selectedCompany().CompanyName,
                                                    DepartmentID:this.selectedBusinessUnit(),
                                                    AssetID:this.selectedVehicle().id,
                                                    SDate:formatSDate,
                                                    EDate:formatEDate,
                                                    PrintBy:WebConfig.userSession.fullname
                                                }
                                };
                this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n('Report_Overspeed')()});

            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(FormOverspeed),
    template: templateMarkup
};