import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class AdvanceDriverPerformanceReport extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());


        this.businessUnits = ko.observableArray();
        this.entityTypes = ko.observableArray();
        this.selectEntitys = ko.observableArray();

        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedEntityType = ko.observable();
        this.selectedEntity = ko.observable();

        var beginDate = Utility.addDays(new Date(),-1);
        this.startDate = ko.observable(beginDate);
        this.formatSet = "dd/MM/yyyy";

        this.selectedEntityBinding = ko.pureComputed(() => {
            this.isBusy(true);
            var result = this.i18n('Common_PleaseSelect')();
            var businessUnitIds = this.selectedBusinessUnit();
           
            //set object businessUnitIds for include Sub BU
            if(this.isIncludeSubBU() && businessUnitIds != null){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }

            var dfd = $.Deferred();
            if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null){
                this.getDriver(businessUnitIds).done((res) => {
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject();
                });
            }else if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null){
                this.getVehicle(businessUnitIds).done((res) => {
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject();
                });
            }else{
                dfd.resolve();
            }
            
            $.when(dfd).done((res) => {
                this.isBusy(false);
                return result;
            });
        });


    }

    setupExtend() {


        this.selectedBusinessUnit.extend({ required: true });
        this.selectedEntityType.extend({ required: true });
        this.selectedEntity.extend({ required: true });
        this.startDate.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedEntityType: this.selectedEntityType,
            selectedEntity: this.selectedEntity,
            startDate: this.startDate
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }


    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    getVehicle(buId) {
        var dfd = $.Deferred();
        this.webRequestVehicle.listVehicleSummary({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: buId,
            sortingColumns: DefaultSorting.Vehicle
        }).done((response) => {
            //set displayName
            response.items.forEach(function(arr) {
                arr.displayName = arr.license;
            });
            let listSelectEntity = response.items;

            if(listSelectEntity.length > 0){
                listSelectEntity.unshift({
                    displayName: this.i18n('Report_All')(),
                    id: 0
                });
            }

            this.selectEntitys(listSelectEntity);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    getDriver(buId) {
        var dfd = $.Deferred();
        this.webRequestDriver.listDriverSummary({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: buId,
            sortingColumns: DefaultSorting.Driver
        }).done((response) => {
            //set displayName
            response.items.forEach(function(arr) {
                arr.displayName = arr.firstName + " " + arr.lastName;
            });
            let listSelectEntity = response.items;

            if(listSelectEntity.length > 0){
                listSelectEntity.unshift({
                    displayName: this.i18n('Report_All')(),
                    id: 0
                });
            }

            this.selectEntitys(listSelectEntity);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }



    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();

        
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [
                //Enums.ModelData.SearchEntityType.Vehicle,
                Enums.ModelData.SearchEntityType.Driver
            ],
            sortingColumns: DefaultSorting.EnumResource
        };

        var d1 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);

        $.when(d1, d2).done((r1, r2) => {

            this.businessUnits(r1.items);
            this.entityTypes(r2.items);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {

            if(this.validationModel.isValid()) 
            {
                var IncludeSubBU = (this.isIncludeSubBU())?1:0;
                var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.startDate()));
                var reportNameSpace = "Gisc.Csd.Service.Report.Library.Advance.DriverPerformanceAdvanceReport, Gisc.Csd.Service.Report.Library";
                var reportTitle = "AdvanceReport_DriverPerformance";

                var reports = {
                    report:reportNameSpace, 
                    parameters: {
                        UserID:WebConfig.userSession.id,
                        CompanyID:WebConfig.userSession.currentCompanyId,
                        BusinessUnitID:this.selectedBusinessUnit(),
                        IncludeSubBusinessUnit:IncludeSubBU,
                        EntityTypeID:this.selectedEntityType().value,
                        SelectedEntityID: this.selectedEntity().id,
                        StartDate: formatSDate,
                        PrintBy:WebConfig.userSession.fullname,
                        Language:WebConfig.userSession.currentUserLanguage
                    }
                };

                this.navigate("cw-report-reportviewer", {reportSource: reports, reportName:this.i18n(reportTitle)()});
            }
            else
            {
                this.validationModel.errors.showAllMessages();
            }
            

        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AdvanceDriverPerformanceReport),
    template: templateMarkup
};