﻿import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCategory";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";


class FleetTypeLogReport extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.businessUnits = ko.observableArray([]);
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectLicense = ko.observableArray([]);
        this.selectedLicense = ko.observable();
        this.categoryVehicleOptions = ko.observableArray([]);
        this.categoryVehicle = ko.observable();
        this.shippingTypeOptions = ko.observableArray([]);
        this.shippingType = ko.observable();


        this.licenseBinding = ko.pureComputed(() => {
            var businessUnitIds = this.selectedBusinessUnit();
            this.selectLicense([]);
            if(this.isIncludeSubBU() && businessUnitIds != null){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }
            if(businessUnitIds != null){
                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle,
                    vehicleCategoryId: this.categoryVehicle().id
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.license;
                    });
                    let listVehicle= response.items;
                    //add displayName "Report_All" and id 0
                        //if(listVehicle.length > 0 ){
                            listVehicle.unshift({
                                displayName: this.i18n('Report_All')(),
                                id: 0
                            });
                        //}
                        this.selectLicense(listVehicle);
                        var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectLicense, "displayName", this.i18n('Report_All')());
                        this.selectedLicense(defaultSelectEntity);
                });
            }
            else{
                this.selectLicense([]);
            }
        });

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());

        this.periodDay = ko.pureComputed(()=>{
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }

            // set date when clear start date and end date
            if(this.start() == null){
                // set end date when clear start date
                if(this.end() == null){
                    isDate = new Date();
                }else{
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            }else{
                this.minDateEnd(this.start())
            }

            return isDate;
        });
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for VehicleCategory module in Web API access.
     * @readonly
     */
    get webRequestCategory() {
        return WebRequestCategory.getInstance();
    }

    /**
    * Get WebRequest specific for Vehicle module in Web API access.
    * @readonly
    */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    setupExtend() {
        this.selectedBusinessUnit.extend({required:true});
        this.start.extend({required:true});
        this.end.extend({required:true});
        this.timeStart.extend({required:true});
        this.timeEnd.extend({required:true});

        this.validationModel = ko.validatedObservable({
            selectedBusinessUnit : this.selectedBusinessUnit,
            start : this.start,
            end : this.end,
            timeStart : this.timeStart,
            timeEnd : this.timeEnd
        });
    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };
        var filterCategoryVehicle = {
            categoryId:Enums.ModelData.CategoryType.Vehicle,
        };
        var filterShippingType = {
            categoryId:Enums.ModelData.CategoryType.ShippingType,
        };

        var d1 = this.webRequestCategory.listCategory(filterCategoryVehicle);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestCategory.listCategory(filterShippingType);

        $.when(d1,d2,d3).done((r1,r2,r3) =>{

            r1.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });
            r3.items.unshift({
                name: this.i18n('Report_All')(),
                id: 0
            });
            this.businessUnits(r2["items"]);
            this.categoryVehicleOptions(r1.items);
            this.shippingTypeOptions(r3.items);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {
            if(!this.validationModel.isValid()){
                this.validationModel.errors.showAllMessages();
                return false;
            }

            var IncludeSubBU = (this.isIncludeSubBU())?1:0;
            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";
            console.log("this.start>>",this.start())
            console.log("this.startFormated>>",formatSDate)
            this.reports = {
                report:'Gisc.Csd.Service.Report.Library.FleetTypeLogs.FleetTypeLogReport, Gisc.Csd.Service.Report.Library', 
                parameters: {
                    UserId:WebConfig.userSession.id,
                    CompanyId:WebConfig.userSession.currentCompanyId,
                    BusinessUnitId:this.selectedBusinessUnit(),
                    IncludeSubBusinessUnit:IncludeSubBU,
                    FromDate:formatSDate,
                    ToDate:formatEDate,
                    VehicleId : this.selectedLicense() ? this.selectedLicense().id : 0 ,
                    VehicleCateId: this.categoryVehicle().id,
                    FleetTypeId: (this.shippingType()) ? this.shippingType().id : 0,
                    PrintBy:WebConfig.userSession.fullname,
                    Language:WebConfig.userSession.currentUserLanguage
                }
            };

            this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n("Report_FleetTypeLog")()});
        }
    }
}


export default{
    viewModel: ScreenBase.createFactory(FleetTypeLogReport),
    template: templateMarkup
};