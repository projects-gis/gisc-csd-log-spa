import ko from "knockout";
import ScreenBase from "../../../../screenbase";
import templateMarkup from "text!./form.html";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import "jquery-ui";
import "jqueryui-timepicker-addon";
//import WebRequestReports from "../../../../../../../app/frameworks/data/apitrackingreport/WebRequestReports";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";

/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class FormValidationDemo extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());
        this.company = ko.observableArray();
        this.selectedCompany = ko.observable();

        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());

        //set endDate if selected startDate over endDate 31days then set endDate = startDate+31days
        this.start.subscribe(value => {
            var diffDays = parseInt((new Date(this.end()) - new Date(value)) / (1000 * 60 * 60 * 24));
            if(diffDays > addDay ){      
                this.end(Utility.addDays(value,addDay));
            }
        });

        //set startDate if selected endDate less startDate then set startDate = endDate
        this.end.subscribe(value => {
            var diffDays = parseInt((new Date(value) - new Date(this.start())) / (1000 * 60 * 60 * 24));
            if(diffDays <= 0 ){      
                this.start(new Date(value));
            }
        });
        
        this.periodDay = ko.pureComputed(()=>{
            return Utility.addDays(this.start(),addDay);
        });
        //Set hidden detail date
        this.showDate = ko.observable(false);



    }

    setupExtend() {

        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.start.extend({ required: true });
        this.end.extend({ required: true });
        // With custom message
    
        this.validationModel = ko.validatedObservable({

            end:this.end,
            start:this.start,

        });

        // Disable Save button in initial state, it will be disabled/enabled based on validationModel.isValid() state
        this.saveButton = this.createAction("actPreview", this.i18n('Report_Preview')());
        this.saveButton.enable(this.validationModel.isValid());

        // Explicitly chaining VM validity with save button's enable state.
        this.validationModel.isValid.subscribe(function(value) {
            this.saveButton.enable(value);
        }.bind(this));

        // Extras: for remote validation, see https://github.com/Knockout-Contrib/Knockout-Validation/wiki/Async-Rules
    }


   

    onLoad(isFirstLoad) {
        

    }

    buildActionBar(actions){
        actions.push(this.saveButton);
        // actions.push(this.createAction("actClear", "Clear"));
    }

    onActionClick(sender) {
        if(sender.id === "actPreview") {
            if(this.validationModel.isValid()) {

                this.reports = {
                                    report:'Gisc.Csd.Service.Report.Library.SummaryADAS_Report.SummaryADAS_Graph, Gisc.Csd.Service.Report.Library', 
                                    parameters: {
                                                    CompanyID:WebConfig.userSession.currentCompanyId,
                                                    // CompanyName:this.selectedCompany().CompanyName,
                                                    SDate:$.datepicker.formatDate("yy-mm-dd", new Date(this.start())),
                                                    EDate:$.datepicker.formatDate("yy-mm-dd", new Date(this.end()))
                                                }
                                };
                
                this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n('Report_SummaryADAS')()});

            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                console.log(this.validationModel.errors());
            }
        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(FormValidationDemo),
    template: templateMarkup
};