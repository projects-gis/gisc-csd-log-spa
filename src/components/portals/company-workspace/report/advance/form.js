﻿import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestFeature from "../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestCustomPOICategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOICategory";
import WebRequestCustomAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomAreaCategory";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomArea";
import WebRequestReportExports from "../../../../../app/frameworks/data/apitrackingreport/webRequestReportExports";



/**
 * Demo for the following features:
 * 1. Validation on ViewModel
 * - Required Field
 * - Numeric Field
 * - Email
 * - Custom (RegEx)
 * - Remote Validation
 * 2. Track change on ViewModel
 * 
 * @class FormValidationDemo
 * @extends {ScreenBase}
 */
class AdvanceReport extends ScreenBase {
    constructor(params) {
        super(params);
      
        this.bladeSize = BladeSize.XLarge;
        this.bladeTitle(this.i18n('Report_AdvanceReport')());
        //this.bladeIsMaximized = true;
        this.selectedReportType = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.isIncludeSubBU = ko.observable(false);
        this.selectedEntityType = ko.observable();
        this.selectedEntity = ko.observable();
        this.reportTypes = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.entityTypes = ko.observableArray([]);
        this.selectEntity = ko.observableArray([]);
        this.parkingTypes = ko.observableArray([]);
        this.eventTypes = ko.observableArray([]);
        this.customPoiValue = ko.observable();
        this.isLastParking = ko.observable(false);
        this.isGroup = ko.observable(true);

        this.empId = ko.observable();
        this.distance = ko.observable();
        this.speed = ko.observable();
        this.fuel = ko.observable();
        this.CusPOI = ko.observable();
        this.customArea = ko.observableArray([]);
        this.movementType = ko.observableArray([]);
        this.directionType = ko.observableArray([]);
        this.engineType = ko.observableArray([]);
        this.gpsStatus = ko.observableArray([]);
        this.gsmStatus = ko.observableArray([]);
        this.customAreaCate = ko.observable();
        this.customPoiCate = ko.observableArray([]);
        this.selectedMovement = ko.observable();
        this.selectedDirection = ko.observable();
        this.selectedEngine = ko.observable();
        this.selectedGps = ko.observable();
        this.selectedGsm = ko.observable();
        this.selectedPoiCate = ko.observable();
        this.selectedCusAreaCate = ko.observable(); 
        this.selectedCustomArea = ko.observable();
        //for keep data this.entityTypes onload
        this.tempEntityTypes = ko.observableArray([]);
        this.parkingEngineOnOver = ko.observable();



        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 0;
        var setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.start = ko.observable(new Date());
        this.end = ko.observable(new Date());
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(new Date());
        this.minDateEnd = ko.observable(new Date());
        this.enableTo = false;

        //for disable column
        this.renderItemColumn = (data, type, row) => {
            if(data) {
                return 'Yes';
            }
            return 'No';
        };

        this.periodDay = ko.pureComputed(()=>{
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(new Date() / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = new Date();
            }

            // set date when clear start date and end date
            if(this.start() == null){
                // set end date when clear start date
                if(this.end() == null){
                    isDate = new Date();
                }else{
                    isDate = this.end();
                }
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
            }else{
                this.minDateEnd(this.start())
            }

            return isDate;
        });

        this.selectedEntityBinding = ko.pureComputed(() => {
            var businessUnitIds = this.selectedBusinessUnit();
            var result = this.i18n('Common_PleaseSelect')();
            this.selectEntity([]);
            //set object businessUnitIds for include Sub BU
            if(this.isIncludeSubBU() && businessUnitIds != null){
                businessUnitIds = Utility.includeSubBU(this.businessUnits(),businessUnitIds);
            }
            //set vihecleType Only
            this.entityTypes((null)?this.tempEntityTypes()[0]:this.tempEntityTypes());

            if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Vehicle && businessUnitIds != null){
                this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Vehicle
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.license;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                        listSelectEntity.unshift({
                            displayName: this.i18n('Report_All')(),
                            id: 0
                        });
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            }else if(this.selectedEntityType() && this.selectedEntityType().value === Enums.ModelData.SearchEntityType.Driver && businessUnitIds != null){
                this.webRequestDriver.listDriverSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: businessUnitIds,
                    sortingColumns: DefaultSorting.Driver
                }).done((response) => {
                    //set displayName
                    response.items.forEach(function(arr) {
                        arr.displayName = arr.firstName + " " + arr.lastName;
                    });
                    let listSelectEntity = response.items;
                    //add displayName "Report_All" and id 0
                    if(Object.keys(listSelectEntity).length > 0 && listSelectEntity[0].id !== 0){
                        listSelectEntity.unshift({
                            displayName: this.i18n('Report_All')(),
                            id: 0
                        });
                    }
                    this.selectEntity(listSelectEntity);
                    var defaultSelectEntity = ScreenHelper.findOptionByProperty(this.selectEntity, "displayName", this.i18n('Report_All')());
                    this.selectedEntity(defaultSelectEntity);
                });
            }else{
                this.selectEntity([]);
            }

            return result;
        });
        this.isVisibleOverSpeed = ko.pureComputed(()=>{
            return !(this.selectedReportType() && this.selectedReportType().value == Enums.ModelData.GeneralReportType.OverSpeed) ; 
        });
        this.isVisibleTracking = ko.pureComputed(()=>{
            return !(this.selectedReportType() && this.selectedReportType().value == Enums.ModelData.GeneralReportType.Tracking) ; 
        });
    }

    setupExtend() {
        var self = this ;
        // Use ko-validation to set up validation rules
        // See url for more information about rules
        // https://github.com/Knockout-Contrib/Knockout-Validation

        this.selectedReportType.extend({ required: true });
        this.selectedBusinessUnit.extend({ required: true });
        //Entity Type
        this.selectedEntityType.extend({ required: true });
        this.selectedEntity.extend({ required: true });
        this.start.extend({ required: true });
        this.end.extend({ required: true });
       
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });
        self.speed.extend({
            required:{
                onlyIf: function(){
                    return !(self.isVisibleOverSpeed());
                }
            }
        });

        // With custom message
        this.validationModel = ko.validatedObservable({
            selectedReportType: this.selectedReportType,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedEntityType: this.selectedEntityType,
            selectedEntity: this.selectedEntity,
            end:this.end,
            start:this.start,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd,
            speed:self.speed
        });

    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }

    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }
    get webRequestCustomPOICategory() {
        return WebRequestCustomPOICategory.getInstance();
    }
    get webRequestCustomAreaCategory() {
        return WebRequestCustomAreaCategory.getInstance();
    }
    
    get webReqestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }

    get webRequestReportExports() {
        return WebRequestReportExports.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var enumReportTemplateTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.GeneralReportType],
            values: [Enums.ModelData.GeneralReportType.Tracking,
                Enums.ModelData.GeneralReportType.OverSpeed
            ],
            sortingColumns: DefaultSorting.EnumResource
        };
        
        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId, 
            sortingColumns: DefaultSorting.BusinessUnit
        };

        var enumEntityTypeFilter = {
            types: [Enums.ModelData.EnumResourceType.SearchEntityType],
            values: [Enums.ModelData.SearchEntityType.Vehicle, 
                     Enums.ModelData.SearchEntityType.Driver],
            sortingColumns: DefaultSorting.EnumResource
        };

        var enumMovement = {
            types:[Enums.ModelData.EnumResourceType.MovementType]
        };
        var enumDirection = {
            types:[Enums.ModelData.EnumResourceType.DirectionType]
        };
        var enumEngine = {
            types:[Enums.ModelData.EnumResourceType.EngineType]
        };
        var enumGps = {
            types:[Enums.ModelData.EnumResourceType.GpsStatus]
        };
        var enumGsm = {
            types:[Enums.ModelData.EnumResourceType.GsmStatus]
        };
        var filter ={
        
        };
        var d1 = this.webRequestEnumResource.listEnumResource(enumReportTemplateTypeFilter);
        var d2 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        var d3 = this.webRequestEnumResource.listEnumResource(enumEntityTypeFilter);
        var d4 = this.webRequestEnumResource.listEnumResource(enumMovement);
        var d5 = this.webRequestEnumResource.listEnumResource(enumDirection);
        var d6 = this.webRequestEnumResource.listEnumResource(enumEngine);
        var d7 = this.webRequestEnumResource.listEnumResource(enumGps);
        var d8 = this.webRequestEnumResource.listEnumResource(enumGsm);
        var cp = this.webRequestCustomPOICategory.listCustomPOICategorySummary({ids:[]});
        var ca = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary({});
        var ca2 = this.webReqestCustomArea.listCustomAreaSummary({});
        $.when(d1, d2, d3,d4,d5,d6,d7,d8,cp,ca,ca2).done((r1, r2, r3,r4,r5,r6,r7,r8,r9,r10,r11) => {

            this.reportTypes(r1["items"]);
            this.businessUnits(r2["items"]);
            this.tempEntityTypes(r3["items"]);
            this.movementType(r4["items"]);
            this.directionType(r5["items"]);
            this.engineType(r6["items"]);
            this.gpsStatus(r7["items"]);
            this.gsmStatus(r8["items"]);
            this.customPoiCate(r9["items"]);
            this.customAreaCate(r10["items"]);
            this.customArea(r11["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;

    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id=="actPreview"){

            var reportTitle = '';
            var reportNamespace = '';
            var IncludeSubBU = (this.isIncludeSubBU())?1:0;

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + this.timeStart() + ":00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + this.timeEnd() + ":00";
            
            if(this.validationModel.isValid()) {
                //var reportExport = new Object();
                var filter = {
                    //ReportTypeID:this.selectedReportType().value,
                    UserID:WebConfig.userSession.id,
                    CompanyID:WebConfig.userSession.currentCompanyId,
                    BusinessUnitID:this.selectedBusinessUnit(),
                    IncludeSubBusinessUnit:IncludeSubBU,
                    EntityTypeID:this.selectedEntityType().value,
                    SelectedEntityID:this.selectedEntity().id,
                    EmployeeID: this.empId()?this.empId():null,
                    Distance: this.distance()?this.distance():null,
                    Movement: this.selectedMovement()?this.selectedMovement().value:null,
                    Speed: this.speed()?this.speed():null,
                    Fuel: this.fuel()?this.fuel():null,
                    Direction:this.selectedDirection()?this.selectedDirection().value:null,
                    Engine: this.selectedEngine()?this.selectedEngine().value:null,
                    GPSStatus: this.selectedGps()?this.selectedGps().value:null,
                    GSMStatus:  this.selectedGsm()?this.selectedGsm().value:null,
                    CustomArea: this.selectedCustomArea()?this.selectedCustomArea().id:null,
                    CustomAreaCate: this.selectedCusAreaCate()?this.selectedCusAreaCate().id:null,
                    CustomPOI: this.CusPOI()?this.CusPOI():null,
                    CustomPOICate: this.selectedPoiCate()?this.selectedPoiCate().id:null,
                    SDate:formatSDate,
                    EDate:formatEDate,
                    parkingEngineOnOver:this.parkingEngineOnOver()?this.parkingEngineOnOver():null,
                    PrintBy:WebConfig.userSession.fullname,
                    Language:WebConfig.userSession.currentUserLanguage
                };

                this.isBusy(true);
                switch (this.selectedReportType() && this.selectedReportType().value) {

                    case Enums.ModelData.GeneralReportType.Tracking:
                        filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx; 
                        this.webRequestReportExports.previewCSVTrackLocationAdvanceReport(filter).done((response) => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        //reportNamespace = 'Gisc.Csd.Service.Report.Library.Advance.TrackLocationAdvanceReport, Gisc.Csd.Service.Report.Library';
                        //reportTitle = 'Report_Trackloc';
                        //reportExport.CSV = this.webRequestReportExports.exportCSVTrackLocationAdvanceReport();
                        break;
                    
                    case Enums.ModelData.GeneralReportType.OverSpeed:
                        filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx; 
                        this.webRequestReportExports.previewCSVOverspeedAdvanceReport(filter).done((response) => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        //reportNamespace = 'Gisc.Csd.Service.Report.Library.Advance.OverspeedAdvanceReport, Gisc.Csd.Service.Report.Library';
                        //reportTitle = 'Report_OverSpeed';
                        //reportExport.CSV = this.webRequestReportExports.exportCSVOverspeedAdvanceReport();
                        break;
                    default:
                        reportNameSpace = null;
                        reportTitle = '';
                        break;
                }

                
                //this.navigate("cw-report-reportviewer", {reportSource: this.reports, reportName:this.i18n(reportTitle)(), reportExport:reportExport });

            } else {
                // This is unreachable code, since we disable save button when VM is not valid
                this.validationModel.errors.showAllMessages();
               
            }



        }
    }
}

export default {
viewModel: ScreenBase.createFactory(AdvanceReport),
    template: templateMarkup
};