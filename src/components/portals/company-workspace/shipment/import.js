﻿import ko from "knockout";
import templateMarkup from "text!./import.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../app/frameworks/core/utility";
import WebRequestCompany from "../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import { Enums } from "../../../../app/frameworks/constant/apiConstant";


class ShipmentImport extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Common_Import")());
        this.bladeSize = BladeSize.XLarge_A1;

        this.bladeIsMaximized = true;

        this.fileName = ko.observable("");
        this.selectedFile = ko.observable(null);
        this.mimeType = [
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx")
        ].join();
        this.media = ko.observable(null);
        this.importValidationError = ko.observable("");
        this.previewData = ko.observableArray([]);
        this.previewDataVisible = ko.observable(false);

        this.canSaveImportData = ko.pureComputed(() => {
            return _.isEmpty(this.importValidationError()) && !_.isNil(this.media());
        });

        this.importListData = ko.observable(null);
        this.urlUpload = WebConfig.appSettings.uploadUrlTrackingCore;
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            //this.distanceUnit(response.distanceUnitSymbol);
            //this.currency(response.currencySymbol);


        });
        $.when(dfdCompanySettings).done(() => {

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdDownloadTemplate", this.i18n("Common_DownloadTemplate")(), "svg-cmd-download-template"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            if (!this.canSaveImportData()) {
                return;
            }
            //   console.log("this media",this.media());
            this.isBusy(true);
            this.webRequestShipment.importShipment({ companyId: WebConfig.userSession.currentCompanyId, media: this.media() }, true).done((response) => {
                this.isBusy(false);


                //if (response.messageIdToErrors.M077.length > 0 || 
                //    response.messageIdToErrors.M140.length > 0 ||  
                //    response.messageIdToErrors.M141.length > 0 ||  
                //    response.messageIdToErrors.M142.length > 0 ||  
                //    response.messageIdToErrors.M143.length > 0 ){
                //    console.log("AAA");
                //    this.handleError(response);
                //}
                //else {
                //    console.log("BBB");
                this.publishMessage("cw-shipment-import-updated");
                this.close(true);
                //}


            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });

            //console.log("is Ok");
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDownloadTemplate") {
            this.showMessageBox(null, this.i18n("M117")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_OK:
                            this.isBusy(true);
                            this.webRequestShipment.downloadImportTemplate(Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
        }
    }

    onRemoveFile() {

        this.previewData.removeAll();
        this.previewDataVisible(false);
        this.importValidationError("");
        this.media(null);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }


    setupExtend() {
        this.media.extend({ trackChange: true });

        // validation
        this.selectedFile.extend({
            fileRequired: true,
            fileExtension: ['xlsx', 'xls'],
            fileSize: 4
        });

        this.validationModel = ko.validatedObservable({
            selectedFile: this.selectedFile
        });
    }

    onBeforeUpload() {
        this.onRemoveFile();

    }

    onUploadSuccess(data) {
        if (data && data.length > 0) {
            this.isBusy(true);
            var media = data[0];
            this.webRequestShipment.importShipment({ companyId: WebConfig.userSession.currentCompanyId, media: media }, false).done((response) => {
                var messageIdToErrors = response.messageIdToErrors;
                var previewData = this.formatPreviewData(response.data);//response.data;
                var listAlert = "";

                //console.log("response", this.formatPreviewData(previewData));



                //  console.log("list ALert",listAlert);
                //  console.log("Data",previewData);

                /*======== Modified to DataGrid =========*/

                if (previewData && previewData.length === 0 && Object.keys(messageIdToErrors).length === 0) {
                    //this.previewData.replaceAll(previewData);

                    this.importListData({ item: previewData }); 

                    this.previewDataVisible(true);
                    this.clearStatusBar();
                    this.refreshImportList();
                }
                else if (previewData && previewData.length > 0) {
                    //case validation pass
                    this.media(media);
                    // set unique tempId for dataTable
                    var index = 1;
                    ko.utils.arrayForEach(previewData, (item) => {
                        item.tempId = index;
                        index++;
                    });

                    //this.previewData.replaceAll(previewData);

                    this.importListData({ item: previewData }); 
                    this.previewDataVisible(true);
                    this.clearStatusBar();
                    this.refreshImportList();
                }
                else if (Object.keys(messageIdToErrors).length > 0) {
                    //case validation error
                    if (messageIdToErrors.M076) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M076", messageIdToErrors.M076));
                    }
                    else if (messageIdToErrors.M077) {
                        this.importValidationError(ScreenHelper.formatImportValidationErrorMessage("M077", messageIdToErrors.M077));
                    }
                }
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }

    }


    formatPreviewData(previewDataList) {
        for (var i = 0; i < previewDataList.length; i++){

            var previewData = previewDataList[i];

            var arrPlanStart = previewData.formatPlanStartDate.split(' ');

            previewData.formatPlanStartDate = arrPlanStart[0];
            previewData.formatPlanStartTime = arrPlanStart[1];
        }

        return previewDataList;
    }

    onUploadFail(e) {
        //  console.log("onUploadFail");
        this.isBusy(false);
        this.handleError(e);
    }

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        if (this.importListData() != null) {
            var filter = Object.assign({}, this.importListData(), gridOption);
            dfd.resolve({
                items: filter.item,
                totalRecords: filter.item.length
            });


        } else {
            dfd.resolve({
                items: [],
                totalRecords: 0
            });
        }
        return dfd;
    }

    refreshImportList() {
        this.dispatchEvent("dgImportShipment", "refresh");
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentImport),
    template: templateMarkup
};