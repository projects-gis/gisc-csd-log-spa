﻿import ko from "knockout";
import templateMarkup from "text!./shipment-cancel.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import { Constants, EntityAssociation } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";

class ShipmentCancelScreen extends ScreenBase {

    get OtherReason() {
        return 6;
    }

    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Transportation_Mngm_Cancel_Shipment_Title")());

        this.bladeMargin = BladeMargin.Normal;
        this.bladeSize = BladeSize.Small;

        this.shipmentId = this.ensureNonObservable(params.shipmentId);

        this.lstCancelReasons = ko.observableArray([]);
        this.cancelReason = ko.observable();
        this.cancelReasonDetail = ko.observable();


        this.subscribeMessage("cw-shipment-disabled-cancelfinish", () => {
            this.close(true);
        });
    }

    setupExtend() {
        let self = this;

        this.cancelReason.extend({ trackChange: true });
        this.cancelReasonDetail.extend({ trackChange: true });

        this.cancelReason.extend({ required: true });
        this.cancelReasonDetail.extend({
            required: {
                onlyIf: function () {
                    return self.cancelReason() && self.cancelReason().id == self.OtherReason;
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            cancelReason: this.cancelReason,
            cancelReasonDetail: this.cancelReasonDetail
        });
    }

    get webRequestShipment()
    { return WebRequestShipment.getInstance(); }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad)
            return;

        let filter = {companyId: WebConfig.userSession.currentCompanyId};

        this.webRequestShipment.cancelCauseList(filter).done((res) => {
            this.lstCancelReasons.replaceAll(res);
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            let cause = this.cancelReason().cause;

            if (!_.isEmpty(this.cancelReasonDetail())) {
                cause += ` - ${this.cancelReasonDetail()}`;
            }

            this.webRequestShipment.postShipmentManageCancel({
                jobId: this.shipmentId,
                cause: cause
            }).done(() => {
                this.isBusy(false);
                this.publishMessage("cw-shipment-manage-cancel-complete", {});
                this.close(true);
            }).fail((e) => {
                this.handleError(e);
            }).always(() => {
                this.isBusy(false);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentCancelScreen),
    template: templateMarkup
};