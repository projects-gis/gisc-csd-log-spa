﻿import ko from "knockout";
import templateMarkup from "text!./po-list.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import AssetInfo from "../asset-info";
import AssetUtility from "../asset-utility";

import * as EventAggregator from "../../../../app/frameworks/constant/eventAggregator";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentPOSearchScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentPOSearchScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n('Transportation_Mngm_POSearch_Title')());

        this.bladeSize = BladeSize.XLarge;

        this.filter = ko.observable({});

        this.timeOutHandler = this.ensureNonObservable(null);
        this.isAutoRefresh = ko.observable(false);
        this.txtLastSync = ko.observable('');
        this.txtRefresh = ko.pureComputed(() => {
            //return (this.i18n("Transportation_Mngm_Shipment_AutoRefresh")() + " (" + this.i18n("Vehicle_MA_LastSync")() + " " + "" + ")");
            return (this.i18n("Vehicle_MA_LastSync")() + " " + this.txtLastSync());
            //return "";
        });

        this.isAutoRefresh.subscribe(isChecked => {
            this.onAutoRefresh();
        });

        this.subscribeMessage("cw-posearch-search", info => {
            this.filter(info);
            this.refreshPOSearchList();
        });

        this.clickOpenShipmentSummary = data => {           
            this.navigate("cw-shipment-summary", { shipmentId: data.shipmentId });
        };

        this._eventAggregator.subscribe(
          EventAggregator.MAP_COMMAND_COMPLETE,
          info => {
              switch (info.command) {
                  case "open-shipment-legend":
                      this.minimizeAll();
                      //console.log("open-shipment-legend completed!!");
                      break;
                  default:
                      break;
              }
          }
        );

        this.clickViewOnMap = (data) => {

            var viewOnMapQuery = this.webRequestShipment.getDataViewOnMap(data.shipmentId);

            $.when(viewOnMapQuery).done(res => {
                
                this.prepareViewOnMapData(res);
               
            });

            
        };
    }

    setDefaultSearchFilter() {
        this.filter({
            PONumber: null,
            ShipmentCode: null
        });
    }

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();

        var filter = Object.assign({}, this.filter(), gridOption);
        this.isBusy(true);
        this.webRequestShipment
          .listPOSummary(filter)
          .done(response => {              
              let model = this.formatData(response.items);
              dfd.resolve({
                  items: model,
                  currentPage : response.currentPage,
                  totalRecords: response.totalRecords
              });

              this.txtLastSync(response["formatTimestamp"]);
          })
        
        .fail(e => {
            this.handleError(e);
        })
        .always(() => {
            this.isBusy(false);
            this.onAutoRefresh(true);
        });

        return dfd;
    }

    formatData(data) {
        let model = [];
        for(let i=0;i<data.length;i++) {
            let dataItem = data[i];
            dataItem.waypointData = {
                text: dataItem.waypointName,
                icon: dataItem.icon.name
            };

            dataItem.confirmDMDate =  {
                plan: dataItem.formatPlanEndDate,
                actual: dataItem.formatActualEndDate,
                actualColor: dataItem.actualColor
            }

            model.push(dataItem);
        }

        return model;
    }

    refreshPOSearchList() {
        this.dispatchEvent("dgPOSearchLst", "refresh");
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.refreshPOSearchList();
                this.onAutoRefresh();
            }, WebConfig.appSettings.shipmentAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }

    prepareViewOnMapData(result) {
        var viewonmapData = {
            panel: {
                jobName: result.jobName,
                driverName: result.driverName,
                jobCode: result.jobCode,
                jobStatusDisplayName: result.jobStatusDisplayName,
                vehicleLicense: result.vehicleLicense
            },
            actual: {
                pin: [],
                route: []
            },
            plan: {
                pin: [],
                route: []
            },
            vehicle: []
        };

        _.forEach(result.shipmentPlanInfos, (planInfo) => {
            var pinItem = {
                longitude: planInfo.longitude,
                latitude: planInfo.latitude,
                attributes: {
                    jobWaypointStatus: planInfo.jobWaypointStatus,
                    jobWaypointName: planInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": planInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": planInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": planInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": planInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": planInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.plan.pin.push(pinItem);
            viewonmapData.plan.route = viewonmapData.plan.route.concat(planInfo.shipmentLocations);
        });

        _.forEach(result.shipmentActualInfos, (actualInfo) => {
            var pinItem = {
                longitude: actualInfo.longitude,
                latitude: actualInfo.latitude,
                attributes: {
                    jobWaypointStatus: actualInfo.jobWaypointStatus,
                    jobWaypointName: actualInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": actualInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": actualInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": actualInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": actualInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": actualInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.actual.pin.push(pinItem);
            viewonmapData.actual.route = viewonmapData.actual.route.concat(actualInfo.shipmentLocations);
        });


        var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
        _.forEach(result.locationInfo, (locationInfo) => {


            var vehicleItem = {
                longitude: locationInfo.longitude,
                latitude: locationInfo.latitude,
                attributes: {
                    "TITLE": locationInfo.vehicleLicense,
                    "SHIPMENTINFO": AssetInfo.getShipmentInfo(locationInfo, locationInfo.trackLocationFeatureValues)
                }
            };

            //switch (movementType) {
            //    case Enums.ModelData.MovementType.Move:
            //        movement = "move";
            //        break;
            //    case Enums.ModelData.MovementType.Stop:
            //        movement = "stop";
            //        break;
            //    case Enums.ModelData.MovementType.Park:
            //        movement = "park";
            //        break;
            //}

            //var movementImageUrl = Utility.resolveUrl("/", "~/images/icon-walk-" + movement + "-direction-");

            var movementImageUrl = "http://l2stg.nostralogistics.com:40015/api/media/VehicleIconImage/1/Images/2/" + directionConst[locationInfo.directionType - 1] + ".png"
            vehicleItem.movementImageUrl = movementImageUrl;
            viewonmapData.vehicle.push(vehicleItem);
        });

        MapManager.getInstance().openShipmentLegend({ data: viewonmapData });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
            
        this.setDefaultSearchFilter();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { 
        this.isAutoRefresh(false);
        MapManager.getInstance().closeViewOnMap();
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdRefresh", this.i18n("Common_Refresh")(), "svg-cmd-refresh"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdFilter":
                this.navigate("cw-shipment-polist-search", {});
                break;
            case "cmdRefresh" :
                this.refreshPOSearchList();
                break;
            default:
                break;
        }

    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

}

export default {
viewModel: ScreenBase.createFactory(ShipmentPOSearchScreen),
    template: templateMarkup
};