﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {
    Constants,
    Enums
} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";

import WebRequestGlobalSearch from "../../../../app/frameworks/data/apitrackingcore/webRequestGlobalSearch";
import WebRequestVehicle from "../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestDriver from "../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestCustomPOI from "../../../../app/frameworks/data/apitrackingcore/webRequestCustomPOI";
import WebRequestMap from "../../../../app/frameworks/data/apitrackingcore/webRequestMap";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestShipmentCategory from "../../../../app/frameworks/data/apitrackingcore/webRequestShipmentCategory"
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import WebRequestShipmentZone from "../../../../app/frameworks/data/apitrackingcore/webRequestShipmentZone";

class ShipmentManage extends ScreenBase {
    constructor(params) {
        super(params);



        this.bladeSize = BladeSize.XLarge_A1;
        this.bladeIsMaximized = true;

        this.isShipmentTemplateChange = ko.observable(false);
        this.testData = ko.observableArray([]);

        this.isVisible = ko.observable(false);
        this.tab2Visible = ko.observable(true);
        this.selectedIndex = ko.observable(0);
        this.apiDataSource = ko.observableArray([]);
        this.mode = ko.observable('create');

        this.txtDisable = ko.observable();
        this.isLatLonDup = ko.observable(false);

        this.dfValuejobAutoFinish = ko.observable();
        this.dflastShipmentRole = ko.observable();
        this.dfDuration = ko.observable();
        this.isCodriverDup = ko.observable(true);

        this.ddEnumJobAutoFinish = ko.observableArray([]);
        this.txtEnumJobAutoFinish = ko.observable();
        this.ddLastShipmentRole = ko.observableArray([]);
        this.txtLastShipmentRole = ko.observable();
        this.txtDuration = ko.observable();

        this.txtStatus = ko.observable("-");
        this.txtShipmentName = ko.observable("");
        this.txtShipmentCode = ko.observable("");
        this.ddShipmentTemplate = ko.observable();
        this.ddShipmentTemplateOptions = ko.observableArray([]);
        this.txtStartDate = ko.observable(new Date());
        this.txtStartTime = ko.observable(this.checkTime());
        this.txtWaitingTime = ko.observable("");
        this.txtBusinessUnit = ko.observable("");
        this.txtVehicle = ko.observable();
        this.txtVehiclelist = ko.observableArray([]);
        this.txtDriver = ko.observable();
        this.cancelDriver = ko.observable();
        this.txtDriverlist = ko.observableArray([]);
        this.txtTraillelist = ko.observableArray([]);
        this.txtTrail = ko.observable();
        this.txtDelivertManlist = ko.observableArray([]);
        this.txtDelivertMan = ko.observable("");
        this.txtDescription = ko.observable("");
        this.txtReference = ko.observable("");
        this.dataUpdate = null;
        this.shipmentData = ko.observable({});
        this.totalWayPoint = ko.observable(0);
        this.isManageSave = ko.observable(false);
        this.isShipmentGridEdit = ko.observable(false);
        this.shipmentGridChange = ko.observable(false);
        this.bladeId = ko.observable(null);
        this.isLoadDataComplete = false;

        this.isBindingData = this.ensureNonObservable(false);

        this.shipmentId = this.ensureNonObservable(null);
        this.shipmentStatus = ko.observable(null);

        this.defaultValue = ko.observable({});

        this.jobDetail = ko.observable({
            startJob: Enums.ModelData.AccessType.Depart,
            closeJob: Enums.ModelData.AccessType.Arrival,
            isTerminal: false
        });

        this.shipmentGridControl = ko.observable();

        this.zone = ko.observableArray([]);
        this.selectedZone = ko.observable();
        this.travelMode = ko.observableArray([]);
        this.selectedTravelMode = ko.observable();
        this.shipmentCategory = ko.observableArray([]);
        this.selectedShipmentCategory = ko.observable();
        this.defaultDriverId = ko.observable();

        this.customRouteImage = this.resolveUrl('~/images/icon_customroute_remove.svg');


        this.getAutoCompleteList = (request, response) => {
            var filter = {
                companyId: WebConfig.userSession.currentCompanyId,
                name: request.term,
                businessUnitId:this.businessUnit()
            }
            
            this.autoCompleteList(filter, response)
        }
        this.getDataRoute = (filter, response) => {

            this.routeData(filter, response)
        }

        this.startDateTime = ko.pureComputed(({
            read: () => {
                return ({
                    date: this.txtStartDate(),
                    time: this.txtStartTime()
                });
            }
        }));

        if (params.filter == undefined) {
            this.txtDisable(true);
            this.isShipmentGridEdit(true);
            this.isManageSave(true);
            this.bladeTitle(this.i18n("Transportation_Mngm_Create_Shipment")());
            this.mode('create');
        } else {
            this.shipmentId = params.filter.shipmentId;
            this.isBindingData = true;
            this.shipmentStatus(params.filter.shipmentStatusId);
            this.bladeTitle(this.i18n("Transportation_Mngm_Update_Shipment")());
            this.mode('update');

            if (this.shipmentStatus() == Enums.JobStatus.Unassigned ||
                this.shipmentStatus() == Enums.JobStatus.Waiting) {
                this.txtDisable(true);
                this.dataUpdate = params;
                this.isShipmentGridEdit(true);
            } else {
                this.isShipmentGridEdit(false);
                this.txtDisable(false);
            }

            // if (this.shipmentStatus() == Enums.JobStatus.Unassigned ||
            //     this.shipmentStatus() == Enums.JobStatus.Waiting ||
            //     this.shipmentStatus() == Enums.JobStatus.Start ||
            //     this.shipmentStatus() == Enums.JobStatus.OnTheWay ||
            //     this.shipmentStatus() == Enums.JobStatus.InsideWaypoint ||
            //     this.shipmentStatus() == Enums.JobStatus.GoingToTerminal ||
            //     this.shipmentStatus() == Enums.JobStatus.InsideTerminal ||
            //     this.shipmentStatus() == Enums.JobStatus.Pending) {
            //     this.isManageSave(true);
            // }

            if (this.shipmentStatus() == Enums.JobStatus.Unassigned ||
                this.shipmentStatus() == Enums.JobStatus.Waiting) {
                this.isManageSave(true);
            }
        }

        this.businessUnitList = ko.observableArray([]);
        this.businessUnit = ko.observable();
        this.vechicleBUList = ko.observableArray([]);
        this.vechicleBU = ko.observable();
        this.driverBUList = ko.observableArray([]);
        this.driverBU = ko.observable();
        //tbl
        this.shipmentJobWaypointInfoLst = ko.observableArray([]);

        this.assignAdditionalrow = ko.observableArray([]);
        this.details = ko.observable();
        this.porterData = ko.observableArray([]);
        this.driverList = ko.observableArray([]);
        this.coDriver = ko.observable();
        this.coDriverList = ko.observableArray([]);
        this.cancelCodriver = ko.observableArray([]);
        this.resultCodriver = ko.observableArray([]);

        this.coDriverId = ko.observable();
        this.selectedItem = ko.observable();

        this.coDriveris = false;

        this.driverList2 = ko.observableArray([]);
        this.isVehicleSelect = ko.pureComputed(() => {

            var isRequired = false;
            if (this.txtDisable()) {
                isRequired = !_.isNil(this.txtVehicle())
            }

            return isRequired;
        });

        this.list = ko.observableArray([]);
        this.filterShipment = ko.observable();
        this.deliveryTypeList = ko.observableArray([]);
        this.indexCodriver = ko.observable();
        this.isFindBestSequenceLoading = ko.observable(false);

        this.subscribeMessage("cw-shipment-manage-finish", () => {
            this.close(true);
        });

        this.subscribeMessage("cw-shipment-manage-cancel-complete", () => {
            this.close(true);
            this.publishMessage("cw-shipment-manage-refresh", {});
        });

        this.isFindBestSequenceLoading.subscribe((res)=>{
            if(res){
                this.isBusy(true);
            }else{
                this.isBusy(false);
            }
        });

    }

    viewOnMap() {
        let route = [];
        let wp = [];
        let pics = [];
        let urlPic = "~/images/"

        ko.utils.arrayForEach(this.shipmentJobWaypointInfoLst(), (itm, ind) => {
            if (!_.isNull(itm.jobWaypointRouteInfos)) {
                itm.jobWaypointRouteInfos.map((itm) => {
                    route.push([itm.longitude, itm.latitude]);
                });
            }
            //{ route = route.concat(); }

            pics.push(this.resolveUrl(`${urlPic}pin_poi_${ind + 1}.png`));

            wp.push({
                name: itm.jobWaypointName,
                lat: itm.lat,
                lon: itm.lon
            });
        });

        if (route.length > 0)
            this.drawRoute(wp, pics, [route]);
    }

    drawRoute(stops, pics, path, color = "#d91515", width = 5, opacity = 1, attributes = null) {
        MapManager.getInstance().closeViewOnMap();
        MapManager.getInstance().clear(this.id);
        MapManager.getInstance().drawOneRoute(this.id, stops, pics, path, color, width, opacity, attributes);
        MapManager.getInstance().setFollowTracking(false);
        this.minimizeAll();
    }

    checkTime() {
        var dateTime = new Date();
        dateTime = this.addMinutes(dateTime, 30);
        var hour = dateTime.getHours();
        var min = dateTime.getMinutes();
        dateTime = this.check(hour) + ":" + this.check(min);
        return dateTime

    }
    check(time) {
        time = parseInt(time);
        if (time < 10) {
            time = "0" + time
        };
        return time;
    }
    onToggleTab() {
        this.tab2Visible(!this.tab2Visible());
    }
    
    autoCompleteList(filter, response) {
        
        this.webRequestCustomPOI.autoCompleteForShipment(filter).done((data) => {
            response($.map(data, (item) => {
                return {
                    label: item.name,
                    value: item.name,
                    item: item
                };
            }));
        });
        
    }

    routeData(filter, response) {
        this.isBusy(true);


        

        this.webRequestMap.solveRoute(filter).done((res) => {
            this.isBusy(false);
            return response(res)
        }).fail((e) => {
            this.isBusy(false);
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

       // this.bladeId($(this)[0]._blade.id);
        //this.isEnabledHeaderBtn();

        let dfd = $.Deferred();
        let dfdAllDone = $.Deferred();

        let self = this;
        let dfdBusinessUnit = null;
        let dfdDefaultValue = null;
        let dfdShipment = null;
        let dfdDeliveryType = null;
        let dfdEnumJobAutoFinish = null;
        let dfdLastShipmentRole = null;
        let dfdShipmentTemplate = null;

        //dfdAllDone.done(() => {
        //    if (this.mode() === 'update' && this.shipmentJobWaypointInfoLst()['length'] != 0) {
        //        this.checkIsChange();
        //    }
        //});

        let buFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        };

        dfdShipmentTemplate = this.webRequestShipment.listShipmentTemplateSummary();

        dfdDefaultValue = this.webRequestShipment.getDefaultValue();

        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(buFilter);

        dfdEnumJobAutoFinish = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobAutoFinish]
        });
        dfdLastShipmentRole = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.LastShipmentRole]
        });

        if (this.shipmentId != null) {
            dfdShipment = this.webRequestShipment.getShipmentUpdate(this.shipmentId);
        } else {
            dfdShipment = $.Deferred();
            dfdShipment.resolve();
        }   

        let shipmentStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.DeliveryType]
        }

        dfdDeliveryType = this.webRequestEnumResource.listEnumResource(shipmentStatusFilter);

        let dfdZone = this.webRequestShipmentZone.listShipmentZone({enable: true});

        let travelModeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.TravelMode,
            sortingColumns: DefaultSorting.EnumResource
        }
        
        let dfdTravelMode = this.webRequestEnumResource.listEnumResource(travelModeFilter);
        

        $.when(dfdBusinessUnit, dfdShipment, dfdDefaultValue, dfdDeliveryType, dfdEnumJobAutoFinish, dfdLastShipmentRole, dfdShipmentTemplate, dfdZone, dfdTravelMode).done((
            responseBusiness,
            responseShipment,
            responseDefaultValue,
            responseDeliveryType,
            responseEnumJobAutoFinish,
            responseLastShipmentRole,
            responseShipmentTemplate,
            responseZone,
            responseTravelMode) => {
            
            // console.log("responseShipmentTemplate", responseShipmentTemplate)

            this.ddShipmentTemplateOptions(responseShipmentTemplate.items);
            this.ddEnumJobAutoFinish(responseEnumJobAutoFinish.items);
            this.ddLastShipmentRole(responseLastShipmentRole.items);
            this.businessUnitList(responseBusiness.items);
            this.vechicleBUList(responseBusiness.items);
            this.driverBUList(responseBusiness.items);
            this.defaultValue(responseDefaultValue);
            this.deliveryTypeList(responseDeliveryType.items);

            this.dflastShipmentRole(responseDefaultValue.lastShipmentRole);
            this.dfValuejobAutoFinish(responseDefaultValue.jobAutoFinish);
            this.dfDuration(responseDefaultValue.duration);
            this.txtEnumJobAutoFinish(ScreenHelper.findOptionByProperty(this.ddEnumJobAutoFinish, "value", responseDefaultValue.jobAutoFinish));
            this.txtLastShipmentRole(ScreenHelper.findOptionByProperty(this.ddLastShipmentRole, "value", responseDefaultValue.lateShipmentRole));
            this.txtDuration(responseDefaultValue.duration);
            this.zone(responseZone.items);
            this.travelMode(responseTravelMode.items);

            if (responseShipment != undefined) { //update
               
                let lst = []
                let timeStart = {
                    hour: this.check(responseShipment.formatPlanStartDate.split(" ")[1].split(":")[0]),
                    min: this.check(responseShipment.formatPlanStartDate.split(" ")[1].split(":")[1])
                }

                this.shipmentData(responseShipment);
                this.businessUnit(responseShipment.businessUnitId.toString());
                if(responseShipment.vehicleBusinessUnitId){
                    this.vechicleBU(responseShipment.vehicleBusinessUnitId.toString())
                }

                if(responseShipment.driverBusinessUnitId){
                    this.driverBU(responseShipment.driverBusinessUnitId.toString())
                }

                //this.txtBusinessUnit(ScreenHelper.findOptionByProperty(this.businessUnitList, "id", responseShipment.businessUnitId.toString()));

                
                this.txtStatus(responseShipment.jobStatusDisplayName);
                this.txtShipmentName(responseShipment.jobName);
                this.txtShipmentCode(responseShipment.jobCode);
                if (responseShipment.jobTemplateId) {
                    this.ddShipmentTemplate(ScreenHelper.findOptionByProperty(this.ddShipmentTemplateOptions, "id", responseShipment.jobTemplateId));

                }
                
                this.txtWaitingTime(responseShipment.waitingTime);
                //this.porterTable(responseShipment.coDriverIds);
                this.txtReference(responseShipment.refNo);
                this.txtDescription(responseShipment.description);
                this.txtDelivertMan(responseShipment.deliveryManId);
                this.txtStartDate(new Date(responseShipment.planStartDate));
                this.txtStartTime(timeStart.hour + ":" + timeStart.min);
                //this.coDriverList(responseShipment.coDriverInfos);
                
                this.selectedZone(ScreenHelper.findOptionByProperty(this.zone, "id", responseShipment.jobRegionId));
                this.selectedTravelMode(ScreenHelper.findOptionByProperty(this.travelMode, "value", responseShipment.travelMode));
                
                let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({businessUnitIds:[responseShipment.businessUnitId]});
                dfdShipmentCategory.done((resShipmentCategory)=>{
                    this.shipmentCategory(resShipmentCategory.items);
                    this.selectedShipmentCategory(ScreenHelper.findOptionByProperty(this.shipmentCategory, "id", responseShipment.jobCategoryId));
                });
                

                this.txtEnumJobAutoFinish(ScreenHelper.findOptionByProperty(this.ddEnumJobAutoFinish, "value", responseShipment.jobAutoFinish));
                this.txtLastShipmentRole(ScreenHelper.findOptionByProperty(this.ddLastShipmentRole, "value", responseShipment.lateShipmentRole));
                this.txtDuration(responseShipment.jobAutoFinishDuration);

                this.jobDetail({
                    startJob: responseShipment.startJob,
                    closeJob: responseShipment.closeJob,
                    isTerminal: responseShipment.isTerminal
                });

                this.resultCodriver.replaceAll(this.formatCodriver(responseShipment.coDriverInfos));
                this.cancelCodriver.replaceAll(this.formatCodriver(responseShipment.coDriverInfos));

                this.totalWayPoint(responseShipment.shipmentJobWpInfos.length);
                for (let i in responseShipment.shipmentJobWpInfos) {
                    responseShipment.shipmentJobWpInfos[i].arrivalId = "arrival" + i;
                    responseShipment.shipmentJobWpInfos[i].departId = "depart" + i;
                    lst.push(responseShipment.shipmentJobWpInfos[i]);
                }
                
                this.shipmentJobWaypointInfoLst.replaceAll(lst);
                var dfdVehicle =responseShipment.vehicleBusinessUnitId? this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [responseShipment.vehicleBusinessUnitId],
                    vehicleTypes: [
                        Enums.ModelData.VehicleType.Car,
                        Enums.ModelData.VehicleType.Truck,
                        Enums.ModelData.VehicleType.Trailer,
                        Enums.ModelData.VehicleType.PassengerCar
                    ]
                }):null;
                var dfdTrail = responseShipment.vehicleBusinessUnitId?this.webRequestVehicle.listVehicleSummary({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [responseShipment.vehicleBusinessUnitId],
                    vehicleTypes: [
                        Enums.ModelData.VehicleType.Trail
                    ]
                }):null;
                var dfddriverlist =responseShipment.driverBusinessUnitId? this.webRequestDriver.driverSummarylist({
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: responseShipment.driverBusinessUnitId
                }):null;

                $.when(dfdTrail, dfdVehicle, dfddriverlist).done((responseTrail, responseVehicle, responsedriverlist) => {

                    if(responsedriverlist){
                        this.txtDriverlist(this.formatCodriver(responsedriverlist.items));
    
                        this.txtDriver(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", responseShipment.driverId));
                        this.cancelDriver(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", responseShipment.driverId));
                    }
                    if(responseVehicle){
                        this.txtVehiclelist(responseVehicle.items);
                        this.txtVehicle(ScreenHelper.findOptionByProperty(this.txtVehiclelist, "id", responseShipment.vehicleId));
                    }

                    if(responseTrail){
                        this.txtTraillelist(responseTrail.items);
                        this.txtTrail(ScreenHelper.findOptionByProperty(this.txtTraillelist, "id", responseShipment.trailId));
                    }

                    this.isBindingData = false;
                    this.coDriveris = true;
                    this.isLoadDataComplete = true;
                    this.codrivertable();
                    dfd.resolve();
                });
                dfdAllDone.resolve();
            } else {
                this.coDriveris = false;
                this.isLoadDataComplete = true;
                this.codrivertable();
                dfd.resolve();
            }
        }).fail((e) => {
            dfd.reject(e);
        }).always(() => {

        });

        this.ddShipmentTemplate.ignorePokeSubscribe((params) => {
            
            // this.mode('update')  
            let dfdShipmentTemplateGet;
           
            if(this.isLoadDataComplete){
                if (params) {
                    
                    this.isBindingData = true;
                    this.isShipmentTemplateChange(true);

                    dfdShipmentTemplateGet = this.webRequestShipment.getShipmentTemplate(params.id);
    
                    $.when(dfdShipmentTemplateGet, dfdDeliveryType, dfdDefaultValue).done((res, responseDeliveryType, responseDefaultValue) => {
                        
                        let waypointTemplatesObj = res.jobWaypointTemplatesInfos;
    
                        let arrival = new Date(this.txtStartDate());
    
                        let arrTime = this.txtStartTime().split(':');
                        let depart;
                        arrival.setHours(arrTime[0])
                        arrival.setMinutes(arrTime[1]);
    
    
                        let shipmentTemplateObj = waypointTemplatesObj.map((items, index)=>{
                            
                            if (index != 0){
                                arrival = depart;
                                arrival.setMinutes(arrival.getMinutes() + parseInt(waypointTemplatesObj[index - 1].formatPlanDriveTime));
                            } 
                                                    
                            // // depart = this.txtStartTime() + items.formatPlanStopTime;
                            depart = new Date(arrival);
                            depart.setMinutes(depart.getMinutes() + parseInt(items.formatPlanStopTime,10));
    
                            items = this.setPropertyForUpdate(items, arrival, depart, index);
                            return items;
                        })

                        this.jobDetail({
                            startJob: res.startJob,
                            closeJob: res.closeJob,
                            isTerminal: res.isTerminal
                        });
                        this.defaultValue(responseDefaultValue);
                        this.deliveryTypeList(responseDeliveryType.items);
    
    
                        _.forEach(shipmentTemplateObj,(item)=>{
                        
                            item.stop.subscribe((stopParam)=>{
                                
                                this.shipmentGridControl().displayDateTime();
                                this.shipmentGridControl().shipmentGridChange(true);
                            });
                            item.travel.subscribe((travelParam)=>{
                                this.shipmentGridControl().displayDateTime();
                                this.shipmentGridControl().shipmentGridChange(true);
                            })
                        });

                        this.businessUnit({ "value": res.businessUnitId });
                        this.vechicleBU({ "value": res.vehicleBusinessUnitId });
                        this.driverBU({ "value": res.driverBusinessUnitId });
                        this.txtShipmentName(res.jobName);
                        this.txtShipmentCode(res.code);
                        this.txtReference(res.refNo);
                        this.txtWaitingTime(res.waitingTime);

                        this.selectedZone(ScreenHelper.findOptionByProperty(this.zone, "id", res.jobRegionId));
                        this.selectedTravelMode(ScreenHelper.findOptionByProperty(this.travelMode, "value", res.travelMode));
                        this.selectedShipmentCategory(ScreenHelper.findOptionByProperty(this.shipmentCategory, "id", res.jobCategoryId));
                       
    
                        var dfdVehicle = res.vehicleBusinessUnitId?this.webRequestVehicle.listVehicleSummary({
                            companyId: WebConfig.userSession.currentCompanyId,
                            businessUnitIds: [res.vehicleBusinessUnitId],
                            vehicleTypes: [
                                Enums.ModelData.VehicleType.Car,
                                Enums.ModelData.VehicleType.Truck,
                                Enums.ModelData.VehicleType.Trailer,
                                Enums.ModelData.VehicleType.PassengerCar
                            ]
                        }):null;
    
                        var dfdTrail = res.vehicleBusinessUnitId?this.webRequestVehicle.listVehicleSummary({
                            companyId: WebConfig.userSession.currentCompanyId,
                            businessUnitIds: [res.vehicleBusinessUnitId],
                            vehicleTypes: [
                                Enums.ModelData.VehicleType.Trail
                            ]
                        }):null;
                        var dfddriverlist = res.driverBusinessUnitId?this.webRequestDriver.driverSummarylist({
                            companyId: WebConfig.userSession.currentCompanyId,
                            businessUnitIds: res.driverBusinessUnitId
                        }):null;

                        let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                            businessUnitIds: [res.businessUnitId],
                            enable: true
                        });
                        
                        $.when(dfdVehicle, dfdTrail, dfddriverlist, dfdShipmentCategory)
                        .done((resVehicle, resTrail, resDriver, resShipmentCategory) => {
                            if(resVehicle){
                                this.txtVehiclelist(resVehicle.items)
                                this.txtVehicle(ScreenHelper.findOptionByProperty(this.txtVehiclelist, "id", res.vehicleId));
                            }
    
                            if(resTrail){
                                this.txtTraillelist(resTrail.items)
                                this.txtTrail(ScreenHelper.findOptionByProperty(this.txtTraillelist, "id", res.trailId));
                            }
                            if(resDriver){
                                this.txtDriverlist(this.formatCodriver(resDriver.items));
                                this.txtDriver(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", res.driverId));
                            }

                            this.shipmentCategory(resShipmentCategory.items);
                            this.selectedShipmentCategory(ScreenHelper.findOptionByProperty(this.shipmentCategory, "id", res.jobCategoryId));

                            this.txtStartTime(res.planStartTime);
                            this.isBindingData = false;
                            this.shipmentJobWaypointInfoLst.replaceAll(shipmentTemplateObj);
                        })
                        
    
                     })
                }else {
                   
                    this.isBindingData = false;
                    this.txtShipmentName('');
                    this.txtShipmentCode('');
                    this.businessUnit('');
                    this.txtVehiclelist([]);
                    this.txtTraillelist([]);
                    this.txtDriverlist([]);
                    this.txtDelivertManlist([]);
                    this.txtWaitingTime('');
                    this.shipmentCategory([]);
                    this.txtStartTime(this.checkTime());

                    if(this.shipmentGridControl() != undefined){
                        this.shipmentGridControl().cancel();
                    }
                }
            }
            
            

        });

        this.vechicleBU.subscribe((idbusinessUnit)=>{
            if(idbusinessUnit && idbusinessUnit.value)return

            if (!this.isBindingData) {

                // this.txtDriver(null);
                this.txtVehicle(null);
                //this.txtTrail(null);
                // this.txtDriverlist.replaceAll([]);
                this.txtVehiclelist.replaceAll([]);
                this.txtTraillelist.replaceAll([]);
                // this.codriverarray.replaceAll([]);
                // this.shipmentCategory.replaceAll([]);
                
                
                if (idbusinessUnit) {
                    this.isBusy(true);
                    var dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                        companyId: WebConfig.userSession.currentCompanyId,
                        businessUnitIds: [idbusinessUnit],
                        vehicleTypes: [
                            Enums.ModelData.VehicleType.Car,
                            Enums.ModelData.VehicleType.Truck,
                            Enums.ModelData.VehicleType.Trailer,
                            Enums.ModelData.VehicleType.PassengerCar
                        ]
                    });
                    var dfdTrail = this.webRequestVehicle.listVehicleSummary({
                        companyId: WebConfig.userSession.currentCompanyId,
                        businessUnitIds: [idbusinessUnit],
                        vehicleTypes: [
                            Enums.ModelData.VehicleType.Trail
                        ]
                    });
                    // var dfddriverlist = this.webRequestDriver.driverSummarylist({
                    //     companyId: WebConfig.userSession.currentCompanyId,
                    //     businessUnitIds: idbusinessUnit
                    // });

                    // let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                    //     businessUnitIds: [idbusinessUnit],
                    //     enable: true
                    // });

                    $.when(dfdTrail, dfdVehicle).done((responseTrail, responseVehicle) => {
              

                        // this.txtDriverlist.replaceAll(this.formatCodriver(responsedriverlist.items));
                        this.txtVehiclelist.replaceAll(responseVehicle.items);
                        this.txtTraillelist.replaceAll(responseTrail.items);
                        // this.driverList.replaceAll(this.formatCodriver(responsedriverlist.items));
                        // this.shipmentCategory.replaceAll(resShipmentCategory.items);

                        // if (this.ddShipmentTemplate()) {
                        //     this.clearShipment();
                        // }

                        this.isBusy(false);

                        dfdTrail.resolve();
                        dfdVehicle.resolve();
                        // dfddriverlist.resolve();
                    })
                }else{
                        this.validationVehicle.errors.showAllMessages(false);       
                }
            }

        })

        this.driverBU.subscribe((idbusinessUnit) => {
            if(idbusinessUnit && idbusinessUnit.value)return

            if (!this.isBindingData) {

                this.txtDriver(null);
                // this.txtVehicle(null);
                // //this.txtTrail(null);
                this.txtDriverlist.replaceAll([]);
                // this.txtVehiclelist.replaceAll([]);
                // this.txtTraillelist.replaceAll([]);
                this.codriverarray.replaceAll([]);
                // this.shipmentCategory.replaceAll([]);
                
                
                if (idbusinessUnit) {
                    this.isBusy(true);
                    // var dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: WebConfig.userSession.currentCompanyId,
                    //     businessUnitIds: [idbusinessUnit],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Car,
                    //         Enums.ModelData.VehicleType.Truck,
                    //         Enums.ModelData.VehicleType.Trailer,
                    //         Enums.ModelData.VehicleType.PassengerCar
                    //     ]
                    // });
                    // var dfdTrail = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: WebConfig.userSession.currentCompanyId,
                    //     businessUnitIds: [idbusinessUnit],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Trail
                    //     ]
                    // });
                    var dfddriverlist = this.webRequestDriver.driverSummarylist({
                        companyId: WebConfig.userSession.currentCompanyId,
                        businessUnitIds: idbusinessUnit
                    });

                    // let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                    //     businessUnitIds: [idbusinessUnit],
                    //     enable: true
                    // });

                    $.when(dfddriverlist).done((responsedriverlist) => {
              
                        
                        this.txtDriverlist.replaceAll(this.formatCodriver(responsedriverlist.items));
                        // this.txtVehiclelist.replaceAll(responseVehicle.items);
                        // this.txtTraillelist.replaceAll(responseTrail.items);
                        if(this.defaultDriverId()){
                           
                            this.txtDriver(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", this.defaultDriverId()));
                        }

                        this.driverList.replaceAll(this.formatCodriver(responsedriverlist.items));
                        // this.shipmentCategory.replaceAll(resShipmentCategory.items);

                        // if (this.ddShipmentTemplate()) {
                        //     this.clearShipment();
                        // }

                        this.isBusy(false);

                        // dfdTrail.resolve();
                        // dfdVehicle.resolve();
                        dfddriverlist.resolve();
                    })
                }else{
                    this.validationDriver.errors.showAllMessages(false);       
                }
            }

        })
        this.businessUnit.subscribe((idbusinessUnit) => {
            
            if (!this.isBindingData) {

                // this.txtDriver(null);
                // this.txtVehicle(null);
                // //this.txtTrail(null);
                // this.txtDriverlist.replaceAll([]);
                // this.txtVehiclelist.replaceAll([]);
                // this.txtTraillelist.replaceAll([]);
                // this.codriverarray.replaceAll([]);
                this.shipmentCategory.replaceAll([]);
                
                
                if (idbusinessUnit) {
                    this.isBusy(true);
                    // var dfdVehicle = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: WebConfig.userSession.currentCompanyId,
                    //     businessUnitIds: [idbusinessUnit],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Car,
                    //         Enums.ModelData.VehicleType.Truck,
                    //         Enums.ModelData.VehicleType.Trailer,
                    //         Enums.ModelData.VehicleType.PassengerCar
                    //     ]
                    // });
                    // var dfdTrail = this.webRequestVehicle.listVehicleSummary({
                    //     companyId: WebConfig.userSession.currentCompanyId,
                    //     businessUnitIds: [idbusinessUnit],
                    //     vehicleTypes: [
                    //         Enums.ModelData.VehicleType.Trail
                    //     ]
                    // });
                    // var dfddriverlist = this.webRequestDriver.driverSummarylist({
                    //     companyId: WebConfig.userSession.currentCompanyId,
                    //     businessUnitIds: idbusinessUnit
                    // });

                    let dfdShipmentCategory = this.webRequestShipmentCategory.listShipmentCategory({
                        businessUnitIds: [idbusinessUnit],
                        enable: true
                    });

                    $.when(dfdShipmentCategory).done((resShipmentCategory) => {
              

                        // this.txtDriverlist.replaceAll(this.formatCodriver(responsedriverlist.items));
                        // this.txtVehiclelist.replaceAll(responseVehicle.items);
                        // this.txtTraillelist.replaceAll(responseTrail.items);
                        // this.driverList.replaceAll(this.formatCodriver(responsedriverlist.items));
                        this.shipmentCategory.replaceAll(resShipmentCategory.items);
                        
                        this.ddShipmentTemplate.poke(null);

                        // if (this.ddShipmentTemplate()) {
                        //     this.clearShipment();
                        // }

                        this.isBusy(false);

                        // dfdTrail.resolve();
                        // dfdVehicle.resolve();
                        dfdShipmentCategory.resolve();
                    })
                }
            }


        });

        this.txtVehicle.subscribe((idVehicle) => {
            if (!this.isBindingData) {
                if (idVehicle != null ||
                    idVehicle != undefined) {
                    this.txtTrail(null);
                    var dfdVehicleId = this.webRequestVehicle.getVehicle(idVehicle.id);
                    $.when(dfdVehicleId).done((responVehicleId) => {
                        
                        if (responVehicleId.defaultDriverId != null) {
                            // this.txtDriver(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", responVehicleId.defaultDriverId));
                            //this.txtDriver(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", "260"));
                            
                            this.defaultDriverId(_.cloneDeep(responVehicleId.defaultDriverId))
                            this.driverBU({ "value": responVehicleId.businessUnitId.toString() })
                        }
                        
                        if (responVehicleId.trailId != null) {
                            this.txtTrail(ScreenHelper.findOptionByProperty(this.txtTraillelist, "id", responVehicleId.trailId));
                        }
                         dfdVehicleId.resolve();
                    });
                }
            }
        });

        let val = this.txtDriver.subscribe(() => {
            if (!this.isBindingData) {
                this.codriverarray.replaceAll([]);
            }
        })

        this.txtEnumJobAutoFinish.subscribe((closeShipment) => {
            if (closeShipment != null) {
                switch (closeShipment.value) {
                    case Enums.ModelData.JobAutoFinish.AfterPlanEnd:
                        this.txtDuration(this.dfDuration());
                        this.isVisible(true);
                        break;
                    default:
                        this.isVisible(false);
                }
            } else {
                this.txtEnumJobAutoFinish(this.dfValuejobAutoFinish());
            }
        })
        this.txtLastShipmentRole.subscribe((lastShipment) => {
            if (lastShipment == null) {
                this.txtLastShipmentRole(this.dflastShipmentRole());
            }
        })

        return dfd;
    }
    codrivertable() {
        /// fix is swap Co Driver 
        var self = this;
        let check = this.isShipmentGridEdit();
        self.codriverarray = ko.observableArray([]);
        let isValidate = false;


        if (this.coDriveris != false) {
            let driverId = this.txtDriver() == null ? null : this.txtDriver().id;

            this.coDriverList(this.txtDriverlist().filter((res) => {
                return res.id != driverId ? res : null
            }));


            for (let item in this.resultCodriver()) {
                this.resultCodriver()[item].imgUrl = this.customRouteImage;

                let driverfilter = this.txtDriverlist().filter((res) => {
                    if (res.id == this.resultCodriver()[item].id) {
                        return res;
                    }
                })
                this.resultCodriver()[item].selectedItem = ko.observable(driverfilter[0]);


                this.resultCodriver()[item].selectedItem.subscribe((driver) => {

                    if (driver != undefined) {
                        this.selectedItem(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", driver.id));
                        this.list([])
                        for (let item in this.codriverarray()) {
                            this.list().push(this.codriverarray()[item].selectedItem());
                        }


                        let isCheckDup = _.uniqBy(this.list(), 'id')
                        if (isCheckDup.length != this.codriverarray().length) {
                            //this.isCodriverDup(false);
                            this.resultCodriver()[item].selectedItem().isDup = false;
                        } else {
                            //this.isCodriverDup(true);
                            this.resultCodriver()[item].selectedItem().isDup = true;

                        }
                    }


                });



                this.resultCodriver()[item].selectedItem.extend({
                    required: true,
                    validation: {
                        validator: ((val) => {
                            return val.isDup

                        }),
                        message: this.i18n('M010')()
                    }
                });




            }
            self.codriverarray(this.resultCodriver())

        }

        self.NewRecordCodriver = function () {
            isValidate = false;
            if (check) {
                for (let i in self.codriverarray()) {
                    let lst = self.codriverarray()[i]
                    if (lst.selectedItem() == "" ||
                        lst.selectedItem() == null) {
                        isValidate = true
                    } else {
                        isValidate = false;
                    }
                }

                if (this.txtDriver() == null) {
                    this.txtDisable(true)
                } else {
                    this.coDriverList(this.txtDriverlist().filter((res) => {
                        return res.id != this.txtDriver().id ? res : null
                    }));

                    if (!isValidate) {
                        self.codriverarray.push(this.createCOData())
                    }
                }
            }
        };

        self.removeCodriver = function () {
            if (check) {
                self.codriverarray.remove(this);
                isValidate = false;
            }
        }

        self.cancelCodriverChang = function () {

            let coDriver = _.filter(this.codriverarray(), (item)=>{
                return _.size(item.selectedItem());
            });
    
            if(_.size(coDriver)){
                this.showMessageBox(null, this.i18n("M246")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch(button) {
                        case BladeDialog.BUTTON_YES:
                            this.clearDataCoDriver(check);
                            break;
                    }
                });
            }else{
                this.clearDataCoDriver(check);
            }

            
        }

    }

    clearDataCoDriver(check){
        if (check) {
            if (this.coDriveris != false) {
                
                for (let item in this.cancelCodriver()) {
                    this.cancelCodriver()[item].imgUrl = this.customRouteImage;

                    let driverfilter = this.txtDriverlist().filter((res) => {
                        if (res.id == this.cancelCodriver()[item].id) {
                            return res;
                        }
                    })
                    this.cancelCodriver()[item].selectedItem = ko.observable(driverfilter[0]);

                    this.cancelCodriver()[item].selectedItem.subscribe((driver) => {

                        if (driver != undefined) {
                            this.selectedItem(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", driver.id));
                            this.list([])
                            for (let item in this.codriverarray()) {
                                this.list().push(this.codriverarray()[item].selectedItem());
                            }


                            let isCheckDup = _.uniqBy(this.list(), 'id')
                            if (isCheckDup.length != this.codriverarray().length) {
                                //this.isCodriverDup(false);
                                this.cancelCodriver()[item].selectedItem().isDup = false;
                            } else {
                                //this.isCodriverDup(true);
                                this.cancelCodriver()[item].selectedItem().isDup = true;

                            }
                        }


                    });


                    this.cancelCodriver()[item].selectedItem.extend({
                        required: true,
                        validation: {
                            validator: ((val) => {
                                return val.isDup

                            }),
                            message: this.i18n('M010')()
                        }
                    });

                }
                this.resultCodriver.replaceAll(this.cancelCodriver())
                //this.txtDriver(this.cancelDriver());
                this.codriverarray(this.resultCodriver())
            } else {
                this.codriverarray.replaceAll([]);
            }
        }
    }

    createCOData() {

        var codriverItem = {
            selectedItem: ko.observable(null),
            imgUrl: this.customRouteImage,
            isDup: true
        };


        codriverItem.selectedItem.subscribe((driver) => {

            if (driver != undefined) {
                this.selectedItem(ScreenHelper.findOptionByProperty(this.txtDriverlist, "id", driver.id));
                this.list([])
                for (let item in this.codriverarray()) {
                    this.list().push(this.codriverarray()[item].selectedItem());
                }


                let isCheckDup = _.uniqBy(this.list(), 'id')
                if (isCheckDup.length != this.codriverarray().length) {
                    codriverItem.selectedItem().isDup = false;
                } else {
                    codriverItem.selectedItem().isDup = true;

                }
            }


        });


        codriverItem.selectedItem.extend({
            required: true,
            validation: {
                validator: ((val) => {
                    return val.isDup

                }),
                message: this.i18n('M010')()
            }
        });

        return codriverItem;
    }

    formatCodriver(response) {
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i];
            dataItem.name = dataItem.firstName + " " + dataItem.lastName;
            data.push(dataItem);
        }
        return data;
    }

    checkIsChange() {
        this.txtShipmentName.subscribe(() => {
            $("#txtShipmentName").blur(() => {
                this.isDisabledHeadereBtn('txtShipmentName');
            });

        });
        this.txtShipmentCode.subscribe(() => {
            $("#txtShipmentCode").blur(() => {
                this.isDisabledHeadereBtn('txtShipmentCode');
            });
        });
        this.txtStartDate.subscribe(() => {
            $("#dtShipmentStartDate").blur(() => {
                this.isDisabledHeadereBtn('txtStartDate');
            });
        });
        this.txtStartTime.subscribe(() => {
            $("#txtStartTime").blur(() => {
                this.isDisabledHeadereBtn('txtStartTime');
            });
        });
        this.txtWaitingTime.subscribe(() => {
            $("#txtWaitingTime").blur(() => {
                this.isDisabledHeadereBtn('txtWaitingTime');
            });
        });
        this.txtBusinessUnit.subscribe((value) => {
            if (value != undefined || value != null) {
                $("#txtBusinessUnit").blur(() => {
                    this.isDisabledHeadereBtn('txtBusinessUnit');
                });
            }
        });
        this.txtVehicle.subscribe((value) => {
            if (value != undefined || value != null) {
                $("#txtVehicle").blur(() => {
                    this.isDisabledHeadereBtn('txtVehicle');
                });
            }
        });
        this.txtDriver.subscribe((value) => {
            if (value != undefined || value != null) {
                $("#txtDriver").blur(() => {
                    this.isDisabledHeadereBtn('txtDriver');
                });
            }
        });
        this.txtTrail.subscribe((value) => {
            if (value != undefined || value != null) {
                $("#txtTrail").blur(() => {
                    this.isDisabledHeadereBtn('txtTrail');
                });
            }
        });
        this.txtDelivertMan.subscribe((value) => {
            if (value != undefined || value != null) {
                $("#txtDelivertMan").blur(() => {
                    this.isDisabledHeadereBtn('txtDelivertMan');
                });
            }
        });
        this.txtDescription.subscribe((val) => {
            $("#txtDescription").blur((e) => {
                this.isDisabledHeadereBtn('txtDescription');
            });
        });
        this.txtReference.subscribe(() => {
            $("#txtReference").blur(() => {
                this.isDisabledHeadereBtn('txtReference');
            });
        });
        this.shipmentData.subscribe((val) => {
            this.isDisabledHeadereBtn('shipmentData');
        });
        this.jobDetail.subscribe((val) => {
            //this.isDisabledHeadereBtn('jobDetail');
        });
        this.shipmentGridChange.subscribe((val) => {
            if (this.shipmentJobWaypointInfoLst()['length'] != 0 && val) {
                this.shipmentGridChange(false);
            }
        });

        setTimeout(() => {
            this.codriverarray.subscribe(() => {
                this.isDisabledHeadereBtn('codriverarray');
            });
        }, 1000 * 3);
    }

    isDisabledHeadereBtn(form) {
        //var bladeId = "#" + this.bladeId();
        //var elem = $(bladeId + " a.app-commandBar-item");

        //for (let index = 0; index < elem.length; index++) {
        //    let title = elem[index].title;
        //    switch (title) {
        //        case this.i18n('Transportation_Mngm_Shipment_Finish')():
        //        case this.i18n('Common_Cancel')():
        //            $(elem[index]).removeClass("app-has-hover");
        //            $(elem[index]).attr("style", "pointer-events: none;color:#dbdbdb;");
        //            $(elem[index].children["0"].children).attr("style", "fill:#dbdbdb;");
        //            break;
        //        default:
        //            break;
        //    }
        //}

        this.publishMessage("cw-shipment-disabled-cancelfinish", {});
    }

    isEnabledHeaderBtn() {
        //var bladeId = "#" + $(this)[0]._blade.id;
        //var classElemFinish = bladeId + " .app-commandBar-itemList>.app-br-default:nth-of-type(1)>.app-commandBar-item";
        //var classElemCancel = bladeId + " .app-commandBar-itemList>.app-br-default:nth-of-type(2)>.app-commandBar-item";

        //$(classElemFinish).addClass("app-has-hover");
        //$(classElemFinish).attr("style", "");
        //$(classElemFinish + " .app-commandBar-item-icon>svg").attr("style", "");
        //$(classElemFinish + " .app-commandBar-item-text").attr("style", "");

        //$(classElemCancel).addClass("app-has-hover");
        //$(classElemCancel).attr("style", "");
        //$(classElemCancel + " .app-commandBar-item-icon>svg").attr("style", "");
        //$(classElemCancel + " .app-commandBar-item-text").attr("style", "");
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
        MapManager.getInstance().clear(this.id);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (this.isManageSave()) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.shipmentId != null) {
            if (this.shipmentStatus() == Enums.JobStatus.Start ||
                this.shipmentStatus() == Enums.JobStatus.OnTheWay ||
                this.shipmentStatus() == Enums.JobStatus.InsideWaypoint ||
                this.shipmentStatus() == Enums.JobStatus.GoingToTerminal ||
                this.shipmentStatus() == Enums.JobStatus.InsideTerminal ||
                this.shipmentStatus() == Enums.JobStatus.Pending ||
                this.shipmentStatus() == Enums.JobStatus.Waiting ||
                this.shipmentStatus() == Enums.JobStatus.PrepareToFinish ||
                this.shipmentStatus() == Enums.JobStatus.PrepareToStart) {
                if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentFinish)) {
                    commands.push(this.createCommand("cmdFinish", this.i18n('Transportation_Mngm_Shipment_Finish')(), "svg-cmd-maintenance-complete"));
                }
            }

            if (this.shipmentStatus() == Enums.JobStatus.Unassigned ||
                this.shipmentStatus() == Enums.JobStatus.Waiting ||
                this.shipmentStatus() == Enums.JobStatus.Start ||
                this.shipmentStatus() == Enums.JobStatus.OnTheWay ||
                this.shipmentStatus() == Enums.JobStatus.InsideWaypoint ||
                this.shipmentStatus() == Enums.JobStatus.GoingToTerminal ||
                this.shipmentStatus() == Enums.JobStatus.InsideTerminal ||
                this.shipmentStatus() == Enums.JobStatus.Pending || 
                this.shipmentStatus() == Enums.JobStatus.PrepareToFinish ||
                this.shipmentStatus() == Enums.JobStatus.PrepareToStart) {

                if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCancel)) {
                    commands.push(this.createCommand("cmdCancel", this.i18n('Common_Cancel')(), "svg-cmd-clear"));
                }
            }

            if (this.shipmentStatus() == Enums.JobStatus.Unassigned ||
                this.shipmentStatus() == Enums.JobStatus.Waiting) {
                if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentDelete)) {
                    commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
                }
            }

        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        // var isValidate = false;
        var isValidate = new Array();

        this.shipmentJobWaypointInfoLst().forEach((items,index) => {
            items.assignOrder = parseInt(index)+1;
            
            
       

            
            let lst = this.shipmentJobWaypointInfoLst()[index];

            let minPark = _.clone(lst.minPark());
            let maxPark = _.clone(lst.maxPark());

            if (lst.waypointVal() == "" ||
                lst.waypointVal() == null ||
                lst.waypoint() == null) {
                // isValidate = true;
                lst.waypointVal("validate Trigger");
                lst.waypointVal("");
                isValidate.push(true);
            }

            if (lst.stop() == null || lst.stop() < 0) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.travel() == null || lst.travel() < 0) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.distance() == null) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.radiusDisplay() == null) {
                // isValidate = true;
                isValidate.push(true);
            }

            if (lst.isLatLonDup) {
                // isValidate = true;
                isValidate.push(true);
            }

            //validation min park nad max park
            if(minPark > maxPark){
                lst.minPark(0);
                lst.minPark(minPark);
                // isValidate = true;
                isValidate.push(true);
            }

            if(maxPark < minPark){
                lst.maxPark(0);
                lst.maxPark(maxPark);
                // isValidate = true;
                isValidate.push(true);
            }

        });


        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            //if(this.vechicleBU() && this.vechicleBU().value){
                if (!this.validationVehicle.isValid()) {
                    this.validationVehicle.errors.showAllMessages();
                    return;
                }
            //}

            //if(this.driverBU() && this.driverBU().value){
                if (!this.validationDriver.isValid()) {
                    this.validationDriver.errors.showAllMessages();
                    return;
                }
            //}

            var coDriverIds = [];
            var model = {
                
                businessUnitId: this.businessUnit(),
                driverId: _.isNil(this.txtDriver()) ? null : this.txtDriver().id,
                jobCode: this.txtShipmentCode(),
                jobName: this.txtShipmentName(),
                vehicleId: _.isNil(this.txtVehicle()) ? null : this.txtVehicle().id,
                trailId: _.isNil(this.txtTrail()) ? null : this.txtTrail().id,
                deliveryManId: this.txtDelivertMan(),
                description: this.txtDescription(),
                startJob: this.jobDetail().startJob,
                closeJob: this.jobDetail().closeJob,
                isTerminal: this.jobDetail().isTerminal,
                waitingTime: this.txtWaitingTime(),
                refNo: this.txtReference(),
                planStartDate: this.setObjDateTime(this.startDateTime().date, this.startDateTime().time),
                shipmentJobWpInfos: this.setShipmentJobWaypoint(this.shipmentJobWaypointInfoLst()),
                jobAutoFinish: this.txtEnumJobAutoFinish() ? this.txtEnumJobAutoFinish().value : null,
                jobAutoFinishDuration: this.isVisible() ? this.txtDuration() : null,
                lateShipmentRole: this.txtLastShipmentRole() ? this.txtLastShipmentRole().value : null,
                jobRegionId: this.selectedZone() ? this.selectedZone().id : null,
                travelMode: this.selectedTravelMode() ? this.selectedTravelMode().value : null,
                jobCategoryId: this.selectedShipmentCategory() ? this.selectedShipmentCategory().id : null,
                jobTemplateId: this.ddShipmentTemplate() ? this.ddShipmentTemplate().id : null
            }


            for (let i in this.codriverarray()) {
                let lst = this.codriverarray()[i].selectedItem()


                if (lst == undefined) {
                    // isValidate = true;
                    isValidate.push(true);
                } else {
                    coDriverIds.push(lst.id)
                }

            }
            let item = _.uniqBy(coDriverIds)

            if (item.length == coDriverIds.length) {
                model.coDriverIds = coDriverIds;
                this.isLatLonDup(false)

            } else {
                // isValidate = true;
                isValidate.push(true);
                this.selectedIndex(1);
                this.isLatLonDup(true)
            }

            let validationModelShipmentGrid = _.filter(isValidate, (val)=>{
                return val;
            });
            
            if (!_.size(validationModelShipmentGrid)) {
                this.isBusy(true);
                if (this.shipmentId != null) { // update
                    model.infoStatus = Enums.InfoStatus.Update;
                    model.id = this.shipmentData().id;
                    model.jobStatus = this.shipmentData().jobStatus;
                    this.webRequestShipment.updateShipment(model).done((res) => {
                        this.afterCallService();
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                } else { // create
                    
                    model.infoStatus = Enums.InfoStatus.Add;
                    this.webRequestShipment.createShipment(model).done((res) => {
                        this.afterCallService();
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                }
            } else {
                this.isBusy(false);
            }
        }
    }
    // ลบ Route ออก BL จะทำนวนให้ทีหลัง
    setShipmentJobWaypoint(data){
        data.forEach((item)=>{
            item.jobWaypointRouteInfos = null
        })

        return data
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdFinish":
                this.navigate("cw-shipment-finish", {
                    shipmentId: this.shipmentId
                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M194")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestShipment.deleteShipment(this.shipmentId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("cw-shipment-manage-delete", {});
                                this.close(true);
                            }).fail((e) => {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
            case "cmdCancel":
                this.navigate("cw-shipment-cancel", {
                    shipmentId: this.shipmentId
                });
                break;

                //this.showMessageBox(null, this.i18n("M193")(),
                //    BladeDialog.DIALOG_YESNO).done((button) => {
                //        switch (button) {
                //            case BladeDialog.BUTTON_YES:
                //                this.isBusy(true);
                //                this.webRequestShipment.postShipmentManageCancel({ "jobId": this.shipmentId, "cause": "ยกเลิก - TestCancel" }).done(() => {
                //                    this.isBusy(false);
                //                    this.publishMessage("cw-shipment-manage-Cancel", {});
                //                    this.close(true);
                //                }).fail((e) => {
                //                    this.handleError(e);
                //                }).always(() => {
                //                    this.isBusy(false);
                //                });
                //                break;
                //        }
                //    });
            default:
                break;
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    afterCallService() {
        this.publishMessage("cw-shipment-manage-refresh");
        this.isBusy(false);
        this.close(true);
    }

    setObjDateTime(dateObj, timeObj) {
        var newDateTime = "";

        if (dateObj && timeObj) {
            let d = new Date(dateObj);
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            newDateTime = year + "-" + month + "-" + day + "T" + timeObj + ":00";
        } else {
            let d = new Date(dateObj);
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();
            let hour = d.getHours().toString();
            let minute = d.getMinutes().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            hour = hour.length > 1 ? hour : "0" + hour;
            minute = minute.length > 1 ? minute : "0" + minute;
            newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":00";
        }

        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        let hours =  d.getHours().toString();
        let minutes = d.getMinutes().toString();
        if(day < 10){
            day = "0"+day;
        }
        if(month < 10){
            month = "0"+month;
        }
        if (hours   < 10) 
        {
            hours   = "0"+hours;
        }
        if (minutes < 10) 
        {
            minutes = "0"+minutes;
        }

        displayDate = day + "/" + month + "/" + year + "<br> " + hours + ":" +minutes ;

        return displayDate
    }

    setPropertyForUpdate(data, arrival, depart, index) {

        let displayArrival = this.setFormatDisplayDateTime(arrival);
        let displayDepart = this.setFormatDisplayDateTime(depart);

        let isEnabled = true;
        let isStartArrival = true;
        let arrivalEnable = true;

        let lastIndex = this.totalWayPoint() - 1;

        if (index == lastIndex) {
            isEnabled = false;
        }

        if (index == 0) {
            arrivalEnable = false;
        }

        if (this.jobDetail().startJob == 2 &&
            index == 0) {
            isStartArrival = false;
        }

        if (this.jobDetail().startJob == 1 &&
            index == 0) {
            isStartArrival = true;
        }

        data.indexLst = ko.observable(index);
        //data.imgUrl = "../../../../../images/icon_customroute_remove.svg";
        data.imgUrl = this.customRouteImage;
        data.stop = ko.observable(data.planStopTime);
        data.travel = ko.observable(data.planDriveTime);
        data.distance = ko.observable(data.planDistance);
        data.radiusDisplay = ko.observable(data.radius);
        data.minPark = ko.observable(data.insideWaypointMinTime);
        data.maxPark = ko.observable(data.insideWaypointMaxTime);
        data.waypoint = ko.observable({
            item: {
                label: data.jobWaypointName,
                name: data.jobWaypointName,
                latitude: data.lat,
                longitude: data.lon,
                id:data.customPoiId
            }
        });
        // data.waypoint = ko.observable(null);
        data.requestWaypoint = this.getAutoCompleteList;
        //data.requestWaypoint = null;
        data.dType = ko.observable(data.deliveryType);
        data.arrival = ko.observable(displayArrival);
        data.depart = ko.observable(displayDepart);
        data.isStartArrival = ko.observable(isStartArrival);
        data.isEnabled = ko.observable(isEnabled);
        data.shipmentToAddressDisplay = ko.observable(data.customerAddress);
        data.waypointVal = ko.observable(data.jobWaypointName);
        data.totalDOs = ko.observable(data.totalDOs);
        data.displayTotalDOs = data.totalDOs?data.totalDOs:null
        data.jobwayPointStatus = ko.observable(data.jobWaypointStatusDisplayName);
        data.InfoStatus = Enums.InfoStatus.Update;
        data.assignOrder = data.assignOrder;
        data.dTypeId = null;
        data.arrivalEnable = ko.observable(arrivalEnable);
        data.departEnable = ko.observable(isStartArrival);
        data.arrivalId = ko.observable("arrival" + index);
        data.departId = ko.observable("depart" + index);
        data.arrivalMinDate = this.startDateTime().date;
        data.tempPlanStopTime = data.planStopTime;
        // data.requestWaypoint = this.shipmentGridControl().getAutoCompleteList(); //this.getAutoCompleteList;

        return data;
    }

    addMinutes(currentDate, minutes) {
        var result = new Date(currentDate);
        result.setMinutes(parseInt(result.getMinutes()) + (parseInt(minutes)));
        return result
    }

    clearShipment() {

        this.ddShipmentTemplate.poke(null);
        this.txtShipmentName(null);
        this.txtShipmentCode(null);
        this.txtStartDate(new Date());
        this.txtStartTime(this.checkTime());
        this.txtReference(null);
        this.txtWaitingTime(0);
        this.selectedZone(null);
        this.selectedTravelMode(null);
        this.selectedShipmentCategory(null);
        this.txtEnumJobAutoFinish(ScreenHelper.findOptionByProperty(this.ddEnumJobAutoFinish, "value", this.defaultValue().jobAutoFinish));
        this.txtLastShipmentRole(ScreenHelper.findOptionByProperty(this.ddLastShipmentRole, "value", this.defaultValue().lateShipmentRole));
        this.txtDuration(this.defaultValue().duration);
        this.txtDescription(null);
        this.codriverarray.replaceAll([]);

        let shipmentTempList = _.clone(this.ddShipmentTemplateOptions());
        let zoneList = _.clone(this.zone());
        let travelModeList = _.clone(this.travelMode());
        let shipmentCategoryList = _.clone(this.shipmentCategory());

        this.ddShipmentTemplateOptions.replaceAll([]);
        this.zone.replaceAll([]);
        this.travelMode.replaceAll([]);
        this.shipmentCategory.replaceAll([]);

        this.ddShipmentTemplateOptions.replaceAll(shipmentTempList);
        this.zone.replaceAll(zoneList);
        this.travelMode.replaceAll(travelModeList);
        this.shipmentCategory.replaceAll(shipmentCategoryList);

    }

    setupExtend() {
        this.txtShipmentName.extend({
            trackChange: true
        });
        this.txtShipmentCode.extend({
            trackChange: true
        });
        this.ddShipmentTemplate.extend({
            trackChange: true
        });
        this.txtStartDate.extend({
            trackChange: true
        });
        this.txtStartTime.extend({
            trackChange: true
        });
        this.txtWaitingTime.extend({
            trackChange: true
        });
        this.businessUnit.extend({
            trackChange: true
        });
        this.txtVehicle.extend({
            trackChange: true
        });
        this.txtDriver.extend({
            trackChange: true
        });
        this.txtTrail.extend({
            trackChange: true
        });
        this.txtDelivertMan.extend({
            trackChange: true
        });
        this.txtDescription.extend({
            trackChange: true
        });
        this.txtReference.extend({
            trackChange: true
        });
        this.shipmentData.extend({
            trackChange: true
        });
        this.totalWayPoint.extend({
            trackChange: true
        });
        this.isManageSave.extend({
            trackChange: true
        });
        this.jobDetail.extend({
            trackChange: true
        });

        this.txtDriver.extend({
            required: {
                onlyIf: () =>{
                    let valDriverBu = this.driverBU();
                    if(valDriverBu && valDriverBu.hasOwnProperty('value')){
                        valDriverBu = valDriverBu.value;
                    }
                    return valDriverBu ? true : false;
                }
            }
        });
        //this.shipmentJobWaypointInfoLst.extend({
        //    trackChange: true
        //});
        // this.codriverarray.extend({
        //     trackArrayChange: true
        // });

        this.txtVehicle.extend({
            required: {
                onlyIf: () =>{
                    let valVechicleBU = this.vechicleBU();
                    if(valVechicleBU && valVechicleBU.hasOwnProperty('value')){
                        valVechicleBU = valVechicleBU.value;
                    }
                    return valVechicleBU ? true : false;
                }
            }
        });

        this.txtShipmentName.extend({
            required: true
        });
        this.txtStartDate.extend({
            required: true
        });
        this.txtShipmentName.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });


        this.txtShipmentCode.extend({
            required: true
        });


        this.txtShipmentCode.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        this.txtStartTime.extend({
            required: true
        });
        this.businessUnit.extend({
            required: true
        });

        // if (this.txtDisable()) {
        //     this.txtDriver.extend({
        //         required: {
        //             onlyIf: () => {
        //                 return !_.isNil(this.txtVehicle());
        //             }
        //         }
        //     });
        // }
        this.validationVehicle = ko.validatedObservable({
            txtVehicle: this.txtVehicle,
        });
        this.validationDriver = ko.validatedObservable({
            txtDriver: this.txtDriver,
        });




        var paramsValidation = {
            txtShipmentName: this.txtShipmentName,
            txtShipmentCode: this.txtShipmentCode,
            txtStartTime: this.txtStartTime,
            txtStartDate: this.txtStartDate,
            businessUnit: this.businessUnit
        }
        this.validationModel = ko.validatedObservable(paramsValidation);
    }
    /* Web Request */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }
    get webRequestGlobalSearch() {
        return WebRequestGlobalSearch.getInstance();
    }
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    get webRequestMap() {
        return WebRequestMap.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }

    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }
    
    
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentManage),
    template: templateMarkup
};