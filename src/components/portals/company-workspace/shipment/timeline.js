﻿import ko from "knockout";
import templateMarkup from "text!./timeline.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import Utility from "../../../../app/frameworks/core/utility";
import {
    Constants
} from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import {
    Enums
} from "../../../../app/frameworks/constant/apiConstant";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import AssetInfo from "../asset-info";
import * as EventAggregator from "../../../../app/frameworks/constant/eventAggregator";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentTimelineScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentPOSearchScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        $('.app-blade-content').css('background-color', '#FFFFFF'); //set background สีขาว
        this.setFormStyle(); //set style in resolution 1366&1600
        

        this.bladeTitle(this.i18n("Transportation_Mngm_ShipmentTimeline_Title")());
        this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.showTooltip = ko.observable(null)
        
        this.data = ko.observableArray([])

        this.timeOutHandler = this.ensureNonObservable(null);
        this.timeOutButton = this.ensureNonObservable(null);
        this.isAutoRefresh = ko.observable(false);


        this.isNotChecked = ko.observable(false);
        this.txtKeyword = ko.observable(null);

        this.businessUnitList = ko.observableArray();
        this.businessUnit = ko.observable();

        this.siteOptions = ko.observableArray();
        this.site = ko.observable();

        this.consignmentOptions = ko.observableArray();
        this.consignment = ko.observable();

        this.nextWaypointStatusOptions = ko.observableArray();
        this.nextWaypointStatus = ko.observable();

        // legend value
        // this.all = ko.observable('10');
        // this.delayVal = ko.observable('4');
        // this.mayBeDelayVal = ko.observable('2');
        // this.onTimeVal = ko.observable('3');
        // this.planVal = ko.observable('2');
        // this.insideWaypointVal = ko.observable('1');

        // this.finishedVal = ko.observable('2');
        //
        
        this.lblLastSync = ko.observable(`( ${this.i18n("Vehicle_MA_LastSync")()} ${''} ) `)
        this.placeHolderEx = ko.observable(this.i18n("Transportation_Mngm_ShipmentTimeline_Search_Placeholder")())

        this.businessUnitOptions = ko.observableArray();
        this.businessUnit = ko.observable();

        this.siteValue = ko.observable();
        this.siteOptions = ko.observableArray();

        this.sortByValue = ko.observable();
        this.sortByOptions = ko.observableArray([]);

        // this.selectedNodes = ko.observable(1)
       
        this.truckStatusOptions = ko.observableArray([]);
        this.truckStatusValue = ko.observableArray([]);

        this.paramStatus = ko.observable();

        // this.currentDateTime = new Date();

        this.isDisable = ko.observable(false);
        this.isShow = ko.observable(true);
        
        this.newStartIndex = ko.observable(-1);
        this.newEndIndex = ko.observable(-1);
        this.selectedDate = ko.observable(new Date());

        // this.dateTime = new Date();
        // this.dateTime.setHours(6);
        // this.dateTime.setHours(0);
        // this.dateTime.setHours(0);

        var startDateTime = new Date();
        //var startDateTime = new Date('2018-07-12'); //mock current time
        startDateTime.setHours(6);
        startDateTime.setMinutes(0);
        startDateTime.setSeconds(0);
        var endDateTime = new Date(startDateTime);
        endDateTime.setDate(endDateTime.getDate() + 1);
        endDateTime.setSeconds(endDateTime.getSeconds() - 1);

        this.zoomFactor = ko.observable(1);
        this.dateCounter = ko.observable(0);
        
        this.startDateTime = Utility.formatDateTime(startDateTime);
        this.endDateTime = Utility.formatDateTime(endDateTime);

        
        this.datetimeNow = new Date();

        //MOck current time
        // this.datetimeNow = new Date(this.startDateTime);
        // var tempMock = new Date();
        // this.datetimeNow.setHours(tempMock.getHours());
        // this.datetimeNow.setMinutes(tempMock.getMinutes());
        // this.datetimeNow.setSeconds(tempMock.getSeconds());

        //console.log('this.startDateTime>>',this.startDateTime);
        //console.log('this.endDateTime>>',this.endDateTime);

        this.isLoadDataComplete = false;

        this.autoRefreshMinutes = WebConfig.appSettings.shipmentTimelineAutoRefreshMinutes;
        this.lblAutoRefresh = ko.observable(this.i18n("Transportation_Mngm_ShipmentTimeline_Auto_Refresh", [this.autoRefreshMinutes]));

        this.filter = ko.observable({});

        this.isNotChecked.subscribe(isChecked=>{ //click autorefresh
        
            if(isChecked){ //ถ้าเป็น true
            this.isAutoRefresh(true)
            this.onAutoRefresh() //call function
            }else{ //ถ้าเป็น false
            this.isAutoRefresh(false)
            this.onAutoRefresh(true)
            }
        })

        this.currentTime = ko.observable(new Date());

         //Map Subscribe
         this._eventAggregator.subscribe(
            EventAggregator.MAP_COMMAND_COMPLETE,
            info => {
                switch (info.command) {
                    case "open-shipment-legend":
                        this.minimizeAll();
                        break;
                    default:
                        break;
                }
            }
          );
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    setFormStyle(){
        let screenWidth = screen.width;
        
        if(screenWidth < 1920){
            $('.right-side-form').css('width', '25%');
            $('.legend-oval').css('width', '30%') 
        }
        
    }

    tooltipHandler(params, item) {
        // New Template
        // console.log('item',item);
        // console.log('params',params);
        let template = null;
        if (item != undefined) {
            if (item.attrs.circleName == 'ActualTooltip') {
                template = "ETA: " + this.formatTooltipText(params.currentTruckDetail.formatNextWaypointEstimateDateTime) + "\n" +
                    "Remain: " + this.formatTooltipText(params.currentTruckDetail.formatNextWaypointEstimateDistance) +" "+ this.i18n("Transportation_Mngm_Shipment_KMS")() + "\n" +
                    "Driver: " + this.formatTooltipText(params.currentTruckDetail.driverName) + "\n" +
                    "Phone: " + this.formatTooltipText(params.currentTruckDetail.phoneNumber)
            } else {                                                            
                if (params.shipment) {
                    template = params.shipment.shipmentCode
                } else {
                    params.waypointDetail.map((itemsPlan) => {
                        if (itemsPlan.assingOrder == item.attrs.circleNumber || itemsPlan.assingOrder == item.attrs.text) {
                            template = "Plan Arrive: " + this.formatTooltipText(itemsPlan.formatArrivePlan) + "\n" +
                                "Actual Arrive: " + this.formatTooltipText(itemsPlan.formatArriveActual) + "\n" +
                                "Plan Depart: " + this.formatTooltipText(itemsPlan.formatDepartPlan) + "\n" +
                                "Actual Depart: " + this.formatTooltipText(itemsPlan.formatDepartActual) +" "
                        }

                    })
                }

            }
        } else {
            if (params.currentTime) {
                template = params.currentTime
            }
        }


        this.showTooltip({
            template,
            date: new Date() // tricker for subscribe
        })
    }

    formatTooltipText(text) {
        return text ? text : "-";
    }

    onAutoRefresh(reCall){
        if (reCall) { //clear timeout เพื่อเริ่มนับเวลาใหม่
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) { //click auto refresh
            this.timeOutHandler = setTimeout(() => {
                this.getDataTimeline();
                this.onAutoRefresh();
            },(1000 * 60) * this.autoRefreshMinutes);
        } else {
            clearTimeout(this.timeOutHandler);
        }
        
    }
    onSearch() {
        this.isBusy(true);
        var self = this
        let filter = {
            "startDate": this.startDateTime,
            "endDate": this.endDateTime,
            "sortBy":  this.sortByValue().value,
            "jobTimelineTruckStatus": this.truckStatusValue(),
            "businessUnitL1Id":  (this.businessUnit()) ?this.businessUnit().id : null,
            "businessUnitL2Id": (this.siteValue()) ? this.siteValue().id : null,
            "keyword":this.txtKeyword()

        }

        this.filter(filter);
        
        this.refreshData().done((responsedfdShipmentTimeline)=>{
            this.lblLastSync(`( ${this.i18n("Vehicle_MA_LastSync")()} ${responsedfdShipmentTimeline.formatTimestamp} ) `)

            this.newStartIndex(0);
            this.newEndIndex(WebConfig.appSettings.shipmentTimelineDisplayRow - 1);
            this.selectedDate(new Date(this.startDateTime));
            this.currentTime(this.datetimeNow)

            // responsedfdShipmentTimeline.items[0].detail = this.getTestData01();
            if (responsedfdShipmentTimeline.items.length !== 0) {
                if(responsedfdShipmentTimeline.items[0].detail.length !== 0){

                    this.isShow(true);
                    
                    this.zoomFactor(1);
                    this.dateCounter(0);

                    this.data.replaceAll(responsedfdShipmentTimeline.items[0].detail);

                    var paramStatus = {
                        delay : responsedfdShipmentTimeline.items[0].delayTotal,
                        finish : responsedfdShipmentTimeline.items[0].finishTotal,
                        insideWaypoint : responsedfdShipmentTimeline.items[0].insideWaypointTotal,
                        maybeDelay : responsedfdShipmentTimeline.items[0].maybeDelayTotal,
                        onTime : responsedfdShipmentTimeline.items[0].onTimeTotal,
                        plan : responsedfdShipmentTimeline.items[0].planTotal,
                        total : responsedfdShipmentTimeline.items[0].total

                    };

                    this.paramStatus(paramStatus);
                    this.onAutoRefresh(true); 
                }else{
                    
                    this.isShow(false)
                    $("#" + self.id).html(this.i18n('Common_DataNotFound')())
                }
                
            }else{
                this.isShow(false)
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())

            }

            clearTimeout(this.timeOutHandler);
            this.isDisable(false)
            this.isBusy(false);
            this.onAutoRefresh(true)
        });
    }

    getDataTimeline(){
        this.isBusy(true);
        let filter = {
            "startDate": this.startDateTime,
            "endDate": this.endDateTime,
            "sortBy": this.sortByValue().value,
            "jobTimelineTruckStatus": this.truckStatusValue(),
            "businessUnitL1Id": this.businessUnit(),
            "businessUnitL2Id": this.siteValue(),
            "keyword":this.txtKeyword()
        }        
        this.filter(filter);

        this.refreshData().done((responsedfdShipmentTimeline)=>{
            this.lblLastSync(`( ${this.i18n("Vehicle_MA_LastSync")()} ${responsedfdShipmentTimeline.formatTimestamp} ) `)
            //this.selectedDate(new Date(responsedfdShipmentTimeline.timestamp));
            if (responsedfdShipmentTimeline.items.length !== 0) {
                if(responsedfdShipmentTimeline.items[0].detail.length !== 0){
                    this.selectedDate(new Date());
                    this.currentTime(new Date());
                    this.data.replaceAll(responsedfdShipmentTimeline.items[0].detail);
                }else{
                    
                    this.isShow(false)
                    $("#" + self.id).html(this.i18n('Common_DataNotFound')())
                }
            }else{
                    
                this.isShow(false)
                $("#" + self.id).html(this.i18n('Common_DataNotFound')())
            }
            
        this.isBusy(false);
        if(this.isDisable()){
            var bladeId = "#" + $(this)[0]._blade.id;
            var classElemRefresh = bladeId + " .app-commandBar-itemList>.app-br-default:nth-of-type(1)>.app-commandBar-item"
            $(classElemRefresh).removeClass("app-has-hover");
            $(classElemRefresh).addClass("app-commandBar-itemDisabled"); 
        }
        });
       
    }

    onTimeLineClick(params) {
        if(params.shipment != undefined){
            this.navigate("cw-shipment-summary", { shipmentId: params.shipment.shipmentId });
        }else{
            this.isBusy(true);
            var viewOnMapQuery = this.webRequestShipment.getDataViewOnMap(params.shipmentId);
            $.when(viewOnMapQuery).done(res => {
                this.isBusy(false);
                this.prepareViewOnMapData(res);
            });
        }
        
       // this.navigate("cw-shipment-list", null)
        

    }
    prepareViewOnMapData(result) {

        result.driverName = result.driverName.trim();

        var viewonmapData = {
            panel: {
                jobName: _.isNil(result.jobName) ? "-" : result.jobName,
                driverName: _.isNil(result.driverName) ? "-" : result.driverName,
                jobCode: _.isNil(result.jobCode) ? "-" : result.jobCode,
                jobStatusDisplayName: _.isNil(result.jobStatusDisplayName) ? "-" : result.jobStatusDisplayName,
                vehicleLicense: _.isNil(result.vehicleLicense) ? "-" : result.vehicleLicense
            },
            actual: {
                pin: [],
                route: []
            },
            plan: {
                pin: [],
                route: []
            },
            vehicle: []
        };

        _.forEach(result.shipmentPlanInfos, (planInfo) => {
            var pinItem = {
                longitude: planInfo.longitude,
                latitude: planInfo.latitude,
                attributes: {
                    jobWaypointStatus: planInfo.jobWaypointStatus,
                    jobWaypointName: planInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": planInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": planInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": planInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": planInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": planInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.plan.pin.push(pinItem);
            viewonmapData.plan.route = viewonmapData.plan.route.concat(planInfo.shipmentLocations);
        });

        _.forEach(result.shipmentActualInfos, (actualInfo) => {
            var pinItem = {
                longitude: actualInfo.longitude,
                latitude: actualInfo.latitude,
                attributes: {
                    jobWaypointStatus: actualInfo.jobWaypointStatus,
                    jobWaypointName: actualInfo.jobWaypointName,
                    info: [
                        {
                            "title": this.i18n("waypoint-info-status")(),
                            "key": "jobWaypointStatusDisplayName",
                            "text": actualInfo.jobWaypointStatusDisplayName || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-planIncomingDate")(),
                            "key": "formatPlanIncomingDate",
                            "text": actualInfo.formatPlanIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualIncomingDate")(),
                            "key": "formatActualIncomingDate",
                            "text": actualInfo.formatActualIncomingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-actualOutgoingDate")(),
                            "key": "formatActualOutgoingDate",
                            "text": actualInfo.formatActualOutgoingDate || '-'
                        },
                        {
                            "title": this.i18n("waypoint-info-remark")(),
                            "key": "jobDescription",
                            "text": actualInfo.jobDescription || '-'
                        }
                    ]
                }
            };
            viewonmapData.actual.pin.push(pinItem);
            viewonmapData.actual.route = viewonmapData.actual.route.concat(actualInfo.shipmentActualRouteInfos);
        });


        var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];
        _.forEach(result.locationInfo, (locationInfo) => {


            var locationInfoStatus = AssetInfo.getLocationInfoStatus(locationInfo);
            var shipmentInfo = AssetInfo.getShipmentInfo(locationInfo, locationInfoStatus);

            locationInfo.vehicleLicense = _.isNil(locationInfo.vehicleLicense) ? "-" : locationInfo.vehicleLicense;
            
            var vehicleItem = {
                longitude: locationInfo.longitude,
                latitude: locationInfo.latitude,
                attributes: {
                    "TITLE": locationInfo.vehicleLicense,
                    "SHIPMENTINFO": shipmentInfo
                }
            };

            var vehicleIcon = result.vehicleIconPath["1"];
            var movementImageUrl = "";
            _.forEach(result.vehicleIconPath["1"], (vehicleIcon) => {
                if (vehicleIcon.movementType == locationInfo.movement) {
                    movementImageUrl = vehicleIcon.imageUrl + "/" + directionConst[locationInfo.directionType - 1] + ".png"
                }
            });
            vehicleItem.movementImageUrl = Utility.resolveUrl(movementImageUrl);
            viewonmapData.vehicle.push(vehicleItem);
        });
        MapManager.getInstance().clear();
        MapManager.getInstance().openShipmentLegend({ data: viewonmapData });
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {


        var dfd = $.Deferred();

        if (!isFirstLoad) {
            return;
        }
        this.currentTime().setDate(12)
        this.currentTime().setMonth(6);
        this.currentTime().setFullYear(2018);

        this.isBusy(true);
        this.currentTime(new Date());




        let dfdEnumJobTimelineTruckStatus = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobTimelineTruckStatus]
        });
        let dfdEnumJobTimelineSort = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobTimelineSort]
        });
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            hideAll: true
        }

        let dfdBusinessUnit1 = this.webRequestBusinessUnit.listBusinessUnitParentSummary(businessUnitFilter);


        $.when(dfdEnumJobTimelineTruckStatus, dfdEnumJobTimelineSort, dfdBusinessUnit1).done((resJobTimelineTruckStatus, resJobTimelineSort, resdfdBusinessUnit1) => {
            let truckStatusObjAll = {
                displayName: this.i18n("Common_All")(),
                value: 104,
                id: 104,
                type: null
            }
            resJobTimelineTruckStatus.items.push(truckStatusObjAll)
            this.truckStatusOptions(resJobTimelineTruckStatus.items);
            this.sortByOptions(resJobTimelineSort.items);


            this.truckStatusValue([Enums.JobTimelineTruckStatus.Delay, Enums.JobTimelineTruckStatus.MaybeDelay]);
            this.sortByValue({ value: Enums.JobTimelineSort.TruckStatus });

            this.businessUnitOptions(resdfdBusinessUnit1.items);

            this.isBusy(false);
            this.isLoadDataComplete = true;
            this.onSearch();
            dfd.resolve();
        });


        var self = this;
        this.businessUnit.subscribe((params) => {
            if (self.isLoadDataComplete){
                if (params != undefined) {

                    let businessUnitFilter2 = {
                        businessUnitId: params.id
                    }
                    self.webRequestBusinessUnit.listBusinessUnitChildListSummary(businessUnitFilter2).done((res) => {
                        self.siteOptions(res.items)
                    }).fail((e) => {
                        console.log("error", e)
                    });

                } else {
                    self.siteOptions('');
                }
            }
        });
      
        return dfd;

    }



    refreshData() {

        var dfd = $.Deferred();

        let dfdShipmentTimeline = this.webRequestShipment.shipmentTimelineSummary(this.filter());
        $.when(dfdShipmentTimeline).done((responsedfdShipmentTimeline) => {
            dfd.resolve(responsedfdShipmentTimeline);
        });

        return dfd;
    }



    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { 
        this.isAutoRefresh(false);
        MapManager.getInstance().closeViewOnMap();

    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(  this.createCommand( "cmdRefresh",  this.i18n("Common_Refresh")(),   "svg-cmd-refresh"  ) );
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdRefresh":
            if(!this.isDisable()){
                this.isDisable(true)
                this.isEnabledHeaderBtn();
            }
            
                //this.getDataTimeline();
                break;
            default:
                break;
        }
    }

    isEnabledHeaderBtn(isShow) {
        var bladeId = "#" + $(this)[0]._blade.id;
        var classElemRefresh = bladeId + " .app-commandBar-itemList>.app-br-default:nth-of-type(1)>.app-commandBar-item";

        this.getDataTimeline();
        $(classElemRefresh).addClass("app-commandBar-itemDisabled"); 

        this.timeOutButton = setTimeout(()=>{
            $(classElemRefresh).removeClass("app-commandBar-itemDisabled");
            clearTimeout(this.timeOutButton);
        this.isDisable(false)

            // this.isEnabledHeaderBtn(true);
        },(1000 * 60));
    }
    getTestData01() {

       
        let dataTest;
        var jsonArr = [];

        for (var i = 1; i <= 50; i++) {

            
            jsonArr.push({
                "shipmentId": 93900,
                "shipmentCode": "SH20181008-0025",
                "vehicleId": 580,
                "license": "นว70-3429",
                "timelineTruckStatus": 5,
                "timelineTruckStatusColor": "#e7e6e6",
                "waypointDetail": [
                  {
                    "waypointId": 195793,
                    "waypointCode": "0210054050",
                    "waypointName": null,
                    "latitude": 14.64124,
                    "longitude": 101.12633,
                    "waypointStatus": 1,
                    "waypointStatusColor": "#e7e6e6",
                    "arrivePlan": "2018-10-08T18:01:37",
                    "arriveActual": null,
                    "departPlan": "2018-10-08T18:01:37",
                    "departActual": null,
                    "assingOrder": 1,
                    "actualOrder": 0,
                    "formatArrivePlan": "8/10/2018 18:01",
                    "formatArriveActual": null,
                    "formatDepartPlan": "8/10/2018 18:01",
                    "formatDepartActual": null
                  },
                  {
                    "waypointId": 195794,
                    "waypointCode": "0310000022",
                    "waypointName": null,
                    "latitude": 17.61262,
                    "longitude": 100.12285,
                    "waypointStatus": 1,
                    "waypointStatusColor": "#e7e6e6",
                    "arrivePlan": "2018-10-10T15:00:00",
                    "arriveActual": null,
                    "departPlan": "2018-10-10T15:00:00",
                    "departActual": null,
                    "assingOrder": 2,
                    "actualOrder": 0,
                    "formatArrivePlan": "10/10/2018 15:00",
                    "formatArriveActual": null,
                    "formatDepartPlan": "10/10/2018 15:00",
                    "formatDepartActual": null
                  },
                  {
                    "waypointId": 195795,
                    "waypointCode": "0110011285",
                    "waypointName": null,
                    "latitude": 14.5407,
                    "longitude": 101.077,
                    "waypointStatus": 1,
                    "waypointStatusColor": "#e7e6e6",
                    "arrivePlan": "2018-10-10T15:00:00",
                    "arriveActual": null,
                    "departPlan": "2018-10-10T15:00:00",
                    "departActual": null,
                    "assingOrder": 3,
                    "actualOrder": 0,
                    "formatArrivePlan": "10/10/2018 15:00",
                    "formatArriveActual": null,
                    "formatDepartPlan": "10/10/2018 15:00",
                    "formatDepartActual": null
                  }
                ],
                "currentTruckDetail": {
                  "currentDateTime": null,
                  "currentLocationDescription": null,
                  "timelineDateTime": null,
                  "nextWaypointEstimateDateTime": null,
                  "nextWaypointEstimateDistance": null,
                  "nextWaypointId": null,
                  "nextWaypointCode": null,
                  "nextWaypointName": null,
                  "employeeId": null,
                  "driverName": null,
                  "phoneNumber": null,
                  "formatCurrentDateTime": null,
                  "formatTimelineDateTime": null,
                  "formatNextWaypointEstimateDateTime": null,
                  "formatNextWaypointEstimateDistance": null
                }
              });
        }
        
        return jsonArr;

        // return [{
        //     "shipmentId": 1, //เลขID Shipment
        //     "vehicleId": 1, //เลขID Vehicle
        //     "vehicleLicense": "ปบ-0001", //ทะเบียนรถ
        //     "shipmentNo": "SH-000001", //รหัสShipment
        //     "statusId" : 1, //IDสถานะShipment
        //     "colorStatus": "#b3b3b3", //สีของสถานะ
        //     "plan": [{
        //         "waypointNumber": "001", //รหัสWaypoint
        //         "dateTime": "2018-07-14T06:30:23", //Date Time สำหรับPlotบนTimeline
        //         "latitude": 13, //Latitude (ถ้าต้องใช้)
        //         "longitude": 100, //Longitude (ถ้าต้องใช้)
        //         "waypointStatus": 1, //IDสถานะWaypointว่าจะส่งตรงเวลา หรือสาย หรือยังไม่เริ่ม
        //         "waypointColor": "green", //สีของสถานะWaypoint
        //         "formatArrivePlan" : "26/8/2018 17:55", //Plan Arrive สำหรับแสดงTooltip
        //         "formatArriveActual" : "26/8/2018 17:00", //Actual Arrive สำหรับแสดงTooltip
        //         "formatDepartPlan" : "26/8/2018 17:10",  //Plan Depart สำหรับแสดงTooltip
        //         "formatDepartActual" : "26/8/2018 17:54",//Actual Depart สำหรับแสดงTooltip
        //         "numberPlan": 1 //เลขของจุดWaypointสำหรับแสดงบนTimeline
        //     }, {
        //         "waypointNumber": "001",
        //         "dateTime": "2018-07-15T09:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:19", 
        //         "formatArriveActual" : "26/8/2018 17:52", 
        //         "formatDepartPlan" : "26/8/2018 17:22", 
        //         "formatDepartActual" : "26/8/2018 17:17",
        //         "numberPlan": 2
        //     }, {
        //         "waypointNumber": "001",
        //         "dateTime": "2018-07-15T16:00:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:13", 
        //         "formatArriveActual" : "26/8/2018 17:32", 
        //         "formatDepartPlan" : "26/8/2018 17:55", 
        //         "formatDepartActual" : "26/8/2018 17:42",
        //         "numberPlan": 3
        //     }],
        //     "actual": {
        //         "dateTime": "2018-07-15T12:00:30", //Date Time สำหรับPlotบนTimeline
        //         "formatETA" : "26/8/2018 18:12", //ETA ที่แสดงบนTooltip
        //         "formatDistanceRemain": "5.0km", //ระยะทางที่เหลือที่แสดงในTooltip
        //         "driverName": "Mr.Min", //ชื่อคนขับ
        //         "phoneNumber" :  "0894452687" //เบอร์โทรศัพท์คนขับ
        //     }
        // }, {
        //     "shipmentId": 2,
        //     "vehicleId": 2,
        //     "vehicleLicense": "ปบ-0002",
        //     "shipmentNo": "SH-000002",
        //     "colorStatus": "#ffffff",
        //     "plan": [{
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T08:00:23",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "yellow",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 1
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T10:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 2
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T13:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 3
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T15:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 4
        //     }],
        //     "actual": {
        //         "dateTime": "2018-07-15T11:00:30", 
        //         "formatETA" : "26/8/2018 11:33", 
        //         "formatDistanceRemain": "4.31km", 
        //         "driverName": "Mr.Boy", 
        //         "phoneNumber" :  "0893212345"
        //     }
        // },
        // {
        //     "shipmentId": 3,
        //     "vehicleId": 2,
        //     "vehicleLicense": "ปบ-0003",
        //     "shipmentNo": "SH-000003",
        //     "colorStatus": "#6dc242",
        //     "plan": [{
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T08:30:23",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "#BB120F",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 1
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T11:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 2
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T13:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 3
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T17:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 4
        //     }],
        //     "actual": {
        //         "dateTime": "2018-07-15T14:30:30", 
        //         "formatETA" : "26/8/2018 17:12", 
        //         "formatDistanceRemain": "2.34km", 
        //         "driverName": "Mr.Kim", 
        //         "phoneNumber" :  "0893212345"
        //     }
        // },
        // {
        //     "shipmentId": 4,
        //     "vehicleId": 2,
        //     "vehicleLicense": "ปบ-0004",
        //     "shipmentNo": "SH-000004",
        //     "colorStatus": "#d33737",
        //     "plan": [{
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T08:30:23",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 1
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T11:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 2
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T13:00:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 3
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T17:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 4
        //     }],
        //     "actual": {
        //         "dateTime": "2018-07-15T14:00:30", 
        //         "formatETA" : "26/8/2018 17:12", 
        //         "formatDistanceRemain": "2.34km", 
        //         "driverName": "Mr.Kim", 
        //         "phoneNumber" :  "0893212345"
        //     }
        // },
        // {
        //     "shipmentId": 5,
        //     "vehicleId": 2,
        //     "vehicleLicense": "ปบ-0005",
        //     "shipmentNo": "SH-000005",
        //     "colorStatus": "green",
        //     "plan": [{
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T08:30:23",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 1
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T11:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 2
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T13:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "red",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 3
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T15:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "#BB120F",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 4
        //     }],
        //     "actual": {
        //         "dateTime": "2018-07-15T12:00:30", 
        //         "formatETA" : "26/8/2018 17:12", 
        //         "formatDistanceRemain": "9.34km", 
        //         "driverName": "Mr.Non", 
        //         "phoneNumber" :  "0893212345"
        //     }
        // },
        // {
        //     "shipmentId": 6,
        //     "vehicleId": 2,
        //     "vehicleLicense": "ปบ-0006",
        //     "shipmentNo": "SH-000006",
        //     "colorStatus": "grey",
        //     "plan": [{
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T17:30:23",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "green",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 1
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T21:00:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "grey",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 2
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-15T21:30:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "grey",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 3
        //     }, {
        //         "waypointNumber": "002",
        //         "dateTime": "2018-07-16T10:00:30",
        //         "latitude": 13,
        //         "longitude": 100,
        //         "waypointStatus": 1,
        //         "waypointColor": "grey",
        //         "formatArrivePlan" : "26/8/2018 17:12", 
        //         "formatArriveActual" : "26/8/2018 17:12", 
        //         "formatDepartPlan" : "26/8/2018 17:12", 
        //         "formatDepartActual" : "26/8/2018 17:12",
        //         "numberPlan": 4
        //     }],
        //     "actual": {
        //         "dateTime": "2018-07-15T18:30:30", 
        //         "formatETA" : "26/8/2018 17:12", 
        //         "formatDistanceRemain": "9.34km", 
        //         "driverName": "Mr.Non", 
        //         "phoneNumber" :  "0893212345"
        //     }
        // }];


    }

    getTestData02() {
        return [{
            "shipmentId": 1,
            "vehicleId": 1,
            "vehicleLicense": "ปบ-0001",
            "shipmentNo": "ทั่วไป",
            "colorStatus": "#4C9E88",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-15T08:30:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 2
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T16:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 3
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-15T08:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-15T13:50:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 1
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T14:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": true,
                    "color": "#c00000",
                    "numberWaypoint": 2
                },
                "depart": {
                    "dateTime": "2018-07-15T15:50:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 2
                },
                "lineColor": "#70ad47"
            }]
        }, {
            "shipmentId": 2,
            "vehicleId": 2,
            "vehicleLicense": "ปบ-0002",
            "shipmentNo": "ทั่วไป ยังไม่เข้าจุดสุดท้าย",
            "colorStatus": "#C2C18B",
            "plan": [{
                "waypointNumber": "002",
                "dateTime": "2018-07-15T08:30:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 2
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T13:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 3
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T15:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 1,
                "waypointStatus": 1,
                "numberPlan": 4
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-15T08:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-15T09:50:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 1
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T12:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 2
                },
                "depart": {
                    "dateTime": "2018-07-15T12:30:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 2
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T13:30:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 3
                },
                "depart": {
                    "dateTime": "2018-07-15T14:30:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 1,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 3
                },
                "lineColor": "#70ad47"
            }]
        }, {
            "shipmentId": 3,
            "vehicleId": 3,
            "vehicleLicense": "ปบ-0003",
            "shipmentNo": "actual เวลาปัจจุบัน",
            "colorStatus": "#95240E",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-15T08:30:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 3,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 3,
                "waypointStatus": 1,
                "numberPlan": 2
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T16:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 3,
                "waypointStatus": 1,
                "numberPlan": 3
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-15T08:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 3,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-15T13:10:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 3,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 1
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T13:20:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 3,
                    "isLast": true,
                    "color": "#c00000",
                    "numberWaypoint": 2
                },
                "lineColor": "#c00000"
            }]
        }, {
            "shipmentId": 4,
            "vehicleId": 4,
            "vehicleLicense": "ปบ-0004",
            "shipmentNo": "ข้ามวันข้ามคืน",
            "colorStatus": "#41DA19",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-14T02:00:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 4,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-16T07:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 4,
                "waypointStatus": 1,
                "numberPlan": 2
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-10T08:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 4,
                    "isLast": true,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-15T20:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 4,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "lineColor": "#70ad47"
            }]
        }, {
            "shipmentId": 5,
            "vehicleId": 5,
            "vehicleLicense": "ปบ-0005",
            "shipmentNo": "ติดกันเยอะๆ",
            "colorStatus": "#8B4E19",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-15T10:00:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T10:10:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 2
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T10:20:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 3
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T10:30:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 4
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T10:40:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 5
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T10:50:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 6
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:00:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 7
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:10:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 8
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:20:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 9
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:30:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 5,
                "waypointStatus": 1,
                "numberPlan": 10
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-15T10:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-15T10:05:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 1
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T10:10:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 2
                },
                "depart": {
                    "dateTime": "2018-07-15T10:15:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 2
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T10:20:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 3
                },
                "depart": {
                    "dateTime": "2018-07-15T10:25:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 3
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T10:30:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 4
                },
                "depart": {
                    "dateTime": "2018-07-15T10:35:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 4
                },
                "lineColor": "#c00000"
            }, {
                "arrival": {
                    "dateTime": "2018-07-15T10:40:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": true,
                    "color": "#c00000",
                    "numberWaypoint": 5
                },
                "lineColor": "#c00000"
            }]
        }, {
            "shipmentId": 6,
            "vehicleId": 6,
            "vehicleLicense": "ปบ-0006",
            "shipmentNo": "ข้ามวันข้ามคืน2",
            "colorStatus": "#AB0B39",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-14T11:00:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 6,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-16T07:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 6,
                "waypointStatus": 1,
                "numberPlan": 2
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-10T03:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 6,
                    "isLast": true,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "lineColor": "#c00000"
            }]
        }, {
            "shipmentId": 7,
            "vehicleId": 7,
            "vehicleLicense": "ปบ-0007",
            "shipmentNo": "plan เป็นอดีต",
            "colorStatus": "#0D718C",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-13T11:00:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 7,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-13T18:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 7,
                "waypointStatus": 1,
                "numberPlan": 2
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-15T08:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": true,
                    "color": "#c00000",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-15T16:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 5,
                    "isLast": false,
                    "color": "#c00000",
                    "numberWaypoint": 1
                },
                "lineColor": "#c00000"
            }]
        }, {
            "shipmentId": 8,
            "vehicleId": 8,
            "vehicleLicense": "ปบ-0008",
            "shipmentNo": "actual เป็นอดีต",
            "colorStatus": "#79618A",
            "plan": [{
                "waypointNumber": "001",
                "dateTime": "2018-07-15T11:00:23",
                "latitude": 13,
                "longitude": 100,
                "poiId": 8,
                "waypointStatus": 1,
                "numberPlan": 1
            }, {
                "waypointNumber": "001",
                "dateTime": "2018-07-15T18:30:30",
                "latitude": 13,
                "longitude": 100,
                "poiId": 8,
                "waypointStatus": 1,
                "numberPlan": 2
            }],
            "actual": [{
                "arrival": {
                    "dateTime": "2018-07-13T08:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 8,
                    "isLast": true,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "depart": {
                    "dateTime": "2018-07-13T16:00:23",
                    "latitude": 13,
                    "longitude": 100,
                    "poiId": 8,
                    "isLast": false,
                    "color": "#70ad47",
                    "numberWaypoint": 1
                },
                "lineColor": "#70ad47"
            }]
        }];
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentTimelineScreen),
    template: templateMarkup
};



// HTML

// <!-- Form -->
//         <div style="width: 100%; float: left;">

//             <div style="width: 50%; float: left;">

//                     <div style="width: 30%; float: left;">

//                             <div style="text-align:left; float: left; margin-top: 2%; margin-right: 1%; ">
//                                 <!-- Transportation_Mngm_ShipmentTimeline_Sector -->
//                                 <gisc-ui-label params="for: 'ddBusinessUnit', text: i18n('Sector')"></gisc-ui-label>

//                             </div>

//                             <div style="width: 70%; text-align:left; float: left ">
//                                 <gisc-ui-dropdowntree params=" id: 'ddBusinessUnit',
//                                             options:businessUnitList,
//                                             optionsID:'id',
//                                             optionsText:'name',
//                                             optionsParent:'parentBusinessUnitId',
//                                             optionsChildren:'childBusinessUnits',
//                                             value:businessUnit,
//                                             mode:'update'" />
//                             </div>

//                         </div>

//                         <div style="width: 30%; float: left;">

//                             <div style="text-align:left; float: left; margin-top: 2%; margin-right: 1%; margin-left: 1%; ">
//                                 <!-- Transportation_Mngm_ShipmentTimeline_Site -->
//                                 <gisc-ui-label params="for: 'ddSite', text: i18n('Site')"></gisc-ui-label>
//                             </div>
//                             <div style="width: 70%; text-align:left; float: left ">
//                                 <gisc-ui-dropdown params=" id: 'ddSite',
//                                                 options:siteOptions,
//                                                 optionsID:'id',
//                                                 optionsText:'name',
//                                                 optionsParent:'parentBusinessUnitId',
//                                                 optionsChildren:'childBusinessUnits',
//                                                 value:site,
//                                                 mode:'update'" />
//                             </div>
//                         </div>

//                         <div style="width: 30%; float: left;">
//                             <div style=" text-align:left; float: left; margin-top: 2%; margin-right: 1%; margin-left: 1%; ">
//                                 <!-- <span data-bind="text: i18n('Report_KpiReport_To')"></span> -->
//                                 <!-- Transportation_Mngm_ShipmentTimeline_Consignment -->
//                                 <gisc-ui-label params="for: 'ddConsignment', text: i18n('Consignment')"></gisc-ui-label>
//                             </div>

//                             <div style="width: 70%; text-align:left; float: left ">
//                                 <gisc-ui-dropdown params="id: 'ddConsignment',
//                                         options: consignmentOptions,
//                                         optionsText: 'name',
//                                         value: consignment" />
//                             </div>
//                         </div>


//             </div>

//             <div style="width: 50%; float: left;">

//                     <div style="width: 15%; float: left;">
//                         <div style="width: 100%; margin-top: 2%; ">
//                                 <!-- Common_Search -->
//                                 <gisc-ui-button params="text: 'Search',
//                                                         onclick: onSearch.bind($data,'Search')" />
//                         </div>
//                     </div>

//                     <div style="width: 25%; float: left;">
//                             <div style="width: 100%; text-align:left; margin-top: 2%;  ">
//                                 <gisc-ui-label params="text: lblLastSync"></gisc-ui-label>
//                             </div>
//                         </div>

//             </div>

//         </div>





//         <!-- legend -->
//         <div style="width: 100%; float: left;">

//             <!-- Square -->
//             <div style="width: 50%; float: left;">

//                 <div data-bind="click: onLegendClick.bind($data,{id:0,name:'All'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Nextwaypoint_Delay -->
//                     <div data-bind="text: all" style="width: 100%; text-align:center; border:1px solid #696969; background-color:#696969; font-size: 25px; color: white"></div>
//                     <div style="width: 100%;">
//                         <gisc-ui-label params="text: i18n('All')"></gisc-ui-label>
//                     </div>
//                 </div>

//                 <div data-bind="click: onLegendClick.bind($data,{id:1,name:'Delay'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Nextwaypoint_Delay -->
//                     <div data-bind="text: delayVal" style="width: 100%; text-align:center; border:1px solid red; background-color: red; font-size: 25px;"></div>
//                     <div style="width: 100%;">
//                         <gisc-ui-label params="text: i18n('Delay')"></gisc-ui-label>
//                     </div>
//                 </div>

//                 <div data-bind="click: onLegendClick.bind($data,{id:2,name:'MaybeDelay'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Maybe_Delay -->
//                     <div data-bind="text: mayBeDelayVal" style="width: 100%; text-align:center; border:1px solid orange; background-color: orange; font-size: 25px;"></div>
//                     <div style="width: 150%;">
//                         <gisc-ui-label params="text: i18n('Maybe delay')"></gisc-ui-label>
//                     </div>
//                 </div>

//                 <div data-bind="click: onLegendClick.bind($data,{id:3,name:'Ontime'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Ontime -->
//                     <div data-bind="text: onTimeVal" style="width: 100%; text-align:center; border:1px solid green; background-color: green; font-size: 25px;"></div>
//                     <div style="width: 100%;">
//                         <gisc-ui-label params="text: i18n('On time')"></gisc-ui-label>
//                     </div>
//                 </div>

//                 <div data-bind="click: onLegendClick.bind($data,{id:4,name:'Plan'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Plan -->
//                     <div data-bind="text: planVal" style="width: 100%; text-align:center; border:1px solid #D3D3D3; background-color: #D3D3D3; font-size: 25px;"></div>
//                     <div style="width: 100%;">
//                         <gisc-ui-label params="text: i18n('Plan')"></gisc-ui-label>
//                     </div>
//                 </div>

//                 <div data-bind="click: onLegendClick.bind($data,{id:5,name:'InsideWaypoint'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Inside_Waypoint -->
//                     <div data-bind="text: insideWaypointVal" style="width: 100%; text-align:center; border:1px solid yellow; background-color: yellow; font-size: 25px;"></div>
//                     <div style="width: 150%;">
//                         <gisc-ui-label params="text: i18n('Inside Waypoint')"></gisc-ui-label>
//                     </div>
//                 </div>

//                 <div data-bind="click: onLegendClick.bind($data,{id:6,name:'Finished'})" style="width: 12%;  float: left; margin-right:15px; ">
//                     <!-- Transportation_Mngm_ShipmentTimeline_Inside_Waypoint -->
//                     <div data-bind="text: finishedVal" style="width: 100%; text-align:center; border:1px solid #D3D3D3; background-color: white; font-size: 25px;"></div>
//                     <div style="width: 150%;">
//                         <gisc-ui-label params="text: i18n('Finished')"></gisc-ui-label>
//                     </div>
//                 </div>

//             </div>



//             <!-- Circle -->
//             <div style="width: 50%; float: left; ">

//                 <div style="width: 100%;">

//                     <div style="width: 15%; float: left;   ">
//                         <!-- Transportation_Mngm_ShipmentTimeline_Ontimearrival_Ontimedepart -->
//                         <div class="circleBase gray" style="float: left"></div>
//                         <div style="margin-left: 20px; ">
//                             <gisc-ui-label params="text: i18n('Plan')"></gisc-ui-label>
//                         </div>
//                     </div>

//                     <div style="width: 30%; float: left;">
//                         <!-- Transportation_Mngm_ShipmentTimeline_Ontimearrival_Late depart -->
//                         <div class="circleBase yellow" style="float: left"></div>
//                         <div style=" margin-left: 20px; ">
//                             <gisc-ui-label params="text: i18n('On time arrival & Late depart')"></gisc-ui-label>
//                         </div>
//                     </div>

//                 </div>

//                 <div style="width: 100%; float: left; margin-top: 5px;">

//                         <div style="width: 15%; float: left;   ">
//                             <!-- Transportation_Mngm_ShipmentTimeline_Ontimearrival_Ontimedepart -->
//                             <div class="circleBase red" style="float: left"></div>
//                             <div style="margin-left: 20px; ">
//                                 <gisc-ui-label params="text: i18n('Late arrival')"></gisc-ui-label>
//                             </div>
//                         </div>

//                         <div style="width: 30%; float: left;">
//                             <!-- Transportation_Mngm_ShipmentTimeline_Ontimearrival_Late depart -->
//                             <div class="circleBase green" style="float: left"></div>
//                             <div style=" margin-left: 20px; ">
//                                 <gisc-ui-label params="text: i18n('On time arrival & On time depart')"></gisc-ui-label>
//                             </div>
//                         </div>

//                 </div>



//             </div>

//         </div>