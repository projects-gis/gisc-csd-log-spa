﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestShipmentCategory from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipmentCategory";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class ShipmentCategoryView extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.id = this.ensureNonObservable(params.id,0);

        this.bladeTitle(this.i18n("ShipmentCategory_View")());

        this.code = ko.observable();
        this.name = ko.observable();
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.enableJobCate = ko.observable();
        this.description = ko.observable();
        this.updateDate = ko.observable();
        this.updateBy = ko.observable();

        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([
            [1, "asc"]
        ]);

        this.order = ko.observable([[1, "asc"]]);

        this.subscribeMessage("cw-shipment-cate-changed", id => {
            this.getShipmentCategory(id)
        });
    }

    /**
     * Get WebRequest Shipment Category
     * @readonly
     */
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }


    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCategoryUpdate)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCategoryDelete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case 'cmdUpdate':
                this.navigate("cw-shipment-category-manage", { id: this.id, mode: Screen.SCREEN_MODE_UPDATE });
                break;
            case 'cmdDelete':
                this.showMessageBox(null, this.i18n("M243")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webRequestShipmentCategory.deleteShipmentCategory(this.id).done(() => {
                                    this.isBusy(false);

                                    this.publishMessage("cw-shipment-category-delete");
                                    this.close(true);
                                }).fail((e) => {
                                    this.handleError(e);
                                    this.isBusy(false);
                                });
                                break;
                        }
                    });
                break;
            default:
        }
    }

    buildActionBar(actions) {
        
    }

    onActionClick(sender) {
        
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        return this.getShipmentCategory(this.id);
    }

    getShipmentCategory(id) {

        var dfd = $.Deferred();
        this.webRequestShipmentCategory.getShipmentCategory(this.id).done((res) => {
            if (res) {
                this.code(res.code);
                this.name(res.name);
                this.enableJobCate(res.formatEnable);
                this.description(res.description);
                this.accessibleBusinessUnits(res.shipmentCategoryAccessibleInfos);
                this.updateBy(res.updateBy);
                this.updateDate(res.formatUpdateDate);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject();
            this.handleError(e);
        });
 
        return dfd;
    }

    setupExtend() {
    }

}

export default {
    viewModel: ScreenBase.createFactory(ShipmentCategoryView),
    template: templateMarkup
};