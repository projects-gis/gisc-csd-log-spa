﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestShipmentCategory from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipmentCategory";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";


/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class ShipmentCategoryList extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("ShipmentCategory_ShipmentCategoryList")());
        this.bladeSize = BladeSize.Large;

        this.jobCateItem = ko.observableArray([]);
        this.order = ko.observable([[1, "asc"]]);
        this.filterText = ko.observable("");
        this.recentChangedRowIds = ko.observableArray([]);

        this._selectingRowHandler = (data) => {
            return this.navigate("cw-shipment-category-view", {id : data.id });
        };


        this.subscribeMessage("cw-shipment-cate-changed", id => {
            this.webRequestShipmentCategory.listShipmentCategory().done((res) => {
                this.jobCateItem.replaceAll(res.items);
                if (id) {
                    this.recentChangedRowIds.replaceAll([id]);
                }
            }).fail((e) => {
                this.handleError(e);
            });
        });

        this.subscribeMessage("cw-shipment-category-delete", () => {
            return this.loadFindSummary();
        });

        this.renderDescription = (data, type, row) => {
            if (data) {
                let node = data.length > 30 ? data.substr(0, 30) + '…' : data;
                return node;
            }
            else {
                return "";
            }
        }

    }

    /**
     * Get WebRequest Category
     * @readonly
     */
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }


    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.ShipmentCategoryCreate)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
       
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("cw-shipment-category-manage", { mode: Screen.SCREEN_MODE_CREATE });
                break;
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        return this.loadFindSummary();

    }

    loadFindSummary() {
        var dfd = $.Deferred();
        this.webRequestShipmentCategory.listShipmentCategory().done((res) => {
            this.jobCateItem(res.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
}

export default {
    viewModel: ScreenBase.createFactory(ShipmentCategoryList),
    template: templateMarkup
};