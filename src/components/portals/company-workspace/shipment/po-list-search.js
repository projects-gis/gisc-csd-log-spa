﻿import ko from "knockout";
import templateMarkup from "text!./po-list-search.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";

import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentPOListSearchScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentPOSearchScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());

        this.bladeSize = BladeSize.Small;

        this.txtPONumber = ko.observable();
        this.txtShipmentCode = ko.observable();

        this.onDatasourceRequestPOName = (request, response) => {
            var filter = {
                poNumber: request.term,
                shipmentCode: this.txtShipmentCode(),
                shipmentFromDate: this.getCurrentDateTime('s'),
                shipmentToDate: this.getCurrentDateTime('e')
            }
            this.webRequestShipment.autocompletePONumber(filter).done((data) => {
                response( $.map( data, (item) => {
                    console.log("item", item);
                    return {
                        label: item.poNumber,
                        value: item.poNumber
                    };
                }));
            });
        }

        this.onDatasourceRequestShipmentCode = (request, response) => {
            var filter = {
                shipmentCode: request.term,
                poNumber: this.txtPONumber(),
                shipmentFromDate: this.getCurrentDateTime('s'),
                shipmentToDate: this.getCurrentDateTime('e')
            }
            this.webRequestShipment.autocompletePOShipmentCode(filter).done((data) => {
                response( $.map( data, (item) => {
                    return {
                        label: item.shipmentCode,
                        value: item.shipmentCode
                    };
                }));
            });
        }
    }

    getCurrentDateTime(type) {
        let dateTime = null;
        let objDate = new Date();
        let year = objDate.getFullYear().toString();
        let month = (objDate.getMonth() + 1).toString();
        let day = objDate.getDate().toString();
        let time = "T00:00:00";

        if(type == 'e'){
            time = "T23:59:59"
        }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        dateTime = year + "-" + month + "-" + day + time;

        return dateTime
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
    }

    setupExtend() {
        //this.txtPONumber.extend({ trackChange: true });;
        //this.txtShipmentCode.extend({ trackChange: true });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        switch(sender.id) {
            case "actSave":
                this.createSearchFilter();
                break;
            default:
                break;

        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) { 
        switch(sender.id) {
            case "cmdClear":
                this.clearSearchFilter();
                break;
            default:
                break;
        }
    }

    createSearchFilter() {
        let searchFilter = {
            PONumber: this.txtPONumber() == "" ? null : this.txtPONumber(),
            ShipmentCode: this.txtShipmentCode() == "" ? null : this.txtShipmentCode()
        };

        this.publishMessage("cw-posearch-search", searchFilter);
    }    

    clearSearchFilter() {
        this.txtPONumber("");
        this.txtShipmentCode("");
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
}

export default {
viewModel: ScreenBase.createFactory(ShipmentPOListSearchScreen),
    template: templateMarkup
};