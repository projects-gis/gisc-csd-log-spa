﻿import ko from "knockout";
import templateMarkup from "text!./default-value.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import { Constants, Enums  } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";

class DefaultValue extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Transportation_Mngm_Default_Value_Title")());
        this.bladeSize = BladeSize.Small;

        this.stopDepartTime = ko.observable();
        this.radius = ko.observable();
        this.minPark = ko.observable();
        this.maxPark = ko.observable();
        this.jobAutoFinishduration = ko.observable(null);
        this.jobAutoFinish = ko.observable(null);
        this.lateShipmentRole = ko.observable(null);
        this.pickupTime = ko.observable();
        this.deliveryTime = ko.observable();
        this.pickupTimeAndDeveryTime = ko.observable();
        this.durationMaxSpeed = ko.observable();
        this.maxSpeed = ko.observable();

        this.isAfterPlanEnd = ko.observable(false);
        this.enableDurationMaxSpeed = ko.observable();

        this.jobAutoFinishOptions = ko.observableArray();
        this.lastShipmentRoleOptions = ko.observableArray();

        this.maxSpeed.subscribe((val) => {
            let value = parseInt(val);
            if (value == 0) {
                this.enableDurationMaxSpeed(true);
            }
            else {
                this.durationMaxSpeed(null);
                this.enableDurationMaxSpeed(false);
            }
        });
 
    }

    setupExtend() {
        //Track Change
        this.stopDepartTime.extend({ trackChange: true });
        this.radius.extend({ trackChange: true });
        this.minPark.extend({ trackChange: true });
        this.maxPark.extend({ trackChange: true });
        this.jobAutoFinish.extend({ trackChange: true });
        this.lateShipmentRole.extend({ trackChange: true });
        this.pickupTime.extend({ trackChange: true });
        this.deliveryTime.extend({ trackChange: true });
        this.pickupTimeAndDeveryTime.extend({ trackChange: true });
        this.maxSpeed.extend({ trackChange: true });
        this.durationMaxSpeed.extend({ trackChange: true });

        //Validation
        this.stopDepartTime.extend({ required: true });
        this.radius.extend({ required: true });
        // this.minPark.extend({ required: true });
        // this.maxPark.extend({ required: true });
        this.jobAutoFinish.extend({ required: true });
        this.lateShipmentRole.extend({ required: true });
        this.pickupTime.extend({ required: true });
        this.deliveryTime.extend({ required: true });
        this.pickupTimeAndDeveryTime.extend({ required: true });
        this.jobAutoFinishduration.extend({
            required: {
                onlyIf: () => {
                    return this.isAfterPlanEnd()
                }
            }
        });
        this.durationMaxSpeed.extend({
            required: {
                onlyIf: () => {
                    return this.enableDurationMaxSpeed()
                }
            }
        });

        this.minPark.extend({
            max: {
                params: this.maxPark,
                message: this.i18n("M242")()
            },
            required: true
        });

        this.maxPark.extend({
            min: {
                params: this.minPark,
                message: this.i18n("M248")()
            },
            required: true
        });


        this.validationModel = ko.validatedObservable({
            stopDepartTime: this.stopDepartTime,
            radius: this.radius,
            minPark: this.minPark,
            maxPark: this.maxPark,
            jobAutoFinish: this.jobAutoFinish,
            lastShipmentRole: this.lastShipmentRole,
            jobAutoFinishduration: this.jobAutoFinishduration,
            durationMaxSpeed: this.durationMaxSpeed,
            pickupTime: this.pickupTime,
            deliveryTime: this.deliveryTime,
            pickupTimeAndDeveryTime: this.pickupTimeAndDeveryTime
        });
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

        if (!isFirstLoad)
            return;

        this.isBusy(true);
        let filter = { companyId: WebConfig.userSession.currentCompanyId };


        let dfd = $.Deferred();
        let dfdDefValue = this.loadDefaultValue(filter);
        let dfdEnumJobAutoFinish = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.JobAutoFinish]
        });
        let dfdLastShipmentRole = this.webRequestEnumResource.listEnumResource({
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.LastShipmentRole]
        });

        

        $.when(dfdDefValue, dfdEnumJobAutoFinish, dfdLastShipmentRole).done((resDefValue, resJobAuto, resLastShipment) => {
            this.stopDepartTime(resDefValue.stopTime);
            this.radius(resDefValue.radius);
            this.minPark(resDefValue.insideWaypointMinTime);
            this.maxPark(resDefValue.insideWaypointMaxTime);
            this.jobAutoFinishduration(resDefValue.jobAutoFinishDuration);
            this.pickupTime(resDefValue.pickUpTime);
            this.deliveryTime(resDefValue.deliveryTime);
            this.pickupTimeAndDeveryTime(resDefValue.pickUpDeliveryTime);
            this.maxSpeed(resDefValue.speedLimit);
            this.durationMaxSpeed(resDefValue.duration);

            if (resJobAuto){
                this.jobAutoFinishOptions(resJobAuto.items);
            }

            if (resLastShipment) {
                this.lastShipmentRoleOptions(resLastShipment.items);
            }

            let jobAutoFinish = !_.isNil(resDefValue.jobAutoFinish) ? ScreenHelper.findOptionByProperty(this.jobAutoFinishOptions, "value", resDefValue.jobAutoFinish) : null;//ScreenHelper.findOptionByProperty(this.jobAutoFinishOptions, "isDefault", true);
            let lastShipmentRole = !_.isNil(resDefValue.lateShipmentRole) ? ScreenHelper.findOptionByProperty(this.lastShipmentRoleOptions, "value", resDefValue.lateShipmentRole) : null;//ScreenHelper.findOptionByProperty(this.lastShipmentRoleOptions, "isDefault", true);
            this.jobAutoFinish(jobAutoFinish);
            this.lateShipmentRole(lastShipmentRole);

            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
            this.isBusy(false);
            dfd.reject(e);
        }).always(() => {
            this.isBusy(false);
            });

        this.jobAutoFinish.subscribe((ddlValue) => {
            //console.log("jobAutoFinish", ddlValue)
            if (ddlValue != null) {
                if (ddlValue.value == Enums.ModelData.JobAutoFinish.AfterPlanEnd) {
                    this.isAfterPlanEnd(true);
                }
                else {
                    this.isAfterPlanEnd(false);
                }
            }
            else {
                this.isAfterPlanEnd(false);
            }
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            let model = {
                companyId: WebConfig.userSession.currentCompanyId,
                stopTime: this.stopDepartTime(),
                radius: this.radius(),
                insideWaypointMinTime: this.minPark(),
                insideWaypointMaxTime: this.maxPark(),
                jobAutoFinish: this.jobAutoFinish() != null ? this.jobAutoFinish().value : null,
                jobAutoFinishDuration: this.isAfterPlanEnd() ? this.jobAutoFinishduration() : null,
                lateShipmentRole: this.lateShipmentRole() != null ? this.lateShipmentRole().value : null,
                speedLimit: this.maxSpeed(),
                duration: this.durationMaxSpeed(),
                pickUpTime: this.pickupTime(),
                deliveryTime: this.deliveryTime(),
                pickUpDeliveryTime: this.pickupTimeAndDeveryTime()
            };

            this.showMessageBox(null, this.i18n("M244")(),
                BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            this.saveDefaultValue(model).done(() => {
                                this.close(true);
                            }).fail((e) => {
                                this.handleError(e);
                                this.isBusy(false);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });

            //this.saveDefaultValue(model).done(() => {
            //    this.close(true);
            //}).fail((e) => {
            //    this.handleError(e);
            //    this.isBusy(false);
            //}).always(() => {
            //    this.isBusy(false);
            //});
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    loadDefaultValue(filter) {
        return this.webRequestShipment.getDefaultValue(filter);
    }

    saveDefaultValue(filter) {
        return this.webRequestShipment.updateDefaultValue(filter);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }
}

export default {
    viewModel: ScreenBase.createFactory(DefaultValue),
    template: templateMarkup
};