﻿import ko from "knockout";
import templateMarkup from "text!./dashboard.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../app/frameworks/core/utility";
import WebRequestShipment from "../../../../app/frameworks/data/apitrackingcore/webRequestShipment";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class ShipmentDashboardScreen extends ScreenBase {

    /**
     * Creates an instance of ShipmentDashboardScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Transportation_Mngm_Dashboard_Shipment_Dashboard")() + " (" + this.i18n('Common_CurrentDay')() + ")"); 
        this.donutCharts = ko.observable();
        this.bladeSize = BladeSize.XLarge_A1;
        this.tab2Visible = ko.observable(true);
        this.selectedIndex = ko.observable(0);
        //this.bladeIsMaximized = true;
        this.txtRefreshTime = ko.observable();
        this.txtRefresh = ko.pureComputed(() => {
            return (
                
                this.i18n("Vehicle_MA_LastSync")() +" " +this.txtRefreshTime() 
                
            );
        });
        this.bladeIsMaximized = true;

        this.isToday = ko.observable(0);
        this.isHistorical = ko.observable(1);
        this.isStateToday = ko.observable(true);

        this.timeOutHandler = this.ensureNonObservable(null);
        this.isAutoRefresh = ko.observable(true);

        this.txtshipmentOnTime = ko.observable("");
        this.txtpercentShipmentOnTime = ko.observable("");

        this.txtshipmentLate = ko.observable("");
        this.txtpercentShipmentLate = ko.observable("");

        this.txtToDayWating = ko.observable("");
        this.txtPercentToDayWating = ko.observable("");
        this.txtToDayInProgress = ko.observable("");
        this.txtPercentToDayInProgress = ko.observable("");
        this.txtToDayFinished = ko.observable("");
        this.txtPercentToDayFinished = ko.observable("");
        this.txtToDayCancelled = ko.observable("");
        this.txtPercentToDayCancelled = ko.observable("");

        this.txtdelayedDelivery = ko.observable("");
        this.txtlateStart = ko.observable("");
        this.txtVehicleCategoryName = ko.observable("");
        this.txtcolorUnstart = ko.observable("#E53935");
        this.txtcolorStarted = ko.observable("#FF8F00");
        this.txtcolorTodayWaiting = ko.observable("");
        this.txtcolorTodayInprogress = ko.observable("");
        this.txtcolorTodayFinished = ko.observable("");
        this.txtcolorTodayCancelled = ko.observable("");
        this.arrayVehicleCategoryList = ko.observableArray([]);
        this.txtTitle = ko.observable("");
        this.txtDateFrom = ko.observable("");
        this.arrayAlerts = ko.observableArray([]);
        this.txtOnTime = ko.observable("(" + this.i18n("Transportation_Mngm_Dashboard_On_Time")() + ")")

        this.txtFromDate = ko.observable("");
        this.txtToDate = ko.observable("");
        this.textLatest = ko.observable("");


        this.colorShimmentOnTime = ko.observable("#73e600");
        this.colorShimmentLate = ko.observable("#E53935");
        this.divAlerts = ko.observable("");
        this.divMTD = ko.observable();
        this.Test = ko.observable(true);
        /*Historical*/
        this.txtDateFromHistorical = ko.observable(this.setFormatDisplayDateTime(this.setDefaultDate('start')));
        this.txtDateToHistorical = ko.observable(this.setFormatDisplayDateTime(this.setDefaultDate('end')));
        this.txtHistoricalPercentShipmentOnTime = ko.observable();
        this.txtHistoricalShipmentOnTime = ko.observable();
        this.txtHistoricalPercentShipmentLate = ko.observable();
        this.txtHistoricalShipmentLate = ko.observable();
        this.txtHistoricalPercentShipmentCancelled = ko.observable();
        this.txtHistoricalShipmentCancelled = ko.observable();
        this.arrayVehicleCategoryHistoricalList = ko.observableArray([]);
        this.historicalGraphData = ko.observable();

        this.propertygraph = ko.observable();

        this.todayFilter = ko.observable({})
        this.historicalFilter = ko.observable({
            "shipmentFromDate": this.setDefaultDate('start'),
            "shipmentToDate": this.setDefaultDate('end')
        });

        this.subscribeMessage("cw-shipment-dashboard-search", (filter) => {
            switch(this.selectedIndex()) {
                case this.isHistorical(): 
                    this.searchHistorical(filter)
                    break;
                case this.isToday(): 
                    this.searchToday(filter)
                    break;
                default :
                    break;
            }            
        });
        
        this.selectedIndex.subscribe((index)=> {
            this.publishMessage("cw-shipment-dashboard-search-swap-content", index);
        })

    }

    pieCallback(data) {
        var statusVehicle = data.dataItem.dataContext.statusId;
        var searchFilter = this.todayFilter()
        var vehicleFilter = {};
        vehicleFilter.businessUnitIds = searchFilter["businessUnitIds"]
        vehicleFilter.includeSubBU = searchFilter["includeSubBU"]
        searchFilter.shipmentFromDate = searchFilter["shipmentFromDate"]
        searchFilter.shipmentToDate = searchFilter["shipmentToDate"]
        vehicleFilter.vehicleCategoryId = searchFilter["vehicleCategoryId"] == 0 ? null : searchFilter["vehicleCategoryId"]
        vehicleFilter.shipmentStatusIds = [statusVehicle];
        vehicleFilter.regionIds = searchFilter.regionIds;
        this.navigate("cw-shipment-list", { status: vehicleFilter });
    }

    onToggleTab() {
        this.tab2Visible(!this.tab2Visible());

    }

    setDefaultDate(info) {
        let objDate = new Date();
        switch(info) {
            case 'start':
                if(objDate.getDate() == 1) {
                    objDate = Utility.minusMonths(objDate, 1)
                }
                objDate.setDate(1);
                objDate.setHours(0);
                objDate.setMinutes(0);
                objDate.setSeconds(0);
                break;
            case 'end' :
                objDate = Utility.minusDays(new Date(), 1);
                objDate.setHours(23);
                objDate.setMinutes(59);
                objDate.setSeconds(59);
                break;
            default :
                break;
        }

        return objDate
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items
        if (!isFirstLoad) {
            return;
        }
        this.getDataDashboardToday();
        this.getDataDashboardHistorical();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { 
        this.isAutoRefresh(false);
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdFilter",this.i18n("Common_Search")(),"svg-cmd-search"));
        commands.push(this.createCommand("cmdExport",this.i18n("Common_Export")(),"svg-cmd-export"));  
        commands.push(this.createCommand("cmdRefresh",this.i18n("Common_Refresh")(),"svg-cmd-refresh"));

        this.selectedIndex.subscribe(() => {  
            commands.splice(0, commands()['length']);

            switch(this.selectedIndex()) {
                case this.isHistorical(): 
                    this.isAutoRefresh(false);
                    this.bladeTitle(this.i18n("Transportation_Mngm_Dashboard_Shipment_Dashboard")() + " (" + this.i18n('Transportation_Mngm_Dashboard_Historical')() + ")");  
                    this.getDataDashboardHistorical();
                    this.isStateToday(false);
                    commands.push(this.createCommand("cmdFilter",this.i18n("Common_Search")(),"svg-cmd-search"));
                    commands.push(this.createCommand("cmdExport",this.i18n("Common_Export")(),"svg-cmd-export"));   
                    break;
                case this.isToday(): 
                    this.isAutoRefresh(true);
                    this.bladeTitle(this.i18n("Transportation_Mngm_Dashboard_Shipment_Dashboard")() + " (" + this.i18n('Common_CurrentDay')() + ")");                      
                    this.getDataDashboardToday();
                    this.isStateToday(true);
                    commands.push(this.createCommand("cmdFilter",this.i18n("Common_Search")(),"svg-cmd-search"));
                    commands.push(this.createCommand("cmdExport",this.i18n("Common_Export")(),"svg-cmd-export"));  
                    commands.push(this.createCommand("cmdRefresh",this.i18n("Common_Refresh")(),"svg-cmd-refresh"));
                    break;
                default :
                    break;
            }
        });
    }
    vehicleClick(statusVehicle,id) {
        let vehicleFilter = {};
        let searchFilter = null;
        vehicleFilter.vehicleCategoryId = id
        if(this.selectedIndex() == this.isHistorical()) {
            searchFilter = this.historicalFilter()
            vehicleFilter.isHistorical = true
            vehicleFilter.dateFrom = searchFilter["shipmentFromDate"]
            vehicleFilter.dateTo = searchFilter["shipmentToDate"]
            vehicleFilter.businessUnitIds = searchFilter["businessUnitIds"]
            vehicleFilter.includeSubBU = searchFilter["includeSubBU"]
        } else {
            searchFilter = this.todayFilter()
            vehicleFilter.businessUnitIds = searchFilter["businessUnitIds"]
            vehicleFilter.includeSubBU = searchFilter["includeSubBU"]
            searchFilter.shipmentFromDate = searchFilter["shipmentFromDate"]
            searchFilter.shipmentToDate = searchFilter["shipmentToDate"]
            vehicleFilter.isHistorical = false
        }
        vehicleFilter.regionIds = searchFilter.regionIds;

        switch (statusVehicle) {
            case 'Waiting':
                vehicleFilter.shipmentStatusIds = ['2'];
                this.navigate("cw-shipment-list", { status: vehicleFilter });
                break;
            case 'Inprogress':
                vehicleFilter.shipmentStatusIds = ['3', '4', '5', '6', '7','13'];
                this.navigate("cw-shipment-list", { status: vehicleFilter });
                break;
            case 'On Time':
                vehicleFilter.shipmentStatusIds = [ '8', '10'];
                this.navigate("cw-shipment-list", { status: vehicleFilter });
                break;
            case 'Late':
                vehicleFilter.shipmentStatusIds = ['9','11'];
                this.navigate("cw-shipment-list", { status: vehicleFilter });
                break;
            default:
                //alert(statusVehicle);
                break;
        }
    }

    onClickStatistics(result) {
        var searchFilter = this.todayFilter()
        var vehicleFilter = {};
        vehicleFilter.businessUnitIds = searchFilter["businessUnitIds"]
        vehicleFilter.includeSubBU = searchFilter["includeSubBU"]
        searchFilter.shipmentFromDate = searchFilter["shipmentFromDate"]
        searchFilter.shipmentToDate = searchFilter["shipmentToDate"]
        vehicleFilter.vehicleCategoryId = searchFilter["vehicleCategoryId"] == 0 ? null : searchFilter["vehicleCategoryId"]
        vehicleFilter.regionIds = searchFilter.regionIds;

        switch (result) {            
            case 'unstart':
                //vehicleFilter.shipmentStatusIds = ['1', '2']  filter in Service  
                vehicleFilter.isLateUnStart = true                
                break;
            case 'started':
                //vehicleFilter.shipmentStatusIds = ['3']  filter in Service 
                vehicleFilter.isLateStart = true   
                break;
            case 'Waiting':
                vehicleFilter.shipmentStatusIds = ['2']
                break;
            case 'inprogress':
                vehicleFilter.shipmentStatusIds = ['3', '4', '5', '6', '7','13']
                break;
            case 'finished':
                vehicleFilter.shipmentStatusIds = ['8', '9', '10', '11']
                break;
            case 'Cancelled':
                vehicleFilter.shipmentStatusIds = ['12']
                break;
            default:
        }
        this.navigate("cw-shipment-list", { status: vehicleFilter});
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id){
            case "cmdFilter":
                this.navigate("cw-shipment-dashboard-search", { isState: this.selectedIndex() });
                break;
            case "cmdRefresh":
                this.setAutoRefreshTime();
                break;
            case "cmdExport":
                this.exportShipment();
                break;
            default :
                break;
        }
    }

    searchToday(filter) {
        let startDate = new Date()
        let endDate = new Date()

        this.todayFilter(filter)
        this.todayFilter()['shipmentFromDate'] = this.setFormatDateTime(startDate, "00:00");
        this.todayFilter()['shipmentToDate'] = this.setFormatDateTime(endDate, "23:59");
        this.getDataDashboardToday();
    }

    searchHistorical(filter) {
        let startDate = new Date(filter['shipmentFromDate'])
        let endDate = new Date(filter['shipmentToDate'])
        this.txtDateFromHistorical(this.setFormatDisplayDateTime(startDate));
        this.txtDateToHistorical(this.setFormatDisplayDateTime(endDate));

        this.historicalFilter(filter)
        this.historicalFilter()['shipmentFromDate'] = this.setFormatDateTime(startDate, "00:00");
        this.historicalFilter()['shipmentToDate'] = this.setFormatDateTime(endDate, "23:59");
        this.getDataDashboardHistorical();
    }

    getDataDashboardToday() {
        var dfd = $.Deferred();
        var propertygraph = [{
            // labelsEnabled: false,
            autoMargins: false,
            marginTop: 0,
            marginBottom: 100,
            marginLeft: 0,
            marginRight: 0
        }]
        this.propertygraph(propertygraph)
        var userSummary = this.webRequestShipment.getDashboardToday(this.todayFilter());
        $.when(userSummary).done((res) => {
            this.txtshipmentOnTime(res.shipmentOnTime + "/" + res.todayStatistics.formatShipmentFinished);
            this.txtpercentShipmentOnTime("(" + res.percentShipmentOnTime +"%)");
            this.txtshipmentLate(res.shipmentLate + "/" + res.todayStatistics.formatShipmentFinished);
            this.txtpercentShipmentLate("(" + res.percentShipmentLate + "%)");

            this.txtToDayWating(res.todayStatistics.formatShipmentWating + "/" + res.formatAllTodayShipment);
            this.txtPercentToDayWating("(" +res.todayStatistics.percentShipmentWating + " %)");
            this.txtToDayInProgress(res.todayStatistics.formatShipmentInProgress + "/" + res.formatAllTodayShipment);
            this.txtPercentToDayInProgress("(" +res.todayStatistics.percentShipmentInProgress + " %)");
            this.txtToDayFinished(res.todayStatistics.formatShipmentFinished + "/" + res.formatAllTodayShipment);
            this.txtPercentToDayFinished("(" +res.todayStatistics.percentShipmentFinished + " %)");
            this.txtToDayCancelled(res.todayStatistics.formatShipmentCancelled + "/" + res.formatAllTodayShipment);
            this.txtPercentToDayCancelled("("+res.todayStatistics.percentShipmentCancelled +" %)");

            this.txtcolorTodayWaiting(res.colorInfos[0].todayWaitingColor);
            this.txtcolorTodayInprogress(res.colorInfos[0].todayInprogressColor);
            this.txtcolorTodayFinished(res.colorInfos[0].todayFinishedColor);
            this.txtcolorTodayCancelled(res.colorInfos[0].todayCancelledColor);

            
            //this.txtVehicleCategoryName(res.vehicleCategoryList.vehicleCategoryName);
            this.txtRefreshTime(res.formatLastSyncDate);
            res.donutGraphInfos['total'] = res.formatAllTodayShipment;
            this.donutCharts(res.donutGraphInfos);

            this.arrayVehicleCategoryList(this.formatVehicle(res.vehicleCategoryList));

            if (res.isShowMTD) {
                this.arrayAlerts(this.formatMTD(res.mtdList));
                this.txtTitle("MTD");
                this.txtDateFrom(this.i18n('Transportation_Mngm_Dashboard_Date_From')() + "   " 
                    + this.setFormatDisplayDateTime(this.setDefaultDate('start')) + "   " 
                    + this.i18n('Transportation_Mngm_Dashboard_Date_To')() + "   " 
                    +  this.setFormatDisplayDateTime(this.setDefaultDate('end')));
            } else {
                this.arrayAlerts(this.formatAlerts(res.alertsList));
                this.txtTitle(this.i18n('Common_Alerts')());
                this.textLatest(this.i18n('Transportation_Mngm_Dashboard_Alerts_Records')());
            }

            this.txtdelayedDelivery(res.currentIssuesInfo.formatDelayedDelivery);
            this.txtlateStart(res.currentIssuesInfo.formatLateStart);
            
            this.onAutoRefresh(true);
            dfd.resolve();
        })

        return dfd;
    }

    getDataDashboardHistorical() {
        this.isBusy(true);
        let dfd = $.Deferred();
        let filter = this.historicalFilter();
        let userSummary = this.webRequestShipment.getDashboardHistorical(filter);
        $.when(userSummary).done((res) => {
            this.setFormatDataHistorical(res);
            this.isBusy(false);            
            this.onAutoRefresh(true);            
            dfd.resolve();
        })
        return dfd;
    }

    setFormatDataHistorical(data) {
        this.txtHistoricalPercentShipmentOnTime("(" + data.percentShipmentOnTime + " %)");
        this.txtHistoricalShipmentOnTime(data.formatShipmentOnTime + "/" + data.todayStatistics.formatShipmentFinished);

        this.txtHistoricalPercentShipmentLate("(" + data.percentShipmentLate + " %)");
        this.txtHistoricalShipmentLate(data.formatShipmentLate + "/" + data.todayStatistics.formatShipmentFinished);

        this.txtHistoricalPercentShipmentCancelled("(" + data.percentShipmentCancel + " %)");
        this.txtHistoricalShipmentCancelled(data.formatShipmentCancel + "/" + data.formatAllHistoryShipment);

        this.arrayVehicleCategoryHistoricalList(this.formatVehicle(data.vehicleCategoryList));

        this.historicalGraphData({
            graphConfig: data.lineGraphConfigs,
            graphData: data.lineGraphInfos,
        });
    }

    formatVehicle(response) {
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i];
            dataItem.formatVehicle = dataItem.formatShipmentOnTime + "/" + dataItem.formatAllFinishVehicle;
            dataItem.percentShipmentWating = "(" + dataItem.percentShipmentWating +" %)";
            dataItem.percentShipmentInProgress = "(" + dataItem.percentShipmentInProgress +" %)";
            dataItem.percentVehicleOnTime = "(" + dataItem.percenFinishVehicle +" %)";
            dataItem.percentShipmentOnTime = "(" + dataItem.percentShipmentOnTime + " %)";
            dataItem.percentShipmentLate = "(" + dataItem.percentShipmentLate + " %)";

            dataItem.txtWaiting = this.i18n('Transportation_Mngm_Dashboard_Title_Waiting')();
            dataItem.txtInprogress = this.i18n('Transportation_Mngm_Dashboard_Title_Inprogress')();
            dataItem.txtOnTime = this.i18n('Transportation_Mngm_Dashboard_On_Time')();
            dataItem.txtLate = this.i18n('Transportation_Mngm_Dashboard_Late')();

            // this.colorShimmentOnTime(dataItem.onTimeColor);
            // this.colorShimmentLate(dataItem.lateColor);

            data.push(dataItem);

        }
        return data;
    }

    formatAlerts(response) {
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i];
            dataItem.column1 = "<div class='box_MTDAndAlerts'><div>" + dataItem.formatAlertDateTime + "</div>   <div>" + dataItem.shipmentNo + "</div>     <div>" + dataItem.shipmentStatusDisplayName + "</div></div>";
            data.push(dataItem);
        }
        return data;
    }
    formatMTD(response) {
        var data = [];
        for (let i = 0; i < response.length; i++) {
            var dataItem = response[i];
            dataItem.column1 = "<div class='box_MTDAndAlerts'><div style='width: 150px;'>" + dataItem.vehicleCategoryName + "</div>   <div>" + dataItem.percenFinishVehicle + "%</div>     <div>" + dataItem.formatAllFinishVehicle +"</div></div>";
            data.push(dataItem);
        }
        return data;
    }

    setFormatDateTime(dateObj, timeObj, defaultTime) {

        var newDateTime = "";

        if (dateObj && timeObj) {

            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date(dateObj);
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + timeObj;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = timeObj + ":00";
                newDateTime = newDate + newTime;
            }

        }
        else if (dateObj && defaultTime) {
            if (typeof (dateObj.getMonth) === 'function') {

                let d = new Date();
                let year = d.getFullYear().toString();
                let month = (d.getMonth() + 1).toString();
                let day = d.getDate().toString();

                month = month.length > 1 ? month : "0" + month;
                day = day.length > 1 ? day : "0" + day;
                newDateTime = year + "-" + month + "-" + day + "T" + defaultTime;
            } else {
                let newDate = dateObj.split("T")[0] + "T";
                let newTime = dateObj.split("T")[1] = defaultTime;
                newDateTime = newDate + newTime;
            }
        }
        else
        { }
        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }

    onAutoRefresh(reCall) {
        if (reCall) {
            clearTimeout(this.timeOutHandler);
            this.onAutoRefresh();
            return;
        }

        if (this.isAutoRefresh()) {
            this.timeOutHandler = setTimeout(() => {
                this.setAutoRefreshTime();
                this.onAutoRefresh();
            }, WebConfig.appSettings.shipmentAutoRefreshTime);
        } else {
            clearTimeout(this.timeOutHandler);
        }
    }
    
    setAutoRefreshTime() {
        switch (this.selectedIndex()) {
            case this.isHistorical():
                this.getDataDashboardHistorical();
                break;
            case this.isToday():
                this.getDataDashboardToday();
                break;
            default:
                break;
        }
    }

    exportShipment() {
        this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
            switch (button) {
                case BladeDialog.BUTTON_YES:
                    if (this.selectedIndex() == this.isHistorical()) {
                        var dfdExportDashboard = this.webRequestShipment.getDashboardExport(
                            this.historicalFilter(),
                            this.historicalFilter().selectedTab = "h"
                        );
                        this.isBusy(true);
                        $.when(dfdExportDashboard).done(response => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        });
                    } else {
                        var dfdExportDashboard = this.webRequestShipment.getDashboardExport(
                            this.todayFilter(),
                            this.todayFilter().selectedTab = "t");
                        this.isBusy(true);
                        $.when(dfdExportDashboard).done(response => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        });
                    }
                    break;
            }
        });
    }

    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
}


export default {
viewModel: ScreenBase.createFactory(ShipmentDashboardScreen),
    template: templateMarkup
};