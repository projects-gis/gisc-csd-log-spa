import ko from "knockout";
import templateMarkup from "text!./form.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import "jquery-ui";
import "jqueryui-timepicker-addon";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";
import Utility from "../../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
import WebRequestShipment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestShipment";
import WebRequestShipmentCategory from "../../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentCategory"
import {
    Constants,
    Enums
} from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestShipmentZone from "../../../../../../app/frameworks/data/apitrackingcore/webRequestShipmentZone";

class BOMsReportsSummaryPassengerByRouteScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeSize = BladeSize.Small;
        this.bladeTitle(this.i18n('Report_Filter')());

        this.plant = ko.observableArray([]);
        this.selectedPlant = ko.observable();
        this.zone = ko.observableArray([]);
        this.selectedZone = ko.observable();
        this.line = ko.observableArray([]);
        this.selectedLine = ko.observable();
        this.travelMode = ko.observableArray([]);
        this.selectedTravelMode = ko.observable();
        this.shiftMode = ko.observableArray([]);
        this.selectedShiftMode = ko.observable();

        this.valueRadio = ko.observable('Yesterday');
        this.isDateTimeEnabled = ko.observable(false);
        this.previewRadio = ko.observable('Bus');

        //datetime
        this.formatSet = "dd/MM/yyyy";
        var addDay = 30;
        var addMonth = 3;
        var setDate = Utility.addDays(new Date(), -1);
        this.start = ko.observable(setDate);
        this.end = ko.observable(setDate);
        this.timeStart = ko.observable("00:00");
        this.timeEnd = ko.observable("23:59");
        this.maxDateStart = ko.observable(setDate);
        this.minDateEnd = ko.observable(setDate);

        this.minDateShipment = ko.observable(Utility.minusMonths(new Date(),addMonth));
        this.maxDateShipment = ko.observable(Utility.addMonths(new Date(),addMonth));


        this.periodDay = ko.pureComputed((e)=>{
            // can't select date more than current date
            var isDate = Utility.addDays(this.start(),addDay);
            let currentDate = parseInt(setDate / (1000 * 60 * 60 * 24));
            let endDate = parseInt(isDate / (1000 * 60 * 60 * 24)) + 1;
            var calDay = endDate - currentDate;
            if(calDay > 0){
                isDate = setDate;
            }
     
            // set date when clear start date and end date
            if(this.start() === null){
                // set end date when clear start date
                if(this.end() === null){
                    
                    isDate = new Date();
                   
                }else{
                 
                    isDate = this.end();
              
                }
            
                this.maxDateStart(isDate);
                this.minDateEnd(isDate);
          
            }else{
                
                this.minDateEnd(this.start())
         
            }

            return isDate;
        });

        // Date Time Criteria
        this.valueRadio.subscribe((res) => {
            let currentDate = new Date();
            var startDate = new Date();
            var endDate = new Date();
            switch (res) {
                case 'Today':
                    this.isDateTimeEnabled(false);
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;

                case 'Yesterday':
                    this.isDateTimeEnabled(false);
    
                    startDate.setDate(startDate.getDate() - 1)
                    // console.log("startDate.setDate(startDate.getDate() - 1)",startDate)
                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: startDate,
                        end: true
                    }))
                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(startDate));

                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    
                    break;
            
                case 'ThisWeek':
                    this.isDateTimeEnabled(false);
                    // let startWeek = parseInt(startDate.getDate() - (startDate.getDay() - 1));
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    let startWeek;
                    // let endWeek = parseInt(endDate.getDate() + (7 - endDate.getDay()));

                    //setStartDate
                    if(currentDate.getDay() == 0){ //ถ้าเป็นวันอาทิตย์ ( 0 คือวันอาทิตย์ )
              
                        startWeek = parseInt((startDate.getDate() - 6));

                    }else if(currentDate.getDay() == 1) { //วันจันทร์
                 
                        startWeek = parseInt((startDate.getDate() ));

                    }
                    else{
                    
                        startWeek = parseInt((startDate.getDate() - (startDate.getDay()-1)));
                    }
                  
                    startDate.setDate(startWeek);

                    //setEndDate
                    if(currentDate.getDay() == 1){ //ถ้าเป็นวันจันทร์ ( 1 คือวันจันทร์ )
                        endDate.setDate(currentDate.getDate());
                        this.maxDateStart(new Date(),0);
                    }

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }));

                    endDate = Utility.addDays(endDate, -1);
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;

                case 'LastWeek':

                    this.isDateTimeEnabled(false);
                    let LastendWeek = parseInt((startDate.getDate() - (startDate.getDay() - 1)));
                    let LaststartWeek = parseInt(LastendWeek - 7);

                    startDate.setDate(LaststartWeek);
                    endDate.setDate(LastendWeek - 1);

                    this.start(this.setFormatDateTime({
                        data: startDate,
                        start: true
                    }));
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));

                    $("#from").val(this.setFormatDisplayDateTime(startDate));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));

                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;
                
                case 'ThisMonth':
                    this.isDateTimeEnabled(false);
                    let firstDay = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                    let lastDay = new Date(endDate.getFullYear(), endDate.getMonth() + 1, 0);

                    this.start(this.setFormatDateTime({
                        data: firstDay,
                        start: true
                    }));

                    endDate = Utility.addDays(endDate, -1);
                    this.end(this.setFormatDateTime({
                        data: endDate,
                        end: true
                    }));

                    $("#from").val(this.setFormatDisplayDateTime(firstDay));
                    $("#to").val(this.setFormatDisplayDateTime(endDate));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    // $("#searchStartDate").val(this.setFormatDisplayDateTime(firstDay));
                    // $("#searchEndDate").val(this.setFormatDisplayDateTime(lastDay));
                    break;
                case 'LastMonth':
                    this.isDateTimeEnabled(false);
                    let LastfirstDay = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                    let LastlastDay = new Date(endDate.getFullYear(), endDate.getMonth(), 0);

                    this.start(this.setFormatDateTime({
                        data: LastfirstDay,
                        start: true
                    }))
                    this.end(this.setFormatDateTime({
                        data: LastlastDay,
                        end: true
                    }))

                    $("#from").val(this.setFormatDisplayDateTime(LastfirstDay));
                    $("#to").val(this.setFormatDisplayDateTime(LastlastDay));
                    this.timeStart("00:00");
                    this.timeEnd("23:59");

                    break;
                case 'Custom':
                    this.isDateTimeEnabled(true);
                    this.timeStart("00:00");
                    this.timeEnd("23:59");
                    break;

                case '':
                    this.isRadioSelectedStartDate(false);
                    this.isRadioSelectedEndDate(false);
                    break;
            }
        });

        this.thisWeekVisible = ko.pureComputed(() => {
            let currentDate = new Date();
            let isMonday = true;
            if(currentDate.getDay() == 1){ //Monday
                isMonday = false;
            }
            return isMonday;
        });

        this.thisMonthVisible = ko.pureComputed(() => {
            let currentDate = new Date();
            let firstDay = true;
            if(currentDate.getDate() == 1){ //first date of month
                firstDay = false;
            }
            return firstDay;
        });

        this.selectedPlant.subscribe((ids)=>{
            if(_.size(ids)){
                this.isBusy(true);
                this.webRequestShipmentCategory.listShipmentCategory({
                    businessUnitIds: ids,
                    enable: true
                }).done((res)=>{
                    this.isBusy(false);
                    var i = 1;
                    _.forEach(res.items, (item)=>{
                        item.parent = 0;
                    });
                    res.items.unshift({id: 0, name: this.i18n("Common_CheckAll")(), parent: "#"});
                    this.shiftMode(res.items);
                });
            }else{
                this.shiftMode([]);
            }
        });

        this.selectedZone.subscribe((data)=>{
            let filter = {
                regionIds: data
            }
            this.line([]);
            this.isBusy(true);
            this.webRequestShipment.listShipmentTemplateSummary(filter).done((res)=>{
                this.line(res.items);
                this.isBusy(false);
            }).fail((e)=>{
                this.handleError(e);
                this.isBusy(false);
            });

        });
    }

    setupExtend() {

        this.start.extend({ required: true });
        this.end.extend({ required: true });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            start: this.start,
            end: this.end,
            timeStart:this.timeStart,
            timeEnd:this.timeEnd,
        });
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * Get WebRequest specific for BusinessUnit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Shipment module in Web API access.
     * @readonly
     */
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }

    /**
     * Get WebRequest specific for Shipment Category module in Web API access.
     * @readonly
     */
    get webRequestShipmentCategory() {
        return WebRequestShipmentCategory.getInstance();
    }

    /**
     * Get WebRequest specific for Shipment Zone module in Web API access.
     * @readonly
     */
    get webRequestShipmentZone() {
        return WebRequestShipmentZone.getInstance();
    }


    setFormatDateTime(dateObj) {

        var newDateTime = "";

        let myDate = new Date(dateObj.data);
        let year = myDate.getFullYear().toString();
        let month = (myDate.getMonth() + 1).toString();
        let day = myDate.getDate().toString();
        let hour = myDate.getHours().toString();
        let minute = myDate.getMinutes().toString();
        let sec = "00";

        if (dateObj.start) {
            hour = "00";
            minute = "00";
        } else if (dateObj.end) {
            hour = "23";
            minute = "59";
            sec = "59";
        } else { }

        month = month.length > 1 ? month : "0" + month;
        day = day.length > 1 ? day : "0" + day;
        hour = hour.length > 1 ? hour : "0" + hour;
        minute = minute.length > 1 ? minute : "0" + minute;
        newDateTime = year + "-" + month + "-" + day + "T" + hour + ":" + minute + ":" + sec;

       

        return newDateTime;
    }

    setFormatDisplayDateTime(objDate) {
        let displayDate = "";
        let d = objDate;
        let year = d.getFullYear().toString();
        let month = (d.getMonth() + 1).toString();
        let day = d.getDate().toString();
        day = day.length > 1 ? day : "0" + day;
        displayDate = day + "/" + month + "/" + year;

        return displayDate
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        
        var dfd = $.Deferred();
        let businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }
        
        let travelModeFilter = { 
            companyId: WebConfig.userSession.currentCompanyId, 
            types: Enums.ModelData.EnumResourceType.TravelMode,
            sortingColumns: DefaultSorting.EnumResource
        }
        
        let d1 = this.webRequestBusinessUnit.listBusinessUnitParentSummary(businessUnitFilter);
        let d2 = this.webRequestShipmentZone.listShipmentZone({enable: true});
        let d3 = this.webRequestShipment.listShipmentTemplateSummary();
        let d4 = this.webRequestEnumResource.listEnumResource(travelModeFilter);
        $.when(d1, d2, d3, d4).done((r1, r2, r3, r4) => {
            this.plant(r1.items);
            this.zone(r2.items);
            this.line(r3.items);
            this.travelMode(r4.items);
            dfd.resolve();
        });

        return dfd;

    }

    buildActionBar(actions){
        actions.push(this.createAction("actPreview", this.i18n('Report_Preview')()));
    }

    onActionClick(sender) {
        if(!this.validationModel.isValid()){
            this.validationModel.errors.showAllMessages();
            return false;
        }
        if(sender.id === "actPreview") {



            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.start())) + " " + "00:00";
            var formatEDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.end())) + " " + "00:00";
            var BUIds = (_.size(this.selectedPlant())) ? JSON.stringify(this.selectedPlant()) : 0;
            var zoneId = (_.size(this.selectedZone())) ? JSON.stringify(this.selectedZone()) : 0;
            var lineId = (_.size(this.selectedLine())) ? this.selectedLine().id : 0;
            var travelModeId = (_.size(this.selectedTravelMode())) ? this.selectedTravelMode().value : 0;
            var shiftModeId = (_.size(this.selectedShiftMode())) ? JSON.stringify(this.selectedShiftMode()) : 0;
            var previewBy = (this.previewRadio() == "Bus") ? "0" : "1";
            var shiftName = new Array();
            _.forEach(this.selectedShiftMode(), (id)=>{
                let shift = _.find(this.shiftMode(), (item)=>{
                    return item.id == id;
                });
                if(_.size(shift)){
                    shiftName.push(shift.name);
                }
            });

            let reports = {
                report:"", 
                parameters: {
                    UserID: WebConfig.userSession.id,
                    CompanyID: WebConfig.userSession.currentCompanyId,
                    BuIds: BUIds,
                    ZoneIds: zoneId,
                    LineIds: lineId,
                    TravelMode: travelModeId,
                    ShiftIds: shiftModeId,
                    ShiftName: _.toString(shiftName),
                    ViewBy: previewBy,
                    SDate: formatSDate,
                    EDate: formatEDate,
                    PrintBy: WebConfig.userSession.fullname,
                    Language: WebConfig.userSession.currentUserLanguage
                }
            };

            var reportExport = { PDF: false, Word: false, CSV: false };

            if(this.previewRadio() == "Bus"){
                reports.report = "Gisc.Csd.Service.Report.Library.BOMs.SummaryPassengerByRouteReport_Bus, Gisc.Csd.Service.Report.Library";
            }else{
                reports.report = "Gisc.Csd.Service.Report.Library.BOMs.SummaryPassengerByRouteReport_Droppoint, Gisc.Csd.Service.Report.Library";
            }

            this.navigate("cw-report-reportviewer", {
                reportSource: reports, 
                reportName: this.i18n("BOMs_Reports_Summary_Passenger_By_Route")(),
                reportExport: reportExport
            });


        } 
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsReportsSummaryPassengerByRouteScreen),
    template: templateMarkup
};