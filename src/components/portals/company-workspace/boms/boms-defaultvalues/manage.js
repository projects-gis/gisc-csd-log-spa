﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestBOMsDefaultValues from "../../../../../app/frameworks/data/apitrackingcore/WebRequestBOMsDefaultValues";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestShipment from "../../../../../app/frameworks/data/apitrackingcore/WebRequestShipment";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";


/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsDefaultValuesList extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeSize = BladeSize.Small;
        this.id = this.ensureNonObservable(params.id, 0);
        this.mode = this.ensureNonObservable(params.mode, 0);

        switch (params.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("BOMs_DefaultValue_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("BOMs_DefaultValue_Update")());
                break;
        }

        //var mockDataJobCategory = [{ id: 1, name: 'JobCate1' }, { id: 2, name: 'JobCate2' }, { id: 3, name: 'JobCate3' }];

        this.name = ko.observable("");
        this.description = ko.observable("");
        this.businessUnitOptions = ko.observableArray([]);
        this.selectedBusinessUnitId = ko.observable();
        this.travelModeOptions = ko.observableArray([]);
        this.selectedTravelMode = ko.observable();
        this.shipmentCateOptions = ko.observableArray([]);
        this.selectedShipmentCate = ko.observable();
        this.late = ko.observable();
        this.maybeLate = ko.observable();

        
    }

    /**
     * Get WebRequest BOMS
     * @readonly
     */
    get webRequestBOMsDefaultValues() {
        return WebRequestBOMsDefaultValues.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    get webRequestShipment() {
        return WebRequestShipment.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var d1 = this.webRequestBusinessUnit.listBusinessUnitParentSummary({ companyId: WebConfig.userSession.currentCompanyId });
        var d2 = this.webRequestEnumResource.listEnumResource({
            types: [Enums.ModelData.EnumResourceType.TravelMode],
            sortingColumns: DefaultSorting.EnumResource
        });
        var d3 = this.webRequestBOMsDefaultValues.getBOMsDefaultValues(this.id);
        var d4 = this.webRequestShipment.shipmentCategorySummary({enable: true});

        //var d3 = this.webRequestShipment.shipmentCategorySummary();

        $.when(d1, d2,d3,d4).done((r1, r2,res,r4) => {
            let buList = $.grep(r1.items, function (n, i) {
                return n.id > 0;
            });

            this.businessUnitOptions(buList);
            this.travelModeOptions(r2.items);
            
            //if (this.mode == Screen.SCREEN_MODE_UPDATE) {

            //}
            //else {
            //    dfd.resolve();
            //}

            if (res) {
                this.name(res.name);
                this.description(res.description);
                this.late(res.late);
                this.maybeLate(res.mayBeLate);
                this.shipmentCateOptions(r4.items);

                let currBuId = ScreenHelper.findOptionByProperty(this.businessUnitOptions, "id", res.businessUnitId);
                let currTravelMode = ScreenHelper.findOptionByProperty(this.travelModeOptions, "value", res.travelMode);
                let currJobCate = ScreenHelper.findOptionByProperty(this.shipmentCateOptions, "id", res.jobCategoryId);

                this.selectedBusinessUnitId(currBuId);
                this.selectedTravelMode(currTravelMode);
                this.selectedShipmentCate(currJobCate);

                this.selectedBusinessUnitId.subscribe((val) => {
                    if (val) {
                        this.webRequestShipment.shipmentCategorySummary({ BusinessUnitIds: [val.id] }).done((r) => {
                            this.shipmentCateOptions(r.items);
                        });
                    }
                    else {
                        this.shipmentCateOptions([]);
                    }

                });
            }
            else {
                this.selectedBusinessUnitId.subscribe((val) => {
                    if (val) {
                        this.webRequestShipment.shipmentCategorySummary({ BusinessUnitIds: [val.id] }).done((r) => {
                            this.shipmentCateOptions(r.items);
                        });
                    }
                    else {
                        this.shipmentCateOptions([]);
                    }

                });
            }

            dfd.resolve();


        }).fail((e) => {
            dfd.reject(e);
            this.handleError(e);
        });
      
        return dfd;
    }

    setupExtend() {

        this.name.extend({ trackChange: true });
        this.description.extend({ trackChange: true });
        this.selectedBusinessUnitId.extend({ trackChange: true });
        this.selectedTravelMode.extend({ trackChange: true });
        this.selectedShipmentCate.extend({ trackChange: true });
        this.late.extend({ trackChange: true });
        this.maybeLate.extend({ trackChange: true });

        this.name.extend({ required: true});
        this.selectedBusinessUnitId.extend({ required: true });
        this.selectedTravelMode.extend({ required: true });
        this.selectedShipmentCate.extend({ required: true });
        this.late.extend({ required: true });
        this.maybeLate.extend({ required: true });

        this.name.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });



        this.validationModel = ko.validatedObservable({
            Name: this.name,
            Description: this.description,
            SelectedBusinessUnitId: this.selectedBusinessUnitId,
            SelectedTravelMode: this.selectedTravelMode,
            SelectedShipmentCate: this.selectedShipmentCate,
            Late: this.late,
            MaybeLate: this.maybeLate
        });

    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n('Common_Save')()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            var info = this.generateModel();
            this.isBusy(true);

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBOMsDefaultValues.createBOMsDefaultValues(info).done((r) => {
                        this.publishMessage("cw-boms-defaultValue-changed", r.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBOMsDefaultValues.updateBOMsDefaultValues(info).done((r) => {
                        this.publishMessage("cw-boms-defaultValue-changed", r.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
            }

        }
    }

    generateModel() {

        var model = {
            id: this.id,
            name: this.name(),
            description: this.description(),
            businessUnitId: this.selectedTravelMode() ? this.selectedBusinessUnitId().id : 0,
            travelMode: this.selectedTravelMode() ? this.selectedTravelMode().value : 0,
            jobCategoryId: this.selectedShipmentCate() ? this.selectedShipmentCate().id : 0,
            late: this.late(),
            mayBeLate: this.maybeLate()
        };

        return model;
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsDefaultValuesList),
    template: templateMarkup
};