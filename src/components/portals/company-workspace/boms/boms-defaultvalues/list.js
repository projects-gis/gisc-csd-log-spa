﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeMargin from "../../../../../app/frameworks/constant/bladeMargin";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import ScreenHelper from "../../../screenhelper";
import GenerateDataGridExpand from "../../generateDataGridExpand";
import WebRequestBOMsDefaultValues from "../../../../../app/frameworks/data/apitrackingcore/WebRequestBOMsDefaultValues";
import { Enums, Constants } from "../../../../../app/frameworks/constant/apiConstant";


/**
 * Display available subscription menu based on user permission.
 * @class MenuScreen
 * @extends {ScreenBase}
 */
class BOMsDefaultValuesList extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("BOMs_Default_Values_Title")());
        this.bladeSize = BladeSize.Large;

        this.bomsDefaultValuesItem = ko.observableArray([]);
        this.order = ko.observable([[1, "asc"]]);
        this.filterText = ko.observable("");
        this.selectedDefaultValue = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);

        this._selectingRowHandler = (data) => {
            return this.navigate("cw-boms-defaultvalues-view", { id: data.id });
        };


        this.subscribeMessage("cw-boms-defaultValue-changed", id => {
            var dfd = $.Deferred();
            this.webRequestBOMsDefaultValues.listBOMsDefaultValues().done((response) => {
                this.bomsDefaultValuesItem.replaceAll(response.items);
                if (id) {
                    this.recentChangedRowIds.replaceAll([id]);
                }
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
            return dfd;

        });

        this.subscribeMessage("cw-boms-defaultValue-delete", () => {
            this.loadFindSummary();
        });
    }

    /**
     * Get WebRequest BOMS
     * @readonly
     */
    get webRequestBOMsDefaultValues() {
        return WebRequestBOMsDefaultValues.getInstance();
    }

   
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateBomsDefaultValue)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("cw-boms-defaultvalues-manage", { mode: Screen.SCREEN_MODE_CREATE });
                break;
        }
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.loadFindSummary();
        
    }

    loadFindSummary() {
        var dfd = $.Deferred();
        this.webRequestBOMsDefaultValues.listBOMsDefaultValues().done((res) => {
            this.bomsDefaultValuesItem(res.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
}

export default {
    viewModel: ScreenBase.createFactory(BOMsDefaultValuesList),
    template: templateMarkup
};