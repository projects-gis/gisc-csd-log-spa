import ko from "knockout";
import { AssetIcons } from "../../../app/frameworks/constant/svg";
import { Enums } from "../../../app/frameworks/constant/apiConstant";
import { Constants } from "../../../app/frameworks/constant/apiConstant";
import i18nextko from "knockout-i18next";
import Utility from "../../../app/frameworks/core/utility";

class AssetInfo {
    
    /**
     * Get statues of asset to display on asset info screen in map.
     * Display only status that has value, no need to check permission. 
     * Service always return only items that user has permission.
     * 
     * @static
     * @param {any} info
     * @param {any} userSession (WebConfig.userSession pass from caller)
     * @returns
     * 
     */
    static getStatuses (info, userSession) {
        var statuses = [];

        // Verify current info is vehicle or deliveryman.
        var isDeliveryMan = !_.isEmpty(info.username);

        var movementDisplayName = info.movementDisplayName;
        info.formatParkDurationCustom = "";
        if (info.movement == Enums.ModelData.MovementType.Park || info.movement == Enums.ModelData.MovementType.ParkEngineOn) {



            if (info.parkDuration != null ){
                //console.log("info", info);
                //var currentDateTime = new Date();
                //var parkTime = new Date(info.dateTime);
                var calParkTime = "";

                ////currentDateTime.setHours(currentDateTime.getHours() + 5);
                //var diffMin = parseInt(((currentDateTime - parkTime) / 1000) / 60);


                var parkDuration = info.parkDuration;
                var dayInMinutes = 60 * 24;

                var day = Math.floor(info.parkDuration / dayInMinutes);
                if(day > 0){
                    calParkTime += day > 0 ? day : "-";
                    calParkTime += " " + i18nextko.t("Common_Day")();
                }
                

                var hr = Math.floor((info.parkDuration % dayInMinutes) / 60);
                if(hr > 0 ){
                    calParkTime += hr > 0 ? " " + hr : " -";
                    calParkTime += " " + i18nextko.t("Common_Hour_Short")();
    
                }
                
                var minutes = Math.floor((info.parkDuration % dayInMinutes) % 60);
                if(minutes > 0){
                    calParkTime += minutes > 0 ? " " + minutes : " -";
                    calParkTime += " " + i18nextko.t("Common_Minutes_Short")();
                }
                
                info.formatParkDurationCustom = calParkTime;

                //movementDisplayName += " (" + calParkTime + ")";

                //var calParkTime = " (" + info.formatParkDuration + " " + i18nextko.t("Common_Minutes_Short")() + ")";
                //movementDisplayName += calParkTime;
            }

        }

        if (info.idleTime != null) {
            //console.log("info", info);
            //var currentDateTime = new Date();
            //var parkTime = new Date(info.dateTime);
            var calIdleTime = "";

            ////currentDateTime.setHours(currentDateTime.getHours() + 5);
            //var diffMin = parseInt(((currentDateTime - parkTime) / 1000) / 60);


            var idleDuration = info.idleTime;
            var dayInMinutes = 60 * 24;

            var day = Math.floor(info.idleTime / dayInMinutes);
            if(day > 0){
            calIdleTime += day > 0 ? day : "-";
            calIdleTime += " " + i18nextko.t("Common_Day")();
            }

            var hr = Math.floor((info.idleTime % dayInMinutes) / 60);
            if(hr > 0){
            calIdleTime += hr > 0 ? " " + hr : " -";
            calIdleTime += " " + i18nextko.t("Common_Hour_Short")();
            }
            
            var minutes = Math.floor((info.idleTime % dayInMinutes) % 60);
            if(minutes > 0){
            calIdleTime += minutes > 0 ? " " + minutes : " -";
            calIdleTime += " " + i18nextko.t("Common_Minutes_Short")();
            }


            info.formatIdleTimeCustom = calIdleTime;

            if(idleDuration == 0){
                info.formatIdleTimeCustom = "-"
            }
            //movementDisplayName += " (" + calParkTime + ")";

            //var calParkTime = " (" + info.formatParkDuration + " " + i18nextko.t("Common_Minutes_Short")() + ")";
            //movementDisplayName += calParkTime;
        }
        
        //แปลงค่าเวลาที่ส่งมาให้อยู่ในรูป วดป
        if (info.moveDuration != null) {
            
            var calMoveDuration = "";

            var moveDuration = info.moveDuration;
            var dayInMinutes = 60 * 24;

            var day = Math.floor(moveDuration / dayInMinutes);
            if(day > 0){
            calMoveDuration += day > 0 ? day : "-";
            calMoveDuration += " " + i18nextko.t("Common_Day")();
            }

            var hr = Math.floor((moveDuration % dayInMinutes) / 60);
            if(hr > 0){
            calMoveDuration += hr > 0 ? " " + hr : " -";
            calMoveDuration += " " + i18nextko.t("Common_Hour_Short")();
            }
            
            var minutes = Math.floor((moveDuration % dayInMinutes) % 60);
            if(minutes > 0){
            calMoveDuration += minutes > 0 ? " " + minutes : " -";
            calMoveDuration += " " + i18nextko.t("Common_Minutes_Short")();
            }


            info.formatmoveDurationCustom = calMoveDuration;

            if(moveDuration == 0){
                info.formatmoveDurationCustom = "-"
            }
            //movementDisplayName += " (" + calParkTime + ")";

            //var calParkTime = " (" + info.formatParkDuration + " " + i18nextko.t("Common_Minutes_Short")() + ")";
            //movementDisplayName += calParkTime;

        }else{
            info.formatmoveDurationCustom = "-"
        }

    

        //Movement
        statuses.push({
            title: i18nextko.t("Common_Movement")(),
            text: info.movementDisplayName,
            icon: AssetIcons.IconMovementTemplateMarkup,
            cssClass: "icon-movement",
            key : "movementDisplayName"
        });

        //Speed
        statuses.push({
            title: i18nextko.t("Common_Speed")(),
            text: info.speed,
            icon: AssetIcons.IconSpeedTemplateMarkup,
            cssClass: "icon-speed",
            key: "speed"
        });

        //Direction
        statuses.push({
            title: i18nextko.t("Common_Direction")(),
            text: info.directionTypeDisplayName,
            icon: AssetIcons.IconDirectionTemplateMarkup,
            cssClass: "icon-direction",
            key: "directionTypeDisplayName"
        });

        //GPS Status
        statuses.push({
            title: i18nextko.t("Common_GPSStatus")(),
            text: info.gpsStatusDisplayName,
            icon: AssetIcons.IconGpsTemplateMarkup,
            cssClass: "icon-gps",
            key: "gpsStatusDisplayName"
        });

        //GSM Status
        statuses.push({
            title: i18nextko.t("Common_GSMStatus")(),
            text: info.gsmStatusDisplayName,
            icon: AssetIcons.IconGsmTemplateMarkup,
            cssClass: "icon-gsm",
            key: "gsmStatusDisplayName"
        });

        //Altitude (check permission and available for vehicle only)
        if(userSession.hasPermission(Constants.Permission.ViewAltitude)) {
            if(!isDeliveryMan) {
                statuses.push({
                    title: i18nextko.t("Common_Altitude")(),
                    text: info.altitude,
                    icon: AssetIcons.IconAltitudeTemplateMarkup,
                    cssClass: "icon-altitude",
                    key: "altitude"
                });
            }
        }

        //DLT Status (check permission and available for vehicle only)
        if(userSession.hasPermission(Constants.Permission.ViewDLTStatus)) {
            if(!isDeliveryMan) {
                statuses.push({
                    title: i18nextko.t("Common_DLTStatus")(),
                    text: info.dltStatusDisplayName,
                    icon: AssetIcons.IconDltStatusTemplateMarkup,
                    cssClass: "icon-dltstatus",
                    key: "dltStatusDisplayName"
                });
            }
        }

        //Features
        _.forEach(info.trackLocationFeatureValues, (o) => {
            //if featureCode is not null, that's mean this feature is installed.
            if (!_.isNil(o.featureCode)) {
                var title = ""; // tooltip
                var text = o.featureUnitSymbol ? Utility.stringFormat("{0} {1}", o.formatValue, o.featureUnitSymbol) : o.formatValue; // value
                var icon = ""; // svg
                var cssClass = ""; // cssClass
                var key = o.featureCode; //key

                switch (o.featureCode) {
                    case "TrkVwEngine":
                        title = i18nextko.t("Common_Engine")();
                        icon = AssetIcons.IconEngineTemplateMarkup;
                        cssClass = "icon-engine";
                        break;
                    case "TrkVwGate":
                        title = i18nextko.t("Common_Gate1")();
                        icon = AssetIcons.IconGateTemplateMarkup;
                        cssClass = "icon-gate";
                        break;
                    case "TrkVwFuel":
                        title = i18nextko.t("Common_FuelLevel")();
                        icon = AssetIcons.IconFuelTemplateMarkup;
                        cssClass = "icon-fuel";
                        break;
                    case "TrkVwTemp":
                        title = i18nextko.t("Common_Temperature1")();
                        icon = AssetIcons.IconTemparetureTemplateMarkup;
                        cssClass = "icon-temperature";
                        break;
                    case "TrkVwVehicleBattery":
                        title = i18nextko.t("Common_VehicleBatteryLevel")();
                        icon = AssetIcons.IconVehicleBatteryTemplateMarkup;
                        cssClass = "icon-vehiclebattery";
                        break;
                    case "TrkVwDeviceBattery":
                        title = i18nextko.t("Common_DeviceBatteryLevel")();
                        icon = AssetIcons.IconDeviceBatteryTemplateMarkup;
                        cssClass = "icon-devicebattery";
                        break;
                    case "TrkVwSOS":
                        title = i18nextko.t("Common_SOS")();
                        icon = AssetIcons.IconSosTemplateMarkup;
                        cssClass = "icon-sos";
                        break;
                    case "TrkVwPower":
                        title = i18nextko.t("Common_Power")();
                        icon = AssetIcons.IconPowerTemplateMarkup;
                        cssClass = "icon-power";
                        break;
                    case "TrkVwDRVCardReader":
                        title = i18nextko.t("Common_DriverCardReader")();
                        icon = AssetIcons.IconDriverCardTemplateMarkup;
                        cssClass = "icon-drivercard";
                        break;
                    case "TrkVwPSGRCardReader":
                        title = i18nextko.t("Common_PassengerCardReader")();
                        icon = AssetIcons.IconPassengerCardTemplateMarkup;
                        cssClass = "icon-passengercard";
                        break;
                    case "TrkVwMileage":
                        title = i18nextko.t("Common_Mileage")();
                        icon = AssetIcons.IconMileageTemplateMarkup;
                        cssClass = "icon-mileage";
                        break;
                    default:
                        title = o.featureName;
                        icon = AssetIcons.IconCustomFeatureTemplateMarkup;
                        cssClass = "icon-customfeature";
                        break;
                }

                statuses.push({
                    title: title,
                    text: text,
                    icon: icon,
                    cssClass: cssClass,
                    key: key
                });
            }
        });

        return statuses;
    }



    /**
    * Get statues of asset to display on asset info screen in map.
    * Display only status that has value, no need to check permission. 
    * Service always return only items that user has permission.
    * 
    * @static
    * @param {any} info
    * @param {any} userSession (WebConfig.userSession pass from caller)
    * @returns
    * 
    */
    static getShipmentInfo(paramRaw, paramStatus,showDuration = true) {
        var shipmentInfo = [];

        ////Unit-Id
        //shipmentInfo.push({
        //    "key": "boxSerialNo",
        //    "title": i18nextko.t("vehicle-info-widget-box-id")(),
        //    "text": paramRaw["boxSerialNo"] || '-'
        //});

        ////Reference No.
        //shipmentInfo.push({
        //    "key": "vehicleId",
        //    "title": i18nextko.t("vehicle-info-widget-vehicle-id")(),
        //    "text": paramRaw["vehicleId"] || '-'
        //});

        ////Vehicle Description
        //shipmentInfo.push({
        //    "key": "vehicleDescription",
        //    "title": i18nextko.t("vehicle-info-widget-vehicle-id")(),
        //    "text": paramRaw["vehicleDescription"] || '-'
        //});

        //Driver Name
        shipmentInfo.push({
            "key": "driverName",
            "title": i18nextko.t("Common_Driver")(),
            "text": paramRaw["driverName"] || '-'
        });

        let formatTriptime = "-";
        if (paramRaw["triptime"]) {
            var calMoveDuration = "";

            var moveDuration = paramRaw["triptime"];
            var dayInMinutes = 60 * 24;

            var day = Math.floor(moveDuration / dayInMinutes);
            if (day > 0) {
                calMoveDuration += day > 0 ? day : "-";
                calMoveDuration += " " + i18nextko.t("Common_Day")();
            }

            var hr = Math.floor((moveDuration % dayInMinutes) / 60);
            if (hr > 0) {
                calMoveDuration += hr > 0 ? " " + hr : " -";
                calMoveDuration += " " + i18nextko.t("Common_Hour_Short")();
            }

            var minutes = Math.floor((moveDuration % dayInMinutes) % 60);
            if (minutes > 0) {
                calMoveDuration += minutes > 0 ? " " + minutes : " -";
                calMoveDuration += " " + i18nextko.t("Common_Minutes_Short")();
            }


            formatTriptime = calMoveDuration;

            if (moveDuration == 0) {
                formatTriptime = "-"
            }
        }
        //Trip Time
        shipmentInfo.push({
            "key": "triptime",
            "title": i18nextko.t("Common_TripTime")(),
            "text": formatTriptime
        });

        //Business Unit
        shipmentInfo.push({
            "key": "businessUnitName",
            "title": i18nextko.t("Common_BusinessUnit")(),
            "text": paramRaw["businessUnitName"] || '-'
        });

        ////Job Status
        //shipmentInfo.push({
        //    "key": "jobStatusDisplayName",
        //    "title": i18nextko.t("Common_JobStatus")(),
        //    "text": paramRaw["jobStatusDisplayName"] || '-'
        //});

        //DateTime
        shipmentInfo.push({
            "key": "formatDateTime",
            "title": i18nextko.t("Common_Datetime")(),
            "text": paramRaw["formatDateTime"] || '-'
        });

        //Location
        shipmentInfo.push({
            "key": "location",
            "title": i18nextko.t("Common_Location")(),
            "text": paramRaw["location"] || '-'
        });

        //Status Used
        //Movement, Speed, Engine, Gate Status(Optional),Fuel Level(Optional), Temperature(Optional), GPS Status
        var arrStatusField = ["movementDisplayName", "speed", "TrkVwEngine", "TrkVwGate", "TrkVwFuel", "TrkVwTemp", "gpsStatusDisplayName"];
       
        _.forEach(arrStatusField, (statusName) => {
            for (var i = 0; i < paramStatus.length; i++) {
                var statusItem = paramStatus[i];
                
                if (statusItem.key == statusName) {
                    var data = {};
                    //เลือกแก้การแสดงผลเฉพาะ movementDisplayName 
                    if(statusItem.key == "movementDisplayName"){

                        data = {
                            "key": statusItem.key,
                            "title": statusItem.title,
                            "text": "<div>"+i18nextko.t("wg-vehicle-info-park-time")() || '-',
                            "icon": statusItem.icon,
                            "cssClass": statusItem.cssClass
                        }
                        //     // แสดงผล 2 บรรทัด

                        if(showDuration){
                            switch (paramRaw["movement"]) {
                                case Enums.ModelData.MovementType.Park:
                                case Enums.ModelData.MovementType.ParkEngineOn:
                                        if (paramRaw["formatParkDurationCustom"] !== undefined || paramRaw["formatIdleTimeCustom"] !== undefined) {
                                            data.text += " : " + paramRaw["formatParkDurationCustom"] + "<br>"
                                                + i18nextko.t("wg-vehicle-info-park-idle-time")() + " : "
                                                + paramRaw["formatIdleTimeCustom"]
                                                + "</div>"
                                        }
                                    break;
                                case Enums.ModelData.MovementType.Move:
                                    if (paramRaw["formatmoveDurationCustom"] !== undefined) { // เช็คifหากมีmoveDurtionคืนมาจาก service
    
                                        data.text = paramRaw["formatmoveDurationCustom"] == "-"?paramRaw["movementDisplayName"]:paramRaw["movementDisplayName"] +" : "+paramRaw["formatmoveDurationCustom"]
                                    }
                                    break;
                                case Enums.ModelData.MovementType.MoveOver35Hrs:
                                case Enums.ModelData.MovementType.MoveOver4Hrs:
                                    if (paramRaw["formatmoveDurationCustom"] !== undefined) { // เช็คifหากมีmoveDurtionคืนมาจาก service
    
                                        data.text = paramRaw["formatmoveDurationCustom"] == "-" ? paramRaw["movementTriptimeOverDisplayName"] : paramRaw["movementTriptimeOverDisplayName"] + " : " + paramRaw["formatmoveDurationCustom"];
                                    }
                                    break;
                                default:
                                    data.text = paramRaw["movementDisplayName"];
                                    break;
                            }
                        }
                        else{ // showDuration = false แสดงว่ามาจาก หน้า Find Nearest
                            data.text = paramRaw["movementDisplayName"];
                        }
            

                    }else{

                        //แสดงผลของ move,stop
                        data = {
                            "key": statusItem.key,
                            "title": statusItem.title,
                            "text": statusItem.text || '-',
                            "icon": statusItem.icon,
                            "cssClass": statusItem.cssClass
                        }
                    }

                    // if (statusItem.key == "movementDisplayName"){
                    //     if (paramRaw["movement"]  == Enums.ModelData.MovementType.Park || paramRaw["movement"]  == Enums.ModelData.MovementType.ParkEngineOn){
                    //         data.text += " (" + paramRaw["formatParkDurationCustom"] + ")";
                    //     }
                    // }

                    shipmentInfo.push(data);
                    break;
                }
            }
        });

        if (paramRaw["jobCode"]) {
            //Shipment Status
            if (paramRaw["jobStatusDisplayName"]) {
                shipmentInfo.push({
                    "key": "jobStatusDisplayName",
                    "title": i18nextko.t("Transportation_Mngm_ShipmentStatus")(),
                    "text": paramRaw["jobStatusDisplayName"] || '-'
                });
            }
            

            //Shipment Code
            if (paramRaw["jobCode"]) {
                shipmentInfo.push({
                    "key": "jobCode",
                    "title": i18nextko.t("Transportation_Mngm_Shipment_Code")(),
                    "text": paramRaw["jobCode"] || '-'
                });
            }

            //Shipment Name
            if (paramRaw["jobName"]) {
                shipmentInfo.push({
                    "key": "jobName",
                    "title": i18nextko.t("Transportation_Mngm_Shipment_Name")(),
                    "text": paramRaw["jobName"] || '-'
                });
            }

            //Remark
            if (paramRaw["jobDescription"]) {
                shipmentInfo.push({
                    "key": "remark",
                    "title": i18nextko.t("Common_Remark")(),
                    "text": Utility.convertUserTagToHTML(paramRaw["jobDescription"]) || '-'
                });
            }

        }


        //shipmentInfo.push({
        //    "key": "remark",
        //    "title": i18nextko.t("Common_Remark")(),
        //    "text": Utility.convertUserTagToHTML('[b][I][u][C COLOR=blue]AAAAA[/C][/u][/I][/b] [B]BBBBB[/B] [I]IIIII[/I] [U]UUUUUU[/U] [color=#00ff00]COLOR GREEN[/color]')
        //});

        return shipmentInfo;
    }

    static getLocationInfoStatus(locationInfo) {

        var statuses = [];
        //Movement
        statuses.push({
            title: i18nextko.t("Common_Movement")(),
            text: locationInfo.movementDisplayName,
            icon: AssetIcons.IconMovementTemplateMarkup,
            cssClass: "icon-movement",
            key: "movementDisplayName"
        });

        //Speed
        statuses.push({
            title: i18nextko.t("Common_Speed")(),
            text: locationInfo.speed,
            icon: AssetIcons.IconSpeedTemplateMarkup,
            cssClass: "icon-speed",
            key: "speed"
        });

        //Direction
        statuses.push({
            title: i18nextko.t("Common_Direction")(),
            text: locationInfo.directionTypeDisplayName,
            icon: AssetIcons.IconDirectionTemplateMarkup,
            cssClass: "icon-direction",
            key: "directionTypeDisplayName"
        });

        //GPS Status
        statuses.push({
            title: i18nextko.t("Common_GPSStatus")(),
            text: locationInfo.gpsStatusDisplayName,
            icon: AssetIcons.IconGpsTemplateMarkup,
            cssClass: "icon-gps",
            key: "gpsStatusDisplayName"
        });

        //GSM Status
        statuses.push({
            title: i18nextko.t("Common_GSMStatus")(),
            text: locationInfo.gsmStatusDisplayName,
            icon: AssetIcons.IconGsmTemplateMarkup,
            cssClass: "icon-gsm",
            key: "gsmStatusDisplayName"
        });


        //Features
        _.forEach(locationInfo.trackLocationFeatureValues, (o) => {
            //if featureCode is not null, that's mean this feature is installed.
            if (!_.isNil(o.featureCode)) {
                var title = ""; // tooltip
                var text = o.featureUnitSymbol ? Utility.stringFormat("{0} {1}", o.formatValue, o.featureUnitSymbol) : o.formatValue; // value
                var icon = ""; // svg
                var cssClass = ""; // cssClass
                var key = o.featureCode; //key

                switch (o.featureCode) {
                    case "TrkVwEngine":
                        title = i18nextko.t("Common_Engine")();
                        icon = AssetIcons.IconEngineTemplateMarkup;
                        cssClass = "icon-engine";
                        break;
                    case "TrkVwGate":
                        title = i18nextko.t("Common_Gate1")();
                        icon = AssetIcons.IconGateTemplateMarkup;
                        cssClass = "icon-gate";
                        break;
                    case "TrkVwFuel":
                        title = i18nextko.t("Common_FuelLevel")();
                        icon = AssetIcons.IconFuelTemplateMarkup;
                        cssClass = "icon-fuel";
                        break;
                    case "TrkVwTemp":
                        title = i18nextko.t("Common_Temperature1")();
                        icon = AssetIcons.IconTemparetureTemplateMarkup;
                        cssClass = "icon-temperature";
                        break;
                    case "TrkVwVehicleBattery":
                        title = i18nextko.t("Common_VehicleBatteryLevel")();
                        icon = AssetIcons.IconVehicleBatteryTemplateMarkup;
                        cssClass = "icon-vehiclebattery";
                        break;
                    case "TrkVwDeviceBattery":
                        title = i18nextko.t("Common_DeviceBatteryLevel")();
                        icon = AssetIcons.IconDeviceBatteryTemplateMarkup;
                        cssClass = "icon-devicebattery";
                        break;
                    case "TrkVwSOS":
                        title = i18nextko.t("Common_SOS")();
                        icon = AssetIcons.IconSosTemplateMarkup;
                        cssClass = "icon-sos";
                        break;
                    case "TrkVwPower":
                        title = i18nextko.t("Common_Power")();
                        icon = AssetIcons.IconPowerTemplateMarkup;
                        cssClass = "icon-power";
                        break;
                    case "TrkVwDRVCardReader":
                        title = i18nextko.t("Common_DriverCardReader")();
                        icon = AssetIcons.IconDriverCardTemplateMarkup;
                        cssClass = "icon-drivercard";
                        break;
                    case "TrkVwPSGRCardReader":
                        title = i18nextko.t("Common_PassengerCardReader")();
                        icon = AssetIcons.IconPassengerCardTemplateMarkup;
                        cssClass = "icon-passengercard";
                        break;
                    case "TrkVwMileage":
                        title = i18nextko.t("Common_Mileage")();
                        icon = AssetIcons.IconMileageTemplateMarkup;
                        cssClass = "icon-mileage";
                        break;
                    default:
                        title = o.featureName;
                        icon = AssetIcons.IconCustomFeatureTemplateMarkup;
                        cssClass = "icon-customfeature";
                        break;
                }

                statuses.push({
                    title: title,
                    text: text,
                    icon: icon,
                    cssClass: cssClass,
                    key: key
                });
            }
        });

        return statuses;
    }


    
    /**
     * Get list of  none accessible businessUnit id.
     * 
     * @static
     * @param {any} businessUnits
     * @returns
     * 
     * @memberOf AssetInfo
     */
    static getNoneAccessibleBU(businessUnits){
        var result = [];
         _.each(businessUnits, (bu) => {
             if(!bu.isAccessible){
                 result.push(bu.id.toString());
             }

             //Check if this obj has child.
             var child = bu.childBusinessUnits
             if (child && child.length > 0){
                 let children = this.getNoneAccessibleBU(child);
                 result = _.union(result,children);
             }
         });
        return result;
    }
}

export default AssetInfo;