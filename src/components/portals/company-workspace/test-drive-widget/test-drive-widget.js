﻿import ko from "knockout";
import templateMarkup from "text!./test-drive-widget.html";
import ScreenBaseWidget from "../../screenbasewidget";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import MapManager from "../../../controls/gisc-chrome/map/mapManager";
import Utility from "../../../../app/frameworks/core/utility";
import WebRequestFleetMonitoring from "../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import "kendo.all.min";
import AssetUtility from "../asset-utility";

class TestDriveWidget extends ScreenBaseWidget {
    constructor(params) {
        super(params);
        this.listSms = ko.observableArray([]);
        this.selectedIndex = ko.observable(0);
        this.order = ko.observable([]);
    }

    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        this.webRequestFleetMonitoring.listSms().done((res)=>{
            var list = new Array();
            var i = 1;
            _.forEach(res, (item, index)=>{
                var result = new ListSmsResultItem(item, index, i);
                list.push(result);
                i++
            })
            this.listSms(list);
            dfd.resolve();
        });
        return dfd;
    }

    onDomReady(){
        $("#tab-test-drive-widget").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        });
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = this.assets();
        var totalItems = items.length;
        dfd.resolve({items: items, totalRecords: totalItems, currentPage: 1});
        return dfd;
    }

    onSelectingRow(data) {
        
        let latitude = data.latitude
        let longitude = data.longitude
        let LvZoom = 19;
        let symbol = {
            url: this.resolveUrl("~/images/test-drive.png"),
            width: '40',
            height: '40',
            offset: {
                x: 0,
                y: 0
            },
            rotation: 0
        }

        let location = {lat:latitude,lon:longitude}
        MapManager.getInstance().drawOnePointZoom(null, location, symbol, LvZoom, null);
        
        setTimeout(()=>{
            MapManager.getInstance().zoomPoint(null, latitude, longitude, LvZoom - 1);
        }, 1000);
        
        this.webRequestFleetMonitoring.sendSms(data.mobileNo, data.messageAlert).done((res)=>{
            console.log("Send Messege: ", res);
        });

    }

    onEventSelected(event) {
        if(event !== null) {
            
            this.zoomAlert(event)
            return Utility.emptyDeferred();
        }
        return false;
    }
    
}

class ListSmsResultItem {
    
    // info = FindAssetsResultInfo, entityType = Enums.ModelData.SearchEntityType
    constructor (info, title, index) {
        var item = info;
        this.id = 'dgTestDriveWidget' + index;
        this.title = title;
        this.items = ko.observableArray(item);
        this._items = _.clone(item);
    }

    /**
     * On Datasource Request Read
     * 
     * @param {any} res
     * @returns
     * 
     * @memberOf TrackWidget
     */
    onDatasourceRequestRead (gridOption) {
        var dfd = $.Deferred();
        var items = this.items();
        var totalItems = items.length;
        let currPage = AssetUtility.currentPage(items, gridOption.page, gridOption.pageSize);
        if(gridOption.filter){
            items = AssetUtility.filter(this._items, gridOption.filter);
            totalItems = items.length;
            currPage = AssetUtility.currentPage(items, gridOption.page, gridOption.pageSize);
        }
        dfd.resolve({items: items, totalRecords: totalItems, currentPage: currPage});
        return dfd;
    }

}



export default {
    viewModel: ScreenBaseWidget.createFactory(TestDriveWidget),
    template: templateMarkup
};