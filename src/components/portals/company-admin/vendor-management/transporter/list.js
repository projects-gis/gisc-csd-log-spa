﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import webRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";

class TransporterListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Vendor_TransporterTitle")());
        this.bladeSize = BladeSize.Medium;
        this.dataItems = ko.observableArray([]);
        this.filterText = ko.observable('');
        this.recentChangedRowIds = ko.observableArray([]);

        // Observe change about filter
        this.subscribeMessage("ca-vendor-transporter-management-changed",(id) => { 
            var userFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                userType: Enums.UserType.Company,
            };
            this.webRequestTransporter.listTransporter(userFilter).done((response) => {
                var data = response["items"];
                this.dataItems.replaceAll(data);
                this.recentChangedRowIds.replaceAll([id]);
            });
        });


        this.selectingRow = (transportor) => {
            return this.navigate("ca-vendor-management-transporter-view", {
                transportorId : transportor.id
            });
            return false;          
        };

    }

    get webRequestTransporter() {
        return webRequestTransporter.getInstance();
    }


    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var userFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            userType: Enums.UserType.Company,
        };
        this.webRequestTransporter.listTransporter(userFilter).done((response) => {
            this.dataItems(response["items"]);
        });
    }

    /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateTransporter)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onCommandClick(sender) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateTransporter)) {
            if (sender.id == "cmdCreate") {
                this.navigate("ca-vendor-management-transporter-manage", 
                {
                    mode: "create"
                }).fail((e)=> {
                    this.handleError(e);
                }).always(()=>{
                    this.isBusy(false);
                });
            }
        }
        return false;
    }

    /**
     * 
     * 
     * 
     * @memberOf TrackingDatabasesListScreen
     */
    onUnload() {

    }
}

export default {
viewModel: ScreenBase.createFactory(TransporterListScreen),
    template: templateMarkup
};