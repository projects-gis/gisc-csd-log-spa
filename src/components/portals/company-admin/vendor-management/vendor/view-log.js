﻿import ko from "knockout";
import templateMarkup from "text!./view-log.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";
import webRequestVendorTransportConnecter from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendorTransportConnecter";

class ViewLogScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Vendor_ViewConnectorLogs")());
        // this.bladeSize = BladeSize.XLarge;
        this.bladeIsMaximized = true;
        this.apiDataSource = ko.observableArray([]);
        this.date = ko.observable(new Date());
        this.formatSet = "dd/MM/yyyy";
        this.VendorId = this.ensureNonObservable(params);

        this.viewSearch =() => {
            let Date = (typeof this.date() == "undefined")? null : this.date();
            var filterState = {
                VendorId: this.VendorId,
                DataDate: Date,
            }
            this.publishMessage("ca-vendor-view-log-search-changed", filterState);
        }
        
        this.viewData = (data) => {
            this.navigate("ca-vendor-management-vendor-detail",
                {
                    LogId: data.LogId
                });
        };

        this.subscribeMessage("ca-vendor-view-log-search-changed",(data) => {
            this.apiDataSource({
                read: this.webRequestVendorTransportConnecter.listVendorLogDataGrid(data),
            });
        });

    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var filter = {
            VendorId: this.VendorId,
            DataDate: this.date(),
        };
        this.apiDataSource({
            read: this.webRequestVendorTransportConnecter.listVendorLogDataGrid(filter),
            update: this.webRequestVendorTransportConnecter.listVendorLogDataGrid(filter)
        });
       
    }
    get webRequestVendorTransportConnecter() {
        return webRequestVendorTransportConnecter.getInstance();
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export")); 
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

    onUnLoad(){

    }
    
}

export default {
viewModel: ScreenBase.createFactory(ViewLogScreen),
    template: templateMarkup
};