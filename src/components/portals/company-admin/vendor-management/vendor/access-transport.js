﻿import ko from "knockout";
import templateMarkup from "text!./access-transport.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";
import WebRequestVendorTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendorTransport";
import webRequestTransporter from "../../../../../app/frameworks/data/apitrackingcore/webRequestTransporter";

class AccessTransportorsScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        
        this.bladeTitle(this.i18n("Vendor_AccessibleTransportors")());
        this.bladeSize = BladeSize.Small;
        this.name = ko.observable();
        this.ip = ko.observable();
        this.transportor = ko.observableArray([]);

        this.items = ko.observableArray([]);

        this.selectedItems = ko.observableArray([]);
        this.order = ko.observable([[ 1, "asc" ]]);
        this.selectedTransport = ko.observable();
        
        this.tempItemAccess = ko.observableArray([]);
        this.transportAll = this.ensureObservable(params.TransAll, []);
        this.itemOnSelected = this.ensureObservable(params.itemSelected,[]);
        this.transportAllFromSelected = this.ensureObservable(params.itemAll,[]);

        this.isEnable = ko.observable(true);
        this.isSubscribe =  (Object.keys(this.itemOnSelected()).length > 0)?false:true ;  // check Subscribe
        this.dataTest = ko.observableArray([]);

        this.dataTest2 = ko.observableArray([
            {id:1,name:"abc"},
            {id:2,name:"abcd"}
        ]);

        if(this.isSubscribe){
            //Subscribe Transportors
            this.selectedTransport.subscribe((val)=>{
                let id  = (typeof val == 'undefined' || val == null) ? null:val.id;
                this.webRequestVendorTransporter.listVendorTransportBu({TransportId:id}).done((r)=>{
                    if(r.items.length > 0){
                        var tempTransport = r["items"];
                        var itemTransport = [];

                        tempTransport.forEach(function(v){
                            if(v.name != "-"){
                                itemTransport.push(v);
                            }
                        });

                        this.items(itemTransport);
                    }
                    else{
                        this.items([]);
                    }
                });
            }); 
        }

    }
    // call WebRequest Transport
    get webRequestTransporter() {
        return webRequestTransporter.getInstance();
    }
    //call WeRequest VendorTransport
    get webRequestVendorTransporter(){
        return WebRequestVendorTransporter.getInstance();
    }
    


    //Required field do this method
    setupExtend() {
        this.selectedTransport.extend({trackChange:true});
        this.selectedItems.extend({
            trackArrayChange: true
        });

        this.selectedTransport.extend({required:true});
        this.selectedItems.extend({
            arrayRequired: true
        });

        this.validationModel = ko.validatedObservable({
            Transport:this.selectedTransport ,
            TransportBu:this.selectedItems
        });
       
    }

    addTransportor(){
        
    }

    /**
    * @lifecycle Called when View is loaded.
    * @param {boolean} isFirstLoad true if ViewModel is first load
    */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred(); 
        
        var userFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            userType: Enums.UserType.Company,
        };

        if(Object.keys(this.transportAll()).length > 0){  //  if selected transport don't select transport dupicate 
            this.webRequestTransporter.listTransporter(userFilter).done((responseV)=>{
                if(responseV.items.length > 0){
                    var tempItems = responseV["items"];
                    let match = {};

                    tempItems.forEach((v)=>{
                        match = _.find(this.transportAll(), (data) => {
                            return v.id == data.id;
                        });
                        if(!match) {
                            this.transportor.push(v);
                        }
                    });
                }
                dfd.resolve();
            }).fail((e)=>{
                dfd.reject(e);
            });

        }
        else if(Object.keys(this.itemOnSelected()).length > 0 ){  //check  Transport to Selected
            this.webRequestTransporter.listTransporter(userFilter).done((responseV)=>{
                if(responseV){
                    this.isEnable(false);
                    this.transportor(responseV.items);
                    this.selectedTransport(ScreenHelper.findOptionByProperty(this.transportor,"id",this.itemOnSelected().transporterId));
                }
            });

            this.webRequestVendorTransporter.listVendorTransportBu({TransportId:this.itemOnSelected().transporterId}).done((response) => {
                debugger
                let tmpItem = response.items  ;
                let itemTransport = [];

                tmpItem.forEach(function(v){ 
                    if(v.name != "-"){
                        itemTransport.push(v);
                    }
                });
                this.items(itemTransport);
                var temppp = this.itemOnSelected().vendorAccessibleBusinessUnitInfos ;
                _.each(temppp, (data) => { // loop for item selected   
                    let match = _.find(this.items(), (transport) => { //find item selected
                        return transport.id == data.id;
                    });

                    if(match) {
                        this.selectedItems.push(match); //Add Selected Item
                    }
                });
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        }
        else{

            this.webRequestTransporter.listTransporter(userFilter).done((r) => {
                this.transportor(r.items);
                dfd.resolve();
            }).fail((e)=> {
                dfd.reject(e);
            });
        }

        return dfd;

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
   
   
    buildActionBar(actions) {
        actions.push(this.createAction("actSave",this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
       
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id==="actSave"){

            if(!this.validationModel.isValid()){
                this.validationModel.errors.showAllMessages();
                return ;
            }

            if(Object.keys(this.itemOnSelected()).length > 0 ){  //if Update Bu in transport 
                var tempItemAll = this.transportAllFromSelected();
                var sentItem = [];
                var buList = this.selectedItems();
                for(var i=0;i<tempItemAll.length;i++){
                    if(tempItemAll[i].id == this.itemOnSelected().id ){
                        tempItemAll[i].vendorAccessibleBusinessUnitInfos = buList ;
                    }
                }
                this.publishMessage("ca-vendor-access-transport-update",{
                    items:tempItemAll,
                    selectedId:this.itemOnSelected().id
                });
            }
            else{  // add new transport

                this.selectedItems().forEach((v)=>{  // Add Field BusinessUnitId 
                    v.BusinessUnitId = v.id
                });

                var buList = this.selectedItems();
                var dataOnAccess = [];
                this.transportAll().push(this.selectedTransport()); //push item
                dataOnAccess = this.transportAll();
                dataOnAccess.forEach((v)=>{
                    debugger
                    if(!v.vendorAccessibleBusinessUnitInfos){ // if dataOnAccess don't have key vendorAccessibleBusinessUnitInfos is insert key
                        v["vendorAccessibleBusinessUnitInfos"] = buList; 
                    }
                    if(!v.transporterId && !v.transporterName ){ // if don't has key transport , add key transport
                        v.transporterId = v.id ; 
                        v.transporterName = v.name;
                    }
                   
                });
                this.publishMessage("ca-vendor-access-transport", dataOnAccess);
            }
            
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
    
    /**
     * 
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
   
}

export default {
viewModel: ScreenBase.createFactory(AccessTransportorsScreen),
    template: templateMarkup
};