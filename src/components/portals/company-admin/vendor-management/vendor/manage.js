﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestVendorTransport from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendorTransport";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";

class ManageVendorScreen extends ScreenBase {
    constructor(params) {
        super(params);
        console.log(params);
        this.mode  = this.ensureNonObservable(params.mode);
        this.item = this.ensureObservable(params.item,null);

        switch(this.mode){
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Vendor_CreateVendor")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Vendor_UpdateVendor")());
                break;
        }
        
        this.bladeSize = BladeSize.Small;
        this.name = ko.observable();
        this.ip = ko.observable();
        this.username = ko.observable();
        this.password = ko.observable();
        this.vendortransportor = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);
        this.recentChangedRowIds = ko.observableArray([]);

        if(this.item()){ 
            this.name(this.item().name);
            this.ip(this.item().ipAddress);
            this.username(this.item().userName);
            this.password(this.item().password);
            this.vendortransportor(this.item().vendorAccessibleTransporterInfos);
            
        }
        // Observe change about User Groups
        this.subscribeMessage("ca-vendor-access-transport", (data) => {
            console.log(data);
            this.vendortransportor.replaceAll(data);
        });

        this.subscribeMessage("ca-vendor-access-transport-update",(data)=>{
            this.vendortransportor.replaceAll(data.items);
            this.recentChangedRowIds.replaceAll([data.selectedId]);
        });
        this.selectingRow = (data) =>{
            console.log(data);
            this.navigate("ca-vendor-management-vendor-accesstransport",{
                itemSelected : data,
                itemAll : this.vendortransportor()
            });
        };
    }
    


    get webRequestVendorTransport() {
        return WebRequestVendorTransport.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    setupExtend(){

        this.name.extend({required:true});
        this.ip.extend({required:true});
        this.username.extend({required:true});
        this.password.extend({required:true});
        this.vendortransportor.extend({
            arrayRequired: true
        });

        this.validationModel = ko.validatedObservable({
            name:this.name,
            ipAddress:this.ip,
            username : this.username,
            password : this.password,
            accessTransport:this.vendortransportor
        });
    }

    addTransportor(){

        this.navigate("ca-vendor-management-vendor-accesstransport", { 
            TransAll : this.vendortransportor()
        });
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
   
   
    buildActionBar(actions) {
        actions.push(this.createAction("actSave",this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
       
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            switch(this.mode){
                case Screen.SCREEN_MODE_CREATE:
                    var filter = {
                        name : this.name(),
                        ipAddress : this.ip(),
                        username : this.username(),
                        password : this.password(),
                        vendorAccessibleTransporterInfos: this.vendortransportor()
                    }

                    this.webRequestVendorTransport.createVendorTransport(filter).done((r)=>{
                        this.close()
                        this.publishMessage("ca-vendor-add-success");
                    }).fail((e)=>{
                        this.handleError(e)
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:

                    break;
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        
    }
    
    /**
     * 
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
   
}

export default {
viewModel: ScreenBase.createFactory(ManageVendorScreen),
    template: templateMarkup
};