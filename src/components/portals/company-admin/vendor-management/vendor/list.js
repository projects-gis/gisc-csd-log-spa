﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestVendorTransport from "../../../../../app/frameworks/data/apitrackingcore/webRequestVendorTransport";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";

class VendorsListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Vendor_VendorsTitle")());
        this.bladeSize = BladeSize.Large;
        this.items = ko.observableArray([]);
        this.filterText = ko.observable();
        this.order = ko.observable([[ 0, "asc" ]]);
        this.selectingRow = (vendor) =>{
            this.navigate("ca-vendor-management-vendor-view",
                { 
                    id: vendor.id
                }
                )
        };

        this.onClickViewlog = (vendor) =>{
            this.navigate("ca-vendor-management-vendor-log",vendor.id)
        };

        this.subscribeMessage("ca-vendor-add-success",()=>{
            this.webRequestVendorTransport.listVendorTransport({}).done((response)=>{
                var itemsVendor = response["items"];
                itemsVendor.forEach((x)=>{
                    x.log = "../../images/ic-search.svg";
                });
                this.items.replaceAll(itemsVendor);
            }).fail((e)=>{
                this.handleError(e);
            });
        });
    }
    

    get webRequestVendorTransport() {
        return WebRequestVendorTransport.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestVendorTransport.listVendorTransport({}).done((response)=>{
         
            var itemsVendor = response["items"];
            itemsVendor.forEach((x)=>{
                x.log = "../../images/ic-search.svg";
            });
            this.items(itemsVendor);            
            dfd.resolve();
        }).fail((e)=>{
            this.handleError(e);
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
   
   
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateVendor))
        {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("ca-vendor-management-vendor-manage",{mode:"create"});
        }
    }
    
    /**
     * 
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
   
}

export default {
viewModel: ScreenBase.createFactory(VendorsListScreen),
    template: templateMarkup
};