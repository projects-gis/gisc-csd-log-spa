﻿import ko from "knockout";
import templateMarkup from "text!./edit-vehicle.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class EditVehicleScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Assets_UpdateVehicle")());
        this.bladeSize = BladeSize.Small;
        this.items = ko.observableArray([
            {id:1,name:"Vendor Name1"},
            {id:2,name:"Vendor Name2"}
        ]);
        this.filterText = ko.observable();
        this.apiDataSource = ko.observableArray([]);
        this.apiDataSource({
            read:this.items()
        });
    }
    
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
   
   
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.navigate("ca-vendor-management-vendor-manage");
        }
    }
    
    /**
     * 
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
   
}

export default {
viewModel: ScreenBase.createFactory(EditVehicleScreen),
    template: templateMarkup
};