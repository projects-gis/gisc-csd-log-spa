import ko from "knockout";
import templateMarkup from "text!./business-units-levels-manage.html";
import ScreenBase from "../../screenbase";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnitLevel from "../../../../app/frameworks/data/apicore/webRequestBusinessUnitLevel";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import ScreenHelper from "../../screenhelper";

/**
 * 
 * 
 * @class BusinessUnitLavelsManageScreen
 * @extends {ScreenBase}
 */
class BusinessUnitLavelsManageScreen extends ScreenBase {

    /**
     * Creates an instance of BusinessUnitLavelsManageScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitLavelsManageScreen
     */
    constructor(params) {
        super(params);
        this.mode = this.ensureNonObservable(params.mode);
        this.items = this.ensureNonObservable(params.data);
        this.id = null;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.totalRecords = this.ensureNonObservable(params.totalRecords);
                this.bladeTitle(this.i18n("BU_CreateBusinessUnitLevel")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.id = this.ensureNonObservable(params.data.id);
                this.bladeTitle(this.i18n("BU_UpdateBusinessUnitLevel")());
                break;
        }

        this.level = ko.observable(0);
        this.nameTitleEng = ko.observable();
        this.nameTitleLocal = ko.observable();
        this.nameEng = ko.observable();
        this.nameLocal = ko.observable();
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnitLevel() {
        return WebRequestBusinessUnitLevel.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        let items = this.items;
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                let totalRecords =  this.totalRecords == 0 ? 1 : this.totalRecords + 1;
                this.level(totalRecords);
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.level(items.levelNo);
                this.nameTitleEng(items.nameTitleEnglish);
                this.nameTitleLocal(items.nameTitleLocal);
                this.nameEng(items.nameEnglish);
                this.nameLocal(items.nameLocal);
                break;
        }
        
    }

    /**
     * 
     * @memberOf BusinessUnitLavelsManageScreen
     */
    setupExtend() {
        this.nameTitleEng.extend({ trackChange: true});
        this.nameTitleLocal.extend({ trackChange: true});
        this.nameEng.extend({ trackChange: true});
        this.nameLocal.extend({ trackChange: true});

        // Validation
        this.nameTitleEng.extend({ required: true});
        this.nameTitleLocal.extend({ required: true});
        this.nameEng.extend({ required: true});
        this.nameLocal.extend({ required: true});

        this.validationModel = ko.validatedObservable({
            nameTitleEng: this.nameTitleEng,
            nameTitleLocal: this.nameTitleLocal,
            nameEng: this.nameEng,
            nameLocal: this.nameLocal
        });

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateBusinessUnit))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateBusinessUnit))) 
        {
             actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        // if(this.mode === Screen.SCREEN_MODE_UPDATE){
        //     commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        // }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);
            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestBusinessUnitLevel.createBusinessUnitLevel(model).done(() => {
                        this.publishMessage("ca-business-unit-level-changed");
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestBusinessUnitLevel.updateBusinessUnitLevel(model).done(() => {
                        this.publishMessage("ca-business-unit-level-changed", {id: this.id , mode: this.mode});
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * Generate Business Unit model from View Model
     * 
     * @returns Business Unit model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.id,
            levelNo: this.level(),
            nameTitleEnglish: this.nameTitleEng(),
            nameTitleLocal: this.nameTitleLocal(),
            nameEnglish: this.nameEng(),
            nameLocal: this.nameLocal()
        };

        return model;
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        // if (sender.id === "cmdDelete") {
            // this.webRequestBusinessUnitLevel.deleteBusinessUnitLevel(this.id).done(() => {
            //     this.publishMessage("ca-business-unit-level-deleted");
            //     this.close(true);
            // }).fail((e)=> {
            //     this.handleError(e);
            // });
        // }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BusinessUnitLavelsManageScreen),
    template: templateMarkup
};