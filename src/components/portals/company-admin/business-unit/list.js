﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../app/frameworks/constant/screen";
import ScreenBase from "../../screenbase";
import Utility from "../../../../app/frameworks/core/utility";
import {BusinessUnitFilter} from "./infos";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Constants} from "../../../../app/frameworks/constant/apiConstant";

/**
 * 
 * 
 * @class BusinessUnitListScreen
 * @extends {ScreenBase}
 */
class BusinessUnitListScreen extends ScreenBase {
    /**
     * Creates an instance of BusinessUnitListScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitListScreen
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("BU_BusinessUnitsHierarchy")());

        this.businessUnits = ko.observableArray();
        this.selectedBusinessUnit = ko.observable();
        this.collapsedNodes = ko.observableArray();

        this.defaultFilter = new BusinessUnitFilter();


         this._onSelectingNode = (businessUnitIds) => {
            if(businessUnitIds) {
                return this.navigate("ca-business-unit-view", {
                    id: parseInt(businessUnitIds)
                });
            }
            return false;
        };

        this.subscribeMessage("ca-business-unit-changed", (vendorId) => {
            this.isBusy(true);
            this.webRequestBusinessUnit.listBusinessUnitSummary(this.defaultFilter).done((r) => {
                this.businessUnits(r["items"]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-business-unit-deleted", (vendorId) => {
            this.isBusy(true);
            this.webRequestBusinessUnit.listBusinessUnitSummary(this.defaultFilter).done((r) => {
                this.businessUnits(r["items"]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (isFirstLoad) {
            var dfd = $.Deferred();
            this.webRequestBusinessUnit.listBusinessUnitSummary(this.defaultFilter).done((r) => {
                this.businessUnits(r["items"]);
                dfd.resolve();
            }).fail((e)=> {
                dfd.reject(e);
            });
            return dfd;
        }
    }

    /**
     * For custom build node.
     * 
     * @param {any} node
     * 
     * @memberOf TreeViewScreen
     */
    onBuildBusinessUnitNode(node) {
        var text = Utility.stringFormat("{0} ({1})", node.text, node.rawData.code);
        node.li_attr.title = text;
        node.text = text;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBusinessUnit(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateBusinessUnit)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            this.selectedBusinessUnit(null);
            this.navigate("ca-business-units-hierarchy-manage", {
                mode: Screen.SCREEN_MODE_CREATE
            });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BusinessUnitListScreen),
    template: templateMarkup
};