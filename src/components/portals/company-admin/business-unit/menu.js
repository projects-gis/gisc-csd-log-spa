import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class BusinessMenuScreen extends ScreenBase {

    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("BU_BusinessUnits")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.items = ko.observableArray([]);
        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.page);
            }
            return false;
        };
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        // TODO: Verify access right to specific items.
        if (!isFirstLoad) {
            return;
        }

        // if(WebConfig.userSession.hasPermission(Constants.Permission.AssetMonitoring))
        // {
            this.items.push({
                text: this.i18n('BU_BusinessUnitsHierarchy')(),
                page: 'ca-business-unit'
            });
        // }
    
        // if(WebConfig.userSession.hasPermission(Constants.Permission.AssetMonitoring))
        // {
            this.items.push({
                text: this.i18n('BU_BusinessUnitsLevels')(),
                page: 'ca-business-units-levels'
            });
        // }

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    // /**
    //  * Navigate to inner screens.
    //  * @param {any} extras
    //  */
    // goto (extras) {
    //     this.navigate(extras.id);
    // }
}

export default {
viewModel: ScreenBase.createFactory(BusinessMenuScreen),
    template: templateMarkup
};