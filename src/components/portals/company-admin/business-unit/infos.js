import ko from "knockout";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import DefaultSoring from "../../../../app/frameworks/constant/defaultSorting";
import {EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";

class BusinessUnitFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.BusinessUnit;
        this.includeAssociationNames = [
            EntityAssociation.BusinessUnit.ChildBusinessUnits
        ];
    }
}
export { BusinessUnitFilter };

class ParentBusinessUnitFilter {
    constructor (ignoreSameHirachyId = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.BusinessUnit;
        this.includeAssociationNames = [
            EntityAssociation.BusinessUnit.ChildBusinessUnits
        ];
        this.maxLevels = 4;
        this.ignoreSameHirachyId = ignoreSameHirachyId;
    }
}
export { ParentBusinessUnitFilter };