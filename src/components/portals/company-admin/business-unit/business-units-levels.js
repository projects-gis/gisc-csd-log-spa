import ko from "knockout";
import templateMarkup from "text!./business-units-levels.html";
import ScreenBase from "../../screenbase";
import * as Screen from "../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestBusinessUnitLevel from "../../../../app/frameworks/data/apicore/webRequestBusinessUnitLevel";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../app/frameworks/constant/defaultSorting";
import ScreenHelper from "../../screenhelper";

class BusinessUnitLavelsScreen extends ScreenBase {

    /**
     * Creates an instance of BusinessUnitLavelsScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitLavelsScreen
     */
    constructor(params) {
        super(params);
        // this.mode = this.ensureNonObservable(params.mode);
        this.bladeTitle(this.i18n("BU_BusinessUnitsLevels")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.Medium;
        this.selectedBusinessUnitLevel = ko.observable();
        this.BusinessUnitDetailData = ko.observable(null);
        this.btnCommand = null;
        this._selectedBusinessUnitLevel = (data) => {
            if(data) {
                return this.navigate("ca-business-units-levels-manage", {data , mode: Screen.SCREEN_MODE_UPDATE});
            }
            return false;
        };

    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnitLevel() {
        return WebRequestBusinessUnitLevel.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

    }

    /**
     * 
     * @memberOf BusinessUnitLavelsScreen
     */
    setupExtend() {

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {

    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(commands){
            this.btnCommand = commands;
        }
    }

    onDatasourceRequestRead(gridOption) {
        var dfd = $.Deferred();
        var filter = Object.assign({}, this.BusinessUnitDetailData(), gridOption);

        this.webRequestBusinessUnitLevel.listBusinessUnitLevelSummary(this.defaultFilter).done((r) => {                
            let items = r["items"];
            this.BusinessUnitDetailData(items);
            dfd.resolve({
                items: items,
                totalRecords: r.totalRecords
            });
            
            let cmdAction = [];
            this.totalRecords = r.totalRecords;

            if (this.totalRecords < 5 && WebConfig.userSession.hasPermission(Constants.Permission.CreateBusinessUnit)) {
                cmdAction.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
            }
            this.btnCommand.replaceAll(cmdAction);
            this.buildCommandBar();
        }).fail((e) => {
                this.handleError(e);
        });
        
        this.subscribeMessage("ca-business-unit-level-changed", (info) => {
            this.refreshShipmentList();
        });

        // this.subscribeMessage("ca-business-unit-level-deleted", (id) => {
        //     this.refreshShipmentList();
        // });

        return dfd;
    }

    refreshShipmentList() {
        this.dispatchEvent("dgBusinessUnitLevel", "refresh");
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdCreate") {
            let totalRecords = this.totalRecords;
            this.navigate("ca-business-units-levels-manage", {
                totalRecords ,
                mode: Screen.SCREEN_MODE_CREATE
            });
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BusinessUnitLavelsScreen),
    template: templateMarkup
};