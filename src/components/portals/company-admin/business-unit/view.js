﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import * as Screen from "../../../../app/frameworks/constant/screen";
import ScreenBase from "../../screenbase";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Constants, EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";

/**
 * 
 * 
 * @class BusinessUnitViewScreen
 * @extends {ScreenBase}
 */
class BusinessUnitViewScreen extends ScreenBase {
    /**
     * Creates an instance of BusinessUnitViewScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitViewScreen
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("BU_ViewBusinessUnitHierarchy")());

        this.businessUnitId = this.ensureNonObservable(params.id);
        this.name = ko.observable("");
        this.code = ko.observable("");
        this.description = ko.observable("");
        this.parent = ko.observable("");
        this.managerName = ko.observable("");
        this.resources = ko.observableArray([]);
        this.listAuthorizedPersons = ko.observableArray([]);

        // Subscribe Message when business unit created/updated
        this.subscribeMessage("ca-business-unit-changed", () => {
            this.isBusy(true);

            this.webRequestBusinessUnit.getBusinessUnit(this.businessUnitId, [EntityAssociation.BusinessUnit.ParentBusinessUnit, EntityAssociation.BusinessUnit.ResourceSummary]).done((businessUnit)=> {

                this.listAuthorizedPersons.replaceAll([]);

                this.name(businessUnit.name);
                this.code(businessUnit.code);
                this.description(businessUnit.description);
                this.parent(businessUnit.parentBusinessUnitPath);
                this.managerName(businessUnit.managerName);
                this.listAuthorizedPersons.replaceAll(businessUnit.authorizedPersonInfos);
                this.resources.replaceAll(this.prepareResourcesDataSource(businessUnit.formatTotalUsers, businessUnit.formatTotalBoxes, businessUnit.formatTotalDrivers, businessUnit.formatTotalVehicles));
            
                this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this._selectingRowHandler = (resource) => {
            if(resource && (Screen.SCREEN_MODE_READONLY !== this.bladeMode)) {
                switch (resource.id) {
                    case 1: //Users
                        return this.navigate("ca-user", {
                            businessUnitId: this.businessUnitId
                        }, true);
                    case 2: //Boxes
                        return this.navigate("ca-asset-box", {
                            businessUnitId: this.businessUnitId
                        }, true);
                    case 3: //Drivers
                        return this.navigate("ca-asset-driver", {
                            businessUnitId: this.businessUnitId
                        }, true);
                    case 4: //Vehicles
                        return this.navigate("ca-asset-vehicle", {
                            businessUnitId: this.businessUnitId
                        }, true);
                }
            }
            return false;
        };
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestBusinessUnit.getBusinessUnit(this.businessUnitId, [EntityAssociation.BusinessUnit.ParentBusinessUnit, EntityAssociation.BusinessUnit.ResourceSummary]).done((businessUnit)=> {
            if(businessUnit){
                this.name(businessUnit.name);
                this.code(businessUnit.code);
                this.description(businessUnit.description);
                this.parent(businessUnit.parentBusinessUnitPath);
                this.resources(this.prepareResourcesDataSource(businessUnit.formatTotalUsers, businessUnit.formatTotalBoxes, businessUnit.formatTotalDrivers, businessUnit.formatTotalVehicles));
                this.managerName(businessUnit.managerName);
                this.listAuthorizedPersons(businessUnit.authorizedPersonInfos);
            }
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateBusinessUnit)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteBusinessUnit)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-business-units-hierarchy-manage", { mode: Screen.SCREEN_MODE_UPDATE, businessUnitId: this.businessUnitId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M024")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            this.webRequestBusinessUnit.deleteBusinessUnit(this.businessUnitId).done(() => {
                                this.isBusy(false);

                                this.publishMessage("ca-business-unit-deleted");
                                this.close(true);
                            }).fail((e)=> {
                                this.isBusy(false);

                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }
    /**
     * 
     * 
     * @param {any} totalUsers
     * @param {any} totalBoxes
     * @param {any} totalDrivers
     * @param {any} totalVehicles
     * 
     * @memberOf BusinessUnitViewScreen
     */
    prepareResourcesDataSource(totalUsers, totalBoxes, totalDrivers, totalVehicles) {
        var ds = [];
        ds.push({ id: 1, resourceType: this.i18n("Common_Users")(), total: totalUsers});
        ds.push({ id: 2, resourceType: this.i18n("Common_Boxes")(), total: totalBoxes});
        ds.push({ id: 3, resourceType: this.i18n("Common_Drivers")(), total: totalDrivers});
        ds.push({ id: 4, resourceType: this.i18n("Common_Vehicles")(), total: totalVehicles});
        return ds;
    }
}

export default {
    viewModel: ScreenBase.createFactory(BusinessUnitViewScreen),
    template: templateMarkup
};