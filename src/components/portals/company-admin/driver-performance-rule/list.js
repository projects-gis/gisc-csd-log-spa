﻿import ko from "knockout";
import ScreenBase from "../../screenBase";
import templateMarkup from "text!./list.html";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../app/frameworks/constant/screen";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import DriverPerformanceRuleSearchInfo from "./search-info";

import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestDriveRule from "../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";

/**
 * List all Driver Performance Rules
 * 
 * @class DriverPerformanceRuleList
 * @extends {ScreenBase}
 */
class DriverPerformanceRuleList extends ScreenBase {

    /**
     * Creates an instance of DriverPerformanceRuleList.
     */
    constructor(params) {
        super(params);

        // Blade properties.
        this.bladeTitle(this.i18n("DriverPerformanceRules_DriverPerformanceRules")());
        this.bladeSize = BladeSize.Medium;

        // View model properties.
        this.selectedItem = ko.observable(); 
        this.searchText = ko.observable("");
        this.recentChangedRowIds = ko.observableArray();
        this.order = ko.observable([[ 1, "asc" ]]);
        this.items = ko.observableArray([]);

        // Service properties.
        this.webRequestDriveRule = WebRequestDriveRule.getInstance();
        this.searchFilter = ko.observable(new DriverPerformanceRuleSearchInfo());
        this.searchFilterIsActive = ko.pureComputed(() => {
            return this.searchFilter().isActive();
        });

        // Event aggregator properties.
        this._subscribeSearchRef = this.subscribeMessage("driver-performance-rule-search-changed", (searchFilter) => {
            this.searchFilter(searchFilter);
            this.refreshDataSource();
        });

        this._subscribeDeleteRef = this.subscribeMessage("ca-driver-performance-rule-deleted", () => {
            this.refreshDataSource();
        });

        this._subscribeChangeRef = this.subscribeMessage("ca-driver-performance-rule-changed", (createdId) => {
            this.refreshDataSource(createdId);
        });
    }

    /**
     * Refresh datasource of datatable.
     */
    refreshDataSource(createdId) {
        this.isBusy(true);
        this.webRequestDriveRule.listDriveRuleSummary(this.searchFilter())
        .done((r) => {
            this.items.replaceAll(r.items);

            if(createdId) {
                this.recentChangedRowIds.replaceAll([createdId]);
            }
            else {
                this.recentChangedRowIds.replaceAll([]);
            }
        })
        .fail((e) => {
            this.handleError(e);
        })
        .always(() => {
            this.isBusy(false);
        });
    }

    /**
     * Handle screen loading.
     * 
     * @param {any} isFirstLoad
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        // Invoke service for list all drive rule summary.
        this.webRequestDriveRule.listDriveRuleSummary(this.searchFilter())
        .done((result) => {
            this.items(result.items);
            dfd.resolve();
        })
        .fail((e)=>{
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Handle when screen is unload
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedItem(null);
    }

    /**
     * Life cycle: create command bar
     * @public
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateDriverPerformanceRule)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.searchFilterIsActive));
    }
    
    /**
     * Life cycle: handle command click
     * @public
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdCreate":
                this.navigate("ca-driver-performance-rule-manage", {mode: Screen.SCREEN_MODE_CREATE});
                break;
            case "cmdSearch":
                this.navigate("ca-driver-performance-rule-search", {filterState: this.searchFilter()});
                break;
        }
    }

    /**
     * Handle to viewing readonly mode when user clicks on table.
     * 
     * @param {any} item
     * @returns
     */
    selectingRowHandler(item) {
        return this.navigate("ca-driver-performance-rule-view", {
            id: item.id
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(DriverPerformanceRuleList),
    template: templateMarkup
};