﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../screenbase";
import WebRequestAlertConfiguration from "../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import WebRequestDriveRule from "../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {Constants, EntityAssociation} from "../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import RegulationInfo from "./regulation-info";
import DefaultSoring from "../../../../app/frameworks/constant/defaultSorting";

class ViewDriverPerformanceRule extends ScreenBase {
    constructor(params) {
        super(params);

        // Blade properties.
        this.bladeTitle(this.i18n("DriverPerformanceRules_ViewDriverPerformanceRule")());
        this.bladeSize = BladeSize.XLarge;

        // Form properties.
        this.driveRuleId = this.ensureNonObservable(params.id, -1);
        this.driveRuleName = ko.observable("");
        this.driveRuleDescription = ko.observable("");
        this.driveRuleEnabled = ko.observable("");
        this.driveRuleAmberScoreRange = ko.observable("");
        this.driveRuleRegulations = ko.observableArray();
        this.alertConfigDS = ko.observableArray();

        // Service properties.
        this.webRequestDriveRule = WebRequestDriveRule.getInstance();
        this.webRequestAlertConfiguration = WebRequestAlertConfiguration.getInstance();
        this.alertConfigurationFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSoring.DrivePerformanceRuleAlertConfiguration,
            includeAssociationNames: [
                EntityAssociation.AlertConfiguration.CustomPOI,
                EntityAssociation.AlertConfiguration.CustomPOICategory,
                EntityAssociation.AlertConfiguration.CustomArea,
                EntityAssociation.AlertConfiguration.CustomAreaCategory,
                EntityAssociation.AlertConfiguration.CustomAreaType,
                EntityAssociation.AlertConfiguration.Feature
            ],
            includeIds: []
        };

        // Event aggregator properties.
        this._subscribeChangesRef = this.subscribeMessage("ca-driver-performance-rule-changed", () => {
            this.isBusy(true);

            this.webRequestDriveRule.getDriveRule(this.driveRuleId, [EntityAssociation.DriverPerformanceRule.DriveRuleRegulations])
            .done((data) => {
                this.populateModel(data);
            })
            .fail((e) => {
                this.handleError(e);
            })
            .always(() => {
                this.isBusy(false);
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestDriveRule.getDriveRule(this.driveRuleId, [EntityAssociation.DriverPerformanceRule.DriveRuleRegulations])
        .done((driveRule) => {
            var ambScore = "";
            if(driveRule.amberMinScore == null && driveRule.amberMaxScore == null){
                ambScore = "-"
            }else{
                ambScore = `${driveRule.amberMinScore} - ${driveRule.amberMaxScore}`;
            }
            this.driveRuleAmberScoreRange(ambScore)
            // Find all alert configuration ids for alert configuration filter.
            driveRule.driveRuleRegulations.forEach((r) => {
                this.alertConfigurationFilter.includeIds.push(r.alertConfigurationId);
            });

            // Load all alert configuration filter.
            this.webRequestAlertConfiguration.listAlertConfiguration(this.alertConfigurationFilter)
            .done((alertConfig) => {

                this.alertConfigDS(alertConfig.items);
                this.populateModel(driveRule);
                dfd.resolve();
            })
            .fail((e) => {
                dfd.reject(e);
            });
        })
        .fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateDriverPerformanceRule)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteDriverPerformanceRule)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch(sender.id) {
            case "cmdUpdate":
                this.navigate("ca-driver-performance-rule-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.driveRuleId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M054")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            this.webRequestDriveRule.deleteDriveRule(this.driveRuleId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("ca-driver-performance-rule-deleted");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }

    /**
     * Populate drive rule model to page.
     * 
     * @param {any} driveRule
     */
    populateModel(driveRule) {
        if(!driveRule) {
            return;
        }

        this.driveRuleName(driveRule.name);
        this.driveRuleDescription(driveRule.description);
        this.driveRuleEnabled(driveRule.formatEnable);
        var ambScore = "";
        if(driveRule.amberMinScore == null && driveRule.amberMaxScore == null){
            ambScore = "-"
        }else{
            ambScore = `${driveRule.amberMinScore} - ${driveRule.amberMaxScore}`;
        }
        this.driveRuleAmberScoreRange(ambScore)
        // Prepare mapping of alert configuration name.
        var alertCnfgNames = {};
        this.alertConfigDS().forEach((c) => {
            alertCnfgNames[c.id] = c.displayName;
        });
        // Transform service regulation to flat regulation.
        var flatRegualtions = [];

        driveRule.driveRuleRegulations.forEach((r) => {

            var newRegulation = new RegulationInfo(r);

            // Find alert configuration name from mapping.
            newRegulation.displayName = alertCnfgNames[r.alertConfigurationId];

            newRegulation.displayadvanceScore = r.advanceScore

            flatRegualtions.push(newRegulation);
        });

        this.driveRuleRegulations(flatRegualtions);
    }
}

export default {
    viewModel: ScreenBase.createFactory(ViewDriverPerformanceRule),
    template: templateMarkup
};