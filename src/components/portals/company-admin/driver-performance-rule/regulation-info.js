import {Enums} from "../../../../app/frameworks/constant/apiConstant";
import Utility from '../../../../app/frameworks/core/utility';

class RegulationInfo {
    constructor(params) {
        this.clientId = Utility.getUniqueId();
        this.id = null;
        this.driveRuleId = -1;
        this.alertConfigurationId = null;
        this.alertTypeDisplayName = null; // Custom property for view mode.
        this.alertConfigurations = [];
        this.infoStatus = Enums.InfoStatus.Add;
        this.error = null;
        this.advanceScore = null;

        // Custom flat grades.
        this.F = new GradeInfo(Enums.ModelData.DriveGrade.F);
        this.D = new GradeInfo(Enums.ModelData.DriveGrade.D);
        this.C = new GradeInfo(Enums.ModelData.DriveGrade.C);
        this.B = new GradeInfo(Enums.ModelData.DriveGrade.B);
        this.A = new GradeInfo(Enums.ModelData.DriveGrade.A);
        this.driveRuleGrades = [this.F, this.D, this.C, this.B, this.A];

        // Populate data if available.
        this.populateData(params);

        // Attach validation method.
        this.isValid = () => {
            var validationResult = !_.isEmpty(this.alertConfigurationId) &&
            !_.isEmpty(this.advanceScore) &&
                this.F.isValidRange() &&
                this.D.isValidRange() &&
                this.C.isValidRange() &&
                this.B.isValidRange() &&
                this.A.isValidRange();


                console.log(">>>>",!_.isEmpty(this.advanceScore));
            return validationResult;
        };
    }

    /**
     * Pouplate property from master object.
     * @param {any} params
     */
    populateData(params) {
        if(params) {
            this.id = params.id;
            this.driveRuleId = params.id;
            this.alertConfigurationId = params.alertConfigurationId;
            this.infoStatus = params.infoStatus;
            this.advanceScore = params.advanceScore;

            params.driveRuleGrades.forEach((r) => {
                var currentGrade = null;

                switch(r.grade) {
                    case Enums.ModelData.DriveGrade.F:
                        currentGrade = this.F;
                        break;
                    case Enums.ModelData.DriveGrade.D:
                        currentGrade = this.D;
                        break;
                    case Enums.ModelData.DriveGrade.C:
                        currentGrade = this.C;
                        break;
                    case Enums.ModelData.DriveGrade.B:
                        currentGrade = this.B;
                        break;
                    case Enums.ModelData.DriveGrade.A:
                        currentGrade = this.A;
                        break;
                }

                if(currentGrade) {
                    currentGrade.id = r.id;
                    currentGrade.min = r.min;
                    currentGrade.max = r.max;
                    currentGrade.formatMin = r.formatMin;
                    currentGrade.formatMax = r.formatMax;
                }
            });
        }
    }
}

class GradeInfo {
    constructor(grade) {
        this.id = null;
        this.min = 0;
        this.max = 0;
        this.grade = grade;
        this.error = null;
        this.formatMin = 0;
        this.formatMax = 0;
    }
    isValidRange() {
        return !(isNaN(this.min) || isNaN(this.max) || this.min > this.max);
    }
    continueFrom(other, expectedComparison = 1) {
        var isContinue = false;

        var currentComparison = this.compareTo(other);
        if(currentComparison === expectedComparison) {
            // Going same direction then evaluate value.
            switch (currentComparison) {
                case -1: // DESC
                    isContinue = ((other.min -1) === this.max);
                    break;
                case 1: // ASC
                    isContinue = ((other.max +1) === this.min);
                    break;
            }
        }
        else {
            // Invalid direction then return false by default.
        }

        return isContinue;
    }
    compareTo(other) {
        // Checking direction of max.
        if(this.max > other.max) {
            return 1;
        }
        else if (this.max < other.max){
            return -1;
        }
        else {
            return 0;
        }
    }
    toString() {
        return this.formatMin + " - " + this.formatMax;
    }
}

export default RegulationInfo;