﻿import ko from "knockout";
import ScreenBase from "../../../screenBase";
import templateMarkup from "text!./speed.html";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import DriverPerformanceRuleSearchInfo from "../search-info";

import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriveRule";
import Utility from '../../../../../app/frameworks/core/utility';

/**
 * 
 * @class AdvanceDriverPerformanceRuleList
 * @extends {ScreenBase}
 */
class AdvanceDriverPerformanceRuleSpeed extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = this.ensureNonObservable(params.mode);
        this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_Speed")());
        this.bladeSize = BladeSize.Medium;
        this.speedInfosVal = this.ensureNonObservable(_.cloneDeep(params.speedInfos), []);
        this.speedInfos = ko.observableArray([]);

        if (_.size(this.speedInfosVal) == 0) {
            this.speedInfos([
                { id: 1, bestSpeedRange: false, speedFrom: 0, speedTo: 20},
                { id: 2, bestSpeedRange: false, speedFrom: 21, speedTo: 40, canDelete: true },
                { id: 3, bestSpeedRange: true, speedFrom: 41, speedTo: 60, canDelete: true },
                { id: 4, bestSpeedRange: false, speedFrom: 61, speedTo: 80, canDelete: true },
                { id: 5, bestSpeedRange: false, speedFrom: 81, speedTo: 100, canDelete: true },
                { id: 6, bestSpeedRange: false, speedFrom: 101, speedTo: 120, canDelete: true },
                { id: 7, bestSpeedRange: false, disableSpan: true, speedTo: 121 }
            ]);
        }
        else {
            this.speedInfosVal[_.findLastIndex(this.speedInfosVal)].disableSpan = true;
            this.speedInfos(this.speedInfosVal);
        }
        this.valueRadio = ko.observable();
        this.validationModel = ko.validatedObservable({});
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        var dfd = $.Deferred();

        dfd.resolve();

        return dfd;
    }

    /**
     * Handle when user close page or hide.
     */
    onUnload() {
        this.speedInfosVal = [];
        this.speedInfos([]);
    }

    setupExtend() {
        this.speedInfos.extend({
            trackArrayChange: true
        });
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel("actCancel", this.i18n("Common_Cancel")()));
    }



    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {

            let M065Str = this.i18n("M065")();
            let invalidScore = false;
            this.speedInfos().forEach((item) => {
                if (item.speedFrom && item.speedTo != undefined) {
                    if (this.isValidRange(item.speedFrom, item.speedTo)) {
                        item.error = null;
                    }
                    else {
                        item.error = M065Str;
                        invalidScore = true;
                    }
                }
            });

            // Load Speed Info ใหม่ ทุกครั้ง
            let tmpSpeedInfos = _.clone(this.speedInfos());
            this.speedInfos([]);
            this.speedInfos.replaceAll(tmpSpeedInfos);

            if (invalidScore) {
                return;
            }

            this.publishMessage("ca-advance-driver-rule-speed", this.speedInfos());
            this.close(true);
        }

    }

    onRemovable(id) {

        let tmpSpeed = _.clone(this.speedInfos());
        this.speedInfos([]);

        let previousMaxScore = 0;
        for (var i = 0; i < tmpSpeed.length; i++) {

            if (i == 0) {
                previousMaxScore = tmpSpeed[0].speedTo;
                continue;
            }
            else if (i == (tmpSpeed.length - 1)) {
                tmpSpeed[i].speedTo = previousMaxScore + 1;
                break;
            }
            tmpSpeed[i].speedFrom = previousMaxScore + 1;
            previousMaxScore = tmpSpeed[i].speedTo;
        }

        this.speedInfos.replaceAll(tmpSpeed);

        let findSelected = _.filter(this.speedInfos(), (item) => { return item.bestSpeedRange == true });
        if (findSelected.length == 0) {
            let tmpData = _.clone(this.speedInfos());
            this.speedInfos([]);
            tmpData[0].bestSpeedRange = true;
            this.speedInfos.replaceAll(tmpData);
        }
    }

    onDataSourceChanged(data) {

        let tmpSpeed = _.clone(this.speedInfos());
        this.speedInfos([]);

        let previousMaxScore = 0;
        for (var i = 0; i < tmpSpeed.length; i++) {

            if (tmpSpeed[i].id != data.id && data.bestSpeedRange == true) {
                tmpSpeed[i].bestSpeedRange = false;
            }

            if (i == 0) {
                previousMaxScore = tmpSpeed[0].speedTo;
                continue;
            }
            else if (i == (tmpSpeed.length - 1)) {
                tmpSpeed[i].speedTo = previousMaxScore + 1;
                break;
            }
            tmpSpeed[i].speedFrom = previousMaxScore + 1;
            previousMaxScore = tmpSpeed[i].speedTo;
        }

        let findSelected = _.filter(tmpSpeed, (item) => { return item.bestSpeedRange == true });
        if (findSelected.length == 0) {
            let currIndex = _.indexOf(tmpSpeed, data);
            tmpSpeed[currIndex].bestSpeedRange = true;
        }

        this.speedInfos.replaceAll(tmpSpeed);
    }

    addSpeed() {
        let lastIndex = _.findLastIndex(this.speedInfos());
        let lastId = _.maxBy(this.speedInfos(),'id').id;
        let tmpSpeedInfos = _.clone(this.speedInfos());
        this.speedInfos([]);


        let lastItem = _.clone(tmpSpeedInfos[lastIndex]);
        let removed = tmpSpeedInfos.splice((lastIndex), 1); // remove row สุดท้ายออก
        let lastTmpSpeedInfo = _.last(tmpSpeedInfos);

        var newSpeed = {
            bestSpeedRange : false,
            speedFrom: lastTmpSpeedInfo.speedTo+1,
            speedTo: 0,
            error : null,
            canDelete: true ,
            id: lastId + 1
        };

        tmpSpeedInfos.push(newSpeed);
        tmpSpeedInfos.push(lastItem);

        this.speedInfos.replaceAll(tmpSpeedInfos);
    }

    isValidRange(speedFrom,speedTo) {
        return !(isNaN(speedFrom) || isNaN(speedTo) || speedFrom >= speedTo);
    }

}



export default {
    viewModel: ScreenBase.createFactory(AdvanceDriverPerformanceRuleSpeed),
    template: templateMarkup
};