﻿import ko from "knockout";
import templateMarkup from "text!./behavior.html";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import { EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestAdvanceDriveRule from "../../../../../app/frameworks/data/apitrackingcore/webRequestAdvanceDriveRule";
import WebRequestAlertConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
import ScreenBase from "../../../screenBase";

/**
 * 
 * @class AdvanceDriverPerformanceRuleList
 * @extends {ScreenBase}
 */
class AdvanceDriverPerformanceRuleBehavior extends ScreenBase {
    constructor(params) {
        super(params);
        this.mode = this.ensureNonObservable(params.mode);
        this.advanceDriveRuleId = this.ensureNonObservable(params.advanceDriveRuleId,-1);
        this.paramsBehaviorInfos = this.ensureNonObservable(params.behaviorInfos, []);

        this.bladeTitle(this.i18n("AdvanceDriverPerformanceRules_Behavior")());
        this.bladeSize = BladeSize.Medium;
        this.behaviorInfos = ko.observableArray([]);

        if (_.size(this.paramsBehaviorInfos)) {
            this.behaviorInfos(_.cloneDeep(this.paramsBehaviorInfos));
        }

        

        this.valueRadio = ko.observable();
        this.alertConfigDS = ko.observableArray();
        this.visibleAdd = ko.pureComputed(() => {
            return _.size(this.behaviorInfos()) >= 10 ? false : true;
        });

        this.validationModel = ko.validatedObservable({});
    }

    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestAlertConfiguration() {
        return WebRequestAlertConfiguration.getInstance();
    }

    get webRequestAdvanceDriveRule() {
        return WebRequestAdvanceDriveRule.getInstance();
    }

    setupExtend() {
        this.behaviorInfos.extend({
            trackArrayChange: true
        });

        this.validationModel({
            behaviorInfos: this.behaviorInfos
        });
    }

    /**
    * Handle load screen.
    * @public
    * @param {any} isFirstLoad
    */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var dfdDriveRuleLoader = $.Deferred();
        var alertConfigurationFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            sortingColumns: DefaultSoring.DrivePerformanceRuleAlertConfiguration,
            includeAssociationNames: [
                EntityAssociation.AlertConfiguration.CustomPOI,
                EntityAssociation.AlertConfiguration.CustomPOICategory,
                EntityAssociation.AlertConfiguration.CustomArea,
                EntityAssociation.AlertConfiguration.CustomAreaCategory,
                EntityAssociation.AlertConfiguration.CustomAreaType,
                EntityAssociation.AlertConfiguration.Feature
            ],
            includeIds: [],
            enable: true
        };


        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                // Do nothing for loading drive rule.
                dfdDriveRuleLoader.resolve({});
                break;
            case Screen.SCREEN_MODE_UPDATE:
                // Loading drive rule then continue load alert configuration.
                this.webRequestAdvanceDriveRule.getAdvanceDriverRule(this.advanceDriveRuleId)
                    .done((driveRule) => {
                        // Find all alert configuration ids for alert configuration filter.
                        driveRule.advanceDriverRuleBehaviors.forEach((r) => {
                            alertConfigurationFilter.includeIds.push(r.alertConfigurationId);
                        });
                        dfdDriveRuleLoader.resolve(driveRule);
                    })
                    .fail((e) => {
                        dfdDriveRuleLoader.reject(e);
                        dfd.reject(e);
                    });
                break;
        }

        $.when(dfdDriveRuleLoader)
            .done(() => {
                // Load all alert configuration filter.
                this.webRequestAlertConfiguration.listAlertConfiguration(alertConfigurationFilter)
                    .done((alertConfig) => {
                        this.alertConfigDS(alertConfig.items);
                        this.populateModel();
                        dfd.resolve();
                    })
                    .fail((e) => {
                        dfd.reject(e);
                    });
            });

        return dfd;
    }


    populateModel() {
        if (_.size(this.behaviorInfos())) {
            // Transform service regulation to flat regulation.
            var flatRegualtions = [];

            // Prepare datasoruce of available alert configuration dropdown.
            this.behaviorInfos().forEach((r, index) => {
                r.alertConfigurations = this.alertConfigDS();
                r.canDelete = index == 0 ? false : true;
                r.enable = true;
                flatRegualtions.push(r);
            });

            this.behaviorInfos(flatRegualtions);
        }

        else {
            this.behaviorInfos([
                { id: 1, penaltyScore: null, alertConfigurations: this.alertConfigDS(), canDelete: false, enable: true }
            ]);
        }
    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel("actCancel", this.i18n("Common_Cancel")()));
    }



    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id == "actSave") {

            let inValid = false;
            let M001Str = this.i18n("M001")(); //required
            let M097Str = this.i18n("M097")(); //dupicate
            let M070Str = this.i18n("M070")(); //invalid
            let alertIds = [];

            this.behaviorInfos().forEach((item) => {
                // Check AlertConfiguration
                let alertId = parseInt(item.alertConfigurationId); //Check Require
                if (_.isNaN(alertId)) {
                    item.error = M001Str;
                    item.showVirtualError = true;
                    inValid = true;
                }
                else {
                    item.error = null;
                    item.showVirtualError = null; // ใส่ไว้เพื่อให้ ตอน require ทำให้ layout เพี้ยน

                    if (alertIds.find((x) => { return x == alertId }) != undefined) { //Check Dupicate
                        item.error = M097Str;
                        item.showVirtualError = true;
                        inValid = true;
                    }
                    else {
                        item.error = null;
                        item.showVirtualError = null;
                        alertIds.push(alertId);
                    }
                   
                }

                // Check PenaltyScore
                if (_.isNil(item.penaltyScore) || item.penaltyScore == "") { //Check PenaltyScore is empty string or null
                    item.errorScore = M001Str;
                    inValid = true;
                }
                else {
                    item.errorScore = null;

                    let penaltyScore = parseInt(item.penaltyScore); // check parse PenaltyScore
                    if (_.isNaN(penaltyScore)) {
                        inValid = true;
                        item.errorScore = M070Str;
                    }
                    else {
                        item.errorScore = null;
                        if (penaltyScore > 100) { // don't allow PenaltyScore more than 100 
                            inValid = true;
                            item.errorScore = M070Str;
                        }
                    }
                }
                

            });

            let tmpItems = _.clone(this.behaviorInfos());
            this.behaviorInfos([]);
            this.behaviorInfos.replaceAll(tmpItems);

            if (inValid) return;

            this.publishMessage("ca-advance-driver-rule-behavior", this.behaviorInfos());
            this.close(true);
        }
    }



    addBehavior() {
        let maxId = _.maxBy(this.behaviorInfos(), 'id').id;
        let newAlert = {
            id: maxId+1,
            penaltyScore: null,
            alertConfigurations: this.alertConfigDS(),
            canDelete: true,
            enable: true,
            displayName:''
        };

        this.behaviorInfos.push(newAlert);
    }


    onDataSourceChanged(data) {
        let tmpItem = _.clone(this.behaviorInfos());
        this.behaviorInfos([]);

        tmpItem.forEach((item) => {
            item.error = null;
            item.showVirtualError = false;

            let scoreVal = parseFloat(item.penaltyScore);
            if (isNaN(scoreVal) || scoreVal <= 0) {
                item.penaltyScore = "";
            }
            else {
                item.errorScore = null;
                if (scoreVal > 100) {
                    item.penaltyScore = "";
                }
                else {
                    item.penaltyScore = _.round(scoreVal, 2);
                }
            }
        });

        this.behaviorInfos.replaceAll(tmpItem);
    }
}



export default {
    viewModel: ScreenBase.createFactory(AdvanceDriverPerformanceRuleBehavior),
    template: templateMarkup
};