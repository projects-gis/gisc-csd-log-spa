﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceTypeManage extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        this.id = this.ensureNonObservable(params.id);
        this.bladeSize = BladeSize.Medium;

        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Vehicle_MA_Create_Maintenance_Type")());
        }
        else {
            this.bladeTitle(this.i18n("Vehicle_MA_Update_Maintenance_Type")());
        }

        this.itemsCheckList = ko.observableArray([]);
        this.itemsAction = ko.observableArray([]);

        this.txtName = ko.observable();
        this.txtCode = ko.observable();
        this.txtDesc = ko.observable();

        this.order = ko.observable([]);
        this.selectedItem = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);

        this.oldItemsCheckList = ko.observableArray([]);
        this.oldItemsAction = ko.observableArray([]);

        this.isCheckListInvalidChecklist = ko.observable();
        this.isCheckListInvalidChecklist(false);
        this.isCheckListInvalidActions = ko.observable();
        this.isCheckListInvalidActions(false);

        this.isCheckListDuplicatedChecklist = ko.observable();
        this.isCheckListDuplicatedChecklist(false);

        this.isCheckListDuplicatedActions = ko.observable();
        this.isCheckListDuplicatedActions(false);

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-import", (value) => {
            for (var i = 0; i < value.data.length; i++) {
                this.itemsCheckList.push({
                    id: this.itemsCheckList().length + 1,
                    name: value.data[i].listName
                });
            }

        });
    }

    setupExtend() {
        this.txtName.extend({ trackChange: true });
        this.txtCode.extend({ trackChange: true });
        this.txtDesc.extend({ trackChange: true });
        this.itemsCheckList.extend({ trackChange: true });
        this.itemsAction.extend({ trackChange: true });
        this.isCheckListInvalidChecklist.extend({ trackChange: true });
        this.isCheckListInvalidActions.extend({ trackChange: true });
        this.isCheckListDuplicatedChecklist.extend({ trackChange: true });
        this.isCheckListDuplicatedActions.extend({ trackChange: true });

        this.txtName.extend({ required: true });
        this.txtCode.extend({ required: true });

        this.txtName.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.txtCode.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });



        let objValidate = null;

        objValidate = {
            txtName: this.txtName,
            txtCode: this.txtCode
        }

        this.validationModel = ko.validatedObservable(objValidate);
    }

    addNewItemsCheckList() {
        this.itemsCheckList.push({
            id: this.itemsCheckList().length + 1,
            name: ""
        });
    }

    addNewItemsAction() {
        this.itemsAction.push({
            id: this.itemsAction().length + 1,
            code: "",
            name: ""
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        if (this.mode == Screen.SCREEN_MODE_UPDATE) {
            this.webRequestVehicleMaintenanceType.getVehicleMaintenanceType(this.id).done((result) => {
                if (result) {
                    this.txtName(result["name"]);
                    this.txtCode(result["code"]);
                    this.txtDesc(result["description"]);
                    this.itemsCheckList(result["checkListMasters"]);
                    this.itemsAction(result["actions"]);
                    this.oldItemsCheckList(JSON.parse(JSON.stringify(result["checkListMasters"])));
                    this.oldItemsAction(JSON.parse(JSON.stringify(result["actions"])));
                }
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            });
        }
        else {
            dfd.resolve();
        }

        return dfd;
    }


    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            commands.push(this.createCommand("cmdImport", this.i18n("Vehicle_MA_Maintenance_Type_Import_Checklist")(), "svg-cmd-import"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            this.isCheckListInvalidChecklist(false);
            this.isCheckListInvalidActions(false);
            this.isCheckListDuplicatedChecklist(false);
            this.isCheckListDuplicatedActions(false);
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            let isCheckListInvalidChecklist = false;
            let isCheckListInvalidAction = false;

            let isDupChecklist = false;
            let isDupAction = false;

            for (let i = 0; i < this.itemsCheckList().length; i++) {
                let currentItem = this.itemsCheckList()[i].name;
                for (let j = 0; j < this.itemsCheckList().length; j++){
                    var checkItem = this.itemsCheckList()[j].name;

                    if(i == j)
                        continue;
                    
                    isDupChecklist  = isDupChecklist || (currentItem == checkItem);
                    //console.log(isDupChecklist);
                        
                }
            }
            //console.log(isDupChecklist);
            this.isCheckListDuplicatedChecklist(isDupChecklist);

            for (let i = 0; i < this.itemsAction().length; i++) {

                let currentItem = this.itemsAction()[i];
                for (let j = 0; j < this.itemsAction().length; j++){
                    let checkItem = this.itemsAction()[j];

                    if(i == j)
                        continue;

                    isDupAction  = isDupAction || ((currentItem.name == checkItem.name) || (currentItem.code == checkItem.code));
                    //console.log(isDupAction);

                }

                //if (this.itemsAction()[i + 1].name == this.itemsAction()[i].name || this.itemsAction()[i + 1].code == this.itemsAction()[i].code) {
                //    this.isCheckListDuplicatedActions(true);
                //    //return;
                //}
            }
            this.isCheckListDuplicatedActions(isDupAction);

            if(this.isCheckListDuplicatedActions() || this.isCheckListDuplicatedChecklist()) {
                return;
            }

            this.itemsCheckList().forEach((f) => {
                if (f.name == null || f.name == "") {
                    isCheckListInvalidChecklist = true;
                }
            });

            this.itemsAction().forEach((f) => {
                if (f.name == null || f.name == "" || f.code == null || f.code == "") {
                    isCheckListInvalidAction = true;
                }
            });

            if (this.itemsCheckList().length > 0) {
                if (this.itemsAction().length == 0) {
                    isCheckListInvalidAction = true;
                }
            }



            if (isCheckListInvalidChecklist || isCheckListInvalidAction) {
                this.isCheckListInvalidChecklist(isCheckListInvalidChecklist);
                this.isCheckListInvalidActions(isCheckListInvalidAction);
                return;
            }

            //if (isCheckListInvalidChecklist) {
            //    this.isCheckListInvalidChecklist(true);
            //    return;
            //}

            //if (isCheckListInvalidAction) {
            //    this.isCheckListInvalidActions(true);
            //    return;
            //}

            this.isBusy(true);

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE: {
                    let model = {
                        "name": this.txtName(),
                        "code": this.txtCode(),
                        "description": this.txtDesc()
                    };

                    model.companyId = WebConfig.userSession.currentCompanyId;
                    model.checkListMasters = this.itemsCheckList();
                    model.actions = this.itemsAction();

                    this.webRequestVehicleMaintenanceType.createVehicleMaintenanceType(model).done((result) => {
                        this.publishMessage("ca-asset-maintenance-management-ma-type-update", result);

                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });

                    break;
                }
                case Screen.SCREEN_MODE_UPDATE:
                    let model = {
                        "name": this.txtName(),
                        "code": this.txtCode(),
                        "description": this.txtDesc()
                    };

                    model.companyId = WebConfig.userSession.currentCompanyId;
                    model.checkListMasters = this.itemsCheckList();
                    model.actions = this.itemsAction();
                    model.id = this.id;

                    var checkListCount = this.itemsCheckList().length;
                    var actionCount = this.itemsAction().length;
                    var oldCheckListCount = this.oldItemsCheckList().length;
                    var oldActionCount = this.oldItemsAction().length;
                    var newChecklist = [];
                    var newAction = [];

                    for (let i = 0; i < checkListCount; i++) {
                        let newId = this.itemsCheckList()[i].id;
                        let newName = this.itemsCheckList()[i].name;
                        let isMatch = $.grep(this.oldItemsCheckList(), (n, m) => {
                            if (n.id == newId) {
                                if (n.name == newName) {
                                    newChecklist.push({
                                        id: newId,
                                        name: newName,
                                        infoStatus: Enums.InfoStatus.Original
                                    })
                                } else {
                                    newChecklist.push({
                                        id: newId,
                                        name: newName,
                                        infoStatus: Enums.InfoStatus.Update
                                    })
                                }
                            } else { }
                            return n.id == newId;
                        });

                        if (isMatch.length == 0) {
                            newChecklist.push({
                                id: newId,
                                name: newName,
                                infoStatus: Enums.InfoStatus.Add
                            })
                        }
                    }

                    for (let i = 0; i < oldCheckListCount; i++) {
                        let oldId = this.oldItemsCheckList()[i].id;
                        let oldName = this.oldItemsCheckList()[i].name;

                        let isMatch = $.grep(this.itemsCheckList(), (n, m) => {
                            return n.id == oldId;
                        });

                        if (isMatch.length == 0) {
                            newChecklist.push({
                                id: oldId,
                                name: oldName,
                                infoStatus: Enums.InfoStatus.Delete
                            })
                        }
                    }

                    for (let i = 0; i < actionCount; i++) {
                        let newId = this.itemsAction()[i].id;
                        let newName = this.itemsAction()[i].name;
                        let newCode = this.itemsAction()[i].code;

                        let isMatch = $.grep(this.oldItemsAction(), (n, m) => {
                            if (n.id == newId) {
                                if (n.name == newName && n.code == newCode) {
                                    newAction.push({
                                        id: newId,
                                        name: newName,
                                        code: newCode,
                                        infoStatus: Enums.InfoStatus.Original
                                    })
                                } else {
                                    newAction.push({
                                        id: newId,
                                        name: newName,
                                        code: newCode,
                                        infoStatus: Enums.InfoStatus.Update
                                    })
                                }
                            } else { }
                            return n.id == newId;
                        });

                        if (isMatch.length == 0) {
                            newAction.push({
                                id: newId,
                                name: newName,
                                code: newCode,
                                infoStatus: Enums.InfoStatus.Add
                            })
                        }
                    }

                    for (let i = 0; i < oldActionCount; i++) {
                        let oldId = this.oldItemsAction()[i].id;
                        let oldName = this.oldItemsAction()[i].name;
                        let oldCode = this.oldItemsAction()[i].code;

                        let isMatch = $.grep(this.itemsAction(), (n, m) => {
                            return n.id == oldId;
                        });

                        if (isMatch.length == 0) {
                            newAction.push({
                                id: oldId,
                                name: oldName,
                                code: oldCode,
                                infoStatus: Enums.InfoStatus.Delete
                            })
                        }
                    }

                    model.checkListMasters = newChecklist;
                    model.actions = newAction;

                    this.webRequestVehicleMaintenanceType.updateVehicleMaintenanceType(model).done((result) => {
                        this.publishMessage("ca-asset-maintenance-management-ma-type-update", result);

                        this.isBusy(false);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });

                    break;
                default:
                    break;
            }

        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdImport":
                this.navigate("ca-asset-maintenance-management-ma-type-manage-import", {});
                break;
            default:
                break;
        }
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceTypeManage),
    template: templateMarkup
};