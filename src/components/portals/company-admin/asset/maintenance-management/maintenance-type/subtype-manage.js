﻿import ko from "knockout";
import templateMarkup from "text!./subtype-manage.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceSubtypeTypeManage extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.id = this.ensureNonObservable(params.id, 0);

        this.bladeSize = BladeSize.Medium;

        this.mode = this.ensureNonObservable(params.mode);
        if (this.mode == Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Vehicle_MA_Create_Maintenance_SubType")());
        }
        else {
            this.bladeTitle(this.i18n("Vehicle_MA_Update_Maintenance_SubType")());
        }

        this.maintenanceTypeId = this.ensureNonObservable(params.maintenanceTypeId);
        this.subtypeName = ko.observable();
        this.subtypeCode = ko.observable();
        this.subtypeDesc = ko.observable();
        this.subtypeParent = ko.observable();
        this.isCheckListInvalid = ko.observable();
        this.isCheckListInvalid(false);
        this.checkList = ko.observableArray([]);

        this.checkListSubItemBeforeUpdate = this.ensureNonObservable([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var dfdMAType = null;



        dfdMAType = this.webRequestVehicleMaintenanceType.getVehicleMaintenanceType(this.maintenanceTypeId).done((result) => {
            if (result) {
                this.subtypeParent(result["name"]);
                var arrCheckList = new Array();
                var actions = result["actions"];

                actions.forEach((x) =>{
                    x.code = x.code + ":" + x.name
                });

                if (this.mode === Screen.SCREEN_MODE_UPDATE) {
                    var dfdMASubType = null;
                    dfdMASubType = this.webRequestVehicleMaintenanceType.getVehicleMaintenanceSubType(this.id, [EntityAssociation.MaintenanceSubType.Checklist]).done((responseSubType) => {
                        this.subtypeName(responseSubType["name"]);
                        this.subtypeCode(responseSubType["code"]);
                        this.subtypeDesc(responseSubType["description"]);
                        this.subtypeParent(responseSubType["maintenanceType"].name);
                        var checkListSubTypes = responseSubType["checkListSubTypes"];

                        for (var i = 0; i < checkListSubTypes.length; i++) {
                            var checkListItem = {
                                subtypeId: checkListSubTypes[i].id,
                                isCheck: checkListSubTypes[i].isCheck,
                                listName: checkListSubTypes[i].listName,
                                id: checkListSubTypes[i].checkListMasterId,
                                selectedAction: checkListSubTypes[i].actionId == 0 ? "" : checkListSubTypes[i].actionId,
                                actions: actions,
                                error: ''
                            };
                            arrCheckList.push(checkListItem);
                        }

                        this.checkListSubItemBeforeUpdate = arrCheckList;
                        this.checkList.replaceAll(arrCheckList);
                    });



                    $.when(dfdMASubType).done(() => {
                        dfdMASubType.resolve();
                    }).fail((e) => {
                        dfdMASubType.reject(e);
                    });
                } else {

                    var checkListMaster = result["checkListMasters"];
                    for (var i = 0; i < checkListMaster.length; i++) {
                        var checkListItem = {
                            subtypeId: 0,
                            isCheck: true,
                            listName: checkListMaster[i].name,
                            id: checkListMaster[i].id,
                            selectedAction: null,
                            actions: actions,
                            error: ''
                        };

                        arrCheckList.push(checkListItem);
                    }
                    this.checkList.replaceAll(arrCheckList);
                }
            }
        });

        $.when(dfdMAType).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {
        this.subtypeName.extend({ trackChange: true });
        this.subtypeCode.extend({ trackChange: true });
        this.subtypeDesc.extend({ trackChange: true });
        this.subtypeParent.extend({ trackChange: true });
        this.isCheckListInvalid.extend({ trackChange: true });

        this.subtypeName.extend({ required: true });
        this.subtypeCode.extend({ required: true });
        this.checkList.extend({
            arrayIsValid: "isValid"
        });

        this.subtypeName.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.subtypeCode.extend({
            serverValidate: {
                params: "Code",
                message: this.i18n("M010")()
            }
        });

        let objValidate = null;

        objValidate = {
            subtypeName: this.subtypeName,
            subtypeCode: this.subtypeCode,
            checkList: this.checkList
        }

        this.validationModel = ko.validatedObservable(objValidate);
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {

            ko.validation.validateObservable(this.checkList);
            this.isCheckListInvalid(false);
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var isCheckListInvalid = false;
            this.checkList().forEach((f) => {
                if (f.isCheck) {
                    if (f.selectedAction == null || f.selectedAction == "") {
                        isCheckListInvalid = true;
                    }
                }
            });

            if (isCheckListInvalid) {
                this.isCheckListInvalid(true);
                return;
            }

            var subtypeModel = this.generateModel();

            this.isBusy(true);
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestVehicleMaintenanceType.createVehicleMaintenanceSubType(subtypeModel, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-maintenance-management-ma-type-view-subtype-create", response);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestVehicleMaintenanceType.updateVehicleMaintenanceSubType(subtypeModel, true).done((response) => {
                        this.isBusy(false);
                        this.publishMessage("ca-asset-maintenance-management-ma-type-view-subtype-update", response);
                        this.close(true);
                    }).fail((e) => {
                        this.handleError(e);
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }

    generateModel() {
        var subtypeModel = {
            id: this.id,
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update,
            name: this.subtypeName(),
            code: this.subtypeCode(),
            description: this.subtypeDesc(),
            maintenanceTypeId: this.maintenanceTypeId
        };

        var checkListSubTypes = new Array();


        //this.checkListSubItemBeforeUpdate;

        var currentIndex = 0;
        this.checkList().forEach((f) => {


            if (this.mode === Screen.SCREEN_MODE_CREATE && f.isCheck){
                var checkListItem = {
                    id: f.subtypeId,
                    actionId: f.selectedAction,
                    checkListMasterId: f.id,
                    isCheck: f.isCheck,
                    listName: f.listName,
                    infoStatus: Enums.InfoStatus.Add
                };

                checkListSubTypes.push(checkListItem);
            }
            else {
                if (this.checkListSubItemBeforeUpdate.length > 0){
                    if (this.checkListSubItemBeforeUpdate[currentIndex].isCheck || f.isCheck) {

                        var infoStatus = null;
                        if (this.checkListSubItemBeforeUpdate[currentIndex].isCheck && f.isCheck) {
                            infoStatus = Enums.InfoStatus.Update;
                        }
                        else if (!this.checkListSubItemBeforeUpdate[currentIndex].isCheck && f.isCheck) {
                            infoStatus = Enums.InfoStatus.Add;
                        }
                        else if (this.checkListSubItemBeforeUpdate[currentIndex].isCheck && !f.isCheck) {
                            infoStatus = Enums.InfoStatus.Delete;
                        }

                        var checkListItem = {
                            id: f.subtypeId,
                            actionId: f.selectedAction,
                            checkListMasterId: f.id,
                            isCheck: f.isCheck,
                            listName: f.listName,
                            infoStatus: infoStatus
                        };

                        checkListSubTypes.push(checkListItem);

                    }
                }
            }
            currentIndex++;

            //if (f.isCheck) {
            //    var checkListItem = {
            //        id: f.subtypeId,
            //        actionId: f.selectedAction,
            //        checkListMasterId: f.id,
            //        isCheck: f.isCheck,
            //        listName: f.listName,
            //        infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update
            //    };
            //    checkListSubTypes.push(checkListItem);
            //}

        });

        subtypeModel.checkListSubTypes = checkListSubTypes;
        return subtypeModel;
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceSubtypeTypeManage),
    template: templateMarkup
};