﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant"; 
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceTypeList extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Vehicle_MA_Common_Maintenance_Type")());

        this.canView = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance);
        });

        this.canEdit = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance);
        });

        this.maTypes = ko.observableArray([]);

        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([]);
        this.selectedMAType = ko.observable(null);

        this.queryModel = { companyId: WebConfig.userSession.currentCompanyId };

        this._selectingRowHandler = (row) => {
            if(row) {
                return this.navigate("ca-asset-maintenance-management-ma-type-view", { maType: row });
            }
            return false;
        };

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-update", (value) => {
            this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(this.queryModel).done((response) => {
                if(response) {
                    response = this.generateName(response);

                    this.maTypes.replaceAll(response["items"]);

                    if(value) {
                        //this.selectedMAType(value);
                        this.recentChangedRowIds([value.id]);
                    }
                };
            });
        });

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-delete", () => {
            this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(this.queryModel).done((response) => {
                if(response) {
                    response = this.generateName(response);

                    this.maTypes.replaceAll(response["items"]);
                };
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();

        this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(this.queryModel).done((response) => {
            if(response) {
                response = this.generateName(response);

                this.maTypes.replaceAll(response["items"]);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
        return dfd;
    }

    generateName(response) {
        response["items"].forEach((val, ind)=>{
            response["items"][ind]["name"] = response["items"][ind]["name"] + " (" + response["items"][ind]["code"] + ")"
        });

        return response;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedMAType(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add", this.canEdit));
        //commands.push(this.createCommand("cmdView", this.i18n("MA_Type_View")(), "svg-cmd-search"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate": 
                this.navigate("ca-asset-maintenance-management-ma-type-manage", { mode: "create" });
                break;
            default:
                break;
        }

        this.selectedMAType(null);
        this.recentChangedRowIds.removeAll();
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceTypeList),
    template: templateMarkup
};