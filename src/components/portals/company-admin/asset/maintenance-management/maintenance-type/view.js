﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenanceTypeView extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.canView = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance);
        });

        this.canEdit = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance);
        });

        this.id = params["maType"]["id"];

        this.bladeTitle(this.i18n("Vehicle_MA_View_Maintenance_Type")());
        this.bladeSize = BladeSize.Medium;

        this.itemsCheckList = ko.observableArray([]);
        this.itemsAction = ko.observableArray([]);

        this.txtName = ko.observable();
        this.txtCode = ko.observable();
        this.txtDesc = ko.observable();

        this.subscribeMessage("ca-asset-maintenance-management-ma-type-update", (value) => {
            this.webRequestVehicleMaintenanceType.getVehicleMaintenanceType(this.id).done((response) => {
                if (response) {
                    this.txtName(response["name"]);
                    this.txtCode(response["code"]);
                    this.txtDesc(response["description"]);
                    this.itemsCheckList(response["checkListMasters"]);
                    this.itemsAction(response["actions"]);
                };
            });
        });

        this.order = ko.observable([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestVehicleMaintenanceType.getVehicleMaintenanceType(this.id).done((result) => {
            if (result) {
                this.txtName(result["name"]);
                this.txtCode(result["code"]);
                this.txtDesc(result["description"]);
                this.itemsCheckList(result["checkListMasters"]);
                this.itemsAction(result["actions"]);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit", this.canEdit));
        commands.push(this.createCommand("cmdObsolate", this.i18n("Vehicle_MA_Obsolete")(), "svg-ic-vmt-obsolate", this.canEdit));
        commands.push(this.createCommand("cmdSubtype", this.i18n("Vehicle_MA_Common_Maintenance_SubType")(), "svg-ic-vmt-mainte-subtype"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-maintenance-management-ma-type-manage", { mode: "update", id: this.id });
                break;
            case "cmdObsolate":
                this.showMessageBox(null, this.i18n("M156")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);
                                this.webRequestVehicleMaintenanceType.setObsoleteVehicleMaintenanceType(this.id).done(() => {
                                    this.isBusy(false);
                                    this.publishMessage("ca-asset-maintenance-management-ma-type-delete");
                                    this.close(true);
                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
                                break;
                        }
                    });
                break;
            case "cmdSubtype":
                this.navigate("ca-asset-maintenance-management-ma-type-view-subtype", { id: this.id });
                break;
            default:
                break;
        }
    }

    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceTypeView),
    template: templateMarkup
};