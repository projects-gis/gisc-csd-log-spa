﻿import ko from "knockout";
import templateMarkup from "text!./vehicle-select.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration"; 
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";
import _ from "lodash";

/**
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenancePlanSelectVehicles extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        //console.log("params",params);
        this.vehicleDetail = ko.observable(params.vehicleDetail);
        this.bladeTitle(this.i18n("MA_Maintenance_Plan_Select_Vehicles")());
        this.businessUnitList = ko.observableArray([]);
        this.businessUnit = ko.observable();
        this.includeSubBU = ko.observable(this.vehicleDetail().includeSub);
        this.checkList = ko.observableArray([]);
        this.buItem = this.ensureNonObservable([]);
        this.selectedSubBU = this.ensureNonObservable([]);

        this.isFirstTimeLoad = this.ensureNonObservable(true);

        this.selectedItems = ko.observableArray([]);

        this._selectedVehicleIds = this.ensureNonObservable(params.vehicleDetail.vehicleIds, []);
        this._selectedItemsSubscribe = this.selectedItems.subscribe((group) => {
         
            this._selectedVehicleIds = group.map((g) => {
                return g.id;
            });
        });

 

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */


    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        this.isBusy(true);

        var dfd = $.Deferred();
        var dfdBusinessUnit = null;
        var dfdVehicle = null;
        
        var businessUnitFilter = {
          companyId: WebConfig.userSession.currentCompanyId
        }
      
        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        dfdBusinessUnit.done((response) => {
          //  console.log("response",response["items"]);

            var responseItem = response["items"];
          
            for (var i = 0; i < responseItem.length; i++){
                if (responseItem[i].parentBusinessUnitId == null){
                    
                    var groupBu = { id : responseItem[i].id , subId : [] };

                    for (var indSub = 0; indSub < responseItem.length; indSub++){
                        if (responseItem[indSub].parentBusinessUnitId == groupBu.id){
                            groupBu.subId.push(responseItem[indSub].id);
                        }
                    }
                    this.buItem.push(groupBu);
                }
            }

         //   console.log("this.buItem",this.buItem);

            this.businessUnitList(response["items"]);
            if (this.vehicleDetail().businessUnitId){
                this.businessUnit(this.vehicleDetail().businessUnitId);
            }
 
        });

        this.businessUnit.subscribe((ddlValue) => {
            //console.log("bu change!!");
            if (!this.isFirstTimeLoad){



                var busUnit = [ddlValue];
                this.selectedSubBU = [];
                if (this.includeSubBU()){
                    $.each(this.buItem,(index, item) => {
                        if (item.id == ddlValue ){
                            this.selectedSubBU = item.subId;
                        }

                    });

                    busUnit = busUnit.concat(this.selectedSubBU);
                }

              //  console.log("this.selectedSubBU",this.selectedSubBU);

                if (ddlValue != null) {
                    var vehicleFilter = {
                        companyId: WebConfig.userSession.currentCompanyId,
                        businessUnitIds: busUnit
                        //includeIds: this.selectedSubBU
                        //includeIds:[this.includeSubBU() ? 1 : 0]
                    };

                   // console.log("vehicleFilter",vehicleFilter);
                    
                    //dfdVehicle = this.webRequestAppointment.listVehicle(vehicleFilter).done((responseVehicle) => {
                    dfdVehicle = this.webRequestVehicle.listVehicleSummary(vehicleFilter).done((responseVehicle) => {
                      //  console.log("responseVehicle",responseVehicle);
                        var arrVehicle = new Array();
                        

                        for (var i = 0; i < responseVehicle.items.length; i++) {
                            arrVehicle.push({ id: responseVehicle.items[i].id, name: responseVehicle.items[i].license });
                        }
                        this.checkList.replaceAll(arrVehicle);
                        this.selectedItems.replaceAll([]);
                    });
                }
            }
        });

        this.includeSubBU.subscribe((isChecked) => {
            if (!this.isFirstTimeLoad){

                if (this.businessUnit() != null) {
                    //var busUnit = this.businessUnit();//ddlValue.id;
                    var busUnit = [this.businessUnit()];
                    this.selectedSubBU = [];
                    if (isChecked){
                        $.each(this.buItem, (index, item) => {
                            if (item.id == busUnit ){
                                this.selectedSubBU = item.subId;
                            }
                        });

                        busUnit = busUnit.concat(this.selectedSubBU);

                    }
         

                    var vehicleFilter = {
                        companyId: WebConfig.userSession.currentCompanyId,
                        businessUnitIds: busUnit
                        //includeIds: this.selectedSubBU
                        //includeIds: [isChecked ? 1 : 0]
                    };


                    dfdVehicle = this.webRequestVehicle.listVehicleSummary(vehicleFilter).done((responseVehicle) => {

                     //   console.log("Chesck Res",responseVehicle);
                        if (responseVehicle) {
                            var arrVehicle = new Array();
                            for (var i = 0; i < responseVehicle.items.length; i++) {
                                arrVehicle.push({ id: responseVehicle.items[i].id, name: responseVehicle.items[i].license });
                            }
                          
                            this.checkList.replaceAll(arrVehicle);
                            this.selectedItems.replaceAll([]);
                        }
                    });

                    $.when(dfdVehicle).done(() => {
                        dfdVehicle.resolve();
                    }).fail((e) => {
                        dfdVehicle.reject(e);
                    });
                }
                //console.log("this.businessUnit().id", this.businessUnit().id);

            }
        });



        if (this.vehicleDetail().businessUnitId && this.vehicleDetail().vehicleIds.length > 0){

            var busUnit = [this.vehicleDetail().businessUnitId].concat(this.vehicleDetail().subBusinessUnitId);
            this.selectedSubBU = this.vehicleDetail().subBusinessUnitId;
            //console.log(this.vehicleDetail(),busUnit);
            var vehicleFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                businessUnitIds: busUnit,
                //includeIds:[this.vehicleDetail().includeSub ? 1 : 0]
                //includeIds: this.vehicleDetail().subBusinessUnitId
            };

            dfdVehicle = this.webRequestVehicle.listVehicleSummary(vehicleFilter).done((responseVehicle) => { 
                
              //  console.log("Chesck Res2",responseVehicle);
                var arrVehicle = new Array(); 
                for (var i = 0; i < responseVehicle.items.length; i++) {
                    arrVehicle.push({ id: responseVehicle.items[i].id, name: responseVehicle.items[i].license });
                }
                this.checkList.replaceAll(arrVehicle);
                _.each(this._selectedVehicleIds, (vehicleId) => {
           
                  
                    let match = _.find(this.checkList(),(chkListItem) => {

                  
                        return chkListItem.id == vehicleId;
                    });
                    if(match) {
                    
                        this.selectedItems.push(match);
                    }
                });
                this.isFirstTimeLoad = false;
            });
            
            $.when(dfdBusinessUnit,dfdVehicle).done(() => {
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            }).then(() => {
                this.isBusy(false);
            });
        }
        else {
            this.isFirstTimeLoad = false;
            $.when(dfdBusinessUnit).done(() => {
                dfd.resolve();
            }).fail((e) => {
                dfd.reject(e);
            }).then(() => {
                this.isBusy(false);
            });
        }

        

       

        return dfd;
    }

    
    setupExtend() {
        this.businessUnitList.extend({ trackChange: true });
        this.businessUnit.extend({ trackChange: true });
        this.includeSubBU.extend({ trackChange: true });
        this.vehicleDetail.extend({ trackChange: true });
        this.selectedItems.extend({
            trackArrayChange: true
        });
    }




    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
    * Get WebRequest specific for EnumResource module in Web API access.
    * @readonly
    */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }


    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }


    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            var arrChecklist = [];
            
            for(var i = 0;i < this.selectedItems().length;i++){

                arrChecklist.push(this.selectedItems()[i].id);
            }


          //  console.log("this.businessUnit()",this.businessUnit())
           // console.log("arrChecklist",arrChecklist)
            this.vehicleDetail().businessUnitId = this.businessUnit();
            this.vehicleDetail().includeSub = this.includeSubBU();
            this.vehicleDetail().vehicleIds = arrChecklist;
            this.vehicleDetail().subBusinessUnitId = this.selectedSubBU;
            
            
           

            //this.vehicleDetail().subBusinessUnitId = this.buItem

            //this.vehicleDetail.businessUnitId = this.businessUnit().id;
            //this.vehicleDetail.includSub = this.includeSubBU();

            //this.vehicleDetail.vehicleIds = arrChecklist;


         //   console.log("Detail ",this.vehicleDetail())
            this.publishMessage("ca-asset-maintenance-management-vehicles-select", this.vehicleDetail());
            this.close(true);

        }
    }



    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
viewModel: ScreenBase.createFactory(MaintenancePlanSelectVehicles),
    template: templateMarkup
};