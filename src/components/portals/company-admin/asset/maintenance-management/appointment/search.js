﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import WebRequestBusinessUnit from "../../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebrequestVehicle from "../../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestVehicleMaintenanceType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import { Enums, EntityAssociation } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AppointmentSearch extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());
        let currentDate = new Date();

        this.maintenanceTypeList = ko.observableArray([]);
        this.maintenanceSubtypeList = ko.observableArray([]);
        this.appointmentStatusList = ko.observableArray([]);
        this.businessUnitList = ko.observableArray([]);
        this.vehicleList = ko.observableArray([]);

        this.maintenanceType = ko.observable();
        this.maintenanceSubtype = ko.observable();
        this.appointmentStatus = ko.observable();
        this.businessUnit = ko.observable();
        this.includeSubBU = ko.observable(true);
        this.vehicle = ko.observable();
        this.assignTo = ko.observable();
        this.startDate = ko.observable(currentDate);
        this.endDate = ko.observable(currentDate);

        let setTimeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);
        let setTime_00 = "00:00";//("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);

        this.startTime = ko.observable(setTime_00);
        this.endTime = ko.observable(setTimeNow);

        this.dateFormat = WebConfig.companySettings.shortDateFormat;

    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
    * Get WebRequest specific for Business module in Web API access.
    * @readonly
    */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
    * Get WebRequest specific for EnumResource module in Web API access.
    * @readonly
    */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
    * Get WebRequest specific for Vehicle module in Web API access.
    * @readonly
    */
    get webrequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    /**
    * Get WebRequest specific for Vehicle Maintenance Plan module in Web API access.
    * @readonly
    */
    get webRequestVehicleMaintenanceType() {
        return WebRequestVehicleMaintenanceType.getInstance();
    }

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }



    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();


        var dfdMaintenanceType = null;
        var dfdMaintenanceSubtype = null;
        var dfdAppointmentStatus = null;
        var dfdBusinessUnit = null;
        var dfdVehicle = null;

        var maintenanceTypeFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        };

        dfdMaintenanceType = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceType(maintenanceTypeFilter).done((response) => {
            this.maintenanceTypeList(response["items"]);
            this.maintenanceType(ScreenHelper.findOptionByProperty(this.maintenanceTypeList, "id"));
        });

        this.maintenanceType.subscribe((ddlValue) => {
            var maTypeID = null;
            if (ddlValue != undefined && ddlValue != null && ddlValue != '') {
                maTypeID = ddlValue.id;
            }
            //else {
            //    maTypeID = '';
            //}

            var maintenanceSubTypeFilter = {
                companyId: WebConfig.userSession.currentCompanyId,
                maintenanceTypeId: maTypeID
            };
            dfdMaintenanceSubtype = this.webRequestVehicleMaintenanceType.listVehicleMaintenanceSubType(maintenanceSubTypeFilter).done((responseMASubType) => {

                this.maintenanceSubtypeList(responseMASubType["items"]);
                this.maintenanceSubtype(ScreenHelper.findOptionByProperty(this.maintenanceSubtypeList, "id"));

            });

            $.when(dfdMaintenanceSubtype).done(() => {
                dfdMaintenanceSubtype.resolve();
            }).fail((e) => {
                dfdMaintenanceSubtype.reject(e);
            });
        });

        var appointmentStatusFilter = {
            companyId: WebConfig.userSession.currentCompanyId,
            types: [Enums.ModelData.EnumResourceType.AppointmentStatus]
        }

        dfdAppointmentStatus = this.webRequestEnumResource.listEnumResource(appointmentStatusFilter).done((response) => {
            this.appointmentStatusList(response["items"]);
            this.appointmentStatus(ScreenHelper.findOptionByProperty(this.appointmentStatusList, "value"));
        });

        var businessUnitFilter = {
            companyId: WebConfig.userSession.currentCompanyId
        }

        dfdBusinessUnit = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter).done((response) => {
            this.businessUnitList(response["items"]);
            this.businessUnit(ScreenHelper.findOptionByProperty(this.businessUnitList, "id"));
        });

        this.businessUnit.subscribe((ddlValue) => {
            if (ddlValue != null) {

                var busUnit = ddlValue;//ddlValue.id;
                var vehicleFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [busUnit],
                    includeIds: [this.includeSubBU() ? 1 : 0]
                };


                dfdVehicle = this.webRequestAppointment.listVehicle(vehicleFilter).done((responseVehicle) => {
                    if (responseVehicle) {
                        var arrVehicle = new Array();
                        for (var i = 0; i < responseVehicle.length; i++) {
                            arrVehicle.push({ id: responseVehicle[i].id, license: responseVehicle[i].license.license });
                        }
                        this.vehicleList(arrVehicle);
                        this.vehicle(ScreenHelper.findOptionByProperty(this.vehicleList, "id"));
                    }
                });

                $.when(dfdVehicle).done(() => {
                    dfdVehicle.resolve();
                }).fail((e) => {
                    dfdVehicle.reject(e);
                });

                //dfdVehicle = this.webrequestVehicle.listVehicleSummary(vehicleFilter).done((responseVehicle) => {
                //    this.vehicleList(responseVehicle["items"]);
                //    this.vehicle(ScreenHelper.findOptionByProperty(this.vehicleList, "id"));
                //});

                //$.when(dfdVehicle).done(() => {
                //    dfdVehicle.resolve();
                //}).fail((e) => {
                //    dfdVehicle.reject(e);
                //});
            }
        });

        this.includeSubBU.subscribe((isChecked) => {
            if (this.businessUnit() != null) {
                var busUnit = this.businessUnit();//ddlValue.id;
                var vehicleFilter = {
                    companyId: WebConfig.userSession.currentCompanyId,
                    businessUnitIds: [busUnit],
                    includeIds: [isChecked ? 1 : 0]
                };


                dfdVehicle = this.webRequestAppointment.listVehicle(vehicleFilter).done((responseVehicle) => {
                    if (responseVehicle) {
                        var arrVehicle = new Array();
                        for (var i = 0; i < responseVehicle.length; i++) {
                            arrVehicle.push({ id: responseVehicle[i].id, license: responseVehicle[i].license.license });
                        }
                        this.vehicleList(arrVehicle);
                        this.vehicle(ScreenHelper.findOptionByProperty(this.vehicleList, "id"));
                    }
                });

                $.when(dfdVehicle).done(() => {
                    dfdVehicle.resolve();
                }).fail((e) => {
                    dfdVehicle.reject(e);
                });
            }
        });

        $.when(dfdMaintenanceType, dfdAppointmentStatus, dfdBusinessUnit).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear", true));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSearch") {
            var formatStartDate = null;
            var formatEndDate = null;
            if (this.startDate() != undefined) {
                formatStartDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.startDate())) + " " + this.startTime() + ":00";
            }

            if (this.endDate() != undefined) {
                formatEndDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.endDate())) + " " + this.endTime() + ":00";
            }


            if (sender.id === "actSearch") {
                var info = {
                    "companyId": WebConfig.userSession.currentCompanyId,
                    "businessUnitId": this.businessUnit() != undefined ? this.businessUnit() : null,
                    "includeSubBusinessUnit": this.includeSubBU(),
                    "vehicleId": this.vehicle() != undefined ? this.vehicle().id : null,
                    "maintenanceTypeId": this.maintenanceType() != undefined ? this.maintenanceType().id : null,
                    "maintenanceSubTypeId": this.maintenanceSubtype() != undefined ? this.maintenanceSubtype().id : null,
                    "assignTo": this.assignTo(),
                    "status": this.appointmentStatus() != undefined ? this.appointmentStatus().value : null,
                    "fromAppointmentDate": formatStartDate,
                    "toAppointmentDate": formatEndDate,
                }

                this.publishMessage("ca-asset-maintenance-management-appointment-search", info);
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdClear":
                this.isBusy(true);
                this.maintenanceType('');
                this.appointmentStatus('');
                this.businessUnit('');
                this.includeSubBU(false);
                this.assignTo('');
                this.startDate('');
                this.endDate('');
                this.startTime('00:00');
                this.endTime('00:00');

                this.isBusy(false);
                //this.showMessageBox(null, "Clear Form?(Temp)",
                //    BladeDialog.DIALOG_YESNO).done((button) => {
                //        switch (button) {
                //            case BladeDialog.BUTTON_YES:

                //                break;
                //        }
                //    });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentSearch),
    template: templateMarkup
};