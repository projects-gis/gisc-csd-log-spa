﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";


/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AppointmentView extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.canView = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance);
        });

        this.canEdit = ko.pureComputed(() => {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance);
        });


        this.id = params["appointment"]["id"];
        this.statusMA = this.ensureNonObservable(params.statusMA);
        this.bladeTitle(this.i18n("Vehicle_MA_View_Appointment")());
        this.bladeSize = BladeSize.Medium;
        this.status = ko.observable();
        this.dateTime = ko.observable();
        this.description = ko.observable();
        this.vehicle = ko.observable();
        this.contact = ko.observable();
        this.maPlan = ko.observable();
        this.assignTo = ko.observable();
        this.remark = ko.observable();
        this.appointmentStatus = ko.observable();
        this.canComplete = ko.observable();

        this.isActiveStatus = ko.pureComputed(() => {
            return (this.appointmentStatus() === Enums.ModelData.AppointmentStatus.Active) && this.canEdit();
        });

        this.isCancel = ko.pureComputed(() => {
            return this.appointmentStatus() === Enums.ModelData.AppointmentStatus.Cancelled || this.appointmentStatus() === Enums.ModelData.AppointmentStatus.Postpone;
        });

        this.subscribeMessage("ca-asset-maintenance-management-appointment-manage-save-completed", (val) => {
            this.webRequestAppointment.getAppointment(this.id).done((result) => {
                if (result) {

                    this.status(result["appointmentStatusDisplayName"]);
                    this.dateTime(result["formatAppointmentDate"]);
                    this.description(result["description"]);
                    this.vehicle(result["vehicleName"]);
                    this.contact(result["contact"]);
                    this.maPlan(result["maintenancePlanName"]);
                    this.assignTo(result["assignTo"]);
                    this.remark(result["remark"]);
                    this.appointmentStatus(result["appointmentStatus"]);
                }



            }).fail((e) => {
                this.handleError(e);
            });
        });





    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestAppointment.getAppointment(this.id).done((result) => {
            if (result) {

                this.status(result["appointmentStatusDisplayName"]);
                this.dateTime(result["formatAppointmentDate"]);
                this.description(result["description"]);
                this.vehicle(result["vehicleName"]);
                this.contact(result["contact"]);
                this.maPlan(result["maintenancePlanName"]);
                this.assignTo(result["assignTo"]);
                this.remark(result["remark"]);
                this.appointmentStatus(result["appointmentStatus"]);


                var dateCompare = new Date();


                var arrSplit = (result["appointmentDate"]).split('T');
                var arrSplitDates = arrSplit[0].split('-');


                var appointmentDate = new Date(arrSplitDates[0], (parseInt(arrSplitDates[1], 10) - 1), arrSplitDates[2]);

                this.canComplete(dateCompare >= appointmentDate);

                //if(dateCompare >= appointmentDate ){
                //    this.canComplete(true);
                //}else{
                //    this.canComplete(false);
                //}
            }

            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() { }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) { }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (this.appointmentStatus() == Enums.ModelData.AppointmentStatus.Active) {


            if (this.statusMA == Enums.ModelData.MaintenanceStatus.Completed || this.statusMA == Enums.ModelData.MaintenanceStatus.Cancelled) {

            } else {
                commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit", this.isActiveStatus));
                commands.push(this.createCommand("cmdPostpone", this.i18n("Vehicle_MA_Appointment_Postpone")(), "svg-ic-va-postpone", this.isActiveStatus));
                if (this.canComplete()) {
                    commands.push(this.createCommand("cmdComplete", this.i18n("Common_Complete")(), "svg-cmd-maintenance-complete", this.isActiveStatus));
                }
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete", this.isActiveStatus));
                commands.push(this.createCommand("cmdCancel", this.i18n("Common_Cancel")(), "svg-cmd-maintenance-cancel", this.isActiveStatus));


            }
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-maintenance-management-appointment-manage", { mode: "update", id: this.id });
                break;
            case "cmdPostpone":
                this.navigate("ca-asset-maintenance-management-appointment-view-postpone", { id: this.id });
                break;
            case "cmdComplete":
                let model = [];
                model.id = this.id;
                model.appointmentStatus = Enums.ModelData.AppointmentStatus.Completed;
                model.remark = "no remark";

                //this.showMessageBox(null, this.i18n("M158")(), BladeDialog.DIALOG_YESNO).done((button) => {
                //    switch (button) {
                //        case BladeDialog.BUTTON_YES:
                            this.webRequestAppointment.completeAppointment(model, true).done((data) => {
                                this.publishMessage("ca-asset-maintenance-management-appointment-manage-save-completed", data);
                                this.close(true);
                            });

                //            break;
                //    }
                //});

                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M155")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.webRequestAppointment.deleteAppointment(this.id).done(() => {
                                this.publishMessage("ca-asset-maintenance-management-appointment-delete");
                                this.close(true);
                            });

                            break;
                    }
                });

                break;
            case "cmdCancel":
                this.navigate("ca-asset-maintenance-management-appointment-view-cancel", { id: this.id });
                break;
            default:
                break;
        }
    }

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentView),
    template: templateMarkup
};