﻿import ko from "knockout";
import templateMarkup from "text!./postpone.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";

import "jquery-ui";
import "jqueryui-timepicker-addon";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AppointmentPostpone extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.id = this.ensureNonObservable(params.id);

        this.bladeTitle(this.i18n("Vehicle_MA_Appointment_Postpone_Header")());

        let setTimeNow = ("0" + new Date().getHours()).slice(-2)+":"+("0" + new Date().getMinutes()).slice(-2);
        this.formatSet = "dd/MM/yyyy";
        this.timeStart = ko.observable(setTimeNow);

        this.status = ko.observable();
        this.selectedVehicleText = ko.observable();
        this.selectedMAText = ko.observable();
        
        this.txtDescription = ko.observable();

        this.txtRemark = ko.observable();
        this.selectedDate = ko.observable();
        this.txtContactPoint = ko.observable();
        this.txtAssignTo = ko.observable();
        
        this.oldAppointment = null;

        this.toDay = ko.observable(new Date());

        this.updateModel = {
            "id": this.id,
            "maintenancePlanId": null,
            "vehicleId": null,
            "assignTo": null,
            "contact": null,
            "appointmentDate": null,
            "description": null,
            "appointmentStatus": null,
            "remark": null
        };
    }

    setupExtend() { 
        this.txtDescription.extend({ trackChange: true });
        this.txtRemark.extend({ trackChange: true });
        this.selectedDate.extend({ trackChange: true });
        this.txtContactPoint.extend({ trackChange: true });
        this.txtAssignTo.extend({ trackChange: true });
        this.timeStart.extend({ trackChange: true });
        
        this.txtRemark.extend({ required: true });
        this.txtContactPoint.extend({ required: true });
        this.txtAssignTo.extend({ required: true });
        this.timeStart.extend({ required: true });

        this.selectedDate.extend({ 
            required: true,
            validation: {
                validator: (val) => {
                    let currentDay = new Date();
                    currentDay.setHours(0);
                    currentDay.setMinutes(0);
                    currentDay.setSeconds(0);
                    currentDay.setMilliseconds(0);

                    //console.log($.datepicker.formatDate("yy-mm-dd", new Date(this.selectedDate())) + " " + this.timeStart() + ":00")

                    let newDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.selectedDate())) + "T" + this.timeStart() + ":00";

                    //console.log(this.oldAppointment);


                    console.log("newDate", newDate);
                    console.log("this.oldAppointment", this.oldAppointment);

                    return Date.parse(val) >= currentDay 
                        && (newDate != this.oldAppointment);
                },
                message: this.i18n("M148")()
            }
        });

        this.validationModel = ko.validatedObservable({
            selectedDate : this.selectedDate,
            txtContactPoint: this.txtContactPoint,
            txtAssignTo: this.txtAssignTo,
            txtRemark: this.txtRemark
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();
        
        this.webRequestAppointment.getAppointment(this.id).done((resultAppointment) => {

            this.updateModel.maintenancePlanId = resultAppointment["maintenancePlanId"];
            this.updateModel.vehicleId = resultAppointment["vehicleId"];

            //this.selectedDate(resultAppointment["appointmentDate"]);

            this.oldAppointment = resultAppointment["appointmentDate"];

            this.selectedDate(new Date());

            this.txtContactPoint(resultAppointment["contact"]);
            this.txtAssignTo(resultAppointment["assignTo"]);
            this.txtDescription(resultAppointment["description"]);
                    
            this.status(resultAppointment["appointmentStatusDisplayName"]);

            this.selectedMAText(resultAppointment["maintenancePlanName"]);
            this.selectedVehicleText(resultAppointment["vehicleName"]);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            
            this.isBusy(true);

            var formatSDate = $.datepicker.formatDate("yy-mm-dd", new Date(this.selectedDate())) + " " + this.timeStart() + ":00";

            this.updateModel.appointmentStatus = Enums.ModelData.AppointmentStatus.Postpone;
            this.updateModel.appointmentDate = formatSDate;
            this.updateModel.description = this.txtDescription();
            this.updateModel.contact = this.txtContactPoint();
            this.updateModel.assignTo = this.txtAssignTo();
            this.updateModel.remark = this.txtRemark();

            this.webRequestAppointment.postponeAppointment(this.updateModel, true).done((result) => {
                this.publishMessage("ca-asset-maintenance-management-appointment-manage-save-completed", result);
                this.isBusy(false);
                this.close(true);
            }).fail((e) => {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentPostpone),
    template: templateMarkup
};