﻿import "jquery";
import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import { Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AppointmentList extends ScreenBase {

    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.isFromAppointment = ko.observable(params.isFromAppointment == undefined ? true : this.ensureNonObservable(params.isFromAppointment));
        this.isFromMA = ko.observable(!this.isFromAppointment());

        this.bladeTitle(!this.isFromAppointment() ? this.i18n("Vehicle_MA_Appointment")() : this.i18n("Vehicle_MA_Appointment_Management")());
        this.bladeSize = BladeSize.XLarge_A0;

        this.canView = ko.pureComputed(() => {
            return this.isFromMA() || WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance);
        });

        this.canEdit = ko.pureComputed(() => {
            return this.isFromMA() || WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance);
        });
        
        
        this.items = ko.observableArray([]);
        
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([]);
        this.selectedAppointment = ko.observable();

        this.maintenancePlanInfo = params.appointment;

        this.vehicleId = null;
        if(!this.isFromAppointment()) {
            this.vehicleId = params.appointment.vehicleId;
            this.vehicleData = params.vehicle;
        }

        if (params.dashboard) {
            this.filter = {};
            let date = new Date(params.dashboard.date);
            this.filter.status = params.dashboard.status;
            this.filter.fromAppointmentDate = $.datepicker.formatDate("yy-mm-dd", date) + "T00:00:00";
            this.filter.toAppointmentDate = $.datepicker.formatDate("yy-mm-dd", date) + "T23:59:59";
        }

        this._selectingRowHandler = (appointment) => {
            if(appointment) {
                
                if(!this.isFromAppointment()){
                    return this.navigate("ca-asset-maintenance-management-appointment-view", { appointment: appointment, statusMA: params.appointment.maintenanceStatus });
                }
                else {
                    return this.navigate("ca-asset-maintenance-management-appointment-view", { appointment: appointment });
                }
            }
            return false;
        }

        this.subscribeMessage("ca-asset-maintenance-management-appointment-delete", () => {
            let model = { companyId: WebConfig.userSession.currentCompanyId }

            if(!this.isFromAppointment()) {
                model.vehicleId = this.vehicleId;
                model.ids = [this.maintenancePlanInfo.id];
            }

            this.webRequestAppointment.listAppointment(model).done((response) => {
                if(response) {
                    this.items.replaceAll(response["items"]);
                    this.selectedAppointment(null);
                }
            });
        });


        this.subscribeMessage("ca-asset-maintenance-management-appointment-search", (filter) => {
            this.webRequestAppointment.listAppointment(filter).done((response) => {
                if (response) {
                    this.items.replaceAll(response["items"]);
                    this.selectedAppointment(null);
                }
            });
        });

        this.subscribeMessage("ca-asset-maintenance-management-appointment-manage-save-completed", (data) => {

            let model = { companyId: WebConfig.userSession.currentCompanyId }

            if(!this.isFromAppointment()) {
                model.vehicleId = this.vehicleId;
                model.ids = [this.maintenancePlanInfo.id];
            }

            this.webRequestAppointment.listAppointment(model).done((response) => {
                if(response) {
                    this.items.replaceAll(response["items"]);
                    this.recentChangedRowIds([data.id]);
                }
            });
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        let model = { companyId: WebConfig.userSession.currentCompanyId }

        if (this.filter) {
            model.status = this.filter.status;
            model.fromAppointmentDate = this.filter.fromAppointmentDate;
            model.toAppointmentDate = this.filter.toAppointmentDate;
        }
        
        if(!this.isFromAppointment()) {
            model.vehicleId = this.vehicleId;
            model.ids = [this.maintenancePlanInfo.id];
        }

        this.webRequestAppointment.listAppointment(model).done((response) => {
            if(response) {
                this.items.replaceAll(response["items"]);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedAppointment(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(this.isFromAppointment()) {
            commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search"));
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add", this.canEdit));
        }
        else {
            if( this.maintenancePlanInfo.maintenanceStatus != Enums.ModelData.MaintenanceStatus.Completed && 
                this.maintenancePlanInfo.maintenanceStatus != Enums.ModelData.MaintenanceStatus.Cancelled) {
                commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add", this.canEdit));
            }
            else {

            }
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdSearch":
                this.navigate("ca-asset-maintenance-management-appointment-search", {});
                break;
            case "cmdCreate":
                this.navigate("ca-asset-maintenance-management-appointment-manage", {
                    mode: "create",
                    isFromAppointment: this.isFromAppointment(),
                    vehicle: this.vehicleData,
                    maPlan : this.maintenancePlanInfo
                });
                break;
            default:
                break;
        }

        this.selectedAppointment(null);
        this.recentChangedRowIds.removeAll();
    }

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentList),
    template: templateMarkup
};