﻿import ko from "knockout";
import templateMarkup from "text!./cancel.html";
import ScreenBase from "../../../../screenbase";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAppointment from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAppointment";
import { Enums } from "../../../../../../app/frameworks/constant/apiConstant";

/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class AppointmentCancel extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.id = this.ensureNonObservable(params.id);

        this.bladeTitle(this.i18n("Vehicle_MA_Appointment_Cancel_Header")());

        this.status = ko.observable();
        this.selectedVehicleText = ko.observable();
        this.selectedMAText = ko.observable();
        
        this.txtDescription = ko.observable();

        this.txtRemark = ko.observable();
        this.selectedDate = ko.observable();
        this.txtContactPoint = ko.observable();
        this.txtAssignTo = ko.observable();

        this.updateModel = {
            "id": this.id,
            "appointmentStatus": null,
            "remark": null
        };
    }

    setupExtend() { 

        this.txtRemark.extend({ trackChange: true });
        this.txtRemark.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            txtRemark: this.txtRemark
        });
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        let dfd = $.Deferred();
        
        this.webRequestAppointment.getAppointment(this.id).done((resultAppointment) => {

            this.selectedDate(resultAppointment["formatAppointmentDate"]);

            this.txtContactPoint(resultAppointment["contact"]);
            this.txtAssignTo(resultAppointment["assignTo"]);
            this.txtDescription(resultAppointment["description"]);
                    
            this.status(resultAppointment["appointmentStatusDisplayName"]);

            this.selectedMAText(resultAppointment["maintenancePlanName"]);
            this.selectedVehicleText(resultAppointment["vehicleName"]);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            
            this.isBusy(true);

            this.updateModel.appointmentStatus = Enums.ModelData.AppointmentStatus.Cancelled;
            this.updateModel.remark = this.txtRemark();

            this.webRequestAppointment.cancelAppointment(this.updateModel, true).done((result) => {
                this.publishMessage("ca-asset-maintenance-management-appointment-manage-save-completed", result);
                this.isBusy(false);
                this.close(true);
            }).fail((e) => {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    get webRequestAppointment() {
        return WebRequestAppointment.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AppointmentCancel),
    template: templateMarkup
};