﻿import "jquery";
import ko from "knockout";
import ScreenBase from "../../../../screenBase";
import templateMarkup from "text!./reports.html";
import * as BladeSize from "../../../../../../app/frameworks/constant/BladeSize";
import * as BladeType from "../../../../../../app/frameworks/constant/BladeType";
import WebConfig from "../../../../../../app/frameworks/configuration/webConfiguration";

class MaintenanceReportViewer extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(params.reportName);
        this.bladeSize = BladeSize.XLarge_Report;
        this.bladeType = BladeType.Compact;
        this.minimizeAll(false);
        this.bladeCanMaximize = false;
        // Prepare report configuration.
        this.reportUrl = this.resolveUrl(WebConfig.reportSession.reportUrl); 
        this.reportSource = params.reportSource;

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {

    }
}

export default {
    viewModel: ScreenBase.createFactory(MaintenanceReportViewer),
    template: templateMarkup
};