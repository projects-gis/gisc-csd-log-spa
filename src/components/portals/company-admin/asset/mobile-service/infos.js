import ko from "knockout";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums,EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";

class MobileServiceFilter {
    constructor (businessUnitIds = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.MobileService;

        if(businessUnitIds){
            this.businessUnitIds = businessUnitIds;
        }else{
            this.businessUnitIds = [];
        }
    }
}
export { MobileServiceFilter };

class BusinessUnitFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.BusinessUnit;
        this.includeAssociationNames = [
            EntityAssociation.BusinessUnit.ChildBusinessUnits
        ];
    }
}
export { BusinessUnitFilter };

class PromotionFilter{
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.OperatorPackageByOperatorPromotion;
    }
}
export { PromotionFilter };

class UserFilter {
    constructor (businessUnitId = null, includeId = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.User;

        if(businessUnitId){
            this.businessUnitIds = [businessUnitId];
        }else{
            this.businessUnitIds = [];
        }

        this.enable = true;
        this.isAssociatedWithMobileService = false;
        this.userType = Enums.UserType.Company;

        if(includeId){
            this.includeIds = [includeId];
        }
    }
}
export { UserFilter };