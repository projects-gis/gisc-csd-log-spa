﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestMobileService from "../../../../../app/frameworks/data/apitrackingcore/webRequestMobileService";
import {Constants, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";

class MobileServiceViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Assets_ViewMobileService")());

        this.mobileServiceId = this.ensureNonObservable(params.mobileServiceId, -1);

        this.mobile = ko.observable('');
        this.simCardSerialNumber = ko.observable('');
        this.promotion = ko.observable('');
        this.businessUnit = ko.observable('');
        this.username = ko.observable('');
        this.imei = ko.observable('');
        this.stopDuration = ko.observable('');
        this.overSpeedDelay = ko.observable('');
        this.startJobInBeginWaypointOnly = ko.observable(false);
        this.checkInInWaypointOnly = ko.observable(false);
        this.finishJobInEndWaypointOnly = ko.observable(false);

        this.subscribeMessage("ca-asset-mobile-service-changed", () => {
            this.isBusy(true);
            this.webRequestMobileService.getMobileService(this.mobileServiceId, 
                [EntityAssociation.MobileService.BusinessUnit, 
                    EntityAssociation.MobileService.Promotion, 
                    EntityAssociation.MobileService.User]).done((mobileService)=> {
                if(mobileService){
                    this.mobile(mobileService.mobile);
                    this.simCardSerialNumber(mobileService.simCardSerialNumber);
                    this.promotion(this.formatDisplayPromotion(mobileService));
                    this.businessUnit(mobileService.businessUnitName);
                    this.username(mobileService.username);
                    this.imei(mobileService.imei);
                    this.stopDuration(mobileService.formatStopDuration);
                    this.overSpeedDelay(mobileService.formatOverSpeedDelay);
                    this.startJobInBeginWaypointOnly(mobileService.startJobInBeginWaypointOnly);
                    this.checkInInWaypointOnly(mobileService.checkInInWaypointOnly);
                    this.finishJobInEndWaypointOnly(mobileService.finishJobInEndWaypointOnly);
                }
                this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Mobile Service module in Web API access.
     * @readonly
     */
    get webRequestMobileService() {
        return WebRequestMobileService.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestMobileService.getMobileService(this.mobileServiceId, 
            [EntityAssociation.MobileService.BusinessUnit, 
                EntityAssociation.MobileService.Promotion, 
                EntityAssociation.MobileService.User]).done((mobileService)=> {
             if(mobileService){
                this.mobile(mobileService.mobile);
                this.simCardSerialNumber(mobileService.simCardSerialNumber);
                this.promotion(this.formatDisplayPromotion(mobileService));
                this.businessUnit(mobileService.businessUnitName);
                this.username(mobileService.username);
                this.imei(mobileService.imei);
                this.stopDuration(mobileService.formatStopDuration);
                this.overSpeedDelay(mobileService.formatOverSpeedDelay);
                this.startJobInBeginWaypointOnly(mobileService.startJobInBeginWaypointOnly);
                this.checkInInWaypointOnly(mobileService.checkInInWaypointOnly);
                this.finishJobInEndWaypointOnly(mobileService.finishJobInEndWaypointOnly);
            }
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateMobileService)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteMobileService)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-mobile-service-manage", { mode: Screen.SCREEN_MODE_UPDATE, mobileServiceId: this.mobileServiceId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M041")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);

                            this.webRequestMobileService.deleteMobileService(this.mobileServiceId).done(() => {
                                this.isBusy(false);

                                this.publishMessage("ca-asset-mobile-service-deleted");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }
    formatDisplayPromotion(mobileService){
        if(mobileService && mobileService.promotionOperator && mobileService.promotionName){
            return Utility.stringFormat("{0} – {1}", mobileService.promotionOperator, mobileService.promotionName);
        }
        return null;
    }
}

export default {
    viewModel: ScreenBase.createFactory(MobileServiceViewScreen),
    template: templateMarkup
};