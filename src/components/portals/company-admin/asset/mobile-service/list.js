﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestMobileService from "../../../../../app/frameworks/data/apitrackingcore/webRequestMobileService";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import {MobileServiceFilter} from "./infos";

class MobileServiceListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Assets_MobileServices")());
        this.bladeSize = BladeSize.Medium;
        this.pageFiltering = ko.observable(false);

        this.searchFilter = new MobileServiceFilter();
        this.searchFilterDefault = new MobileServiceFilter(); 

        this.mobileServices = ko.observableArray([]);

        this.filterText = ko.observable('');
        this.selectedMobileService = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this._selectingRowHandler = (mobileService) => {
            if(mobileService) {
                return this.navigate("ca-asset-mobile-service-view", {
                    mobileServiceId: mobileService.id
                });
            }
            return false;
        };

        this.subscribeMessage("ca-asset-mobile-service-changed", (mobileServiceId) => {
            this.isBusy(true);
            this.webRequestMobileService.listMobileServiceSummary(this.searchFilter).done((r) => {
                this.mobileServices.replaceAll(r.items);
                if(mobileServiceId){
                    this.recentChangedRowIds.replaceAll([mobileServiceId]);
                }else{
                    this.recentChangedRowIds.replaceAll([]);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-asset-mobile-service-deleted", () => {
            this.isBusy(true);
            this.webRequestMobileService.listMobileServiceSummary(this.searchFilter).done((r) => {
                this.mobileServices.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-asset-mobile-service-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter = searchFilter;
            
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter, this.searchFilterDefault));

            this.webRequestMobileService.listMobileServiceSummary(this.searchFilter).done((r) => {
                this.mobileServices.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Mobile Service module in Web API access.
     * @readonly
     */
    get webRequestMobileService() {
        return WebRequestMobileService.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestMobileService.listMobileServiceSummary(this.searchFilter).done((r)=> {
            this.mobileServices(r["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateMobileService)) {
             commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id === "cmdCreate") {
            this.navigate("ca-asset-mobile-service-manage", { mode: Screen.SCREEN_MODE_CREATE });
        }
        else if(sender.id == "cmdFilter") {
            this.navigate("ca-asset-mobile-service-search", {searchFilter: this.searchFilter});
        }
        this.selectedMobileService(null);
        this.recentChangedRowIds.removeAll();
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedMobileService(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(MobileServiceListScreen),
    template: templateMarkup
};