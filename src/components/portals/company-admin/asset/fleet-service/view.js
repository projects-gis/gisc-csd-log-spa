﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestFleetService from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetService";
import {Constants, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";

class FleetServiceViewScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_ViewFleetService")());

        this.fleetServiceId = this.ensureNonObservable(params.fleetServiceId, -1);
        this.mobile = ko.observable('');
        this.simCardSerialNumber = ko.observable('');
        this.promotion = ko.observable('');
        this.businessUnit = ko.observable('');
        this.box = ko.observable('');
        this.vehicle = ko.observable('');
        this.enableDltInterface = ko.observable('');

        this.subscribeMessage("ca-asset-fleet-service-changed", () => {
            this.isBusy(true);
            this.webRequestFleetService.getFleetService(this.fleetServiceId, 
                [EntityAssociation.FleetService.BusinessUnit, 
                    EntityAssociation.FleetService.Promotion, 
                    EntityAssociation.FleetService.Box, 
                    EntityAssociation.FleetService.Vehicle]).done((fleetService)=> {
                    if(fleetService){
                        let promotion = this.formatDisplayPromotion(fleetService);
                        this.mobile(fleetService.mobile);
                        this.simCardSerialNumber(fleetService.simCardSerialNumber);
                        this.promotion(promotion);
                        this.businessUnit(fleetService.businessUnitName);
                        this.box(fleetService.boxSerialNo);
                        this.vehicle(fleetService.vehicleLicense);
                        this.enableDltInterface(fleetService.formatEnableDltInterface);
                    }
                this.isBusy(false);
            }).fail((e)=> {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Fleet Service module in Web API access.
     * @readonly
     */
    get webRequestFleetService() {
        return WebRequestFleetService.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestFleetService.getFleetService(this.fleetServiceId, 
               [EntityAssociation.FleetService.BusinessUnit, 
                EntityAssociation.FleetService.Promotion, 
                EntityAssociation.FleetService.Box, 
                EntityAssociation.FleetService.Vehicle]).done((fleetService)=> {
                if(fleetService){
                    let promotion = this.formatDisplayPromotion(fleetService);
                    this.mobile(fleetService.mobile);
                    this.simCardSerialNumber(fleetService.simCardSerialNumber);
                    this.promotion(promotion);
                    this.businessUnit(fleetService.businessUnitName);
                    this.box(fleetService.boxSerialNo);
                    this.vehicle(fleetService.vehicleLicense);
                    this.enableDltInterface(fleetService.formatEnableDltInterface);
                }
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateFleetService)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteFleetService)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-fleet-service-manage", { mode: Screen.SCREEN_MODE_UPDATE, fleetServiceId: this.fleetServiceId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M039")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:

                            this.showMessageBox(null, this.i18n("M176", [this.mobile()]),
                                BladeDialog.DIALOG_YESNO).done((button) => {
                                var filterFleetService = {
                                    id: this.fleetServiceId,
                                }
                                switch (button) {
                                    case BladeDialog.BUTTON_YES:
                                        filterFleetService.isClosed = true;
                                        this.webRequestFleetService.deleteFleetService(filterFleetService).done(() => {
                                            this.isBusy(false);
                                            this.publishMessage("ca-asset-fleet-service-deleted");
                                            this.close(true);
                                        }).fail((e)=> {
                                            this.handleError(e);
                                            this.isBusy(false);
                                        });
                                        break;
                                    case BladeDialog.BUTTON_NO:
                                        filterFleetService.isClosed = false;
                                        this.webRequestFleetService.deleteFleetService(filterFleetService).done(() => {
                                            this.isBusy(false);
                                            this.publishMessage("ca-asset-fleet-service-deleted");
                                            this.close(true);
                                        }).fail((e)=> {
                                            this.handleError(e);
                                            this.isBusy(false);
                                        });
                                        break;
                                }
                            });
                            break;
                    }
                });
                break;
        }
    }
    formatDisplayPromotion(fleetService){
        if(fleetService && fleetService.promotionOperator && fleetService.promotionName){
            return Utility.stringFormat("{0} ({1})", fleetService.promotionOperator, fleetService.promotionName);
        }
        return null;
    }
}

export default {
    viewModel: ScreenBase.createFactory(FleetServiceViewScreen),
    template: templateMarkup
};