﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenHelper from "../../../screenhelper";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestFleetService from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetService";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import {FleetServiceFilter} from "./infos";

class FleetServiceListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_FleetServices")());
        this.bladeSize = BladeSize.Medium;
        this.pageFiltering = ko.observable(false);

        this.searchFilter = new FleetServiceFilter();
        this.searchFilterDefault = new FleetServiceFilter(); 

        this.fleetServices = ko.observableArray([]);

        this.filterText = ko.observable('');
        this.selectedFleetService = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);

        this._selectingRowHandler = (fleetService) => {
            if(fleetService) {
                return this.navigate("ca-asset-fleet-service-view", {
                    fleetServiceId: fleetService.id
                });
            }
            return false;
        };

        this.subscribeMessage("ca-asset-fleet-service-changed", (fleetServiceId) => {
            this.isBusy(true);
            this.webRequestFleetService.listFleetServiceSummary(this.searchFilter).done((r) => {
                this.fleetServices.replaceAll(r.items);
                if(fleetServiceId){
                    this.recentChangedRowIds.replaceAll([fleetServiceId]);
                }else{
                    this.recentChangedRowIds.replaceAll([]);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-asset-fleet-service-deleted", () => {
            this.isBusy(true);
            this.webRequestFleetService.listFleetServiceSummary(this.searchFilter).done((r) => {
                this.fleetServices.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-asset-fleet-service-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter = searchFilter;
            
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter, this.searchFilterDefault));

            this.webRequestFleetService.listFleetServiceSummary(this.searchFilter).done((r) => {
                this.fleetServices.replaceAll(r.items);
                this.recentChangedRowIds.replaceAll([]);
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });
    }
    /**
     * Get WebRequest specific for Fleet Service module in Web API access.
     * @readonly
     */
    get webRequestFleetService() {
        return WebRequestFleetService.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        this.webRequestFleetService.listFleetServiceSummary(this.searchFilter).done((r)=> {
            this.fleetServices(r["items"]);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateFleetService)) {
             commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));

    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if(sender.id === "cmdCreate") {
            this.navigate("ca-asset-fleet-service-manage", { mode: Screen.SCREEN_MODE_CREATE });
        }
        else if(sender.id == "cmdFilter") {
            this.navigate("ca-asset-fleet-service-search", {searchFilter: this.searchFilter});
        }
        else if(sender.id == "cmdExport"){
            this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestFleetService.exportFleetService(this.searchFilter).done((response)=>{
                            ScreenHelper.downloadFile(response.fileUrl);
                        }).fail((e)=>{
                            this.handleError(e);
                        }).always(()=>{
                            this.isBusy(false);
                        });
                        break;
                }
            });
        }
        this.selectedFleetService(null);
        this.recentChangedRowIds.removeAll();
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedFleetService(null);
    }
}

export default {
    viewModel: ScreenBase.createFactory(FleetServiceListScreen),
    template: templateMarkup
};