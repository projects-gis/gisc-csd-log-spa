﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestFleetService from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetService";
import WebRequestOperatorPackage from "../../../../../app/frameworks/data/apitrackingcore/webRequestOperatorPackage";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webRequestBox";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import webRequestTicket from "../../../../../app/frameworks/data/apitrackingcore/webRequestTicket";
import {Constants} from "../../../../../app/frameworks/constant/apiConstant";
import {BusinessUnitFilter, PromotionFilter, BoxFilter, VehicleFilter} from "./infos";
import Utility from "../../../../../app/frameworks/core/utility";

class FleetServiceManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Assets_CreateFleetService")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_UpdateFleetSetvice")());
                break;
        }

        this.fleetServiceId = this.mode==Screen.SCREEN_MODE_CREATE?null:this.ensureNonObservable(params.fleetServiceId, -1);

        this.enableOptions = ScreenHelper.createYesNoObservableArray();
        this.promotions = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);
        this.boxes = ko.observableArray([]);
        this.vehicles = ko.observableArray([]);
        this.temp_mobile  = ko.observable();
        this.mobile = ko.observable();
        this.mobileArray = ko.observableArray([]);
        this.simCardSerialNumber = ko.observable('');
        this.selectedPromotion = ko.observable();
        this.selectedBusinessUnit = ko.observable(null);
        this.selectedBox = ko.observable();
        this.selectedVehicle = ko.observable();
        this.selectedMobile = ko.observable();
        this.enableDltInterface = ko.observable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", false)); 

        this._businessUnitId = 0;
        this._boxId = 0;
        this._vehicleId = 0;
        this._selectedBusinessUnitRef = this.selectedBusinessUnit.ignorePokeSubscribe((value)=>{
            this.selectedBox(null);
            this.selectedVehicle(null);
            var selectedBusinessUnit = this.selectedBusinessUnit();
            if(selectedBusinessUnit){
                this.isBusy(true);

                var boxFilter = null;
                var vehicleFilter = null;

                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                        boxFilter = new BoxFilter(selectedBusinessUnit);
                        vehicleFilter = new VehicleFilter(selectedBusinessUnit);
                        break;
                    case Screen.SCREEN_MODE_UPDATE:
                        if(this._businessUnitId == selectedBusinessUnit){
                            boxFilter = new BoxFilter(selectedBusinessUnit, this._boxId);
                            vehicleFilter = new VehicleFilter(selectedBusinessUnit, this._vehicleId);
                        }else{
                            boxFilter = new BoxFilter(selectedBusinessUnit);
                            vehicleFilter = new VehicleFilter(selectedBusinessUnit);
                        }
                        break;
                }

                var d1 = this.webRequestBox.listBoxSummary(boxFilter);
                var d2 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);
                $.when(d1, d2).done((r1, r2) => {
                    this.boxes(r1["items"]);
                    this.vehicles(r2["items"]);

                    this.isBusy(false);
                }).fail((e) => {
                    this.handleError(e);
                    this.isBusy(false);
                });
            }else{
                this.boxes([]);
                this.vehicles([]);
            }
        });

            this.selectedPromotion.subscribe((val)=>{
                let id = (typeof val =='undefined')?null:val.id
                let filterSim = {
                    CompanyId:this.companyId,                    
                    OperatorId:id
                };

            this.webRequestTicket.listSimWithFreetId(filterSim).done((response)=>{
                    this.mobileArray(response.items);
                });
            });


    }
    /**
     * Get WebRequest specific for Fleet Service module in Web API access.
     * @readonly
     */
    get webRequestFleetService() {
        return WebRequestFleetService.getInstance();
    }
    /**
     * Get WebRequest specific for Operator Package module in Web API access.
     * @readonly
     */
    get webRequestOperatorPackage() {
        return WebRequestOperatorPackage.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }

    get webRequestTicket(){
        return webRequestTicket.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var businessUnitFilter = new BusinessUnitFilter();
        var promotionFilter = new PromotionFilter();

        var a1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestFleetService.getFleetService(this.fleetServiceId) : null;
        var a2 = this.webRequestOperatorPackage.listOperatorPackage(promotionFilter);
        var a3 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        $.when(a1, a2, a3).done((r1, r2, r3) => {
            var fleetService = r1;
            var promotions = r2["items"];
            Utility.applyFormattedPropertyToCollection(promotions, "displayText", "{0} ({1})", "operator", "promotion")
            this.promotions(promotions);
            this.businessUnits(r3["items"]);

            if (fleetService) {

                this.temp_mobile(fleetService.mobile);
                this.simCardSerialNumber(fleetService.simCardSerialNumber);
                this.selectedPromotion(ScreenHelper.findOptionByProperty(this.promotions, "id", fleetService.operatorId));
                this.selectedBusinessUnit.poke(fleetService.businessUnitId.toString());
                this.enableDltInterface(ScreenHelper.findOptionByProperty(this.enableOptions, "value", fleetService.enableDltInterface));
               

                this._businessUnitId = fleetService.businessUnitId.toString();
                this._boxId = fleetService.boxId;
                this._vehicleId = fleetService.vehicleId;

                
                var boxFilter = new BoxFilter(fleetService.businessUnitId, this._boxId);
                var vehicleFilter = new VehicleFilter(fleetService.businessUnitId, this._vehicleId);
               
                var a4 = this.webRequestBox.listBoxSummary(boxFilter);
                var a5 = this.webRequestVehicle.listVehicleSummary(vehicleFilter);
                var a6 = this.webRequestTicket.listSimWithFreetId({FleetId:fleetService.id, OperatorId:fleetService.operatorId})
                $.when(a4, a5,a6).done((r4, r5,r6) => {
                    this.boxes(r4["items"]);
                    this.vehicles(r5["items"]);
                    this.mobileArray(r6["items"]);
                    this.selectedBox(ScreenHelper.findOptionByProperty(this.boxes, "id", this._boxId));
                    this.selectedVehicle(ScreenHelper.findOptionByProperty(this.vehicles, "id", this._vehicleId));
                    this.selectedMobile(ScreenHelper.findOptionByProperty(this.mobileArray, "mobileNo", fleetService.mobile));
                    dfd.resolve();
                }).fail((e) => {
                    dfd.reject(e);
                });

            }else{
                dfd.resolve();
            }   
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {

        // this.selectedMobile.extend({
        //     trackChange: true
        // });
        // this.simCardSerialNumber.extend({
        //     trackChange: true
        // });
        // this.selectedPromotion.extend({
        //     trackChange: true
        // });
        this.selectedBusinessUnit.extend({
            trackChange: true
        });
        this.selectedBox.extend({
            trackChange: true
        });
        this.selectedVehicle.extend({
            trackChange: true
        });
        this.enableDltInterface.extend({
            trackChange: true
        });

        // Manual setup validation rules
        // this.selectedPromotion.extend({
        //     required: true
        // });
        // this.selectedMobile.extend({
        //     required: true
        // });

        this.selectedBusinessUnit.extend({
            required: true
        });
        this.selectedBox.extend({
            required: true
        });
        this.selectedVehicle.extend({
            required: true
        });
        this.enableDltInterface.extend({
            required: true
        });

        this.validationModel = ko.validatedObservable({
            // selectedPromotion:this.selectedPromotion,
            // selectedMobile: this.selectedMobile,
            selectedBusinessUnit: this.selectedBusinessUnit,
            selectedBox: this.selectedBox,
            selectedVehicle: this.selectedVehicle,
            enableDltInterface: this.enableDltInterface
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.hasPermission(Constants.Permission.CreateFleetService))
            || (this.mode === Screen.SCREEN_MODE_UPDATE && WebConfig.userSession.hasPermission(Constants.Permission.UpdateFleetService))) 
        {
             actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate fleet service model from View Model
     * 
     * @returns fleet service model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id:this.fleetServiceId,
            companyId: this.companyId,
            //mobile:this.selectedMobile().mobileNo,
           // simId:this.selectedMobile().id,
            //simCardSerialNumber: this.simCardSerialNumber(),
            //promotionId: this.selectedPromotion().id,
            businessUnitId: this.selectedBusinessUnit(),
            boxId: this.selectedBox().id,
            vehicleId: this.selectedVehicle().id,
            isClosed : false , 
            enableDltInterface: this.enableDltInterface().value
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

         if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestFleetService.createFleetService(model).done((response) => {
                        this.publishMessage("ca-asset-fleet-service-changed", response.id);
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    var isChangeMobileNumber = (model.mobile!=this.temp_mobile()) ? true:false;
                    // if(isChangeMobileNumber) {
                    
                    // this.showMessageBox(null, this.i18n("M176", [this.temp_mobile()]),
                    // BladeDialog.DIALOG_YESNO).done((button) => {
                    //     switch(button){
                    //         case BladeDialog.BUTTON_YES:
                    //             this.isBusy(true);
                    //             model.isClosed = true;
                    //             this.webRequestFleetService.updateFleetService(model).done((response) => {
                    //                 this.publishMessage("ca-asset-fleet-service-changed");
                    //                 this.isBusy(false);
                    //                 this.close(true);
                    //             }).fail((e)=> {
                    //                 this.isBusy(false);
                    //                 this.handleError(e);
                    //             });
                    //             break;
                    //         case BladeDialog.BUTTON_NO:
                    //             this.isBusy(true);
                    //             model.isClosed = false;
                    //             this.webRequestFleetService.updateFleetService(model).done((response) => {
                    //                 this.publishMessage("ca-asset-fleet-service-changed");
                    //                 this.isBusy(false);
                    //                 this.close(true);
                    //             }).fail((e)=> {
                    //                 this.isBusy(false);
                    //                 this.handleError(e);
                    //             });
                    //             break;

                    //     }
                    // });

                    // }
                    // else{
                        this.webRequestFleetService.updateFleetService(model).done((response) => {
                            this.publishMessage("ca-asset-fleet-service-changed");
                            this.isBusy(false);
                            this.close(true);
                        }).fail((e)=> {
                            this.isBusy(false);

                            this.handleError(e);
                        });
                  //  } 
                    break;
            }
            return ; 
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(FleetServiceManageScreen),
    template: templateMarkup
};