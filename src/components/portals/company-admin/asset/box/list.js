﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import { BoxFilter } from "./infos";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";

class BoxListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Common_Boxes")());
        this.bladeSize = BladeSize.XLarge;

        this.boxes = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.selectedBox = ko.observable(null);
        this.searchFilter = ko.observable(new BoxFilter());
        this.searchFilterDefault = new BoxFilter(); // Use to compare with searchFilter to apply active state
        this.pageFiltering = ko.observable(false); // Use to apply active state for command search
        this.order = ko.observable([[ 0, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (box) => {
            if(box) {
                return this.navigate("ca-asset-box-view", { id: box.id });
            }
            return false;
        };

        // Subscribe Message when box search filter apply
        this.subscribeMessage("ca-asset-box-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter(searchFilter);

            this.webRequestBox.listBoxSummary(this.generateFilter()).done((response) => {
                this.boxes.replaceAll(response["items"]);
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        });

        // subscribe on searchFilter change to apply filter state
        this._changeSearchFilterSubscribe = this.searchFilter.subscribe((searchFilter) => {
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter(), this.searchFilterDefault));
        });

        // change searchFilter after subscribe to make sure active state also applied
        if (params.businessUnitId) {
            this.searchFilter(new BoxFilter(null, params.businessUnitId));
        }
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestBox.listBoxSummary(this.generateFilter()).done((response) => {
            this.boxes(response["items"]);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedBox(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdSearch") {
            this.navigate("ca-asset-box-search", { searchFilter: this.searchFilter() });
        }
        this.selectedBox(null);
    }
    
    /**
     * 
     * Generate BoxFilterInfo for WebRequest
     * @returns
     */
    generateFilter() {
        return {
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.searchFilter().businessUnitIds ? this.searchFilter().businessUnitIds : null,
            vendorId: this.searchFilter().vendorId,
            statuses: this.searchFilter().boxStatus ? [this.searchFilter().boxStatus] : null,
            boxTemplateId: this.searchFilter().boxTemplateId,
            isForRent: this.searchFilter().isForRent,
            sortingColumns: DefaultSorting.Box
        };
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxListScreen),
    template: templateMarkup
};