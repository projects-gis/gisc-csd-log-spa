class BoxFilter {
    constructor (vendorId = null, businessUnitIds = null,  boxStatus = null, boxTemplateId = null, isForRent = null) {
        this.vendorId = vendorId;
        this.businessUnitIds = businessUnitIds;
        this.boxStatus = boxStatus;
        this.boxTemplateId = boxTemplateId;
        this.isForRent = isForRent;
    }
}

export { BoxFilter };