﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-view.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestBoxMaintenance from "../../../../../app/frameworks/data/apitrackingcore/webRequestBoxMaintenance";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";

class BoxMaintenanceViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Assets_ViewBoxMaintenance")());

        this.id = this.ensureNonObservable(params.id);
        this.formatDateTime = ko.observable();
        this.description = ko.observable();
        this.author = ko.observable();

        // Subscribe Message when box maintenance updated
        this.subscribeMessage("ca-asset-box-maintenance-updated", (info) => {
            this.formatDateTime(info.formatDateTime);
            this.description(info.description);
            this.author(info.author);
        });
    }

    /**
     * Get WebRequest specific for Box Maintenance module in Web API access.
     * @readonly
     */
    get webRequestBoxMaintenance() {
        return WebRequestBoxMaintenance.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestBoxMaintenance.getBoxMaintenance(this.id).done((response) => {
            this.formatDateTime(response.formatDateTime);
            this.description(response.description);
            this.author(response.author);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateBoxMaintenance)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteBoxMaintenance)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-box-view-maintenance-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.id })
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M037")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestBoxMaintenance.deleteBoxMaintenance(this.id).done(() => {
                                this.isBusy(false);
                                this.publishMessage("ca-asset-box-maintenance-deleted");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxMaintenanceViewScreen),
    template: templateMarkup
};