﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestBox from "../../../../../app/frameworks/data/apitrackingcore/webrequestBox";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";

class BoxViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Assets_ViewBox")());
        this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id, 0);

        this.imei = "";
        this.serialNo = "";
        this.vendor = "";
        this.isForRent = "";
        this.businessUnit = "";
        this.status = "";
        this.installedDateTime = "";
        this.installedDateTimeVisible = false;
        this.trackInterval = "";
        this.adjustTemperature = "";
        this.isCalculateDistanceByMileage = "";
        this.enableMultipath = "";
        this.template = "";
        // DataTable need observableArray
        this.inputEvents = ko.observableArray([]); 
        this.outputEvents = ko.observableArray([]);
        this.inputAccessories = ko.observableArray([]);
        this.outputAccessories = ko.observableArray([]);
    }
    /**
     * Get WebRequest specific for Box module in Web API access.
     * @readonly
     */
    get webRequestBox() {
        return WebRequestBox.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestBox.getBox(this.id, [ EntityAssociation.Box.BusinessUnit, EntityAssociation.Box.Features, EntityAssociation.Box.Vendor ]).done((box) => {
            this.imei = box.imei;
            this.serialNo = box.serialNo;
            this.vendor = box.vendor ? box.vendor.name : "";
            this.isForRent = box.formatIsForRent;
            this.businessUnit = box.businessUnitName;
            this.status = box.boxStatusDisplayName;
            if (box.boxStatus === Enums.ModelData.BoxStatus.Ready) {
                this.installedDateTimeVisible = true;
                this.installedDateTime = box.formatInstalledDate;
            }
            this.trackInterval = box.formatTrackInterval;
            this.adjustTemperature = box.formatAdjustTemperature;
            this.isCalculateDistanceByMileage = box.formatIsCalculateDistanceByMileage;
            this.enableMultipath = box.formatEnableMultipath;
            this.template = box.boxTemplateName;

            ko.utils.arrayForEach(box.features, (feature) => {
                // display only if feature is installed
                if (feature.isInstall) {
                    switch (feature.eventType) {
                        case Enums.ModelData.BoxFeatureEventType.InputEvent:
                            this.inputEvents.push(feature);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.OutputEvent:
                            this.outputEvents.push(feature);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.InputAccessory:
                            this.inputAccessories.push(feature);
                            break;
                        case Enums.ModelData.BoxFeatureEventType.OutputAccessory:
                            this.outputAccessories.push(feature);
                            break;
                    }
                } 
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdMaintenance", this.i18n("Common_Maintenances")(), "svg-cmd-maintenance"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdMaintenance":
                this.navigate("ca-asset-box-view-maintenance", { boxId: this.id });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(BoxViewScreen),
    template: templateMarkup
};