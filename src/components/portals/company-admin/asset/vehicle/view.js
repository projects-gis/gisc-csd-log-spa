﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import { Enums, Constants, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";


class VehicleViewScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_ViewVehicle")());
        this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id);

        this.images = ko.observableArray([]);
        this.type = ko.observable(null);
        this.typeDisplayName = ko.observable("");
        this.ermTypeDisplayName = ko.observable("");
        this.isNotTrail = ko.pureComputed(() => {
            return _.isNil(this.type()) || this.type() !== Enums.ModelData.VehicleType.Trail;
        });
        this.isTrailer = ko.pureComputed(() => {
            return this.type() && this.type() === Enums.ModelData.VehicleType.Trailer;
        });
        this.model = ko.observable("");
        this.icon = ko.observable(null);
        this.businessUnit = ko.observable("");
        this.enable = ko.observable("");
        this.chassisNo = ko.observable("");
        this.category = ko.observable("");
        this.fleetType = ko.observable("");
        this.seat = ko.observable("");
        this.volume = ko.observable("");
        this.volumeUnit = ko.observable("");
        this.description = ko.observable("");
        this.referenceNo = ko.observable("");
        this.fuelRate = ko.observable("");
        this.lastDateOdometer = ko.observable("");
        this.stopDuration = ko.observable("");
        this.registeredType = ko.observable("");
        
        this.registeredTypeVisible = ko.pureComputed(() => {
            return this.type() && [Enums.ModelData.VehicleType.Truck, Enums.ModelData.VehicleType.Trailer, Enums.ModelData.VehicleType.PassengerCar].indexOf(this.type()) > -1;
        });
        this.trail = ko.observable("");
        this.overSpeedDelay = ko.observable("");
        this.parkEngineOnLimit = ko.observable("");
        this.defaultOverSpeedLimit = ko.observable("");
        this.selectedTabIndex = ko.observable(0);

        // License Details
        this.license = ko.observable("");
        this.licensePlate = ko.observable("");
        this.licenseProvince = ko.observable("");
        this.licenseCountry = ko.observable("");

        // Default Driver
        this.defaultDriverName = ko.observable("");
        this.coDriver1Name = ko.observable("");
        this.coDriver2Name = ko.observable("");

        // Alert Configurations
        this.smsAlert = ko.observable("");
        this.emailAlert = ko.observable("");
        this.alertConfigurations = ko.observableArray([]);

        // Subscribe Message when vehicle create/update/delete
        this.subscribeMessage("ca-asset-vehicle-changed", () => {
            this.isBusy(true);
            this.webRequestVehicle.getVehicle(this.id, this.generateEnityAssociations()).done((vehicle) => {
                this.vehicleData = vehicle;
                this.dmsItem(vehicle.dmsInfos);
                this.adsItem(vehicle.adasInfos);
                this.generateViewModel(vehicle);
                this.inverseFeaturesItems(_.size(vehicle.inverseFeatureInfos) ? vehicle.inverseFeatureInfos : []);
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        });

        this.vehicleData = null;

        this.defaultMDVRModel = ko.observable();
        this.defaultMDVR = ko.observable();
        this.dmsItem = ko.observableArray([]);
        this.orderDms = ko.observable([[1, "asc"]]);
        this.adsItem = ko.observableArray([]);
        this.orderAds = ko.observable([[1, "asc"]]);
        this.orderinverseFeatures = ko.observable([[0, "asc"]]);
        this.inverseFeaturesItems = ko.observableArray([]);
        this.dcpoiName = ko.observable();

        this.installedDate = ko.observable();
        this.isShowOdometerFixing = ko.observable();
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var w1 = this.webRequestVehicle.getVehicle(this.id, this.generateEnityAssociations());
        var w2 = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId);

        $.when(w1,w2).done((vehicle,company) => {

            this.isShowOdometerFixing(company.enableOdometerFixing);

            if(vehicle.sysCalculation == null){
                vehicle.sysCalculation = 100;
            }
            this.vehicleData = vehicle;
            this.generateViewModel(vehicle);
            this.dmsItem(vehicle.dmsInfos);
            this.adsItem(vehicle.adasInfos);
            this.inverseFeaturesItems(_.size(vehicle.inverseFeatureInfos) ? vehicle.inverseFeatureInfos : []);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }

    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicle)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.ViewMaintenance) ||
            WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance)) 
        {
            commands.push(this.createCommand("cmdMaintenance", this.i18n("Common_Maintenances")(), "svg-cmd-maintenance"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicle) && this.isNotTrail()) {
            commands.push(this.createCommand("cmdOdoMeter", this.i18n("Common_ODOMeter")(), "svg-cmd-odometer"));
        }
        if (this.isShowOdometerFixing()) {
            commands.push(this.createCommand("cmdOdoMeterFixing", this.i18n("Assets_OdoMeterFixing")(), "svg-cmd-odometer"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteVehicle)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-vehicle-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.id });
                break;
            case "cmdMaintenance":
                this.navigate("ca-asset-vehicle-view-maintenance-plan", { id: this.id, vehicle: this.vehicleData });
                break;
            case "cmdOdoMeter":
                this.navigate("ca-asset-vehicle-view-odo-meter-manage", { id: this.id , lastDateOdometer: this.lastDateOdometer()});
                break;
            case "cmdOdoMeterFixing":
                this.navigate("ca-asset-vehicle-view-odo-meter-fixing", 
                { 
                    id: this.id ,
                    vehicle : this.vehicleData,

                });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M030")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);
                                this.webRequestVehicle.deleteVehicle(this.id).done(() => {
                                    this.isBusy(false);
                                    this.publishMessage("ca-asset-vehicle-changed");
                                    this.close(true);
                                }).fail((e) => {
                                    this.isBusy(false);
                                    this.handleError(e);
                                });
                                break;
                        }
                    });
                break;
        }
    }

    /**
     * 
     * Generate view model from BL info
     * @param {any} vehicle
     * 
     * @memberOf VehicleViewScreen
     */
    generateViewModel(vehicle) {
        this.images.replaceAll(vehicle.images || []);
        this.type(vehicle.vehicleType);
        this.typeDisplayName(vehicle.vehicleTypeDisplayName);
        this.ermTypeDisplayName(vehicle.ermVehicleTypeDisplayName);
        this.model(Utility.stringFormat("{0} - {1}", vehicle.brand, vehicle.model));
        this.icon(vehicle.icon && vehicle.icon.previewImage ? vehicle.icon.previewImage.fileUrl : "");
        this.businessUnit(vehicle.businessUnitName);
        this.enable(vehicle.formatEnable);
        this.category(vehicle.categoryName);
        this.fleetType(vehicle.fleetTypeDisplayName);
        this.chassisNo(vehicle.chassisNo);
        this.seat(vehicle.formatSeat);
        this.volume(vehicle.formatVolume);
        this.volumeUnit(vehicle.volumeUnitSymbol);
        this.description(vehicle.description);
        this.referenceNo(vehicle.referenceNo);
        this.stopDuration(vehicle.formatStopDuration);
        this.lastDateOdometer(vehicle.lastDateOdometer);
        this.fuelRate(vehicle.formatFuelRate);
        this.registeredType(vehicle.vehicleRegisteredType ? vehicle.vehicleRegisteredType.displayName : "");
        this.trail(vehicle.trail && vehicle.trail.license ? vehicle.trail.license.license : "");
        this.overSpeedDelay(vehicle.formatOverSpeedDelay);
        this.parkEngineOnLimit(vehicle.formatParkEngineOnLimit);
        this.defaultOverSpeedLimit(vehicle.formatDefaultOverSpeedLimit);
        this.installedDate(vehicle.formatInstalledDate);

        this.defaultMDVRModel(vehicle.mdvrModel);
        this.defaultMDVR(vehicle.mdvrDeviceId);
        this.dcpoiName(vehicle.dcpoiName);

        // License Details
        this.license(vehicle.license ? vehicle.license.license : "");
        this.licensePlate(vehicle.license ? vehicle.license.licensePlate : "");
        this.licenseProvince(vehicle.license ? vehicle.license.licenseProvince : "");
        this.licenseCountry(vehicle.license ? vehicle.license.licenseCountry : "");

        // Default Driver
        this.defaultDriverName(vehicle.defaultDriverDisplayName);
        this.coDriver1Name(vehicle.coDriver1DisplayName);
        this.coDriver2Name(vehicle.coDriver2DisplayName);

        //Alert Configuration
        this.smsAlert(vehicle.smsAlert);
        this.emailAlert(vehicle.emailAlert);
        this.alertConfigurations.replaceAll(vehicle.alertConfigurations);

        this.installedDate(vehicle.formatInstalledDate);
    }

    /**
     * 
     * Generate entityAssociations array for webRequest getVehicle
     * @returns
     * 
     * @memberOf VehicleViewScreen
     */
    generateEnityAssociations() {
        return [EntityAssociation.Vehicle.Images,
        EntityAssociation.Vehicle.Icon,
        EntityAssociation.Vehicle.Trail,
        EntityAssociation.Vehicle.DefaultDriver,
        EntityAssociation.Vehicle.CoDriver1,
        EntityAssociation.Vehicle.CoDriver2,
        EntityAssociation.Vehicle.AlertConfigurations,
        EntityAssociation.Vehicle.Model,
        EntityAssociation.Vehicle.VehicleRegisteredType,
        EntityAssociation.Vehicle.License,
        EntityAssociation.Vehicle.InverseFeatures];
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleViewScreen),
    template: templateMarkup
};