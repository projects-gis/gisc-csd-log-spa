﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan-cancel.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebRequestVehicleMaintenanceType from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenanceType";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
/**
 * Display available asset menu based on user permission.
 * @class AssetMenuScreen
 * @extends {ScreenBase}
 */
class MaintenancePlanCancel extends ScreenBase {
    
    /**
     * Creates an instance of AssetMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.vehicleData = params.info;

        this.bladeTitle(this.i18n("Vehicle_MA_Cancel_Maintenance_Plan")());

        this.remark = ko.observable();

     

    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }
    }
    setupExtend() {

     //   this.remark.extend({ required: true });


        this.remark.extend({ 
            required: true,
            validation: {
                validator: () => {
                    //var validation = true;
                    return this.remark().length <= 1000;                    
                },
                message: this.i18n("M147 ")()
            },
            trackChange: true
        });


        this.validationModel = ko.validatedObservable({
            remark: this.remark         
        });


    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        
    }

 
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.vehicleData.remark = this.remark();

            this.showMessageBox(null, this.i18n("M069")(),
                BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestVehicleMaintenancePlan.cancelVehicleMaintenancePlan(this.vehicleData).done(() => {
                                this.publishMessage("ca-asset-vehicle-maintenance-plan-updated");
                                //   this.publishMessage("ca-asset-vehicle-maintenance-plan-view-updated");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });

        }
    }

    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {

    }
}

export default {
viewModel: ScreenBase.createFactory(MaintenancePlanCancel),
    template: templateMarkup
};