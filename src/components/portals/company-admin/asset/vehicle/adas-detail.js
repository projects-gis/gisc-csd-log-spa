﻿import ko from "knockout";
import templateMarkup from "text!./adas-detail.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestADAS from "../../../../../app/frameworks/data/apitrackingcore/webRequestADAS";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebRequestADASModel from "../../../../../app/frameworks/data/apitrackingcore/WebRequestADASModel";

class DMSScreen extends ScreenBase {
    constructor(params) {
        
        super(params);
        this.mode = this.ensureNonObservable(params.adsmode);
        this.bladeTitle(this.i18n("Assets_ADASDetail")());
        this.modeEnableModel = this.mode === Screen.SCREEN_MODE_CREATE ? true : null;
        this.modeDevice = this.mode === Screen.SCREEN_MODE_CREATE ? false : null;
        this.dataTable = this.ensureNonObservable(params.dataTable,[]); 
        this.currentAds = this.ensureNonObservable(params.selectedData,null);
        this.vehicleId = this.ensureNonObservable(params.vehicleId,0);

        this.adsModelOptions = ko.observableArray([]);
        this.adsDeviceOptions = ko.observableArray([]);
        this.selectedADSModel = ko.observable();
        this.selectedADSDevice = ko.observable();

        this.selectedADSModel.subscribe((val) => {
            if(val){
                this.webRequestADAS.listADASSummary({modelId : val.id,inuse : this.modeDevice}).done((resp) => {
                    let arrRefId = this.dataTable.map(x => x.deviceId);
                    let itemCompute = [];
                    
                    if(arrRefId && arrRefId.length > 0){
                        _.each(resp.items,(item) => {
                            let checkExistItem = $.grep(arrRefId,(x) => {
                                return x == item.deviceId
                            });
                            if(checkExistItem.length == 0 || (this.currentAds && this.currentAds.id == item.id)){
                                itemCompute.push(item);
                            }
                        });
                    }
                    else{
                        itemCompute = resp.items;
                    }
                    
                    this.adsDeviceOptions(itemCompute);
                    if(this.currentAds){
                        this.selectedADSDevice(ScreenHelper.findOptionByProperty(this.adsDeviceOptions,"deviceId",this.currentAds.deviceId));
                    }
                       
                });
            }
            else {
                this.adsDeviceOptions([]);
            }
        });
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestADAS() {
        return WebRequestADAS.getInstance();
    }
    get webRequestADASModel() {
        return WebRequestADASModel.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();
        
        let modelId = this.currentAds ? this.currentAds.adasModelId : -1 ; 
        var deviceId = this.currentAds ? this.currentAds.deviceId : null;
        var d1 = this.webRequestADASModel.listADASModelSummary({enable : this.modeEnableModel});
        var d2 = this.webRequestADAS.listADASSummary({modelId : modelId,inuse : this.modeDevice});

        $.when(d1, d2).done((r1, r2) => {
            
            this.adsModelOptions(r1.items);
            this.adsDeviceOptions(r2.items);
            if(modelId != -1){
                this.selectedADSModel(ScreenHelper.findOptionByProperty(this.adsModelOptions,"id",modelId));
                this.selectedADSDevice(ScreenHelper.findOptionByProperty(this.adsDeviceOptions,"deviceId", deviceId));
            }
           
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.selectedADSDevice.extend({ trackChange: true });
        this.selectedADSModel.extend({ trackChange: true });
        this.selectedADSDevice.extend({ required: true });
        this.selectedADSModel.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            selectedADSDevice : this.selectedADSDevice,
            selectedADSModel : this.selectedADSModel
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {

    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_OK")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate fleet service model from View Model
     * 
     * @returns fleet service model which is ready for ajax request 
     */
    generateModel() {
        return { 
                id : this.selectedADSDevice().id,
                adasModelName : this.selectedADSModel().model ,  
                deviceId : this.selectedADSDevice().deviceId,
                adasModelId : this.selectedADSModel().id,
                infoStatus : Enums.InfoStatus.Add
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id === "actSave"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var result = this.generateModel();
            this.webRequestADAS.CheckUsingDevice({ids:[result.id],vehicleId :this.vehicleId }).done((resp) => {
                if(resp){
                    this.showMessageBox(null, this.i18n("M233")(),BladeDialog.DIALOG_YESNO).done((button) => {
                        switch(button){
                            case BladeDialog.BUTTON_NO :
                                return;
                                break;
                            default :
                                this.manageADASItem(result);
                        }
                    });
                }
                else{
                    this.manageADASItem(result);
                }
               
            });
            


            
            
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    
    manageADASItem(result){
        var strMessage = null;
        var rowId = 0;
        if(this.dataTable && this.dataTable.length > 0){
            rowId = Math.max.apply(Math, this.dataTable.map(function (item) { return item.rowId; })); //หา maxId จาก object array 
        }
            
        if(this.currentAds){
            strMessage = Enums.InfoStatus.Update;
            let itemADS = this.dataTable;
            _.each(this.dataTable,(item) => {
                if(item.rowId == this.currentAds.rowId){
                    item.id = result.id;
                    item.adasModelName = result.adasModelName;
                    item.deviceId = result.deviceId;
                    item.adasModelId = result.adasModelId;
                    item.infoStatus = Enums.InfoStatus.Update;
                }
            });
            this.publishMessage("ca-vehicle-ads-addItem",{ source : itemADS,message : strMessage , selectedRowId : this.currentAds.rowId}); // sent data to replaceAll in manage.js
        }
            // เมื่อเป็นการ Add     
        else{
            result.rowId = rowId+1;
            this.publishMessage("ca-vehicle-ads-addItem",{ source : result,message : strMessage,selectedRowId : result.rowId}); // sent data to replaceAll in manage.js
        }

        this.close(true);
                    
    }

}

export default {
viewModel: ScreenBase.createFactory(DMSScreen),
    template: templateMarkup
};