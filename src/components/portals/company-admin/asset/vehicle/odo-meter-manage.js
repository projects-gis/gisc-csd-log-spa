﻿import ko from "knockout";
import templateMarkup from "text!./odo-meter-manage.html";
import ScreenBase from "../../../screenbase";
// import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import moment from "moment";

class VehicleManageOdoMeterScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_ODOMeter")());
        // this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id);
        //this.lastDateOdometer = params.lastDateOdometer ? params.lastDateOdometer : new Date();

        this.currentDistance = ko.observable("");
        this.distanceUnitSymbol = ko.observable("");
        this.distanceUnit = ko.observable(null);
        this.newDistance = ko.observable("");
        this.useCurrent = ko.observable(false);

        this.dateTime = ko.observable();
        
        this.lastArchivedDate = ko.observable(null);
        this.dateTimeEnable = ko.pureComputed(() => {
            return this.useCurrent() === false;
        });

        this.currDate = moment(new Date()).format();

        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.timeFormat = WebConfig.companySettings.shortTimeFormat;
        this.minDate = params.lastDateOdometer ? params.lastDateOdometer : '-3m' ;
        this.minTime = params.lastDateOdometer ? moment(params.lastDateOdometer).format("HH:mm") : '';
        this.maxDate = this.currDate ;

        this._changeUseCurrentSubscribe = this.useCurrent.subscribe((useCurrent) => {
            if (useCurrent === true) {
                this.dateTime(null);
            }
        });

        this.historyItems = ko.observableArray([]);
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestVehicle.summaryVehicleOdometer({id : this.id}).done((response) => {
            this.historyItems(response);

            //this.vehicles(this.getVehicleDatasource(response["items"]));
            //dfd.resolve();
        }).fail((e) => {
            //dfd.reject(e);
        });

        this.webRequestVehicle.getCurrentDistance(this.id).done((currentDistance) => {
            this.currentDistance(currentDistance.formatCurrentDistance);
            this.distanceUnitSymbol(currentDistance.distanceUnitSymbol);
            this.distanceUnit(currentDistance.distanceUnit);
            if (currentDistance.lastArchivedDate) {
                this.lastArchivedDate(currentDistance.lastArchivedDate);
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    setupExtend() {
        this.newDistance.extend({ trackChange: true });
        this.dateTime.extend({ trackChange: true });
        this.useCurrent.extend({ trackChange: true });

        this.newDistance.extend({ required: true, min: 0.01 });
        this.dateTime.extend({
            required: {
                onlyIf: () => {
                    return this.useCurrent() === false;
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            newDistance: this.newDistance,
            dateTime: this.dateTime,
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            this.webRequestVehicle.updateOdoMeter(this.generateModel()).done(() => {
                this.publishMessage("ca-asset-vehicle-changed");
                this.close(true);
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            })
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }


    /**
     * 
     * Generate VehicleOdoMeterInfo from view model
     * @returns
     * 
     * @memberOf VehicleOdoMeterManageScreen
     */
    generateModel() {
        return {
            newDistance: this.newDistance(),
            currentDate: this.currDate,
            date: this.useCurrent() ? this.currDate :  this.dateTime(),
            useCurrent: this.useCurrent(),
            infoStatus: Enums.InfoStatus.Update,
            id: this.id
        };
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleManageOdoMeterScreen),
    template: templateMarkup
};