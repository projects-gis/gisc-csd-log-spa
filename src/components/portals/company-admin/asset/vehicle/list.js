﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Enums, Constants} from "../../../../../app/frameworks/constant/apiConstant";
import { VehicleFilter } from "./infos";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";


class VehicleListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Vehicles")());
        this.bladeSize = BladeSize.Large;
        this.distanceUnit = ko.observable();
        this.vehicles = ko.observableArray([]);
        this.filterText = ko.observable("");
        this.selectedVehicle = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.searchFilter = ko.observable(new VehicleFilter());
        this.searchFilterDefault = new VehicleFilter(); // Use to compare with searchFilter to apply active state
        this.pageFiltering = ko.observable(false); // Use to apply active state for command search
        this.order = ko.observable([[ 0, "asc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (vehicle) => {
            if(vehicle) {
                return this.navigate("ca-asset-vehicle-view", { id: vehicle.id });
            }
            return false;
        };

        // Subscribe Message when vehicle create/update/delete
        this.subscribeMessage("ca-asset-vehicle-changed", (vehicleIds) => {
            this.isBusy(true);
            this.webRequestVehicle.listVehicleSummary(this.generateFilter()).done((response) => {
                this.vehicles.replaceAll(this.getVehicleDatasource(response["items"]));
                this.recentChangedRowIds.replaceAll(vehicleIds ? vehicleIds : []);
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        });

        // Subscribe Message when box search filter apply
        this.subscribeMessage("ca-asset-vehicle-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter(searchFilter);

            this.webRequestVehicle.listVehicleSummary(this.generateFilter()).done((response) => {
                this.vehicles.replaceAll(this.getVehicleDatasource(response["items"]));
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        });

        this.onVehicleMaintenancePlanListTableReady = () => {
            if (!_.isEmpty(this.distanceUnit())) {
                this.dispatchEvent("dtVehicleMaintenancePlanList2", "setColumnTitle", [{
                    columnIndex: 7,
                    columnTitle: this.i18n("Vehicle_MA_Common_AccDistance", [this.distanceUnit()])
                }]);
            }
        };


        // subscribe on searchFilter change to apply filter state
        this._changeSearchFilterSubscribe = this.searchFilter.subscribe((searchFilter) => {
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter(), this.searchFilterDefault));
        });

        // change searchFilter after subscribe to make sure active state also applied
        if (params.businessUnitId) {
            this.searchFilter(new VehicleFilter(params.businessUnitId));
        }
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
        });


        this.webRequestVehicle.listVehicleSummary(this.generateFilter()).done((response) => {
            this.vehicles(this.getVehicleDatasource(response["items"]));
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedVehicle(null);
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateVehicle)){
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.CreateVehicle) && WebConfig.userSession.hasPermission(Constants.Permission.UpdateVehicle)){
            commands.push(this.createCommand("cmdImport", this.i18n("Common_Import")(), "svg-cmd-import"));
        }
        commands.push(this.createCommand("cmdExport", this.i18n("Common_Export")(), "svg-cmd-export"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdSearch":
                this.navigate("ca-asset-vehicle-search", { searchFilter: this.searchFilter() });
                break;
            case "cmdCreate":
                this.navigate("ca-asset-vehicle-manage", { mode: Screen.SCREEN_MODE_CREATE });
                break;
            case "cmdImport":
                this.navigate("ca-asset-vehicle-import");
                break;
            case "cmdExport":
                this.showMessageBox(null, this.i18n("M128")(), BladeDialog.DIALOG_YESNO).done((button)=> {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            var filter = $.extend(true, {}, this.generateFilter());
                            filter.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;
                            filter.sortingColumns = this.generateSortingColumns();
                            this.webRequestVehicle.exportVehicle(filter).done((response) => {
                                this.isBusy(false);
                                ScreenHelper.downloadFile(response.fileUrl);
                            }).fail((e) => {
                                this.isBusy(false);
                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
        
        this.selectedVehicle(null);
        this.recentChangedRowIds.removeAll();
    }
    /**
     * Generate VehicleFilter for WebRequest
     * @returns
     */
    generateFilter() {
        return {
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.searchFilter().businessUnitIds ? this.searchFilter().businessUnitIds : null,
            vehicleTypes: this.searchFilter().type ? [ this.searchFilter().type ] : null,
            vehicleModelIds: this.searchFilter().modelId ? [ this.searchFilter().modelId ] : null,
            enable: this.searchFilter().enable,
            sortingColumns: DefaultSorting.Vehicle
        };
    }

    
    /**
     * Get DataTable Datasource
     * 
     * @param {any} items
     * @returns
     * 
     * @memberOf VehicleListScreen
     */
    getVehicleDatasource(items) {
        Utility.applyFormattedPropertyToCollection(items, "vehicleModelDisplayText", "{0} - {1}", "brand", "model");
        return items;
    }

    /**
     * Generate SortingColumns array for export driver filter.
     * @returns
     */
    generateSortingColumns(){
        
        var sortingColumns = [{},{}];
        sortingColumns[0].direction = this.order()[0][1] === "asc" ? 1 : 2 ;

         switch (this.order()[0][0]) {
            case 0:
                sortingColumns[0].column = Enums.SortingColumnName.VehicleLicense;
                break;
            case 1:
            case 2:
                 sortingColumns[0].column = Enums.SortingColumnName.VehicleType;
                break;
            case 3:
                 sortingColumns[0].column = Enums.SortingColumnName.Brand;
                break;
            case 4:
                 sortingColumns[0].column = Enums.SortingColumnName.BusinessUnitName;
                break;        
            case 5:
                 sortingColumns[0].column = Enums.SortingColumnName.FormatEnable;
                break;    
        }

        if(this.order()[0][0] == 3){
            sortingColumns[1].column = Enums.SortingColumnName.Model;
            sortingColumns[1].direction = sortingColumns[0].direction;

            sortingColumns.push({column: Enums.SortingColumnName.Id, direction: sortingColumns[0].direction} );
        }else{
            sortingColumns[1].column = Enums.SortingColumnName.Id;
            sortingColumns[1].direction = sortingColumns[0].direction;
        }
        return sortingColumns;
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleListScreen),
    template: templateMarkup
};