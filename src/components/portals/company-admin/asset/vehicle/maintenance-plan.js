﻿import ko from "knockout";
import templateMarkup from "text!./maintenance-plan.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestCompany from "../../../../../app/frameworks/data/apicore/webrequestCompany";
import WebRequestVehicleMaintenancePlan from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleMaintenancePlan";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";

class VehicleMaintenancePlanListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.bladeTitle(this.i18n("Assets_MaintenancePlans")());
        this.bladeSize = BladeSize.XLarge_A0;

        this.vehicleId = this.ensureNonObservable(params.id);
        this.filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            //vehicleIds: [this.vehicleId]
            vehicleId: this.vehicleId
        };
        this.distanceUnit = ko.observable();
        this.vehicleMaintenancePlans = ko.observableArray([]);
        this.order = ko.observable([]);
        this.selectedVehicleMaintenancePlan = ko.observable();
        this.filterText = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);

        this.vehicleData = params.vehicle;

        this._selectingRowHandler = (row) => {
            if (row) {
                if (row.vehicleMaintenanceType == Enums.ModelData.VehicleMaintenanceType.Log) {
                    return this.navigate("ca-asset-vehicle-view-maintenance-plan-view-log", { id: row.id, vehicleId: this.vehicleId, appointment: row, vehicle: this.vehicleData, isFromAppointment: false });
                }
                else if (row.maintenanceStatus == Enums.ModelData.MaintenanceStatus.Cancelled) {
                    return this.navigate("ca-asset-vehicle-view-maintenance-plan-view", { id: row.id, vehicleId: this.vehicleId, appointment: row, vehicle: this.vehicleData, isFromAppointment: false });
                }
                else {
                    return this.navigate("ca-asset-vehicle-view-maintenance-plan-view", { id: row.id, vehicleId: this.vehicleId, appointment: row, vehicle: this.vehicleData, isFromAppointment: false });
                }
            }
            return false;
        };

        // Set custom column title with DataTable
        this.onVehicleMaintenancePlanListTableReady = () => {
            if (!_.isEmpty(this.distanceUnit())) {
                this.dispatchEvent("dtVehicleMaintenancePlanList", "setColumnTitle", [{
                    columnIndex: 5,
                    columnTitle: this.i18n("Vehicle_MA_Common_AccDistance", [this.distanceUnit()])
                }]);
            }
        };

        // Subscribe Message when vehicle maintenance plan created
        this.subscribeMessage("ca-asset-vehicle-maintenance-plan-created", (info) => {
            var infoID = [];
            for (var i = 0; i < info.length; i++) {
                infoID.push(info[i].id);
            }
            this.refreshDatasource().done(() => {
                this.recentChangedRowIds.replaceAll(infoID);
            });
        });

        // Subscribe Message when vehicle maintenance plan updated
        this.subscribeMessage("ca-asset-vehicle-maintenance-plan-updated", (info) => {
            this.refreshDatasource();
        });

        // Subscribe Message when vehicle maintenance plan deleted
        this.subscribeMessage("ca-asset-vehicle-maintenance-plan-deleted", () => {
            this.refreshDatasource();
            this.selectedVehicleMaintenancePlan(null);
        });

        // Subscribe Message when appointment created
        this.subscribeMessage("ca-asset-maintenance-management-appointment-manage-save-completed", (result) => {
            this.refreshDatasource();
            //this.selectedVehicleMaintenancePlan(result);
        });

        this.subscribeMessage("ca-asset-maintenance-management-appointment-delete", () => {
            this.refreshDatasource();
        });
    }

    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }

    /**
     * Get WebRequest specific for Vehicle Maintenance Plan module in Web API access.
     * @readonly
     */
    get webRequestVehicleMaintenancePlan() {
        return WebRequestVehicleMaintenancePlan.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var dfdCompanySettings = this.webRequestCompany.getCompanySetting(WebConfig.userSession.currentCompanyId).done((response) => {
            this.distanceUnit(response.distanceUnitSymbol);
        });

        var dfdVehicleMaintenancePlan = this.webRequestVehicleMaintenancePlan.listVehicleMaintenancePlanSummary(this.filter).done((response) => {

            for (var i = 0; i < response.items.length; i++) {
                var rs = response.items[i];
                var iconAppointment = rs.isAppointment ? "~/images/ic_table_appointment.svg" : "~/images/ic_table_notappointment.svg";

                //set icon
                rs.icon = this.resolveUrl(iconAppointment);

                //check Maintenance type
                switch (rs.vehicleMaintenanceType) {
                    case 2:
                        rs.icon = undefined;
                        break;
                    default:
                        break;
                }
            }

            this.vehicleMaintenancePlans(response.items);
        });

        // Click Icon Appointment
        this.clickIcon = (data) => {
            /*
            * เป็นจริงเปิดหน้า Appointment 
            * เป็นเท็จเปิดหน้า Create Appointment
            */

            if (data.isAppointment) {
                this.navigate("ca-asset-maintenance-management-appointment", { appointment: data, vehicle: this.vehicleData, isFromAppointment: false });
            }
            if (!data.isAppointment) {
                if (data.maintenanceStatus != Enums.ModelData.MaintenanceStatus.Cancelled && data.maintenanceStatus != Enums.ModelData.MaintenanceStatus.Completed){
                    this.navigate("ca-asset-maintenance-management-appointment-manage", { appointment: data, vehicle: this.vehicleData, mode: "create", isFromAppointment: false });
                }                
            }
        };

        $.when(dfdCompanySettings, dfdVehicleMaintenancePlan).done(() => {
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedVehicleMaintenancePlan(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateVehicleMaintenancePlan)
            || WebConfig.userSession.hasPermission(Constants.Permission.UpdateMaintenance)) {
            commands.push(this.createCommand("cmdCreateMaPlan", this.i18n("Vehicle_MA_Btn_Create_Maintenance_Plan")(), "svg-cmd-add"));
            commands.push(this.createCommand("cmdCreateMaLog", this.i18n("Vehicle_MA_Btn_Create Maintenance Log")(), "svg-cmd-add"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreateMaPlan":
                this.navigate("ca-asset-vehicle-view-maintenance-plan-manage", { mode: Screen.SCREEN_MODE_CREATE, vehicleId: this.vehicleId });
                break;
            case "cmdCreateMaLog":
                this.navigate("ca-asset-vehicle-view-maintenance-plan-create-log", { mode: Screen.SCREEN_MODE_CREATE, vehicleId: this.vehicleId });
                //alert("อยู่ในระหว่างการพัฒนา");
                break;
        }

        this.selectedVehicleMaintenancePlan(null);
        this.recentChangedRowIds.removeAll();
    }


    /**
     * Refresh DataTable datasource
     */
    refreshDatasource() {
        this.isBusy(true);
        return this.webRequestVehicleMaintenancePlan.listVehicleMaintenancePlanSummary(this.filter).done((response) => {

            for (var i = 0; i < response.items.length; i++) {
                var rs = response.items[i];
                var iconAppointment = rs.isAppointment ? "~/images/ic_table_appointment.svg" : "~/images/ic_table_notappointment.svg";

                //set icon
                rs.icon = this.resolveUrl(iconAppointment);

                //check Maintenance type
                switch (rs.vehicleMaintenanceType) {
                    case 2:
                        rs.icon = undefined;
                        break;
                    default:
                        break;
                }
            }

            this.vehicleMaintenancePlans.replaceAll(response.items);
            this.isBusy(false);
        }).fail((e) => {
            this.handleError(e);
            this.isBusy(false);
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleMaintenancePlanListScreen),
    template: templateMarkup
};