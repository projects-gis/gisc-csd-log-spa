﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import { VehicleFilter } from "./infos";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestVehicleModel from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicleModel";
import Utility from "../../../../../app/frameworks/core/utility";

class VehicleSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.businessUnitId());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.type = ko.observable();
        this.model = ko.observable();
        this.businessUnitId = ko.observable(null);
        this.enable = ko.observable();

        this.typeOptions = ko.observableArray([]);
        this.modelOptions = ko.observableArray([]);
        this.businessUnitOptions = ko.observableArray([]);
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle Model module in Web API access.
     * @readonly
     */
    get webRequestVehicleModel() {
        return WebRequestVehicleModel.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        var d1 = this.webRequestEnumResource.listEnumResource({ companyId: WebConfig.userSession.currentCompanyId, types: [ Enums.ModelData.EnumResourceType.VehicleType ], sortingColumns: DefaultSorting.EnumResource });
        var d2 = this.webRequestVehicleModel.listVehicleModelSummary({ sortingColumns: DefaultSorting.VehicleModelByBrandModel });
        var d3 = this.webRequestBusinessUnit.listBusinessUnitSummary({ companyId: WebConfig.userSession.currentCompanyId, includeAssociationNames: [ EntityAssociation.BusinessUnit.ChildBusinessUnits ], sortingColumns: DefaultSorting.BusinessUnit });

        $.when(d1, d2, d3).done((r1, r2, r3) => {
            this.typeOptions(r1["items"]);

            Utility.applyFormattedPropertyToCollection(r2["items"], "displayText", "{0} - {1}", "brand", "model"); 
            this.modelOptions(r2["items"]);

            this.businessUnitOptions(r3["items"]);

            if (this.searchFilter.type) {
                this.type(ScreenHelper.findOptionByProperty(this.typeOptions, "value", this.searchFilter.type));
            }
            if (this.searchFilter.modelId) {
                this.model(ScreenHelper.findOptionByProperty(this.modelOptions, "id", this.searchFilter.modelId));
            }
            if (this.searchFilter.businessUnitId) {
                this.businessUnitId(this.searchFilter.businessUnitId.toString());
            }
            if (this.searchFilter.enable) {
                this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", this.searchFilter.enable));
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {
            this.searchFilter = new VehicleFilter(
                null,
                this.type() ? this.type().value : null,
                this.model() ? this.model().id : null,
                this.enable() ? this.enable().value : null
            );

            // Default selection is single.
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.businessUnitId());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    this.searchFilter.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnitOptions(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    this.searchFilter.businessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.publishMessage("ca-asset-vehicle-search-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.type(null);
            this.model(null);
            this.businessUnitId(null);
            this.enable(null);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleSearchScreen),
    template: templateMarkup
};