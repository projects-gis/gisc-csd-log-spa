import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

class VehicleFilter {
    constructor (businessUnitIds = null,  type = null, modelId = null, enable = null) {
        this.type = type;
        this.modelId = modelId;
        this.businessUnitIds = businessUnitIds;
        this.enable = enable;
    }
}

export { VehicleFilter };

class VehicleLicenseDetails {
    constructor (
        license = "", 
        licensePlate = "", 
        licenseProvince = "", 
        licenseCountry = "", 
        infoStatus = Enums.InfoStatus.Add, 
        id = 0) 
    {
        this.license = license;
        this.licensePlate = licensePlate;
        this.licenseProvince = licenseProvince;
        this.licenseCountry = licenseCountry;
        this.infoStatus = infoStatus;
        this.id = id;
    }
}

export { VehicleLicenseDetails };

class VehicleAlerts {
    constructor (smsAlert = "", emailAlert = "") {
        this.smsAlert = smsAlert;
        this.emailAlert = emailAlert;
    }
}

export { VehicleAlerts };

class VehicleDefaultDrivers {
    constructor (defaultDriverId = null, coDriver1Id = null, coDriver2Id = null) {
        this.defaultDriverId = defaultDriverId;
        this.coDriver1Id = coDriver1Id;
        this.coDriver2Id = coDriver2Id;
    }
}

export { VehicleDefaultDrivers };