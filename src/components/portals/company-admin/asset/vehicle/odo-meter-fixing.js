import ko from "knockout";
import templateMarkup from "text!./odo-meter-fixing.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Enums, Constants, EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";
import WebrequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webrequestVehicle";
import moment from "moment";

class VehicleManageOdoMeterFixingScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Assets_OdoMeterFixing")());
        this.bladeSize = BladeSize.Medium;

        this.id = this.ensureNonObservable(params.id,0);
        let isUseOdoManual = params.vehicle ? params.vehicle.isUseOdoManual : false;
        this.isFixVal = ko.observable(isUseOdoManual);

        this.valManual = ko.observable(100.00);

        let sysCalVal = params.vehicle ? params.vehicle.sysCalculation : 100;
        this.valSystem = `${sysCalVal}%`;

        this.changeHistoryItems = ko.observableArray([]);

        this.isEnableManual = ko.pureComputed(() => {
            return this.isFixVal();
        });

        this.currDate = moment(new Date()).format();


    }

    get webRequestVehicle() {
        return WebrequestVehicle.getInstance();
    }

    setupExtend() {
        this.valManual.extend({
            required: {
                onlyIf: () => {
                    return this.isEnableManual()
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            valManual: this.valManual
        });
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }
        this.webRequestVehicle.summaryVehicleOdoFixing({id: this.id}).done((response) => {
            this.changeHistoryItems(response);

            let itemFixing = response;
            if(_.size(itemFixing)){
                let strManual = this.i18n("Assets_OdometerFixingManual")();
                let itemManual = _.filter(itemFixing , (val) => {
                    return val.mode == strManual;
                });

                if(_.size(itemManual)){
                    _.orderBy(itemManual,['activateDate'], ['desc']);
                    this.valManual(itemManual[0].fixingValue);
                }
                
            }
            //this.vehicles(this.getVehicleDatasource(response["items"]));
            //dfd.resolve();
        }).fail((e) => {
            //dfd.reject(e);
        });


    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {

    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {            

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var modelData = this.generateModel();

            this.webRequestVehicle.vehicleOdoFixing(modelData).done((response) => {
                this.publishMessage("ca-asset-vehicle-changed");
                this.close(true);
            }).fail((e) => {
               this.handleError(e);
            });

        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    /**
     * 
     * Generate VehicleOdoMeterInfo from view model
     * @returns
     * 
     * @memberOf VehicleOdoMeterManageScreen
     */
    generateModel() {
        let model = {
            fixingValue: this.valManual(),
            currentDate: this.currDate,
            vehicleId: this.id,
            mode: this.isFixVal() ? "M" : "S"
        }
        return model;
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleManageOdoMeterFixingScreen),
    template: templateMarkup
};