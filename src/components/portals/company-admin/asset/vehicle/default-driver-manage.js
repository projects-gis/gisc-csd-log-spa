﻿import ko from "knockout";
import templateMarkup from "text!./default-driver-manage.html";
import ScreenBase from "../../../screenbase";
import WebrequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webrequestDriver";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import ScreenHelper from "../../../screenhelper";
import { VehicleDefaultDrivers } from "./infos";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import Utility from "../../../../../app/frameworks/core/utility";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";

class VehicleManageDefaultDriverScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Assets_DefaultDrivers")());

        this.vehicleDriver = this.ensureNonObservable($.extend(true, {}, params.defaultDrivers));
        this.businessUnitId = ko.observable();
        this.item = this.ensureNonObservable(_.cloneDeep(params));

        this.businessUnitOptions = ko.observableArray([]);
        this.defaultDriver = ko.observable();
        this.coDriver1 = ko.observable();
        this.coDriver2 = ko.observable();

        this.driverOptions = ko.observableArray([]);
        this.enableDropdown = this.item ? this.item.enableDropdown : true;
        // Subscribe message when business unit changed from manage screen
        this.subscribeMessage("ca-asset-vehicle-manage-changed-business-unit", (businessUnitId) => {
            this.close(true);

            this.businessUnitId([businessUnitId]);
        });

        // Subscribe message when type change to trail from manage screen
        this.subscribeMessage("ca-asset-vehicle-manage-type-changed", (type) => {
            if (type === Enums.ModelData.VehicleType.Trail) {
                this.close(true);
            }
        });

        this.subscribeMessage("ca-asset-vehicle-confirm-dialog-cahnge-bu-cancel", (val) => {
            this.businessUnitId([val.toString()]);
        });
    }
    /**
     * Get WebRequest specific for Driver module in Web API access.
     * @readonly
     */
    get webRequestDriver() {
        return WebrequestDriver.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();
        var d0 = $.Deferred();
        var d1 = $.Deferred();

        this.webRequestBusinessUnit.listBusinessUnitSummary({
            companyId: WebConfig.userSession.currentCompanyId,
            isIncludeFullPath: true,
            sortingColumns: DefaultSorting.BusinessUnit
        }).done((response) => {
            Utility.applyFormattedPropertyToCollection(response["items"], "displayText", "{0} ({1})", "name", "fullCode");
            this.businessUnitOptions(response.items);
            if (this.item && this.item.businessUnitId) {
                this.businessUnitId(this.item.businessUnitId.toString());
            }
            d1.resolve();
        })

        $.when(d1).done(() =>{
            if (this.businessUnitId()) {
                this.webRequestDriver.listDriverSummary({ companyId: WebConfig.userSession.currentCompanyId, businessUnitIds: [this.businessUnitId()], sortingColumns: DefaultSorting.DriverByFirstNameLastNameEmployeeID }).done((response) => {
                    this.driverOptions(this.getDriverOptions(response["items"]));
    
                    if (this.vehicleDriver.defaultDriverId) {
                        this.defaultDriver(ScreenHelper.findOptionByProperty(this.driverOptions, "id", this.vehicleDriver.defaultDriverId));
                    }
                    if (this.vehicleDriver.coDriver1Id) {
                        this.coDriver1(ScreenHelper.findOptionByProperty(this.driverOptions, "id", this.vehicleDriver.coDriver1Id));
                    }
                    if (this.vehicleDriver.coDriver2Id) {
                        this.coDriver2(ScreenHelper.findOptionByProperty(this.driverOptions, "id", this.vehicleDriver.coDriver2Id));
                    }
    
                    d0.resolve();
                }).fail((e) => {
                    d0.reject(e);
                });
            }
            else {
                d0.resolve();
            }    
        })
        
        $.when(d0).done(() => {

            this._changeBusinessUnitSubscibe = this.businessUnitId.subscribe((businessUnitId) => {

                if(Array.isArray(businessUnitId)){
                    businessUnitId = businessUnitId[0];
                }

                // ถ้า default driver มีค่าอยู่ ไม่ต้อง clear
                //if(!_.size(this.defaultDriver()) && !_.size(this.coDriver1()) && !_.size(this.coDriver2())){
                    this.defaultDriver(null);
                    this.coDriver1(null);
                    this.coDriver2(null);
                //}

                if (businessUnitId) {
                    this.isBusy(true);
                    this.webRequestDriver.listDriverSummary({ companyId: WebConfig.userSession.currentCompanyId, businessUnitIds: [businessUnitId], sortingColumns: DefaultSorting.DriverByFirstNameLastNameEmployeeID }).done((response) => {
                        this.driverOptions.replaceAll(this.getDriverOptions(response["items"]));
                        this.isBusy(false);
                    }).fail((e) => {
                        this.driverOptions.removeAll();
                        this.isBusy(false);
                        this.handleError(e);
                    });
                }
                else {
                    this.driverOptions.removeAll();
                }
            });

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });
        
        return dfd;
    }
    /**
     * Setup trackChange/validation
     * @memberOf VehicleManageLicenseDetails
     */
    setupExtend() {
        this.defaultDriver.extend({ trackChange: true });
        this.coDriver1.extend({ trackChange: true });
        this.coDriver2.extend({ trackChange: true });

        this.coDriver1.extend({ 
            validation: {
                validator: (val) => {
                    var valid = true;

                    var defaultDriverId = this.defaultDriver() ? this.defaultDriver().id : null;

                    if (val && val.id && val.id === defaultDriverId) {
                        valid = false;
                    }
                    return valid;
                },
                message: this.i18n("M071")()
            }
        });

        this.coDriver2.extend({ 
            validation: {
                validator: (val) => {
                    var valid = true;

                    var defaultDriverId = this.defaultDriver() ? this.defaultDriver().id : null;
                    var coDriver1Id = this.coDriver1() ? this.coDriver1().id : null;

                    if (val && val.id && (val.id === defaultDriverId || val.id === coDriver1Id)) {
                        valid = false;
                    }

                    return valid;
                },
                message: this.i18n("M071")()
            }
        });

        this.validationModel = ko.validatedObservable({
            coDriver1: this.coDriver1,
            coDriver2: this.coDriver2
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = new VehicleDefaultDrivers(
                this.defaultDriver() ? this.defaultDriver().id : null,
                this.coDriver1() ? this.coDriver1().id : null,
                this.coDriver2() ? this.coDriver2().id : null
            );
            model.buId = this.businessUnitId();

            this.publishMessage("ca-asset-vehicle-manage-default-drivers-changed", model);
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    
    /**
     * Prepare options in format "firstName lastName (employeeId)"
     * 
     * @param {any} items
     * @returns
     * 
     * @memberOf VehicleManageDefaultDriverScreen
     */
    getDriverOptions(items) {
        Utility.applyFormattedPropertyToCollection(items, "displayText", "{0} {1} ({2})", "firstName", "lastName", "employeeId");
        return items;
    }
}

export default {
    viewModel: ScreenBase.createFactory(VehicleManageDefaultDriverScreen),
    template: templateMarkup
};