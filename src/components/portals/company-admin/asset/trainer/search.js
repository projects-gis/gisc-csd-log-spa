﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import { TrainerFilter } from "./infos";

class AssetTraineSearchScreen extends ScreenBase
{
    constructor(params)
    {
        super(params);

        this.bladeTitle(this.i18n("Assets_Trainer_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.selectedBusinessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.selectedBusinessUnit = ko.observable();
        this.businessUnits = ko.observableArray([]);
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
               EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        };
        
        this.webRequestBusinessUnit.listBusinessUnitSummary(filter).done((r)=> {
            this.businessUnits(r["items"]);
            if(this.searchFilter.businessUnitIds && this.searchFilter.businessUnitIds.length > 0){
                this.selectedBusinessUnit(this.searchFilter.businessUnitIds[0].toString());
            }

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }

    onUnload() {}

    onChildScreenClosed() {}

    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }

    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }

    onActionClick(sender) {
        super.onActionClick(sender);

         if (sender.id === "actSearch") {
            this.searchFilter = new TrainerFilter();

            // Default selection is single.
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.selectedBusinessUnit());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    this.searchFilter.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnits(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    this.searchFilter.businessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.publishMessage("ca-asset-trainer-search-filter-changed", this.searchFilter);
        }
    }

    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.selectedBusinessUnit(null);
        }
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetTraineSearchScreen),
    template: templateMarkup
};