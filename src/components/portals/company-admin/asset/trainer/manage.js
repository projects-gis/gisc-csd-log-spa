﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestAssetTrainer from "../../../../../app/frameworks/data/apitrackingcore/webRequestAssetTrainer";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import {BusinessUnitFilter, EnumResourceFilter} from "./infos";
import {Enums, Constants, EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import FileUploadModel from "../../../../../components/controls/gisc-ui/fileupload/fileuploadModel";
import Utility from "../../../../../app/frameworks/core/utility";

class AssetTraineManageScreen extends ScreenBase
{
    constructor(params)
    {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);
        this.companyId = WebConfig.userSession.currentCompanyId;

        this.canEdit = ko.pureComputed(function() {
            return WebConfig.userSession.hasPermission(Constants.Permission.UpdateTrainer);
            //return true;
        }, this);

        this.canCreate = ko.pureComputed(function() {
            return WebConfig.userSession.hasPermission(Constants.Permission.CreateTrainer);
            //return true;
        }, this);

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Assets_Trainer_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Assets_Trainer_Update")());
                break;
        }

        this.id = this.ensureNonObservable(params.id, -1);

        this.genders = ko.observableArray([]);
        this.businessUnits = ko.observableArray([]);

        this.picture = ko.observable(null);
        this.pictureFileName = ko.observable('');
        this.pictureSelectedFile = ko.observable(null);
        this.pictureUrl = ko.pureComputed(() => {
            if (this.picture() && this.picture().infoStatus !== Enums.InfoStatus.Delete) {
                return this.picture().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });
        this.pictureRemoveIconVisible = ko.pureComputed(() => {
            return this.picture() && this.picture().infoStatus !== Enums.InfoStatus.Delete;
        });
        this.pictureMimeTypes = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();

        this.employeeId = ko.observable('');
        this.cardId = ko.observable('');
        
        this.selectedGender = ko.observable();
        this.title = ko.observable('');
        this.firstName = ko.observable('');
        this.lastName = ko.observable('');
        this.phone = ko.observable('');
        this.nationalId = ko.observable();
        this.desc = ko.observable();
    }

    onBeforeUpload(isUploadFileValid) {
        if(!isUploadFileValid){
            //Show Placeholder
            if(this.picture() != null)
            {
                if (this.picture().infoStatus === Enums.InfoStatus.Add) {
                    this.picture(null);
                } else {
                    var picture = this.picture();
                    picture.infoStatus = Enums.InfoStatus.Delete;
                    this.picture(picture);
                }
            }
        }
    }
    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccess(data) {
        if(data && data.length){
            var picture = data[0];
            var currentPicture = this.picture();
            if (currentPicture) {
                switch (currentPicture.infoStatus) {
                    case Enums.InfoStatus.Add:
                        picture.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        picture.infoStatus = Enums.InfoStatus.Update;
                        picture.id = currentPicture.id;
                        break;
                }
            } else {
                picture.infoStatus = Enums.InfoStatus.Add;
            }

            this.picture(picture);
        }
    }
    
    onUploadFail(e) {
        this.pictureFileName('');
        this.handleError(e);
    }

    onRemoveFile(e) {
        if(this.picture() != null)
        {
            if (this.picture().infoStatus === Enums.InfoStatus.Add) {
                this.picture(null);
            } else {
                this.picture().infoStatus = Enums.InfoStatus.Delete;

                var picture = this.picture();
                picture.infoStatus = Enums.InfoStatus.Delete;
                this.picture(picture);
            }
        }
        this.pictureSelectedFile(null);
        this.pictureFileName('');
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        let dfd = $.Deferred();
        let enumResourceFilter = new EnumResourceFilter();
        let businessUnitFilter = new BusinessUnitFilter();

        let a1 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestTrainer.getTrainer(this.id, [EntityAssociation.Trainer.Image]) : null;
        let a2 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);
        let a3 = this.webRequestBusinessUnit.listBusinessUnitSummary(businessUnitFilter);
        
        $.when(a1, a2, a3).done((r1, r2, r3) => {

            let trainerData = r1;

            let enumResources = r2["items"];
            let businessUnits = r3["items"];

            let genders = _.filter(enumResources, (item) => {
                return item.type === Enums.ModelData.EnumResourceType.Gender;
            });
            Utility.applyFormattedPropertyToCollection(genders, "displayName", "{0} ({1})", "displayName", "value")
            
            this.genders.replaceAll(genders);


            if (trainerData) {
                if (trainerData.image) {
                    this.picture(trainerData.image);
                    this.pictureFileName(trainerData.image.fileName);
                }

                this.employeeId(trainerData.employeeId);
                this.selectedGender(ScreenHelper.findOptionByProperty(this.genders, "value", trainerData.gender));
                this.title(trainerData.title);
                this.firstName(trainerData.firstName);
                this.lastName(trainerData.lastName);
                this.phone(trainerData.phone);
                this.nationalId(trainerData.citizenId);
                this.desc(trainerData.description);
            }
            dfd.resolve();  
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    onUnload() {}

    onChildScreenClosed() {}

    buildActionBar(actions) {
        if((this.mode === Screen.SCREEN_MODE_CREATE && this.canCreate())
            || (this.mode === Screen.SCREEN_MODE_UPDATE && this.canEdit())) 
        {
             actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        }
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    
    }

    buildCommandBar(commands) {}

    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id == "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();

                return;
            }

            let model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE: 
                    this.webRequestTrainer.createTrainer(model).done(() => { 
                        this.publishMessage("ca-asset-trainer-changed");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE: 
                    this.webRequestTrainer.updateTrainer(model).done(() => { 
                        this.publishMessage("ca-asset-trainer-changed");
                        this.isBusy(false);
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);
                        this.handleError(e);
                    });
                    break;
            }
        }
    }

    onCommandClick(sender) {}

    setupExtend() {
        var self = this;

        self.pictureSelectedFile.extend({
            fileExtension: ['jpg', 'jpeg', 'png'],
            fileSize: 4
        });

        self.picture.extend({
            trackChange: true
        });
        self.employeeId.extend({
            trackChange: true
        });
        
        self.selectedGender.extend({
            trackChange: true
        });
        self.title.extend({
            trackChange: true
        });
        self.firstName.extend({
            trackChange: true
        });
        self.lastName.extend({
            trackChange: true
        });
        self.phone.extend({
            trackChange: true
        });

        self.employeeId.extend({
            required: true,
            serverValidate: {
                params: "EmployeeId",
                message: this.i18n("M010")()
            }
        });
        
        self.nationalId.extend({
            serverValidate: {
                params: "CitizenId",
                message: this.i18n("M010")()
            }
        });

        self.selectedGender.extend({
            required: true
        });
        self.firstName.extend({
            required: true
        });
        self.lastName.extend({
            required: true
        });

        self.validationModel = ko.validatedObservable({
            pictureSelectedFile: self.pictureSelectedFile,
            employeeId: self.employeeId,
            selectedGender: self.selectedGender,
            firstName: self.firstName,
            lastName: self.lastName
        });
    }

    generateModel() {

        var test  = [];
        var model = {
            id: this.mode === Screen.SCREEN_MODE_CREATE ? null : this.id,
            companyId: this.companyId,
            image: this.picture(),
            employeeId: this.employeeId(),
            gender: this.selectedGender() ? this.selectedGender().value : null,
            title: this.title(),
            firstName: this.firstName(),
            lastName: this.lastName(),
            phone: this.phone(),
            citizenId : this.nationalId(),
            description : this.desc()
        };

        return model;
    }

    get webRequestTrainer() {
        return WebRequestAssetTrainer.getInstance();
    }

    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
}

export default {
    viewModel: ScreenBase.createFactory(AssetTraineManageScreen),
    template: templateMarkup
};