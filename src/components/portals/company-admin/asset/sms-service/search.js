﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import ScreenHelper from "../../../screenhelper";
import { SMSServiceFilter } from "./infos";

class SMSServiceSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Common_Search")());

        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));
        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.enable = ko.observable();
        this.dateFormat = WebConfig.companySettings.shortDateFormat;
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        if (this.searchFilter.startDate) {
            this.startDate(this.searchFilter.startDate);
        }
        if (this.searchFilter.endDate) {
            this.endDate(this.searchFilter.endDate);
        }
        if (this.searchFilter.enable) {
            this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", this.searchFilter.enable));
        }
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSearch") {
            this.searchFilter = new SMSServiceFilter (
                this.startDate() ? this.startDate() : null,
                this.endDate() ? this.endDate() : null,
                this.enable() ? this.enable().value : null
            );

            this.publishMessage("ca-asset-sms-service-search-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdClear":
                this.startDate(null);
                this.endDate(null);
                this.enable(null);
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(SMSServiceSearchScreen),
    template: templateMarkup
};