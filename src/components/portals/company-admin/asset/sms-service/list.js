﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestSMSService from "../../../../../app/frameworks/data/apitrackingcore/webRequestSMSService";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import { SMSServiceFilter } from "./infos";

class SMSServiceListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Assets_SMSServices")());
        this.bladeSize = BladeSize.Medium;

        this.searchFilter = ko.observable(new SMSServiceFilter());
        this.searchFilterDefault = new SMSServiceFilter(); 
        this.pageFiltering = ko.observable(false);

        this.smsServices = ko.observableArray([]);
        this.selectedSMSService = ko.observable();
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([[ 1, "desc" ]]);

        // Determine if selecting action is legal, return false to cancel selecting event.
        this._selectingRowHandler = (row) => {
            if(row) {
                return this.navigate("ca-asset-sms-service-view", { id: row.id });
            }
            return false;
        };

        // Subscribe Message when sms service search filter apply
        this.subscribeMessage("ca-asset-sms-service-search-filter-changed", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter(searchFilter);
            this.refreshDatasource();
        });

        // subscribe on searchFilter change to apply filter state
        this._changeSearchFilterSubscribe = this.searchFilter.subscribe((searchFilter) => {
            // apply active command state if searchFilter does not match default search
            this.pageFiltering(!_.isEqual(this.searchFilter(), this.searchFilterDefault));
        });

        this.subscribeMessage("ca-asset-sms-service-deleted", () => {
            this.refreshDatasource();
            this.selectedSMSService(null);
        });

        this.subscribeMessage("ca-asset-sms-service-created", (id) => {
            this.refreshDatasource().done(() => {
                this.recentChangedRowIds.replaceAll([id]);
            });
        });

        this.subscribeMessage("ca-asset-sms-service-updated", (id) => {
            this.refreshDatasource().done(() => {
                this.recentChangedRowIds.replaceAll([id]);
            });
        });
    }

    /**
     * Get WebRequest specific for SMS Service module in Web API access.
     * @readonly
     */
    get webRequestSMSService() {
        return WebRequestSMSService.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestSMSService.listSMSService(this.searchFilter()).done((response) => {
            this.smsServices(response.items);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSMSService(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.CreateSMSService)) {
             commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
        commands.push(this.createCommand("cmdSearch", this.i18n("Common_Search")(), "svg-cmd-search", true, true, this.pageFiltering));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("ca-asset-sms-service-manage", { mode: Screen.SCREEN_MODE_CREATE });
                break;
            case "cmdSearch":
                this.navigate("ca-asset-sms-service-search", { searchFilter: this.searchFilter() });
                break;
        }

        this.selectedSMSService(null);
        this.recentChangedRowIds([]);
    }

    
    /**
     * Refresh datatable datasource
     */
    refreshDatasource() {
        this.isBusy(true);
        return this.webRequestSMSService.listSMSService(this.searchFilter()).done((response) => {
            this.smsServices.replaceAll(response.items);
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }
}

export default {
    viewModel: ScreenBase.createFactory(SMSServiceListScreen),
    template: templateMarkup
};