﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import WebRequestSMSService from "../../../../../app/frameworks/data/apitrackingcore/webRequestSMSService";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as Screen from "../../../../../app/frameworks/constant/screen";

class SMSServiceViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.bladeTitle(this.i18n("Assets_ViewSMSService")());

        this.id = this.ensureNonObservable(params.id);
        this.maxItems = ko.observable();
        this.limitRate = ko.observable();
        this.currentUsage = ko.observable();
        this.formatStartDate = ko.observable();
        this.formatEndDate = ko.observable();
        this.formatEnable = ko.observable();

        this.subscribeMessage("ca-asset-sms-service-updated", () => {
            this.refreshDatasource();
        });
    }

    /**
     * Get WebRequest specific for SMS Service module in Web API access.
     * @readonly
     */
    get webRequestSMSService() {
        return WebRequestSMSService.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        this.webRequestSMSService.getSMSService(this.id).done((response) => {
            this.populateData(response);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateSMSService)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteSMSService)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-asset-sms-service-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.id });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M038")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestSMSService.deleteSMSService(this.id).done(() => {
                                this.publishMessage("ca-asset-sms-service-deleted");
                                this.close(true);
                            }).fail((e)=> {
                                this.handleError(e);
                            }).always(() => {
                                this.isBusy(false);
                            });
                            break;
                    }
                });
                break;
        }
    }

    /**
     * Refresh datatable datasource
     */
    refreshDatasource() {
        this.isBusy(true);
        return this.webRequestSMSService.getSMSService(this.id).done((response) => {
            this.populateData(response);
        }).fail((e) => {
            this.handleError(e);
        }).always(() => {
            this.isBusy(false);
        });
    }

    
    /**
     * Populate Data
     */
    populateData(response) {
        this.maxItems(response.formatMaxItems);
        this.currentUsage(response.formatCurrentUsage);
        this.limitRate(response.formatLimitRate);
        this.formatStartDate(response.formatStartDate);
        this.formatEndDate(response.formatEndDate);
        this.formatEnable(response.formatEnable);
    }
}

export default {
    viewModel: ScreenBase.createFactory(SMSServiceViewScreen),
    template: templateMarkup
};