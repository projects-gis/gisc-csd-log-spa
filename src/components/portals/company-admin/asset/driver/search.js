﻿import ko from "knockout";
import templateMarkup from "text!./search.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";
import {DriverFilter} from "./infos";

class DriverSearchScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Search")());
        
        this.searchFilter = this.ensureNonObservable($.extend(true, {}, params.searchFilter));
        this.includeSubBusinessUnitEnable = ko.pureComputed(() => {return !_.isEmpty(this.selectedBusinessUnit());});
        this.includeSubBusinessUnit = ko.observable(false);
        this.selectedBusinessUnit = ko.observable();
        this.businessUnits = ko.observableArray([]);
        this.enable = ko.observable();
        this.enableOptions = ScreenHelper.createYesNoObservableArray();
    }
    /**
     * Get WebRequest specific for Business Unit module in Web API access.
     * @readonly
     */
    get webRequestBusinessUnit() {
        return WebRequestBusinessUnit.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var filter = {
            companyId: WebConfig.userSession.currentCompanyId,
            includeAssociationNames: [
               EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        };
        
        this.webRequestBusinessUnit.listBusinessUnitSummary(filter).done((r)=> {
            this.businessUnits(r["items"]);
            if(this.searchFilter.businessUnitIds && this.searchFilter.businessUnitIds.length > 0){
                this.selectedBusinessUnit(this.searchFilter.businessUnitIds[0].toString());
            }
            if (this.searchFilter.enable) {
                this.enable(ScreenHelper.findOptionByProperty(this.enableOptions, "value", this.searchFilter.enable));
            }

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });
        return dfd;
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSearch", this.i18n("Common_Search")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdClear", this.i18n("Common_Clear")(), "svg-cmd-clear"));
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

         if (sender.id === "actSearch") {
            this.searchFilter = new DriverFilter(
                null,
                this.enable() ? this.enable().value : null
            );

            // Default selection is single.
            if(this.includeSubBusinessUnitEnable()){
                var selectedBusinessUnitId = parseInt(this.selectedBusinessUnit());

                // If user included sub children then return all business unit ids.
                if(this.includeSubBusinessUnit()){
                    this.searchFilter.businessUnitIds = ScreenHelper.findBusinessUnitsById(this.businessUnits(), selectedBusinessUnitId);
                }
                else {
                    // Single selection for business unit.
                    this.searchFilter.businessUnitIds = [selectedBusinessUnitId];
                }
            }

            this.publishMessage("ca-asset-driver-search-filter-changed", this.searchFilter);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdClear") {
            this.selectedBusinessUnit(null);            
            this.enable(null);
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(DriverSearchScreen),
    template: templateMarkup
};