﻿import ko from "knockout";
import templateMarkup from "text!./import.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestDriver from "../../../../../app/frameworks/data/apitrackingcore/webRequestDriver";
import Utility from "../../../../../app/frameworks/core/utility";
import ScreenHelper from "../../../screenhelper";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";

class DriverImportScreen extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_Import")());
        this.bladeSize = BladeSize.XLarge;

        this.fileName = ko.observable('');
        this.selectedFile = ko.observable(null);
        this.mimeType = [
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx")
        ].join();
        this.media = ko.observable(null);
        this.previewData = ko.observableArray([]);
        this.previewDataVisible = ko.observable(false);
        this.importError = ko.observable('');
        this.urlUpload = WebConfig.appSettings.uploadUrlTrackingCore;
    }
    /**
     * Get WebRequest specific for Driver module in Web API access.
     * @readonly
     */
    get webRequestDriver() {
        return WebRequestDriver.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
    }
    /**
     * 
     * setup Extend trackChange/validation
     * 
     * @memberOf DriverImportScreen
     */
    setupExtend() {
        this.selectedFile.extend({
            fileExtension: ['xlsx', 'xls'],
            fileSize: 4,
            fileRequired: true
        });

        this.media.extend({
            trackChange: true
        });

        this.validationModel = ko.validatedObservable({
            selectedFile: this.selectedFile
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdDownload", this.i18n("Common_DownloadTemplate")(), "svg-cmd-download-template"));
    }
    /**
     * 
     * Generate ImportDriverInfo for webRequest from view model
     * @returns
     * 
     * @memberOf DriverImportScreen
     */
    generateModel() {
        return {
            companyId: WebConfig.userSession.currentCompanyId,
            media: this.media()
        };
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actOK") {
            if (!this.validationModel.isValid() || this.media() == null) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            this.webRequestDriver.importDriver(this.generateModel(), true).done(() => {
                this.isBusy(false);
                this.publishMessage("ca-asset-driver-changed");
                this.close(true);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDownload") {
            this.showMessageBox(null, this.i18n("M117")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.isBusy(true);
                        this.webRequestDriver.downloadDriverTemplate(Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx).done((response) => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        }).fail((e)=> {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }
    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload() {
        this.media(null);
        this.fileName('');
        this.importError('');
        this.previewDataVisible(false); 
    }
    /**
     * 
     * Hook on fileuploadsuccess
     * @param {any} data
     * 
     */
    onUploadSuccess(data) {
        if (data && data.length > 0) {
            this.isBusy(true);
            this.media(data[0]);
            this.webRequestDriver.importDriver(this.generateModel(), false).done((response) => {
                var messageIdToErrors = response.messageIdToErrors;
                var previewData = response.data;

                if(Object.keys(response.messageIdToErrors).length){
                    if (messageIdToErrors.M076) {
                        this.importError(ScreenHelper.formatImportValidationErrorMessage("M076", messageIdToErrors.M076));

                    }
                    else if (messageIdToErrors.M077) {
                        this.importError(ScreenHelper.formatImportValidationErrorMessage("M077", messageIdToErrors.M077));
                    }

                    this.media(null);
                }else{
                    if (previewData && previewData.length > 0) {
                        // set unique tempId for dataTable
                        var index = 1;
                        ko.utils.arrayForEach(previewData, (item) => {
                            item.tempId = index;
                            index++;
                        });
                    }

                    this.previewDataVisible(true);
                    this.previewData.replaceAll(previewData);
                }
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
                this.handleError(e);
                this.media(null);
                this.fileName('');
            });
        }
    }
    /**
     * 
     * Hook on fileuploadfail
     * @param {any} e
     * 
     */
    onUploadFail(e) {
        this.fileName('');
        this.handleError(e);
    }
    /**
     * 
     * Hook on Remove File
     * @param {any} e
     * 
     */
    onRemoveFile(e) {
        this.media(null);
        this.fileName('');
        this.selectedFile(null); 
        this.importError('');
        this.previewDataVisible(false); 
    }
}

export default {
    viewModel: ScreenBase.createFactory(DriverImportScreen),
    template: templateMarkup
};