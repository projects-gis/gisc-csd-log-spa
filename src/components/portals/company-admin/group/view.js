﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../screenbase";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import WebRequestPermission from "../../../../app/frameworks/data/apicore/webRequestPermission";
import { PermissionMap } from "./infos";
import {EntityAssociation, Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../app/frameworks/constant/bladeDialog";
import DefaultSoring from "../../../../app/frameworks/constant/defaultSorting";

/**
 * Company Admin: View Group
 * 
 * @class GroupViewScreen
 * @extends {ScreenBase}
 */
class GroupViewScreen extends ScreenBase {

    /**
     * Creates an instance of GroupViewScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Group_ViewUserGroup")());

        this.groupId = this.ensureNonObservable(params.groupId, -1);
        this.isSystem = ko.observable(false);
        this.name = ko.observable();
        this.description = ko.observable();

        // Note: this is differnt from Create/Update since the permissionMap itself is observable
        // The reason is so it able to refresh view.
        this.permissionMap = ko.observable();

        // Observe change about group/permission
        this.subscribeMessage("ca-group-changed", (message) => {
            if(message.mode === "delete") return;

            this.isBusy(true);

            var d1 = this.webRequestGroup.getGroup(message.groupId, [EntityAssociation.Group.GroupPermissions]);
            var d2 = this.webRequestPermission.listPermission({
                portal: Enums.ModelData.Portal.Company,
                includeAssociationNames: [EntityAssociation.Permission.ChildPermissions],
                sortingColumns: DefaultSoring.Permission
            });

            $.when(d1, d2).done((userGroup, r2) => {
                var permissions = r2["items"];
                if(userGroup && permissions) {
                    this.isSystem(userGroup.isSystem);
                    this.name(userGroup.name);
                    this.description(userGroup.description);
                    this.permissionMap(PermissionMap.createInstance(userGroup.groupPermissions, permissions, true));
                }
            }).fail((e)=> {
                this.handleError(e);
            }).always(()=>{
                this.isBusy(false);
            });
        });

        // Subscription Group is a group created in subscription, it is system-wide and considering
        // semi-system group. User should unable to update/delete this group.
        this.isSubscriptionGroup = ko.observable(false);
    }

    /**
     * Get WebRequest specific for Pemission module in Web API access.
     * @readonly
     */
    get webRequestPermission() {
        return WebRequestPermission.getInstance();
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();

        var d1 = this.webRequestGroup.getGroup(this.groupId, [EntityAssociation.Group.GroupPermissions]);
        var d2 = this.webRequestPermission.listPermission({
            portal: Enums.ModelData.Portal.Company,
            includeAssociationNames: [EntityAssociation.Permission.ChildPermissions],
            sortingColumns: DefaultSoring.Permission
        });

        $.when(d1, d2).done((userGroup, r2) => {
            var permissions = r2["items"];
            if(userGroup && permissions) {
                this.isSystem(userGroup.isSystem);
                this.name(userGroup.name);
                this.description(userGroup.description);
                this.permissionMap(PermissionMap.createInstance(userGroup.groupPermissions, permissions, true));
            }

            if(userGroup) {
                this.isSubscriptionGroup(_.isNil(userGroup.companyId));
            }

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateGroup_Company) 
            && !this.isSystem()
            && !this.isSubscriptionGroup()) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }

        if(WebConfig.userSession.hasPermission(Constants.Permission.DeleteGroup_Company) 
            && !this.isSystem()
            && !this.isSubscriptionGroup()) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-group-manage", { mode: "update", groupId: this.groupId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M101")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestGroup.deleteGroup(this.groupId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("ca-group-changed", {
                                    groupId: this.groupId,
                                    mode: "delete"
                                });
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);

                                this.handleError(e);
                            });
                            break;
                    }
                });
                break;
        }
    }
}

export default {
    viewModel: ScreenBase.createFactory(GroupViewScreen),
    template: templateMarkup
};