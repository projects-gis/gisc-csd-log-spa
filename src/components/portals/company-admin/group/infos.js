import ko from "knockout";
import Utility from "../../../../app/frameworks/core/utility";
import ObjectBase from "../../../../app/frameworks/core/objectBase";
import {Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Permission Map for User Group
 * 
 * @class PermissionMap
 */
class PermissionMap {
    
    /**
     * Creates an instance of PermissionMap.
     * 
     * @param {any} params
     */
    constructor(params){
        this._map = new Map();
        this._map.set(Enums.ModelData.PermissionCategory.Visibility, []);
        this._map.set(Enums.ModelData.PermissionCategory.Functionality, []);
        this._map.set(Enums.ModelData.PermissionCategory.Alert, []);

        var groupPermissions = Utility.extractParam(params.groupPermissions, []);
        var allPermissions = Utility.extractParam(params.allPermissions, []);

        if(params.isReadOnly) {
            this._initalizeReadOnly(groupPermissions, allPermissions);
        } else {
            this._initialize(groupPermissions, allPermissions);
        }

        this.hasVisibilityPermissions = ko.pureComputed(function() {
            return this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Visibility);
        }, this);

        this.hasFunctionalityPermissions = ko.pureComputed(function() {
            return this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Functionality);
        }, this);

        this.hasAlertPermissions = ko.pureComputed(function() {
            return this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Alert);
        }, this);

        this.hasAnyPermission = ko.pureComputed(function() {
            return this.hasVisibilityPermissions() || this.hasFunctionalityPermissions() || this.hasAlertPermissions();
        }, this);

        this.visibilityPermissions = ko.pureComputed(function() {
            return this._get(Enums.ModelData.PermissionCategory.Visibility);
        }, this);

        this.functionalityPermissions = ko.pureComputed(function() {
            return this._get(Enums.ModelData.PermissionCategory.Functionality);
        }, this);

        this.alertPermissions = ko.pureComputed(function() {
            return this._get(Enums.ModelData.PermissionCategory.Alert);
        }, this);

        // Category name {Visibility, Functionality, Alert} is data, not constant
        this.getVisibilityCategoryName = ko.pureComputed(function() {
            if(this.hasVisibilityPermissions()) {
                let firstElem = this.visibilityPermissions()[0];
                return firstElem.permissionCategoryDisplayName;
            }
            return null;
        }, this);

        this.getFunctionalityCategoryName = ko.pureComputed(function() {
            if(this.hasFunctionalityPermissions()) {
                let firstElem = this.functionalityPermissions()[0];
                return firstElem.permissionCategoryDisplayName;
            }
            return null;
        }, this);

        this.getAlertCategoryName = ko.pureComputed(function() {
            if(this.hasAlertPermissions()) {
                let firstElem = this.alertPermissions()[0];
                return firstElem.permissionCategoryDisplayName;
            }
            return null;
        }, this);
    }

    /**
     * Find GroupPermission that match given permission, return null if there is no match.
     * 
     * @private
     * @param {any} groupPermissions
     * @param {any} permission
     * @returns
     */
    _matchGroupPermissions(groupPermissions, permission) {
        for(let i = 0; i < groupPermissions.length; i++) {
            var gPermission = groupPermissions[i];
            if(gPermission.permissionId === permission.id) {
                return gPermission;
            }
        }
        return null;
    }

    /**
     * 
     * @private
     * @param {any} groupPermissions
     * @param {any} allPermissions
     */
    _initalizeReadOnly(groupPermissions, allPermissions) {
        allPermissions.forEach(function(perm) {
            let gPermission = this._matchGroupPermissions(groupPermissions, perm);
            if(gPermission) {
                var entry = new PermissionMapItem({
                    id: gPermission.id,
                    groupId: gPermission.groupId,
                    permissionId: perm.id,
                    code: perm.code,
                    name: perm.name,
                    categoryId: perm.category,
                    categoryDisplayName: perm.categoryDisplayName,
                    isChild: perm.parentId !== null,
                    subscriptionId: gPermission.subscriptionId
                });
                this._map.get(perm.category).push(entry);
            }
            if(perm.childPermissions.length) {
                perm.childPermissions.forEach(function(child) {
                    let gPermission = this._matchGroupPermissions(groupPermissions, child);
                    if(gPermission) {
                        var childEntry = new PermissionMapItem({
                            id: gPermission.id,
                            groupId: gPermission.groupId,
                            permissionId: child.id,
                            code: child.code,
                            name: child.name,
                            categoryId: child.category,
                            categoryDisplayName: child.categoryDisplayName,
                            isChild: child.parentId !== null,
                            subscriptionId: gPermission.subscriptionId,
                            parent: entry
                        });

                        if(child.childPermissions.length){
                            var childPermission2 = new Array();
                            child.childPermissions.forEach(function(child2) {
                                let gPermission = this._matchGroupPermissions(groupPermissions, child2);
                                var childEntry2 = new PermissionMapItem({
                                    id: (gPermission !== null) ? gPermission.id : null,
                                    groupId: (gPermission !== null) ? gPermission.groupId : null,
                                    permissionId: child2.id,
                                    code: child2.code,
                                    name: child2.name,
                                    categoryId: child2.category,
                                    categoryDisplayName: child2.categoryDisplayName,
                                    isChild: child2.parentId !== null,
                                    selected: gPermission !== null,
                                    subscriptionId: (gPermission !== null) ? gPermission.subscriptionId : null,
                                    parent: childEntry
                                });
                                childPermission2.push(childEntry2);
                            }, this);
                            childEntry.child2 = childPermission2;
                        }

                        this._map.get(child.category).push(childEntry);
                    }
                }, this);
            }
        }, this); 
    }

    /**
     * 
     * @private
     * @param {any} groupPermissions
     * @param {any} allPermissions
     */
    _initialize(groupPermissions, allPermissions) {
        allPermissions.forEach(function(perm) {
            let gPermission = this._matchGroupPermissions(groupPermissions, perm);
            var entry = new PermissionMapItem({
                id: (gPermission !== null) ? gPermission.id : null,
                groupId: (gPermission !== null) ? gPermission.groupId : null,
                permissionId: perm.id,
                code: perm.code,
                name: perm.name,
                categoryId: perm.category,
                categoryDisplayName: perm.categoryDisplayName,
                isChild: perm.parentId !== null,
                selected: gPermission !== null,
                subscriptionId: (gPermission !== null) ? gPermission.subscriptionId : null
            });
            this._map.get(perm.category).push(entry);
            
            if(perm.childPermissions.length) {
                perm.childPermissions.forEach(function(child) {
                    let gPermission = this._matchGroupPermissions(groupPermissions, child);
                    var childEntry = new PermissionMapItem({
                        id: (gPermission !== null) ? gPermission.id : null,
                        groupId: (gPermission !== null) ? gPermission.groupId : null,
                        permissionId: child.id,
                        code: child.code,
                        name: child.name,
                        categoryId: child.category,
                        categoryDisplayName: child.categoryDisplayName,
                        isChild: child.parentId !== null,
                        selected: gPermission !== null,
                        subscriptionId: (gPermission !== null) ? gPermission.subscriptionId : null,
                        parent: entry
                    });
                    
                    if(child.childPermissions.length){
                        var childPermission2 = new Array();
                        child.childPermissions.forEach(function(child2) {
                            let gPermission = this._matchGroupPermissions(groupPermissions, child2);
                            var childEntry2 = new PermissionMapItem({
                                id: (gPermission !== null) ? gPermission.id : null,
                                groupId: (gPermission !== null) ? gPermission.groupId : null,
                                permissionId: child2.id,
                                code: child2.code,
                                name: child2.name,
                                categoryId: child2.category,
                                categoryDisplayName: child2.categoryDisplayName,
                                isChild: child2.parentId !== null,
                                selected: gPermission !== null,
                                subscriptionId: (gPermission !== null) ? gPermission.subscriptionId : null,
                                parent: childEntry
                            });
                            childPermission2.push(childEntry2);
                        }, this);
                        childEntry.child2 = childPermission2;
                    }

                    this._map.get(child.category).push(childEntry);

                }, this);
            }
        }, this); 
    }
    
    /**
     * 
     * @private
     * @param {any} category
     * @returns {boolean}
     */
    _hasCategoryPermissions(category) {
        return this._map.get(category).length > 0;
    }

    /**
     * 
     * @private
     * @param {any} category
     * @returns
     */
    _get(category) {
        return this._map.get(category);
    }
    
    /**
     * 
     * @public
     * @returns {array} collection of PermissionMapItem
     */
    getAll() {
        var all = [];
        if(this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Visibility)) {
            all = [].concat(this._get(Enums.ModelData.PermissionCategory.Visibility));
        }
        if(this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Functionality)) {
            all = all.concat(this._get(Enums.ModelData.PermissionCategory.Functionality));
        }
        if(this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Alert)) {
            all = all.concat(this._get(Enums.ModelData.PermissionCategory.Alert));
        }
        return all;
    }
    
    /**
     * 
     * @public
     * @returns
     */
    getAllAsJSON() {
        return this.getAll().map((obj) => {
            return obj.toJSON();
        });
    }

    /**
     * Use in Prototype phase, get only selected PermissionMapItem so it can be saved
     * with MockUtility
     * @public
     * @returns
     */
    getSelected() {
        var selected = [];
        if(this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Visibility)) {
            this._get(Enums.ModelData.PermissionCategory.Visibility).forEach(function(element) {

                if(element.selected()) {
                    selected.push(element);
                }

                if(element.child2 && element.child2.length){
                    element.child2.forEach(function(element2) {
                        if(element2.selected()) {
                            selected.push(element2);
                        }
                    }, this);
                }
                
            }, this);
        }
        if(this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Functionality)) {
            this._get(Enums.ModelData.PermissionCategory.Functionality).forEach(function(element) {
                
                if(element.selected()) {
                    selected.push(element);
                }
         
                if(element.child2 && element.child2.length){
                    element.child2.forEach(function(element2) {
                        if(element2.selected()) {
                            selected.push(element2);
                        }
                    }, this);
                }

            }, this);
        }
        if(this._hasCategoryPermissions(Enums.ModelData.PermissionCategory.Alert)) {
            this._get(Enums.ModelData.PermissionCategory.Alert).forEach(function(element) {

                if(element.selected()) {
                    selected.push(element);
                }

                if(element.child2 && element.child2.length){
                    element.child2.forEach(function(element2) {
                        if(element2.selected()) {
                            selected.push(element2);
                        }
                    }, this);
                }

            }, this);
        }
        return selected;
    }

    setTrackChange() {
        this.getAll().forEach(function(element) {
            element.setTrackChange();
        }, this);
    }

    isDirty() {
        var isItDirty = false;
        var all = this.getAll();
        for(let i = 0; i < all.length; i++) {
            var element = all[i];
            isItDirty = element.isDirty();
            if(isItDirty) break;
        }
        return isItDirty;
    }
    
    /**
     * Dispose the object
     */
    dispose() {
        if(this._map) {
            this.getAll().forEach(function(element) {
                element.dispose();
                element = null;
            }, this);
            this._map = null;
        }   
    }
    
    /**
     * Create instance of PermissionMap given group permissions 
     * and all permissions in the system.
     * @public
     * @static
     * @param {any} groupPermissions
     * @param {any} allPermissions
     * @param {boolean} isTemplate if true then create map with all unchecked entry
     * false will return all checked entry.
     * Set to false when you need to update the map, True when you create.
     * @returns
     */
    static createInstance(groupPermissions, allPermissions, isReadOnly = false) {
        return new PermissionMap({ 
            groupPermissions: groupPermissions,
            allPermissions: allPermissions,
            isReadOnly: isReadOnly
        });
    }
}


/**
 * Entry of PermissionMap
 * 
 * @class PermissionMapItem
 * @extends {ObjectBase}
 */
class PermissionMapItem extends ObjectBase {
    
    /**
     * Creates an instance of PermissionMapItem.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        
        this.id = Utility.extractParam(params.id, null);
        this.groupId = Utility.extractParam(params.groupId, null);
        this.permissionId = Utility.extractParam(params.permissionId, null);
        this.permissionCode = Utility.extractParam(params.code, null);
        this.permissionName = Utility.extractParam(params.name);
        this.permissionCategoryId = Utility.extractParam(params.categoryId, null);
        this.permissionCategoryDisplayName = Utility.extractParam(params.categoryDisplayName, null);
        this.subscriptionId = Utility.extractParam(params.subscriptionId, null);

        this.parent = Utility.extractParam(params.parent, null);
        this.isChild = Utility.extractParam(params.isChild, false);
        this.selected = ko.observable(Utility.extractParam(params.selected, false));


        this.tdCssClass = ko.pureComputed(function() {
            return this.isChild ? "child" : "";
        }, this);

        this._selectedSubscribeRef = this.selected.subscribe((isSelc) => {
            if(this.parent) {
                // Select child -> auto select parent -- NOT chaining to select other child.
                if(isSelc && !this.parent.selected()) {
                    this.parent.selected.poke(true);
                }
            }
        });
        if(this.parent) {
            this.parent.selected.ignorePokeSubscribe((isSelc) => {
                // De-select parent -> auto de-select all childrens
                if(!isSelc && this.selected()) {
                    this.selected(false);
                }
                // Select parent -> auto select all childrens
                if(isSelc && !this.selected()) {
                    this.selected(true);
                }
            });
        }
        
    }

    toGroupPermissionInfo() {
        return {
            permissionId: this.permissionId,
            permissionCode: this.permissionCode,
            groupId: this.groupId,
            subscriptionId: this.subscriptionId,
            id: this.id
        };
    }

    dispose() {
        if(this._selectedSubscribeRef) {
            this._selectedSubscribeRef.dispose();
        }
    }

    setTrackChange() {
        this.selected.extend({
            trackChange: true
        });
    }
}

export { PermissionMap, PermissionMapItem };