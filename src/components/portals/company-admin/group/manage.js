﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../screenbase";
import Utility from "../../../../app/frameworks/core/utility";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import WebRequestPermission from "../../../../app/frameworks/data/apicore/webRequestPermission";
import {PermissionMap} from "./infos";
import {EntityAssociation, Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../app/frameworks/constant/screen";
import DefaultSoring from "../../../../app/frameworks/constant/defaultSorting";

/**
 * Company Admin: Create/Update Group
 * 
 * @class GroupManageScreen
 * @extends {ScreenBase}
 */
class GroupManageScreen extends ScreenBase {

    /**
     * Creates an instance of GroupManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("Common_CreateUserGroup")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("Common_UpdateUserGroup")());
        }

        this.groupId = this.ensureNonObservable(params.groupId, -1);

        this.name = ko.observable("");
        this.description = ko.observable("");
        this.permissionMap = null; // will be calculated in onLoad

        // These properties will be used to calculate delta changed.
        this._originalGroupPermissions = [];
    }

    /**
     * Get WebRequest specific for Pemission module in Web API access.
     * @readonly
     */
    get webRequestPermission() {
        return WebRequestPermission.getInstance();
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();

        var d1 = (this.mode === "update") ? this.webRequestGroup.getGroup(this.groupId, [EntityAssociation.Group.GroupPermissions]) : null;
        var d2 = this.webRequestPermission.listPermission({
            portal: Enums.ModelData.Portal.Company,
            includeAssociationNames: [EntityAssociation.Permission.ChildPermissions],
            sortingColumns: DefaultSoring.Permission
        });

        $.when(d1, d2).done((userGroup, r2) => {
            var groupPermissions = [];
            var permissions = r2["items"];

            if(userGroup) {
                this.name(userGroup.name);
                this.description(userGroup.description);
                
                // For compute delta
                this._originalGroupPermissions = $.extend(true, [], userGroup.groupPermissions);
                groupPermissions = userGroup.groupPermissions;
            }
            this.permissionMap = PermissionMap.createInstance(groupPermissions, permissions);
            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Setup track change.
        this.name.extend({
            trackChange: true
        });
        this.description.extend({
            trackChange: true
        });
        this.permissionMap.setTrackChange();

        // Setup validation rules
        this.name.extend({
            required: true,
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            permissionMap: this.permissionMap
        });
    }

    generateModel() {
        var model = {
            companyId: WebConfig.userSession.currentCompanyId,
            id: this.groupId,
            name: this.name(),
            description: this.description(),
            isSystem: false
        };

        var selectedPermissionItems = this.permissionMap.getSelected();
        // Generate array with InfoStatus on each item.
        var generatedTempId = -1;
        model.groupPermissions = Utility.generateArrayWithInfoStatus(
            this._originalGroupPermissions, 
            selectedPermissionItems.map((obj) => {
                let gPermissionInfo = obj.toGroupPermissionInfo();
                gPermissionInfo.groupId = this.groupId;
                if(_.isNil(gPermissionInfo.id)) {
                    gPermissionInfo.id = generatedTempId--;
                }
                return gPermissionInfo;
            })
        );
        return model;
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        switch(this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.CreateGroup_Company)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
            case Screen.SCREEN_MODE_UPDATE:
                if(WebConfig.userSession.hasPermission(Constants.Permission.UpdateGroup_Company)) {
                    actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
                }
                break;
        }
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();

            switch(this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestGroup.createGroup(model).done((response) => {
                        this.isBusy(false);

                        this.publishMessage("ca-group-changed", {
                            groupId: response.id,
                            mode: this.mode
                        });
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestGroup.updateGroup(model).done(() => {
                        this.isBusy(false);

                        this.publishMessage("ca-group-changed", {
                            groupId: this.groupId,
                            mode: this.mode
                        });
                        this.close(true);
                    }).fail((e)=> {
                        this.isBusy(false);

                        this.handleError(e);
                    });
                    break;
            }
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(GroupManageScreen),
    template: templateMarkup
};