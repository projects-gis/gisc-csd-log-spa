﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../screenbase";
import ScreenHelper from "../../screenhelper";
import Utility from "../../../../app/frameworks/core/utility";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebRequestGroup from "../../../../app/frameworks/data/apicore/webRequestGroup";
import WebRequestBusinessUnit from "../../../../app/frameworks/data/apicore/webRequestBusinessUnit";
import WebRequestEnumResource from "../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {EntityAssociation, Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";
import * as Screen from "../../../../app/frameworks/constant/screen";
import * as EventAggregatorConstant from "../../../../app/frameworks/constant/eventAggregator";

/**
 * Company Admin: Create/Update User
 * 
 * @class UserManageScreen
 * @extends {ScreenBase}
 */
class UserManageScreen extends ScreenBase {

    /**
     * Creates an instance of UserManageScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.mode = this.ensureNonObservable(params.mode);

        if (this.mode === Screen.SCREEN_MODE_CREATE) {
            this.bladeTitle(this.i18n("User_CreateUser")());
        } else if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            this.bladeTitle(this.i18n("User_UpdateUser")());
        }

        this.pictureFileName = ko.observable('');
        this.pictureSelectedFile = ko.observable(null);
        this.picture = ko.observable(null);

        this.enableDS = ScreenHelper.createYesNoObservableArray();

        this.userId = this.ensureNonObservable(params.userId, -1);
        this.dateFormat = WebConfig.companySettings.shortDateFormat;

        this.firstName = ko.observable("");
        this.lastName = ko.observable("");
        this.username = ko.observable("");
        this.email = ko.observable("");
        this.phone = ko.observable("");
        this.expDate = ko.observable("");
        
        this.ipPermit = ko.observable("");
        this.userGroups = ko.observableArray([]);
        this.orderUserGroups = ko.observable([[ 1, "asc" ]]);
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.orderAccessibleBusinessUnits = ko.observable([[ 1, "asc" ]]);
        this.businessUnit = ko.observable();
        this.enableBusinessUnit = ko.observable(true);
        this.businessUnitDS = ko.observableArray([]);
        this.disableBusinessUnits = ko.observableArray([]);
        
        this.userEnabled = ko.observable();
        this.authType = ko.observable();

        this.pmsTypeOption = ko.observableArray();
        this.pmsType = ko.observable();

        this.enablePmsType = this.mode === Screen.SCREEN_MODE_CREATE && WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Public;
        
        this.pictureMimeTypes = [
            Utility.getMIMEType("jpg"),
            Utility.getMIMEType("png")
        ].join();

        this.pictureUrl = ko.pureComputed(() => {
            if (this.picture() && this.picture().infoStatus !== Enums.InfoStatus.Delete) {
                return this.picture().fileUrl;
            } else {
                return this.resolveUrl("~/images/upload-img-placeholder-100x100.jpg");
            }
        });

        this.pictureRemoveIconVisible = ko.pureComputed(() => {
            return this.picture() && this.picture().infoStatus !== Enums.InfoStatus.Delete;
        });

        // When open this blade from View Driver
        this.driverId = null;
        var driver = params.driver;
        if (this.mode === Screen.SCREEN_MODE_CREATE && driver) {
            this.driverId = driver.id;
            this.firstName(driver.firstName);
            this.lastName(driver.lastName);
            this.phone(driver.phone);
            this.businessUnit(driver.businessUnitId.toString());
            this.enableBusinessUnit(false);
        }   

        // Observe change about User Groups
        this.subscribeMessage("ca-user-manage-group-selected", (selectedUserGroups) => {
            var mappedSelectedGroups = selectedUserGroups.map((group) => {
                let mgroup = group;
                mgroup.groupId = group.id;
                mgroup.groupName = group.name;
                mgroup.canManage = true;
                return mgroup;
            });

            // #30250 - need to keep unable to manage group + mappedSelectedGroups
            var unableToManageGroups = _.filter(this._originalGroups, (o) => {
                return !o.canManage;
            });
            if(unableToManageGroups) {
                mappedSelectedGroups = mappedSelectedGroups.concat(unableToManageGroups);
            }
            
            this.userGroups.replaceAll(mappedSelectedGroups);
        });

        // Observe change about Accessible Business Units
        this.subscribeMessage("ca-user-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.name;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });

            // #30250 - need to keep unable to manage bu + mappedSelectedBUs
            var unableToManageBUs = _.filter(this._originalAccessibleBusinessUnits, (o) => {
                return !o.canManage;
            });
            if(unableToManageBUs) {
                // exclude unable to manage BU before merge
                var unableToManageBUIds = unableToManageBUs.map((bu) => { return bu.businessUnitId });
                mappedSelectedBUs = _.filter(mappedSelectedBUs, (bu) => {
                    return _.indexOf(unableToManageBUIds, bu.businessUnitId) === -1;
                });

                // merge select BU and unable to manage items
                mappedSelectedBUs = mappedSelectedBUs.concat(unableToManageBUs);
            }

            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        this.username.subscribe((val) => {
            if(this.authType() && this.authType().value == Enums.ModelData.AuthenticationType.PEAIdm){
                this.webRequestUser.getPEAIdmEmployee(val).done((res) => 
                {
                    if(res){
                        this.firstName(res.firstName);
                        this.lastName(res.lastName);
                        this.email(res.email);
                        this.phone(res.phone);
                    }
                }).fail((e) => {
                    this.firstName(null);
                    this.lastName(null);
                    this.email(null);
                    this.phone(null);
                    this.handleError(e);
                });
            }
        });
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * Get WebRequest for Group module in Web API access.
     * @readonly
     */
    get webRequestGroup() {
        return WebRequestGroup.getInstance();
    }

    /**
     * Get WebRequest specific for Business module in Web API access.
     * @readonly
     */
    get webRequestBU() {
        return WebRequestBusinessUnit.getInstance();
    }

    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        
        var dfd = $.Deferred();

        var d1 = this.webRequestEnumResource.listEnumResource({
            types: [
                Enums.ModelData.EnumResourceType.AuthenticationType,
                Enums.ModelData.EnumResourceType.PermissionType
            ]
        });
        var d2 = this.webRequestBU.listBusinessUnitSummary({ 
            companyId: WebConfig.userSession.currentCompanyId, 
            includeAssociationNames: [
               EntityAssociation.BusinessUnit.ChildBusinessUnits
            ]
        });
        var d3 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? 
            this.webRequestUser.getUser(this.userId, [
                EntityAssociation.User.Image,
                EntityAssociation.User.Groups, 
                EntityAssociation.User.AccessibleBusinessUnits
            ]) : null;

        $.when(d1, d2, d3).done((r1, r2, user) => {

            let tmpAuthenItem = _.filter(r1["items"], (data) => { return data.type == Enums.ModelData.EnumResourceType.AuthenticationType });
            let tmpPermissionItem = _.filter(r1["items"], (data) => { return data.type == Enums.ModelData.EnumResourceType.PermissionType });

            if (_.size(tmpPermissionItem) <= 0) {
                tmpPermissionItem = [
                    { displayName: 'Public', value: 1 }, { displayName: 'Private', value: 2 }
                ];
            }

            this.authTypeDS = ScreenHelper.createGenericOptionsObservableArray(tmpAuthenItem, "displayName", "value");
            this.pmsTypeOption = ScreenHelper.createGenericOptionsObservableArray(tmpPermissionItem, "displayName", "value");
            this.businessUnitDS(r2["items"]);
            
            var businessUnit = r2["items"];
            this.disableBusinessUnits(this._getNotAccessibleBU(businessUnit));

            switch(this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.userEnabled(ScreenHelper.findOptionByProperty(this.enableDS, "value", true));
                    this.authType(ScreenHelper.findOptionByProperty(this.authTypeDS, "value", Enums.AuthenticationType.Form));

                    if (WebConfig.userSession.permissionType == Enums.ModelData.PermissionType.Private) {
                        this.pmsType(ScreenHelper.findOptionByProperty(this.pmsTypeOption, "value",2));
                    }

                    this._originalGroups = [];
                    this._originalAccessibleBusinessUnits = [];

                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    if(user) {
                        if (user.image) {
                            this.picture(user.image);
                            this.pictureFileName(user.image.fileName);
                        }

                        this.username(user.username);
                        this.firstName(user.firstName);
                        this.lastName(user.lastName);
                        this.email(user.email);
                        this.phone(user.phone);
                        this.expDate(user.expirationDate);
                        this.ipPermit(user.ipPermit);
                        this.businessUnit(user.businessUnitId.toString());

                        this.userEnabled(ScreenHelper.findOptionByProperty(this.enableDS, "value", user.enable));
                        this.authType(ScreenHelper.findOptionByProperty(this.authTypeDS, "value", user.authenticationType));
                        this.pmsType(ScreenHelper.findOptionByProperty(this.pmsTypeOption, "value", user.permissionType));

                        this.userGroups(user.groups);
                        this.accessibleBusinessUnits(user.accessibleBusinessUnits);

                        // For compute delta
                        this._originalGroups = _.cloneDeep(user.groups);
                        this._originalAccessibleBusinessUnits = _.cloneDeep(user.accessibleBusinessUnits);
                    }
                    break;
            }

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }
    

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.pictureSelectedFile.extend({
            fileExtension: ['jpg', 'jpeg', 'png'],
            fileSize: 4
        });

        // Manual setup track change.
        this.username.extend({
            trackChange: true
        });
        this.firstName.extend({
            trackChange: true
        });
        this.lastName.extend({
            trackChange: true
        });
        this.email.extend({
            trackChange: true
        });
        this.phone.extend({
            trackChange: true
        });
        this.userEnabled.extend({
            trackChange: true
        });
        this.businessUnit.extend({
            trackChange: true
        });
        this.authType.extend({
            trackChange: true
        });
        this.expDate.extend({
            trackChange: true
        });
        this.ipPermit.extend({
            trackChange: true
        });
        this.userGroups.extend({
            trackArrayChange: {
                uniqueFromPropertyName: "groupId"
            }
        });
        this.accessibleBusinessUnits.extend({
            trackArrayChange: {
                uniqueFromPropertyName: "businessUnitId"
            }
        });

        // Manual setup validation rules
        this.username.extend({
            required: true,
            serverValidate: {
                params: "Username",
                message: this.i18n("M010")()
            }
        });
        this.email.extend({
            email: true,
            required: true
        });
        this.phone.extend({
            required: true
        });
        this.userEnabled.extend({
            required: true
        });
        this.businessUnit.extend({
            required: true
        });
        this.authType.extend({
            required: true
        });
        this.userGroups.extend({
            arrayRequired: true
        });
        this.accessibleBusinessUnits.extend({
            arrayRequired: true
        });
        this.ipPermit.extend({
            ipAddressMaximum: 5
        });

        this.validationModel = ko.validatedObservable({
            username: this.username,
            email: this.email,
            phone: this.phone,
            userEnabled: this.userEnabled,
            businessUnit: this.businessUnit,
            authType: this.authType,
            userGroups: this.userGroups,
            accessibleBusinessUnits: this.accessibleBusinessUnits,
            ipPermit: this.ipPermit,
            pictureSelectedFile: this.pictureSelectedFile,
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * 
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFail(e) {
       this.pictureFileName('');
       this.handleError(e);
    }
    /**
     * 
     * Hook on Remove File
     * @param {any} e
     * 
     */
    onRemoveFile(e) {
        if(this.picture() != null)
        {
            if (this.picture().infoStatus === Enums.InfoStatus.Add) {
                this.picture(null);
            } else {
                this.picture().infoStatus = Enums.InfoStatus.Delete;

                var picture = this.picture();
                picture.infoStatus = Enums.InfoStatus.Delete;
                this.picture(picture);
            }
        }
        this.pictureSelectedFile(null);
        this.pictureFileName('');
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * Generate user model from View Model
     * 
     * @returns user model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            id: this.userId,
            firstName: this.firstName(),
            lastName: this.lastName(),
            username: this.username(),
            email: this.email(),
            phone: this.phone(),
            enable: this.userEnabled().value,            
            image: this.picture(),
            authenticationType: this.authType().value,
            expirationDate: this.expDate(),
            ipPermit: this.ipPermit(),
            accessibleCompanies: [],
            businessUnitId: this.businessUnit(),
            userType: Enums.UserType.Company,
            companyId: WebConfig.userSession.currentCompanyId,
            driverId: this.driverId,
            permissionType: this.pmsType() ? this.pmsType().value : null,
        };
        model.groups = Utility.generateArrayWithInfoStatus(
            this._originalGroups, 
            this.userGroups().map((obj) => {
                return {
                    userId: this.userId,
                    groupId: obj.id,
                    groupName: obj.name,
                    id: obj.id
                };
            })
        );
        model.accessibleBusinessUnits = Utility.generateArrayWithInfoStatus(
            this._originalAccessibleBusinessUnits,
            this.accessibleBusinessUnits()
        );

        return model;
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);

            var model = this.generateModel();

            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                    this.webRequestUser.createUser(model).done((response) => {
                        if(this.driverId) {
                            this.publishMessage("ca-asset-driver-changed");
                        } else {
                            this.publishMessage("ca-user-changed", {
                                mode: this.mode,
                                userId: response.id
                            });
                        }
                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
                case Screen.SCREEN_MODE_UPDATE:
                    this.webRequestUser.updateUser(model, false).done(() => {
                        this.publishMessage("ca-user-changed", {
                            mode: this.mode,
                            userId: this.userId
                        });

                        // If update current username, notify user menu
                        if(WebConfig.userSession.id === this.userId) {
                            this._eventAggregator.publish(EventAggregatorConstant.CURRENT_USERNAME_CHANGED, this.username());
                            WebConfig.userSession.permissionType = this.pmsType().value;
                        }

                        this.close(true);
                    }).fail((e)=> {
                        this.handleError(e);
                    }).always(()=>{
                        this.isBusy(false);
                    });
                    break;
            }
        }
    }

    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload(isUploadFileValid) {
        if(!isUploadFileValid){
            //Show Placeholder
            if(this.picture() != null)
            {
                if (this.picture().infoStatus === Enums.InfoStatus.Add) {
                    this.picture(null);
                } else {
                    var picture = this.picture();
                    picture.infoStatus = Enums.InfoStatus.Delete;
                    this.picture(picture);
                }
            }
        }
    }

    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccess(data) {
        if(data && data.length){
            var picture = data[0];
            var currentPicture = this.picture();
            if (currentPicture) {
                switch (currentPicture.infoStatus) {
                    case Enums.InfoStatus.Add:
                        picture.infoStatus = Enums.InfoStatus.Add;
                        break;
                    case Enums.InfoStatus.Original:
                    case Enums.InfoStatus.Update:
                    case Enums.InfoStatus.Delete:
                        picture.infoStatus = Enums.InfoStatus.Update;
                        picture.id = currentPicture.id;
                        break;
                }
            } else {
                picture.infoStatus = Enums.InfoStatus.Add;
            }

            this.picture(picture);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    /**
     * Choose User Group
     */
    addUserGroup() {
        var targetMode = (this.mode === "create") ? "ca-user-create" : "ca-user-update";
        var selectedUserGroupIds = this.userGroups().map((obj) => {
            return obj.groupId;
        });
        //find unableToManageGroupIds for case update primary admin, do not allow to select/unselect subscription groups to prevent missing permissions 
        var unableToManageGroupIds = _.filter(this._originalGroups, (o) => {
            return !o.canManage;
        }).map((o) => { return o.groupId });
        this.navigate("ca-user-manage-group-select", { 
            mode: targetMode,
            selectedUserGroupIds: selectedUserGroupIds,
            unableToManageGroupIds: unableToManageGroupIds
        });
    }

    /**
     * Choose Accessible Business Unit
     */
    addAccessibleBusinessUnit() {
        var targetMode = (this.mode === "create") ? "ca-user-create" : "ca-user-update";
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.businessUnitId;
        });
        this.navigate("ca-user-manage-accessible-business-unit-select", { 
            mode: targetMode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }

    
    /**
     * Map BU that unable to access by current user
     * 
     * @param {any} businessUnits
     * @returns
     * 
     * @memberOf UserManageScreen
     */
    _getNotAccessibleBU(businessUnits){
        var result = [];
         _.each(businessUnits, (bu) => {
            //  console.log("BU = "+bu.name +" "+bu.id+ " isAccessible: "+bu.isAccessible);
            //  debugger;
             if(!bu.isAccessible){
                 result.push(bu.id.toString());
             }

             var child = bu.childBusinessUnits
             if (child && child.length > 0){
                 let children = this._getNotAccessibleBU(child);
                 result = _.union(result,children);
             }
         });
        return result;
         
    }
}

export default {
    viewModel: ScreenBase.createFactory(UserManageScreen),
    template: templateMarkup
};