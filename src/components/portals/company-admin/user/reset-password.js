﻿import ko from "knockout";
import templateMarkup from "text!./reset-password.html";
import ScreenBase from "../../screenbase";
import WebRequestUser from "../../../../app/frameworks/data/apicore/webRequestUser";
import WebConfig from "../../../../app/frameworks/configuration/webConfiguration";
import {EntityAssociation, Constants, Enums} from "../../../../app/frameworks/constant/apiConstant";

/**
 * Company Admin: User Reset Password
 * 
 * @class UserResetPasswordScreen
 * @extends {ScreenBase}
 */
class UserResetPasswordScreen extends ScreenBase {

    /**
     * Creates an instance of UserResetPasswordScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_ResetPassword")());

        this.userId = this.ensureNonObservable(params.userId, -1);

        this.newPassword = ko.observable();
        this.confirmNewPassword = ko.observable();
    }

    /**
     * Get WebRequest specific for User module in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {}

    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup validation rules
        this.newPassword.extend({
            required: true
        });
        this.confirmNewPassword.extend({
            required: true,
            areSame: {
                params: this.newPassword,
                message: this.i18n("M006")()
            }
        });

        // Tracking change for editable fields.
        this.newPassword.extend({
            trackChange: true
        });
        this.confirmNewPassword.extend({
            trackChange: true
        });

        this.validationModel = ko.validatedObservable({
            newPassword: this.newPassword,
            confirmNewPassword: this.confirmNewPassword
        });
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            // Mark start of long operation
            this.isBusy(true);

            this.webRequestUser.resetUserPassword(this.userId, this.newPassword(), this.confirmNewPassword()).done(() => {
                this.publishMessage("ca-user-reset-password-success");
            }).fail((e)=> {
                this.publishMessage("ca-user-reset-password-fail", e);
            }).always(()=>{
                this.isBusy(false);
                this.close(true);
            });
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
    viewModel: ScreenBase.createFactory(UserResetPasswordScreen),
    template: templateMarkup
};