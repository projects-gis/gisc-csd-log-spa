﻿import ko from "knockout";
import templateMarkup from "text!./menu.html";
import ScreenBase from "../../screenbase";
import * as BladeMargin from "../../../../app/frameworks/constant/bladeMargin";
import * as BladeSize from "../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums } from "../../../../app/frameworks/constant/apiConstant";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class ConfigurationMenuScreen extends ScreenBase {
    
    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_ConfigurationsTitlePage")());
        this.bladeMargin = BladeMargin.Narrow;
        this.bladeSize = BladeSize.XSmall;

        this.selectedIndex = ko.observable();

        this._selectingRowHandler = (item) => {
            if (item) {
                return this.navigate(item.id);
            }
            return false;
        };

        this.items = ko.observableArray([]);
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.AnnouncementAdmin)) {
            this.items.push({
                text: this.i18n("Menu_Announcements")(),
                id: 'ca-configuration-announcement'
            });
       }

        if (WebConfig.userSession.hasPermission(Constants.Permission.AlertManagement)) {
            this.items.push({
                text: this.i18n("Common_Alerts")(),
                id: 'ca-configuration-alert'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.AlertCategory)) {
            this.items.push({
                text: this.i18n("Configurations_Categories_Alert")(),
                id: 'ca-configuration-alert-categories'
            });
        }
        

        if(WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_Company))
        {
            this.items.push({
                text: this.i18n("Configurations_POIIcons")(),
                id: 'ca-configuration-poi-icon'
            });
        }
        
        if(WebConfig.userSession.hasPermission(Constants.Permission.TranslationManagement))
        {
            this.items.push({
                text: this.i18n("Configurations_Translation")(),
                id: 'ca-configuration-translation'
            });
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.Barrier)) {
            this.items.push({
                text: this.i18n("Configurations_Barrier")(),
                id: 'ca-configuration-barrier'
            });
        }
   
        if (WebConfig.userSession.hasPermission(Constants.Permission.Ticket_Response)) {
            this.items.push({
                text: this.i18n("Configurations_TicketResponse")(),
                id: 'ca-configuration-ticket-response'
            });  
        }

        if (WebConfig.userSession.hasPermission(Constants.Permission.Telematics_Configuration)) {
            this.items.push({
                text: this.i18n("Configurations_TelematicsConfiguration")(),
                id: 'ca-configuration-telematices-configuration'
            });  
        }
       
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {}

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}
}

export default {
viewModel: ScreenBase.createFactory(ConfigurationMenuScreen),
    template: templateMarkup
};