﻿import ko from "knockout";
import templateMarkup from "text!./message-manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";
import {LanguageFilter} from "./info";


class AnnouncementMessageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Announcements_Message")());

        this.isView = params ? params.isView : false; // Use for View Language from view.js
        

        this.announcementId = this.ensureNonObservable(params.id, -1);
        this.languageId = this.ensureNonObservable(params.languageId);
        this.message = this.ensureObservable(params.message);

        this.languages = ko.observableArray(); 
        this.languageOption = ko.observable();

        this.txtLanguage = ko.observable();

         this.enableLanguages = ko.pureComputed(() => {
            return _.isEmpty(params);
        });
    }

     /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // //Track change
        this.message.extend({
            trackChange: true
        });

        this.languageOption.extend({
            trackChange: true
        });

        
        // Validation
        this.message.extend({
            required: true
        });

        this.languageOption.extend({
            required: true
        });
        

        this.validationModel = ko.validatedObservable({
            message: this.message,
            languageOption: this.languageOption,
        });
    }

      /**
     * Get WebRequest specific for languages in Core Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    
    /**
     * @param {any} isFirstLoad
     * @returns
     * 
     * @memberOf AnnouncementMessageScreen
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

       var filterLanguage = new LanguageFilter();
       
        var d0 = this.webRequestLanguage.listLanguage(filterLanguage);
        
        $.when(d0).done((r0) => {
            var lang = r0["items"];
            if(lang){
               this.languages(lang);
            }

            var selectedLang = ScreenHelper.findOptionByProperty(this.languages, "id", this.languageId);

            if(selectedLang){
                this.languageOption(selectedLang);
                this.txtLanguage(selectedLang.name);
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        if (!this.isView) {
            actions.push(this.createAction("actOk", this.i18n("Common_OK")()));
        }
        actions.push(this.createActionCancel());
    }

   

     /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actOk"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            var model = {
                id: this.announcementId,
                languageId: this.languageOption().id,
                message: this.message()
            };

            model.canDelete = this.languageOption().isSystem ? false : true;
            
            this.publishMessage("ca-message-manage-changed", model);
            this.close(true);
        }
    }

    onUnload() {}
    
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementMessageScreen),
    template: templateMarkup
};