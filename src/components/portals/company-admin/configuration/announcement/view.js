﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestAnnouncementBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestAnnouncementBusinessUnit";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import i18nextko from "knockout-i18next";
import { LanguageFilter, AnnouncementFilter } from "./info";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";


class AnnouncementViewScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Announcements_View")());
        this.bladeSize = BladeSize.Medium;
        this.announcementId = this.ensureNonObservable(params.id, null);

        this.startDate = ko.observable();
        this.endDate = ko.observable();
        this.isAllBu = ko.observable();
        this.accessibleBusinessUnits = ko.observableArray();
        this.messages = ko.observableArray();
        this.displayBu = ko.observable();
        this.order = ko.observable([
            [0, "asc"]
        ]);

        this.subscribeMessage("ca-announcement-changed", (id) => {
            this.announcementId = id;
            this.getAnnouncementItem();
        });

        this._selectingRowHandler = (data) => {
            return this.navigate("ca-configuration-announcement-manage-message-manage", { isView: true, languageId: data.languageId, message: data.message});
        };
    }

    /**
     * Get WebRequest specific for AnnouncementBusinessUnit in Core Web API access.
     * @readonly
     */
    get webRequestAnnouncementBusinessUnit() {
        return WebRequestAnnouncementBusinessUnit.getInstance();
    }


    setupExtend() { }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        if (!this.announcementId) return; //return if refresh browser (F5)

        return this.getAnnouncementItem();
        
    }

    getAnnouncementItem() {
        var dfd = $.Deferred();
        var announcementFilter = new AnnouncementFilter();

        this.webRequestAnnouncementBusinessUnit.getAnnouncementBusinessUnit(this.announcementId, announcementFilter).done((item) => {
            if (item) {
                this.startDate(item.formatStartDate);
                this.endDate(item.formatEndDate);
                this.messages(item.messages);
                this.accessibleBusinessUnits(item.accessibleBusinessUnits);
                this.displayBu(item.isAllBusinessUnits);

                if (item.isAllBusinessUnits) {
                    this.isAllBu(i18nextko.t("Common_All")());
                }
                else {
                    this.isAllBu(i18nextko.t("Common_Custom")());
                }
            }

            dfd.resolve();
        }).fail((e) => {
            this.handleError(e);
            dfd.reject();
        });

        return dfd;
    }

    buildActionBar(actions) { }

    generateModel() { }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateAnnouncementAdmin)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteAnnouncementAdmin)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                this.navigate("ca-configuration-announcement-manage", { mode: Screen.SCREEN_MODE_UPDATE, id: this.announcementId });
                break;
            case "cmdDelete":
                this.showMessageBox(null, this.i18n("M021")(),
                    BladeDialog.DIALOG_YESNO).done((button) => {
                        switch (button) {
                            case BladeDialog.BUTTON_YES:
                                this.isBusy(true);

                                this.webRequestAnnouncementBusinessUnit.deleteAnnouncementBusinessUnit(this.announcementId).done(() => {
                                    this.isBusy(false);

                                    this.publishMessage("ca-announcements-search-filter-changed");
                                    this.close(true);
                                }).fail((e) => {
                                    this.isBusy(false);

                                    this.handleError(e);
                                });
                                break;
                        }
                    });
                break;
        }
    }

    onUnload() { }
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementViewScreen),
    template: templateMarkup
};
