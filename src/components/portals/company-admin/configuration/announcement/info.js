import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import {EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";

class SearchFilter {
    constructor (startDate = null, endDate = null) {
        this.startDate = startDate;
        this.endDate = endDate;
    }
}

export { SearchFilter };

class LanguageFilter{
    constructor () {
        this.enable = true;
    }
}
export { LanguageFilter };

class AnnouncementFilter{
    constructor () {
        this.includeAssociationNames = [
            EntityAssociation.Announcement.Messages
        ];
    }
}
export { AnnouncementFilter };