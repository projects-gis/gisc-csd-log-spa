﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants, Enums } from "../../../../../app/frameworks/constant/apiConstant";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestAnnouncementBusinessUnit from "../../../../../app/frameworks/data/apicore/webRequestAnnouncementBusinessUnit";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import { LanguageFilter, AnnouncementFilter } from "./info";

class AnnouncementManagementScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.mode = this.ensureNonObservable(params.mode);
        this.bladeSize = BladeSize.Medium;

        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
               this.bladeTitle(this.i18n("Announcements_CreateAnnouncement")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.bladeTitle(this.i18n("Announcements_UpdateAnnouncement")());
                break;
        }


        this.announcementId = this.ensureNonObservable(params.id, -1);
        
        this.startDate = ko.observable();
        this.endDate = ko.observable();

        this.messages = ko.observableArray([]);
        this._originalMessages = [];
        this.messagesValidator = ko.observable();
        this.accBuValidator = ko.observable();

        this.selectedMessages= ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);

        this.languageList = ko.observableArray([]);
        
        this._selectingRowHandler = (messages) => {
            if(messages) {
                return this.navigate("ca-configuration-announcement-manage-message-manage", { 
                    id: messages.id,
                    languageId: messages.languageId,
                    message: messages.message
                });
            }
            return false;
        };

        this._originalAccessibleBusinessUnits = [];
        this.accessibleBusinessUnits = ko.observableArray([]);
        this.order = ko.observable([
            [0, "asc"]
        ]);

        this.isAllBusinessUnit = ScreenHelper.createAllCustomObservableArray();
        this.isAllBusinessUnitSelected = ko.observable();
        this.visibleAccessibleBusinessUnit = ko.pureComputed(() => {
            var a = this.isAllBusinessUnitSelected();
            if (!a) {
                return true;
            } else {
                return a.value;
            }
        });

        // Observe change about Accessible Business Units
        this.subscribeMessage("cw-announcement-manage-accessible-business-unit-selected", (selectedBusinessUnits) => {
            var mappedSelectedBUs = selectedBusinessUnits.map((bu) => {
                let mbu = bu;
                mbu.businessUnitId = bu.id;
                mbu.businessUnitName = bu.businessUnitName;
                mbu.businessUnitPath = bu.businessUnitPath;
                mbu.canManage = true;
                return mbu;
            });
            this.accessibleBusinessUnits.replaceAll(mappedSelectedBUs);
        });

        //subscribe add/update message.
        this.subscribeMessage("ca-message-manage-changed", (message) => {

            var modifyMessageList = this.messages();       

            var isExistLang = false;

            _.forEach(modifyMessageList, (m)=> {
                if(m.languageId === message.languageId){
                    m.message = message.message;
                    isExistLang = true
                }
            });

            if(!isExistLang){
                var newMessage = this.generateNewMessage( this.languageList(), message);                
                modifyMessageList.push(newMessage);
            }

            this.messages.replaceAll(modifyMessageList);
        });
    }

    
    /**
     * Generate new message object.
     * @param {any} languageList
     * @param {any} message
     * @returns
     * 
     * @memberOf AnnouncementManagementScreen
     */
    generateNewMessage(languageList, message){

            var canDelete = message.isSystem ? false : true;
            var languageName = null;
            _.forEach(this.languageList(), (lang)=> {
                if(lang.id === message.languageId){
                    languageName = lang.name;
                }
            });    

             return  {
                id: message.id,
                languageId: message.languageId,
                languageName: languageName,
                message: message.message,
                canDelete: canDelete
            };
    }

     /**
     * Get WebRequest specific for languages in Core Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }

     /**
     * Get WebRequest specific for AnnouncementBusinessUnit in Core Web API access.
     * @readonly
     */
    get webRequestAnnouncementBusinessUnit() {
        return WebRequestAnnouncementBusinessUnit.getInstance();
    }

     /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Track change
        this.startDate.extend({
            trackChange: true
        });

        this.endDate.extend({
            trackChange: true
        });

        this.isAllBusinessUnitSelected.extend({
            trackChange: true
        });

        this.messages.extend({
            trackArrayChange: true
        });

        this.accessibleBusinessUnits.extend({
            trackArrayChange: true
        });


        // Validation
        this.startDate.extend({
            required: true
        });

        this.endDate.extend({
            required: true
        });

        var self = this;
        this.accessibleBusinessUnits.extend({
            required: {
                onlyIf: () => {
                    return self.isAllBusinessUnitSelected() && self.isAllBusinessUnitSelected().value == false;
                }
            }
        });

        //Custom validation
        //Check if messages array is not empty and default language message is not null.
        this.messagesValidator.extend({
            validation: {
                validator: (val, messages) => {
                    if(messages) {
                        var isNotNull = true; 
                         _.forEach(messages, (ms)=> {
                            if(!ms.message){
                                isNotNull = false;
                            }
                        });
                        return (messages.length > 0)
                        && isNotNull;
                    }
                    return true; 
                },
                params: this.messages,
                message: this.i18n("M001")()
            }
        });

        this.validationModel = ko.validatedObservable({
            startDate: this.startDate,
            endDate: this.endDate,
            messages: this.messagesValidator,
            accessibleBusinessUnits: this.accessibleBusinessUnits
        });
    }

    
     /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */    
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        //initail value for AccessibleCompanies
        var dfd = $.Deferred();

        var filterLanguage = new LanguageFilter();
        var announcementFilter = new AnnouncementFilter();

        var d1 = this.webRequestLanguage.listLanguage(filterLanguage);
        var d2 = (this.mode === Screen.SCREEN_MODE_UPDATE) ? this.webRequestAnnouncementBusinessUnit.getAnnouncementBusinessUnit(this.announcementId, announcementFilter) : null;


        $.when(d1, d2).done((r1, annoucement) => {
            

            var languages = r1["items"];
            this.languageList(languages);

            //Find default language's index.
            var index =  null
            _.forEach(languages, (lang, i)=> {
                    if(lang.isSystem){
                        index = i;
                    }
            });

            var isAllBusinessUnit = ScreenHelper.findOptionByProperty(this.isAllBusinessUnit, "value", true);

            if(annoucement){
                this.startDate(annoucement.startDate);
                this.endDate(annoucement.endDate);

                _.forEach(annoucement.messages, (m)=> {
                    m.canDelete = m.languageId === this.languageList()[index].id ? false : true;
                });

                this.messages(annoucement.messages);
                this._originalMessages = $.extend(true, [], annoucement.messages);

                
                this.accessibleBusinessUnits(annoucement.accessibleBusinessUnits);
                this._originalAccessibleBusinessUnits = $.extend(true, [], annoucement.accessibleBusinessUnits);
                
                isAllBusinessUnit = ScreenHelper.findOptionByProperty(this.isAllBusinessUnit, "value", annoucement.isAllBusinessUnits);

            }
            else{               

                //Compose JSON for Default Language.       
                var m = {
                        id: -1,
                        infoStatus: Enums.InfoStatus.Add,
                        languageId: this.languageList()[index].id,
                        languageName:  this.languageList()[index].name,
                        canDelete:  false,
                        message: null
                }
                this._originalMessages = $.extend(true, [], this.messages());         
                this.messages().push(m);
            }
            
            this.isAllBusinessUnitSelected(isAllBusinessUnit);

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    
    
    /**
     * Add language.
     * @memberOf AnnouncementManagementScreen
     */
    addLanguage() {
        this.navigate("ca-configuration-announcement-manage-message-manage");
    }

    
    /**
     * Add accessible company
     * 
     * @memberOf AnnouncementManagementScreen
     */
    addAccessibleBusinessUnits(){
        var targetMode = (this.mode === "create") ? "create" : "update";
        var selectedBusinessUnitIds = this.accessibleBusinessUnits().map((obj) => {
            return obj.id;
        });

        this.navigate("ca-configuration-accessible-bu-select", {
            mode: targetMode,
            selectedBusinessUnitIds: selectedBusinessUnitIds
        });
    }

    /**
     * 
     * @public
     * @param {any} actions
     */
    buildActionBar(actions) {
        //if (WebConfig.userSession.hasPermission(Constants.Permission.UpdateAnnouncement)) {
            actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
            actions.push(this.createActionCancel());
        //}
    }

    
    /**
     * Generate message model from View Model
     * @returns
     * 
     * @memberOf AnnouncementManagementScreen
     */
    generateModel(){

        var model = {
            id: this.announcementId,
            startDate: this.startDate(),
            endDate: this.endDate(),
            isAllBusinessUnits: this.isAllBusinessUnitSelected().value,
            //isAllCompanies: this.isAllCompaniesSelected().value,
            messages: this.messages(),
            infoStatus: this.mode === Screen.SCREEN_MODE_CREATE ? Enums.InfoStatus.Add : Enums.InfoStatus.Update
        };

         
        if (this.isAllBusinessUnitSelected && this.isAllBusinessUnitSelected().value) {
            model.accessibleBusinessUnits = [];
        } 
        else {
            model.accessibleBusinessUnits = Utility.generateArrayWithInfoStatus(
                this._originalAccessibleBusinessUnits, 
                this.accessibleBusinessUnits(),
                "businessUnitId"
            );
        }

        //Set status info if languageId changed.
        var messagesWithInfo = Utility.generateArrayWithInfoStatus(
            this._originalMessages, 
            this.messages(),
            "languageId"
        );

       //Set status info if message changed.
        _.forEach(this.messages(), (s)=> {
            var index = _.findIndex(messagesWithInfo, ['languageId', s.languageId]);
            if(index != -1){
                var isSame = messagesWithInfo[index].message === s.message;
                if(!isSame){
                    messagesWithInfo[index].message = s.message;
                    messagesWithInfo[index].infoStatus = Enums.InfoStatus.Update;
                }
            }
        });
        
        model.messages = messagesWithInfo;

        return model;
    }

      /**
     * 
     * @public
     * @param {any} sender
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actSave"){
             if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            this.isBusy(true);

            var model = this.generateModel();
            //console.log(JSON.stringify(model));

             switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                     this.webRequestAnnouncementBusinessUnit.createAnnouncementBusinessUnit(model, true).done((response) => {
                        this.isBusy(false);

                         this.publishMessage("ca-announcement-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
                case Screen.SCREEN_MODE_UPDATE:
                     this.webRequestAnnouncementBusinessUnit.updateAnnouncementBusinessUnit(model, true).done((response) => {
                        this.isBusy(false);

                         this.publishMessage("ca-announcement-changed", response.id);
                        this.close(true);
                    }).fail((e) => {
                        this.isBusy(false);

                        this.handleError(e);
                    });

                    break;
            }
        }
    }

     /**
     * 
     * 
     * @param {any} commands
     * 
     * @memberOf ConfigurationVehicleIconManageScreen
     */
    buildCommandBar(commands) {
        //if (this.mode === Screen.SCREEN_MODE_UPDATE) {
            if (WebConfig.userSession.hasPermission(Constants.Permission.DeleteAnnouncement)) {
                commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
            }
        //}
    }


    /**
     * 
     * 
     * @param {any} sender
     * 
     * @memberOf ConfigurationVehicleIconManageScreen
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDelete") {
            this.showMessageBox(null, this.i18n("M021")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestAnnouncement.deleteAnnouncement(this.announcementId).done(() => {
                            this.isBusy(false);
                            this.publishMessage("bo-announcement-changed", this.announcementId);
                            this.close(true);
                        }).fail((e) => {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    }

    
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}
}

export default {
    viewModel: ScreenBase.createFactory(AnnouncementManagementScreen),
    template: templateMarkup
};
