﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";

import WebRequestAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomAreaCategory";
import WebRequestBarrier from "../../../../../app/frameworks/data/apitrackingcore/webRequestBarrier";

class ConfigurationBarrierViewListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        this.barrierId = ko.observable(params.data.id);
        this.bladeTitle(this.i18n("Configurations_Barrier_View")());
        this.bladeSize = BladeSize.Medium;

        this.barrierData = ko.observable();
        this.txtName = ko.observable();
        this.txtAreaCategory = ko.observable();
        this.txtTime = ko.observable();
        this.txtDayfoWeek = ko.observable();
        this.txtEnable = ko.observable();
        this.txtDescription = ko.observable();

        this.subscribeMessage("ca-configuration-barrier-refresh-view", info => {
            this.getBarrier();
        });

        
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        this.getBarrier();
    }

    getBarrier(){
        this.isBusy(true);
        var dfd = $.Deferred();
        var dfdBarrierDetail = this.webRequestBarrier.barrierDetail(this.barrierId());
        

        $.when(dfdBarrierDetail).done((resourcebarrier) => {
            this.isBusy(false);
            this.barrierData(resourcebarrier);
            this.txtName(resourcebarrier.name);
            this.txtTime(resourcebarrier.formatTime);
            this.txtDayfoWeek(resourcebarrier.formatDayofWeek);
            this.txtEnable(resourcebarrier.formatEnable);
            this.txtDescription(resourcebarrier.description);
            this.txtAreaCategory(resourcebarrier.areaCategoryDisplayName);  
        }).fail((e) => {
            dfd.reject(e);
        }).always(() => {
            this.isBusy(false);
        });

        return dfd;
    }
    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.Barriers_Update)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.Barriers_Delete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));            
        }
    }

    onCommandClick(sender) {
        switch(sender.id){
            case "cmdUpdate":
                this.navigate("ca-configuration-barrier-manage", {
                    mode: Screen.SCREEN_MODE_UPDATE,
                    data: this.barrierData()
                });                
                break;
            case "cmdDelete":
                    this.showMessageBox(null,this.i18n("M202")(),
                        BladeDialog.DIALOG_YESNO).done((button) => {
                            switch (button) {
                                case BladeDialog.BUTTON_YES:
                                            this.isBusy(true);
                                            this.webRequestBarrier.deleteBarrier(this.barrierId()).done(() => {
                                                this.isBusy(false);
                                                this.publishMessage("ca-configuration-barrier-refresh-list");
                                                this.close(true);
                                            }).fail((e) => {
                                                this.handleError(e);
                                            }).always(() => {
                                                this.isBusy(false);
                                            });
                                        break;
                                case BladeDialog.BUTTON_NO:
                                    
                                        break;
                                    default:
                                        break;  
                            }
                        }
                    );
                break;
            default:
            break;
        }
        
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestBarrier() {
        return WebRequestBarrier.getInstance();
    }
    get webRequestAreaCategory() {
        return WebRequestAreaCategory.getInstance();
    }
}



export default {
viewModel: ScreenBase.createFactory(ConfigurationBarrierViewListScreen),
    template: templateMarkup
};