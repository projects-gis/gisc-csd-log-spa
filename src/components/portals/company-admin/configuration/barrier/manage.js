﻿import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webRequestCustomAreaCategory";
import WebRequestBarrier from "../../../../../app/frameworks/data/apitrackingcore/webRequestBarrier";
import DefaultSorting from "../../../../../app/frameworks/constant/defaultSorting";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";

class ConfigurationBarrierManageListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties
        this.mode = this.ensureNonObservable(params.mode);
        switch (this.mode) {
            case Screen.SCREEN_MODE_CREATE:
                this.bladeTitle(this.i18n("Configurations_Barrier_Create")());
                break;
            case Screen.SCREEN_MODE_UPDATE:
                this.barrierUpdate = ko.observable(params.data);
                this.bladeTitle(this.i18n("Configurations_Barrier_Update")());
                break;
        }
        this.bladeSize = BladeSize.Medium;

        this.barrierId = ko.observable();
        this.name = ko.observable();
        this.areaCategory = ko.observable();
        this.vehicleType = ko.observable();
        this.timeStart = ko.observable();
        this.timeEnd = ko.observable();        
        this.enable = ko.observable();
        this.description = ko.observable(null);

        this.isCheckDay = ko.observable(false);
        this.chkAllDays = ko.observable(false);
        this.chkMon = ko.observable(false);
        this.chkTue = ko.observable(false);
        this.chkWed = ko.observable(false);
        this.chkThu = ko.observable(false);
        this.chkFri = ko.observable(false);
        this.chkSat = ko.observable(false);
        this.chkSun = ko.observable(false);

        //ddr
        this.ddrAreaCategory = ko.observableArray([]);
        this.ddrVehicleTypeLst = ko.observableArray();
        this.ddrEnabledLst = ko.observableArray(this.setEnabledLst());

        this.timeStart.subscribe((time)=> {
            let startTime = this.setObjDateTime(this.timeStart());
            let endTime = this.setObjDateTime(this.timeEnd());

            if(startTime >= endTime
                && endTime != null) {
                this.timeEnd(this.setObjToTime(this.addMinutes(startTime, 1)))
            }
        })

        this.timeEnd.subscribe(()=> {
            let startTime = this.setObjDateTime(this.timeStart());
            let endTime = this.setObjDateTime(this.timeEnd());

            if(startTime >= endTime
                && startTime != null) {
                this.timeStart(this.setObjToTime(this.minusMinutes(endTime, 1)))
            }
        })

        this.chkMon.subscribe((state)=> {            
            this.checkdays();
        });

        this.chkTue.subscribe((state)=> {
            this.checkdays();
        });

        this.chkWed.subscribe((state)=> {
            this.checkdays();
        });

        this.chkThu.subscribe((state)=> {
            this.checkdays();
        });

        this.chkFri.subscribe((state)=> {
            this.checkdays();
        });

        this.chkSat.subscribe((state)=> {
            this.checkdays();
        });

        this.chkSun.subscribe((state)=> {
            this.checkdays();
        });
    }

    onCheckAllDays() {
        this.chkAllDays(!this.chkAllDays())
        let state = this.chkAllDays();
        this.chkMon(state);
        this.chkTue(state);
        this.chkWed(state);
        this.chkThu(state);
        this.chkFri(state);
        this.chkSat(state);
        this.chkSun(state);
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        var dfd = $.Deferred();

        var dfdVehicleType = this.webRequestEnumResource.listEnumResource({ companyId: WebConfig.userSession.currentCompanyId, types: [ Enums.ModelData.EnumResourceType.VehicleType ], sortingColumns: DefaultSorting.EnumResource });
        var dfdAreaCategory = this.webRequestAreaCategory.listCustomAreaCategorySummary({ companyId: WebConfig.userSession.currentCompanyId });

        $.when(dfdVehicleType, dfdAreaCategory).done((vehicleTypeLst, areaCategory) => {
            this.ddrVehicleTypeLst(vehicleTypeLst["items"]);
            this.ddrAreaCategory(areaCategory['items']);

            if(Screen.SCREEN_MODE_UPDATE === this.mode){
                this.barrierId(this.barrierUpdate().id);
                this.name(this.barrierUpdate().name);
                this.areaCategory(ScreenHelper.findOptionByProperty(this.ddrAreaCategory, "id", this.barrierUpdate().areaCategoryId));
                this.vehicleType(ScreenHelper.findOptionByProperty(this.ddrVehicleTypeLst, "value", this.barrierUpdate().vehicleType));

                this.chkMon(this.barrierUpdate().monday);
                this.chkTue(this.barrierUpdate().tuesday);
                this.chkWed(this.barrierUpdate().wednesday);
                this.chkThu(this.barrierUpdate().thursday);
                this.chkFri(this.barrierUpdate().friday);
                this.chkSat(this.barrierUpdate().saturday);
                this.chkSun(this.barrierUpdate().sunday);
                this.timeStart(this.barrierUpdate().formatTime.split(" - ")[0]);
                this.timeEnd(this.barrierUpdate().formatTime.split(" - ")[1]);

                
                this.enable(ScreenHelper.findOptionByProperty(this.ddrEnabledLst, "id", this.barrierUpdate().enable));
                this.description(this.barrierUpdate().description);
            }

            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    buildCommandBar(commands) {
        
    }

    onCommandClick(sender) {

    }

    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {

            if(!this.chkMon()
                && !this.chkTue()
                && !this.chkWed()
                && !this.chkThu()
                && !this.chkFri()
                && !this.chkSat()
                && !this.chkSun()) {
                this.isCheckDay(true);
                //return;
            }

            if (!this.validationModel.isValid() || this.isCheckDay()) {
                this.validationModel.errors.showAllMessages();
                //this.isCheckDay(true);
                return;
            } else {
                this.isBusy(true);

                let model = {
                    name: this.name(),
                    areaCategoryId: this.areaCategory()['id'],
                    vehicleType: this.vehicleType()['value'],
                    enable: this.enable()['id'],
                    formatTime: this.timeStart() + "-" + this.timeEnd(),
                    sunday: this.chkSun(),
                    monday: this.chkMon(),
                    tuesday: this.chkTue(),
                    wednesday: this.chkWed(),
                    thursday: this.chkThu(),
                    friday: this.chkFri(),
                    saturday: this.chkSat(),
                    description: this.description()
                };
                switch(this.mode){
                    case Screen.SCREEN_MODE_CREATE:
                        model.infoStatus = Enums.InfoStatus.Add;
                        this.webRequestBarrier.createBarrier(model).done((res)=> {
                            this.publishMessage("ca-configuration-barrier-refresh-list", res);
                            this.close(true);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        break
                    case Screen.SCREEN_MODE_UPDATE:
                        model.id = this.barrierId();
                        model.infoStatus = Enums.InfoStatus.Update;
                        this.webRequestBarrier.editBarrier(model).done((res)=> {
                            this.publishMessage("ca-configuration-barrier-refresh-list", res);
                            this.publishMessage("ca-configuration-barrier-refresh-view", res);
                            this.close(true);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        break
                    default:
                        break
                }
                
            }
        }
    }

    setupExtend() {
        this.name.extend({ trackChange: true });
        this.areaCategory.extend({ trackChange: true });
        this.vehicleType.extend({ trackChange: true });
        this.timeStart.extend({ trackChange: true });
        this.timeEnd.extend({ trackChange: true });
        this.enable.extend({ trackChange: true });
        this.description.extend({ trackChange: true });

        this.chkAllDays.extend({ trackChange: true });
        this.chkMon.extend({ trackChange: true });
        this.chkTue.extend({ trackChange: true });
        this.chkWed.extend({ trackChange: true });
        this.chkThu.extend({ trackChange: true });
        this.chkFri.extend({ trackChange: true });
        this.chkSat.extend({ trackChange: true });
        this.chkSun.extend({ trackChange: true });

        this.name.extend({ required: true });
        this.areaCategory.extend({ required: true });
        this.vehicleType.extend({ required: true });
        this.timeStart.extend({ required: true });
        this.timeEnd.extend({ required: true });
        this.enable.extend({ required: true });

        this.name.extend({
            serverValidate: {
                params: "Name",
                message: this.i18n("M010")()
            }
        });

        this.validationModel = ko.validatedObservable({
            name: this.name,
            areaCategory: this.areaCategory,
            vehicleType: this.vehicleType,
            timeStart: this.timeStart,
            timeEnd: this.timeEnd,
            enable: this.enable
        });
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    checkdays() {
        if(this.chkMon()
            && this.chkTue()
            && this.chkWed()
            && this.chkThu()
            && this.chkFri()
            && this.chkSat()
            && this.chkSun()) {
            this.chkAllDays(true)            
        } else { 
            this.chkAllDays(false)
        }

        this.isCheckDay(false);
    }

    setEnabledLst() {
        let lst = [];
        lst.push({
            id: true,
            name: this.i18n('Common_Yes')()
        },{
            id: false,
            name: this.i18n('Common_No')()
        })

        return lst;
    }

    setObjDateTime(timeObj) {
        var newDateTime = "";

        if(timeObj) {
            let d = new Date();
            let year = d.getFullYear().toString();
            let month = (d.getMonth() + 1).toString();
            let day = d.getDate().toString();

            month = month.length > 1 ? month : "0" + month;
            day = day.length > 1 ? day : "0" + day;
            newDateTime = year + "-" + month + "-" + day + "T" + timeObj + ":00";
            newDateTime = new Date(newDateTime)
        } else {
            newDateTime = null;
        }

        return newDateTime;
    }

    setObjToTime(dateObj) {
        var time = "";

        if(dateObj) {
            let d = new Date(dateObj);
            let hour = d.getHours().toString();
            let minute = d.getMinutes().toString();

            hour = hour.length > 1 ? hour : "0" + hour;
            minute = minute.length > 1 ? minute : "0" + minute;
            time = hour + ":" + minute;
        } else {
            time = null;
        }

        return time;
    }

    setTimeToInt(time) {
        let numInt = 0;
        if(time) {
            numInt = time.split(':');
        }
        numInt = parseInt(numInt[0]);
        return numInt
    }

    addMinutes(currentDate, minutes) {
        var result = new Date(currentDate);
        result.setMinutes(parseInt(result.getMinutes()) + (parseInt(minutes)));
        return result
    }

    minusMinutes(currentDate, minutes) {
        var result = new Date(currentDate);
        result.setMinutes(parseInt(result.getMinutes()) - parseInt(minutes));
        return result
    }

    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }

    get webRequestAreaCategory() {
        return WebRequestAreaCategory.getInstance();
    }

    get webRequestBarrier() {
        return WebRequestBarrier.getInstance();
    }
}



export default {
viewModel: ScreenBase.createFactory(ConfigurationBarrierManageListScreen),
    template: templateMarkup
};