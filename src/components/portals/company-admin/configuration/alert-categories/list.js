﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants
} from "../../../../../app/frameworks/constant/apiConstant";


import WebRequestAlertCategories from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";


class ConfigurationAlertCategoryListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        //this.barrierId = ko.observable(params.data.id);
        this.bladeTitle(this.i18n("Configurations_Categories_Alert")());
        this.bladeSize = BladeSize.Medium;

        this.alertCategories = ko.observableArray([]);

        this.filterText = ko.observable('');
        this.selectedalertCategories = ko.observable(null);
        this.recentChangedRowIds = ko.observableArray([]);
        this.order = ko.observable([
            [0, "asc"]
        ]);

        this.selectRow = (row) => {
            this.navigate("ca-configuration-alert-categories-view", {
                data: row
            });
        }

        this.subscribeMessage("ca-alert-Category-list", info => {
            let id = info != undefined || info != null ? [info.id] : null
            this.genList(id);
        });

        this.subscribeMessage("ca-alert-Category-list-search", (searchFilter) => {
            this.isBusy(true);
            this.searchFilter = searchFilter;

            this.webRequestAlertCategories.listAlertCategorySummary(this.searchFilter).done((r) => {
                this.alertCategories.replaceAll(r['items'])
                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });


    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;



        this.genList();

    }

    genList(id) {
        this.isBusy(true);
        this.webRequestAlertCategories.listAlertCategorySummary(WebConfig.userSession.currentCompanyId).done((response) => {
            this.isBusy(false);
            this.alertCategories.replaceAll(response['items'])
            if(id) {
                this.recentChangedRowIds.replaceAll(id);
            }
        });
    }


    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.Alert_Category_Create)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
       // commands.push(this.createCommand("cmdFilter", this.i18n("Common_Search")(), "svg-cmd-search"));

    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdCreate":
                this.navigate("ca-configuration-alert-categories-manage", {
                    mode: Screen.SCREEN_MODE_CREATE
                });
                break;
            case "cmdFilter":
                this.navigate("ca-configuration-alert-categories-search", {
                    searchFilter: this.searchFilter
                });
                break;
            default:
                break;
        }

    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestAlertCategories() {
        return WebRequestAlertCategories.getInstance();
    }

}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationAlertCategoryListScreen),
    template: templateMarkup
};