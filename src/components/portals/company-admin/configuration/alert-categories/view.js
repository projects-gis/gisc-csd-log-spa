﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";

import WebRequestAlertCategories from "../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";



class ConfigurationAlertCategoryViewListScreen extends ScreenBase {
    constructor(params) {
        super(params);
        // Properties

        //console.log(params);
        this.resultAlert = ko.observable(params.data);
        this.bladeTitle(this.i18n("Configurations_Categories_Alert_ViewAlert")());
        this.bladeSize = BladeSize.Small;


        this.Name = ko.observable();
        this.Description = ko.observable();
        this.Rank = ko.observable();

        this.subscribeMessage("ca-alert-Category-view-refresh", info => {
            this.getAlertCategory();
        });

    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.getAlertCategory();
    }

    getAlertCategory(){
        this.isBusy(true);
        var dfd = $.Deferred();
        var dfdAlertCategory = this.webRequestAlertCategories.getAlertCategory(this.resultAlert().id);
 
        $.when(dfdAlertCategory).done((resourceAlertCategory) => {
    
            this.Name(resourceAlertCategory.name);
            this.Description(resourceAlertCategory.description);
            this.Rank(resourceAlertCategory.rank); 

        }).fail((e) => {
            dfd.reject(e);
        }).always(() => {
            this.isBusy(false);
        });

        return dfd;
    }


    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.Alert_Category_Update)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        if (WebConfig.userSession.hasPermission(Constants.Permission.Alert_Category_Delete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
    }

    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
            this.navigate("ca-configuration-alert-categories-manage", {
                mode: Screen.SCREEN_MODE_UPDATE,
                data: this.resultAlert()
            });      
                break;
            case "cmdDelete":
            this.showMessageBox(null,this.i18n("M206")(),
            BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                                this.isBusy(true);
                                this.webRequestAlertCategories.deleteAlertCategory(this.resultAlert().id).done(() => {
                                    this.isBusy(false);
                                    this.publishMessage("ca-alert-Category-list");
                                    this.close(true);
                                }).fail((e) => {
                                    this.showMessageBox(null,this.i18n("M207")());
                                    this.handleError(e);
                                }).always(() => {
                                    this.isBusy(false);
                                });
                            break;
                    case BladeDialog.BUTTON_CANCEL:
                        
                            break;
                        default:
                            break;  
                }
            }
        );
                break;
            default:
                break;
        }

    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() { }

    get webRequestAlertCategories() {
        return WebRequestAlertCategories.getInstance();
    }

  
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationAlertCategoryViewListScreen),
    template: templateMarkup
};