﻿import ko from "knockout";
import templateMarkup from "text!./bu-asset-select.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import WebRequestVehicle from "../../../../../app/frameworks/data/apitrackingcore/webRequestVehicle";
import WebRequestUser from "../../../../../app/frameworks/data/apicore/webRequestUser";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import {VehicleFilter, UserFilter} from "./infos";

class BuAssetSelect extends ScreenBase {
    constructor(params) {
        super(params);
        
        this.bladeTitle(this.i18n("Common_Assets")());

        this.vehicles = ko.observableArray([]);
        this.users = ko.observableArray([]);
        this.orderVehicles = ko.observable([[ 1, "asc" ]]);
        this.orderUsers = ko.observable([[ 1, "asc" ]]);

        this.selectedVehicleIds = params.selectedVehicleIds;
        this.selectedDeliveryMenIds = params.selectedDeliveryMenIds;
        
        this.selectedVehicles = ko.observableArray();
        this.selectedDeliveryMen = ko.observableArray();
        
        this.businessUnitList = ko.observableArray([]);
        this.businessUnit = ko.observable();

        
    }
    /**
     * Get WebRequest specific for User in Web API access.
     * @readonly
     */
    get webRequestUser() {
        return WebRequestUser.getInstance();
    }
    /**
     * Get WebRequest specific for Vehicle module in Web API access.
     * @readonly
     */
    get webRequestVehicle() {
        return WebRequestVehicle.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if(!isFirstLoad) return;

        // var dfd = $.Deferred();
        // var a1 = this.visibleVehicle() ? this.webRequestVehicle.listVehicleSummary(new VehicleFilter(this.featureId())) : null;
        // var a2 = this.visibleDeliveryMen() ? this.webRequestUser.listUserSummary(new UserFilter()) : null;

        // $.when(a1, a2).done((r1, r2) => {
        //     if(r1){
        //         this.vehicles(r1.items);
        //         this.selectedVehicleIds.forEach(function(element) {
        //             let vehicle = ko.utils.arrayFirst(this.vehicles(), function(v) {
        //                 return v.id === element;
        //             });
        //             if(vehicle) {
        //                 this.selectedVehicles.push(vehicle);
        //             }
        //         }, this);
        //     }
        //     if(r2){
        //         this.users(r2.items);
        //         this.selectedDeliveryMenIds.forEach(function(element) {
        //             let user = ko.utils.arrayFirst(this.users(), function(u) {
        //                 return u.id === element;
        //             });
        //             if(user) {
        //                 this.selectedDeliveryMen.push(user);
        //             }
        //         }, this);
        //     }

        //     dfd.resolve(); 
        // }).fail((e) => {
        //     dfd.reject(e);
        // });

        // return dfd;
    }
     /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // this.selectedVehicles.extend({
        //    trackArrayChange: true
        // });
        // this.selectedDeliveryMen.extend({
        //    trackArrayChange: true
        // });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if(sender.id === "actOK") {
            // var selectedVehicleIds = [];
            // var selectedDeliveryMenIds = [];

            // this.selectedVehicles().forEach(function(element) {
            //     selectedVehicleIds.push(element.id);
            // }, this);
            // this.selectedDeliveryMen().forEach(function(element) {
            //     selectedDeliveryMenIds.push(element.id);
            // }, this);

            // this.publishMessage("ca-configuration-alert-assets-selected", { selectedVehicleIds: selectedVehicleIds, selectedDeliveryMenIds: selectedDeliveryMenIds });
            // this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

}

export default {
    viewModel: ScreenBase.createFactory(BuAssetSelect),
    template: templateMarkup
};