﻿import ko from "knockout";
import templateMarkup from "text!./stop-overtime-province-manage.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestProvince from "../../../../../../app/frameworks/data/apicore/webRequestProvince";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";

class StopOvertimeProvinceManageScreen extends ScreenBase {
    constructor(params) {
        super(params);

        ko.validation.rules['provinceValidate'] = {
        
            validator: function (val, otherField) {
                let boolean = true;
                if (val && otherField != null) {
                    for (let i = 0; i < val.length; i++) {
        
                        for (let j = 0; j < otherField.length; j++) {
        
                            if(val[i] == otherField[j]){
                                boolean = false;
                                i=val.length;
                            }
                        }
                    }
                }
                return boolean;
        
                // return val != this.getValue(otherField); //ถ้าค่าที่ใส่มาไม่เท่ากับค่าที่มีอยู่แล้ว คืน true
            },
            message: this.i18n("M010")()
        };

        ko.validation.registerExtenders();



        this.stopOvertimeLst = params.stopOvertimeList;
        this.paramsData = params;
        this.bladeSize = BladeSize.Medium;
        this.mode = this.ensureNonObservable(params.mode);
        this.editTypeMode = this.ensureNonObservable(params.editTypeMode);
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.temperatureUnit = ko.observable('');
        this.selectedSubMenuItem = ko.observable(null);
        
        // switch (this.mode) {
        //     case Screen.SCREEN_MODE_CREATE:
        //        this.bladeTitle(this.i18n("Unathorized_Create_Stop_Overtime_Province")());
        //         break;
        //     case Screen.SCREEN_MODE_UPDATE:
        //         this.bladeTitle(this.i18n("Unathorized_Update_Stop_Overtime_Province")());
        //         break;
        // }


        if(this.mode == Screen.SCREEN_MODE_CREATE){

            if(this.editTypeMode == "update"){
                this.bladeTitle(this.i18n("Unathorized_Update_Stop_Overtime_Province")());
            }else{
                this.bladeTitle(this.i18n("Unathorized_Create_Stop_Overtime_Province")());
            }

        }else if(this.mode == Screen.SCREEN_MODE_UPDATE){

                if(this.editTypeMode == "create"){
                    this.bladeTitle(this.i18n("Unathorized_Create_Stop_Overtime_Province")());
                }else{
                    this.bladeTitle(this.i18n("Unathorized_Update_Stop_Overtime_Province")());
                }
        }

        this.enableType = ko.observable(true);
      
        this.provinceOptions = ko.observableArray();
        // this.address = new CompanyAddress();

        this.selectedProvince = ko.observable();
        this.provinceGroupName = ko.observable();
        this.stopDurationValue = ko.observable();
        this.regionOptions = ko.observableArray([]);
        this.selectedRegion = ko.observable();

        this.selectedRegion.subscribe((selectedRegionObj) => {
                
                var dfd = $.Deferred();
                var provinceFilter =    {   "countryIds": [1],
                                            "regionIds": (selectedRegionObj)?[selectedRegionObj.id] : undefined
                                        }
                var d1 = this.webRequestProvince.listProvince(provinceFilter);
                var allObj;
                switch (this.mode) {

                    case Screen.SCREEN_MODE_CREATE:

                    if (selectedRegionObj) {

                        $.when(d1).done((r1) => {
                            
                            allObj = {}; //add all
                            allObj.id = 0;
                            allObj.name = this.i18n("Common_CheckAll")();
                            allObj.parent = "#";

                            r1.items.forEach((items)=>{
                                items.parent = 0
                            })
        
                            r1.items.unshift(allObj);

                            this.provinceOptions(r1.items)//this.categoryNameOptions(categoryName)
                        
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
        
                        return dfd;
                    }
    
                    break;

                    case Screen.SCREEN_MODE_UPDATE:

                  
                    if (selectedRegionObj) {

                        $.when(d1).done((r1) => {
                            
                            allObj = {}; //add all
                            allObj.id = 0;
                            allObj.name = this.i18n("Common_CheckAll")();
                            allObj.parent = "#";

                            r1.items.forEach((items)=>{
                                items.parent = 0
                            })
        
                            r1.items.unshift(allObj);

                            this.provinceOptions(r1.items)//this.categoryNameOptions(categoryName)
                        
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
        
                        return dfd;
                    }

                break;
 
                }

            

        })

        this.maxId = this.getMaxId(params.stopOvertimeList);
        this.provinceIdList = this.combineCategoryId(params.stopOvertimeList);

        
    }

    getMaxId(data) {
        let maxId;
        if (data) {
            if (data.length != 0) {
                maxId = Math.max.apply(Math, data.map(function (item) { return item.dataId; })) //หา maxId จาก object array โดย https://stackoverflow.com/questions/4020796/finding-the-max-value-of-an-attribute-in-an-array-of-objects
            } else {
                maxId = 0;
            }
        }
      
        return maxId;
    }

    combineCategoryId(stopOvertimeLst){
        
        let provinceIdList = []
        if (stopOvertimeLst) { 
            if(stopOvertimeLst.length!=0 && stopOvertimeLst!=undefined){ 
             stopOvertimeLst.forEach((items,index)=>{
               
                    items.selectedProvinceId.forEach((id)=>{
                        provinceIdList.push(id)
                    })
                })
    
            }
         }
        let provinceIdAll = provinceIdList.filter((items)=>{
            return items != 0;
        })

        return provinceIdAll;
    }
    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webRequestProvince() {
        return WebRequestProvince.getInstance();
    }
  
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();

        var d3 = this.webRequestProvince.listRegion();

        switch (this.mode) {

            case Screen.SCREEN_MODE_CREATE:
                    $.when(d3).done((r3) => {
                        this.regionOptions(r3.items)

                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
            break;

            case Screen.SCREEN_MODE_UPDATE:
               
                    var provinceFilter =    {   "countryIds": [1],
                                                "regionIds": [this.paramsData.selectedRegion]
                                        }
                    var d1 = this.webRequestProvince.listProvince(provinceFilter);

                    $.when(d1,d3).done((r1,r3) => {

                        this.regionOptions(r3.items)
                        this.selectedRegion(ScreenHelper.findOptionByProperty(this.regionOptions, "id", this.paramsData.selectedRegion ) )

                        this.provinceGroupName(this.paramsData.provinceGroupName)
                        
                        this.stopDurationValue(this.paramsData.stopDurationForUpdate)

                    //////////////////////////////////////////Category Name Dropdown Update//////////////////////////////////////////////////////////////////////
                   
            
                        var allObj = {}; //add all
                        allObj.id = 0;
                        allObj.name = this.i18n("Common_CheckAll")();
                        allObj.parent = "#";

                        r1.items.forEach((items)=>{
                            items.parent = 0
                        })

                        r1.items.unshift(allObj);

                        this.provinceOptions(r1.items)//this.categoryNameOptions(categoryName)
                        this.selectedProvince(this.paramsData.selectedProvinceId)
                

                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
                break;
        }
        return dfd; 
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        this.provinceGroupName.extend({
            required: true
        });
        this.selectedRegion.extend({
            required: true
        });
        this.selectedProvince.extend({
            required: true,
            provinceValidate:(this.provinceIdList != 0) ? this.provinceIdList : null
        });
        this.stopDurationValue.extend({
            required: true
        });
        
        this.validationModel = ko.validatedObservable({
            provinceGroupName: this.provinceGroupName,
            selectedRegion: this.selectedRegion,
            selectedProvince: this.selectedProvince,
            stopDurationValue: this.stopDurationValue
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate Alert Configuration model from View Model
     * 
     * @returns Alert Configuration model which is ready for ajax request 
     */
    getProvinceNameList(){
        let provinceNameList = []
        let selectProvinceNotAll =  this.selectedProvince().filter((items)=>{

            return items != 0;
        })

        this.provinceOptions().forEach((items)=>{

            selectProvinceNotAll.forEach((id)=>{

                if(items.id == id){
                    provinceNameList.push(items.name)
                }

            })
            
        })
        
        return provinceNameList;
    }
    generateModel() {
        let provinceNameList = this.getProvinceNameList()
        var model = {
                dataId: (this.mode == Screen.SCREEN_MODE_CREATE) ? this.maxId + 1 : this.paramsData.stopOvertimeId,
                configType:3,
                provinceGroupName: this.provinceGroupName(),
                selectedRegion: this.selectedRegion().id,
                selectedProvinceId: this.selectedProvince(),
                selectedProvinceForDisplay: provinceNameList,
                stopDuration: this.stopDurationValue()+" "+this.i18n("Unathorized_Stop_Minutes")(),
                duration:this.stopDurationValue(),
                provinceIds: this.selectedProvince(),
                regionId:this.selectedRegion().id
        };
     
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();

                return;
            }

            this.isBusy(true);
            var model = this.generateModel();
            switch (this.mode) {
                case Screen.SCREEN_MODE_CREATE:
                   
                    let stopOvertimeTableData = this.stopOvertimeLst

                    stopOvertimeTableData.push(model)

                    this.publishMessage("stop-over-time-manage", stopOvertimeTableData);
                    this.close(true);
                    break;
                case Screen.SCREEN_MODE_UPDATE:

                let allTableList =this.paramsData.allTableList

                let stopOvertimeUpdate = allTableList.map((item)=>{

                    if(item.dataId == model.dataId){
                        item = model
                    }
                    return item
                
                })
                
                this.publishMessage("stop-over-time-manage", stopOvertimeUpdate);
                this.close(true);
                    break;   
            }
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
   
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedSubMenuItem(null);
    }

}

export default {
    viewModel: ScreenBase.createFactory(StopOvertimeProvinceManageScreen),
    template: templateMarkup
};