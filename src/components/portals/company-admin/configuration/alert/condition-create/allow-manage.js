﻿import ko from "knockout";
import templateMarkup from "text!./allow-manage.html";
import * as Screen from "../../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../../screenbase";
import ScreenHelper from "../../../../screenhelper";
import * as BladeSize from "../../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../../app/frameworks/configuration/webconfiguration";
// import WebrequestAlertConfiguration from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertConfiguration";
// import WebRequestAlertType from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertType";
import WebRequestEnumResource from "../../../../../../app/frameworks/data/apicore/webRequestEnumResource";
// import WebRequestCompany from "../../../../../../app/frameworks/data/apicore/webrequestCompany";
// import WebRequestCustomAreaType from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaType";
// import WebRequestFeature from "../../../../../../app/frameworks/data/apitrackingcore/webRequestFeature";

// import WebRequestCustomArea from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomArea";
import WebRequestCustomAreaCategory from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaCategory";
// import WebRequestCustomPOI from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOI";
import WebRequestCustomPOICategory from "../../../../../../app/frameworks/data/apitrackingcore/webrequestCustomPOICategory";
// import WebRequestAlertCategory from "../../../../../../app/frameworks/data/apitrackingcore/webRequestAlertCategories";
import DefaultSorting from "../../../../../../app/frameworks/constant/defaultSorting";
// import {AlertTypeFilter, EnumResourceFilter, CustomPOIFilter, CustomPOICategoryFilter, CustomAreaFilter, CustomAreaTypeFilter, CustomAreaTypeCategoryFilter, FeatureFilter} from "../infos";
import { EntityAssociation, Enums, Constants } from "../../../../../../app/frameworks/constant/apiConstant";


class AllowManageScreen extends ScreenBase {
    constructor(params) {
        super(params);


        ///////////////////Custom Ko Valiadate For//////////////////////////////
        ko.validation.rules['categoryTypeNotSameValueValidate'] = {
   
            validator: function (val, otherField) {
        
                let boolean = true;
                if (val && otherField != null) {
                    for (let i = 0; i < otherField.length; i++) {
                        if (val.value == otherField[i].categoryTypeValue) {
                            boolean = false
                            break;
                        } else {
                            boolean = true
                        }
                    }
             
                }
          
                return boolean;
        
                // return val != this.getValue(otherField); //ถ้าค่าที่ใส่มาไม่เท่ากับค่าที่มีอยู่แล้ว คืน true
            },
            message: this.i18n("M010")()
        };
        
        
        ko.validation.rules['categoryNameSameValueValidate'] = {
        
            validator: function (val, otherField) {
               
                let boolean = true;
                if (val && otherField != null) {
                    for (let i = 0; i < val.length; i++) {
        
                        for (let j = 0; j < otherField.length; j++) {
        
                            if(val[i] == otherField[j]){
                                boolean = false;
                                i=val.length;
                            }
                        }
                    }
                }
                return boolean;
        
                // return val != this.getValue(otherField); //ถ้าค่าที่ใส่มาไม่เท่ากับค่าที่มีอยู่แล้ว คืน true
            },
            message: this.i18n("M010")()
        };

        ko.validation.registerExtenders();
        ///////////////////////////////////////////////////////////////////////
        this.isFirst = false;
        this.allowLst = params.allowList;
        this.paramsData = params;
        this.bladeSize = BladeSize.Medium;
        this.mode = this.ensureNonObservable(params.mode);
        this.editTypeMode = this.ensureNonObservable(params.editTypeMode);
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.temperatureUnit = ko.observable('');
        this.selectedSubMenuItem = ko.observable(null);

        if(this.mode == Screen.SCREEN_MODE_CREATE){

            if(this.editTypeMode == "update"){
                this.bladeTitle(this.i18n("Unathorized_Update_Allow")());
            }else{
                this.bladeTitle(this.i18n("Unathorized_Create_Allow")());
            }

        }else if(this.mode == Screen.SCREEN_MODE_UPDATE){

                if(this.editTypeMode == "create"){
                    this.bladeTitle(this.i18n("Unathorized_Create_Allow")());
                }else{
                    this.bladeTitle(this.i18n("Unathorized_Update_Allow")());
                }
        }

        // switch (this.mode) {
        //     case Screen.SCREEN_MODE_CREATE:
        //        this.bladeTitle(this.i18n("Unathorized_Create_Allow")());
        //         break;
        //     case Screen.SCREEN_MODE_UPDATE:
                // this.bladeTitle(this.i18n("Unathorized_Update_Allow")());
                // break;
        // }

        this.enableType = ko.observable(true);
        this.categoryTypeOptions = ko.observableArray();
        this.conditionOptions = ko.observableArray();
        this.categoryNameOptions = ko.observableArray([]);

        this.selectedCategoryName = ko.observable();
        this.selectedCategoryType = ko.observable();
        this.selectedCondition = ko.observable();

        this.parkingTypeRadio = ko.observable('Limited');

        this.parkingNotOvertime = ko.observable();
        this.conditionType = ko.observable();

        this.selectedCondition.subscribe((conditionSelectedObj) => {
            
            
            var dfd = $.Deferred();
            let enumCategoryTypeFillter = {}
            switch (this.mode) {

                case Screen.SCREEN_MODE_CREATE:
                    if (conditionSelectedObj) {
                        this.conditionType(conditionSelectedObj.value)
                        if (conditionSelectedObj.value == 1) { //Custom Poi
                            enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.PoiCategoryType] }
        
                        } else if (conditionSelectedObj.value == 2) { //Custom Area
        
                            enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.AreaCategoryType] };
                        }
                    } else {
                        enumCategoryTypeFillter = { types: [-1] }
                    }
        
                    var d1 = this.webRequestEnumResource.listEnumResource(enumCategoryTypeFillter);
                    $.when(d1).done((r1) => {
        
                        this.categoryTypeOptions(r1.items)
                
        
                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
                break;
    
                case Screen.SCREEN_MODE_UPDATE:
                    // this.selectedCategoryType('')
                        if (conditionSelectedObj) {
                         
                            this.conditionType(conditionSelectedObj.value)
                            // this.selectedCategoryType('')
                            if (conditionSelectedObj.value == 1) { //Custom Poi
                                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.PoiCategoryType] }
            
                            } else if (conditionSelectedObj.value == 2) { //Custom Area
            
                                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.AreaCategoryType] };
                            }
                        } else {
                            enumCategoryTypeFillter = { types: [-1] }
                        }
 
                        var d2 = this.webRequestEnumResource.listEnumResource(enumCategoryTypeFillter);
                        $.when(d2).done((r2) => {

                            this.categoryTypeOptions(r2.items)
                            this.selectedCategoryType(ScreenHelper.findOptionByProperty(this.categoryTypeOptions, "value", this.paramsData.categoryTypeValue ) )
                            
                
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
                break;
            }
            
            return dfd;
        })
        this.parkingTypeRadio.subscribe((value)=>{
            this.parkingNotOvertime(null)

        })

        this.selectedCategoryType.subscribe((categoryTypeSelectedObj) => {
          
            
            if (this.isFirst) {
                
                this.selectedCategoryName([]);
                this.categoryNameOptions([]);
            }

                var dfd = $.Deferred();
                var allObj
                // var customPOICategoryFilter = (this.mode == Screen.SCREEN_MODE_UPDATE ) ? { "ids":params.categoryNameId} : {}
                // var customPOICategoryFilter = {}
                var d1;
                if(this.conditionType() == 1){
                    d1 = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
                }else if(this.conditionType() == 2){
                    d1 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary();
                }
                //  var d1 = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
                // var d2 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary()
                let categoryName;
                switch (this.mode) {
                    case Screen.SCREEN_MODE_CREATE:
                    
                    if (categoryTypeSelectedObj) {
                        
                        $.when(d1).done((r1) => {
                          
                            if(this.conditionType() == 1){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.poiCategoryType == categoryTypeSelectedObj.value
                                });
                            }else if(this.conditionType() == 2){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.areaCategoryType == categoryTypeSelectedObj.value
                                });
                            }

                            /////////////////////////////////////////////
                            if(categoryName.length != 0){
                                let allObj = {}
                                allObj = this.addAll()
                                    categoryName.forEach((items)=>{
                                        items.parent = 0
                                })
                                                
                                categoryName.unshift(allObj);
                                this.categoryNameOptions(categoryName)
                            }else{
                                this.categoryNameOptions([])
                            }
                            ///////////////////////////////////////////////
                            

                            //this.categoryNameOptions(categoryName)
                            this.isFirst = true
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
        
                        return dfd;
                    }
    
                    break;
                    case Screen.SCREEN_MODE_UPDATE:
                  
                        if (categoryTypeSelectedObj) {
                            
                            
                        $.when(d1).done((r1) => {
                            if(this.conditionType() == 1){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.poiCategoryType == categoryTypeSelectedObj.value
                                });
                            }else if(this.conditionType() == 2){
                                categoryName = r1.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                    return items.areaCategoryType == categoryTypeSelectedObj.value
                                });
                            }

            
                            ///////////////////////////////////////////////////////////////////
                            if(categoryName.length != 0){
                            
                                let allObj = {}
                                allObj = this.addAll()
                                    categoryName.forEach((items)=>{
                                        items.parent = 0
                                })
                                                
                                categoryName.unshift(allObj);
                                this.categoryNameOptions(categoryName)
                               
                                
                            }else{
                             
                                this.categoryNameOptions([])
                            }
                            //////////////////////////////////////////////////////////////////

                            this.isFirst = true
                            dfd.resolve();
                        }).fail((e) => {
                            dfd.reject(e);
                        });
        
                        return dfd;
                    }
                    break;
                }
        })

        this.visibleParkingNotOvertime = ko.pureComputed(() => {
            return this.parkingTypeRadio() === "Limited"
        });

        this.maxId = this.getMaxId(params.allowList);
        this.categoryIdList = this.combineCategoryId(params.allowList);
        this.enumCategoryType = this.getEnumCategoryTypeForUpdate();

        
    }
    addAll(){
        let allObj = {}; //add all
                            allObj.id = 0;
                            allObj.name = this.i18n("Common_CheckAll")();
                            allObj.parent = "#";
        return allObj
    }
    getEnumCategoryTypeForUpdate(){
        let enumCategoryTypeFillter = {}
        if (this.paramsData.conditionValue) {
            if (this.paramsData.conditionValue == 1) { //Custom Poi
                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.PoiCategoryType] }

            } else if (this.paramsData.conditionValue == 2) { //Custom Area

                enumCategoryTypeFillter = { types: [Enums.ModelData.EnumResourceType.AreaCategoryType] };
            }
        } else {
            enumCategoryTypeFillter = { types: [-1] }
        }

        return enumCategoryTypeFillter;
    }
    getMaxId(allowList) {
        let maxId;
        if (allowList) {
            if (allowList.length != 0) {
                maxId = Math.max.apply(Math, allowList.map(function (item) { return item.dataId; })) //หา maxId จาก object array โดย https://stackoverflow.com/questions/4020796/finding-the-max-value-of-an-attribute-in-an-array-of-objects
            } else {
                maxId = 0;
            }
        }
      
        return maxId;
    }
    combineCategoryId(allowList){
        
        let categoryIdArr = []
        if (allowList) { 
            if(allowList.length!=0 && allowList!=undefined){ 
             allowList.forEach((items,index)=>{
               
                    items.categoryNameId.forEach((id)=>{
                        categoryIdArr.push(id)
                    })
                })
    
            }
         }
        let categoryIdLst = categoryIdArr.filter((items)=>{
            return items != 0;
        })

        return categoryIdLst;
    }

    /**
     * Get WebRequest specific for Alert Configuration module in Web API access.
     * @readonly
     */
    get webrequestAlertConfiguration() {
        return WebrequestAlertConfiguration.getInstance();
    }
    /**
     * Get WebRequest specific for Alert Type module in Web API access.
     * @readonly
     */
    get webRequestAlertType() {
        return WebRequestAlertType.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for Company module in Web API access.
     * @readonly
     */
    get webRequestCompany() {
        return WebRequestCompany.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Type module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaType() {
        return WebRequestCustomAreaType.getInstance();
    }
    /**
     * Get WebRequest specific for Feature module in Web API access.
     * @readonly
     */
    get webRequestFeature() {
        return WebRequestFeature.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area module in Web API access.
     * @readonly
     */
    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Category module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaCategory() {
        return WebRequestCustomAreaCategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI module in Web API access.
     * @readonly
     */
    get webRequestCustomPOI() {
        return WebRequestCustomPOI.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI Category module in Web API access.
     * @readonly
     */
    get webRequestCustomPOICategory() {
        return WebRequestCustomPOICategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom POI Category module in Web API access.
     * @readonly
     */
    get webRequestAlertCategory() {
        return WebRequestAlertCategory.getInstance();
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();
        var enumConditionFilter = {
            types: [Enums.ModelData.EnumResourceType.AlertUnauthorizedCondition],
            // values: this.getReportTemplateType(),
            sortingColumns: DefaultSorting.EnumResource
        };
        var enumCategoryTypeFilter = this.mode==Screen.SCREEN_MODE_UPDATE ? this.enumCategoryType : {}
        
        var d1 = this.webRequestEnumResource.listEnumResource(enumConditionFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumCategoryTypeFilter);
        switch (this.mode) {

            case Screen.SCREEN_MODE_CREATE:

                    // var d2 = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
                    $.when(d1,d2).done((r1,r2) => {

                        this.conditionOptions(r1.items)

                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
            break;

            case Screen.SCREEN_MODE_UPDATE:
                    
                    var d3 ;
                    if(this.paramsData.condition == 1){
                        d3 = this.webRequestCustomPOICategory.listCustomPOICategorySummary();
                    }else if(this.paramsData.condition == 2){
                        d3 = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary();
                    }

                    
                    $.when(d1,d2,d3).done((r1,r2,r3) => {
                   
                        this.conditionOptions(r1.items)
                        this.selectedCondition(ScreenHelper.findOptionByProperty(this.conditionOptions, "value", this.paramsData.conditionValue ) )
                        this.parkingTypeRadio(this.paramsData.parkingType)
                        this.parkingNotOvertime(this.paramsData.duration)
                  

                    //////////////////////////////////////////Category Name Dropdown Update//////////////////////////////////////////////////////////////////////
                        // let categoryName = r3.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                        //     return items.poiCategoryType == this.paramsData.categoryTypeValue
                        // });
                        let categoryName;
                        if(this.paramsData.condition == 1){
                            categoryName = r3.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                return items.poiCategoryType == this.paramsData.categoryTypeValue
                            });
                        }else if(this.paramsData.condition == 2){
                            categoryName = r3.items.filter((items) => { //filter เอาเฉพาะ categoryname ของ category type ที่ได้เลือก
                                return items.areaCategoryType == this.paramsData.categoryTypeValue
                            });
                        }

                        var allObj = {};
                        allObj.id = 0;
                        allObj.name = this.i18n("Common_CheckAll")();
                        allObj.parent = "#";

                        categoryName.forEach((items) => {
                            items.parent = 0
                        })
                        categoryName.unshift(allObj);
                
                        this.categoryNameOptions(categoryName)//this.categoryNameOptions(categoryName)

                        // this.paramsData.categoryNameId.push(0)
              
                        this.selectedCategoryName(this.paramsData.categoryNameId)
                        
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                
                        dfd.resolve();
                    }).fail((e) => {
                        dfd.reject(e);
                    });
            break;
        }

        return dfd;
        
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        var self = this;

        this.parkingNotOvertime.extend({
            required: {           
                onlyIf:  () => { 
                    return this.parkingTypeRadio() === "Limited"; //require เมื่อ
                }
            }
        });

        this.selectedCondition.extend({
            required: true
        });

        this.selectedCategoryType.extend({
            required: true,
            categoryTypeNotSameValueValidate: (this.allowLst!=undefined && this.allowLst.length != 0) ? this.allowLst : null
            
        })
        this.selectedCategoryName.extend({
            required: true,
            categoryNameSameValueValidate:(this.categoryIdList != 0) ? this.categoryIdList : null
        })
        this.parkingTypeRadio.extend({
            required: true
        })

        this.validationModel = ko.validatedObservable({
            selectedCondition: this.selectedCondition,
            selectedCategoryType: this.selectedCategoryType,
            selectedCategoryName: this.selectedCategoryName,
            parkingTypeRadio: this.parkingTypeRadio,
            parkingNotOvertime: this.parkingNotOvertime
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate Alert Configuration model from View Model
     * 
     * @returns Alert Configuration model which is ready for ajax request 
     */
generateModel() {
    //     var model = {
    //         id: this.alertConfigurationId,
    //         companyId: this.companyId,
    //         alertTypeId: this.selectedAlertType().id,
    //         alertCategoryId: (this.selectedAlertCategory()) ? this.selectedAlertCategory().id : null,
    //         priority: this.priority(),
    //         enable: this.selectedEnable().value,
    //         mailTo: this.mailTo(),
    //         enableWebsiteAlert: this.enableWebsiteAlert(),
    //         enableEmailAlert: this.enableEmailAlert(),
    //         enableSmsAlert: this.enableSmsAlert(),
    //         enableInterfaceAlert: this.enableInterfaceAlert(),
    //         enableDeviceAlert: this.enableDeviceAlert(),
    //         description: this.description(),
    //         featureId: this.selectedAlertType().featureId,
    //         enableAutoTicket: this.enableCreateTicket(),
    //         enableMDVR: this.enableMDVR(),
    //         enableReport: this.enableReportAlert(),
    //         maneuverLevel: (this.selectedML()) ? this.selectedML().value : null
    //     };
    
    }

    getCategoryNameList(){
        let categoryNameLst = []
        let selectCategoryNameNotAll =  this.selectedCategoryName().filter((items)=>{

            return items != 0;
        })

        this.categoryNameOptions().forEach((items)=>{

            selectCategoryNameNotAll.forEach((id)=>{

                if(items.id == id){
                    categoryNameLst.push(items.name)
                }

            })
            
        })
        return categoryNameLst;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        var isValid = false;

        if (sender.id === "actSave") {

            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
    
            let categoryNameLst = this.getCategoryNameList();
         
            let allowParam = {

                dataId: (this.mode == Screen.SCREEN_MODE_CREATE) ? this.maxId + 1 : this.paramsData.allowId,
                configType:1,
                condition: this.selectedCondition().value,
                conditionForDisplay: this.selectedCondition().displayName,
                conditionValue: this.selectedCondition().value,
                categoryTypeName: this.selectedCategoryType().displayName,
                categoryTypeValue: this.selectedCategoryType().value,
                categoryNameId: this.selectedCategoryName(),
                customIds: this.selectedCategoryName(),
                categoryNameLst: categoryNameLst,
                parkingType: this.parkingTypeRadio(),
                duration: (this.parkingTypeRadio() === 'Limited' ) ? this.parkingNotOvertime() : 0,
                parkingDurationForDisplay: (this.parkingTypeRadio() === 'Limited' ) ? this.i18n("Unauthorized_Stop_Limited")() +" "+ this.parkingNotOvertime()+" "+this.i18n("Unathorized_Stop_Minutes")() : this.i18n("Unauthorized_Stop_Unlimited")()

            }                                                                                                       
            
            
           
            switch (this.mode) {
                
                case Screen.SCREEN_MODE_CREATE:

                let allowTableData = this.allowLst
 
                allowTableData.push(allowParam)
    
    
                this.publishMessage("allow-manage", allowTableData);
                this.close(true);
            
                break;
    
                case Screen.SCREEN_MODE_UPDATE:
                let allTableList =this.paramsData.allTableList

                        let allowUpdate = allTableList.map((item)=>{

                            if(item.dataId == allowParam.dataId){
                                item = allowParam
                            }
                            return item
                        
                        })
                       
                        this.publishMessage("allow-manage", allowUpdate);
                        this.close(true);

                break;
            }

        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        // this.selectedSubMenuItem(null);
    }

 

    

}



export default {
    viewModel: ScreenBase.createFactory(AllowManageScreen),
    template: templateMarkup
};