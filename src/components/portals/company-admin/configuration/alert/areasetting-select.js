﻿import ko from "knockout";
import templateMarkup from "text!./areasetting-select.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestCustomAreaType from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaType";
import WebRequestCustomArea from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomArea";
import WebRequestCustomAreaCategory from "../../../../../app/frameworks/data/apitrackingcore/webrequestCustomAreaCategory";
import { CustomAreaFilter, CustomAreaTypeFilter, CustomAreaTypeCategoryFilter } from "./infos";
class AreaSettingSelect extends ScreenBase {
    constructor(params) {
        super(params);
        this.bladeTitle(this.i18n("Configurations_AreaSetting")());
        this.items = this.ensureObservable(params,[]);
        this.customAreas = ko.observableArray([]);
        this.customAreaTypes = ko.observableArray([]);
        this.customAreaCategories = ko.observableArray([]);
        this.selectedCustomArea = ko.observable();
        this.selectedCustomAreaType = ko.observable();
        this.selectedCustomAreaCategory = ko.observable();
        this._selectedCustomAreaRef = this.selectedCustomArea.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomAreaType.poke(null);
                this.selectedCustomAreaCategory.poke(null);
            } else {
                this.selectedCustomArea.poke(null);
            }
        });
        this._selectedCustomAreaTypeRef = this.selectedCustomAreaType.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomArea.poke(null);
                this.selectedCustomAreaCategory.poke(null);
            } else {
                this.selectedCustomAreaType.poke(null);
            }
        });
        this._selectedCustomAreaCategoryRef = this.selectedCustomAreaCategory.ignorePokeSubscribe((value)=>{
            if(value){
                this.selectedCustomArea.poke(null);
                this.selectedCustomAreaType.poke(null);
            } else {
                this.selectedCustomAreaCategory.poke(null);
            }
        });
    }
    /**
     * Get WebRequest specific for Custom Area module in Web API access.
     * @readonly
     */
    get webRequestCustomArea() {
        return WebRequestCustomArea.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Category module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaCategory() {
        return WebRequestCustomAreaCategory.getInstance();
    }
    /**
     * Get WebRequest specific for Custom Area Type module in Web API access.
     * @readonly
     */
    get webRequestCustomAreaType() {
        return WebRequestCustomAreaType.getInstance();
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();

        var listCustomAreaSummary = this.webRequestCustomArea.listCustomAreaSummary(new CustomAreaFilter());
        var listCustomAreaType = this.webRequestCustomAreaType.listCustomAreaType(new CustomAreaTypeFilter());
        var listCustomAreaCategorySummary = this.webRequestCustomAreaCategory.listCustomAreaCategorySummary(new CustomAreaTypeCategoryFilter());

        $.when(listCustomAreaSummary,listCustomAreaType,listCustomAreaCategorySummary).done((r1,r2,r3) =>{
            this.customAreas(r1.items);
            this.customAreaTypes(r2.items);
            this.customAreaCategories(r3.items);
            if(this.items().insideArea)
            {
                this.selectedCustomArea(ScreenHelper.findOptionByProperty(this.customAreas, "id",this.items().insideArea));
            }
            else if(this.items().insideAreaType)
            {
                this.selectedCustomAreaType(ScreenHelper.findOptionByProperty(this.customAreaTypes, "id",this.items().insideAreaType));
            }
            else if(this.items().insideAreaCate)
            {
                this.selectedCustomAreaCategory(ScreenHelper.findOptionByProperty(this.customAreaCategories, "id",this.items().insideAreaCate));   
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
    * Setup Knockout Extends such as validation and trackChange
    * @lifecycle Called after onLoad
    */
    setupExtend() {

        var self = this;
        let checkValid = function(){
            return false;//_.isNil(self.selectedCustomArea()) && _.isNil(self.selectedCustomAreaType()) && _.isNil(self.selectedCustomAreaCategory());
        }

        self.selectedCustomArea.extend({
            trackChange: true
        });
        self.selectedCustomAreaType.extend({
            trackChange: true
        });
        self.selectedCustomAreaCategory.extend({
            trackChange: true
        });

        self.selectedCustomArea.extend({
            required: {           
                onlyIf: function () { 
                    return checkValid() ;
                }
            }
        });
        self.selectedCustomAreaType.extend({
            required: {           
                onlyIf: function () { 
                    return checkValid(); 
                }
            }
        });
        self.selectedCustomAreaCategory.extend({
            required: {           
                onlyIf: function () { 
                    return checkValid(); 
                }
            }
        });

        this.validationModel = ko.validatedObservable({
            selectedCustomArea: self.selectedCustomArea,
            selectedCustomAreaType: self.selectedCustomAreaType,
            selectedCustomAreaCategory: self.selectedCustomAreaCategory
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id == "actSave"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            var keyCustom = {};
            if(this.selectedCustomArea()){
                keyCustom.customArea = this.selectedCustomArea().id;
            }
            if(this.selectedCustomAreaType()){
                keyCustom.customAreaType = this.selectedCustomAreaType().id;
            }
            if(this.selectedCustomAreaCategory()){
                keyCustom.customAreaCate = this.selectedCustomAreaCategory().id;
            }
            
            console.log(this.selectedCustomArea());
            console.log(this.selectedCustomAreaType());
            console.log(this.selectedCustomAreaCategory());
            this.publishMessage("ca-configuration-alert-assetsetting-selected",keyCustom);
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    
}

export default {
viewModel: ScreenBase.createFactory(AreaSettingSelect),
    template: templateMarkup
};