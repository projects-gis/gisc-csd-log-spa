import ko from "knockout";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import {Enums,EntityAssociation} from "../../../../../app/frameworks/constant/apiConstant";

class AlertConfigurationFilter {
    constructor (alertTypeId = null, enable = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.AlertConfiguration;

        this.enable = enable;
        if(alertTypeId){
            this.alertTypeIds = [alertTypeId];
        }else{
            this.alertTypeIds = [];
        }
    }
}
export { AlertConfigurationFilter };

class AlertTypeFilter {
    constructor (includeId = null) {
        this.excludeBackgroundAlertTypes = true;
        this.types = [Enums.ModelData.EnumResourceType.AlertType];

        if(includeId){
            this.includeIds = [includeId];
        }
    }
}
export { AlertTypeFilter };

class EnumResourceFilter {
    constructor (type = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.EnumResource;

        if(type){
            this.types = [type];
        }else{
            this.types = [];
        }
    }
}
export { EnumResourceFilter };

class CustomPOIFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.CustomPOI;
    }
}
export { CustomPOIFilter };

class CustomPOICategoryFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.CustomPOICategory;
    }
}
export { CustomPOICategoryFilter };

class CustomAreaFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.CustomArea;
    }
}
export { CustomAreaFilter };

class CustomAreaTypeFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.CustomAreaType;
    }
}
export { CustomAreaTypeFilter };

class CustomAreaTypeCategoryFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.CustomAreaTypeCategory;
    }
}
export { CustomAreaTypeCategoryFilter };

class FeatureFilter {
    constructor (unit = null) {
        this.isCustomAlert = true;
        if(unit){
            this.unit = unit;
            this.isCustomAlert = false;
        }
        this.sortingColumns = DefaultSoring.BoxFeature;
    }
}
export { FeatureFilter };

class VehicleFilter {
    constructor (featureId = null) {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.Vehicle;
        this.enable = true;
        this.isAssociatedWithFleetService = true;
        if(featureId){
            this.featureIds = [featureId];
        }
    }
}
export { VehicleFilter };

class UserFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.sortingColumns = DefaultSoring.User;
        this.enable = true;
        this.isAssociatedWithMobileService = true;
        this.userType = Enums.UserType.Company;
    }
}
export { UserFilter };