﻿import ko from "knockout";
import templateMarkup from "text!./datetimesetting-select.html";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
class AreaSettingSelect extends ScreenBase {
    constructor(params) {
        super(params);
        console.log(params);
        this.bladeTitle(this.i18n("Configurations_DateTimeSetting")());
        this.items = this.ensureObservable(params,[]);
        this.enableOnAllDay = ko.observable();
        this.enableOnMonday = ko.observable();
        this.enableOnTuesday = ko.observable();
        this.enableOnWednesday = ko.observable();
        this.enableOnThursday = ko.observable();
        this.enableOnFriday = ko.observable();
        this.enableOnSaturday = ko.observable();
        this.enableOnSunday = ko.observable();
        this.startTime = ko.observable();
        this.endTime = ko.observable();

        this._enableOnAllDay = this.enableOnAllDay.ignorePokeSubscribe((value)=>{
            this.enableOnMonday(value);
            this.enableOnTuesday(value);
            this.enableOnWednesday(value);
            this.enableOnThursday(value);
            this.enableOnFriday(value);
            this.enableOnSaturday(value);
            this.enableOnSunday(value);
        });

        this.enableOnMonday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);

        this.enableOnTuesday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);

        this.enableOnWednesday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);

        this.enableOnThursday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);

        this.enableOnFriday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);

        this.enableOnSaturday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);

        this.enableOnSunday.subscribe(function(newValue) {
            if(this.enableOnMonday() &&
                this.enableOnTuesday() &&
                this.enableOnWednesday() &&
                this.enableOnThursday() &&
                this.enableOnFriday() &&
                this.enableOnSaturday() &&
                this.enableOnSunday()) {
                this.enableOnAllDay(newValue);
            } else if (!newValue){
                this.enableOnAllDay.poke(newValue);
            }
        },this);
    }

    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;
        var dfd = $.Deferred();
        this.enableOnMonday(this.items().enableOnMonday);
        this.enableOnTuesday(this.items().enableOnTuesday);
        this.enableOnWednesday(this.items().enableOnWednesday);
        this.enableOnThursday(this.items().enableOnThursday);
        this.enableOnFriday(this.items().enableOnFriday);
        this.enableOnSaturday(this.items().enableOnSaturday);
        this.enableOnSunday(this.items().enableOnSunday);
        this.startTime(this.items().startTime);
        this.endTime(this.items().endTime);
        console.log('Start time ' + this.items().startTime);
        dfd.resolve();
        return dfd;
    }
    /**
    * Setup Knockout Extends such as validation and trackChange
    * @lifecycle Called after onLoad
    */
    setupExtend() {

        var self = this;
        let checkValid = function(){
            return true;
        }

        self.enableOnMonday.extend({
            trackChange: true
        });
        self.enableOnTuesday.extend({
            trackChange: true
        });
        self.enableOnWednesday.extend({
            trackChange: true
        });
        self.enableOnThursday.extend({
            trackChange: true
        });
        self.enableOnFriday.extend({
            trackChange: true
        });
        self.enableOnSaturday.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if (!(self.enableOnMonday() ||
                            self.enableOnTuesday() || 
                            self.enableOnWednesday() ||
                            self.enableOnThursday() ||
                            self.enableOnFriday() ||
                            self.enableOnSaturday() ||
                            self.enableOnSunday()) 
                            && !ko.validation.utils.isEmptyVal(self.startTime()) 
                            && !ko.validation.utils.isEmptyVal(self.endTime())) {
                        isValid = false;
                    }
                    return isValid;
                },
                message: this.i18n("Configuration_Day_Warning")()
            },
            trackChange: true
        });
        self.enableOnSunday.extend({
            trackChange: true
        });
        self.startTime.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if ((self.enableOnMonday() ||
                            self.enableOnTuesday() || 
                            self.enableOnWednesday() ||
                            self.enableOnThursday() ||
                            self.enableOnFriday() ||
                            self.enableOnSaturday() ||
                            self.enableOnSunday() || 
                            !ko.validation.utils.isEmptyVal(self.endTime())) 
                        && ko.validation.utils.isEmptyVal(val)) {
                        isValid = false;
                    }
                    return isValid;
                },
                message: this.i18n("Configuration_StartTime_Warning")()
            },
            trackChange: true
        });
        self.endTime.extend({
            validation: {
                validator: function (val, validate) {
                    if (!validate) { return true; }
                    var isValid = true;
                    if ((self.enableOnMonday() ||
                            self.enableOnTuesday() || 
                            self.enableOnWednesday() ||
                            self.enableOnThursday() ||
                            self.enableOnFriday() ||
                            self.enableOnSaturday() ||
                            self.enableOnSunday() || 
                            !ko.validation.utils.isEmptyVal(self.startTime())) 
                        && ko.validation.utils.isEmptyVal(val)) {
                        isValid = false;
                    }
                    return isValid;
                },
                message: this.i18n("Configuration_EndTime_Warning")()
            },
            trackChange: true
        });

        this.validationModel = ko.validatedObservable({
            enableOnMonday: self.enableOnMonday,
            enableOnTuesday: self.enableOnTuesday,
            enableOnWednesday: self.enableOnWednesday,
            enableOnThursday: self.enableOnThursday,
            enableOnFriday: self.enableOnFriday,
            enableOnSaturday: self.enableOnSaturday,
            enableOnSunday: self.enableOnSunday,
            startTime: self.startTime,
            endTime: self.endTime
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createAction("actCancel", this.i18n("Common_Cancel")(), "cancel"));
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if(sender.id == "actSave"){
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }
            var keyCustom = {};
            if(this.enableOnMonday()){
                keyCustom.enableOnMonday = true;
            }
            if(this.enableOnTuesday()){
                keyCustom.enableOnTuesday = true;
            }
            if(this.enableOnWednesday()){
                keyCustom.enableOnWednesday = true;
            }
            if(this.enableOnThursday()){
                keyCustom.enableOnThursday = true;
            }
            if(this.enableOnFriday()){
                keyCustom.enableOnFriday = true;
            }
            if(this.enableOnSaturday()){
                keyCustom.enableOnSaturday = true;
            }
            if(this.enableOnSunday()){
                keyCustom.enableOnSunday = true;
            }

            keyCustom.startTime = this.startTime();
            keyCustom.endTime = this.endTime();

            this.publishMessage("ca-configuration-alert-datetimesetting-selected",keyCustom);
            this.close(true);
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }

    
}

export default {
viewModel: ScreenBase.createFactory(AreaSettingSelect),
    template: templateMarkup
};