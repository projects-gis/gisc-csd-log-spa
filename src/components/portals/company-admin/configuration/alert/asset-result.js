﻿import ko from "knockout";
import templateMarkup from "text!./asset-result.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webConfiguration";
import WebRequestFleetMonitoring from "../../../../../app/frameworks/data/apitrackingcore/webRequestFleetMonitoring";
import Utility from "../../../../../app/frameworks/core/utility";
import { Enums } from "../../../../../app/frameworks/constant/apiConstant";
import checkboxCheckedTemplateMarkup from "text!../../../../svgs/checkbox-checked.html";

class AlertConfigurationFindAssetResultScreen extends ScreenBase {
    constructor(params) {
        super(params);
        this.id = "AlertConfigurationFindAssetResult";
        this.bladeTitle(this.i18n("FleetMonitoring_FindAssetResult")());
        this.bladeSize = BladeSize.Medium;
        this.filter = this.ensureNonObservable(params);
        this.results = ko.observableArray([]);
        this.selectedVehicleIds = ko.observableArray([]);
        this.selectedDeliveryMenIds = ko.observableArray([]);
        this.listAssetsByBU = new Array();
        this.dtIds = [];
        this.enableApplyAction = ko.observable(true);

        this.alertType = ko.observable(this.filter.alertType);
        this.featureId = ko.observable(this.filter.featureId);

        // this.filter.businessUnitIds = [25, 22, 53, 28, 44, 38, 82, 11, 83, 76, 71, 79, 31, 33, 27, 62, 107, 112, 78, 56, 88, 26, 100, 89, 108, 63, 16, 58, 66, 110, 43, 77, 15, 64, 65, 80, 30, 5, 84, 75, 86, 52, 67, 49, 20, 116, 54];
        // this.filter.selectedVehicleIds = [76,77,78,79,80,82,83,84,85,87,91,93,95,100,101,102,107,109,110,116,121,124,125,126,128,129,130,131,133,135,138,139,140,141,142,143,144,148,149,150,151,152,153,154,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,203,204,205,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,239,240,241,242,243,244,245,249,250,252,253,254,255,256,257,258,259,261,262,265,266,269,272,273,274,277,279,282,288,289,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,311,312,313,314,315,316,317,318,319,320,321,323,324,325,326,328,329,330,331,332,333,334,389,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,466,467,468,469,471,472,473,474,475,477,478,479,481,482,483,486,492,493,496,498,499,500,502,503,504,505,506,508,509,510,511,512,513,514,515,516,517,518,519,521,522,523,524,525,526,527,530,532,533,534,536,537,538,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,559,560,561,564,566,571,587,588,589,590,591,592,593,594,595,596,597,598,599,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,630,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,650,651,653,654,655,656,657,658,659,660,661,662,663,664,665,666,667,668,669,674,675,678,679,680,681,682,683,684,685,686,687,688,689,690,691,692,693,694,695,696,697,698,699,700,702,704,705,706,707,708,709,710,711,712,713,714,715,716,717,718,719,720,721,722,723,724,725,726,727,728,729,730,731,732,733,734,735,736,737,738,739,740,741,742,743,745,746,748,750,751,752,753,754,755,756,757,759,760,761,762,763,764,765,766,767,768,769,770,771,772,773,774,778,779,780,781,782,783,785,788,789,790,791,792,794,795,796,797,798,799,800,801,802,803,804,805,806,807,808,809,810,811,10680,10682,10684,10685,10686,10688,10689,10690,10691,10692,10699,10700,10701,10702,10704,10706,10708,10709,10712,10714,10715,10717,10718,10719,10720,10721,10722,10723,10724,10728,10729,10730,10731,10732,10733,10734,10736,10737,10738,10739,10740,10749,10751,10754,10755,10756,10757,10758,10759,10760,10761,10762,10775,10777,10786,10788,10790,10795,10798,10803,10812,10813,10814,10816,10821,10822,10825,10830,10835,10842,10843,10855,10857,10860,10863,10867,10882,10883,10884,10885,10886,10887,10888,10889,10890,10895,10898,10902,10908,10911,10912,10913,10916,10917,10918,10919,10920,10922,10923,10924,10925,10926,10927,10928,10930,10931,10932,10933,10934,10935,10936,10937,10938,10940,10941,10942,10943,10944,10945,10946,10947,10948,10949,10950,10951,10952,10953,10954,10955,10956,10957,10958,10959,10960,10961,10962,10963,10964,10965,10966,10967,10968,10969,10970,10971,10973,10974,10975,10976,10978,10981,10982,10985,10986,10987,10988,10989,10990,10991,10992,10994,10995,10997,10998,10999,11000,11001,11002,11003,11004,11005,11006,11007,11008,11009,11010,11011,11012,11013,11014,11015,11016,11017,11018,11019,11024,11025,11026,11027,11028,11029,11030,11031,11032,11033,11034,11035,11036,11037,11038,11039,11040,11041,11042,11044,11045,11046,11048,11049,11050,11051,11052,11053,11054,11055,11057,11058,11059,11060,11061,11062,11063,11064,11065,11066,11067,11068,11069,11070,11071,11072,11073,11074,11075,11076,11077,11078,11079,11080,11082,11083,11084,11085,11086,11087,11088,11089,11090,11091,11092,11093,11094,11095,11096,11097,11098,11099,11100,11101,11102,11103,11104,11105,11106,11107,11108,11109,11110,11111,11113,11114,11115,11116,11117,11118,11119,11120,11121,11122,11123,11124,11125,11126,11127,11128,11129,11130,11131,11132,11133,11134,11135,11136,11137,11138,11139,11140,11141,11144,11146,11147,11148,11150,11151,11152,11153,11154,11155,11156,11159,11160,11172,11173,11174,11175,11176,11177,11179,11184,11188,11192,11193,11194,11195,11197,11198,11199,11200,11220,11224,11225,11226,11247,11248,11249,11250,11251,11252,11253,11258,11260,11261,11262,11263,11264,11266,11271,11273,11274,11275,11277,11282,11283,11284,11285,11289,11290,11291,11292,11297,11301,11303,11309,11310,11312,11313,11314,11315,11320,11321,11323,11325,11326,11329,11330,11331,11333,11334,11337,11344,11345,11346,11347,11348,11349,11350,11353,11354,11356,11357,11358,11359,11360,11361,11362,11363,11364,11365,11366,11367,11368,11370,11371,11372,11374,11375,11376,11377,11378,11379,11381,11382,11383,11384,11385,11386,11387,11388,11389,11390,11391,11392,11393,11394,11395,11398,11403,11405,11412,11418,11423,11426,11427,11432,11436,11438,11439,11441,11443,11444,11445,11446,11447,11448,11449,11450,11451,11452,11453,11454,11455,11456,11457,11458,11459,11460,11461,11462,11463,11464,11467,11468,11469,11471,11472,11473,11475,11476,11477,11478,11479,11480,11481,11482,11483,11484,11485,11486,11487,11488,11489,11490,11491,11492,11493,11495,11496,11497,11501,11503,11504,11505,11506,11507,11508,11509,11510,11511,11513,11514,11515,11516,11517,11518,11519,11520,11521,11522,11523,11524,11525,11526,11527,11549,11558,11559,11560,11561,11565,11567,21528,21529,21530,21535,21536,21537,21539,21540,21541,21542,21543,21544,21545,21546,21547,21548,21549,21550,21551,21552,21553,21554,21555,21556,21557,21558,21559,21560,21561,21562,21563,21565,21566,21567,21568,21569,21570,21571,21572,21573,21574,21575,21576,21577,21578,21579,21580,21581,21582,21583,21585,21586,21587,21588,21589,21591,21592,21593,21594,21595,21596,21597,21598,21600,21601,21602,21606,21607,21608,21609,21611,21612,21613,21614,21615,21616,21618,21619,21620,21621,21622,21623,21624,21625,21626,21627,21628,21629,21630,21631,21632,21634,21635,21636,21637,21638,21639,21640,21641,21642,21643,21644,21645,21646,21671,21672,21674,21681,21682,21684,21685,21686,21687,21688,21689,21690,21691,21692,21693,21694,21695,21696,21697,21698,21699,21700,21701,21704,21705,21706,21707,21708,21711,21715,21717,21720,21721,21722,21723,21724,21725,21726,21727,21728,21729,21730,21731,21732,21733,21734,21735,21736,21737,21739,21740,21741,21742,21743,21744,21746,21747,21748,21749,21750,21751,21752,21753,21754,21755,21756,21757,21758,21759,21760,21761,21762,21763,21764,21765,21766,21767,21768,21774,21775,21776,21777,21778,21779,21780,21781,21782,21783,21784,21785,21786,21787,21788,21789,21790,21792,21793,21794,21795,21796,21797,21798,21799,21800,21801,21802,21803,21804,21805,21806,21807,21808,21809,21812,21813,21814,21817,21818,21819,21820,21821,21822,21823,21824,21825,21826,21828,21829,21830,21831,21832,21833,21834,21835,21836,21837,21838,21841,21842,21843,21844,21845,21846,21847,21848,21850,21851,21852,21853,21854,21855,21856,21857,21858,21859,21860,21861,21862,21863,21864,21865,21866,21867,21868,21869,21870,21871,21872,21873,21874,21875,21876,21878,21879,21882,21883,21884,21885,21886,21889,21891,21892,21893,21894,21895,21896,21897,21898,21899,21997,22687,22858,22859,22902,23185,23217,23290,23291,23292,23323,23364,23365,23367,23378,23422,23535];
        // this.filter.selectedDeliveryMenIds = [196];

        this.selectedVehicleItems = ko.observableArray([]);
        this.selectedDeliveryManItems = ko.observableArray([]);
        this.searchText = ko.observable("");

        this._resultsSubscribe = this.results.subscribe(() => {
            this.enableApplyAction(this.results() && this.results().length > 0);
        });

        this.searchText.subscribe((data) => {
            var value = data.toLowerCase();
            $("#" + this.id + " .AlertConfigurationFindAssetFilter tr").filter(function () {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });
        });

        // this.selectedVehicleItems.subscribe(function(value) {
        //     console.log(this.selectedVehicleItems());
        // }.bind(this));

        // this.selectedDeliveryManItems.subscribe(function(value) {
        //     console.log(this.selectedDeliveryManItems());
        // }.bind(this));

        this.checkboxCheckedTemplateMarkup = ko.observable(checkboxCheckedTemplateMarkup);


        this.subscribeMessage("ca-alert-asset-businessunit", (data) => {
            this.isBusy(true);
            this.filter.businessUnitIds = data.businessUnitIds;
            let listVehicleSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.Vehicle);
            let listDeliveryManSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.DeliveryMan);
            $.when(listVehicleSummary, listDeliveryManSummary).done((rListVehicleSummary, rListDeliveryManSummary) => {
                this.initResult(rListVehicleSummary, rListDeliveryManSummary);
                this.initOnClickCheckbox();
                this.initCheckbox();
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
            });
        });

        this.subscribeMessage("ca-configuration-manage-alert-changed", (alertTypeValue) => {
            this.isBusy(false);
            this.results([]);
            this.alertType(alertTypeValue);
            this.featureId(null);
            if (this.forceCloseBlade()) {
                //force close this blade because alert type dose not valid
                this.close(true);
            } else if (alertTypeValue != null) {
                let listVehicleSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.Vehicle);
                let listDeliveryManSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.DeliveryMan);
                $.when(listVehicleSummary, listDeliveryManSummary).done((rListVehicleSummary, rListDeliveryManSummary) => {
                    this.initResult(rListVehicleSummary, rListDeliveryManSummary);
                    this.initOnClickCheckbox();
                    this.initCheckbox();
                    this.isBusy(false);
                }).fail((e) => {
                    this.isBusy(false);
                });
            }
        });

        this.subscribeMessage("ca-configuration-manage-custom-alert-featureId-changed", (featureId) => {
            this.isBusy(false);
            this.results([]);
            this.featureId(featureId);
            let listVehicleSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.Vehicle);
            let listDeliveryManSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.DeliveryMan);
            $.when(listVehicleSummary, listDeliveryManSummary).done((rListVehicleSummary, rListDeliveryManSummary) => {
                this.initResult(rListVehicleSummary, rListDeliveryManSummary);
                this.initOnClickCheckbox();
                this.initCheckbox();
                this.isBusy(false);
            }).fail((e) => {
                this.isBusy(false);
            });
        });

        this.forceCloseBlade = ko.pureComputed(() => {
            var result = false;
            var selectedAlertType = this.alertType();
            if (selectedAlertType) {
                if (selectedAlertType.value === Enums.ModelData.AlertType.CustomAlert) {
                    result = true;
                } else {
                    //Asset (Vehicle), Asset (Delivery Man), Asset (Vehicle, Delivery Man)   
                    if (_.includes(selectedAlertType.levels, Enums.ModelData.AlertLevel.Vehicle.toString())
                        || _.includes(selectedAlertType.levels, Enums.ModelData.AlertLevel.Dispatcher.toString())) {
                        result = false;
                    } else {
                        result = true;
                    }
                }
            }

            return result;
        });

    }

    /**
     * Get WebRequest specific for FleetMonitoring module in Web API access.
     * @readonly
     */
    get webRequestFleetMonitoring() {
        return WebRequestFleetMonitoring.getInstance();
    }

    /**
     * List Business Unit of data table.
     * @readonly
     */
    listBusinessUnit(entityType) {
        var dfd = $.Deferred();
        // Calling service for search asset by criteria.
        this.webRequestFleetMonitoring.listAsset({
            companyId: WebConfig.userSession.currentCompanyId,
            businessUnitIds: this.filter.businessUnitIds ? this.filter.businessUnitIds : [0],
            searchEntityType: entityType,
            featureIds: this.featureId() ? [this.featureId()] : null,
        }).done((response) => {
            // Map original response to UI render object, group by business UNIT.
            var results = _.map(response, (item, index) => {
                var result = new FindAssetsResultItem(item, entityType, index);
                this.dtIds.push(result.dtId);
                return result;
            });
            // Update search result.
            // this.results(results);
            dfd.resolve(results);
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    /**
     * Function onclick checkbox of table
     */
    onCheckboxClick(event) {
        if (event.target.type == 'checkbox') {
            // console.log("onCheckboxClick");

            let tbId = $(event.target).closest("table").attr("id");
            let tbType = $(event.target).closest("table").attr("data-type-table");
            let cbId = $(event.target).attr("id");
            let checked = event.target.checked;
            let selectedVehicleIds = this.selectedVehicleIds();
            let selectedDeliveryMenIds = this.selectedDeliveryMenIds();
            // if check is all then input checkBoxAll is equal check else input checkBoxAll is equal uncheck
            let isChecked = $("#" + tbId + " .checkbox").find("input[type='checkbox']:checked").length;
            let isCheckBox = $("#" + tbId + " .checkbox").find("input[type='checkbox']").length;

            if (isChecked === isCheckBox) {
                $("#" + tbId + " .checkboxAll").find("input[type='checkbox']").prop("checked", true);
            } else {
                $("#" + tbId + " .checkboxAll").find("input[type='checkbox']").prop("checked", false);
            }

            if (checked) {
                if (tbType == Enums.ModelData.SearchEntityType.Vehicle) {
                    selectedVehicleIds.push(cbId);
                } else if (tbType == Enums.ModelData.SearchEntityType.DeliveryMan) {
                    selectedDeliveryMenIds.push(cbId);
                }
                $(event.target).closest("tr").addClass("selected");
            } else {
                if (tbType == Enums.ModelData.SearchEntityType.Vehicle) {
                    selectedVehicleIds = _.pullAll(selectedVehicleIds, [cbId]);
                } else if (tbType == Enums.ModelData.SearchEntityType.DeliveryMan) {
                    selectedDeliveryMenIds = _.pullAll(selectedDeliveryMenIds, [cbId]);
                }
                $(event.target).closest("tr").removeClass("selected");
            }

            this.selectedVehicleIds(selectedVehicleIds);
            this.selectedDeliveryMenIds(selectedDeliveryMenIds);
        }
    }

    /**
     * Function onclick checkbox all of table
     */
    onCheckboxAllClick(event) {
        if (event.target.type == 'checkbox') {
            // console.log("onCheckboxAllClick");
            let tbId = $(event.target).closest("table").attr("id");
            var tbType = $(event.target).closest("table").attr("data-type-table");
            let checkAll = $(event.target).hasClass("all");
            let checked = event.target.checked;
            let selectedVehicleIds = this.selectedVehicleIds();
            let selectedDeliveryMenIds = this.selectedDeliveryMenIds();
            if (checkAll) {
                $("#" + tbId + " .checkbox").find("input[type='checkbox']").prop("checked", checked);
            }
            $('#' + tbId + ' .checkbox input[type="checkbox"]').each(function () { // Loop through all checkboxes that aren't "all"
                let cbId = $(this).attr("id");
                if ($(this).is(':checked')) {
                    if (tbType == Enums.ModelData.SearchEntityType.Vehicle) {
                        let fData = _.filter(selectedVehicleIds, function (x) { return x == cbId; });
                        if (fData.length === 0) {
                            selectedVehicleIds.push(cbId);
                        }
                    } else if (tbType == Enums.ModelData.SearchEntityType.DeliveryMan) {
                        let fData = _.filter(selectedDeliveryMenIds, function (x) { return x == cbId; });
                        if (fData.length === 0) {
                            selectedDeliveryMenIds.push(cbId);
                        }
                    }
                    $(this).closest('tr').addClass('selected');
                } else {
                    if (tbType == Enums.ModelData.SearchEntityType.Vehicle) {
                        selectedVehicleIds = _.pullAll(selectedVehicleIds, [cbId]);
                    } else if (tbType == Enums.ModelData.SearchEntityType.DeliveryMan) {
                        selectedDeliveryMenIds = _.pullAll(selectedDeliveryMenIds, [cbId]);
                    }
                    $(this).closest('tr').removeClass('selected');
                }
            });
            this.selectedVehicleIds(selectedVehicleIds);
            this.selectedDeliveryMenIds(selectedDeliveryMenIds);
        }
    }

    checkAll() {
        var stateCheckBox = true;
        let isChecked = $("#" + this.id + " .checkboxAll").find("input[type='checkbox']:checked").length;
        let isCheckBox = $("#" + this.id + " .checkboxAll").find("input[type='checkbox']").length;
        if (isChecked === isCheckBox) {
            stateCheckBox = false;
        }

        $("#" + this.id + " .checkboxAll input[type='checkbox']").each(function () {
            // $(this).prop("checked", stateCheckBox);
            let checked = $(this).is(':checked');
            if (stateCheckBox && !checked) {
                $(this).trigger("click");
            } else if (!stateCheckBox && checked) {
                $(this).trigger("click");
            }
        });
    }

    /**
     * Function set checkbox when init
     */
    initCheckbox() {
        var self = this;
        if (_.size(this.filter.selectedVehicleIds) > 0) {
            var selectedVehicleIds = self.selectedVehicleIds();
            $("#" + self.id + " table[data-type-table='" + Enums.ModelData.SearchEntityType.Vehicle + "']>tbody>tr.tbbody .checkbox input[type='checkbox']").each(function () {
                let cbId = $(this).attr("id");
                let fData = _.filter(self.filter.selectedVehicleIds, function (x) { return x == cbId; });
                if (fData.length) {
                    let checkEmptyData = _.filter(selectedVehicleIds, function (x) { return x == fData[0]; });
                    if (!checkEmptyData.length) {
                        selectedVehicleIds.push(cbId);
                    }
                    $(this).prop("checked", true);
                    $(this).closest('tr').addClass('selected');
                }
            });
            self.selectedVehicleIds(selectedVehicleIds);
        }

        if (_.size(this.filter.selectedDeliveryMenIds) > 0) {
            var selectedDeliveryMenIds = self.selectedDeliveryMenIds();
            $("#" + self.id + " table[data-type-table='" + Enums.ModelData.SearchEntityType.DeliveryMan + "']>tbody>tr.tbbody .checkbox input[type='checkbox']").each(function () {
                let cbId = $(this).attr("id");
                let fData = _.filter(self.filter.selectedDeliveryMenIds, function (x) { return x == cbId; });
                if (fData.length) {
                    let checkEmptyData = _.filter(selectedDeliveryMenIds, function (x) { return x == fData[0]; });
                    if (!checkEmptyData.length) {
                        selectedDeliveryMenIds.push(cbId);
                    }
                    $(this).prop("checked", true);
                    $(this).closest('tr').addClass('selected');
                }
            });
            self.selectedDeliveryMenIds(selectedDeliveryMenIds);
        }


        $("#" + this.id + " table").each(function () {
            // if check is all then input checkBoxAll is equal check else input checkBoxAll is equal uncheck
            let tbId = $(this).attr("id");
            let isChecked = $("#" + tbId + " .checkbox").find("input[type='checkbox']:checked").length;
            let isCheckBox = $("#" + tbId + " .checkbox").find("input[type='checkbox']").length;
            if (isChecked === isCheckBox) {
                $("#" + tbId + " .checkboxAll").find("input[type='checkbox']").prop("checked", true);
            } else {
                $("#" + tbId + " .checkboxAll").find("input[type='checkbox']").prop("checked", false);
            }
        });

    }

    /**
     * Function convert data to array
     */
    initResult(listVehicleSummary, listDeliveryManSummary) {
        var dataArray = listVehicleSummary.concat(listDeliveryManSummary);
        var list = new Array();
        var arr = new Array();
        if (_.size(dataArray)) {
            _.forEach(dataArray, (item) => {
                var data = Object();
                data.businessUnitId = item.businessUnitId;
                switch (item.entityType) {
                    case Enums.ModelData.SearchEntityType.Vehicle:
                        data.vehicle = item;
                        break
                    case Enums.ModelData.SearchEntityType.DeliveryMan:
                        data.deliveryMan = item;
                        break;
                }
                list.push(data);
            });
            var results = _.groupBy(list, 'businessUnitId');
        }

        //convert object to array
        _.forEach(results, (item) => {
            arr.push(item);
        });

        this.results(arr);
    }

    /**
     * When onDomReady not found table, init onclick again
     */
    initOnClickCheckbox() {

        $("#" + this.id + " table tr.tbheader")
            .off('click', this.onCheckboxAllClick.bind(this)) // remove handler
            .on('click', this.onCheckboxAllClick.bind(this)); // add handler

        $("#" + this.id + " table tr.tbbody")
            .off('click', this.onCheckboxClick.bind(this)) // remove handler
            .on('click', this.onCheckboxClick.bind(this)); // add handler

        // $("#" + this.id + " table tr.tbheader").click(this.onCheckboxAllClick.bind(this));
        // $("#" + this.id + " table tr.tbbody").click(this.onCheckboxClick.bind(this));
    }

    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if (!isFirstLoad) {
            return;
        }

        var dfd = $.Deferred();

        let listVehicleSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.Vehicle);
        let listDeliveryManSummary = this.listBusinessUnit(Enums.ModelData.SearchEntityType.DeliveryMan);

        $.when(listVehicleSummary, listDeliveryManSummary).done((rListVehicleSummary, rListDeliveryManSummary) => {
            this.initResult(rListVehicleSummary, rListDeliveryManSummary);
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    onDomReady() {
        this.initOnClickCheckbox();
        this.initCheckbox();
    }

    /**
     * Setup trackChange/validation
     */
    setupExtend() {
    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actApply", this.i18n("Common_Apply")(), "default", true, this.enableApplyAction));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdFindAsset", this.i18n("Common_FindAssets")(), "svg-cmd-search"));
        commands.push(this.createCommand("cmdCheckAll", this.i18n("Common_All")(), "svg-cmd-view-all"));
    }

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

        if (sender.id === "actApply") {
            var selectedVehicleIds = [];
            var selectedDeliveryMenIds = [];

            this.selectedVehicleIds().forEach(function (vehicleId) {
                selectedVehicleIds.push(vehicleId);
            }, this);
            this.selectedDeliveryMenIds().forEach(function (deliveryManId) {
                selectedDeliveryMenIds.push(deliveryManId);
            }, this);
            this.publishMessage("ca-configuration-alert-assets-selected", {
                selectedVehicleIds: selectedVehicleIds,
                selectedDeliveryMenIds: selectedDeliveryMenIds,
                selectedBusinessUnitIds: this.filter.businessUnitIds
            });
            this.close(true);
        }
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id == "cmdFindAsset") {
            this.navigate("ca-configuration-alert-manage-asset-businessunit", this.filter);
        } else if (sender.id == "cmdCheckAll") {
            this.checkAll();
        }
    }

}

class FindAssetsResultItem {
    // info = FindAssetsResultInfo, entityType = Enums.ModelData.SearchEntityType
    constructor(info, entityType, index) {
        this.businessUnitId = info.businessUnitId;
        this.businessUnitPath = info.businessUnitPath;
        let assets = info.assets;
        this.businessUnitPath += " (" + assets.length + ")";
        this.dtId = 'dtConfigAlertAssetList' + entityType + index;
        this.assets = assets;
        this.entityType = entityType;
    }
}

export default {
    viewModel: ScreenBase.createFactory(AlertConfigurationFindAssetResultScreen),
    template: templateMarkup
};