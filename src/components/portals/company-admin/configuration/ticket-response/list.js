﻿import ko from "knockout";
import templateMarkup from "text!./list.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestTickerResponse from "../../../../../app/frameworks/data/apitrackingcore/webRequestTickerResponse";

class ConfigurationTicketResponseListScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_TicketResponse")());
        this.bladeSize = BladeSize.Small;

        this.ticketResponseLst = ko.observableArray([]);
        this.filterText = ko.observable();
        this.selectedTicketResponse = ko.observable();
        this.recentChangedRowIds = ko.observableArray();

        this.order = ko.observable([]);


        this.selectRow = (row) => {
            this.navigate("ca-configuration-ticket-response-view", { data: row });
        }

        this.subscribeMessage("ca-configuration-ticket-response-refresh-list", info => {
            let id = info != undefined || info != null ? [info.id] : null
            this.getTicketResponseData(id);            
        });
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.getTicketResponseData();
    }

    getTicketResponseData(id) {
        this.isBusy(true);
        this.webRequestTickerResponse.ticketResponseList().done((res)=> {
            this.isBusy(false);
            this.ticketResponseLst.replaceAll(res.items);
            if(id) {
                this.recentChangedRowIds.replaceAll(id);
            }
        });
        
    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.Ticket_Response_Create)) {
            commands.push(this.createCommand("cmdCreate", this.i18n("Common_Create")(), "svg-cmd-add"));
        }
    }

    onCommandClick(sender) {
        if (sender.id == "cmdCreate") {
            this.navigate("ca-configuration-ticket-response-manage", { mode: Screen.SCREEN_MODE_CREATE });
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestTickerResponse() {
        return WebRequestTickerResponse.getInstance();
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationTicketResponseListScreen),
    template: templateMarkup
};