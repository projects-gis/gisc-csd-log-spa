﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as Screen from "../../../../../app/frameworks/constant/screen";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { Constants } from "../../../../../app/frameworks/constant/apiConstant";
import WebRequestTickerResponse from "../../../../../app/frameworks/data/apitrackingcore/webRequestTickerResponse";

class ConfigurationTicketResponseViewScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.data = ko.observable(params.data);
        this.bladeTitle(this.i18n("Configurations_TicketResponse_View")());
        this.bladeSize = BladeSize.Small;
        this.ticketResponseId = ko.observable(null);
        this.txtName = ko.observable(null);

        this.subscribeMessage("ca-configuration-ticket-response-refresh-view", info => {            
            this.getTicketResponseData();
        });
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;

        this.getTicketResponseData();        
    }

    getTicketResponseData() {
        this.isBusy(true);
        this.ticketResponseId(this.data()['id']);        
        this.webRequestTickerResponse.ticketResponseViewDetail(this.ticketResponseId()).done((res)=> {
            this.isBusy(false);
            this.txtName(res['name']);
        }); 
    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.Ticket_Response_Update)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
        }
        
        if (WebConfig.userSession.hasPermission(Constants.Permission.Ticket_Response_Delete)) {
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
        
    }

    onCommandClick(sender) {
        if (sender.id == "cmdUpdate") {
            this.navigate("ca-configuration-ticket-response-manage", {
                mode: Screen.SCREEN_MODE_UPDATE,
                data: this.data()
            });
        }

        if (sender.id == "cmdDelete") {
            this.showMessageBox(null, this.i18n("M208")(), BladeDialog.DIALOG_YESNO).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_YES:
                        this.isBusy(true);
                        this.webRequestTickerResponse.deleteTicketResponse(this.ticketResponseId()).done(() => {
                            this.isBusy(false);
                            this.publishMessage("ca-configuration-ticket-response-refresh-list");
                            this.close(true);
                        }).fail((e) => {
                            this.handleError(e);
                        }).always(() => {
                            this.isBusy(false);
                        });
                        break;
                    case BladeDialog.BUTTON_NO:
                        break;
                    default:
                        break;
                }
            });
        }
    }

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {

    }

    onUnload() {}

    get webRequestTickerResponse() {
        return WebRequestTickerResponse.getInstance();
    }
}



export default {
    viewModel: ScreenBase.createFactory(ConfigurationTicketResponseViewScreen),
    template: templateMarkup
};