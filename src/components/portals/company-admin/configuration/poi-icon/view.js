﻿import ko from "knockout";
import templateMarkup from "text!./view.html";
import ScreenBase from "../../../screenbase";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import {
    Constants,
    Enums,
    EntityAssociation
} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import WebRequestPoiIcon from "../../../../../app/frameworks/data/apitrackingcore/webRequestPoiIcon";

/**
 * 
 * 
 * @class ConfigurationPoiIconViewScreen
 * @extends {ScreenBase}
 */
class ConfigurationPoiIconViewScreen extends ScreenBase {
    /**
     * Creates an instance of ConfigurationPoiIconViewScreen.
     * 
     * @param {any} params
     * 
     * @memberOf BusinessUnitViewScreen
     */
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Configurations_ViewPOIIcon")());

        this.iconId = this.ensureNonObservable(params.iconId);
        this.name = ko.observable();
        this.iconUrl = ko.observable();
        this.offsetX = ko.observable();
        this.offsetY = ko.observable();

        this.subscribeMessage("ca-configuration-poi-icon-change", () => {
            this.isBusy(true);
            this.webRequestPoiIcon.getPoiIcon(this.iconId, [EntityAssociation.PoiIcon.Image]).done((poiIcon) => {

                this.name(poiIcon.name);
                this.iconUrl(poiIcon.image.fileUrl);
                this.offsetX(poiIcon.formatOffsetX);
                this.offsetY(poiIcon.formatOffsetY);

                this.isBusy(false);
            }).fail((e) => {
                this.handleError(e);
                this.isBusy(false);
            });
        });

    }

    get webRequestPoiIcon() {
        return WebRequestPoiIcon.getInstance();
    }

    onLoad(isFirstLoad) {
        if (!isFirstLoad) return;
        var dfd = $.Deferred();

        var d1 = this.webRequestPoiIcon.getPoiIcon(this.iconId, [EntityAssociation.PoiIcon.Image]);
        $.when(d1).done((poiIcon) => {
            if (poiIcon) {
                // debugger
                this.name(poiIcon.name);
                this.iconUrl(poiIcon.image.fileUrl);
                this.offsetX(poiIcon.formatOffsetX || "");
                this.offsetY(poiIcon.formatOffsetY || "");
            }
            dfd.resolve();
        }).fail((e) => {
            dfd.reject(e);
        });

        return dfd;
    }

    buildActionBar(actions) {}

    onActionClick(sender) {
        super.onActionClick(sender);
    }

    buildCommandBar(commands) {
        if (WebConfig.userSession.hasPermission(Constants.Permission.POIIconsManagement_Company)) {
            commands.push(this.createCommand("cmdUpdate", this.i18n("Common_Update")(), "svg-cmd-edit"));
            commands.push(this.createCommand("cmdDelete", this.i18n("Common_Delete")(), "svg-cmd-delete"));
        }
       
    }


    onCommandClick(sender) {
        switch (sender.id) {
            case "cmdUpdate":
                // console.log(this.iconId);
                this.navigate("ca-configuration-poi-icon-manage", {
                    mode: "update",
                    iconId: this.iconId
                });
                break;
            case "cmdDelete":

                this.showMessageBox(null, this.i18n("M121")(), BladeDialog.DIALOG_YESNO).done((button) => {
                    switch (button) {
                        case BladeDialog.BUTTON_YES:
                            this.isBusy(true);
                            this.webRequestPoiIcon.deletePoiIcon(this.iconId).done(() => {
                                this.isBusy(false);
                                this.publishMessage("ca-configuration-poi-icon-deleted", this.iconId);
                                this.close(true);
                            }).fail((e) => {
                                this.isBusy(false);
                                var errorObj = e.responseJSON;
                                if (errorObj) {
                                    this.displaySubmitError(errorObj);
                                }
                            });
                            break;
                    }
                });

                break;
        }
    }

    onUnload() {}

}

export default {
    viewModel: ScreenBase.createFactory(ConfigurationPoiIconViewScreen),
    template: templateMarkup
};