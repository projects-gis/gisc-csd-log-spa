import ko from "knockout";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import { EntityAssociation } from "../../../../../app/frameworks/constant/apiConstant";


class PoiIconFilter {
    constructor () {
        this.companyId = WebConfig.userSession.currentCompanyId;
        this.excludeDefaultPoiIcon = true;
        this.includeAssociationNames = [
            EntityAssociation.PoiIcon.Image
        ];
    }
}
export { PoiIconFilter };