import ko from "knockout";
import templateMarkup from "text!./manage.html";
import ScreenBase from "../../../screenbase";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import WebRequestTelematicsConfiguration from "../../../../../app/frameworks/data/apitrackingcore/webRequestTelematicsConfiguration";

/**
 * Display configuration menu based on user permission.
 * @class ConfigurationMenuScreen
 * @extends {ScreenBase}
 */
class TelematicesConfiguration extends ScreenBase {
    
    /**
     * Creates an instance of ConfigurationMenuScreen.
     * 
     * @param {any} params
     */
    constructor(params) {
        super(params);
        this.bladeSize = BladeSize.Medium;
        this.bladeTitle(this.i18n("Configurations_TelematicsConfiguration")());

        this.valAcceptance = ko.observable(20);
    }

    /**
     * Get WebRequest specific for Custom Area Category module in Web API access.
     * @readonly
     */
    get webRequestTelematicsConfiguration() {
        return WebRequestTelematicsConfiguration.getInstance();
    }

    setupExtend() {
        this.valAcceptance.extend({ required: true });

        this.validationModel = ko.validatedObservable({
            valAcceptance : this.valAcceptance
        });
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) {
            return;
        }

        this.webRequestTelematicsConfiguration.getTelematicsConfiguration(1)
            .done((response) => {
                if(response){
                    this.valAcceptance(response.fixingValue);
                }
        });

    }

    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {}

    /**
     * @lifecycle Called when adjacent child (only one level) is closed
     */
    onChildScreenClosed() {
        this.selectedIndex(null);
    }

    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actSave", this.i18n("Common_Save")()));
        actions.push(this.createActionCancel());
    }

    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {}

    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actSave") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                //this.isCheckDay(true);
                return;
            }

            var modelData = this.generateModel()
            this.webRequestTelematicsConfiguration.updateTelematicsConfiguration(modelData)
                .done((response) => {
                    this.close(true); 
                });
        }
        
    }

    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {}

    generateModel(){
        let model = {
            fixingValue : this.valAcceptance()
        }
        return model;
    }
}

    
export default {
viewModel: ScreenBase.createFactory(TelematicesConfiguration),
    template: templateMarkup
};