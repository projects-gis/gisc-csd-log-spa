﻿import ko from "knockout";
import templateMarkup from "text!./import.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import * as BladeSize from "../../../../../app/frameworks/constant/bladeSize";
import * as BladeDialog from "../../../../../app/frameworks/constant/bladeDialog";
import Utility from "../../../../../app/frameworks/core/utility";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestStringResource from "../../../../../app/frameworks/data/apicore/webRequestStringResource";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";

class TranslationImportScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Import")());
        this.bladeSize = BladeSize.Medium;

        this.selectedLanguage = ko.observable();
        this.selectedCategory = ko.observable();
        this.overwrite = ko.observable(false);
        this.media = ko.observable(null);
        this.attachmentMimeTypes = [
            Utility.getMIMEType("xls"),
            Utility.getMIMEType("xlsx")
        ].join();

        this.languages = ko.observableArray([]);
        this.categories = ko.observableArray([]);

        this.showDataTable = ko.observable(false);
        this.isValidToUpload = ko.pureComputed(()=> {
            if(this.selectedLanguage() && this.selectedCategory()){
                return true;
            }else{
                return false;
            }
        });

        this.enableDownload = ko.observable(false);
        this._selectedCategoryRef = this.selectedCategory.subscribe((value)=>{
            if(value){
                this.enableDownload(true);
            } else{
                this.enableDownload(false);
            } 

           this.clearUploadFile();  
        });
        this._selectedLanguageRef = this.selectedLanguage.subscribe((value)=>{
            this.clearUploadFile();
        });
        this._overwriteRef = this.overwrite.subscribe((value)=>{
            this.clearUploadFile();
        });

        //Translation File
        this.fileName = ko.observable('');
        this.selectedFile = ko.observable(null);
        this.importError = ko.observable('');
        this.previewStringResources = ko.observableArray([]);
        this.order = ko.observable([[ 0, "asc" ]]);
    }
    /**
     * Get WebRequest specific for Language module in Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for String Resource module in Web API access.
     * @readonly
     */
    get webRequestStringResource() {
        return WebRequestStringResource.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
        if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var languageFilter = {
            enable: true,
            sortingColumns: DefaultSoring.Language
        };
        var enumResourceFilter = {
            types: [Enums.ModelData.EnumResourceType.ResourceType],
            values: [Enums.ModelData.ResourceType.JobStatus, Enums.ModelData.ResourceType.JobWaypointStatus],
            sortingColumns: DefaultSoring.EnumResource
        };

        var d1 = this.webRequestLanguage.listLanguage(languageFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);

        $.when(d1, d2).done((r1, r2) => {
            this.languages(r1["items"]);
            this.categories(r2["items"]);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup track change.
        this.selectedLanguage.extend({
            trackChange: true
        });
        this.selectedCategory.extend({
            trackChange: true
        });
        this.overwrite.extend({
            trackChange: true
        });
        this.media.extend({
            trackChange: true
        });

        // Manual setup validation rules
        this.selectedLanguage.extend({
            required: true
        });
        this.selectedCategory.extend({
            required: true
        });
        this.selectedFile.extend({
            fileRequired: true,
            fileExtension: ['xlsx', 'xls'],
            fileSize: 4
        });
        this.validationModel = ko.validatedObservable({
            selectedLanguage: this.selectedLanguage,
            selectedCategory: this.selectedCategory,
            selectedFile: this.selectedFile
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
        commands.push(this.createCommand("cmdDownload", this.i18n("Common_DownloadTemplate")(), "svg-cmd-download-template", true, this.enableDownload)); 
    }
    /**
     * Generate translation model from View Model
     * 
     * @returns translation model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            companyId: WebConfig.userSession.currentCompanyId,
            cultureCode: this.selectedLanguage().code,
            resourceType: this.selectedCategory().value,
            overwrite: this.overwrite(),
            media: this.media()
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);

         if (sender.id === "actOK") {
            if (!this.validationModel.isValid() || this.media() == null) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);
            var model = this.generateModel();

            this.webRequestStringResource.importStringResource(model, true).done((response) => {
                this.isBusy(false);

                this.publishMessage("ca-language-translation-imported", { cultureCode: model.cultureCode, resourceType: model.resourceType });
                this.close(true);
            }).fail((e)=> {
                this.isBusy(false);

                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
        if (sender.id === "cmdDownload") {
            this.showMessageBox(null, this.i18n("M117")(),
                BladeDialog.DIALOG_OKCANCEL).done((button) => {
                switch (button) {
                    case BladeDialog.BUTTON_OK:
                        this.isBusy(true);
                        this.webRequestStringResource.downloadStringResourceTemplate(this.selectedCategory().value, Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx).done((response) => {
                            this.isBusy(false);
                            ScreenHelper.downloadFile(response.fileUrl);
                        }).fail((e)=> {
                            this.isBusy(false);
                            this.handleError(e);
                        });
                        break;
                }
            });
        }
    } 
    /**
     * 
     * Hook on fileupload before upload
     * @param {any} e
     */
    onBeforeUpload() {
        this.media(null);
        this.importError('');
        this.showDataTable(false); 
    }
    /**
     * Hook on fileuploadsuccess
     * @param {any} data
     */
    onUploadSuccess(data) {
        if(data && data.length){
            this.isBusy(true);
            this.media(data[0]);
            var model = this.generateModel();
            this.webRequestStringResource.importStringResource(model, false).done((response) => {
                if(Object.keys(response.messageIdToErrors).length){
                    var messageIdToErrors = response.messageIdToErrors;
                    var message = "";
                     if(messageIdToErrors.M090){
                        message = this.i18n("M090")();
                    }
                    else if (messageIdToErrors.M076) {
                        message = ScreenHelper.formatImportValidationErrorMessage("M076", messageIdToErrors.M076);

                    }
                    else if (messageIdToErrors.M077) {
                        message = ScreenHelper.formatImportValidationErrorMessage("M077", messageIdToErrors.M077);
                    }

                    this.media(null);
                    this.importError(message);
                }else{
                    this.showDataTable(true);
                    this.previewStringResources.replaceAll(response.data);
                }
                this.isBusy(false);
            }).fail((e)=> {
                this.isBusy(false);
                this.handleError(e);
                this.media(null);
                this.fileName('');
            });
        }
    }
    /**
     * 
     * Hook on fileuploadfail
     * @param {any} e
     */
    onUploadFail(e) {
        this.fileName('');
        this.handleError(e);
    }
    /**
     * 
     * Clear Upload File
     */
    clearUploadFile(){
        this.media(null);
        
         //Hide Preview Table
        this.importError('');
        this.showDataTable(false);

        //Clear Uploaded File
        this.fileName('');
        this.selectedFile(null);   
    }
}

export default {
    viewModel: ScreenBase.createFactory(TranslationImportScreen),
    template: templateMarkup
};