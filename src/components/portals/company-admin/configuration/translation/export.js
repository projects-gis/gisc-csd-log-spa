﻿import ko from "knockout";
import templateMarkup from "text!./export.html";
import ScreenBase from "../../../screenbase";
import ScreenHelper from "../../../screenhelper";
import WebRequestLanguage from "../../../../../app/frameworks/data/apicore/webRequestLanguage";
import WebRequestEnumResource from "../../../../../app/frameworks/data/apicore/webRequestEnumResource";
import WebRequestStringResource from "../../../../../app/frameworks/data/apicore/webRequestStringResource";
import {Enums} from "../../../../../app/frameworks/constant/apiConstant";
import WebConfig from "../../../../../app/frameworks/configuration/webconfiguration";
import DefaultSoring from "../../../../../app/frameworks/constant/defaultSorting";
import Utility from "../../../../../app/frameworks/core/utility";

class TranslationExportScreen extends ScreenBase {
    constructor(params) {
        super(params);

        this.bladeTitle(this.i18n("Common_Export")());

        this.selectedLanguage = ko.observable();
        this.selectedCategory = ko.observable();

        this.languages = ko.observableArray([]);
        this.categories = ko.observableArray([]);
    }
    /**
     * Get WebRequest specific for Language module in Web API access.
     * @readonly
     */
    get webRequestLanguage() {
        return WebRequestLanguage.getInstance();
    }
    /**
     * Get WebRequest specific for Enum Resource module in Web API access.
     * @readonly
     */
    get webRequestEnumResource() {
        return WebRequestEnumResource.getInstance();
    }
    /**
     * Get WebRequest specific for String Resource module in Web API access.
     * @readonly
     */
    get webRequestStringResource() {
        return WebRequestStringResource.getInstance();
    }
    /**
     * @lifecycle Called when View is loaded.
     * @param {boolean} isFirstLoad true if ViewModel is first load
     */
    onLoad(isFirstLoad) {
         if(!isFirstLoad) return;

        var dfd = $.Deferred();
        var languageFilter = {
            enable: true,
            sortingColumns: DefaultSoring.Language
        };
        var enumResourceFilter = {
            types: [Enums.ModelData.EnumResourceType.ResourceType],
            values: [Enums.ModelData.ResourceType.JobStatus, Enums.ModelData.ResourceType.JobWaypointStatus],
            sortingColumns: DefaultSoring.EnumResource
        };

        var d1 = this.webRequestLanguage.listLanguage(languageFilter);
        var d2 = this.webRequestEnumResource.listEnumResource(enumResourceFilter);

        $.when(d1, d2).done((r1, r2) => {
            this.languages(r1["items"]);
            this.categories(r2["items"]);

            dfd.resolve();
        }).fail((e)=> {
            dfd.reject(e);
        });

        return dfd;
    }
    /**
     * Setup Knockout Extends such as validation and trackChange
     * @lifecycle Called after onLoad
     */
    setupExtend() {
        // Manual setup validation rules
        this.selectedLanguage.extend({
            required: true
        });
        this.selectedCategory.extend({
            required: true
        });
        this.validationModel = ko.validatedObservable({
            selectedLanguage: this.selectedLanguage,
            selectedCategory: this.selectedCategory
        });
    }
    /**
     * Clear reference from DOM for garbage collection.
     * @lifecycle Called when View is unloaded.
     */
    onUnload() {
    }
    /**
     * @lifecycle Build available action button collection.
     * @param {any} actions
     */
    buildActionBar(actions) {
        actions.push(this.createAction("actOK", this.i18n("Common_OK")()));
        actions.push(this.createActionCancel());
    }
    /**
     * @lifecycle Build available commands collection.
     * @param {any} commands
     */
    buildCommandBar(commands) {
    }
    /**
     * Generate translation model from View Model
     * 
     * @returns translation model which is ready for ajax request 
     */
    generateModel() {
        var model = {
            companyId: WebConfig.userSession.currentCompanyId,
            cultureCode: this.selectedLanguage().code,
            resourceType: this.selectedCategory().value
        };
        return model;
    }
    /**
     * @lifecycle Handle when button on ActionBar is clicked.
     * @param {any} commands
     */
    onActionClick(sender) {
        super.onActionClick(sender);
        if (sender.id === "actOK") {
            if (!this.validationModel.isValid()) {
                this.validationModel.errors.showAllMessages();
                return;
            }

            this.isBusy(true);

            var model = this.generateModel();
            model.templateFileType = Utility.isiOSDevice() ? Enums.TemplateFileType.Xls : Enums.TemplateFileType.Xlsx;

            this.webRequestStringResource.exportStringResource(model).done((response) => {
                this.isBusy(false);
                ScreenHelper.downloadFile(response.fileUrl);
            }).fail((e)=> {
                this.isBusy(false);
                this.handleError(e);
            });
        }
    }
    /**
     * @lifecycle Hangle when command on CommandBar is clicked.
     * @param {any} sender
     */
    onCommandClick(sender) {
    }
}

export default {
    viewModel: ScreenBase.createFactory(TranslationExportScreen),
    template: templateMarkup
};