﻿define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/Constants",
    "GOTModule/utils/ResourceLib"
], function (declre, lang, ApplicationConfig, Constants, ResourceLib) {
    var _instance = null;
    var _cls = null;

    var _kmToMile = 0.621371192;
    var _m2Km = 1000;
    var _m2ToKm2 = Math.pow(_m2Km, 2);
    var _m2ToRai2 = 1600;
    var _m2ToNgan2 = 400;
    var _m2ToWa2 = 4;

    // var thaiUnit = {
    //     "en-US": {
    //         rai: "Rai",
    //         ngan: "Ngan",
    //         wa: "Wa"
    //     },
    //     "th-TH": {
    //         rai: "ไร่",
    //         ngan: "งาน",
    //         wa: "วา"
    //     }
    // };


    _cls = declre(null, {

        loadWidgetCSS: function (href) {
            var headID = document.getElementsByTagName("head").item(0), cssNode;
            var link = document.getElementsByTagName("link");
            var cssExist = false;
            for (var j = 0; j < link.length; j++) {
                if (link.item(j).href.toString().indexOf(href) > -1) {
                    cssExist = true;
                    break;
                }
            }
            if (!cssExist) {
                cssNode = document.createElement("link");
                cssNode.type = "text/css";
                cssNode.rel = "stylesheet";
                cssNode.href = href;
                headID.appendChild(cssNode);
            }
        },

        generateThemeLogoPath: function(key) {
            return ApplicationConfig.loadingPath + key + ".html";
        },

        createStreetViewUrl: function(obj){
            return ApplicationConfig.streetViewUrl + obj["lat"] + "," + obj["lon"];
        },

        debugLog: function (grpName, params) {

            if (!ApplicationConfig.debugLog) {
                return;
            }

            if (!params)
                params = [];
            else if (typeof (params) == 'string')
                params = [params];
            else if (!params.length)
                params = [params];

            // console.group(grpName);
            if (params.length > 0) {
                for (var ind in params) {
                    // console.log(params[ind]);
                }
            }
            // console.groupEnd();
        },

        getUniqueItem: function (arrData) {
            var u = {}, a = [];
            for (var i = 0, l = arrData.length; i < l; ++i) {
                if (u.hasOwnProperty(arrData[i])) {
                    continue;
                }
                a.push(arrData[i]);
                u[arrData[i]] = 1;
            }
            return a;
        },

        convertRGBA2Hex: function (str) {
            let result = "#";
            let a = 1;
            let ind = 0;
            str.split(',').map(lang.hitch(this, function (val) {
                if (ind == 3) {
                    a = Number(val);
                }
                else {
                    let pref = Number(val) > 16 ? "" : "0";
                    result += (pref + (Number(val).toString(16).toUpperCase()));
                }
                ind++;
            }));

            return { hex: result, a: a }
        },

        postify:function (value, prefix) {
            var result = {};

            var buildResult = function (object, prefix) {
                for (var key in object) {

                    var postKey = isFinite(key)
                        ? (prefix != "" ? prefix : "") + "[" + key + "]"
                        : (prefix != "" ? prefix + "." : "") + key;
                    switch (typeof (object[key])) {
                        case "number": case "string": case "boolean":
                            result[postKey] = object[key];
                            break;

                        case "object":
                            if (object[key] != null && object[key].toUTCString)
                                result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
                            else {
                                buildResult(object[key], postKey != "" ? postKey : key);
                            }
                    }
                }
            };

            if (value !== null && value !== undefined) {
                buildResult(value, (typeof (prefix) === "string" && prefix.length > 0) ? prefix : "");
            }

            return result;
        },

        isMobileDevice: function () {
            var isMobile = /Android|webOS|iPhone|iPad|BlackBerry|Windows Phone|Opera Mini|IEMobile|Mobile/i.test(navigator.userAgent);
            return isMobile;
        },

        calculateSizeForZoomFactor: function(size, zoom) {
            let sizeCalculated = size;
            if( ApplicationConfig.zoomFactor[zoom] )
                sizeCalculated= size * ( ApplicationConfig.zoomFactor[zoom] / 100);

            return sizeCalculated;
        },

        toDistanceUnitCompanyFormat: function (value) {
            var returnValue = null;
            var distanceUnit = ApplicationConfig.userInfo.info["_distanceUnitSymbol"];

            switch (ApplicationConfig.userInfo.info["_distanceUnit"])
            {
                case Constants.DistanceUnit.Metre: {
                    returnValue = value.toFixed(4) + " " + distanceUnit;
                    break;
                }
                case Constants.DistanceUnit.Kilometre: {
                    returnValue = (value / _m2Km).toFixed(4) + " " + distanceUnit;
                    break;
                }
                case Constants.DistanceUnit.Mile: {
                    returnValue = ((value / _m2Km) * _kmToMile).toFixed(4) + " " + distanceUnit;
                    break;
                }
                default:
                    break;
            }

            return returnValue;
        },

        toAreaUnit: function (value, unit){
            var retVal = null;


            console.log("value m2",value);
            switch(unit)
            {
                case "square-meters" : retVal = value; break;
                case "square-kilometers": retVal = Number((value / _m2ToKm2).toFixed(4)); break;
                case "thailand-unit":
                    var rai = 0, ngan = 0, wa = 0;

                    var remaining = value;
                    rai = Math.floor(remaining / _m2ToRai2);
                    remaining -= (rai * _m2ToRai2);

                    if (remaining > 0) {
                        ngan = Math.floor(remaining / _m2ToNgan2);
                        remaining -= (ngan * _m2ToNgan2);
                    }

                    if (remaining > 0) {
                        wa = Math.floor(remaining / _m2ToWa2);
                        remaining -= (wa * _m2ToWa2);
                    }

                    retVal = rai + ":" + ngan + ":" + wa;
                    break;
            }

            return retVal;
        },

        toAreaUnitCompanyFormat: function (value) {
            var appLang = ApplicationConfig.userInfo.appLanguage;
            var returnValue = null;
            var areaUnit = ApplicationConfig.userInfo.info["_areaUnitSymbol"];

            switch (ApplicationConfig.userInfo.info["_areaUnit"]) {
                case Constants.AreaUnit.SquareMetres: {
                    returnValue = value.toFixed(4) + " " + areaUnit;
                    break;
                }
                case Constants.AreaUnit.SquareKilometres: {
                    returnValue = (value / _m2ToKm2).toFixed(4) + " " + areaUnit;
                    break;
                }
                case Constants.AreaUnit.ThailandUnit: {

                    var rai = 0, ngan = 0, wa = 0;

                    var remaining = value;
                    rai = Math.floor(remaining / _m2ToRai2);
                    remaining -= (rai * _m2ToRai2);

                    if (remaining > 0) {
                        ngan = Math.floor(remaining / _m2ToNgan2);
                        remaining -= (ngan * _m2ToNgan2);
                    }

                    if (remaining > 0) {
                        wa = Math.floor(remaining / _m2ToWa2);
                        remaining -= (wa * _m2ToWa2);
                    }

                    var raiText = ResourceLib.text("Common_Rai");
                    var nganText = ResourceLib.text("Common_Ngan");
                    var waText = ResourceLib.text("Common_Wa");

                    returnValue = rai + " " + raiText
                        + " " + ngan + " " + nganText
                        + " " + wa + " " + waText;

                    break;
                }
                default:
                    break;
            }

            return returnValue;
        }
    });

    if (!_instance) {
        _instance = new _cls();

        //if (!ApplicationConfig.debugLog) {
        //    _instance.debugLog = function () { };
        //}
    }

    return _instance;
});