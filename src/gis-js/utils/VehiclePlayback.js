﻿define([
    "dijit/_WidgetBase",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/topic",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig"
], function (_WidgetBase, declare, lang, on, topic, jsonQuery, ApplicationConfig) {

    var _cls = declare([_WidgetBase], {
        
        animateTime: 0,
        fps: 0,
        directionConst: ["N", "NE", "E", "SE", "S", "SW", "W", "NW"],

        startLocation: null,
        currentLocation: null,
        nextLocation: null,

        lat: 0,
        lon: 0,
        currentAttributes: null,

        lonPerTime: 0,
        latPerTime: 0,

        graphic: null,
        geometry: null,
        symbol: null,

        textGraphic: null,
        textGeometry: null,

        playbackData: null,
        animateHandler: null,

        positionIndex: 0,
        arrayIndex: 0,
        totalBarLength: 0,

        isPause: null,

        timerExtent: 0,
        extentIndex: 0,

        constructor: function () {
            this.positionIndex = 0;
        },

        setPlaybackData: function (obj) {
            this.animateTime = ApplicationConfig.animationConfig.animateTime;
            this.fps = ApplicationConfig.animationConfig.fps;

            this.isPause = null;

            this.currentAttributes = null;
            this.arrayIndex = 0;
            this.positionIndex = 0;

            this.id = obj.id;

            this.graphic = obj.graphic;
            this.geometry = this.graphic.geometry;
            this.symbol = this.graphic.symbol;

            this.textGraphic = obj.textGraphic;
            this.textGeometry = this.textGraphic.geometry;

            this.playbackData = obj.playback;
            this.startLocation = obj.playback[0];
            this.directionPic = obj.playback[0].pic;

            this.currentAttributes = this.startLocation.attributes;

            this.lat = this.playbackData[0].lat;
            this.lon = this.playbackData[0].lon;

            this.totalBarLength = this.fps * this.animateTime * (this.playbackData.length - 1);

            this.timerExtent = ApplicationConfig.animationConfig.animateTime - 1;
            if (this.timerExtent == 0)
                this.timerExtent = 0.5;

            this.extentIndex = 0;

            this.onAttributeChanges(this.currentAttributes);
        },

        startAnimatePlayback: function () {

            if (this.positionIndex == 0) {
                this.onChangeView([[this.playbackData[this.arrayIndex].lon, this.playbackData[this.arrayIndex].lat], [this.playbackData[this.arrayIndex + 1].lon, this.playbackData[this.arrayIndex + 1].lat]]);
            }

            if (this.isPause) {
                this.animateHandler = requestAnimationFrame(lang.hitch(this, this.startAnimatePlayback));
            } else {
                this.positionIndex += 1;
                this.extentIndex += 1;

                this.arrayIndex = Math.floor(this.positionIndex / (this.fps * this.animateTime));

                if (this.positionIndex >= this.totalBarLength) {
                    this.stopAnimatePlayback();
                    return;
                }
                else {
                    if (this.currentAttributes !== this.playbackData[this.arrayIndex + 1].attributes)
                        this.onAttributeChanges(this.playbackData[this.arrayIndex + 1].attributes);

                    if (this.arrayIndex >= this.playbackData.length) {

                        this.stopAnimatePlayback();
                        return;
                    }

                    //เดิม
                    //if ((this.startLocation.lat != this.playbackData[this.arrayIndex].lat) && (this.startLocation.lon != this.playbackData[this.arrayIndex].lon)) {
                    //    this.onChangeView([[this.playbackData[this.arrayIndex].lon, this.playbackData[this.arrayIndex].lat], [this.playbackData[this.arrayIndex + 1].lon, this.playbackData[this.arrayIndex + 1].lat]]);
                    //}

                    //ใหม่
                    //console.log(this.extentIndex, this.timerExtent, this.extentIndex % (this.timerExtent * this.fps));

                    if (this.extentIndex % (this.timerExtent * this.fps) == 0) {

                        if (this.arrayIndex + 1 < this.playbackData.length && this.arrayIndex + 2 < this.playbackData.length) {

                            this.onChangeView([[this.playbackData[this.arrayIndex + 1].lon, this.playbackData[this.arrayIndex + 1].lat], [this.playbackData[this.arrayIndex + 2].lon, this.playbackData[this.arrayIndex + 2].lat]]);
                            this.timerExtent = this.animateTime;
                            this.extentIndex = 0;
                        }
                    }

                    this.startLocation = this.playbackData[this.arrayIndex];
                    this.nextLocation = this.playbackData[this.arrayIndex + 1];
                    this.currentAttributes = this.nextLocation.attributes;
                    this.directionPic = this.nextLocation.pic;

                    this.onVehicleMove({ geometry: this.geometry, index: this.positionIndex });
                    
                    if (this.positionIndex % (this.fps * this.animateTime) == 0) {
                        this.lat = this.playbackData[this.arrayIndex].lat;
                        this.lon = this.playbackData[this.arrayIndex].lon;
                    }

                    //if (!this.nextLocation) {
                    //    this.stopAnimatePlayback();
                    //    return;
                    //}

                    this.calculateRotation();
                    this.calculatePosition();

                    //console.log(this.nextLocation);

                    if (!this.nextLocation) {
                        this.stopAnimatePlayback();
                        return;
                    }


                    var maxLon = Math.max(this.nextLocation["lon"], this.startLocation["lon"]),
                        minLon = Math.min(this.nextLocation["lon"], this.startLocation["lon"]),
                        maxLat = Math.max(this.nextLocation["lat"], this.startLocation["lat"]),
                        minLat = Math.min(this.nextLocation["lat"], this.startLocation["lat"]);

                    if ((this.lon >= minLon || this.lon <= maxLon) ||
                        (this.lat >= minLat || this.lat <= maxLat)) {
                        this.animateHandler = requestAnimationFrame(lang.hitch(this, this.startAnimatePlayback))
                    } else {
                        this.stopAnimatePlayback();
                    }
                }
            }
        },

        pauseAnimatePlayback: function () {
            if (!this.playbackData) { return; }
            this.isPause = true;
        },

        resumeAnimatePlayback: function () {
            if (!this.playbackData) { return; }
            this.isPause = false;
        },

        stopAnimatePlayback: function () {

            if (this.animateHandler){
                cancelAnimationFrame(this.animateHandler);
                this.animateHandler = null;
                this.positionIndex = 0;
                this.arrayIndex = 0;

                this.currentAttributes = this.playbackData[0].attributes;
                this.startLocation = this.playbackData[0];

                this.lat = this.playbackData[0].lat;
                this.lon = this.playbackData[0].lon;

                this.timerExtent = ApplicationConfig.animationConfig.animateTime - 1;
                if (this.timerExtent == 0)
                    this.timerExtent = 0.5;

                this.extentIndex = 0;

                if (this.positionIndex >= this.totalBarLength) {
                    this.geometry.setLatitude(this.lat);
                    this.geometry.setLongitude(this.lon);

                    this.graphic.setGeometry(this.geometry);

                    this.textGeometry.setLatitude(this.lat);
                    this.textGeometry.setLongitude(this.lon);

                    this.textGraphic.setGeometry(this.textGeometry);

                    var directoinPic = this.directionPic["N"];
                    this.symbol.setUrl(directoinPic);
                    this.graphic.setSymbol(this.symbol);
                }
            }

            this.onVehicleEndAnimate();
        },

        stepIndex: function (indexValue) {
            this.positionIndex = indexValue;
            this.arrayIndex = Math.floor(indexValue / (this.fps * this.animateTime));

            this.startLocation = this.playbackData[this.arrayIndex];
            this.nextLocation = this.playbackData[this.arrayIndex + 1];

            let lonPerTime = (this.startLocation["lon"] - this.nextLocation["lon"]) / (this.fps * this.animateTime);
            let latPerTime = (this.startLocation["lat"] - this.nextLocation["lat"]) / (this.fps * this.animateTime);

            let forwardTime = this.positionIndex % (this.fps * this.animateTime);
            //this.lon = (this.lon - lonPerTime);
            //this.lat = (this.lat - latPerTime);

            this.lat = this.startLocation.lat - (latPerTime * forwardTime);
            this.lon = this.startLocation.lon - (lonPerTime * forwardTime);

            this.currentAttributes = this.nextLocation.attributes;

            this.calculateRotation();
            this.calculatePosition();

            if (this.positionIndex == this.totalBarLength - 1) {
                this.stopAnimatePlayback();
                this.isPause = null;
                return;
            }

            
            if (this.positionIndex > ((this.animateTime - 1) * this.fps)) {
                this.timerExtent = ApplicationConfig.animationConfig.animateTime;
                this.extentIndex = this.positionIndex - ((this.animateTime - 1) * this.fps);
            }
            else {
                this.timerExtent = ApplicationConfig.animationConfig.animateTime - 1;
                if (this.timerExtent == 0)
                    this.timerExtent = 0.5;

                this.extentIndex = this.positionIndex;
            }


            //if (this.isPause) {
            //    this.animateHandler = requestAnimationFrame(lang.hitch(this, this.startAnimatePlayback));
            //}
        },

        calculatePosition: function () {

            let lonPerTime = (this.startLocation["lon"] - this.nextLocation["lon"]) / (this.fps * this.animateTime);
            let latPerTime = (this.startLocation["lat"] - this.nextLocation["lat"]) / (this.fps * this.animateTime);
            
            this.lon = (this.lon - lonPerTime);
            this.lat = (this.lat - latPerTime);

            this.geometry.setLatitude(this.lat);
            this.geometry.setLongitude(this.lon);

            this.graphic.setGeometry(this.geometry);


            this.textGeometry.setLatitude(this.lat);
            this.textGeometry.setLongitude(this.lon);

            this.textGraphic.setGeometry(this.textGeometry);

        },

        calculateRotation: function () {

            if (!this.nextLocation) {
                this.stopAnimatePlayback();
                return;
            }

            var rotateDegree = (Math.atan2(this.nextLocation["lon"] - this.startLocation["lon"], this.nextLocation["lat"] - this.startLocation["lat"]) * (180 / Math.PI)).toFixed(1);

            var directionIndex = (Math.round((rotateDegree - 22.5) / 45));

            if (directionIndex < 0)
                directionIndex += 8;

            var directoinPic = this.directionPic[this.directionConst[directionIndex]];

            this.symbol.setUrl(directoinPic);
            this.graphic.setSymbol(this.symbol);
        },

        onVehicleMove: function (point) { /* attach event */ },
        onVehicleEndAnimate: function () {  /* attach event */ },
        onChangeView: function (evt) {  /* attach event */ },
        onAttributeChanges: function (evt) { /* attach event */ }
    });

    return _cls;
});