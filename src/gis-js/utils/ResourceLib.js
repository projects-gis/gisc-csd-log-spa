﻿define(["dojo/_base/declare", "dojo/topic"], function (declare, topic) {
    var _instance = null;
    var _resource = null;
    var _ResourceLib = null;
    var _NO_RESOURCE_TEXT = "NO TEXT VALUE";
    var _INVALID = "INVALID RESOURCE CONFIG";

    _ResourceLib = declare(null, {

        /*
        @description Current language key
        */
        currentLanguage: null,

        /*
        @description Load i18n resource json
        @param {json} i18next resource json format
        */
        loadResource: function (json) {
            _resource = json;

            //publish notify : Resource has been changed.
            topic.publish("resources-changed");
        },

        /*
        @description Set language of resource
        @param {language} language key
        */
        setLanguage: function (language) {
            this.currentLanguage = language;

            //publish notify : Language has been changed.
            topic.publish("language-changed", language);
        },

        /*
        @description Get resource
        @param {key} resource key
        @param {rplc} replace key in text resource
        @return {string} resource in current language
        */
        text: function (key, rplc) {
            var returnVal;

            if (key &&
                _resource &&
                this.currentLanguage &&
                _resource[this.currentLanguage] &&
                _resource[this.currentLanguage]["translation"]) {

                returnVal = _resource[this.currentLanguage]["translation"][key] || _NO_RESOURCE_TEXT;

                if (rplc) {
                    for (var rplcKey in rplc) {
                        returnVal = returnVal.replace(new RegExp("{{" + rplcKey + "}}", "g"), rplc[rplcKey]);
                    }
                }
            }
            else
                returnVal = _INVALID;

            return returnVal;
        }
    });

    /*
        @description singelton resource object
    */
    if (!_instance) {
        _instance = new _ResourceLib();
    }

    return _instance;
});