﻿define([
    "dijit/_WidgetBase",
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/on",
    "dojo/topic",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig"
], function (_WidgetBase, declare, lang, on, topic, jsonQuery, ApplicationConfig) {
    var _cls = declare([_WidgetBase], {

        vehicleTrackingList: null,

        constructor: function () {
            this.vehicleTrackingList = [];
        },

        addTrackableItem: function (obj) {
            //console.log("addTrackableItem", obj);
            if (jsonQuery("[?id='" + obj.id + "']", this.vehicleTrackingList).length == 0) {
                this.vehicleTrackingList.push({
                    id: obj.id,
                    vehicle: new VehicleItemCls({
                        vehicleID: obj.id,
                        directionPic: obj.directionPic,
                        location: obj.location,
                        graphic: obj.graphic,
                        textGraphic: obj.textGraphic
                    }),
                    track: (obj.track == true)
                });

                let lstTrack = jsonQuery("[?track=true][=vehicle]", this.vehicleTrackingList);

                this.onTrackLocationChange(lstTrack);
            }
        },

        checkAlreadyAddItem: function (id) {
            return jsonQuery("[?id='" + id + "']", this.vehicleTrackingList).length > 0;
        },

        getVehicle: function (id) {
            return jsonQuery("[?id='" + id + "']", this.vehicleTrackingList)[0] || null;
        },

        recieveData: function (objTrack, immediately) {
            var itm = objTrack

            let itmCurrent = this.getVehicle(itm.id);

            if (itmCurrent) {
                itmCurrent.track = itm.track;
                // Use CPU peak
                topic.publish("Trackable-Changed", { data : itm, immediately : immediately });
                let lstTrack = jsonQuery("[?track=true][=vehicle]", this.vehicleTrackingList);
                this.onTrackLocationChange(lstTrack);
             }
             else {
                 this.addTrackableItem(itm);
             }
        },

        removeTrackableItem: function (vehicle) {
            //console.log(vehicle);

            for (let i = 0; i < this.vehicleTrackingList.length; i++) {
                if (this.vehicleTrackingList[i].id == vehicle.id) {
                    this.vehicleTrackingList.splice(i, 1);
                    break;
                }
            }
        },

        clearAllTrackable: function () {
            this.vehicleTrackingList = [];
        },

        onTrackLocationChange: function (data) { /* attach event */ }
    });

    var VehicleItemCls = declare([_WidgetBase], {

        directionConst: ["N", "NE", "E", "SE", "S", "SW", "W", "NW"],

        animateTime: ApplicationConfig.animationConfig.animateTime,
        fps: ApplicationConfig.animationConfig.fps,

        vehicleID: null,
        directionPic: { "N": null, "NE": null, "E": null, "SE": null, "S": null, "SW": null, "W": null, "NW": null },
        animateHandler: null,

        startLocation: null,
        currentLocation: null,
        nextLocation: null,
        lat: 0,
        lon: 0,

        lonPerTime: 0,
        latPerTime: 0,

        graphic: null,
        geometry: null,
        symbol: null,

        textGraphic: null,
        textGeometry: null,

        animateState: true,
        currentAttributes: null,

        constructor: function (obj) {
            this.vehicleID = obj.vehicleID;
            this.directionPic = obj.pic;
            this.startLocation = obj.location;

            this.graphic = obj.graphic;
            this.geometry = this.graphic.geometry;
            this.symbol = this.graphic.symbol;

            this.lat = obj.location.lat;
            this.lon = obj.location.lon;
            
            if (obj.textGraphic) {
                this.textGraphic = obj.textGraphic;
                this.textGeometry = this.textGraphic.geometry;
            }

            topic.subscribe("Trackable-Changed", lang.hitch(this, function (obj) {
                var data = obj.data;
                var immediately = obj.immediately;

                if (data.id == this.vehicleID) {

                    //console.log("Current ID :: ", this.vehicleID, " Track Changed :: ", obj.data.id);

                    this.stopAnimate();

                    this.directionPic = data.directionPic;

                    //console.log("Attributes :: ",data.attributes);

                    this.graphic.setAttributes(data.attributes);

                    if (this.nextLocation) {
                        this.startLocation = lang.clone(this.nextLocation);
                        this.lat = this.startLocation.lat;
                        this.lon = this.startLocation.lon;
                    }

                    this.nextLocation = data.location;
                    if (this.startLocation.lat != this.nextLocation.lat || this.startLocation.lon != this.nextLocation.lon)
                    {
                        this.calculateRotation();
                    }
                    else {
                        //Edit 20190326 : Min - ถ้า Latitude Longitude ไม่เป็น จะเซ็ตรูปตามDirectionที่ได้จากข้อมูลServices
                        var directionIndex = data.attributes["RAW"]["directionType"] - 1;
                        var directoinPic = this.directionPic[this.directionConst[directionIndex]];

                        this.symbol.setUrl(directoinPic);
                        this.graphic.setSymbol(this.symbol);
                    }


                    if (immediately) {
                        this.lat = this.nextLocation.lat;
                        this.lon = this.nextLocation.lon;

                        this.warp();
                        //this.animateTime = 1;
                        //this.fps = 1;
                    } else {
                        //this.animateTime = ApplicationConfig.animationConfig.animateTime;
                        //this.fps = ApplicationConfig.animationConfig.fps;
                        this.startAnimate();
                    }
                }
            }));
        },

        calculateRotation: function () {
            var rotateDegree = (Math.atan2(this.nextLocation["lon"] - this.startLocation["lon"], this.nextLocation["lat"] - this.startLocation["lat"]) * (180 / Math.PI)).toFixed(1);

            var directionIndex = (Math.round((rotateDegree - 22.5) / 45));

            if (directionIndex < 0)
                directionIndex += 8;

            var directoinPic = this.directionPic[this.directionConst[directionIndex]];

            this.symbol.setUrl(directoinPic);
            this.graphic.setSymbol(this.symbol);
        },

        warp: function () {
            //console.log("warp ID :: ", this.vehicleID);
            this.geometry.setLatitude(this.lat);
            this.geometry.setLongitude(this.lon);

            this.graphic.setGeometry(this.geometry);

            if (this.textGraphic) {
                this.textGeometry.setLatitude(this.lat);
                this.textGeometry.setLongitude(this.lon);

                this.textGraphic.setGeometry(this.geometry);
            }
        },

        calculatePosition: function () {
            
            lonPerTime = (this.startLocation["lon"] - this.nextLocation["lon"]) / (this.fps * this.animateTime);
            latPerTime = (this.startLocation["lat"] - this.nextLocation["lat"]) / (this.fps * this.animateTime);

            this.lon = (this.lon - lonPerTime);
            this.lat = (this.lat - latPerTime);

            this.geometry.setLatitude(this.lat);
            this.geometry.setLongitude(this.lon);

            this.graphic.setGeometry(this.geometry);

            if (this.textGraphic) {
                this.textGeometry.setLatitude(this.lat);
                this.textGeometry.setLongitude(this.lon);

                this.textGraphic.setGeometry(this.geometry);
            }
        },

        startAnimate: function () {
            if (!this.nextLocation) { return; }
            this.calculatePosition();

            var maxLon = Math.max(this.nextLocation["lon"], this.startLocation["lon"]),
                minLon = Math.min(this.nextLocation["lon"], this.startLocation["lon"]),
                maxLat = Math.max(this.nextLocation["lat"], this.startLocation["lat"]),
                minLat = Math.min(this.nextLocation["lat"], this.startLocation["lat"]);

            if ((this.lon >= minLon && this.lon <= maxLon) &&
                (this.lat >= minLat && this.lat <= maxLat)) {
                this.animateHandler = requestAnimationFrame(lang.hitch(this, this.startAnimate))
            }
            else {
                this.stopAnimate();
            }
        },

        stopAnimate: function () {
            //console.log("stop from :: ", this.vehicleID);
            cancelAnimationFrame(this.animateHandler);
        }
    });

    return _cls;
});