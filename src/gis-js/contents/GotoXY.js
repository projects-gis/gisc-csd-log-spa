﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/GotoXY.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () {
            on(this.btnPressOk, "click", lang.hitch(this, function () {
                this._sentLatLon();
            }));
        },

        _sentLatLon: function () {
            var valLatLon = this.txtLatLon.value;
            //console.log("txt",valLatLon);
            var regexLatLon = new RegExp(/(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)/);

            if (regexLatLon.test(valLatLon)) {
                dojo.removeClass(this.txtLatLon, "global-form-invalid");
                this.lblLatLonErrorMsg.innerHTML = "";
                var splitLatLon = valLatLon.split(",");

                //validate lat lon is in bound
                let lat = splitLatLon[0];
                let lon = splitLatLon[1];

                if (!(lat && !isNaN(Number(lat)) != NaN && (lat <= 90 && lat >= -90))) {
                    dojo.addClass(this.txtLatLon, "global-form-invalid");
                    this.lblLatLonErrorMsg.innerHTML = ResourceLib.text("wg-goto-xy-invalid-coord-msg");
                    return;
                }

                if (!(lon && !isNaN(Number(lon)) && (lon <= 180 && lon >= -180))) {
                    dojo.addClass(this.txtLatLon, "global-form-invalid");
                    this.lblLatLonErrorMsg.innerHTML = ResourceLib.text("wg-goto-xy-invalid-coord-msg");
                    return;
                }


                this.onAction({ key: "goto-location", param: { lat: splitLatLon[0], lon: splitLatLon[1] } });
            } else {
                dojo.addClass(this.txtLatLon, "global-form-invalid");
                this.lblLatLonErrorMsg.innerHTML = ResourceLib.text("wg-goto-xy-invalid-coord-msg");
            }
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }

            this.lblLatLonErrorMsg.innerHTML = "";
        },

        /* On toolpanel close */
        onClose: function () {
            var lyr = ApplicationConfig.mapManager.getLayer("lyr-goto-xy");
            lyr.clear();

            dojo.removeClass(this.txtLatLon, "global-form-invalid");
            this.lblLatLonErrorMsg.innerHTML = ResourceLib.text("wg-goto-xy-invalid-coord-msg");
            this.txtLatLon.value = "";
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});