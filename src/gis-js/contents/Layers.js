﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/Layers.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility) {

    let ctrl = null;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () { },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                ctrl = new ListItemContainer({ selectedMode: "toggle" });
                domConstruct.place(ctrl.domNode, this.dibBasemapList, "last");

                var lstLayerItem = ApplicationConfig.lstLayerState;
                var lstItem = [];
                array.forEach(lstLayerItem, lang.hitch(this, function (itm) {
                    lstItem.push({
                        key: itm["id"],
                        text: ResourceLib.text("wg-layer-" + itm["id"]),
                        icon: "./gis-js/images/menu/layers.png"
                    });
                }));

                ctrl.createItem(lstItem);

                on(ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));
            }
        },

        _ListClickHandler: function (obj) {
            this.onAction({ key: "toggle-layer", param: { layer: obj.key } });
            this.onHasActiveContent(ctrl.getCountSelected());
        },

        /* On toolpanel close */
        onClose: function () { },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ },

        /* Send State to ToolsPanel */
        onHasActiveContent: function (hasActive) {/* attach event */ }
    });
});