﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/VehicleInfo.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () { },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }

            //console.log(param);

            let attributes = param["attributes"];
            let status = attributes["STATUS"];
            let keySuffix = ResourceLib.currentLanguage.toLowerCase() == "en" ? "English" : "Local";

            this.divVehicleHeader.innerHTML = (attributes["TITLE"] || "") + " " + (attributes["BOX_ID"] ? ("(" + attributes["BOX_ID"] + ")") : "");
            this.divDepartment.innerHTML = attributes["DEPT"] || "";
            this.divDepartment.title = attributes["DEPT"] || "";
            this.divDriverName.innerHTML = attributes["DRIVER_NAME"] || "";
            this.divLocation.innerHTML = attributes["LOCATION"] || "";
            this.divDate.innerHTML = attributes["DATE"] || "";

            if (attributes["PARK_TIME"] && attributes["PARK_TIME"] != "") {
                this.divParkTime.innerHTML = ResourceLib.text("wg-vehicle-info-park-time") + ": " + (attributes["PARK_TIME"] || ""); //+ " " + ResourceLib.text("wg-vehicle-info-park-time-unit");
            }
            else {
                this.divParkTime.innerHTML = "";
            }

            if (attributes["PARK_IDLE_TIME"] && attributes["PARK_IDLE_TIME"] != ""&& attributes["PARK_IDLE_TIME"] != "-") {
                this.divIdleTime.innerHTML = ResourceLib.text("wg-vehicle-info-park-idle-time") + ": " + (attributes["PARK_IDLE_TIME"] || ""); //+ " " + ResourceLib.text("wg-vehicle-info-park-time-unit");
                //domClass.remove(this.divIdleTime, "hide-park");
            }
            else {
                this.divIdleTime.innerHTML = "";
            }
            /* STATUS SETTING */

            domConstruct.empty(this.divListStat);

            let divBlock = null;
            array.forEach(status, lang.hitch(this, function (statItem, i) {

                var title = "";
                var statusText = "";

                if (statItem.title != undefined && statItem.title != null)
                    title = statItem.title;

                if (statItem.text != undefined && statItem.text != null)
                    statusText = statItem.text;

                if (i % 2 == 0) {
                    divBlock = domConstruct.create("div", { class: "div-status-blog" }, this.divListStat, "last");
                }

                let divContainer = domConstruct.create("div", { class: "div-icon-container", title: title }, divBlock, "last");

                let divIcon = domConstruct.create("div", { class: "div-svg-icon-status " + statItem.cssClass }, divContainer, "last");
                let svg = domConstruct.toDom(statItem.icon);

                let divText = domConstruct.create("div", { class: "div-svg-icon-status-text", innerHTML: statusText, title: statusText }, divContainer, "last");

                domConstruct.place(svg, divIcon, "last");
            }));
        },

        /* On toolpanel close */
        onClose: function () {
            this.onAction({ key: "close-panel" });
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});