define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/PolygonArea.html",

    "dojo/text!../images/svg/btn_editingtool_back_press.svg",
    "dojo/text!../images/svg/btn_editingtool_cancel_press.svg",
    "dojo/text!../images/svg/btn_editingtool_draw_press.svg",
    "dojo/text!../images/svg/btn_editingtool_next_press.svg",
    "dojo/text!../images/svg/btn_editingtool_stop_press.svg",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/on",
    "dojox/json/query",

    "esri/graphic",
    "esri/Color",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/geometry/geometryEngine",
    "esri/geometry/webMercatorUtils",
    "esri/geometry/Polygon",
    "esri/geometry/Point",

    "esri/toolbars/draw",
    "esri/toolbars/edit",

    "GOTModule/controls/ListItemPolygon",
    "GOTModule/controls/ToolPanel",


    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, svgBack, svgCancel, svgDraw, svgNext, svgStop,
    dom, domConstruct, domClass, domStyle, domAttr, lang, array, dojoEvent, on, jsonQuery,
    Graphic, Color, SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, geometryEngine, webMercatorUtils, Polygon, Point, DrawTools, EditTools,
    ListItemPolygon,ToolPanel,
    ApplicationConfig, ResourceLib, Utility) {
        let textOutAreaMain = ResourceLib.text("wg-polygon-editor-error-parent-area-out")
        let textintersectsArea = ResourceLib.text("wg-polygon-editor-error-parent-area-intersects")
        let editGraphic = null;
        let mapManager = ApplicationConfig.mapManager;
        let layerID = ApplicationConfig.commandOptions["enable-draw-polygon-area"].layer;
        let layerIDMainArea = ApplicationConfig.commandOptions["draw-polygon-main-area"].layer;
        let layerIDSubArea = ApplicationConfig.commandOptions["draw-polygon-sub-area"].layer;
        let map = ApplicationConfig.mapObject;
        let drawMode = DrawTools.POLYGON;
        let editMode = EditTools.MOVE | EditTools.EDIT_VERTICES | EditTools.SCALE | EditTools.ROTATE;
        let drawOption = {
            showTooltips: false
        }
        let undoIndex = 0;
        let undoStack = [null];

        let areaValue = 0;
        let numOfPointValue = 0;
        let areaUnit = null;
        let guid = null;
        let clickHandler = null;
        let isInArea = null;
        let inSubAreaStatus = true;

        let polygonMarker = new SimpleFillSymbol(
            SimpleFillSymbol.STYLE_SOLID,
            new SimpleLineSymbol(
                SimpleLineSymbol.STYLE_SOLID,
                new Color(ApplicationConfig.measurementSetting.polygonSymbol.lineColor), ApplicationConfig.measurementSetting.polygonSymbol.lineWidth),
            new Color(ApplicationConfig.measurementSetting.polygonSymbol.fillColor));
    
        polygonMarker.color.a = ApplicationConfig.measurementSetting.polygonSymbol.fillOpacity;
        polygonMarker.outline.color.a = ApplicationConfig.measurementSetting.polygonSymbol.lineOpacity;
    
        let editOptions = {
            uniformScaling: false,
            allowAddVertices: true,
            allowDeleteVertices: true,
            showTooltips: false
        };
        
        let textInpolygon = null
        let symbolBorderColor = null
        let symbolFillColor = null


    var _cls = declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) {
            layerDrawingArea = mapManager.getLayer(layerID);
            layerMainArea = mapManager.getLayer(layerIDMainArea);

            drawMngArea = new DrawTools(map);
            editMngArea = new EditTools(map);

            // //drawMng.setFillSymbol(polygonMarker);

            on(drawMngArea, "draw-complete", lang.hitch(this, "_drawPolygonCompleted"));

            on(editMngArea, "deactivate", lang.hitch(this, "_editPolygonCompleted"));
            
            
            // editMngArea.on("vertex-move-stop", (evt) => {
            //     editMngArea.deactivate()
            // })
            // map.on("click", function(evt) {
            //     editMngArea.deactivate();
            //      // editToolbar.activate(Edit.EDIT_VERTICES, selected);
            //    });
        },
        postCreate: function () {
            //svgBack, svgCancel, svgDraw, svgNext, svgStop,
            domConstruct.place(domConstruct.toDom(svgBack), this.btnUndoArea, "last");
            domConstruct.place(domConstruct.toDom(svgNext), this.btnRedoArea, "last");
            domConstruct.place(domConstruct.toDom(svgCancel), this.btnDeleteGraphicsArea, "last");
            domConstruct.place(domConstruct.toDom(svgDraw), this.btnStartDrawingArea, "last");
            domConstruct.place(domConstruct.toDom(svgStop), this.btnFinishDrawingArea, "last");

            

            on(this.btnStartDrawingArea, "click", lang.hitch(this, "_startDrawing"));
            on(this.btnFinishDrawingArea, "click", lang.hitch(this, "_endDrawing"));
            on(this.btnDeleteGraphicsArea, "click", lang.hitch(this, "_clearDrawing"));
            on(this.btnUndoArea, "click", lang.hitch(this, "_undoEditing"));
            on(this.btnRedoArea, "click", lang.hitch(this, "_redoEditing"));

            on(this.btnSubmit, "click", lang.hitch(this, "_submitDrawing"));
            on(this.btnCancel, "click", lang.hitch(this, "_cancelDrawing"));

        },

        _cancelDrawing: function () {
            this._clearDrawing();
            guid = null;
            //this.onAction({ key: "close-panel" });

            this.onAction({ key: "cancel-drawing-polygon", param: { guid: guid } });
        },
        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

            }    
                domConstruct.empty(this.divPointCollectionArea);
                domConstruct.empty(this.divSizeValue);
                domConstruct.empty(this.divNumValue);
                
                if (param && param["guid"])
                guid = param["guid"];
                
                if(param && param["name"]){
                    textInpolygon = param["name"]
                }
                if(param && param["typearea"]){
                    symbolBorderColor = param["typearea"].symbolBorderColor
                    symbolFillColor = param["typearea"].symbolFillColor
                }
                
                if (param && param["areaUnit"]) {
                    areaUnit = param["areaUnit"];
                    this.divSizeUnitText.innerHTML = areaUnit.text;
                }

                this._clearDrawing();
                
                if (clickHandler) { clickHandler.remove(); }
                clickHandler = on(layerDrawingArea, "click", lang.hitch(this, "_OnGPDrawingClick"));
                
                mapManager.setMapClickMode("enable-draw-polygon-area", "enable-draw-polygon-area");
             
            if (param && param["point"]) {
                let ringData = param["point"];

                let plg = new Polygon({ rings: ringData, spatialReference: { wkid: 4326 } });
                let merPlg = webMercatorUtils.geographicToWebMercator(plg);
                let gp = new Graphic(merPlg, polygonMarker);

                editGraphic = gp.toJson();

                undoStack = [];
                undoStack.push(editGraphic);

                layerDrawingArea.add(gp);

                this.createPointTable({ rings: ringData });
                this.calculate(merPlg);

                //array.forEach(merPlg.rings[0], lang.hitch(this, function (pt) {
                //    layerDrawingArea.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                //}));
            }
        },

        /* On toolpanel close */
        onClose: function (stat) {
            startEdit = null;
            if (clickHandler) { clickHandler.remove(); }

            layerDrawingArea.clear();

            drawMngArea.deactivate();
            editMngArea.deactivate();

            domClass.add(this.btnRedoArea, "disabled-btn");
            domClass.add(this.btnFinishDrawingArea, "disabled-btn");
            domClass.add(this.btnDeleteGraphicsArea, "disabled-btn");
            domClass.add(this.btnUndoArea, "disabled-btn");
            domClass.remove(this.btnStartDrawingArea, "disabled-btn");

            // if (clickHandler) { clickHandler.remove(); }
            // mapManager.setMapClickMode("none", "");

            // if (stat)
            //     this.onAction({ key: "close-drawing-polygon", param: { guid: guid } });
        },
        _OnGPDrawingClick: function (e) {
            editMngArea.activate(editMode, e.graphic, editOptions);

            startEdit = e.graphic;

            domClass.add(this.btnStartDrawingArea, "disabled-btn");
            domClass.add(this.btnDeleteGraphicsArea, "disabled-btn");
            domClass.add(this.btnUndoArea, "disabled-btn");
            domClass.add(this.btnRedoArea, "disabled-btn");
            domClass.remove(this.btnFinishDrawingArea, "disabled-btn");

            
            dojoEvent.stop(e);
        },
        _undoEditing: function () {

            if (domClass.contains(this.btnUndoArea, "disabled-btn")) { return;}

            undoIndex -= 1;

            if (undoIndex < 0)
                undoIndex = 0;

            let operation = lang.clone(undoStack[undoIndex]);
            editGraphic = operation;

            if (operation) {
                let gp = new Graphic(operation, polygonMarker)
                layerDrawingArea.clear();
                layerDrawingArea.add(gp);

                var latlonGeo = webMercatorUtils.webMercatorToGeographic(gp.geometry);
                this.createPointTable(latlonGeo);
                this.calculate(this.adjustedGeometry(latlonGeo));

                //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
                //    layerDrawingArea.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                //}));
            }
            else {
                domConstruct.empty(this.divPointCollectionArea);
                //domConstruct.create("div", { "class": "div-point-collection-item" }, this.divPointCollection, "last");

                layerDrawingArea.clear();
                this.calculate(null);
            }
        },

        _redoEditing: function () {
            if (domClass.contains(this.btnRedoArea, "disabled-btn")) { return; }

            undoIndex += 1;

            if (undoIndex >= undoStack.length)
                undoIndex = undoStack.length - 1;

            let operation = lang.clone(undoStack[undoIndex]);

            if (operation) {

                editGraphic = operation;
                let gp = new Graphic(operation, polygonMarker)
                layerDrawingArea.clear();
                layerDrawingArea.add(gp);

                var latlonGeo = webMercatorUtils.webMercatorToGeographic(gp.geometry);
                this.createPointTable(latlonGeo);
                this.calculate(this.adjustedGeometry(latlonGeo));

                //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
                //    layerDrawingArea.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                //}));
            }
        },

        _drawPolygonCompleted: function (obj) {

            // console.log("layerIDSubArea",layerSubArea);
            isInArea =  geometryEngine.contains(layerMainArea.graphics[0].geometry, obj.geographicGeometry);
            if(!isInArea){
                this.divAlertText.innerHTML = textOutAreaMain
            }else{
                this.divAlertText.innerHTML = ""
                let layerSubArea = mapManager.getLayer(layerIDSubArea);
                if(layerSubArea.graphics.length > 0){
                    let isInSubArea = []
                    layerSubArea.graphics.forEach(function(element) {
                        if(element.geometry.rings){
                            isInSubArea.push(geometryEngine.intersects(obj.geographicGeometry, element.geometry));
                            
                        }
                    });
                            let param = isInSubArea.every(function(ev){return ev == false});
                            inSubAreaStatus = param
    
                            if(!param){
                                this.divAlertText.innerHTML = textintersectsArea
                            }else{
                                this.divAlertText.innerHTML = ""
                            }
                }
            }
            

            

            drawMngArea.deactivate();
            domClass.remove(this.btnStartDrawingArea, "disabled-btn");
            domClass.remove(this.btnDeleteGraphicsArea, "disabled-btn");
            domClass.remove(this.btnUndoArea, "disabled-btn");
            domClass.remove(this.btnRedoArea, "disabled-btn");
            domClass.add(this.btnFinishDrawingArea, "disabled-btn");

            var geometry = obj.geometry;
            var latlonGeo = obj.geographicGeometry;
            this.createPointTable(latlonGeo);
            this.calculate(this.adjustedGeometry(obj.geographicGeometry));

            let polygonMarkerArea = new SimpleFillSymbol(
                SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_SOLID,
                    new Color("rgba("+symbolBorderColor+")"), 
                    ApplicationConfig.measurementSetting.polygonSymbol.lineWidth),
                    new Color("rgba("+symbolFillColor+")"));

        
            var gp = new Graphic(geometry, polygonMarker);
            editGraphic = gp.toJson();

            undoStack = undoStack.splice(0, undoIndex + 1);
            undoStack.push(editGraphic);
            undoIndex = undoStack.length - 1;

            layerDrawingArea.clear();
            layerDrawingArea.add(gp);

            //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
            //    layerDrawingArea.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
            //}));
        },

        _editPolygonCompleted: function (obj) {
            
            var latlonGeo = webMercatorUtils.webMercatorToGeographic(obj.graphic.geometry);
            isInArea =  geometryEngine.contains(layerMainArea.graphics[0].geometry, latlonGeo);
            if(!isInArea){
                this.divAlertText.innerHTML = textOutAreaMain
            }else{
                this.divAlertText.innerHTML = ""
                let layerSubArea = mapManager.getLayer(layerIDSubArea);
                if(layerSubArea.graphics.length > 0){
                    let isInSubArea = []
                    layerSubArea.graphics.forEach(function(element){
                        if(element.geometry.rings){
                            isInSubArea.push(geometryEngine.intersects(latlonGeo, element.geometry));
                            
                        }
                    });
    
                            let param = isInSubArea.every(function(ev){return ev == false});
                            inSubAreaStatus = param
                            if(!param){
                                this.divAlertText.innerHTML = textintersectsArea
                            }else{
                                this.divAlertText.innerHTML = ""
                            }
                }
            }


            this.createPointTable(latlonGeo);
            // this.calculate(this.adjustedGeometry(latlonGeo));
            this.calculate(latlonGeo);

            editGraphic = lang.clone(obj.graphic.toJson());

            undoStack = undoStack.splice(0, undoIndex + 1);

            undoStack.push(editGraphic);
            undoIndex = undoStack.length - 1;
        },
        adjustedGeometry: function (geoLatlon) {
            let ring = lang.clone(geoLatlon.rings[0]);
            let max = ring.length;

            for (let i = 0; i < max; i++) {
                ring[i][1] = ring[i][1].toFixed(6);
                ring[i][0] = ring[i][0].toFixed(6);
            }

            var polygonJson = {
                "rings": [ring], "spatialReference": { "wkid": 4326 }
            };
            var polygon = new Polygon(polygonJson);
            return webMercatorUtils.geographicToWebMercator(polygon);
        },

        _submitDrawing: function () {
            var resultGeo = null;
            var centroid = null;
            if (editGraphic) {
                resultGeo = webMercatorUtils.webMercatorToGeographic(new Polygon(editGraphic.geometry)).toJson();
                centroid = webMercatorUtils.webMercatorToGeographic(new Polygon(editGraphic.geometry)).getCentroid();
            }
            if (!resultGeo) { return; }
            if(!isInArea) {return;}
            if(!inSubAreaStatus){return;}

            let max = resultGeo.rings[0].length;
            let lstLat = [], lstLon = [];
            for (let i = 0; i < max; i++) {
                resultGeo.rings[0][i][0] = Number(resultGeo.rings[0][i][0].toFixed(6));
                resultGeo.rings[0][i][1] = Number(resultGeo.rings[0][i][1].toFixed(6));

                lstLon.push(Number(resultGeo.rings[0][i][0].toFixed(6)));
                lstLat.push(Number(resultGeo.rings[0][i][1].toFixed(6)));
            }

            /* Find Min - Max of Latitude and Longitude */
            lstLon = lstLon.sort(function (a, b) { return a - b });
            lstLat = lstLat.sort(function (a, b) { return a - b });
            this._clearDrawing();

            let sendingData = {
                    geometry: resultGeo,
                    area: areaValue,
                    count: numOfPointValue,
                    guid: guid,
                    centralLatitude: centroid.y.toFixed(6),
                    centralLongitude: centroid.x.toFixed(6),
                    minLatitude: lstLat[0],
                    maxLatitude: lstLat[max - 1],
                    minLongitude: lstLon[0],
                    maxLongitude: lstLon[max - 1]
            }
            this.onAction({
                key: "drawing-polygon-sub-area-complete",
                param: sendingData
                // param: {
                //     geometry: resultGeo,
                //     area: areaValue,
                //     count: numOfPointValue,
                //     guid: guid,
                //     centralLatitude: centroid.y.toFixed(6),
                //     centralLongitude: centroid.x.toFixed(6),
                //     minLatitude: lstLat[0],
                //     maxLatitude: lstLat[max - 1],
                //     minLongitude: lstLon[0],
                //     maxLongitude: lstLon[max - 1]
                // }
            });
        },
        clearDrawingGraphic: function () {
            layerDrawingArea.clear();
        },
        _clearDrawing: function () {
            if (domClass.contains(this.btnDeleteGraphicsArea, "disabled-btn")) { return; }

            undoStack = [null];
            undoIndex = 0;
            layerDrawingArea.clear();
            editGraphic = null;

            domConstruct.empty(this.divSizeValue);
            domConstruct.empty(this.divNumValue);
            domConstruct.empty(this.divPointCollectionArea);
        },

        _startDrawing: function () {
            if (domClass.contains(this.btnStartDrawingArea, "disabled-btn")) { return; }
            mapManager.onDrawing = "enable-draw-polygon-area" //ตั้งค่าการวาด custom area
            layerDrawingArea.clear();
            drawMngArea.activate(drawMode, { showTooltips: false });

            this._clearDrawing();

            domClass.add(this.btnDeleteGraphicsArea, "disabled-btn");
            domClass.add(this.btnUndoArea, "disabled-btn");
            domClass.add(this.btnRedoArea, "disabled-btn");
            domClass.add(this.btnStartDrawingArea, "disabled-btn");
            domClass.add(this.btnFinishDrawingArea, "disabled-btn");
            //"disabled-btn"
        },
        _endDrawing: function () {
            if (!startEdit) { return; }

            editMngArea.deactivate();
            domClass.add(this.btnFinishDrawingArea, "disabled-btn");
            domClass.remove(this.btnDeleteGraphicsArea, "disabled-btn");
            domClass.remove(this.btnUndoArea, "disabled-btn");
            domClass.remove(this.btnRedoArea, "disabled-btn");
            domClass.remove(this.btnStartDrawingArea, "disabled-btn");

            let poly = startEdit.geometry;

            // var latlonGeo = webMercatorUtils.webMercatorToGeographic(poly);

            var gp = new Graphic(poly, polygonMarker);

            layerDrawingArea.clear();
            layerDrawingArea.add(gp);

            //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
            //    layerDrawingArea.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
            //}));

        },

        createPointTable: function (geoLatlon) {

            let ring = geoLatlon.rings[0];
            let max = ring.length;

            domConstruct.empty(this.divPointCollectionArea);
            //domConstruct.create("div", { "class": "div-point-collection-item" }, this.divPointCollection, "last");

            //not include last(first point)
            for (let i = 0; i < max - 1; i++) {
                domConstruct.create("div", { "class": "div-point-collection-item", innerHTML: ring[i][1].toFixed(6) + ',' + ring[i][0].toFixed(6) }, this.divPointCollectionArea, "last");
            }
        },

        calculate: function (geo) {

            var areaUnitConvertType = "";
            switch (areaUnit.type) {
                case 1:
                    areaUnitConvertType = "square-meters";
                    break;
                case 2:
                    areaUnitConvertType = "square-kilometers";
                    break;
                case 3:
                    areaUnitConvertType = "thailand-unit";
                    break;
            }

            if (geo) {
                //areaValue = Utility.toAreaUnit(geometryEngine.geodesicArea(geo, "square-meters"), "square-meters");

                areaValue = geometryEngine.geodesicArea(geo, "square-meters");//Utility.toAreaUnit(geometryEngine.geodesicArea(geo, "square-meters"), "square-meters");
                numOfPointValue = geo.rings[0].length - 1;

                if (areaUnitConvertType == "thailand-unit"){
                    this.divSizeValue.innerHTML = Utility.toAreaUnit(areaValue, areaUnitConvertType);
                }
                else {
                    this.divSizeValue.innerHTML = Utility.toAreaUnit(areaValue, areaUnitConvertType).toFixed(6);
                }
                this.divNumValue.innerHTML = numOfPointValue;
            }
            else {
                if (areaUnitConvertType == "thailand-unit") {
                    this.divSizeValue.innerHTML = "0:0:0";
                }
                else {
                    this.divSizeValue.innerHTML = "0";
                }
                this.divNumValue.innerHTML = 0;

                areaValue = 0;
                numOfPointValue = 0;
            }
            mapManager.onDrawing = "" //ตั้งค่าการวาด custom area

        },
        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });

    return _cls;
});