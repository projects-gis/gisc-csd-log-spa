﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/Favorite.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/WebAPIInterface"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ListItemContainer, ResourceLib, Utility, WebAPIInterface) {

    let wgWebApiCaller = new WebAPIInterface();

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        currentFavoriteData: null,

        constructor: function (params) { },
        postCreate: function () { },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }

            wgWebApiCaller.requestGET({
                url: ApplicationConfig.webAPIConfig.userPreference,
                data: { keys: ApplicationConfig.webAPIConfig.userPreferenceBookmarkKey },
                success: lang.hitch(this, function (data) {

                    let favJson = data[0].value ? JSON.parse(data[0].value) : [];
                    this._displayFavoriteList(favJson);
                }),
                fail: lang.hitch(this, function (e) { console.log("Error : Cannot get favorite", e) })
            });
        },

        _displayFavoriteList: function (favJson) {
            
            this.currentFavoriteData = favJson;

            domConstruct.empty(this.divFavoriteList);

            let ctrl = new ListItemContainer();

            let lstItem = [];

            let keySuffix = (ResourceLib.currentLanguage.toUpperCase().indexOf("EN") > -1) ? "English" : "Local";
            for (var key in favJson) {
                lstItem.push({
                    key: favJson[key]["key"],
                    text: favJson[key]["name" + keySuffix],
                    svg: "./gis-js/images/svg/icon_locationinfo_favorite.svg",
                    itemData: favJson[key],
                    hasDelete:true
                });
            }

            ctrl.createItem(lstItem);
            on(ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));
            on(ctrl, "DeleteClick", lang.hitch(this, "_DeleteClickHandler"));

            domConstruct.place(ctrl.domNode, this.divFavoriteList, "last");
        },

        _ListClickHandler: function (data) {
            this.onAction({ key: "zoom-favorite", param: data.itemData });
        },

        _DeleteClickHandler: function (data) {
            let key = data.key;

            for (let i = 0; i < this.currentFavoriteData.length; i++) {
                if (key == this.currentFavoriteData[i].key) {
                    this.currentFavoriteData.splice(i, 1);
                    break;
                }
            }

            let paramPOST = {
                key: ApplicationConfig.webAPIConfig.userPreferenceBookmarkKey,
                value: JSON.stringify(this.currentFavoriteData)
            }

            wgWebApiCaller.requestPOST({
                url: ApplicationConfig.webAPIConfig.userPreference,
                data: Utility.postify([paramPOST]),
                success: lang.hitch(this, function (postData) {
                    this._displayFavoriteList(this.currentFavoriteData);
                })
            });
        },

        /* On toolpanel close */
        onClose: function () {
            var lyr = ApplicationConfig.mapManager.getLayer("lyr-location-info");
            lyr.clear();
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});