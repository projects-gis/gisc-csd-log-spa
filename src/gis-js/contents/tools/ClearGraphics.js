﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/ClearGraphics.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () {
            this.wgLabel.innerHTML = ResourceLib.text("menu-clear-graphics-tooltip");
            on(this.btnClearMap, "click", lang.hitch(this, function () {
                this._clearMap();
            }));
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            
            
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }
        },

        _clearMap: function () {
            this.onAction({ key: "clear-graphics-measurement" });
        },

        /* On toolpanel close */
        onClose: function () { },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});