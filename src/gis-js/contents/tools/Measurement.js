﻿var toolsbar;
define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/Measurement.html",

    "dojo/dom",
    "dojo/html",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",

    "esri/map",
    "esri/layers/ArcGISTiledMapServiceLayer",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/layers/OpenStreetMapLayer",
    "esri/layers/GraphicsLayer",
    "esri/geometry/Point",
    "esri/symbols/PictureMarkerSymbol",
    "esri/graphic",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/geometry/Polyline",
    "esri/Color",
    "esri/graphicsUtils",
    "esri/geometry/Polygon",
    "esri/geometry/webMercatorUtils",
    "esri/geometry/geometryEngine",
    "esri/SpatialReference",
    "esri/toolbars/draw",

], function (declare, _WidgetBase, _TemplatedMixin, template, dom, html, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ApplicationConfig, ResourceLib, Utility, Map, ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, OpenStreetMapLayer, GraphicsLayer,
    Point, PictureMarkerSymbol, Graphic, SimpleFillSymbol, SimpleLineSymbol, SimpleMarkerSymbol, Polyline, Color, graphicsUtils,
    Polygon, webMercatorUtils, geometryEngine, SpatialReference, Draw) {
    
    /*Point*/
    let pointMarker = new PictureMarkerSymbol(
        ApplicationConfig.measurementSetting.pointSymbol.url,
        ApplicationConfig.measurementSetting.pointSymbol.width,
        ApplicationConfig.measurementSetting.pointSymbol.height);

    pointMarker.setOffset(
        ApplicationConfig.measurementSetting.pointSymbol.offsetX,
        ApplicationConfig.measurementSetting.pointSymbol.offsetY);

    /*Polyline*/
    let polylineMarker = new SimpleLineSymbol(
        SimpleLineSymbol.STYLE_SOLID,
        new Color(ApplicationConfig.measurementSetting.lineSymbol.lineColor),
        ApplicationConfig.measurementSetting.lineSymbol.lineWidth);

    polylineMarker.color.a = ApplicationConfig.measurementSetting.lineSymbol.lineOpacity;

    let polylinePoint = new SimpleMarkerSymbol(
        SimpleMarkerSymbol.STYLE_CIRCLE,
        ApplicationConfig.measurementSetting.lineSymbol.pointSize,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.measurementSetting.lineSymbol.pointColor), 1),
        new Color(ApplicationConfig.measurementSetting.lineSymbol.pointColor));

    let polylineDestinationPoint = new SimpleMarkerSymbol(
        SimpleMarkerSymbol.STYLE_CIRCLE,
        ApplicationConfig.measurementSetting.lineSymbol.destinationPointSize,
        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.measurementSetting.lineSymbol.destinationPointColor), 1),
        new Color(ApplicationConfig.measurementSetting.lineSymbol.destinationPointColor));


    /*Polygon*/
    let polygonMarker = new SimpleFillSymbol(
        SimpleFillSymbol.STYLE_SOLID, 
        new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SOLID, 
            new Color(ApplicationConfig.measurementSetting.polygonSymbol.lineColor), ApplicationConfig.measurementSetting.polygonSymbol.lineWidth),
        new Color(ApplicationConfig.measurementSetting.polygonSymbol.fillColor));

    polygonMarker.color.a = ApplicationConfig.measurementSetting.polygonSymbol.fillOpacity;
    polygonMarker.outline.color.a = ApplicationConfig.measurementSetting.polygonSymbol.lineOpacity;

    let polygonPoint = new SimpleMarkerSymbol(
    SimpleMarkerSymbol.STYLE_CIRCLE,
    ApplicationConfig.measurementSetting.polygonSymbol.pointSize,
    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.measurementSetting.polygonSymbol.pointColor), 1),
    new Color(ApplicationConfig.measurementSetting.polygonSymbol.pointColor));


    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        onEvent: null,
        mapOverHandler: null,
        mapClickHandler: null,
        btnPoloyLineClick :null,

        map: ApplicationConfig.mapObject,
        markMapTitle: ResourceLib.text("wg-measurement-markmap"),
        polyLineLastTitle: ResourceLib.text("wg-measurement-polyline-last"),
        polyLineTotalTitle: ResourceLib.text("wg-measurement-polyline-all"),
        polyLineFirstUnit: ResourceLib.text("wg-measurement-polyline-first-unit"),
        polyLineSecondUnit: ResourceLib.text("wg-measurement-polyline-second-unit"),
        polygonTitle: ResourceLib.text("wg-measurement-polygon"),
        polygonFirstUnit: ResourceLib.text("wg-measurement-polygon-first-unit"),
        polygonSecondUnit: ResourceLib.text("wg-measurement-polygon-second-unit"),
        //geoEngine : new geometryEngine(),

        constructor: function (params) { },
        postCreate: function () {
            this.wgLabel.innerHTML = ResourceLib.text("menu-tools-measurement-header");
            require([
                "dojo/text!./gis-js/images/svg/icon_maptools_poi.svg",
                "dojo/text!./gis-js/images/svg/icon_maptools_add.svg",
                "dojo/text!./gis-js/images/svg/icon_maptools_area.svg"
            ], lang.hitch(this, function (pointSVG, lineSVG, areaSVG) {

                domAttr.set(this.bntMarkMap, "title", ResourceLib.text("wg-measurement-location-measure"));
                domAttr.set(this.btnPolyLine, "title", ResourceLib.text("wg-measurement-distance-measure"));
                domAttr.set(this.btnPolygon, "title", ResourceLib.text("wg-measurement-area-measure"));

                domAttr.set(this.bntMarkMap, "alt", ResourceLib.text("wg-measurement-location-measure"));
                domAttr.set(this.btnPolyLine, "alt", ResourceLib.text("wg-measurement-distance-measure"));
                domAttr.set(this.btnPolygon, "alt", ResourceLib.text("wg-measurement-area-measure"));

                domConstruct.place(domConstruct.toDom(pointSVG), this.bntMarkMap, "last");
                domConstruct.place(domConstruct.toDom(lineSVG), this.btnPolyLine, "last");
                domConstruct.place(domConstruct.toDom(areaSVG), this.btnPolygon, "last");
                
                toolsbar = new Draw(this.map, { showTooltips: false });

                toolsbar.setFillSymbol(polygonMarker)
                toolsbar.setLineSymbol(polylineMarker)

                toolsbar.on("draw-end", lang.hitch(this, this._addToMap));

                on(this.bntMarkMap, "click", lang.hitch(this, function () {

                    domClass.add(this.divBtnPoint, "active");
                    domClass.remove(this.divBtnLine, "active");
                    domClass.remove(this.divBtnPolygon, "active");

                    //console.log("map", this.map);
                    toolsbar.activate("point");
                    this.onAction({ "key": "measurement", "param": "measurement" });
                    this.map.getLayer("lyr-measurement-wg").clear();
                    this.map.setMapCursor("pointer");
                    domConstruct.empty(this.detailMarkMap);
                    domConstruct.empty(this.detailPolyLine);
                    domConstruct.empty(this.detailPolyLineAll);
                    domConstruct.empty(this.detailPolygon);
                    domStyle.set(this.labelMarkMap, "display", "block");
                    domStyle.set(this.labelPolyLine, "display", "none");
                    domStyle.set(this.labelPolygon, "display", "none");
                }));

                on(this.btnPolyLine, "click", lang.hitch(this, function () {

                    domClass.remove(this.divBtnPoint, "active");
                    domClass.add(this.divBtnLine, "active");
                    domClass.remove(this.divBtnPolygon, "active");

                    toolsbar.activate("polyline");

                    this.mapClickHandler = on(this.map, "click", lang.hitch(this, function (evt) {
                        this._calOnMouseMove(evt);
                    }));
                    this.onAction({ "key": "measurement", "param": "measurement" });
                    this.map.getLayer("lyr-measurement-wg").clear();
                    this.map.setMapCursor("pointer");
                    domConstruct.empty(this.detailMarkMap);
                    domConstruct.empty(this.detailPolyLine);
                    domConstruct.empty(this.detailPolyLineAll);
                    domConstruct.empty(this.detailPolygon);
                    domStyle.set(this.labelMarkMap, "display", "none");
                    domStyle.set(this.labelPolyLine, "display", "block");
                    domStyle.set(this.labelPolygon, "display", "none");
                    //domConstruct.empty(this.labelDetail);
                    //this.btnPoloyLineClick.remove();
                }));

                on(this.btnPolygon, "click", lang.hitch(this, function () {

                    domClass.remove(this.divBtnPoint, "active");
                    domClass.remove(this.divBtnLine, "active");
                    domClass.add(this.divBtnPolygon, "active");

                    toolsbar.activate("polygon");
                    this.onAction({ "key": "measurement", "param": "measurement" });
                    this.map.getLayer("lyr-measurement-wg").clear();
                    this.map.setMapCursor("pointer");
                    domConstruct.empty(this.detailMarkMap);
                    domConstruct.empty(this.detailPolyLine);
                    domConstruct.empty(this.detailPolyLineAll);
                    domConstruct.empty(this.detailPolygon);
                    domStyle.set(this.labelMarkMap, "display", "none");
                    domStyle.set(this.labelPolyLine, "display", "none");
                    domStyle.set(this.labelPolygon, "display", "block");
                    //domConstruct.empty(this.labelDetail);
                }));

                html.set(this.tilteMarkMap, this.markMapTitle);
                html.set(this.tiltePolyLine, this.polyLineLastTitle);
                html.set(this.tiltePolyLineAll, this.polyLineTotalTitle);
                html.set(this.tiltePolygon, this.polygonTitle);


            }));
        },

        clearActiveState: function () {

            domClass.remove(this.divBtnPoint, "active");
            domClass.remove(this.divBtnLine, "active");
            domClass.remove(this.divBtnPolygon, "active");

            domStyle.set(this.labelMarkMap, "display", "none");
            domStyle.set(this.labelPolyLine, "display", "none");
            domStyle.set(this.labelPolygon, "display", "none");
        },


        _calOnMouseMove: function (evt) {
            var firstPosition = evt.mapPoint;

            if (this.mapOverHandler) {
                this.mapOverHandler.remove();
                this.mapOverHandler = null;
            }
            this.mapOverHandler = on(this.map, "mouse-move", lang.hitch(this, function (evtHover) {
                var polyLineLast = new Polyline(new SpatialReference({ wkid: 102100 }));
                polyLineLast.addPath([evt.mapPoint, evtHover.mapPoint]);
                //html.set(this.detailPolyLine, evt.mapPoint.x);
                var calMeters = geometryEngine.geodesicLength(polyLineLast, "meters");

                this.detailPolyLine.innerHTML = Utility.toDistanceUnitCompanyFormat(calMeters);

                //var calMiles = geometryEngine.geodesicLength(polyLineLast, "miles");
                //this.detailPolyLine.innerHTML = calKilometers.toFixed(3) + " " + this.polyLineFirstUnit + " ( " + calMiles.toFixed(3) + " " + this.polyLineSecondUnit + " )";

                //calKilometers + " " + polyLineFirstUnit + "," + calMiles + " " + polyLineSecondUnit
                //console.log("hover", calKilometers);
            }));


                  
        },

        _addToMap: function (evt) {

            let lyr = this.map.getLayer("lyr-measurement-wg");

            var symbol;
            var location;
            toolsbar.deactivate();

            switch (evt.geometry.type) {
                case "point": {
                    symbol = pointMarker;

                    location = webMercatorUtils.webMercatorToGeographic(evt.geometry);
                    html.set(this.detailMarkMap, location.y.toFixed(4) + "," + location.x.toFixed(4));

                    var graphic = new Graphic(evt.geometry, symbol);
                    lyr.add(graphic);

                    break;
                }
                case "polyline": {
                    symbol = polylineMarker;

                    var calMetersAll = geometryEngine.geodesicLength(evt.geometry, "meters");
                    //var calMilesAll = geometryEngine.geodesicLength(evt.geometry, "miles");                  

                    //html.set(this.detailPolyLineAll, calKilometersAll.toFixed(3) + " " + this.polyLineFirstUnit + " ( " + calMilesAll.toFixed(3) + " " + this.polyLineSecondUnit + " )");                                                           
                    html.set(this.detailPolyLineAll, Utility.toDistanceUnitCompanyFormat(calMetersAll));                                                           


                    var graphic = new Graphic(evt.geometry, symbol);
                    lyr.add(graphic);

                    latlonLine = webMercatorUtils.webMercatorToGeographic(evt.geometry);

                    array.forEach(latlonLine.paths[0], lang.hitch(this, function (pt) {
                        lyr.add(new Graphic(new Point(pt[0], pt[1]), polylinePoint));
                    }))

                    let lastPt = latlonLine.paths[0][latlonLine.paths[0].length - 1];
                    lyr.add(new Graphic(new Point(lastPt[0], lastPt[1]), polylineDestinationPoint));

                    break;
                }
                default: {
                    symbol = polygonMarker;

                    var areaOnSquareMeters = geometryEngine.geodesicArea(evt.geometry, "square-meters");
                    //var areaOnSquareMiles = geometryEngine.geodesicArea(evt.geometry, "square-miles");

                    //html.set(this.detailPolygon, areaOnSquareKilometers.toFixed(3) + " " + this.polygonFirstUnit + " ( " + areaOnSquareMiles.toFixed(3) + " " + this.polygonSecondUnit + " )");
                    html.set(this.detailPolygon, Utility.toAreaUnitCompanyFormat(areaOnSquareMeters));


                    var graphic = new Graphic(evt.geometry, symbol);
                    lyr.add(graphic);

                    latlonPolygon = webMercatorUtils.webMercatorToGeographic(evt.geometry);
                    
                    array.forEach(latlonPolygon.rings[0], lang.hitch(this, function (pt) {
                        lyr.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                    }))

                    break;
                }
            }

               
            this._disableClick();
        },

        _disableClick: function () {
            this.onAction({ "key": "measurement", "param": "none" });
            this.map.setMapCursor("default");
            //console.log("end", this.btnPoloyLineClick);
            //console.log("end", this.mapClickHandler);
            if (this.mapClickHandler || this.mapClickHandler != null) {
                this.mapClickHandler.remove();                
                this.mapClickHandler = null;
                
            }

            if (this.mapOverHandler || this.mapOverHandler != null) {
                this.mapOverHandler.remove();
                this.mapOverHandler = null;
            }
        },

        clearMeasurement: function () {
            toolsbar.finishDrawing();
            toolsbar.deactivate();
            this._disableClick();
            domConstruct.empty(this.detailMarkMap);
            domConstruct.empty(this.detailPolyLineAll);
            domConstruct.empty(this.detailPolyLine);
            domConstruct.empty(this.detailPolygon);

            this.clearActiveState();

            this.map.getLayer("lyr-measurement-wg").clear();
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }
        },

        /* On toolpanel close */
        onClose: function () {
            toolsbar.finishDrawing();
            toolsbar.deactivate();
            this._disableClick();
            dojo.empty(this.detailMarkMap);
            dojo.empty(this.detailPolyLineAll);
            dojo.empty(this.detailPolyLine);
            dojo.empty(this.detailPolygon);
            
            this.map.getLayer("lyr-measurement-wg").clear();

            domClass.remove(this.divBtnPoint, "active");
            domClass.remove(this.divBtnLine, "active");
            domClass.remove(this.divBtnPolygon, "active");

            domStyle.set(this.labelMarkMap, "display", "none");
            domStyle.set(this.labelPolyLine, "display", "none");
            domStyle.set(this.labelPolygon, "display", "none");
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */
        }
    });
});