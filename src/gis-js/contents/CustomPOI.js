﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/CustomPOI.html",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/controls/ListItemContainer",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/WebAPIInterface",

    "dojo/_base/lang",
    "dojo/on",
    "dojo/dom-construct",
    "BowerModule/jquery/src/jquery"
], function (declare, _WidgetBase, _TemplatedMixin, template, ApplicationConfig, ListItemContainer, ResourceLib, Utility, WebAPIInterface, lang, on, domConstruct, $) {
    let wgWebApiCaller = new WebAPIInterface();

    let POI_GP = "open-poi-gp";
    let POI_TEXT = "open-poi-text";
    let ctrl = null;
    let mainState = false;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,
        constructor: function (params) { },
        postCreate: function () { },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                ctrl = new ListItemContainer({ selectedMode: "toggle" ,type : "checked", useHighlight : false });
                domConstruct.place(ctrl.domNode, this.divItemList, "last");

                if(ApplicationConfig.CustomPOIMainState == undefined) ApplicationConfig.CustomPOIMainState = false;
                if(ApplicationConfig.CustomPOITextState == undefined) ApplicationConfig.CustomPOITextState = false;

                var lstItem = [
                    {
                        key: POI_GP,
                        text: ResourceLib.text('wg-custom-poi-show-gp'),
                        blankIcon: true,
                        selected: ApplicationConfig.CustomPOIMainState
                    },
                    {
                        key: POI_TEXT,
                        text: ResourceLib.text('wg-custom-poi-show-text'),
                        blankIcon: true,
                        selected: ApplicationConfig.CustomPOITextState
                    }
                ];

                ctrl.createItem(lstItem);
                on(ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));
                
            }

            //ctrl.setSelectedIndex(0, ApplicationConfig.CustomPOIMainState);
            //ctrl.setSelectedIndex(1, ApplicationConfig.CustomPOITextState);

        },
        /* On toolpanel close */
        onClose: function () {},

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ },

        _ListClickHandler: function(obj) {
            
            switch(obj["key"]) 
            {
                case POI_GP: {
                    
                    if(obj["isSelected"]) {
                        ctrl.setSelectedIndex(0, true);
                        ctrl.setSelectedIndex(1, true);

                        //open poi
                        //open text
                        this.onContentLoading(true);

                        var def = ApplicationConfig.mapManager.customPoi();
                        ApplicationConfig.mapManager.setCustomPOIText(true);

                        ApplicationConfig.CustomPOIMainState = true
                        ApplicationConfig.CustomPOITextState = true;

                        this.onHasActiveContent(true);

                        def.done(lang.hitch(this, function(){
                            this.onContentLoading(false);
                        })).fail(lang.hitch(this, function(e){
                            this.onContentLoading(false);
                        }));
                    }
                    else {
                        ctrl.setSelectedIndex(0, false);
                        ctrl.setSelectedIndex(1, false);

                        //hide poi
                        //hide text
                        ApplicationConfig.mapManager.disableCustomPoi();
                        ApplicationConfig.mapManager.setCustomPOIText(false);

                        ApplicationConfig.CustomPOIMainState = false
                        ApplicationConfig.CustomPOITextState = false;

                        this.onHasActiveContent(false);
                    }
                    break;
                }

                case POI_TEXT: {
                    
                    if(ApplicationConfig.CustomPOIMainState) {
                        ApplicationConfig.mapManager.setCustomPOIText(obj["isSelected"]);
                        ApplicationConfig.CustomPOITextState = obj["isSelected"];
                    }
                    else
                        ctrl.setSelectedIndex(1, false);
                    break;
                }
            }

            //console.log(obj);
        },

        setAllState: function(flg) {
            ctrl.setSelectedIndex(0, flg);
            ctrl.setSelectedIndex(1, flg);
        },

        /* Send State to ToolsPanel */
        onHasActiveContent: function (hasActive) {/* attach event */ },

        /* Show Loadinng Panel */
        onContentLoading: function (isLoad) { }
    });
});