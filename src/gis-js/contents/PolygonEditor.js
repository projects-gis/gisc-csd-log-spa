﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/PolygonEditor.html",

    "dojo/text!../images/svg/btn_editingtool_back_press.svg",
    "dojo/text!../images/svg/btn_editingtool_cancel_press.svg",
    "dojo/text!../images/svg/btn_editingtool_draw_press.svg",
    "dojo/text!../images/svg/btn_editingtool_next_press.svg",
    "dojo/text!../images/svg/btn_editingtool_stop_press.svg",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/on",
    "dojox/json/query",

    "esri/graphic",
    "esri/Color",
    "esri/symbols/SimpleLineSymbol",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/geometry/geometryEngine",
    "esri/geometry/webMercatorUtils",
    "esri/geometry/Polygon",
    "esri/geometry/Point",

    "esri/toolbars/draw",
    "esri/toolbars/edit",
    
    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, svgBack, svgCancel, svgDraw, svgNext, svgStop,
    dom, domConstruct, domClass, domStyle, domAttr, lang, array, dojoEvent, on, jsonQuery,
    Graphic, Color, SimpleLineSymbol, SimpleFillSymbol, SimpleMarkerSymbol, geometryEngine, webMercatorUtils, Polygon, Point, DrawTools, EditTools,
    ApplicationConfig, ResourceLib, Utility) {

    let sizeUnitText = ResourceLib.text("wg-polygon-editor-size-unit");
    let numOfPointText = ResourceLib.text("wg-polygon-editor-num-of-point");
    let textOutAreaMain = ResourceLib.text("wg-polygon-editor-error-sub-area-out")
    let areaValue = 0;
    let numOfPointValue = 0;
    let areaUnit = null;

    let customOperation = {};
    let fillSymbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color("#1e7358"), 2), new Color("#20af80"));
    let clickHandler = null;
    let map = ApplicationConfig.mapObject;
    let mapManager = ApplicationConfig.mapManager;
    let layerID = ApplicationConfig.commandOptions["enable-draw-polygon"].layer;
    let layerIDMainArea = ApplicationConfig.commandOptions["draw-polygon-sub-area"].layer;
    let layerDrawing = null;
    let drawMode = DrawTools.POLYGON;
    let editMode = EditTools.MOVE | EditTools.EDIT_VERTICES | EditTools.SCALE | EditTools.ROTATE;
    let drawOption = {
        showTooltips: false
    }
    let editOptions = {
        uniformScaling: false,
        allowAddVertices: true,
        allowDeleteVertices: true,
        showTooltips: false
    };

    let startEdit = null;
    let inArea = null;

    let undoIndex = 0;
    let undoStack = [null];

    let editGraphic = null;
    let guid = null;

    let polygonMarker = new SimpleFillSymbol(
        SimpleFillSymbol.STYLE_SOLID,
        new SimpleLineSymbol(
            SimpleLineSymbol.STYLE_SOLID,
            new Color(ApplicationConfig.measurementSetting.polygonSymbol.lineColor), ApplicationConfig.measurementSetting.polygonSymbol.lineWidth),
        new Color(ApplicationConfig.measurementSetting.polygonSymbol.fillColor));

    polygonMarker.color.a = ApplicationConfig.measurementSetting.polygonSymbol.fillOpacity;
    polygonMarker.outline.color.a = ApplicationConfig.measurementSetting.polygonSymbol.lineOpacity;

    let polygonPoint = new SimpleMarkerSymbol(
    SimpleMarkerSymbol.STYLE_CIRCLE,
    ApplicationConfig.measurementSetting.polygonSymbol.pointSize,
    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.measurementSetting.polygonSymbol.pointColor), 1),
    new Color(ApplicationConfig.measurementSetting.polygonSymbol.pointColor));

    var _cls = declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) {
            layerDrawing = mapManager.getLayer(layerID);

            drawMng = new DrawTools(map);
            editMng = new EditTools(map);

            //drawMng.setFillSymbol(polygonMarker);

            on(drawMng, "draw-complete", lang.hitch(this, "_drawPolygonCompleted"));

            on(editMng, "deactivate", lang.hitch(this, "_editPolygonCompleted"));

        },
        postCreate: function () {

            //this.divSizeUnitText.innerHTML = sizeUnitText;
            this.divNumOfPointText.innerHTML = numOfPointText;

            //svgBack, svgCancel, svgDraw, svgNext, svgStop,
            domConstruct.place(domConstruct.toDom(svgBack), this.btnUndo, "last");
            domConstruct.place(domConstruct.toDom(svgNext), this.btnRedo, "last");
            domConstruct.place(domConstruct.toDom(svgCancel), this.btnDeleteGraphics, "last");
            domConstruct.place(domConstruct.toDom(svgDraw), this.btnStartDrawing, "last");
            domConstruct.place(domConstruct.toDom(svgStop), this.btnFinishDrawing, "last");


            on(this.btnStartDrawing, "click", lang.hitch(this, "_startDrawing"));
            on(this.btnFinishDrawing, "click", lang.hitch(this, "_endDrawing"));
            on(this.btnDeleteGraphics, "click", lang.hitch(this, "_clearDrawing"));
            on(this.btnUndo, "click", lang.hitch(this, "_undoEditing"));
            on(this.btnRedo, "click", lang.hitch(this, "_redoEditing"));

            on(this.btnCancel, "click", lang.hitch(this, "_cancelDrawing"));
            on(this.btnSubmit, "click", lang.hitch(this, "_submitDrawing"));
        },

        _cancelDrawing: function () {
            this._clearDrawing();
            guid = null;
            inArea = null;
            this.divAlertText.innerHTML = ""
            //this.onAction({ key: "close-panel" });

            this.onAction({ key: "cancel-drawing-polygon", param: { guid: guid } });
        },

        _submitDrawing: function () {
            var resultGeo = null;
            var centroid = null;
            if (editGraphic) {
                resultGeo = webMercatorUtils.webMercatorToGeographic(new Polygon(editGraphic.geometry)).toJson();
                centroid = webMercatorUtils.webMercatorToGeographic(new Polygon(editGraphic.geometry)).getCentroid();
            }
                

            if (!resultGeo) { return; }

            if(inArea === false){return;}

            let max = resultGeo.rings[0].length;
            let lstLat = [], lstLon = [];
            for (let i = 0; i < max; i++) {
                resultGeo.rings[0][i][0] = Number(resultGeo.rings[0][i][0].toFixed(6));
                resultGeo.rings[0][i][1] = Number(resultGeo.rings[0][i][1].toFixed(6));

                lstLon.push(Number(resultGeo.rings[0][i][0].toFixed(6)));
                lstLat.push(Number(resultGeo.rings[0][i][1].toFixed(6)));
            }

            /* Find Min - Max of Latitude and Longitude */
            lstLon = lstLon.sort(function (a, b) { return a - b });
            lstLat = lstLat.sort(function (a, b) { return a - b });
            this._clearDrawing();

            this.onAction({
                key: "drawing-polygon-complete",
                param: {
                    geometry: resultGeo,
                    area: areaValue,
                    count: numOfPointValue,
                    guid: guid,
                    centralLatitude: centroid.y.toFixed(6),
                    centralLongitude: centroid.x.toFixed(6),
                    minLatitude: lstLat[0],
                    maxLatitude: lstLat[max - 1],
                    minLongitude: lstLon[0],
                    maxLongitude: lstLon[max - 1]
                }
            });
        },

        _undoEditing: function () {

            if (domClass.contains(this.btnUndo, "disabled-btn")) { return;}

            undoIndex -= 1;

            if (undoIndex < 0)
                undoIndex = 0;

            let operation = lang.clone(undoStack[undoIndex]);
            editGraphic = operation;

            if (operation) {
                let gp = new Graphic(operation, polygonMarker)
                layerDrawing.clear();
                layerDrawing.add(gp);

                var latlonGeo = webMercatorUtils.webMercatorToGeographic(gp.geometry);
                this.createPointTable(latlonGeo);
                this.calculate(this.adjustedGeometry(latlonGeo));

                //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
                //    layerDrawing.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                //}));
            }
            else {
                domConstruct.empty(this.divPointCollection);
                //domConstruct.create("div", { "class": "div-point-collection-item" }, this.divPointCollection, "last");

                layerDrawing.clear();
                this.calculate(null);
            }
        },

        _redoEditing: function () {
            if (domClass.contains(this.btnRedo, "disabled-btn")) { return; }

            undoIndex += 1;

            if (undoIndex >= undoStack.length)
                undoIndex = undoStack.length - 1;

            let operation = lang.clone(undoStack[undoIndex]);

            if (operation) {

                editGraphic = operation;

                //console.log(operation, undoStack, undoIndex);

                let gp = new Graphic(operation, polygonMarker)
                layerDrawing.clear();
                layerDrawing.add(gp);

                var latlonGeo = webMercatorUtils.webMercatorToGeographic(gp.geometry);
                this.createPointTable(latlonGeo);
                this.calculate(this.adjustedGeometry(latlonGeo));

                //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
                //    layerDrawing.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                //}));
            }
        },

        _drawPolygonCompleted: function (obj) {
            drawMng.deactivate();
            domClass.remove(this.btnStartDrawing, "disabled-btn");
            domClass.remove(this.btnDeleteGraphics, "disabled-btn");
            domClass.remove(this.btnUndo, "disabled-btn");
            domClass.remove(this.btnRedo, "disabled-btn");
            domClass.add(this.btnFinishDrawing, "disabled-btn");

            var geometry = obj.geometry;
            var latlonGeo = obj.geographicGeometry;

            // var latlonGeo = webMercatorUtils.webMercatorToGeographic(obj.graphic.geometry);
            let layerMainArea = mapManager.getLayer(layerIDMainArea);
            if(layerMainArea.graphics.length > 0){
                let isInArea = []
                layerMainArea.graphics.forEach(function(element) {
                    if(element.geometry.rings){
                        isInArea.push(geometryEngine.contains(latlonGeo, element.geometry));
                        
                    }
                });
                        let param = isInArea.every(function(ev){return ev == true});
                        inArea = param
                        if(!param){
                            this.divAlertText.innerHTML = textOutAreaMain
                        }else{
                            this.divAlertText.innerHTML = ""
                        }
            }

            this.createPointTable(latlonGeo);
            this.calculate(this.adjustedGeometry(obj.geographicGeometry));

            var gp = new Graphic(geometry, polygonMarker);

            editGraphic = gp.toJson();

            undoStack = undoStack.splice(0, undoIndex + 1);
            undoStack.push(editGraphic);
            undoIndex = undoStack.length - 1;

            layerDrawing.clear();
            layerDrawing.add(gp);

            //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
            //    layerDrawing.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
            //}));
        },

        _editPolygonCompleted: function (obj) {

            var latlonGeo = webMercatorUtils.webMercatorToGeographic(obj.graphic.geometry);
            let layerMainArea = mapManager.getLayer(layerIDMainArea);
            if(layerMainArea.graphics.length > 0){
                let isInArea = []
                layerMainArea.graphics.forEach(function(element){
                    if(element.geometry.rings){
                        isInArea.push(geometryEngine.contains(latlonGeo, element.geometry));
                        
                    }
                });
                        let param = isInArea.every(function(ev){return ev == true});
                        inArea = param
                        if(!param){
                            this.divAlertText.innerHTML = textOutAreaMain
                        }else{
                            this.divAlertText.innerHTML = ""
                        }
            }
           
            this.createPointTable(latlonGeo);
            this.calculate(this.adjustedGeometry(latlonGeo));

            editGraphic = lang.clone(obj.graphic.toJson());

            undoStack = undoStack.splice(0, undoIndex + 1);

            undoStack.push(editGraphic);
            undoIndex = undoStack.length - 1;
        },

        _clearDrawing: function () {
            domConstruct.empty(this.divSizeValue);
            domConstruct.empty(this.divNumValue);
            domConstruct.empty(this.divPointCollection);

            if (domClass.contains(this.btnDeleteGraphics, "disabled-btn")) { return; }


            undoStack = [null];
            undoIndex = 0;
            layerDrawing.clear();
            editGraphic = null;
            inArea=null;
        },

        _startDrawing: function () {
            if (domClass.contains(this.btnStartDrawing, "disabled-btn")) { return; }
            mapManager.onDrawing = "enable-draw-polygon" //ตั้งค่าการวาด custom area
            layerDrawing.clear();
            drawMng.activate(drawMode, { showTooltips: false });
            this._clearDrawing();

            domClass.add(this.btnDeleteGraphics, "disabled-btn");
            domClass.add(this.btnUndo, "disabled-btn");
            domClass.add(this.btnRedo, "disabled-btn");
            domClass.add(this.btnStartDrawing, "disabled-btn");
            domClass.add(this.btnFinishDrawing, "disabled-btn");
            //"disabled-btn"
        },

        _endDrawing: function () {
            if (!startEdit) { return; }

            editMng.deactivate();
            domClass.add(this.btnFinishDrawing, "disabled-btn");
            domClass.remove(this.btnDeleteGraphics, "disabled-btn");
            domClass.remove(this.btnUndo, "disabled-btn");
            domClass.remove(this.btnRedo, "disabled-btn");
            domClass.remove(this.btnStartDrawing, "disabled-btn");

            let poly = startEdit.geometry;

            var latlonGeo = webMercatorUtils.webMercatorToGeographic(poly);

            var gp = new Graphic(poly, polygonMarker);

            layerDrawing.clear();
            layerDrawing.add(gp);

            //array.forEach(latlonGeo.rings[0], lang.hitch(this, function (pt) {
            //    layerDrawing.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
            //}));

        },

        _OnGPDrawingClick: function (e) {

            editMng.activate(editMode, e.graphic, editOptions);

            startEdit = e.graphic;

            domClass.add(this.btnStartDrawing, "disabled-btn");
            domClass.add(this.btnDeleteGraphics, "disabled-btn");
            domClass.add(this.btnUndo, "disabled-btn");
            domClass.add(this.btnRedo, "disabled-btn");
            domClass.remove(this.btnFinishDrawing, "disabled-btn");

            dojoEvent.stop(e);
        },

        clearDrawingGraphic: function () {
            layerDrawing.clear();
        },


        adjustedGeometry: function (geoLatlon) {
            let ring = lang.clone(geoLatlon.rings[0]);
            let max = ring.length;

            for (let i = 0; i < max; i++) {
                ring[i][1] = ring[i][1].toFixed(6);
                ring[i][0] = ring[i][0].toFixed(6);
            }

            var polygonJson = {
                "rings": [ring], "spatialReference": { "wkid": 4326 }
            };
            var polygon = new Polygon(polygonJson);
            return webMercatorUtils.geographicToWebMercator(polygon);
        },

        createPointTable: function (geoLatlon) {

            let ring = geoLatlon.rings[0];
            let max = ring.length;

            domConstruct.empty(this.divPointCollection);
            //domConstruct.create("div", { "class": "div-point-collection-item" }, this.divPointCollection, "last");
           
            //not include last(first point)
            for (let i = 0; i < max - 1; i++) {
                domConstruct.create("div", { "class": "div-point-collection-item", innerHTML: ring[i][1].toFixed(6) + ',' + ring[i][0].toFixed(6) }, this.divPointCollection, "last");
            }
        },

        calculate: function (geo) {

            var areaUnitConvertType = "";
            switch (areaUnit.type) {
                case 1:
                    areaUnitConvertType = "square-meters";
                    break;
                case 2:
                    areaUnitConvertType = "square-kilometers";
                    break;
                case 3:
                    areaUnitConvertType = "thailand-unit";
                    break;
            }

            if (geo) {
                //areaValue = Utility.toAreaUnit(geometryEngine.geodesicArea(geo, "square-meters"), "square-meters");


                //console.log("geo",geo);

                areaValue = geometryEngine.geodesicArea(geo, "square-meters");//Utility.toAreaUnit(geometryEngine.geodesicArea(geo, "square-meters"), "square-meters");
                numOfPointValue = geo.rings[0].length - 1;

                if (areaUnitConvertType == "thailand-unit"){
                    this.divSizeValue.innerHTML = Utility.toAreaUnit(areaValue, areaUnitConvertType);
                }
                else {
                    this.divSizeValue.innerHTML = Utility.toAreaUnit(areaValue, areaUnitConvertType).toFixed(6);
                }
                this.divNumValue.innerHTML = numOfPointValue;
            }
            else {
                if (areaUnitConvertType == "thailand-unit") {
                    this.divSizeValue.innerHTML = "0:0:0";
                }
                else {
                    this.divSizeValue.innerHTML = "0";
                }
                this.divNumValue.innerHTML = 0;

                areaValue = 0;
                numOfPointValue = 0;
            }
            mapManager.onDrawing = "" //ตั้งค่าการวาด custom area

        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function (param) {
            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }
            guid = []
            areaUnit = ""
            
            if (param && param["guid"])
                guid = param["guid"];

            if (param && param["areaUnit"]) {
                areaUnit = param["areaUnit"];
                this.divSizeUnitText.innerHTML = areaUnit.text;
            }


            this._clearDrawing();

            if (clickHandler) { clickHandler.remove(); }
            clickHandler = on(layerDrawing, "click", lang.hitch(this, "_OnGPDrawingClick"));

            //layerDrawing.clear();
            mapManager.setMapClickMode("enable-draw-polygon", "enable-draw-polygon");

            if (param && param["point"]) {
                let ringData = param["point"];

                let plg = new Polygon({ rings: ringData, spatialReference: { wkid: 4326 } });
                let merPlg = webMercatorUtils.geographicToWebMercator(plg);
                let gp = new Graphic(merPlg, polygonMarker);

                editGraphic = gp.toJson();

                undoStack = [];
                undoStack.push(editGraphic);

                layerDrawing.add(gp);

                this.createPointTable({ rings: ringData });
                this.calculate(merPlg);

                //array.forEach(merPlg.rings[0], lang.hitch(this, function (pt) {
                //    layerDrawing.add(new Graphic(new Point(pt[0], pt[1]), polygonPoint));
                //}));
            }
        },

        /* On toolpanel close */
        onClose: function (stat) {
            //console.log("close stat", stat);
            startEdit = null;

            layerDrawing.clear();

            drawMng.deactivate();
            editMng.deactivate();

            domClass.add(this.btnRedo, "disabled-btn");
            domClass.add(this.btnFinishDrawing, "disabled-btn");
            domClass.add(this.btnDeleteGraphics, "disabled-btn");
            domClass.add(this.btnUndo, "disabled-btn");
            domClass.remove(this.btnStartDrawing, "disabled-btn");

            // if (clickHandler) { clickHandler.remove(); }
            // mapManager.setMapClickMode("none", "");

            // if (stat)
            //     this.onAction({ key: "close-drawing-polygon", param: { guid: guid } });
        },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });

    return _cls;
});