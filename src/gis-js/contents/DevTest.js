﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/DevTest.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () {

            //divTestList
            let ctrl = new ListItemContainer();

            domConstruct.place(ctrl.domNode, this.divTestList, "last");

            ctrl.createItem([
                { key: "A1", text: "Test Item 1", icon: "./gis-js/images/menu/basemap.png" },
                { key: "A2", text: "Test Item 2", icon: "./gis-js/images/menu/basemap.png" },
                { key: "A3", text: "Test Item 3", icon: "./gis-js/images/menu/basemap.png" }
            ]);
        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;
            }
        },

        /* On toolpanel close */
        onClose: function () { },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});