﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/Basemap.html",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",

    "GOTModule/controls/ListItemContainer",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility"
], function (declare, _WidgetBase, _TemplatedMixin, template, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery,
    ListItemContainer, ApplicationConfig, ResourceLib, Utility) {

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        constructor: function (params) { },
        postCreate: function () {

        },

        /* Event from ToolPanel */
        /* Flag : is First Load this widget ? */
        isFirstLoad: true,
        /* On widget display on screen */
        onOpen: function () {

            //First Load Operation
            if (this.isFirstLoad) {
                this.isFirstLoad = false;

                let firstKey = "";
                let ctrl = new ListItemContainer({ selectedMode: "one" });
                domConstruct.place(ctrl.domNode, this.dibBasemapList, "last");

                let lstBasemapKey = ApplicationConfig.mapProviderData["basemap"]["keys"];
                let lstItem = [];
                let cnt = 0;
                for (var key in lstBasemapKey) {
                    lstItem.push({
                        key: lstBasemapKey[key],
                        text: ResourceLib.text("wg-basemap-" + lstBasemapKey[key]),
                        //icon: ApplicationConfig.basemapPicConfig[lstBasemapKey[key]]
                        svg: ApplicationConfig.basemapPicConfig[lstBasemapKey[key]] || './gis-js/images/svg/icon_basemap_streetmap_worldstreetmap.svg'
                    });

                    if (cnt == 0)
                        firstKey = lstBasemapKey[key];

                    cnt++;
                }

                ctrl.createItem(lstItem);
                var setSelected = false;
                for (var indItem =0;indItem < lstItem.length; indItem++) {
                    if (lstItem[indItem]["key"] == gmapBasemapItem) {
                        ctrl.setSelectedIndex(indItem);
                        setSelected = true;
                        break;
                    }
                }
                if (!setSelected) ctrl.setSelectedIndex(0);
                on(ctrl, "ItemClick", lang.hitch(this, "_ListClickHandler"));
            }
        },

        _ListClickHandler: function (obj) {
            this.onAction({ key: "change-basemap", param: { basemap: obj.key } });
        },

        /* On toolpanel close */
        onClose: function () { },

        /* Send Action to other control */
        onAction: function (obj) { /* attach event */ }
    });
});