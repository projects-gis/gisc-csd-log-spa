﻿require([
    "dojo/on",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/dom-construct",
    "dojo/_base/lang",
    "dojox/json/query",
    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/MISInterface",
    "GOTModule/utils/WebAPIInterface",
    "GOTModule/utils/Utility",
    "GOTModule/controls/WorkingMapManager",

    "dojo/domReady!"
], function (on, dom, domClass, domConstruct, lang, jsonQuery, ApplicationConfig, ResourceLib, MISInterface, WebAPIInterface, Utility, MapManager) {
    var startupData = null;
    /* initialize web api interface */
    var wgWebApiCaller = new WebAPIInterface();
    var mapManager = null;

    /* MISInterface RecieveMessage event */
    on(MISInterface, "RecieveMessage", onRecievePostMessage);

    function onRecievePostMessage(postData) {
        let isPass = true;
        let msg = null;

        Utility.debugLog("WM onRecievePostMessage", postData);

        try {
            switch (postData["command"]) {
                case "wm-set-startup-resources": { 
                    initialize(postData["param"]);
                    break; 
                }
                case "wm-set-create-polygon-from-line": {
                    console.log("gis working",postData["param"]);
                    mapManager.drawPolygonFromPolyLine(postData["param"]);
                    break;
                }
                case "wm-get-result-polygon": {
                    var resultPolygon = mapManager.getResultPolygon();

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "wm-get-result-polygon-completed",
                        status: true,
                        result: resultPolygon
                    }));

                    break;
                }
                case "wm-clear-graphics": {
                    mapManager.clearGraphics();
                    break;
                }
                case "wm-create-route-from-line": {
                    mapManager.drawRoute(postData["param"]);

                    MISInterface.sendMessage(MISInterface.createResponseResult({
                        command: "wm-create-route-from-line",
                        status: true
                    }));
                    break;
                }
                default: { 
                    break; 
                }
            }
        }
        catch (ex) {

            let message = "invalid parameter";

            if (ApplicationConfig.debugLog) { message = "command error : " + ex.message };

            MISInterface.sendMessage(MISInterface.createResponseResult({
                command: postData["command"],
                status: false,
                message: message
            }));
        }
    }

    function loadTheme() {
        /*=== Loading Theme ===*/

        /* Main Theme Class*/
        var mainTheme = ApplicationConfig.userInfo.info["_companyThemeName"];

        /* Add Class Theme to Body */
        domClass.add(document.body, mainTheme);

        /* Draw Loading Screen */
        var loadingLogoUrl = Utility.generateThemeLogoPath(mainTheme);

        require(["dojo/text!" + loadingLogoUrl], function(svgLogo){
            //domConstruct.place(domConstruct.toDom(svgLogo), dom.byId("divLoadingLogo"), "last");
        });
    }

    function initialize(startupParam) {
        if (startupData) { return; }
        startupData = startupParam;

        /* set app language & resources */
        ResourceLib.loadResource(startupParam.i18nResources);
        ResourceLib.setLanguage(startupParam.userInfo.appLanguage);

        /* Setting startup config */
        ApplicationConfig.userPermission = startupParam.permission;


        if (ApplicationConfig.userInfo) {
            lang.mixin(ApplicationConfig.userInfo, startupParam.userInfo);
        }
        else {
            ApplicationConfig.userInfo = startupParam.userInfo;
        }

        /* Loading Theme */
        loadTheme();
        
        wgWebApiCaller.requestPOST({
            url: ApplicationConfig.webAPIConfig.getMapProvider,
            data: {
                "companyId": ApplicationConfig.userInfo.info._userSession.currentCompanyId
            },
            success: function (data) {
                Utility.debugLog("WM Get Map Provider Success", data);
                ApplicationConfig.setMapProvider(data);
            },
            error: function (err) {
                Utility.debugLog("WM Get Map Provider Error", err);
                ApplicationConfig.setMapProvider([]);
            },
            finished: function () {
                Utility.debugLog("WM Get Map Provider Finished Job");
                Utility.debugLog("WM Map provider data", ApplicationConfig.mapProviderData);

                mapManager = new MapManager({ domId: "divWorkingMap" });
                mapManager.createMap();               

                /* Send message back */
                MISInterface.sendMessage(MISInterface.createResponseResult({
                    command: "wm-set-startup-resources",
                    status: true
                }));
            }
        });
    }

    MISInterface.sendMessage(MISInterface.createResponseResult({
        command: "wm-map-api-ready",
        status: true
    }));
});