﻿var gmapA = null; // use in GoogleMapLayer.js
var gmapInstance = null;
var gmapBasemapItem = "GGGG";
var dojoConfig = (function () {
    var baseLocation = window.location.origin;
    var loc = window.location.pathname;
    var dir = loc.substring(0, loc.lastIndexOf('/'));
    var lang = this.getCookie("i18next")
    return {
        parseOnLoad: true,
        async: true,
        packages: [
            {
                name: "GOTModule",
                location: baseLocation + dir + "/gis-js"
            },
            {
                name: "BowerModule",
                location: baseLocation + dir + "/gis-js/libs"
            },
            {
                name: "vendors",
                location: baseLocation + dir + "/vendors"
            },
            {
                name: "GOTModuleWidget",
                location: baseLocation + "/gis-js"
            }
        ],
        locale: lang //"th-th"//"en-us"
    };
})();

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }