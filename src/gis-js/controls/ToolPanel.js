﻿define([
    "dojo/_base/declare",
    "dijit/_WidgetBase",
    "dijit/_TemplatedMixin",

    "dojo/text!./templates/ToolPanel.html",
    "dojo/text!../images/svg/icon_panel_close.svg",
    "dojo/text!../images/svg/btn_editingtool_next_press.svg",
    "dojo/text!../images/svg/btn_editingtool_back_press.svg",
    "dojo/text!../images/svg/action-pin.svg",

    "dojo/dom",
    "dojo/dom-construct",
    "dojo/dom-class",
    "dojo/dom-style",
    "dojo/dom-attr",
    "dojo/_base/lang",
    "dojo/_base/array",
    "dojo/on",
    "dojox/json/query",
    "dojo/query",

    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "BowerModule/jquery/src/jquery"
], function (declare, _WidgetBase, _TemplatedMixin, template, closeSVG, hideSVG, showSVG, actionPinSVG, dom, domConstruct, domClass, domStyle, domAttr, lang, array, on, jsonQuery, nodeQuery,
    ApplicationConfig, ResourceLib, Utility, $) {

    var divPlaceNode = "";
    var lastOtherContent = "";
    var otherDiv = null;
    var isOpen = false;
    var isPin = false;

    return declare([_WidgetBase, _TemplatedMixin], {
        templateString: template,

        lstWidget: null,
        otherContentKey: "wg-other-content",
        currentWidget: null,

        isOpen: function () {
            return isOpen;
        },

        constructor: function (params) {
            divPlaceNode = params.domNode;
            this.lstWidget = {};
            this.panel = {};
        },

        postCreate: function () {
            this.inherited(arguments);
            domConstruct.place(this.domNode, divPlaceNode, "last");
            domConstruct.place(domConstruct.toDom(closeSVG), this.btnClose, "last");
            domConstruct.place(domConstruct.toDom(hideSVG), this.btnHide, "last");
            domConstruct.place(domConstruct.toDom(actionPinSVG), this.btnPin, "last");

            /* Create Menu from menu config */
            let cntGroupMenuMain = ApplicationConfig.menuConfig.length;
            let cntGroupMenuMainExcludeViewOnMap = ApplicationConfig.menuConfig.length - 1;

            //let divFirstGap = domConstruct.create("div", { class: "wg-toolpanel-menu-space" }, this.divMenuList, "last");
            for (let indGrp = 0; indGrp < cntGroupMenuMain; indGrp++) {

                let grpMenu = ApplicationConfig.menuConfig[indGrp];
                let cntGroupMenu = grpMenu.length;
                let divMenu = null;

                for (let indItm = 0; indItm < cntGroupMenu; indItm++) {

                    let menuItem = grpMenu[indItm];

                    let className = (menuItem.key == "shipment-legend") ? "wg-toolpanel-menu-item shipment-legend" : "wg-toolpanel-menu-item";
                    divMenu = domConstruct.create("div", { class: className, "data-key": menuItem["key"] }, this.divMenuList, "last");
    
                    require(["dojo/text!" + menuItem["svg"]], lang.hitch(this, function (divMenuMth, objMenuItem, svg) {
      
                        let svgTag = domConstruct.toDom(svg);
                        domConstruct.place(svgTag, divMenuMth, "last");


                        //console.log("menu-" + objMenuItem["key"] + "-tooltip", ResourceLib.text("menu-" + objMenuItem["key"] + "-tooltip"));

                        //let imgIcon = domConstruct.create("img", { src: objMenuItem["icon"], class: "wg-toolpanel-panel-icon" }, divMenuMth, "last");

                        let divTooltip = domConstruct.create("div", {
                            class: "wg-toolpanel-menu-item-tooltip",
                            innerHTML: ResourceLib.text("menu-" + objMenuItem["key"] + "-tooltip")
                        }, divMenuMth, "last");
                    }, divMenu, menuItem));

                    if (menuItem["key"] == "basemap") { domClass.add(divMenu, "content-active"); }
                    if (menuItem["key"] == "vehicle") { domClass.add(divMenu, "content-active"); }

                    if (menuItem["cls"]) {
                        require([menuItem["cls"]], lang.hitch(this, function (clsKey, nd, wgName, wgResourceKey, wgClass) {
                            let wg = new wgClass();
                            wg.widgetKey = clsKey;
                            wg.widgetName = wgName;
                            wg.contentHeaderText = ResourceLib.text(wgResourceKey);
                            this.lstWidget[clsKey] = wg;
                            on(wg, "Action", lang.hitch(this, "onContentAction"));

                            if (wg["onHasActiveContent"]) {
                                on(wg, "HasActiveContent", lang.hitch(this, "_onContentActive", nd));
                            }

                            if(wg["onContentLoading"]) {
                                on(wg, "ContentLoading", lang.hitch(this, "_isContentLoading"));
                            }

                        }, menuItem["key"], divMenu, menuItem["name"], menuItem["resourceKey"]));
                    }

                    on(divMenu, "click", lang.hitch(this, "_onMenuClick", menuItem, divMenu));
                }

                // ignore view on map
                if(indGrp != cntGroupMenuMainExcludeViewOnMap){
                    if (indGrp + 1 < cntGroupMenuMainExcludeViewOnMap)
                        domConstruct.create("div", { class: "wg-toolpanel-menu-gap", "data-key": "other-content" }, this.divMenuList, "last");
                    else {
                        domClass.add(divMenu, "last-item");
                    }
                }
            }

            /* Create Other Content */
            let cntOther = ApplicationConfig.otherContents.length;
            for (let indItem = 0; indItem < cntOther; indItem++) {
                let menuItem = ApplicationConfig.otherContents[indItem];

                if (menuItem["cls"]) {
                    require([menuItem["cls"]], lang.hitch(this, function (clsKey, wgName, wgResourceKey, wgClass) {
                        let wg = new wgClass();
                        wg.widgetKey = clsKey;
                        wg.widgetName = wgName;
                        wg.contentHeaderText = ResourceLib.text(wgResourceKey)

                        on(wg, "Action", lang.hitch(this, "onContentAction"));

                        this.lstWidget[clsKey] = wg;

                        if(typeof wg.onContentLoading !== "undefined") {
                            on(wg, "ContentLoading", lang.hitch(this, "_isContentLoading"));
                        }

                    }, menuItem["key"], menuItem["name"], menuItem["resourceKey"]));
                }
            }

            //otherDiv = domConstruct.create("div", { class: "wg-toolpanel-menu-item other-content" }, this.divMenuList, "last");
            //domConstruct.place(domConstruct.toDom(showSVG), otherDiv, "last")
            //let imgIcon = domConstruct.create("img", { src: "./gis-js/images/menu/favorite.png", class: "wg-toolpanel-panel-icon" }, otherDiv, "last");

            //on(otherDiv, "click", lang.hitch(this, "_otherMenuClick"));

            Utility.debugLog("available content", this.lstWidget);

            on(this.btnClose, "click", lang.hitch(this, "closePanel", true));
            on(this.btnHide, "click", lang.hitch(this, "hidePanel"));
            on(this.btnPin, "click", lang.hitch(this, "pinPanel", false));
            //on(otherDiv, "click", lang.hitch(this, "showPanel"));
            //on(this.divHiddingWidget, "click", lang.hitch(this, "showPanel"));

            on(this.divHiddingWidget, "mouseover", lang.hitch(this, "showPanel"));


            // on(this.divPanelContent, "mouseout", lang.hitch(this, function(e) {
            //     var tg = e.target;
            //     var next = null;

            //     if(e.toElement)
            //     {
            //         next = e.toElement;
            //     }
            //     else if(e.relatedTarget)
            //     {
            //         next = e.relatedTarget;
            //     }

            //     if(next) {
            //         var isInMainElem = $(next).parents('.wg-toolpanel-panel').length == 1;
                    
            //         if(!isInMainElem && !isPin)
            //         {
            //             this.unpinPanel();
            //             this.hidePanel();
            //         }
            //     }
            // }));

            //set div id:divToolPanel height auto when hide
            //domStyle.set(dom.byId("divToolPanel"), "height", "auto");
            domClass.remove(dom.byId("divToolPanel"), "open");
        },

        pinPanel: function(forcePin) {
            if(typeof forcePin != "boolean")
                forcePin = false;

            if(forcePin)
            {
                $(this.btnPin).addClass("active-pin");
                isPin = true;
            }
            else
            {
                $(this.btnPin).toggleClass("active-pin");
                isPin = $(this.btnPin).hasClass("active-pin");
            }
        },

        unpinPanel:function() {
            $(this.btnPin).removeClass("active-pin");
            isPin = false;
        },

        isPin: function() {
            return isPin;
        },

        clearContent: function (key) {
            domConstruct.empty(this.divWgContent);
        },

        invokeContentFunction: function (key, func, param) {
            if (key && this.lstWidget[key]) {
                let wg = this.lstWidget[key];

                if (wg && wg[func] && (typeof (wg[func]) === "function")) {
                    wg[func](param);
                }
            }
        },

        openPanel: function (key, param) {

            //console.log(this.lstWidget[key].domNode);

            if (key && this.lstWidget[key]) {

                if(key== "tools") { this.pinPanel(true); };

                isOpen = true;
                let wg = this.lstWidget[key];

                if (this.currentWidget && this.currentWidget.widgetKey && this.currentWidget.widgetKey == key) {
                    //console.log(this.currentWidget.widgetKey, key);

                    //let headerText = this.currentWidget;
                    //nodeQuery(".global-widget-header-text", this.currentWidget.domNode).innerHTML = headerText;

                    if (this.currentWidget.onOpen)
                        this.currentWidget.onOpen(param);
                }
                else {
                    if (this.currentWidget)
                    { 
                        this.currentWidget.onClose(); 
                    }

     
                    //domConstruct.empty(this.divWgContent); 
                    // => IE มัน Empty widget domNode ด้วย ทำให้กด widget เดิมครั้งที่ 2 แล้วได้ Panel เปล่า
                    // => แก้โดยใช้ removeChild แทน

                    if (this.currentWidget)
                        this.divWgContent.removeChild(this.currentWidget.domNode);

                    domConstruct.place(wg.domNode, this.divWgContent, "last");

                    this.currentWidget = wg;

                    //set header with resource key
                    //console.log(wg.contentHeaderText);
                    let headerText = wg.contentHeaderText;
                    nodeQuery(".global-widget-header-text", wg.domNode).map(lang.hitch(this, function(nd) {
                        nd.innerHTML = headerText;
                    }))

                    nodeQuery(".content-open", this.divMenuList).map(lang.hitch(this, function (nd) {
                        domClass.remove(nd, "content-open");
                    }));
                    
                    //set style
                    var nodeList = nodeQuery("[data-key='" + key + "']", this.divMenuList);
                    if (nodeList.length == 1) {
                        let node = nodeList[0];

                        domClass.add(node, "content-open");
                    }
                    else {
                        //var otherNode = nodeQuery(".wg-toolpanel-menu-item.other-content", this.divMenuList)[0];
                        //lastOtherContent = key;

                        //domClass.add(otherNode, "content-open");
                        //domStyle.set(otherNode, "display", "block");
                    }

                    //this.divHideName.innerHTML = wg.widgetName;
                    this.divHideName.innerHTML = headerText;

                    if (wg.onOpen)
                        wg.onOpen(param);


                    //domClass.remove(otherDiv, "show");
                }

                //set div id:divToolPanel height auto when hide
                //domStyle.set(dom.byId("divToolPanel"), "height", "calc(100%)");
                //domStyle.set(this.divPanelContent, "display", "block");
                //domStyle.set(this.divMenuList, "right", "285px");

                domClass.add(dom.byId("divToolPanel"), "open");
                domClass.add(this.divPanelContent, "open");
                domClass.add(this.divMenuList, "open");

                domClass.remove(this.divMainWg, "open");

            }

            if (this.onContentOpen)
                this.onContentOpen();
        },

        closePanel: function (stat) {

            //set div id:divToolPanel height auto when hide

            //domStyle.set(dom.byId("divToolPanel"), "height", "auto");
            //domStyle.set(this.divPanelContent, "display", "none");
            //domStyle.set(this.divMenuList, "right", "0px");

            domClass.remove(dom.byId("divToolPanel"), "open");
            domClass.remove(this.divPanelContent, "open");
            domClass.remove(this.divMenuList, "open");

            domClass.remove(this.divMainWg, "open");

            if (this.currentWidget && this.currentWidget.onClose)
                this.currentWidget.onClose(stat);

            nodeQuery(".content-open", this.divMenuList).map(lang.hitch(this, function (nd) {
                domClass.remove(nd, "content-open");
            }));

            isOpen = false;
        },

        hidePanel: function () {
            //domClass.add(otherDiv, "show");
            domClass.remove(dom.byId("divToolPanel"), "open");
            domClass.remove(this.divPanelContent, "open");
            domClass.remove(this.divMenuList, "open");

            domClass.add(this.divMainWg, "open");


            if (this.currentWidget && this.currentWidget.onHidePanel)
                this.currentWidget.onHidePanel();
        },

        showPanel: function () {
            //domClass.remove(otherDiv, "show");
            domClass.add(dom.byId("divToolPanel"), "open");
            domClass.add(this.divPanelContent, "open");
            domClass.add(this.divMenuList, "open");

            domClass.remove(this.divMainWg, "open");

            if (this.currentWidget && this.currentWidget.onShowPanel)
                this.currentWidget.onShowPanel();
        },

        _isContentLoading: function(flg) {

           // console.log("check load", flg);

            if(flg)
                domStyle.set(this.divPanelLoading, "display", "block");
            else
                domStyle.set(this.divPanelLoading, "display", "none");
        },

        _onMenuClick: function (obj, div) {
            let key = obj.key;
            let data = (this.panel) ? this.panel.data : {};
            let propMenu = (this.panel) ? this.panel.menu : {};
            if(key == "clear-graphics"){
                key = (propMenu && propMenu.disableClearGraphic) ? null : "clear-graphics";
            }
            
            switch (obj.type) {
                case "content": {
                    if (this.lstWidget[key]) {
                        this.openPanel(key, data);
                    }
                    break;
                }
                case "action": {
                    this.onActionClick(key);
                    break;
                }
                case "toggle": {
                    this.onActionClick(key);
                    domClass.toggle(div, "content-active");
                    break;
                }
                default: break;
            }
        },

        _otherMenuClick: function () {
            let wg = this.lstWidget[lastOtherContent];
            this.currentWidget = wg;

            if (wg.onOpen)
                wg.onOpen();

            domConstruct.empty(this.divWgContent);
            domConstruct.place(wg.domNode, this.divWgContent, "last");

            var otherNode = nodeQuery(".wg-toolpanel-menu-item.other-content", this.divMenuList)[0];
            //lastOtherContent = key;

            domClass.add(otherNode, "content-open");
            domStyle.set(otherNode, "display", "block");

            this.openPanel();
        },

        _onContentActive: function (node, hasActive) {
            if (hasActive > 0) {
                domClass.add(node, "content-active");
            }
            else {
                domClass.remove(node, "content-active");
            }
        },

        _onShowMenu: function (params) {
            this.panel = (params) ? params : {};
            let propMenu = (params) ? params.menu : {};
            let key = propMenu.key;
            let showMenu = propMenu.show;
            nodeQuery("div[data-key='" + key + "']", this.divMenuList).map(lang.hitch(this, function(nd) {
                if(showMenu){
                    domClass.add(nd, "content-open");
                    $(nd).show();
                }else{
                    domClass.remove(nd, "content-open");
                    $(nd).hide();
                }
            }));
        },

        onMapLoad: function (e) {
            //Utility.debugLog("On Map Load", this.lstWidget);
            ////onMapLoad
            //for (var key in this.lstWidget) {
            //    console.log(key);
            //    if (this.lstWidget[key].onMapLoad)
            //        this.lstWidget[key].onMapLoad(e);
            //}
        },

        onHidePanel: function(){/*attach event*/},
        onShowPanel: function(){/*attach event*/ },
        onContentOpen: function () { /*attach event*/ },
        onActionClick: function (key) { /*attach event*/ },
        onContentAction: function (param) { /*attach event*/ }
    });
});