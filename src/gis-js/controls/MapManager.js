﻿define(["dojo/_base/declare",
    "dijit/_WidgetBase",

    "dojo/_base/array",
    "dojo/_base/lang",
    "dojo/_base/event",

    "dojo/dom",
    "dojo/dom-attr",
    "dojo/dom-construct",
    "dojo/dom-style",
    "dojo/dom-class",
    "dojo/on",
    "dojox/json/query",
    "dojo/query",

    "esri/map",
    "esri/toolbars/draw",
    "esri/dijit/Scalebar",
    "esri/layers/ArcGISTiledMapServiceLayer",
    "esri/layers/ArcGISDynamicMapServiceLayer",
    "esri/layers/OpenStreetMapLayer",
    "esri/layers/GraphicsLayer",
    "esri/layers/FeatureLayer",
    "esri/geometry/Point",
    "esri/geometry/Multipoint",
    "esri/symbols/PictureMarkerSymbol",
    "esri/graphic",
    "esri/symbols/SimpleFillSymbol",
    "esri/symbols/SimpleLineSymbol",
    "esri/geometry/Polyline",
    "esri/Color",
    "esri/graphicsUtils",
    "esri/geometry/Extent",
    "esri/geometry/Polygon",
    "esri/geometry/ScreenPoint",
    "esri/geometry/webMercatorUtils",
    "esri/geometry/screenUtils",
    "esri/geometry/geometryEngine",
    "esri/symbols/SimpleMarkerSymbol",
    "esri/symbols/TextSymbol",
    "esri/SpatialReference",
    "esri/symbols/Font",
    "esri/dijit/InfoWindow",
    "esri/renderers/ClassBreaksRenderer",
    "esri/dijit/PopupTemplate",
    "esri/layers/WebTiledLayer",
    "GOTModule/controls/ClusterPointLayer",
    "GOTModule/controls/GoogleMapLayer",
    "GOTModule/controls/ContextMenu",
    "GOTModule/utils/ResourceLib",
    "GOTModule/utils/Utility",
    "GOTModule/utils/VehicleTracking",
    "GOTModule/config/ApplicationConfig",
    "GOTModule/utils/WebAPIInterface",
    "BowerModule/jquery/src/jquery"
], function (declare, _WidgetBase, array, lang, dojoEvent, dom, domAttr, domConstruct, domStyle, domClass, on, jsonQuery, nodeQuery,
    Map,Draw, Scalebar, ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, OpenStreetMapLayer, GraphicsLayer, FeatureLayer,
    Point, Multipoint, PictureMarkerSymbol, Graphic, SimpleFillSymbol, SimpleLineSymbol, Polyline, Color, graphicsUtils,
    Extent, Polygon, ScreenPoint, webMercatorUtils, screenUtils, geometryEngine, SimpleMarkerSymbol, TextSymbol, SpatialReference, Font, InfoWindow, ClassBreaksRenderer, PopupTemplate,
    WebTiledLayer,

    ClusterPointLayer, GoogleMapLayer, ContextMenu, ResourceLib, Utility, VehicleTracking, ApplicationConfig, WebAPIInterface, $) {
    var directionConst = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"];

    var domNode = null;
    var map = null;
    var mapProviderData = null;
    var basemapGrp = null;
    var lstLayer = null;
    var arrAllLayerAvailable = null;

    var clusterLayer = null;
    var nonClusterLayer = null;

    var customPOIFilterExtent = null;

    var poiLabelFlg = true;
    var areaLabelFlg = true;

    var isDisplayingInfoLayer = [];

    var lstAllBasemapKey = null;
    var wgVehicleTracking = null;
    var wgVehiclePlayback = null;
    var commandClickmap = "";
    var clickmapState = "none";
    var guid = null;
    var wgWebApiCaller = new WebAPIInterface();
    var customPoiHandle = null;
    var customAreaHandle = null;
    var currentBasemapActive = null;
    var mapMode = "normal"; // print
    var mapClickSymbol = new PictureMarkerSymbol(ApplicationConfig.clickMapPin.url, ApplicationConfig.clickMapPin.width, ApplicationConfig.clickMapPin.height);
    mapClickSymbol.setOffset(ApplicationConfig.clickMapPin.offsetX, ApplicationConfig.clickMapPin.offsetY);

    var locationInfoMapSymbol = new PictureMarkerSymbol(ApplicationConfig.locationInfoPin.url, ApplicationConfig.locationInfoPin.width, ApplicationConfig.locationInfoPin.height);
    locationInfoMapSymbol.setOffset(ApplicationConfig.locationInfoPin.offsetX, ApplicationConfig.locationInfoPin.offsetY);

    var limitDrawTrackVehicle = 300;
    /*
        none => default state,
        enable-click-map => click map to get latlon from command : enable-click-map
    */

    var getDirectionIndex = function (start, next) {
        var rotateDegree = (Math.atan2(Number(next[0]) - Number(start[0]), Number(next[1]) - Number(start[1])) * (180 / Math.PI)).toFixed(1);

        var directionIndex = (Math.round((rotateDegree - 22.5) / 45));

        if (directionIndex < 0)
            directionIndex += 8;

        return directionIndex;
    }

    var context = null;
    var lstLyrResize = [];

    return declare([_WidgetBase], {

        args: null,

        directionConst: ["N", "NE", "E", "SE", "S", "SW", "W", "NW"],

        constructor: function (args) {
            this.args = args;
        },

        initializeMap: function () {
            if (!this.args.domId)
                throw new Error("domNode is not defined.");

            if (this.args.mapMode)
                mapMode = this.args.mapMode;

            var config = this.args.config || ApplicationConfig.defaultMapInitialize;

            domId = this.args.domId;
            // console.log(!Utility.isMobileDevice());
            if (config) {
                map = new Map(domId, {
                    autoResize: true,
                    nav: false,
                    logo: false,
                    slider: false,
                    optimizePanAnimation: false,
                    showAttribution: false,
                    smartNavigation: false,
                    isDoubleClickZoom: !Utility.isMobileDevice(),
                    center: config.center,
                    zoom: config.zoomz
                });
            } else {
                map = new Map(domId, {
                    autoResize: true,
                    nav: false,
                    logo: false,
                    slider: false,
                    optimizePanAnimation: false,
                    showAttribution: false,
                    smartNavigation: false,
                    isDoubleClickZoom: !Utility.isMobileDevice(),
                    extent: new Extent(ApplicationConfig.defaultMapExtent)
                });
            }

            //ตังค่า MAP
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (pos) {
                    map.centerAt([pos.coords.longitude, pos.coords.latitude])
                })
            }


            var scalebar = new Scalebar({
                map: map,
                attachTo: "bottom-right",
                scalebarUnit: "dual"
            });

            on(map, "load", lang.hitch(this, function (e) {
                if (mapMode == "print" || mapMode == "print-route") {
                    //map.disableMapNavigation();
                    //map.disablePan();
                    //map.disableScrollWheelZoom();
                    //map.disableShiftDoubleClickZoom();
                }

                Utility.debugLog("Map Loaded");
                /* for use in map tools widget [ measurement / print map / delete graphic ] */
                ApplicationConfig.mapObject = e.map;


                on(map.infoWindow, "hide", lang.hitch(this, function () {
                    isDisplayingInfoLayer = [];
                }));

                this.fireTrigger({
                    command: "first-map-loaded",
                    param: e
                });
            }));

            if (mapMode != "print" && mapMode != "print-route") {

                wgVehicleTracking = new VehicleTracking();

                on(wgVehicleTracking, "TrackLocationChange", lang.hitch(this, "_onTrackLocationChange"));

                mapProviderData = ApplicationConfig["mapProviderData"];
                arrAllLayerAvailable = [];
                basemapGrp = {};
                lstLayer = [];
                lstAllBasemapKey = [];

                var firstKey = null;
                var lstDependLayer = [];
                var baseMapData = mapProviderData["basemap"];
                var layerData = mapProviderData["layer"];

                /* create basemap */
                var fixLoadMap = (window.location.origin.indexOf("localhost:8080") > -1);
                if (fixLoadMap) {
                    var id = "lyr-basemap-dev-test";
                    //https://services.arcgisonline.com/arcgis/rest/services/World_Street_Map/MapServer
                    //https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer
                    //"http://10.254.111.231/arcgis/rest/services/PEA_VECTOR_CACHE/MapServer"
                    //http://10.254.111.231/arcgis/rest/services/Cache/MapServer //PEA Map Cache
                    
                    var bLayer = new ArcGISTiledMapServiceLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer", {
                        id: id,
                        visible: true,
                        resampling: true
                    });

                    // var bLayer = new WebTiledLayer("https://mts1.google.com/vt/lyrs=s@186112443&hl=x-local&src=app&x={col}&y={row}&z={level}&s=Galile", {
                    //     copyright: "Google Maps",
                    //     id: id
                    // });
   

                    lstAllBasemapKey.push(id);
                    arrAllLayerAvailable.push(bLayer);

                } else {
                    var cntBasemap = baseMapData["keys"].length;
                    for (var i = 0; i < cntBasemap; i++) {

                        var grp = baseMapData["items"][i];
                        basemapGrp[baseMapData["keys"][i]] = [];

                        array.forEach(grp, lang.hitch(this, function (itm) {
                            var id = baseMapData["keys"][i] + "-" + itm["serviceID"];
                            var layerState = {
                                id: id,
                                mainStateVisible: itm["isVisible"],
                                extentStateVisible: true,
                                dependLayer: null,
                                maxLevel: itm["maxLevel"],
                                minLevel: itm["minLevel"],
                                mapExtent: null,
                            };

                            if (itm["extentMaxX"] != 0 && itm["extentMaxY"] != 0 && itm["extentMinX"] != 0 && itm["extentMinY"] != 0) {
                                layerState["mapExtent"] = new Extent(itm["extentMinX"], itm["extentMinY"], itm["extentMaxX"], itm["extentMaxY"], new SpatialReference({
                                    wkid: 4326
                                }))
                            }

                            //default visible at index : 0
                            if (i == 0) {
                                layerState.mainStateVisible = true;
                                firstKey = baseMapData["keys"][0];
                            } else
                                layerState.mainStateVisible = false;
                            var arrDependMap = (itm["dependMap"] != undefined && itm["dependMap"] != null) ? itm["dependMap"].replace(new RegExp("'", 'g'), "").split(',') : [];

                            if (arrDependMap.length > 0) {
                                var dependBase = [];
                                var dependLayer = [];
                                array.forEach(arrDependMap, lang.hitch(this, function (dependID) {
                                    dependBase = dependBase.concat(jsonQuery("[?serviceID='" + dependID + "'][=serviceID]", mapProviderData["basemap"]["all"]));
                                    dependLayer = dependLayer.concat(jsonQuery("[?serviceID='" + dependID + "'][=layerName]", mapProviderData["layer"]["items"]));

                                }));

                                lstDependLayer = lstDependLayer.concat(dependLayer);

                                dependBase = dependBase.map(lang.hitch(this, function (svid) {
                                    return baseMapData["keys"][i] + "-" + svid
                                }));
                                layerState.dependLayer = dependBase.concat(dependLayer);

                                //add depend layer to lstAllKey
                                lstAllBasemapKey = lstAllBasemapKey.concat(layerState.dependLayer)

                            }

                            var basemapInstance = null;
                            var url = ResourceLib.currentLanguage.toLowerCase().indexOf("en") > -1 ? itm["urlEnglish"] : itm["urlLocal"];
                            var token = ResourceLib.currentLanguage.toLowerCase().indexOf("en") > -1 ? itm["tokenEnglish"] : itm["tokenLocal"];

                            if (token)
                                url += "?token=" + token;

                            switch (itm["mapServiceType"]) {
                                case "Tile": {
                                    basemapInstance = new ArcGISTiledMapServiceLayer(url, {
                                        id: id,
                                        visible: (itm["isVisible"] && layerState.mainStateVisible),
                                        resampling: true
                                    });
                                    break;
                                }
                                case "Dynamic": {
                                    basemapInstance = new ArcGISDynamicMapServiceLayer(url, {
                                        id: id,
                                        visible: (itm["isVisible"] && layerState.mainStateVisible)
                                    });
                                    break;
                                }
                                case "OSM": {
                                    basemapInstance = new OpenStreetMapLayer({
                                        id: id,
                                        visible: (itm["isVisible"] && layerState.mainStateVisible)
                                    });
                                    break;
                                }
                                case "GOOGLE": {
                                    //basemapInstance = new ArcGISTiledMapServiceLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer",
                                    //    { id: id, visible: (itm["isVisible"] && layerState.mainStateVisible) });
                                    //require google map key
                                    // add GoogleMapLayer widget
                                    //// ก่อน
                                    gmapBasemapItem = itm["baseMapItem"];
                                    gmapA = map; // ต้องมีการตั้งค่า gmapA ให้เท่ากับค่า map object ของ ArcGIS ไว้ในการ callback ใน GoogleMapLayer.js
                                    basemapInstance = new GoogleMapLayer(map, {
                                        id: id,
                                        googleKey: ApplicationConfig.GOOGLEAPIKEY,
                                        mapType: "roadmap",
                                        language: "TH",
                                        traffic: false,
                                        transit: false
                                    });
                                    gmapA = map;
                                    gmapInstance = basemapInstance;

                                    break;
                                }
                                case "GOOGLEIMG":{
                                    basemapInstance = new WebTiledLayer(url, {
                                        copyright: "Google Maps",
                                        id: id,
                                        visible: (itm["isVisible"] && layerState.mainStateVisible)
                                    });
                                    break;
                                }
                                default:
                                    basemapInstance = null;
                                    break;
                            }

                            if (itm["isVisible"]) {
                                basemapGrp[baseMapData["keys"][i]].push(layerState);

                                lstAllBasemapKey.push(id);
                            }
                            if (basemapInstance) arrAllLayerAvailable.push(basemapInstance);

                        }));
                    }

                    /* create layer */
                    var cntLayer = layerData["keys"].length;
                    for (var i = 0; i < cntLayer; i++) {

                        let itm = layerData["items"][i];
                        var layerInstance = null;
                        var url = (ResourceLib.currentLanguage.toUpperCase().indexOf("EN") > -1) ? itm["urlEnglish"] : itm["urlLocal"];
                        var token = (ResourceLib.currentLanguage.toUpperCase().indexOf("EN") > -1) ? itm["tokenEnglish"] : itm["tokenLocal"];
                        var id = itm["layerName"];

                        var layerState = {
                            id: id,
                            mainStateVisible: false
                        };

                        if (token)
                            url += "?token=" + token;

                        switch (layerData["items"][i]["mapServiceType"]) {
                            case "Tile": {
                                layerInstance = new ArcGISTiledMapServiceLayer(url, {
                                    id: id,
                                    visible: layerState.mainStateVisible,
                                    resampling: true
                                });
                                break;
                            }
                            case "Dynamic": {
                                layerInstance = new ArcGISDynamicMapServiceLayer(url, {
                                    id: id,
                                    visible: layerState.mainStateVisible
                                });
                                break;
                            }
                        }


                        if (lstDependLayer.indexOf(itm["layerName"]) == -1)
                            lstLayer.push(layerState);


                        arrAllLayerAvailable.push(layerInstance);

                    }
                }

                /* store basemap state and layer state to ApplicationConfig */
                ApplicationConfig.lstBasemapState = basemapGrp;
                ApplicationConfig.lstLayerState = lstLayer;


                var lstGPLyr = this.createGraphicsLayerList(ApplicationConfig.defaultGraphicLayerList);
                arrAllLayerAvailable = arrAllLayerAvailable.concat(lstGPLyr)

                Utility.debugLog("Basemap Group List", basemapGrp);
                Utility.debugLog("Dynamic Layer List", lstLayer);
                Utility.debugLog("Graphics Layer List ", lstGPLyr);

                map.addLayers(arrAllLayerAvailable);

                currentBasemapActive = firstKey;

                array.forEach(basemapGrp[firstKey], lang.hitch(this, function (itm) {
                    array.forEach(itm.dependLayer, lang.hitch(this, function (dpLyr) {
                        let lyr = map.getLayer(dpLyr);

                        if (lyr)
                            lyr.setVisibility(true);
                    }));
                }));

            } else {
                mapProviderData = ApplicationConfig["mapProviderData"];
                arrAllLayerAvailable = [];
                basemapGrp = {};
                lstLayer = [];
                lstAllBasemapKey = [];

                var firstKey = null;
                var lstDependLayer = [];
                var baseMapData = mapProviderData["basemap"];
                var layerData = mapProviderData["layer"];

                /* create basemap */
                var cntBasemap = baseMapData["keys"].length;
                for (var i = 0; i < cntBasemap; i++) {

                    var grp = baseMapData["items"][i];
                    basemapGrp[baseMapData["keys"][i]] = [];

                    array.forEach(grp, lang.hitch(this, function (itm) {
                        var id = baseMapData["keys"][i] + "-" + itm["serviceID"];
                        var layerState = {
                            id: id,
                            mainStateVisible: itm["isVisible"],
                            extentStateVisible: true,
                            dependLayer: null,
                            maxLevel: itm["maxLevel"],
                            minLevel: itm["minLevel"],
                            mapExtent: null,
                        };

                        if (itm["extentMaxX"] != 0 && itm["extentMaxY"] != 0 && itm["extentMinX"] != 0 && itm["extentMinY"] != 0) {
                            layerState["mapExtent"] = new Extent(itm["extentMinX"], itm["extentMinY"], itm["extentMaxX"], itm["extentMaxY"], new SpatialReference({
                                wkid: 4326
                            }))
                        }

                        //default visible at index : 0
                        if (i == 0) {
                            layerState.mainStateVisible = true;
                            firstKey = baseMapData["keys"][0];
                        } else
                            layerState.mainStateVisible = false;

                        var arrDependMap = (itm["dependMap"] != undefined && itm["dependMap"] != null) ? itm["dependMap"].replace(new RegExp("'", 'g'), "").split(',') : [];

                        if (arrDependMap.length > 0) {
                            var dependBase = [];
                            var dependLayer = [];
                            array.forEach(arrDependMap, lang.hitch(this, function (dependID) {
                                dependBase = dependBase.concat(jsonQuery("[?serviceID='" + dependID + "'][=serviceID]", mapProviderData["basemap"]["all"]));
                                dependLayer = dependLayer.concat(jsonQuery("[?serviceID='" + dependID + "'][=layerName]", mapProviderData["layer"]["items"]));

                            }));

                            lstDependLayer = lstDependLayer.concat(dependLayer);

                            dependBase = dependBase.map(lang.hitch(this, function (svid) {
                                return baseMapData["keys"][i] + "-" + svid
                            }));
                            layerState.dependLayer = dependBase.concat(dependLayer);

                            //add depend layer to lstAllKey
                            lstAllBasemapKey = lstAllBasemapKey.concat(layerState.dependLayer)

                        }

                        var basemapInstance = null;
                        var url = ResourceLib.currentLanguage.toLowerCase().indexOf("en") > -1 ? itm["urlEnglish"] : itm["urlLocal"];
                        var token = ResourceLib.currentLanguage.toLowerCase().indexOf("en") > -1 ? itm["tokenEnglish"] : itm["tokenLocal"];

                        if (token)
                            url += "?token=" + token;

                        switch (itm["mapServiceType"]) {
                            case "Tile": {
                                basemapInstance = new ArcGISTiledMapServiceLayer(url, {
                                    id: id,
                                    visible: (itm["isVisible"] && layerState.mainStateVisible),
                                    resampling: true
                                });
                                break;
                            }
                            case "Dynamic": {
                                basemapInstance = new ArcGISDynamicMapServiceLayer(url, {
                                    id: id,
                                    visible: (itm["isVisible"] && layerState.mainStateVisible)
                                });
                                break;
                            }
                            case "OSM": {
                                basemapInstance = new OpenStreetMapLayer({
                                    id: id,
                                    visible: (itm["isVisible"] && layerState.mainStateVisible)
                                });
                                break;
                            }
                            case "GOOGLE": {
                                //basemapInstance = new ArcGISTiledMapServiceLayer("https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer",
                                //    { id: id, visible: (itm["isVisible"] && layerState.mainStateVisible) });
                                //require google map key
                                //add GoogleMapLayer widget
                                gmapBasemapItem = itm["baseMapItem"];
                                gmapA = map; // ต้องมีการตั้งค่า gmapA ให้เท่ากับค่า map object ของ ArcGIS ไว้ในการ callback ใน GoogleMapLayer.js
                                basemapInstance = new GoogleMapLayer(map, {
                                    id: id,
                                    googleKey: ApplicationConfig.GOOGLEAPIKEY,
                                    mapType: "roadmap",
                                    language: "TH",
                                    traffic: false,
                                    transit: false
                                });
                                gmapA = map;
                                gmapInstance = basemapInstance;

                                break;
                            }
                            default:
                                basemapInstance = null;
                                break;
                        }

                        if (itm["isVisible"]) {
                            basemapGrp[baseMapData["keys"][i]].push(layerState);

                            lstAllBasemapKey.push(id);
                        }

                        if (basemapInstance) arrAllLayerAvailable.push(basemapInstance);

                    }));
                }

                /* create layer */
                var cntLayer = layerData["keys"].length;
                for (var i = 0; i < cntLayer; i++) {

                    let itm = layerData["items"][i];
                    var layerInstance = null;
                    var url = (ResourceLib.currentLanguage.toUpperCase().indexOf("EN") > -1) ? itm["urlEnglish"] : itm["urlLocal"];
                    var token = (ResourceLib.currentLanguage.toUpperCase().indexOf("EN") > -1) ? itm["tokenEnglish"] : itm["tokenLocal"];
                    var id = itm["layerName"];

                    var layerState = {
                        id: id,
                        mainStateVisible: false
                    };

                    if (token)
                        url += "?token=" + token;

                    switch (layerData["items"][i]["mapServiceType"]) {
                        case "Tile": {
                            layerInstance = new ArcGISTiledMapServiceLayer(url, {
                                id: id,
                                visible: layerState.mainStateVisible,
                                resampling: true
                            });
                            break;
                        }
                        case "Dynamic": {
                            layerInstance = new ArcGISDynamicMapServiceLayer(url, {
                                id: id,
                                visible: layerState.mainStateVisible
                            });
                            break;
                        }
                    }


                    if (lstDependLayer.indexOf(itm["layerName"]) == -1)
                        lstLayer.push(layerState);


                    arrAllLayerAvailable.push(layerInstance);

                }
                /* store basemap state and layer state to ApplicationConfig */
                ApplicationConfig.lstBasemapState = basemapGrp;
                ApplicationConfig.lstLayerState = lstLayer;


                var lstGPLyr = this.createGraphicsLayerList(ApplicationConfig.defaultGraphicLayerList);
                arrAllLayerAvailable = arrAllLayerAvailable.concat(lstGPLyr)

                Utility.debugLog("Basemap Group List", basemapGrp);
                Utility.debugLog("Dynamic Layer List", lstLayer);
                Utility.debugLog("Graphics Layer List ", lstGPLyr);

                map.addLayers(arrAllLayerAvailable);

                currentBasemapActive = firstKey;

                array.forEach(basemapGrp[firstKey], lang.hitch(this, function (itm) {
                    array.forEach(itm.dependLayer, lang.hitch(this, function (dpLyr) {
                        let lyr = map.getLayer(dpLyr);

                        if (lyr)
                            lyr.setVisibility(true);
                    }));
                }));


                //console.log(this.args.mapData);
                var mapOption = this.args.mapData.mapOptions;
                var allLayers = this.args.mapData.operationalLayers;
                map.setExtent(new Extent(mapOption.extent));

                if (mapMode != "print-route")
                    map.setScale(mapOption.scale);

                //console.log(allLayers);

                array.forEach(allLayers, lang.hitch(this, function (itm) {

                    var lyr = map.getLayer(itm.id);

                    switch (itm.declaredClass) {
                        case "esri.layers.GraphicsLayer": {

                            lyr.setVisibility(true);

                            if (itm.featureCollection.layers.length > 0) {
                                var featureSet = itm.featureCollection.layers[0].featureSet;


                                array.forEach(featureSet.features, lang.hitch(this, function (gpLayer, gp) {
                                    gpLayer.add(new Graphic(gp));
                                }, lyr));
                            }

                            break;
                        }
                        default:
                            if (lyr)
                                lyr.setVisibility(mapOption.currentBasemapActive.indexOf(itm.id) > -1);
                            break;
                    }

                }));
            }
            //ลบ param ที่ไม่ต้องการให้เห็นออก
            //delete args.domNode;
            //delete args.center;
            //delete args.zoom;
            //delete args.config;

            if (mapMode != "print" && mapMode != "print-route") {
                context = new ContextMenu();
                domConstruct.place(context.domNode, document.getElementById(domId), "last");
            }

            //disable right click on map
            document.getElementById(domId).addEventListener("contextmenu", lang.hitch(this, function (e) {
                e.preventDefault();
            }));

            /* Binding Event */
            if (mapMode != "print" && mapMode != "print-route") {

                if (Utility.isMobileDevice()) {
                    on(map, "click", lang.hitch(this, "_mapClickMainEvent"));

                    //disable right click on map
                    document.getElementById(domId).addEventListener("contextmenu", lang.hitch(this, function (e) {
                        e.preventDefault();
                    }));
                } else {
                    on(map, "click", lang.hitch(this, "_mapClickPCEvent"));

                    //disable right click on map
                    document.getElementById(domId).addEventListener("contextmenu", lang.hitch(this, function (e) {
                        this._mapRightClickEvent(e);
                        e.preventDefault();
                    }));
                }
            }

            on(map, "click", lang.hitch(this, "_mapClickDefault"));

            if (mapMode != "print" && mapMode != "print-route") {

                on(map, "extent-change", lang.hitch(this, "_basemapVisibilityByOption"));

                on(context, "Action", lang.hitch(this, "_ContextMenuClick"));

                /* disable context */
                on(map, "pan-start", lang.hitch(this, "_mapExtentChangeEvent"));
                on(map, "zoom-start", lang.hitch(this, "_mapExtentChangeEvent"));

                on(map, "zoom-end", lang.hitch(this, "_mapZoomEnd"));
            }

        },

        hideContextMenu: function () {
            if (mapMode != "print" && mapMode != "print-route" && mapMode != "measurement" && this.onDrawing != "enable-draw-polygon"&& this.onDrawing != "enable-draw-polygon-area") {
                context.hide();
                map.graphics.clear();
            }
        },

        getLayer: function (key) {
            return map.getLayer(key);
        },

        setMapClickMode: function (state, command) {
            clickmapState = state;
            commandClickmap = command;
        },

        _basemapVisibilityByOption: function () {
            var currentExtent = webMercatorUtils.webMercatorToGeographic(map.extent);
            var currentLevel = map.getZoom();

            //console.log(currentExtent, currentLevel);

            array.forEach(basemapGrp[currentBasemapActive], lang.hitch(this, function (itm) {
                var visibleLevel = true;
                var visibleExtent = true;

                if (itm["minLevel"] != -1 && itm["maxLevel"] != -1 &&
                    itm["minLevel"] != 0 && itm["maxLevel"] != 0) {
                    //DO Visible level
                    visibleLevel = ((currentLevel >= itm["minLevel"]) && currentLevel <= itm["maxLevel"]);
                }

                if (itm["mapExtent"]) {
                    visibleExtent = geometryEngine.intersects(itm["mapExtent"], currentExtent);
                }

                //console.log(itm["url"], visibleLevel, visibleExtent);

                itm["extentStateVisible"] = visibleLevel && visibleExtent;
                if (this.getLayer(itm["id"])) {
                    this.getLayer(itm["id"]).setVisibility((itm["mainStateVisible"] && itm["extentStateVisible"]));
                } else {

                }
            }));

            map.infoWindow.hide();
        },

        _onTrackLocationChange: function (lstDest) {

            if (lstDest.length > 0) {

                let mulPt = new Multipoint(new SpatialReference({
                    wkid: 4326
                }));

                array.forEach(lstDest, lang.hitch(this, function (itm) {
                    let lat, lon;
                    //console.log(itm);
                    if (itm.nextLocation) {
                        mulPt.addPoint(new Point(itm.nextLocation.lon, itm.nextLocation.lat));
                    }

                    if (itm.startLocation) {
                        mulPt.addPoint(new Point(itm.startLocation.lon, itm.startLocation.lat));
                    }
                }));

                if (ApplicationConfig.IsFollowTrackable) {
                    map.setExtent(mulPt.getExtent().expand(2));
                }
            }
        },

        _mapExtentChangeEvent: function () {
            this.hideContextMenu();
        },

        _mapZoomEnd: function (e) {
            //custom poi
            // if (e.level >= ApplicationConfig["customPOIMinimumLevelShow"]) {
            //     map.getLayer("lyr-custom-poi-layer").show();

            //     if (poiLabelFlg) {
            //         map.getLayer("lyr-custom-poi-label-layer").show();
            //     }
            //     else {
            //         map.getLayer("lyr-custom-poi-label-layer").hide();
            //     }
            // }
            // else {
            //     map.getLayer("lyr-custom-poi-layer").hide();
            //     map.getLayer("lyr-custom-poi-label-layer").hide();
            // }

            if (lstLyrResize.length > 0) {
                //doing resize
                //console.log(lstLyrResize);
                array.forEach(lstLyrResize, lang.hitch(this, function (lyr) {
                    this._resizeGraphicInLayer(lyr, e.level);
                }));
            }
        },

        _resizeGraphicInLayer: function (lyr, zoom) {
            //console.log(lyr.id, lyr);
            array.forEach(lyr.graphics, lang.hitch(this, function (gp) {

                if (gp.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] &&
                    gp.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]]) {

                    //console.log(gp.symbol.type);

                    if (gp.symbol.type == "picturemarkersymbol") {
                        var newHeight = Utility.calculateSizeForZoomFactor(gp.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]], zoom);
                        var newWidth = Utility.calculateSizeForZoomFactor(gp.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]], zoom);

                        //console.log("new height :: ", newHeight);
                        //console.log("new width :: ", newWidth);

                        gp.symbol.setWidth(newWidth);
                        gp.symbol.setHeight(newHeight);
                    }
                }

                if (gp.attributes[ApplicationConfig.constantKeys["defaultTextSize"]]) {
                    if (gp.symbol.type == "textsymbol") {
                        var newTextSize = Utility.calculateSizeForZoomFactor(gp.attributes[ApplicationConfig.constantKeys["defaultTextSize"]], zoom);
                        gp.symbol.setFont(new Font(newTextSize + "pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                    }
                }
            }));
        },

        //for pc map right click
        _mapRightClickEvent: function (e) {
            Utility.debugLog("_mapRightClickEvent", e);

            var screenPt = new ScreenPoint(e.clientX, e.clientY);

            var mapPt = screenUtils.toMapPoint(map.extent, map.width, map.height, screenPt);

            var ptGeo = webMercatorUtils.webMercatorToGeographic(mapPt);

            /* create clicked screen extent with screenPoint with size pic */
            var topRightPt = screenUtils.toMapPoint(map.extent, map.width, map.height, new ScreenPoint(e.clientX + 16, e.clientY + 16));
            var bottomLeftPt = screenUtils.toMapPoint(map.extent, map.width, map.height, new ScreenPoint(e.clientX - 16, e.clientY - 16));

            var topRightPtGeo = webMercatorUtils.webMercatorToGeographic(topRightPt);
            var bottomLeftPtGeo = webMercatorUtils.webMercatorToGeographic(bottomLeftPt);

            var clickedExtent = new Extent({
                xmin: bottomLeftPtGeo.x,
                xmax: topRightPtGeo.x,
                ymin: bottomLeftPtGeo.y,
                ymax: topRightPtGeo.y,
                spatialReference: {
                    "wkid": 4326
                }
            });


            if (mapMode != "print" && mapMode != "print-route") {
            
                var clickedGp = this.hitTest(clickedExtent);
                if (clickedGp) {
                    if (clickedGp.gp.visible) {
                        var contextKey = "vehicle";

                        if (clickedGp.lyr == "lyr-custom-poi-layer" || clickedGp.lyr == "lyr-custom-poi-non-cluster") {

                            contextKey = "poi";

                            map.graphics.clear();
                            //this.fireTrigger({
                            //    command: "view-custompoi-info",
                            //    param: clickedGp.gp.attributes
                            //});
                        } else if (clickedGp.lyr == "lyr-draw-alert") {
                            map.graphics.clear();
                            contextKey = "alert";
                        } else if (clickedGp.lyr == "lyr-custom-poi-suggestion") {
                            map.graphics.clear();
                            contextKey = "poiSuggestion";

                            //ถ้า Suggestion นั้นเป็น POI จะไม่สามารถ add ได้
                            if (clickedGp.gp.attributes["isPOI"]) {
                                return;
                            }
                        }else if(clickedGp.lyr == "lyr-shipment-poi-plan" ){
                                if(mapMode === "shipmentViewOnMap"){
                                    return
                                }
                            contextKey = "waypoint";
                            map.graphics.clear();
                        }


                        context.show(e.clientY, e.clientX, contextKey, {
                            param: {
                                geometry: {
                                    lon: clickedGp.gp.geometry.x,
                                    lat: clickedGp.gp.geometry.y,
                                    extent: this.getMapExtent()
                                },
                                attributes: clickedGp.gp.attributes
                            }
                        });
                    } else {
                        context.show(e.clientY, e.clientX, "map", {
                            param: {
                                geometry: {
                                    lon: ptGeo.x,
                                    lat: ptGeo.y,
                                    extent: this.getMapExtent()
                                }
                            }
                        });

                        map.graphics.clear();
                        map.graphics.add(new Graphic(mapPt, locationInfoMapSymbol));
                    }
                } else {
                    context.show(e.clientY, e.clientX, "map", {
                        param: {
                            geometry: {
                                lon: ptGeo.x,
                                lat: ptGeo.y,
                                extent: this.getMapExtent()
                            }
                        }
                    });

                    map.graphics.clear();
                    map.graphics.add(new Graphic(mapPt, locationInfoMapSymbol));
                }

                //var clickLatLon = {
                //    lat: clickedGp.gp.geometry.y,
                //    lon: clickedGp.gp.geometry.x
                //};
                //var symbol = {
                //    url: "/images/block_hilight.png",
                //    width: 32,
                //    height: 32,
                //    offset: {
                //        x: 0,
                //        y: 0
                //    },
                //    rotation: 0
                //};

                if (clickedGp) {
                    if (clickedGp.gp.visible) {
                        var layer = this.getLayer(ApplicationConfig.commandOptions["draw-one-point-zoom"].layer);
                        layer.clear();
                        var picSymbol = new PictureMarkerSymbol("/images/block_hilight.png", 32, 32);
                        var point = new Point([clickedGp.gp.geometry.x, clickedGp.gp.geometry.y]);
                        var graphicPoint = new Graphic(point, picSymbol);
                        layer.add(graphicPoint);
                    }
                }
            }
        },

        //for pc map click
        _mapClickPCEvent: function (e) {
            
// console.log("_mapClickPCEvent",e);
            switch (clickmapState) {
                case "none": {
                    break;
                }
                case "enable-click-map": {

                    var convertedMP = webMercatorUtils.webMercatorToGeographic(e.mapPoint);
                    this.fireTrigger({
                        command: commandClickmap,
                        result: {
                            guid: guid,
                            location: {
                                lon: convertedMP.x,
                                lat: convertedMP.y,
                                extent: this.getMapExtent()
                            }
                        }
                    });

                    //ไม่ต้องวาดจุด
                    //map.graphics.clear();
                    //map.graphics.add(new Graphic(e.mapPoint, mapClickSymbol));

                    clickmapState = "none";
                    commandClickmap = "";
                    map.setMapCursor("default");
                    break;
                }
            }
        },

        //for mobile map touch
        _mapClickMainEvent: function (e) {

            switch (clickmapState) {
                case "none": {
                    //console.log(obj.evt);
                    var left = e.screenPoint.x;
                    var top = e.screenPoint.y;

                    var ptGeo = webMercatorUtils.webMercatorToGeographic(e.mapPoint);

                    map.graphics.clear();
                    map.graphics.add(new Graphic(e.mapPoint, locationInfoMapSymbol));

                    if (mapMode != "print" && mapMode != "print-route")
                        context.show(top, left, "map", {
                            param: {
                                geometry: {
                                    lon: ptGeo.x,
                                    lat: ptGeo.y
                                }
                            }
                        });

                    break;
                }
                case "enable-click-map": {
                    var convertedMP = webMercatorUtils.webMercatorToGeographic(e.mapPoint);
                    this.fireTrigger({
                        command: commandClickmap,
                        result: {
                            guid: guid,
                            location: {
                                lon: convertedMP.x,
                                lat: convertedMP.y,
                                extent: this.getMapExtent()
                            }
                        }
                    });

                    //ไม่ต้องวาดจุด
                    //map.graphics.clear();
                    //map.graphics.add(new Graphic(e.mapPoint, mapClickSymbol));

                    clickmapState = "none";
                    commandClickmap = "";
                    map.setMapCursor("default");
                    break;
                }
            }
        },

        //for map click on all devlice
        _mapClickDefault: function () {

            this.fireTrigger({
                command: "mapclick-close-mis-menu"
            });
        },


        _layerMouseOverMainEvent: function (lyrID, e) {
            //Utility.debugLog("Mouse Over GP Layer", [lyrID, e]);
            //Utility.debugLog("Mouse Over GP Layer", [lyrID, e.graphic]);

            var params = {
                attributes: e.graphic.attributes
            };

            //switch (lyrID) {
            //    case "lyr-track-command":

            //        this.fireTrigger({
            //            command: "open-vehicle-info-window",
            //            param: params
            //        });

            //        break;
            //    case "lyr-playback-stop-command":

            //        this.fireTrigger({
            //            command: "open-playback-footprint-window",
            //            param: params
            //        });

            //        break;
            //    default:
            //        break;
            //}
        },

        _layerMouseOutMainEvent: function (lyrID, e) {
            //Utility.debugLog("Mouse Out GP Layer", [lyrID , e]);

            //var params = {
            //    attributes: e.graphic.attributes
            //};

            //switch (lyrID) {
            //    case "lyr-track-command":

            //        this.fireTrigger({
            //            command: "close-vehicle-info-window",
            //            param: params
            //        });

            //        break;
            //    case "lyr-playback-stop-command":

            //        this.fireTrigger({
            //            command: "close-playback-footprint-window",
            //            param: params
            //        });

            //        break;
            //    default:
            //        break;
            //}
        },

        _suggestionLyrNearbyClick: function (e) {
            console.log("_suggestionLyrNearbyClick")
            map.infoWindow.hide();
            this._clearHilightPoint();
            this.fireTrigger({
                command: "re-draw-poi-suggestion",
                param: e.graphic.attributes
            });
        },

        _layerClickMainEvent: function (lyrID, contextKey, e) {
            console.log("_layerClickMainEvent")
            //Utility.debugLog("_layerClickMainEvent", [lyrID, contextKey, e.graphic]);

            if (context && contextKey) {
                var left = e.screenPoint.x;
                var top = e.screenPoint.y;


                context.show(top, left, contextKey, {
                    param: {
                        geometry: {
                            lon: e.graphic.geometry.x,
                            lat: e.graphic.geometry.y
                        },
                        attributes: e.graphic.attributes
                    }
                });
            }
            dojoEvent.stop(e);
            if (lyrID == "lyr-custom-poi-layer") {
                map.graphics.clear();
                this.fireTrigger({
                    command: "view-custompoi-info",
                    param: e.graphic.attributes
                });
            }
        },

        _layerShowInfo: function (lyrID, e) {
            //  console.log("_layerShowInfo",e);

            if (isDisplayingInfoLayer.indexOf(lyrID) == -1) {
                isDisplayingInfoLayer = [];
                map.infoWindow.hide();
            } else {
                return;
            }

            map.infoWindow.clearFeatures();



            //map.infoWindow.setFixedAnchor(InfoWindow.ANCHOR_UPPERLEFT);

            /* calculate infowindow position */
            var sc = map.toScreen(e.graphic.geometry);

            var posX = sc.x;
            var posY = sc.y;

            var mapWidth = map.width;
            var mapHeight = map.height;

            var posAnchor = [];

            var maxInfoHeight = 400;
            var maxInfoWidth = 400;

            if (ApplicationConfig.isPanelOpen()) {
                maxInfoWidth += 400;
            }

            //console.log(posX, posY, mapWidth, mapHeight);
            //var newPt = null;

            if ((posY + maxInfoHeight) <= mapHeight) {
                posAnchor.push("bottom");
            } else if ((mapHeight - posY) + maxInfoHeight <= mapHeight) {
                posAnchor.push("top");
            } else {
                //newPt = screenUtils.toMapPoint(map.extent, map.width, map.height, new ScreenPoint(posX, posY));
            }

            if ((posX + maxInfoWidth) < mapWidth) {
                posAnchor.push("right");
            } else {
                posAnchor.push("left");
            }

            //console.log(posAnchor);
            switch (lyrID) {
                case "lyr-track-command":
                case "lyr-playback-stop-command":
                case "lyr-playback-alert-command":
                case "lyr-playback-alert-chart-command":
                case "lyr-shipment-vehicle":
                case "lyr-track-nearest":
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;


                        map.infoWindow.setTitle(attributes["TITLE"]);
                        map.infoWindow.setContent(this._createMapInfoShipmentTable(attributes["SHIPMENTINFO"]));
                        map.infoWindow.resize(310, 500);

                        map.infoWindow.anchor = posAnchor.join("-");

                        //setTimeout(lang.hitch(this, function () {
                        map.infoWindow.show(e.graphic.geometry);
                        //}), 500);

                    }
                    break;
                case "lyr-shipment-poi-plan":
                case "lyr-shipment-poi-actual":
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        if (attributes) {
                            map.infoWindow.setTitle(attributes["jobWaypointName"]);
                            map.infoWindow.setContent(this._createMapInfoWaypoint(attributes["info"]));
                            map.infoWindow.resize(400, 400);
                            map.infoWindow.anchor = posAnchor.join("-");
                            //setTimeout(lang.hitch(this, function () {
                            map.infoWindow.show(e.graphic.geometry);
                            //}), 500);
                        }
                    }
                    break;
                case "lyr-shipment-waiting":
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        if (attributes) {
                            map.infoWindow.setTitle(attributes["TITLE"]);
                            map.infoWindow.setContent(this._createMapInfoShipmentTable(attributes["SHIPMENTINFO"]));
                            map.infoWindow.resize(310, 500);
                            map.infoWindow.anchor = posAnchor.join("-");
                            map.infoWindow.show(e.graphic.geometry);
                        }
                    }
                    break;
                case "lyr-shipment-poi-waiting":
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        if (attributes) {
                            map.infoWindow.setTitle(attributes["TITLE"]);
                            map.infoWindow.setContent(this._createMapInfoShipmentTable(attributes["SHIPMENTINFO"]));
                            map.infoWindow.resize(310, 500);
                            map.infoWindow.anchor = posAnchor.join("-");
                            map.infoWindow.show(e.graphic.geometry);
                        }
                    }
                    break;
                case "lyr-draw-alert":
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        map.infoWindow.setTitle(attributes["vehicleLicense"]);
                        map.infoWindow.setContent(this._createMapInfoAlert(attributes));
                        map.infoWindow.resize(310, 500);
                        map.infoWindow.anchor = posAnchor.join("-");
                        map.infoWindow.show(e.graphic.geometry);

                        //console.log(attributes);
                    }
                    break;
                case "lyr-custom-poi-layer": {
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        map.infoWindow.setTitle(attributes["code"]);
                        map.infoWindow.setContent(this._createMapInfoPOI(attributes));
                        map.infoWindow.anchor = posAnchor.join("-");
                        map.infoWindow.show(e.graphic.geometry);
                    }
                    break;
                }

                case "lyr-custom-poi-suggestion-nearby": {
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        map.infoWindow.setTitle(attributes["license"]);
                        map.infoWindow.setContent(this._createMapInfoSuggestionPOI(attributes));
                        map.infoWindow.anchor = posAnchor.join("-");
                        map.infoWindow.show(e.graphic.geometry);
                    }
                    break;
                }

                default:
                    break;
            }

        },

        _layerDisplayInfo: function (lyrID, e) {

            //map.infoWindow.setFixedAnchor(InfoWindow.ANCHOR_UPPERLEFT);

            /* calculate infowindow position */
            var sc = map.toScreen(e.graphic.geometry);

            var posX = sc.x;
            var posY = sc.y;

            var mapWidth = map.width;
            var mapHeight = map.height;

            var posAnchor = [];

            var maxInfoHeight = 400;
            var maxInfoWidth = 400;

            if (ApplicationConfig.isPanelOpen()) {
                maxInfoWidth += 400;
            }

            //console.log(posX, posY, mapWidth, mapHeight);
            //var newPt = null;

            if ((posY + maxInfoHeight) <= mapHeight) {
                posAnchor.push("bottom");
            } else if ((mapHeight - posY) + maxInfoHeight <= mapHeight) {
                posAnchor.push("top");
            } else {
                //newPt = screenUtils.toMapPoint(map.extent, map.width, map.height, new ScreenPoint(posX, posY));
            }

            if ((posX + maxInfoWidth) < mapWidth) {
                posAnchor.push("right");
            } else {
                posAnchor.push("left");
            }

            //console.log(posAnchor);

            switch (lyrID) {
                case "lyr-track-command":
                case "lyr-track-nearest":
                case "lyr-shipment-vehicle":
                    if (e.graphic.symbol.type == undefined || e.graphic.symbol.type != "textsymbol") {
                        var attributes = e.graphic.attributes;
                        map.infoWindow.setTitle(attributes["TITLE"]);
                        map.infoWindow.setContent(this._createMapInfoShipmentTable(attributes["SHIPMENTINFO"]));
                        map.infoWindow.resize(310, 500);

                        map.infoWindow.anchor = posAnchor.join("-");

                        //setTimeout(lang.hitch(this, function () {
                        map.infoWindow.show(e.graphic.geometry);
                        //}), 500);

                        if (isDisplayingInfoLayer.indexOf(lyrID) == -1) {
                            isDisplayingInfoLayer.push(lyrID);
                        }
                    }
                    break;
                default:
                    break;
            }
        },

        _layerHideInfo: function (lyrID, e) {
            //Utility.debugLog("To Hide Info on Layer", [lyrID, e]);
            switch (lyrID) {
                case "lyr-track-command":
                case "lyr-playback-stop-command":
                case "lyr-playback-alert-command":
                case "lyr-playback-alert-chart-command":
                case "lyr-shipment-poi-plan":
                case "lyr-shipment-poi-actual":
                case "lyr-shipment-poi-waiting":
                case "lyr-shipment-waiting":
                case "lyr-shipment-vehicle":
                case "lyr-draw-alert":
                case "lyr-custom-poi-layer":
                case "lyr-custom-poi-suggestion-nearby":
                case "lyr-track-nearest":
                    if (isDisplayingInfoLayer.indexOf(lyrID) == -1) {
                        map.infoWindow.hide();
                    }
                    break;
                default:
                    break;
            }

        },

        _createMapInfoSuggestionPOI: function (attr) {
            var table = domConstruct.create("table");

            var row = table.insertRow(0);
            var cellTitle = row.insertCell(0);
            var cellText = row.insertCell(1);

            //License
            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Assets_VehicleLicense");
            cellText.innerHTML = attr["license"] || "-";

            //Code
            row = table.insertRow(1);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Common_Location");
            cellText.innerHTML = attr["location"] || "-";

            return table;
        },

        _createMapInfoPOI: function (attr) {
            var table = domConstruct.create("table");

            var row = table.insertRow(0);
            var cellTitle = row.insertCell(0);
            var cellText = row.insertCell(1);

            //POI Name
            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("CustomPOI_Name");
            cellText.innerHTML = attr["name"] || "-";

            //Code
            row = table.insertRow(1);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("CustomPOI_Code");
            cellText.innerHTML = attr["code"] || "-";

            //Lon
            row = table.insertRow(2);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("CustomPOI_Longitude");
            cellText.innerHTML = attr["longitude"] || "-";

            //Lat
            row = table.insertRow(3);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("CustomPOI_Latitude");
            cellText.innerHTML = attr["latitude"] || "-";

            //Location

            var strLocation = attr["townName"] + ' ' + attr["cityName"] + ' ' + attr["provinceName"];

            row = table.insertRow(4);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("CustomPOI_Location");
            cellText.innerHTML = strLocation || "-";

            return table;
        },

        _createMapInfoAlert: function (attr) {
            var table = domConstruct.create("table");
            var sensors = attr["trackLocationFeatureValues"];
            var svg = null;
            var divIcon = null;

            //Alert Type
            var row = table.insertRow(0);
            var cellTitle = row.insertCell(0);
            var cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Common_AlertType");
            cellText.innerHTML = attr["alertTypeDisplayName"] || "-";

            //Driver
            row = table.insertRow(1);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Common_Driver");
            cellText.innerHTML = attr["driverName"] || "-";

            //BU
            row = table.insertRow(2);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Common_BusinessUnit");
            cellText.innerHTML = attr["businessUnitName"] || "-";

            //DateTime
            row = table.insertRow(3);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Common_Datetime");
            cellText.innerHTML = attr["formatAlertDateTime"] || "-";

            //Location
            row = table.insertRow(4);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            cellTitle.innerHTML = ResourceLib.text("Common_Location");
            cellText.innerHTML = attr["location"] || "-";

            //Movement
            row = table.insertRow(5);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            svg = domConstruct.toDom(attr["svgIcon"]["movement"]);
            divIcon = domConstruct.create("div", null, cellTitle);
            domClass.add(divIcon, "div-svg-icon-status");
            domConstruct.place(svg, divIcon, "last");

            //cellTitle.innerHTML = ResourceLib.text("Common_Movement");
            cellText.innerHTML = attr["movementDisplayName"] || "-";

            //Speed
            row = table.insertRow(6);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            svg = domConstruct.toDom(attr["svgIcon"]["speed"]);
            divIcon = domConstruct.create("div", null, cellTitle);
            domClass.add(divIcon, "div-svg-icon-status");
            domConstruct.place(svg, divIcon, "last");

            //cellTitle.innerHTML = ResourceLib.text("Common_Speed");
            if (attr["formatSpeed"])
                cellText.innerHTML = attr["formatSpeed"] || "-";
            else if (attr["speed"])
                cellText.innerHTML = attr["speed"] || "-";
            else
                cellText.innerHTML = "-";

            //Engine
            row = table.insertRow(7);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            svg = domConstruct.toDom(attr["svgIcon"]["engine"]);
            divIcon = domConstruct.create("div", null, cellTitle);
            domClass.add(divIcon, "div-svg-icon-status");
            domConstruct.place(svg, divIcon, "last");

            var engineSensor = this._findObjectSensor("TrkVwEngine", sensors);
            //cellTitle.innerHTML = ResourceLib.text("Common_Engine");
            if (engineSensor)
                cellText.innerHTML = engineSensor["formatValue"] || "-";
            else
                cellText.innerHTML = "-";

            //Fuel
            row = table.insertRow(8);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            svg = domConstruct.toDom(attr["svgIcon"]["fuel"]);
            divIcon = domConstruct.create("div", null, cellTitle);
            domClass.add(divIcon, "div-svg-icon-status");
            domConstruct.place(svg, divIcon, "last");

            var fuelSensor = this._findObjectSensor("TrkVwFuel", sensors);
            //cellTitle.innerHTML = ResourceLib.text("Common_FuelLevel");
            if (fuelSensor)
                cellText.innerHTML = fuelSensor["formatValue"] + " " + fuelSensor["featureUnitSymbol"] || "-";
            else
                cellText.innerHTML = "-";

            //GPS
            row = table.insertRow(9);
            cellTitle = row.insertCell(0);
            cellText = row.insertCell(1);

            domClass.add(cellTitle, "mapinfo-column-title");
            domClass.add(cellText, "mapinfo-column-text");

            svg = domConstruct.toDom(attr["svgIcon"]["gps"]);
            divIcon = domConstruct.create("div", null, cellTitle);
            domClass.add(divIcon, "div-svg-icon-status");
            domConstruct.place(svg, divIcon, "last");

            //cellTitle.innerHTML = ResourceLib.text("Common_GPSStatus");
            cellText.innerHTML = attr["gpsStatusDisplayName"] || "-";

            return table;
        },

        _findObjectSensor: function (key, allSensorObj) {

            var obj = null;
            for (var ind in allSensorObj) {
                if (allSensorObj[ind]["featureCode"] == key) {
                    obj = allSensorObj[ind];
                    break;
                }
            }

            return obj;
        },



        _ContextMenuClick: function (obj) {
            Utility.debugLog("Context Menu Click", obj);

            context.hide();

            this.fireTrigger(obj);
        },

        _createMapInfoShipmentTable: function (data) {
            var table = domConstruct.create("table");

            if (data && data.length) {

                for (var i = 0; i < data.length; i++) {
                    if (data[i]["key"] != "remark") {
                        if (data[i]["icon"]) {
                            this._createMapInfoShipmentRow(table, i, data[i]["icon"], data[i]["text"], true);
                        } else {
                            this._createMapInfoShipmentRow(table, i, data[i]["title"], data[i]["text"]);
                        }
                    } else {
                        var rowRemark = table.insertRow(i);

                        var cellRemark = rowRemark.insertCell(0);
                        cellRemark.colSpan = 2;
                        domClass.add(rowRemark, "mapinfo-row");
                        var spnRemark = domConstruct.create("span", {
                            innerHTML: data[i]["title"]
                        }, cellRemark);
                        domClass.add(spnRemark, "mapinfo-remark-title");
                        var br = domConstruct.create("br", null, cellRemark);
                        var spnRemark2 = domConstruct.create("span", {
                            innerHTML: data[i]["text"]
                        }, cellRemark);
                        domClass.add(spnRemark2, "mapinfo-remark-text");
                    }
                }
            }

            return table;
        },

        _createMapInfoWaypoint: function (data) {
            var table = null;
            if (data) {
                table = domConstruct.create("table");
                var i = 0;
                for (var i = 0; i < data.length; i++) {
                    this._createMapInfoShipmentRow(table, i, data[i]["title"], data[i]["text"]);
                }
            }
            return table;
        },


        _createMapInfoShipmentRow: function (table, indRow, title, text, isTitleIcon) {
            var row = table.insertRow(indRow);
            var cellTitle = row.insertCell(0);
            var cellText = row.insertCell(1);
            domClass.add(row, "mapinfo-row");
            if (isTitleIcon) {
                domClass.add(cellTitle, "mapinfo-column-title");
                var divIcon = domConstruct.create("div", null, cellTitle);
                domClass.add(divIcon, "div-svg-icon-status");
                var svg = domConstruct.toDom(title);
                domConstruct.place(svg, divIcon, "last");
            } else {
                domClass.add(cellTitle, "mapinfo-column-title");
                cellTitle.innerHTML = title;
            }

            domClass.add(cellText, "mapinfo-column-text");

            cellText.innerHTML = text;
            return row;
        },

        hitTest: function (ext) {
            var lstLayerId = ["lyr-track-command", 
            "lyr-custom-poi-layer", 
            "lyr-custom-poi-non-cluster", 
            "lyr-draw-alert", 
            "lyr-custom-poi-suggestion",
            "lyr-shipment-poi-plan"];
            //"lyr-polygon-drawing-POIArea"];

            var gpHit = null;

            array.forEach(lstLayerId, lang.hitch(this, function (id) {
                if (gpHit)
                    return;

                var lyr = this.getLayer(id);
                var maxGp = lyr.graphics.length;

                for (var i = 0; i < maxGp; i++) {

                    //console.log(lyr.graphics[i]);

                    if (lyr.graphics[i].symbol == null)
                        continue;

                    if (lyr.graphics[i].symbol.type == "textsymbol")
                        continue;
                    if (lyr.graphics[i].attributes["clusterCount"] && lyr.graphics[i].attributes["clusterCount"] > 1)
                        continue;

                    //console.log(lyr.graphics[i].symbol);
                    //console.log(ext);
                    //console.log(geometryEngine.intersects(lyr.graphics[i].geometry, ext));
                    var gpExtent = lyr.graphics[i].geometry;

                    if (lyr.id == "lyr-custom-poi-layer" || lyr.id == "lyr-custom-poi-non-cluster") {// ||lyr.id == "lyr-polygon-drawing-POIArea") {
                        gpExtent = webMercatorUtils.webMercatorToGeographic(gpExtent);
                    }

                    if (lyr.id == "lyr-draw-alert" || lyr.id == "lyr-custom-poi-suggestion") {
                        var currentScreenPt = screenUtils.toScreenPoint(map.extent, window.innerWidth, window.innerHeight, lyr.graphics[i].geometry);
                        var symb = lyr.graphics[i].symbol;

                        var width = (Number(lyr.graphics[i].symbol.width) / 2);
                        var height = (Number(lyr.graphics[i].symbol.height) / 2);

                        //var offsetX = Number(lyr.graphics[i].xoffset || 0);
                        //var offsetY = Number(lyr.graphics[i].yoffset || 0);

                        var topRightPt = screenUtils.toMapPoint(map.extent, map.width, map.height, new ScreenPoint(currentScreenPt.x + width /*- offsetX*/ , currentScreenPt.y + height /*- offsetY*/ ));
                        var bottomLeftPt = screenUtils.toMapPoint(map.extent, map.width, map.height, new ScreenPoint(currentScreenPt.x - width /*- offsetX*/ , currentScreenPt.y - height /*- offsetY*/ ));

                        var topRightPtGeo = webMercatorUtils.webMercatorToGeographic(topRightPt);
                        var bottomLeftPtGeo = webMercatorUtils.webMercatorToGeographic(bottomLeftPt);

                        gpExtent = new Extent({
                            xmin: bottomLeftPtGeo.x,
                            xmax: topRightPtGeo.x,
                            ymin: bottomLeftPtGeo.y,
                            ymax: topRightPtGeo.y,
                            spatialReference: {
                                "wkid": 4326
                            }
                        });
                    }

                    //console.log(geometryEngine.intersects(gpExtent, ext));

                    if (geometryEngine.intersects(gpExtent, ext)) {

                        gpHit = {
                            lyr: id,
                            gp: new Graphic(lyr.graphics[i].toJson())
                        };

                        // if(lyr.id == "lyr-polygon-drawing-POIArea"){
                        //     gpHit.visible =  true
                        // }

                        if (lyr.id == "lyr-custom-poi-layer" || lyr.id == "lyr-custom-poi-non-cluster") {
                            var geoPt = webMercatorUtils.webMercatorToGeographic(gpHit.gp.geometry);
                            gpHit.gp.setGeometry(geoPt);
                        }

                        break;
                    }
                }
            }));

            // console.log(gpHit);

            return gpHit;
        },

        createGraphicsLayerList: function (arrLyrConfig) {
            var lstGPLayer = [];
            array.forEach(arrLyrConfig, lang.hitch(this, function (lyrData) {
                var gpLyr;

                if (lyrData.lyrType && lyrData.lyrType == "feature") {
                    console.log("inFeature")
                    var layerDefinition = {
                        "geometryType": "esriGeometryPolygon",
                        "fields": [{
                            "name": "OBJECTID",
                            "type": "esriFieldTypeOID",
                            "alias": "Object ID"
                        }]
                    }
                    var featureCollection = {
                        layerDefinition: layerDefinition,
                        featureSet: null
                    };

                    gpLyr = new FeatureLayer(featureCollection, {
                        id: lyrData.id,
                        visible: lyrData.visible,
                        showLabels: true
                    });

                    lstGPLayer.push(gpLyr);
                } else if (lyrData.lyrType && lyrData.lyrType == "cluster" && ApplicationConfig.showPoiCluster) {

                    var singleTemplate = new PopupTemplate({
                        "title": "",
                        "fieldInfos": [{
                            "fieldName": "name",
                            "label": ResourceLib.text("CustomPOI_Name"),
                            visible: true
                        }, {
                            "fieldName": "code",
                            "label": ResourceLib.text("CustomPOI_Code"),
                            visible: true
                        }, {
                            "fieldName": "location",
                            "label": ResourceLib.text("CustomPOI_Location"),
                            visible: true
                        }]
                    });

                    /*  create cluster */
                    var clusterOption = {
                        //"data": lstData,
                        "distance": 5, //25,
                        "id": lyrData.id,
                        "labelColor": "#fff",
                        "labelOffset": -3,
                        "labelSize": "16px",
                        "resolution": map.extent.getWidth() / map.width,
                        "singleColor": "#888",
                        "singleTemplate": singleTemplate
                    };

                    var cluserDefaultSym = new SimpleMarkerSymbol(
                        ApplicationConfig.cluster.default.type,
                        ApplicationConfig.cluster.default.size,
                        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.cluster.default.borderColor), ApplicationConfig.cluster.default.borderSize),
                        new Color(ApplicationConfig.cluster.default.fillColor)
                    )

                    var clusterRenderer = new ClassBreaksRenderer(cluserDefaultSym, "clusterCount");

                    array.forEach(ApplicationConfig.cluster.classbreak, lang.hitch(this, function (brk) {
                        var breakSym = null;

                        if (ApplicationConfig.cluster.classbreakSymbol == "simple") {
                            breakSym = new SimpleMarkerSymbol(
                                brk.type,
                                brk.size,
                                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(brk.borderColor), brk.borderSize),
                                new Color(brk.fillColor));
                        } else if (ApplicationConfig.cluster.classbreakSymbol == "picture") {
                            breakSym = new PictureMarkerSymbol({
                                "url": brk.url,
                                "height": brk.height,
                                "width": brk.width,
                                "type": "picturemarkersymbol"
                            });
                        }


                        clusterRenderer.addBreak(brk.min, brk.max, breakSym);
                    }));

                    gpLyr = new ClusterPointLayer(clusterOption);
                    gpLyr.setRenderer(clusterRenderer);

                    clusterLayer = gpLyr;

                    lstGPLayer.push(gpLyr);
                }

                /////////////  Graphic Layer ///////////////
                else if (lyrData.lyrType && lyrData.lyrType == "non-cluster-graphics" && !ApplicationConfig.showPoiCluster) {
                    //console.log("in non-cluster-graphics")

                    gpLyr = new GraphicsLayer({
                        id: lyrData.id,
                        visible: (lyrData.visible == undefined ? true : lyrData.visible)
                    });
                    nonClusterLayer = gpLyr;

                    lstGPLayer.push(nonClusterLayer);

                } else {
                    gpLyr = new GraphicsLayer({
                        id: lyrData.id,
                        visible: (lyrData.visible == undefined ? true : lyrData.visible)
                    });
                    lstGPLayer.push(gpLyr);
                }

                if (Utility.isMobileDevice()) {
                    if (lyrData["onclick"])
                        on(gpLyr, "click", lang.hitch(this, "_layerClickMainEvent", lyrData.id, lyrData.context));
                    else {}
                } else {
                    if (lyrData["onmouseover"]) {
                        on(gpLyr, "mouse-over", lang.hitch(this, "_layerMouseOverMainEvent", lyrData.id));
                        on(gpLyr, "mouse-out", lang.hitch(this, "_layerMouseOutMainEvent", lyrData.id));
                    }


                    if (lyrData["showInfo"]) {
                        on(gpLyr, "mouse-over", lang.hitch(this, "_layerShowInfo", lyrData.id));
                        on(gpLyr, "mouse-out", lang.hitch(this, "_layerHideInfo", lyrData.id));
                    }

                    if (lyrData["onclickDisplayInfo"]) {
                        on(gpLyr, "click", lang.hitch(this, "_layerDisplayInfo", lyrData.id));
                    }
                }

                if (lyrData.id == "lyr-custom-poi-suggestion-nearby") {
                    on(gpLyr, "click", lang.hitch(this, "_suggestionLyrNearbyClick"));
                }

                if (lyrData.id == "lyr-custom-poi-non-cluster") {
                    on(gpLyr, "click", lang.hitch(this, "_showCustomPOIInfo"));
                }

                if (lyrData["enableResize"]) {
                    lstLyrResize.push(gpLyr);
                }
            }));

            return lstGPLayer;
        },

        showContext: function (contextType, obj) {

        },

        fireTrigger: function (obj) {
            if (this.onTriggerFire)
                this.onTriggerFire(obj);
        },
        drawPOIArea: function(){
            let mapManager = ApplicationConfig.mapManager;
            let layerID = "lyr-polygon-drawing-POIArea";
            let polygonMarker = new SimpleFillSymbol(
                SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_SOLID,
                    new Color(ApplicationConfig.measurementSetting.polygonSymbol.lineColor), ApplicationConfig.measurementSetting.polygonSymbol.lineWidth),
                new Color(ApplicationConfig.measurementSetting.polygonSymbol.fillColor));
            polygonMarker.color.a = ApplicationConfig.measurementSetting.polygonSymbol.fillOpacity;
            polygonMarker.outline.color.a = ApplicationConfig.measurementSetting.polygonSymbol.lineOpacity;
            
            layerDrawing = mapManager.getLayer(layerID);
            drawMng = new Draw(map);
            drawMng.activate(Draw.POLYGON, { showTooltips: false });

            drawMng.on("draw-complete", function(evt){
                drawMng.deactivate()

                // map.graphics.add(new Graphic(evt.geometry,polygonMarker));
                let gp = new Graphic(evt.geometry,polygonMarker,{ID:"POIArea",visible: "true"})
                gp.visible = true;

                layerDrawing.clear();
                layerDrawing.add(gp);
            })

        },

        drawLocationInfoPin: function (geo) {
            var lyr = ApplicationConfig.mapManager.getLayer("lyr-location-info");
            lyr.clear();

            lyr.add(new Graphic(new Point(geo.lon, geo.lat), mapClickSymbol));
        },

        /* Command Function */

        trackVehicles: function (obj) {
            var lyr = this.getLayer(ApplicationConfig.commandOptions["track-vehicles"].layer);

            let countDrawTrack = 1;
            let isShowAnimate = (obj.vehicles.length < 50); //50
            let isShowTextGP = (obj.vehicles.length < limitDrawTrackVehicle); //300

            try {
                if (obj.vehicles.length > limitDrawTrackVehicle) {
                    console.error("Over limit display vehicle : " + limitDrawTrackVehicle);
                }

                if (obj.vehicles && obj.vehicles.length > 0) {
                    // console.log("before foreach :: ", obj.vehicles.length);
                    array.forEach(obj.vehicles, lang.hitch(this, function (item) {

                        //เพิ่ม property เก็บขนาด default ของ symbol ไว้ที่ attributes
                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = 32;
                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = 32;
                        item.attributes[ApplicationConfig.constantKeys["defaultTextSize"]] = 14;

                        //if (countDrawTrack > limitDrawTrackVehicle) throw BreakException;
                        //countDrawTrack++;
                        if (!wgVehicleTracking.checkAlreadyAddItem(item.attributes.ID)) {

                            var zoomLevel = map.getZoom();

                            var width = Utility.calculateSizeForZoomFactor(item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]], zoomLevel);
                            var height = Utility.calculateSizeForZoomFactor(item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]], zoomLevel);
                            var textSize = Utility.calculateSizeForZoomFactor(item.attributes[ApplicationConfig.constantKeys["defaultTextSize"]], zoomLevel);
                            var directionType = item.attributes["RAW"]["directionType"] - 1;
                            var symbol = new PictureMarkerSymbol(item.pic[this.directionConst[directionType]], width, height);
                            var geo = new Point([item.location.lon, item.location.lat]);
                            var gp = new Graphic(geo, symbol, item.attributes);

                            lyr.add(gp);
                            var textSymb = new TextSymbol();
                            textSymb.setText(item.attributes.TITLE);
                            textSymb.setOffset(0, -27);
                            textSymb.setFont(new Font(textSize + "pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                            textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                            textSymb.setHaloSize(ApplicationConfig.haloSize);
                            var textGp = new Graphic(geo, textSymb, item.attributes);
                            textGp.visible = isShowTextGP;
                            lyr.add(textGp);
                            wgVehicleTracking.addTrackableItem({
                                id: item.attributes.ID,
                                directionPic: item.pic,
                                location: item.location,
                                graphic: gp,
                                textGraphic: textGp,
                                track: item.track
                            });

                        } else {
                            // เริ่มขยับจุด 
                            // show animation if isShowAnimate = true
                            wgVehicleTracking.recieveData({
                                id: item.attributes.ID,
                                location: item.location,
                                attributes: item.attributes,
                                directionPic: item.pic,
                                track: item.track
                            }, !isShowAnimate);
                        }
                    }));
                }
            } catch (ex) {
                console.error("ex : ", ex);
            }
        },
        trackNearestVehicles: function (obj) {
            var lyr = this.getLayer(ApplicationConfig.commandOptions["track-nearest-vehicle"].layer);
            this.getLayer(ApplicationConfig.commandOptions["draw-one-point-zoom"].layer).clear();

            let isShowTextGP = (obj.vehicles.length < limitDrawTrackVehicle); //300

            if (lyr.graphics.length == 0) {

                try {
                    if (obj.vehicles.length > limitDrawTrackVehicle) {
                        console.error("Over limit display vehicle : " + limitDrawTrackVehicle);
                    }

                    if (obj.vehicles && obj.vehicles.length > 0) {
                        // console.log("before foreach :: ", obj.vehicles.length);
                        array.forEach(obj.vehicles, lang.hitch(this, function (item) {

                            //เพิ่ม property เก็บขนาด default ของ symbol ไว้ที่ attributes
                            item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = 32;
                            item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = 32;
                            item.attributes[ApplicationConfig.constantKeys["defaultTextSize"]] = 14;

                            var zoomLevel = map.getZoom();

                            var width = Utility.calculateSizeForZoomFactor(item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]], zoomLevel);
                            var height = Utility.calculateSizeForZoomFactor(item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]], zoomLevel);
                            var textSize = Utility.calculateSizeForZoomFactor(item.attributes[ApplicationConfig.constantKeys["defaultTextSize"]], zoomLevel);
                            var directionType = item.attributes["RAW"]["directionType"] - 1;
                            var symbol = new PictureMarkerSymbol(item.pic[this.directionConst[directionType]], width, height);
                            var geo = new Point([item.location.lon, item.location.lat]);
                            var gp = new Graphic(geo, symbol, item.attributes);

                            lyr.add(gp);
                            var textSymb = new TextSymbol();
                            textSymb.setText(item.attributes.TITLE);
                            textSymb.setOffset(0, -27);
                            textSymb.setFont(new Font(textSize + "pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                            textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                            textSymb.setHaloSize(ApplicationConfig.haloSize);
                            var textGp = new Graphic(geo, textSymb, item.attributes);
                            textGp.visible = isShowTextGP;
                            lyr.add(textGp);

                            var layerExtent = new graphicsUtils.graphicsExtent(lyr.graphics);
                            var zoomToExtent = layerExtent.expand(1.2);
                            map.setExtent(zoomToExtent, true);
                        }));
                    }
                } catch (ex) {
                    console.error("ex : ", ex);
                }
            }

        },

        removeTrackVehicle: function (obj) {
            var lyr = this.getLayer(ApplicationConfig.commandOptions["track-vehicles"].layer);

            if (obj.vehicleIDs && obj.vehicleIDs.length > 0) {
                array.forEach(obj.vehicleIDs, lang.hitch(this, function (id) {
                    let itm = wgVehicleTracking.getVehicle(id);

                    if (itm) {
                        lyr.remove(itm.vehicle.graphic);
                        lyr.remove(itm.vehicle.textGraphic);
                        wgVehicleTracking.removeTrackableItem(itm);
                    }
                }));
            }
        },

        removeAllTrackable: function () {
            wgVehicleTracking.clearAllTrackable();
        },

        hidePlaybackLegend: function () {
            domClass.remove(dom.byId("divFootprintLegend"), "open");
        },

        enableClickMap: function (obj) {
            console.log("inClick")
            context.hide();
            clickmapState = "enable-click-map";
            commandClickmap = "enable-click-map";
            guid = obj["guid"];
            map.setMapCursor("pointer");
        },

        enableClickdrawPOIArea:function (obj) {
            console.log("drawPOIArea",obj);
            // clickmapState = "enable-click-map-POIArea";
            // commandClickmap = "enable-click-map";
            // guid = obj["guid"];
            map.setMapCursor("pointer");
            this.drawPOIArea()
        },

        enableDrawingPolygon: function (obj) {
            context.hide();
            clickmapState = "enable-draw-polygon";
            commandClickmap = "enable-draw-polygon";
            guid = obj["guid"];
        },

        enableDrawingPolygonArea: function (obj) {
            context.hide();
            clickmapState = "enable-draw-polygon-area";
            commandClickmap = "enable-draw-polygon-area";
            guid = obj["guid"];
        },

        cancelClickMap: function (obj) {
            clickmapState = "none";
            commandClickmap = "";
            guid = null;
            map.setMapCursor("default");
        },

        cancelDrawingPolygon: function (obj) {
            clickmapState = "none";
            commandClickmap = "";
            guid = null;
        },

        drawOnePointZoom: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-one-point-zoom"].layer);
            //layer.disableMouseEvents();
            layer.clear();
            var picSymbol = new PictureMarkerSymbol(obj.symbol.url, obj.symbol.width, obj.symbol.height);
            picSymbol.setOffset(obj.symbol.offset.x, obj.symbol.offset.y);
            picSymbol.setAngle(obj.symbol.rotation);
            var point = new Point([obj.location.lon, obj.location.lat]);
            var graphicPoint = new Graphic(point, picSymbol, obj.attributes);
            layer.add(graphicPoint);
            if (obj.zoom) {
                map.centerAndZoom(point, obj.zoom);
            }

            return JSON.stringify(graphicPoint.toJson());
        },

        drawPointPoiArea: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-point-main-area"].layer);
            //layer.disableMouseEvents();
            layer.clear();
            var picSymbol = new PictureMarkerSymbol(obj.symbol.url, obj.symbol.width, obj.symbol.height);
            picSymbol.setOffset(obj.symbol.offset.x, obj.symbol.offset.y);
            picSymbol.setAngle(obj.symbol.rotation);
            var point = new Point([obj.location.lon, obj.location.lat]);
            var graphicPoint = new Graphic(point, picSymbol, obj.attributes);
            layer.add(graphicPoint);
            if (obj.zoom) {
                map.centerAndZoom(point, obj.zoom);
            }

            return JSON.stringify(graphicPoint.toJson());
        },
        panOnePointZoom: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-one-point-zoom"].layer);
            layer.clear();
            var picSymbol = new PictureMarkerSymbol(obj.symbol.url, obj.symbol.width, obj.symbol.height);
            picSymbol.setOffset(obj.symbol.offset.x, obj.symbol.offset.y);
            picSymbol.setAngle(obj.symbol.rotation);
            var point = new Point([obj.location.lon, obj.location.lat]);
            var graphicPoint = new Graphic(point, picSymbol, obj.attributes);
            layer.add(graphicPoint);
            map.centerAt(point);

        },

        drawAlertPoint: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-alert-point"].layer);
            layer.clear();
            var picSymbol = new PictureMarkerSymbol(obj.symbol.url, obj.symbol.width, obj.symbol.height);
            picSymbol.setOffset(obj.symbol.offset.x, obj.symbol.offset.y);
            picSymbol.setAngle(obj.symbol.rotation);
            var point = new Point([obj.location.lon, obj.location.lat]);
            var graphicPoint = new Graphic(point, picSymbol, obj.attributes);
            layer.add(graphicPoint);
            if (obj.zoom) {
                map.centerAndZoom(point, obj.zoom);
            }

            return JSON.stringify(graphicPoint.toJson());
        },

        drawOneLineZoom: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-one-line-zoom"].layer);
            layer.clear();
            var symbol = new SimpleLineSymbol().setStyle(obj.symbol.style).setColor(new Color(obj.symbol.color)).setWidth(obj.symbol.width);
            var polylineJson = {
                "paths": obj.path,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var line = new Polyline(polylineJson);
            var graphic = new Graphic(line, symbol);
            layer.add(graphic);
            var layerExtent = new graphicsUtils.graphicsExtent(layer.graphics);
            var zoomToExtent = layerExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);
        },
        drawOnePolygonZoom: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-one-polygon-zoom"].layer);
            layer.clear();
            var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj.color.border), 1), new Color(obj.color.fill));
            symbol.color.a = obj.opacity.fill;
            symbol.outline.color.a = obj.opacity.border;
            var polygonJson = {
                "rings": obj.ring,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var polygon = new Polygon(polygonJson);


            //console.log(obj.attributes.size);

            /* Calculate Area */
            //
            var centerOfPolygon = polygon.getCentroid();
            var mctPolygon = webMercatorUtils.geographicToWebMercator(polygon);
            var areaValue = geometryEngine.geodesicArea(mctPolygon, "square-meters");
            var displayAreaValue = Utility.toAreaUnitCompanyFormat(areaValue);

            var areaTextPolygon = new TextSymbol();
            areaTextPolygon.setText(displayAreaValue);
            areaTextPolygon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            areaTextPolygon.setHaloColor(new Color(ApplicationConfig.haloColor));
            areaTextPolygon.setHaloSize(ApplicationConfig.haloSize);
            areaTextPolygon.setOffset(0, -18);

            var name = "";
            if (obj.attributes)
                name = obj.attributes.name;

            var textPolygon = new TextSymbol();
            textPolygon.setText(name);
            textPolygon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            textPolygon.setHaloColor(new Color(ApplicationConfig.haloColor));
            textPolygon.setHaloSize(ApplicationConfig.haloSize);

            var graphic = new Graphic(polygon, symbol);
            layer.add(graphic);
            var graphicText = new Graphic(centerOfPolygon, textPolygon);
            layer.add(graphicText);
            var graphicAreaText = new Graphic(centerOfPolygon, areaTextPolygon);
            layer.add(graphicAreaText);

            var zoomToExtent = polygon.getExtent().expand(1.2);
            map.setExtent(zoomToExtent, true);
        },

        drawOnePolygonZoomArea: function (obj) {
            let layerID = obj.layer ? obj.layer:"draw-one-polygon-zoom"
            var layer = this.getLayer(ApplicationConfig.commandOptions[layerID].layer);
            var layerSubarea = this.getLayer(ApplicationConfig.commandOptions["enable-draw-polygon-area"].layer);
            // var layerSubareaMain = this.getLayer(ApplicationConfig.commandOptions["draw-polygon-sub-area"].layer);
            // layerSubarea.clear();
            // layerSubareaMain.clear();
            if(obj.attributes && obj.attributes.index == 0){
                layer.clear();
            }
            var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj.color.border), 1), new Color(obj.color.fill));
            symbol.color.a = obj.opacity.fill;
            symbol.outline.color.a = obj.opacity.border;
            var polygonJson = {
                "rings": obj.ring,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var polygon = new Polygon(polygonJson);


            //console.log(obj.attributes.size);

            /* Calculate Area */
            //
            var centerOfPolygon = polygon.getCentroid();
            var mctPolygon = webMercatorUtils.geographicToWebMercator(polygon);
            var areaValue = geometryEngine.geodesicArea(mctPolygon, "square-meters");
            var displayAreaValue = Utility.toAreaUnitCompanyFormat(areaValue);

            var areaTextPolygon = new TextSymbol();
            areaTextPolygon.setText(displayAreaValue);
            areaTextPolygon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            areaTextPolygon.setHaloColor(new Color(ApplicationConfig.haloColor));
            areaTextPolygon.setHaloSize(ApplicationConfig.haloSize);
            areaTextPolygon.setOffset(0, -18);

            var name = "";
            if (obj.attributes)
                name = obj.attributes.name;

            var textPolygon = new TextSymbol();
            textPolygon.setText(name);
            textPolygon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            textPolygon.setHaloColor(new Color(ApplicationConfig.haloColor));
            textPolygon.setHaloSize(ApplicationConfig.haloSize);

            var graphic = new Graphic(polygon, symbol);
            layer.add(graphic);
            var graphicText = new Graphic(centerOfPolygon, textPolygon);
            layer.add(graphicText);
            var graphicAreaText = new Graphic(centerOfPolygon, areaTextPolygon);
            layer.add(graphicAreaText);

            // var zoomToExtent = polygon.getExtent().expand(1.2);
            // map.setExtent(zoomToExtent, true);
        },
        drawMultiplePolygonZoomArea: function (obj) {
            let layerID = obj.layer ? obj.layer:"draw-multiple-polygon-zoom-area"
            var layer = this.getLayer(ApplicationConfig.commandOptions[layerID].layer);
            
            // obj.data.map((itm)=>{
            //     var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj.color.border), 1), new Color(obj.color.fill));
            //     symbol.color.a = obj.opacity.fill;
            //     symbol.outline.color.a = obj.opacity.border;
            //     var polygonJson = {
            //         "rings": obj.ring,
            //         "spatialReference": {
            //             "wkid": 4326
            //         }
            //     };
            //     var polygon = new Polygon(polygonJson);
            // })
        },
        drawMultiplePolygonZoom: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-multiple-polygon-zoom"].layer);
            layer.clear();

            var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj.color.border), 1), new Color(obj.color.fill));
            symbol.color.a = obj.opacity.fill;
            symbol.outline.color.a = obj.opacity.border;
            var polygonJson = {
                "rings": obj.ring,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var polygon = new Polygon(polygonJson);
            var graphic = new Graphic(polygon, symbol);
            layer.add(graphic);

            var layerExtent = new graphicsUtils.graphicsExtent(layer.graphics);
            var zoomToExtent = layerExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);
        },
        drawMultipleLineZoom: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-multiple-line-zoom"].layer);
            layer.clear();
            var symbol = new SimpleLineSymbol().setStyle(obj.symbol.style).setColor(new Color(obj.symbol.color)).setWidth(obj.symbol.width);
            var polylineJson = {
                "paths": obj.path,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var line = new Polyline(polylineJson);
            var graphic = new Graphic(line, symbol);
            layer.add(graphic);
            layer.show();
            var layerExtent = new graphicsUtils.graphicsExtent(layer.graphics);
            var zoomToExtent = layerExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);
        },
        drawMultipleVehicles: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-multiple-vehicles"].layer);
            layer.clear();
            for (i = 0; i < obj.vehicles.length; i++) {
                var picSymbol = new PictureMarkerSymbol(obj.vehicles[i].symbol.url, obj.vehicles[i].symbol.width, obj.vehicles[i].symbol.height);
                picSymbol.setOffset(obj.vehicles[i].symbol.offset.x, obj.vehicles[i].symbol.offset.y);
                picSymbol.setAngle(obj.vehicles[i].symbol.rotation);
                var point = new Point([obj.vehicles[i].location.lon, obj.vehicles[i].location.lat]);
                var graphicPoint = new Graphic(point, picSymbol, obj.vehicles[i].attributes);
                layer.add(graphicPoint);
            }
            var layerExtent = new graphicsUtils.graphicsExtent(layer.graphics);
            var zoomToExtent = layerExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);
        },
        zoomPoint: function (obj) {
            var point = new Point([obj.location.lon, obj.location.lat]);
            map.centerAndZoom(point, obj.zoom);

            on.once(map, "zoom-end", lang.hitch(this, function () {
                this.fireTrigger({
                    command: "zoom-point-complete"
                });
            }))
        },
        drawOneRoute: function (obj) {
            this.getLayer("lyr-shipment-poi-area-info").clear();

            var layerStop = this.getLayer(ApplicationConfig.commandOptions["draw-one-route"].layer.stop);
            layerStop.clear();
            var layerRoute = this.getLayer(ApplicationConfig.commandOptions["draw-one-route"].layer.route);
            layerRoute.clear();
            for (i = 0; i < obj.stops.length; i++) {
                var picSymbol = new PictureMarkerSymbol(obj.pics[i], 35, 50);
                picSymbol.setOffset(0, 23.5);
                var point = new Point([obj.stops[i].lon, obj.stops[i].lat]);
                var graphicPoint = new Graphic(point, picSymbol);
                layerStop.add(graphicPoint);
            }
            var symbol = new SimpleLineSymbol().setStyle("solid").setColor(new Color(obj.symbol.color)).setWidth(obj.symbol.width);
            symbol.color.a = obj.symbol.opacity;
            var polylineJson = {
                "paths": obj.path,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var line = new Polyline(polylineJson);
            var graphic = new Graphic(line, symbol, obj.attributes);
            layerRoute.add(graphic);
            var routeExtent = new graphicsUtils.graphicsExtent(layerRoute.graphics);
            var pinExtent = new graphicsUtils.graphicsExtent(layerStop.graphics);
            routeExtent.union(pinExtent);

            var centerExtent = pinExtent.getCenter();
            var name = "";

            if (obj.attributes) {
                name = obj.attributes.name ? obj.attributes.name : "";
            }

            /* Calculate Area */
            var mctLine = webMercatorUtils.geographicToWebMercator(line);
            var lengthValue = geometryEngine.geodesicLength(mctLine, "meters");
            var displayLengthValue = Utility.toDistanceUnitCompanyFormat(lengthValue);

            var lengthTextLine = new TextSymbol();
            lengthTextLine.setText(displayLengthValue);
            lengthTextLine.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            lengthTextLine.setHaloColor(new Color(ApplicationConfig.haloColor));
            lengthTextLine.setHaloSize(ApplicationConfig.haloSize);
            lengthTextLine.setOffset(0, -18);

            var textLine = new TextSymbol();
            textLine.setText(name);
            textLine.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            textLine.setHaloColor(new Color(ApplicationConfig.haloColor));
            textLine.setHaloSize(ApplicationConfig.haloSize);

            var graphicText = new Graphic(centerExtent, textLine);
            layerRoute.add(graphicText);
            var graphicLengthText = new Graphic(centerExtent, lengthTextLine);
            layerRoute.add(graphicLengthText);

            var zoomToExtent = routeExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);
        },
        drawRouteBuffer: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-route-buffer"].layer);
            layer.clear();
            var polylineJson = {
                "paths": obj.path,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var line = new Polyline(polylineJson);
            line = webMercatorUtils.geographicToWebMercator(line);
            var geometry = geometryEngine.buffer(line, obj.radius, obj.unit);

            if (obj.generalize) {
                geometry = geometryEngine.generalize(geometry, obj.generalize);
            }

            var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj.symbol.color.border), 1), new Color(obj.symbol.color.fill));
            symbol.color.a = obj.symbol.opacity.fill;
            symbol.outline.color.a = obj.symbol.opacity.border;
            var graphic = new Graphic(geometry, symbol);
            layer.add(graphic);

            var layerExtent = new graphicsUtils.graphicsExtent(layer.graphics);
            var zoomToExtent = layerExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);

            //return webMercatorUtils.webMercatorToGeographic(geometry).toJson();

            let areaValue = geometryEngine.geodesicArea(geometry, "square-kilometers");
            let numOfPointValue = geometry.rings[0].length - 1;

            /// หา minLatitude,maxLatitude,minLongitude, maxLongitude/////////////////////////

            let newGeometry = webMercatorUtils.webMercatorToGeographic(geometry).toJson();
            let max = newGeometry.rings[0].length;

            let lstLat = [],
                lstLon = [];

            for (let i = 0; i < max; i++) {
                newGeometry.rings[0][i][0] = Number(newGeometry.rings[0][i][0].toFixed(6));
                newGeometry.rings[0][i][1] = Number(newGeometry.rings[0][i][1].toFixed(6));

                lstLon.push(Number(newGeometry.rings[0][i][0].toFixed(6)));
                lstLat.push(Number(newGeometry.rings[0][i][1].toFixed(6)));
            }

            /* Find Min - Max of Latitude and Longitude */
            lstLon = lstLon.sort(function (a, b) {
                return a - b
            });
            lstLat = lstLat.sort(function (a, b) {
                return a - b
            });
            ////////////////////////////////////////////////////////////////////////

            let centroid = webMercatorUtils.webMercatorToGeographic(new Polygon(geometry)).getCentroid(); //หา centralLongitude,centralLatitude

            return {

                geometry: webMercatorUtils.webMercatorToGeographic(geometry).toJson(),
                centralLatitude: centroid.y.toFixed(6),
                centralLongitude: centroid.x.toFixed(6),
                minLatitude: lstLat[0],
                maxLatitude: lstLat[max - 1],
                minLongitude: lstLon[0],
                maxLongitude: lstLon[max - 1],
                area: areaValue,
                count: numOfPointValue,
            };
        },
        drawCustomArea: function (obj) {
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-custom-area"].layer);
            layer.clear();

            var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj.symbol.color.border), 1), new Color(obj.symbol.color.fill));
            symbol.color.a = obj.symbol.opacity.fill;
            symbol.outline.color.a = obj.symbol.opacity.border;
            var polygonJson = {
                "rings": obj.ring,
                "spatialReference": {
                    "wkid": 4326
                }
            };
            var polygon = new Polygon(polygonJson);
            var graphic = new Graphic(polygon, symbol, obj.attributes);
            layer.add(graphic);

            var layerExtent = new graphicsUtils.graphicsExtent(layer.graphics);
            var zoomToExtent = layerExtent.expand(1.2);
            map.setExtent(zoomToExtent, true);
        },

        clearLayers: function (obj) {

            var layers = obj["layer"] || [obj];

            array.forEach(layers, lang.hitch(this, function (lyrID) {
                this.getLayer(lyrID).clear();
            }));
        },
        clearLayersId: function (obj) {
                this.getLayer(ApplicationConfig.commandOptions[obj].layer).clear();
  
        },
        drawPointBuffer: function (obj) {
            var pt = obj["location"];
            var bufferConfig = ApplicationConfig.bufferConfig;

            var point = webMercatorUtils.geographicToWebMercator(new Point(pt.lon, pt.lat));

            var buffRadius = obj["radius"] || bufferConfig.defaultBufferRadius;
            var buffUnit = obj["unit"] || bufferConfig.defaultBufferUnit;

            var buffArea = geometryEngine.buffer(point, buffRadius, buffUnit);

            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-point-buffer"].layer);
            layer.clear();

            var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(obj["symbol"]["color"]["border"]), bufferConfig.defaultBufferBorderWidth),
                new Color(obj["symbol"]["color"]["fill"]));

            symbol.color.a = obj["symbol"]["opacity"]["fill"];
            symbol.outline.color.a = obj["symbol"]["opacity"]["border"];


            var graphic = new Graphic(buffArea, symbol);

            layer.add(graphic);
        },

        drawMultiplePoint: function (obj) {
            var lstPoint = obj.points;
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-multiple-point"].layer);
            layer.clear();

            array.forEach(lstPoint, lang.hitch(this, function (ptData) {

                var attr = ptData.attributes;
                var pt = new Point(ptData.location.lon, ptData.location.lat);
                var symb = new PictureMarkerSymbol(ptData.symbol.url, ptData.symbol.width, ptData.symbol.height);
                symb.setOffset(ptData.symbol.offset.x, ptData.symbol.offset.y);

                var gp = new Graphic(pt, symb, attr);
                layer.add(gp);
            }));
        },

        drawPOISuggestion: function (obj) {

            var zoom = obj.zoom;
            var lstPoint = obj.points;
            var mainPoint = obj.mainPoint;
            var radius = obj.radius;
            var unit = obj.unit;
            var bufferConfig = ApplicationConfig.bufferConfig;
            var layer = this.getLayer("lyr-custom-poi-suggestion");
            var layerNearBy = this.getLayer("lyr-custom-poi-suggestion-nearby");
            var layerBuffer = this.getLayer("lyr-draw-buffer-command");
            var fillColor = new Color([0, 255, 0]);
            var fillOpacity = 0.4;
            var borderColor = new Color([0, 255, 0]);
            var borderOpacity = 1;
            var isPOI = obj.mainPoint.isPOI;
            layer.clear();
            layerNearBy.clear();
            layerBuffer.clear();

            array.forEach(lstPoint, lang.hitch(this, function (ptData) {
                var attr = ptData.attributes;
                var pt = new Point(ptData.location.lon, ptData.location.lat);
                var symb = new PictureMarkerSymbol(ptData.symbol.url, ptData.symbol.width, ptData.symbol.height);
                symb.setOffset(ptData.symbol.offset.x, ptData.symbol.offset.y);

                var gp = new Graphic(pt, symb, attr);
                layerNearBy.add(gp);
            }));

            var attrMain = lang.mixin(mainPoint.attributes, {
                isPOI: isPOI
            });

            var ptMain = new Point(mainPoint.location.lon, mainPoint.location.lat);
            var symbMain = new PictureMarkerSymbol(mainPoint.symbol.url, mainPoint.symbol.width, mainPoint.symbol.height);
            symbMain.setOffset(mainPoint.symbol.offset.x, mainPoint.symbol.offset.y);

            var gpMain = new Graphic(ptMain, symbMain, attrMain);
            layer.add(gpMain);

            var buffArea = geometryEngine.buffer(webMercatorUtils.geographicToWebMercator(ptMain), radius, unit);

            var symbolBuffer = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, borderColor, bufferConfig.defaultBufferBorderWidth),
                fillColor);

            symbolBuffer.color.a = fillOpacity;
            symbolBuffer.outline.color.a = borderOpacity;

            var gpBuffer = new Graphic(buffArea, symbolBuffer);
            layerBuffer.add(gpBuffer);

            // on.once(map, "extent-change", lang.hitch(this, function() {
            //     this.fireTrigger({ command: "suggestion-poi-complete" });
            // }))

            let zoomArea = buffArea.getExtent().expand(1.5);
            // var t = new Graphic(zoomArea, new SimpleFillSymbol());
            // layerBuffer.add(t);

            map.setExtent(zoomArea).then(lang.hitch(this, function () {
                //map.centerAt(ptMain);
                this.fireTrigger({
                    command: "suggestion-poi-complete"
                });
            }));

            //map.centerAndZoom(ptMain, zoom);

        },

        animatePlayback: function (item) {
            var lyr = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.animate);
            lyr.clear();
            var symbol = new PictureMarkerSymbol(item.playback[0].pic["N"], 32, 32);
            var geo = new Point([item.playback[0].lon, item.playback[0].lat]);

            var gp = new Graphic(geo, symbol, item.playback[0].attributes);

            var textSymb = new TextSymbol();
            textSymb.setText(item.playback[0].attributes.TITLE);
            textSymb.setOffset(0, -27);
            textSymb.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
            textSymb.setHaloSize(ApplicationConfig.haloSize);

            var textGP = new Graphic(geo, textSymb, item.playback[0].attributes);

            lyr.add(gp);
            lyr.add(textGP);

            //เพิ่มการ Zoom ไปยังจุดแรก
            map.centerAndZoom(geo, 17);

            var param = {
                id: item.id,
                pic: item.pic,
                playback: item.playback,
                graphic: gp,
                textGraphic: textGP
            };

            return param;
        },

        drawPlaybackFootprint: function (obj) {
            var lyr = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.route);
            var lyrStop = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.stop);
            var lyrAlert = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.alert);
            lyr.clear();
            lyrStop.clear();
            lyrAlert.clear();

            var lyrStopSetting = jsonQuery("[?id='" + ApplicationConfig.commandOptions["playback-command"].layer.stop + "']", ApplicationConfig.defaultGraphicLayerList)[0];
            var lyrAlertSetting = jsonQuery("[?id='" + ApplicationConfig.commandOptions["playback-command"].layer.alert + "']", ApplicationConfig.defaultGraphicLayerList)[0];

            var mode = obj.mode || "ALL";
            var allLine = new Polyline(new SpatialReference({
                wkid: 4326
            }));

            domConstruct.empty(dom.byId("divFootprintLegend"));

            let zoomLevel = map.getZoom();

            array.forEach(obj.playback, lang.hitch(this, function (pbItem, ind) {

                let divLgItem = domConstruct.create("div", {
                    class: "div-legend-item"
                }, dom.byId("divFootprintLegend"));
                let divColor = domConstruct.create("div", {
                    class: "div-legend-color"
                }, divLgItem);
                let divIconColor = domConstruct.create("div", {
                    class: "div-legend-color-item"
                }, divColor);
                let divLabel = domConstruct.create("div", {
                    class: "div-legend-label",
                    innerHTML: pbItem[0]["attributes"]["TITLE"]
                }, divLgItem);

                domStyle.set(divIconColor, {
                    "backgroundColor": ApplicationConfig.playbackFootprintSetting.color[ind]
                });
                domClass.add(dom.byId("divFootprintLegend"), "open");

                let lstPoint = [];
                let line = new Polyline(new SpatialReference({
                    wkid: 4326
                }));

                array.forEach(pbItem, lang.hitch(this, function (item, i) {

                    let point = new Point(item.lon, item.lat);
                    let symb = null;

                    if (mode == "ALL") {

                        if (i != 0 && i != pbItem.length - 1) {
                            switch (item.movement) {
                                case "PARK": {
                                    let width = ApplicationConfig.playbackFootprintSetting.footprintParkPin.width;
                                    let height = ApplicationConfig.playbackFootprintSetting.footprintParkPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFootprintSetting.footprintParkPin.url,
                                        width,
                                        height);
                                    break;
                                }
                                case "PARKENGINEON": {
                                    let width = ApplicationConfig.playbackFootprintSetting.footprintEngineOnPin.width;
                                    let height = ApplicationConfig.playbackFootprintSetting.footprintEngineOnPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFootprintSetting.footprintEngineOnPin.url,
                                        width,
                                        height);
                                    break;
                                }
                                case "STOP": {
                                    let width = ApplicationConfig.playbackFootprintSetting.footprintStopPin.width;
                                    let height = ApplicationConfig.playbackFootprintSetting.footprintStopPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFootprintSetting.footprintStopPin.url,
                                        width,
                                        height);
                                    break;
                                }
                                default: {

                                    let width = ApplicationConfig.playbackFootprintSetting.footprintPin.width;
                                    let height = ApplicationConfig.playbackFootprintSetting.footprintPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFootprintSetting.footprintPin.url,
                                        width,
                                        height);
                                    break;
                                }
                            }
                            symb.setAngle(item.rotation || 0);

                            lyrStop.add(new Graphic(point, symb, item.attributes));
                        } else {}
                    } else {
                        if (item.movement == "STOP" || item.movement == "PARK") {
                            switch (item.movement) {
                                case "PARK": {

                                    let width = ApplicationConfig.playbackFootprintSetting.footprintParkPin.width;
                                    let height = ApplicationConfig.playbackFootprintSetting.footprintParkPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = ApplicationConfig.playbackFootprintSetting.footprintParkPin.height;
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = ApplicationConfig.playbackFootprintSetting.footprintParkPin.width;

                                        width = Utility.calculateSizeForZoomFactor(ApplicationConfig.playbackFootprintSetting.footprintParkPin.width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(ApplicationConfig.playbackFootprintSetting.footprintParkPin.height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFootprintSetting.footprintParkPin.url,
                                        width,
                                        height);
                                    break;
                                }
                                default: {

                                    let width = ApplicationConfig.playbackFootprintSetting.footprintStopPin.width;
                                    let height = ApplicationConfig.playbackFootprintSetting.footprintStopPin.height;

                                    if (lyrStopSetting.enableResize) {
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = height;
                                        item.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = width;

                                        width = Utility.calculateSizeForZoomFactor(width, zoomLevel);
                                        height = Utility.calculateSizeForZoomFactor(height, zoomLevel);
                                    }

                                    symb = new PictureMarkerSymbol(
                                        ApplicationConfig.playbackFootprintSetting.footprintStopPin.url,
                                        width,
                                        height);
                                    break;
                                }
                            }

                            symb.setAngle(item.rotation || 0);

                            lyrStop.add(new Graphic(point, symb, item.attributes));
                        }
                    }

                    lstPoint.push(point);
                }));

                line.addPath(lstPoint);
                allLine.addPath(lstPoint);

                var sym = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(ApplicationConfig.playbackFootprintSetting.color[ind]), 6);

                var gp = new Graphic(line, sym);
                lyr.add(gp);

                //create start and end
                // var symbPoint = new PictureMarkerSymbol(
                //     ApplicationConfig.playbackFootprintSetting.destinationPin.url,
                //     ApplicationConfig.playbackFootprintSetting.destinationPin.width,
                //     ApplicationConfig.playbackFootprintSetting.destinationPin.height);

                let beginWidth = ApplicationConfig.playbackFootprintSetting.pinBegin.width;
                let beginHeigth = ApplicationConfig.playbackFootprintSetting.pinBegin.height;
                let beginAttr = lang.clone(pbItem[0]);
                let endWidth = ApplicationConfig.playbackFootprintSetting.pinEnd.width;
                let endHeigth = ApplicationConfig.playbackFootprintSetting.pinEnd.height;
                let endAttr = lang.clone(pbItem[pbItem.length - 1]);

                beginAttr.SHIPMENTINFO = beginAttr.attributes.SHIPMENTINFO;
                beginAttr.TITLE = beginAttr.attributes.TITLE;
                endAttr.SHIPMENTINFO = endAttr.attributes.SHIPMENTINFO;
                endAttr.TITLE = endAttr.attributes.TITLE;

                if (lyrStopSetting.enableResize) {
                    beginAttr.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = beginWidth;
                    beginAttr.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = beginHeigth;

                    endAttr.attributes[ApplicationConfig.constantKeys["defaultIconHeight"]] = endWidth;
                    endAttr.attributes[ApplicationConfig.constantKeys["defaultIconWidth"]] = endHeigth;

                    beginWidth = Utility.calculateSizeForZoomFactor(beginWidth, zoomLevel);
                    beginHeigth = Utility.calculateSizeForZoomFactor(beginHeigth, zoomLevel);

                    endWidth = Utility.calculateSizeForZoomFactor(endWidth, zoomLevel);
                    endHeigth = Utility.calculateSizeForZoomFactor(endHeigth, zoomLevel);
                }

                var symbBegin = new PictureMarkerSymbol(
                    ApplicationConfig.playbackFootprintSetting.pinBegin.url,
                    beginWidth,
                    beginHeigth);

                var symbEnd = new PictureMarkerSymbol(
                    ApplicationConfig.playbackFootprintSetting.pinEnd.url,
                    endWidth,
                    endHeigth);

                //symbPoint.setOffset(ApplicationConfig.playbackFootprintSetting.destinationPin.offsetX, ApplicationConfig.playbackFootprintSetting.destinationPin.offsetY);

                var pointStart = new Point(pbItem[0].lon, pbItem[0].lat);
                var pointEnd = new Point(pbItem[pbItem.length - 1].lon, pbItem[pbItem.length - 1].lat);

                lyrStop.add(new Graphic(pointStart, symbBegin, beginAttr));
                lyrStop.add(new Graphic(pointEnd, symbEnd, endAttr));

            }));

            array.forEach(obj.alert, lang.hitch(this, function (alertItem, ind) {

                let alertAttr = alertItem.attributes;
                let alertWidth = 32;
                let alertHeight = 32;


                if (lyrAlertSetting.enableResize) {
                    alertAttr[ApplicationConfig.constantKeys["defaultIconHeight"]] = alertHeight;
                    alertAttr[ApplicationConfig.constantKeys["defaultIconWidth"]] = alertWidth;

                    alertWidth = Utility.calculateSizeForZoomFactor(alertWidth, zoomLevel);
                    alertHeight = Utility.calculateSizeForZoomFactor(alertHeight, zoomLevel);
                }

                var pointAlert = new Point(alertItem.longitude, alertItem.latitude);
                var symbAlert = new PictureMarkerSymbol(alertItem.attributes.imageUrl, alertWidth, alertHeight);
                lyrAlert.add(new Graphic(pointAlert, symbAlert, alertAttr));
            }));

            //console.log(allLine);
            map.setExtent(allLine.getExtent().expand(1.5));
        },

        closePlaybackFootprintLegend: function () {
            var lyr = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.route);
            var lyrStop = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.stop);
            var lyrAlert = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.alert);
            var lyrAlertChart = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.alertChart);
            lyr.clear();
            lyrStop.clear();
            lyrAlert.clear();
            lyrAlertChart.clear();

            domClass.remove(dom.byId("divFootprintLegend"), "open");
        },

        drawPlaybackFully: function (obj) {

            //console.log(obj);

            //var lyr = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.route);
            //lyr.clear();

            ////footprint
            //array.forEach(obj.footprint, lang.hitch(this, function (fpItem) {
            //    let line = new Polyline(fpItem.route);
            //    let sym = new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(fpItem.color), 6);

            //    let gp = new Graphic(line, sym);
            //    lyr.add(gp);
            //}));

            ////animation
            //array.forEach(obj.playback, lang.hitch(this, function (pbItem) {

            //}));

        },

        drawPlaybackAlertChart: function (alertItem) {

            var lyrAlertChart = this.getLayer(ApplicationConfig.commandOptions["playback-command"].layer.alertChart);
            lyrAlertChart.clear();
            var pointAlert = new Point(alertItem.longitude, alertItem.latitude);
            var symbAlert = new PictureMarkerSymbol(alertItem.imageUrl, 32, 32);
            lyrAlertChart.add(new Graphic(pointAlert, symbAlert, alertItem.attributes));

            map.centerAndZoom(pointAlert, 17);

        },

        closeViewOnMap: function () {
            var lyrPlanRoute = this.getLayer("lyr-shipment-plan");
            var lyrPlanPin = this.getLayer("lyr-shipment-poi-plan");
            var lyrActualRoute = this.getLayer("lyr-shipment-actual");
            var lyrActualPin = this.getLayer("lyr-shipment-poi-actual");
            var lyrVehicle = this.getLayer("lyr-shipment-vehicle");
            var lyrWaitingRoute = this.getLayer("lyr-shipment-waiting");
            var lyrWaitingPin = this.getLayer("lyr-shipment-poi-waiting");
            var lyrAreaInfo = this.getLayer("lyr-shipment-poi-area-info");

            lyrPlanRoute.clear();
            lyrPlanPin.clear();
            lyrActualRoute.clear();
            lyrActualPin.clear();
            lyrVehicle.clear();
            lyrWaitingRoute.clear();
            lyrWaitingPin.clear();
            lyrAreaInfo.clear();

        },

        drawViewOnMap: function (data) {
            var lstPlanItem = data.plan;
            var lstActualItem = data.actual;
            var lstVehicle = data.vehicle;
            var lstWaitingItem = data.waiting;
            // console.log(">>>>>>>>>>>>>.")
            // console.log("lstActualItem",lstActualItem)
            var lyrPlanRoute = this.getLayer("lyr-shipment-plan");
            var lyrPlanPin = this.getLayer("lyr-shipment-poi-plan");
            var lyrActualRoute = this.getLayer("lyr-shipment-actual");
            var lyrActualPin = this.getLayer("lyr-shipment-poi-actual");
            var lyrWaitingRoute = this.getLayer("lyr-shipment-waiting");
            var lyrWaitingPin = this.getLayer("lyr-shipment-poi-waiting");
            var lyrVehicle = this.getLayer("lyr-shipment-vehicle");
            var lyrAreaInfo = this.getLayer("lyr-shipment-poi-area-info");
           

            lyrPlanRoute.clear();
            lyrPlanPin.clear();
            lyrActualRoute.clear();
            lyrActualPin.clear();
            lyrWaitingRoute.clear();
            lyrWaitingPin.clear();
            lyrVehicle.clear();
            lyrAreaInfo.clear();

            lyrPlanRoute.show();
            lyrPlanPin.show();
            lyrActualRoute.show();
            lyrActualPin.show();
            lyrWaitingRoute.show();
            lyrWaitingPin.show();
            lyrVehicle.show();
            lyrAreaInfo.hide();

            var mpPoint = new Multipoint();
            var indPinNumber = 0;

            var n = 0;
            for (var ind = 0; ind < lstActualItem.route.length; ind++) { //วาดเส้นทาง actual
                if (n === ApplicationConfig.waypointConfig.routeColor.actual.length) {
                    n = 0;
                }
                // console.log(">>>",ApplicationConfig.waypointConfig.routeColor.actual[n].color)
                this._generateRouteViewOnMap(lstActualItem.route[ind].shipmentLocations, ApplicationConfig.waypointConfig.routeColor.actual[n].color, lyrActualRoute, mpPoint);
                n++;
            }

            this._generateRouteViewOnMap(lstPlanItem.route, ApplicationConfig.waypointConfig.routeColor.plan, lyrPlanRoute, mpPoint); //วาดเส้นทางplan


            for (var ind = 0; ind < lstActualItem.pin.length; ind++) {

                indPinNumber++;
                this._generatePinViewOnMap(lstActualItem.pin[ind], indPinNumber, mpPoint, lyrActualPin);
            }

            indPinNumber = 0;

            for (var ind = 0; ind < lstPlanItem.pin.length; ind++) {
                indPinNumber++;
                this._generatePinViewOnMap(lstPlanItem.pin[ind], indPinNumber, mpPoint, lyrPlanPin,lyrAreaInfo,"plan");
            }
            map.setExtent(mpPoint.getExtent().expand(1.5)); //extentไปยังจุดและเส้นทางนั้น

            // for (var ind = 0; ind < lstWaitingItem.pin.length; ind++) {
            //     this._generatePinBeginEndViewOnMap(lstWaitingItem.pin[ind], mpPoint, lyrWaitingPin, ind);
            // }
            if (lstWaitingItem.pin.length) {
                this._generatePinBeginEndViewOnMap(lstWaitingItem.pin[0], mpPoint, lyrWaitingPin, 0);
                if (!lstVehicle.length) {

                    this._generatePinBeginEndViewOnMap(lstWaitingItem.pin[1], mpPoint, lyrWaitingPin, 1);
                }

            }

            for (var ind = 0; ind < lstVehicle.length; ind++) {
                var vehItem = lstVehicle[ind];
                var point = new Point(vehItem.longitude, vehItem.latitude);
                var symbWp = new PictureMarkerSymbol(vehItem.movementImageUrl, 32, 32);

                var textSymb = new TextSymbol();
                textSymb.setText(vehItem.attributes.TITLE);
                textSymb.setOffset(0, -27);
                textSymb.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                textSymb.setHaloSize(ApplicationConfig.haloSize);

                lyrVehicle.add(new Graphic(point, symbWp, vehItem.attributes));
                lyrVehicle.add(new Graphic(point, textSymb));

            }



            this._generateRouteDirectionViewOnMap(lstWaitingItem.route, lyrWaitingRoute);

            //  map.setExtent(mpPoint.getExtent().expand(1.5));
        },

        _generatePinViewOnMap: function (item, pinNumber, mpPoint, lyr,lyrAreaInfo,status = "Actual") {

            if(status === "plan"){
                if(item.attributes.areaInfo && item.attributes.areaInfo.length > 0){
                    let listOPI = item.attributes.areaInfo?item.attributes.areaInfo.reverse():[]
    
                    listOPI.forEach(function (item,index) {
                        var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, 
                            new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, 
                            new Color("rgba("+item.type.symbolBorderColor+")"), 1), 
                            new Color("rgba("+item.type.symbolFillColor+")"));
              
                        symbol.color.a = ApplicationConfig.bufferConfig.defaultBufferFillOpacity;
                        symbol.outline.color.a = ApplicationConfig.bufferConfig.defaultBufferBorderOpacity;
                        var polygonJson = {
                            "rings": JSON.parse(item.points),
                            "spatialReference": {
                                "wkid": 4326
                            }
                        };
                        var polygon = new Polygon(polygonJson);
                        var graphic = new Graphic(polygon, symbol);
                        lyrAreaInfo.add(graphic);
                        if(index !== 0){
                            let areaTextPolygon = new TextSymbol();
                            areaTextPolygon.setText(item.name);
                            areaTextPolygon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                            areaTextPolygon.setHaloColor(new Color(ApplicationConfig.haloColor));
                            areaTextPolygon.setHaloSize(ApplicationConfig.haloSize);
                            areaTextPolygon.setOffset(0, -18);
                            let centerOfPolygon = polygon.getCentroid();
                            let graphicAreaText = new Graphic(centerOfPolygon, areaTextPolygon);
                            lyrAreaInfo.add(graphicAreaText);
                        }
                    })
    
                }else{
                    let bufferConfigRadius = ApplicationConfig.bufferConfig;
                    let pointRadius = webMercatorUtils.geographicToWebMercator(new Point(item.longitude, item.latitude));
  
                    //แก้ให้ render radius ตามค่า attribute ถ้าไม่มี ใช้ default จาก config
                    let radius = bufferConfigRadius.defaultBufferRadius;
                    if (item.attributes && item.attributes.radius){
                        radius = item.attributes.radius;
                    }
        
        // //             // if (item.attributes && item.attributes.raw && item.attributes.raw.radius) {
        // //             //     radius = item.attributes.raw.radius;
        // //             // }
   
                    let buffAreaRadius = geometryEngine.buffer(pointRadius, radius, bufferConfigRadius.defaultBufferUnit);

                    let symbolRadius = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, 
                            new Color(bufferConfigRadius.defaultBufferBorderColor), 
                            bufferConfigRadius.defaultBufferBorderWidth),
                            new Color(bufferConfigRadius.defaultBufferFillColor));
        
                        symbolRadius.color.a = bufferConfigRadius.defaultBufferFillOpacity;
                        symbolRadius.outline.color.a = bufferConfigRadius.defaultBufferBorderOpacity;
  
                        let graphicRadius = new Graphic(buffAreaRadius, symbolRadius);
                        lyrAreaInfo.add(graphicRadius);
                }

            }
            
            
            
            var wpConfigItem = ApplicationConfig.waypointConfig.status[item.attributes.jobWaypointStatus];

            var point = new Point(item.longitude, item.latitude);
            var symbWp = new PictureMarkerSymbol(wpConfigItem.url, 35, 50);
            symbWp.setOffset(0, 23);

            mpPoint.addPoint(point);

            var symbTextPin = new TextSymbol();
            symbTextPin.setText(pinNumber);
            symbTextPin.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            symbTextPin.setOffset(0, 26);

            lyr.add(new Graphic(point, symbWp, item.attributes));
            lyr.add(new Graphic(point, symbTextPin, item.attributes));
        },

        _generateRouteViewOnMap: function (routeItem, routeColor, lyr, mpPoint) {

            var pathItem = new Array();
            for (var ind = 0; ind < routeItem.length; ind++) {
                pathItem.push([routeItem[ind].lon, routeItem[ind].lat]);
                mpPoint.addPoint(new Point(routeItem[ind].lon, routeItem[ind].lat));
            }

            var polyline = new Polyline({
                "paths": [pathItem],
                "spatialReference": {
                    "wkid": 4326
                }
            });

            var symbol = new SimpleLineSymbol(
                SimpleLineSymbol.STYLE_SOLID,
                new Color(routeColor), 6
            );

            lyr.add(new Graphic(polyline, symbol));
        },

        _generatePinBeginEndViewOnMap: function (item, mpPoint, lyr, index) {
            let img = (index === 0) ? ApplicationConfig.playbackFootprintSetting.pinBegin.url : ApplicationConfig.playbackFootprintSetting.pinEnd.url;
            let width = ApplicationConfig.playbackFootprintSetting.pinBegin.width;
            let height = ApplicationConfig.playbackFootprintSetting.pinBegin.height;
            let point = new Point(item.longitude, item.latitude);
            let symbWp = new PictureMarkerSymbol(img, width, height);
            mpPoint.addPoint(point);

            var symbTextPin = new TextSymbol();
            symbTextPin.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
            symbTextPin.setOffset(0, 26);

            lyr.add(new Graphic(point, symbWp, item.attributes));
            lyr.add(new Graphic(point, symbTextPin));
        },

        _generateRouteDirectionViewOnMap: function (routeItem, lyr) {
            if (routeItem.length) {

                //Line
                var pathItem = new Array();
                for (var ind = 0; ind < routeItem.length; ind++) {
                    pathItem.push([routeItem[ind].lon, routeItem[ind].lat]);
                }
                var polyline = new Polyline({
                    "paths": [pathItem],
                    "spatialReference": {
                        "wkid": 4326
                    }
                });
                var symbol = new SimpleLineSymbol(
                    SimpleLineSymbol.STYLE_SOLID,
                    new Color(ApplicationConfig.playbackFootprintSetting.color[2]), 6
                );
                lyr.add(new Graphic(polyline, symbol));

                //Arrow
                for (var k = 0; k < routeItem.length; k++) {
                    let point = new Point(routeItem[k].lon, routeItem[k].lat);
                    let symb = null;

                    switch (routeItem[k].movement) {
                        case 4: {
                            let width = ApplicationConfig.playbackFootprintSetting.footprintEngineOnPin.width;
                            let height = ApplicationConfig.playbackFootprintSetting.footprintEngineOnPin.height;

                            symb = new PictureMarkerSymbol(
                                ApplicationConfig.playbackFootprintSetting.footprintEngineOnPin.url,
                                width,
                                height);
                            break;
                        }
                        case 3: {
                            let width = ApplicationConfig.playbackFootprintSetting.footprintParkPin.width;
                            let height = ApplicationConfig.playbackFootprintSetting.footprintParkPin.height;

                            symb = new PictureMarkerSymbol(
                                ApplicationConfig.playbackFootprintSetting.footprintParkPin.url,
                                width,
                                height);
                            break;
                        }
                        case 2: {
                            let width = ApplicationConfig.playbackFootprintSetting.footprintStopPin.width;
                            let height = ApplicationConfig.playbackFootprintSetting.footprintStopPin.height;

                            symb = new PictureMarkerSymbol(
                                ApplicationConfig.playbackFootprintSetting.footprintStopPin.url,
                                width,
                                height);
                            break;
                        }
                        default: {
                            let width = ApplicationConfig.playbackFootprintSetting.footprintPin.width;
                            let height = ApplicationConfig.playbackFootprintSetting.footprintPin.height;

                            symb = new PictureMarkerSymbol(
                                ApplicationConfig.playbackFootprintSetting.footprintPin.url,
                                width,
                                height);
                            break;
                        }
                    }
                    symb.setAngle(routeItem[k].direction || 0);

                    var textSymb = new TextSymbol();
                    textSymb.setOffset(0, -27);
                    textSymb.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                    textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                    textSymb.setHaloSize(ApplicationConfig.haloSize);

                    lyr.add(new Graphic(point, symb, routeItem[k].attributes));
                    lyr.add(new Graphic(point, textSymb));
                }

            }
        },
        /* Widget Function */

        favoriteZoom: function (data) {
            var point = new Point([data.lon, data.lat]);

            var lyr = this.getLayer("lyr-location-info");
            lyr.clear();
            lyr.add(new Graphic(point, mapClickSymbol));

            map.centerAndZoom(point, data.zoom || 17);
        },

        zoomIn: function () {
            Utility.debugLog("Zoom In");

            map.setZoom(map.getZoom() + 1);
        },

        zoomOut: function () {
            Utility.debugLog("Zoom Out");

            map.setZoom(map.getZoom() - 1);
        },

        currentLocation: function () {
            Utility.debugLog("Current Location");

            //Edit : ขอเพิ่ม onError , option : {enableHighAccuracy: true}
            //By : Champ 05/10/2016

            //navigator.geolocation.getCurrentPosition(this._callCurrent);
            navigator.geolocation.getCurrentPosition(lang.hitch(this, "_callCurrent"), this.onCurrentError, {
                enableHighAccuracy: true
            });
        },

        changeBasemap: function (obj) {
            Utility.debugLog("changeBasemap", obj.basemap);
            let bmKey = obj.basemap;

            currentBasemapActive = bmKey;

            var lstBM = ApplicationConfig.lstBasemapState[bmKey];

            /* set visible false all */
            array.forEach(lstAllBasemapKey, lang.hitch(this, function (bmItem) {
                let bm = this.getLayer(bmItem);
                if (bm)
                    bm.setVisibility(false);
            }));

            /* set state and set visible on selected */
            for (let key in ApplicationConfig.lstBasemapState) {
                array.forEach(ApplicationConfig.lstBasemapState[key], lang.hitch(this, function (itm) {
                    if (bmKey == key) {
                        itm.mainStateVisible = true;

                        let bm = this.getLayer(itm.id);
                        if (bm)
                            bm.setVisibility(itm.mainStateVisible && itm.extentStateVisible);

                        if (itm.dependLayer && itm.dependLayer.length > 0) {
                            array.forEach(itm.dependLayer, lang.hitch(this, function (dpLyr) {
                                let lyr = this.getLayer(dpLyr);

                                if (lyr)
                                    lyr.setVisibility(true);
                            }));
                        }
                    } else {
                        itm.mainStateVisible = false;
                    }
                }));
            }
        },

        toggleLayer: function (obj) {
            Utility.debugLog("Change Layer", obj);

            var lstLayer = ApplicationConfig.lstLayerState;
            var lyr = jsonQuery("[?id='" + obj.layer + "']", lstLayer)[0];

            lyr.mainStateVisible = !lyr.mainStateVisible;

            var layer = this.getLayer(lyr.id);
            if (layer)
                layer.setVisibility(lyr.mainStateVisible);
        },

        toggleVehicle: function () {
            var lyrTrack = this.getLayer(ApplicationConfig.commandOptions["track-vehicles"].layer) // "lyr-track-command"
            var isShowTextGP = (lyrTrack.graphics.length / 2) < limitDrawTrackVehicle; // หาร 2 เพราะมีทะเบียนรถด้วย
            //console.log("b debug isShowTextGP : ", isShowTextGP);
            array.forEach(lyrTrack.graphics, lang.hitch(this, function (grp) {
                //ถ้าสถานะของรถที่แสดงตรงกับ filter ที่ toggle ใน panel ด้านขวา
                var attrMovement = ApplicationConfig.currentTrackVehicleFilter.indexOf(grp.attributes.MOVEMENT) != -1;
                var isTextSymbol = (grp.symbol.type && grp.symbol.type == "textsymbol");
                if (attrMovement && !isTextSymbol) {
                    grp.show();
                } else if (attrMovement && isTextSymbol) {
                    if (isShowTextGP) grp.show();
                    else grp.hide();
                } else {
                    grp.hide();
                }
            }));
        },


        showAllTrackVehicle: function () {
            ApplicationConfig.currentTrackVehicleFilter = new Array();
            ApplicationConfig.vehicleStatusConfig.forEach(lang.hitch(this, function (itm) {
                ApplicationConfig.currentTrackVehicleFilter.push(itm.key);
            }));

            this.toggleVehicle();
        },

        hideAllTrackVehicle: function () {
            ApplicationConfig.currentTrackVehicleFilter = new Array();
            this.toggleVehicle();
        },

        toggleViewOnMap: function (obj) {
            if (obj.toggle.indexOf("plan") != -1) {
                this.getLayer("lyr-shipment-plan").show();
                this.getLayer("lyr-shipment-poi-plan").show();
            } else {
                this.getLayer("lyr-shipment-plan").hide();
                this.getLayer("lyr-shipment-poi-plan").hide();

            }

            if (obj.toggle.indexOf("actual") != -1) {
                this.getLayer("lyr-shipment-actual").show();
                this.getLayer("lyr-shipment-poi-actual").show();
            } else {
                this.getLayer("lyr-shipment-actual").hide();
                this.getLayer("lyr-shipment-poi-actual").hide();
            }

            if (obj.toggle.indexOf("waiting") != -1) {
                this.getLayer("lyr-shipment-waiting").show();
                this.getLayer("lyr-shipment-poi-waiting").show();
            } else {
                this.getLayer("lyr-shipment-waiting").hide();
                this.getLayer("lyr-shipment-poi-waiting").hide();
            }

            if (obj.toggle.indexOf("poiarea") != -1) {
                this.getLayer("lyr-draw-command").clear();
                mapMode = "shipmentViewOnMap"
                this.getLayer("lyr-shipment-poi-area-info").show();
                // this.getLayer("lyr-shipment-poi-waiting").show();
            } else {
                mapMode = "normal"
                this.getLayer("lyr-shipment-poi-area-info").hide();
                // this.getLayer("lyr-shipment-poi-waiting").hide();
            }
        },


        setTrackAllVisible: function () {
            var lyrTrack = this.getLayer(ApplicationConfig.commandOptions["track-vehicles"].layer) //;"lyr-track-command"
            var isShowTextGP = (lyrTrack.graphics.length / 2) < limitDrawTrackVehicle; // หาร 2 เพราะมีทะเบียนรถด้วย
            array.forEach(lyrTrack.graphics, lang.hitch(this, function (grp) {
                //ถ้าสถานะของรถที่แสดงตรงกับ filter ที่ toggle ใน panel ด้านขวา
                var attrMovement = ApplicationConfig.currentTrackVehicleFilter.indexOf(grp.attributes.MOVEMENT) != -1;
                var isTextSymbol = (grp.symbol.type && grp.symbol.type == "textsymbol");
                if (attrMovement && !isTextSymbol) {
                    grp.show();
                } else if (attrMovement && isTextSymbol) {
                    if (isShowTextGP) grp.show();
                    else grp.hide();
                } else {
                    grp.hide();
                }
            }));
        },

        gotoXY: function (obj) {

            let lat = obj.lat;
            let lon = obj.lon;

            var zoom = ApplicationConfig.currentLocationZoomLevel;
            var point = new Point(lon, lat);
            //console.log(point);

            map.centerAndZoom(point, zoom);

            var lyr = this.getLayer("lyr-goto-xy");
            lyr.clear();
            lyr.add(new Graphic(point, mapClickSymbol));
        },


        showRadius: function (obj) {
            if((obj.attributes.areaInfo && obj.attributes.areaInfo.length > 0) 
            || (obj.attributes.raw && obj.attributes.raw.areaInfo.length > 0) ){
                let listOPI = obj.attributes.areaInfo?obj.attributes.areaInfo:[]
                if(obj["attributes"].raw && obj["attributes"].raw.areaInfo && obj["attributes"].raw.areaInfo.length > 0){
                    listOPI = obj["attributes"].raw.areaInfo
                }
                let layer = this.getLayer("lyr-draw-command");
                layer.clear();
                listOPI.forEach(function (item,index) {
                    var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, 
                        new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, 
                        new Color("rgba("+item.type.symbolBorderColor+")"), 1), 
                        new Color("rgba("+item.type.symbolFillColor+")"));
          
                    symbol.color.a = ApplicationConfig.bufferConfig.defaultBufferFillOpacity;
                    symbol.outline.color.a = ApplicationConfig.bufferConfig.defaultBufferBorderOpacity;
                    var polygonJson = {
                        "rings": JSON.parse(item.points),
                        "spatialReference": {
                            "wkid": 4326
                        }
                    };
                    var polygon = new Polygon(polygonJson);
                    var graphic = new Graphic(polygon, symbol);
                    layer.add(graphic);
                    if(index !== 0){
                        let areaTextPolygon = new TextSymbol();
                        areaTextPolygon.setText(item.name);
                        areaTextPolygon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                        areaTextPolygon.setHaloColor(new Color(ApplicationConfig.haloColor));
                        areaTextPolygon.setHaloSize(ApplicationConfig.haloSize);
                        areaTextPolygon.setOffset(0, -18);
                        let centerOfPolygon = polygon.getCentroid();
                        let graphicAreaText = new Graphic(centerOfPolygon, areaTextPolygon);
                        layer.add(graphicAreaText);
                        
                    }
                })
            }else{
                var pt = obj["geometry"];
    
                var bufferConfig = ApplicationConfig.bufferConfig;
    
                var point = webMercatorUtils.geographicToWebMercator(new Point(pt.lon, pt.lat));
    
                //แก้ให้ render radius ตามค่า attribute ถ้าไม่มี ใช้ default จาก config
                var radius = bufferConfig.defaultBufferRadius;
                if (obj.attributes && obj.attributes.radius)
                    radius = obj.attributes.radius;
    
                if (obj.attributes && obj.attributes.raw && obj.attributes.raw.radius) {
                    radius = obj.attributes.raw.radius;
                }
                var buffArea = geometryEngine.buffer(point, radius, bufferConfig.defaultBufferUnit);
    
                var layer = this.getLayer("lyr-draw-command");
                layer.clear();
    
                var symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
                    new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(bufferConfig.defaultBufferBorderColor), bufferConfig.defaultBufferBorderWidth),
                    new Color(bufferConfig.defaultBufferFillColor));
    
                symbol.color.a = bufferConfig.defaultBufferFillOpacity;
                symbol.outline.color.a = bufferConfig.defaultBufferBorderOpacity;
    
                var graphic = new Graphic(buffArea, symbol);
                layer.add(graphic);
            }

        },

        _callCurrent: function (position) {
            var lat = position.coords.latitude;
            var lon = position.coords.longitude;
            var zoom = ApplicationConfig.currentLocationZoomLevel;
            var point = new Point(lon, lat);
            //console.log(point);
            map.centerAndZoom(point, zoom);

            //Edit : เพิ่ม Pin Current Location
            var lyr = this.getLayer("lyr-current-location");
            var symb = new PictureMarkerSymbol(ApplicationConfig.currentLocatinPin.url, ApplicationConfig.currentLocatinPin.width, ApplicationConfig.currentLocatinPin.height);

            lyr.clear();
            lyr.add(new Graphic(point, symb));
        },

        //cahnge clickmapState
        measurement: function (param) {
            switch (param) {
                case "measurement": {
                    mapMode = 'measurement'
                    clickmapState = param;
                    break;
                }
                case "none": {
                    clickmapState = param;
                    break;
                }
                default:
                    break;
            }

        },

        setCustomPOIText: function (flg) {
            poiLabelFlg = flg;

            if (map.getLevel() >= ApplicationConfig["customPOIMinimumLevelShow"]) {

                if (poiLabelFlg) {
                    map.getLayer("lyr-draw-command").show();

                    map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").show()

                    map.getLayer("lyr-custom-poi-label-layer").show();
                    map.getLayer("lyr-custom-poi-single-label-layer").show();
                } else {

                    map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").hide()

                    map.getLayer("lyr-custom-poi-label-layer").hide();
                    map.getLayer("lyr-custom-poi-single-label-layer").hide();
                }
            } else {

                map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").hide()

                map.getLayer("lyr-custom-poi-label-layer").hide();
                map.getLayer("lyr-custom-poi-single-label-layer").hide();
            }
        },

        setCustomAreaText: function (flg) {
            areaLabelFlg = flg;

            if (areaLabelFlg) {
                map.getLayer("lyr-custom-area-label-layer").show();
            } else {
                map.getLayer("lyr-custom-area-label-layer").hide();
            }
        },

        customPoi: function () {
            var def = $.Deferred();

            poiLabelFlg = true;
            ApplicationConfig.CustomPOIMainState = true;
            ApplicationConfig.CustomPOITextState = true;


            if (ApplicationConfig.showPoiCluster) {
                //clear layer
                map.getLayer("lyr-custom-poi-layer").clear();
                map.getLayer("lyr-custom-poi-label-layer").clear();
                map.getLayer("lyr-custom-poi-single-label-layer").clear();


                map.getLayer("lyr-custom-poi-layer").show();
                map.getLayer("lyr-custom-poi-label-layer").show();
                map.getLayer("lyr-custom-poi-single-label-layer").show();
            } else {
                map.getLayer("lyr-custom-poi-non-cluster").clear();
                map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").clear();

                map.getLayer("lyr-custom-poi-non-cluster").show();
                map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").show();
            }

            wgWebApiCaller.requestPOST({
                url: ApplicationConfig.webAPIConfig.getCustomPOILightWeight,
                data: {
                    CompanyId: ApplicationConfig.userInfo.info._userSession.currentCompanyId,
                },
                success: lang.hitch(this, function (data) {
                   if (ApplicationConfig.showPoiCluster) {
                    let layerSubArea = this.getLayer("draw-polygon-sub-area");

                        // Create a symbol for drawing the point
                        var lstData = array.map(data, function (itm) {
                            var pt = webMercatorUtils.geographicToWebMercator(new Point(itm.long, itm.lat));

                            return {
                                "x": pt.x,
                                "y": pt.y,
                                "attributes": itm
                            };
                        });
                        clusterLayer.drawNewData(lstData);

                        // data.forEach((item)=>{
                        //     if(item.areaInfo.length>0){
                        //         item.areaInfo.forEach((graphic)=>{
                        //             let symbol = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID, new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(`rgba(${graphic.type.symbolBorderColor})`), 1), new Color(`rgba(${graphic.type.symbolFillColor})`));
                        //             // symbol.color.a = graphic.opacity.fill;
                        //             // symbol.outline.color.a = obj.opacity.border;
                        //             let polygonJson = {
                        //                 "rings": JSON.parse(graphic.points),
                        //                 "spatialReference": {
                        //                     "wkid": 4326
                        //                 }
                        //             };
                        //             let polygon = new Polygon(polygonJson);
                        // console.log(polygon,symbol);
                        //             let graphicArea = new Graphic(polygon, symbol);
                        // console.log(graphicArea,layerSubArea);

                        // layerSubArea.add(graphicArea);
                        //         })
                        //     }
                        // })

                    } else {
                        // console.log("data>>", data)
                        data.forEach(function (item) {
                            var singleTemplate = new PopupTemplate({
                                "title": "",
                                "fieldInfos": [{
                                    "fieldName": "name",
                                    "label": ResourceLib.text("CustomPOI_Name"),
                                    visible: true
                                }, {
                                    "fieldName": "code",
                                    "label": ResourceLib.text("CustomPOI_Code"),
                                    visible: true
                                }, {
                                    "fieldName": "location",
                                    "label": ResourceLib.text("CustomPOI_Location"),
                                    visible: true
                                }]
                            });

                            var zoomLevel = map.getZoom();
                            var size = Utility.calculateSizeForZoomFactor(ApplicationConfig.cluster.pinSize.large, zoomLevel);

                            //create point
                            var point = webMercatorUtils.geographicToWebMercator(new Point(item.long, item.lat));

                            // Create a symbol for drawing the point
                            var markerSymbol = new PictureMarkerSymbol(item.iconUrl, size, size)

                            // map.centerAt(point);

                            var textSymb = new TextSymbol();
                            textSymb.setText(item.name);

                            var font = new Font();
                            font.setSize("14pt");
                            font.setStyle(Font.STYLE_NORMAL);
                            font.setVariant(Font.VARIANT_NORMAL);
                            font.setWeight(Font.WEIGHT_NORMAL);
                            font.setFamily("DB-Regular");

                            textSymb.setFont(font);
                            textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                            textSymb.setHaloSize(ApplicationConfig.haloSize);
                            textSymb.setOffset(0, 27);

                            var gText = new Graphic(point, textSymb);

                            nonClusterLayer.add(new Graphic(point, markerSymbol, item, singleTemplate));
                            map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").add(gText);
                        });
                    }

                    def.resolve();
                }),
                error: function (err) {
                    console.log(err);
                    def.reject();
                }
            });

            if (customPoiHandle) {
                customPoiHandle.remove();
                customPoiHandle = null;
            }

            return def;
        },
        _evtExtentPOIHandler: null,
        _customPoi: function (evt) {

            console.log("_customPoi>>>")
            map.getLayer("lyr-custom-poi-layer").clear();

            let labelLyr = map.getLayer("lyr-custom-poi-label-layer");
            map.getLayer("lyr-custom-poi-single-label-layer").clear();
            labelLyr.clear();

            if (this._evtExtentPOIHandler)
                clearTimeout(this._evtExtentPOIHandler);

            this._evtExtentPOIHandler = setTimeout(lang.hitch(this, function () {
                wgWebApiCaller.requestPOST({
                    url: ApplicationConfig.webAPIConfig.getCustomPOI,
                    data: {
                        //companyId: ApplicationConfig.userInfo["GISC-CompanyId"]
                        CompanyId: ApplicationConfig.userInfo.info._userSession.currentCompanyId,
                    },
                    success: lang.hitch(this, function (data) {

                        let result = data.items;

                        array.forEach(result, lang.hitch(this, function (itm) {

                            let pt = new Point(itm.longitude, itm.latitude);

                            if (geometryEngine.contains(customPOIFilterExtent, pt)) {

                                //create label

                                var name = itm["name"];
                                var textSymb = new TextSymbol();
                                textSymb.setText(name);
                                textSymb.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                                textSymb.setHaloColor(new Color(ApplicationConfig.haloColor));
                                textSymb.setHaloSize(ApplicationConfig.haloSize);
                                textSymb.setOffset(0, 27);

                                labelLyr.add(new Graphic(new Point(itm.longitude, itm.latitude), textSymb, itm));

                                if (itm.icon != null) {
                                    // change icon structure
                                    let iconUrl = itm.icon.image["fileUrl"];
                                    let img = new Image();

                                    img.onload = lang.hitch(this, function (objData, e) {

                                        //แก้ปัญหาของ Firefox ไม่มี property : path
                                        let imgData = e.path ? e.path[0] : e.target;

                                        let symb = new PictureMarkerSymbol(objData.src, imgData.width, imgData.height);
                                        //symb.setOffset(0, 14);
                                        symb.setOffset(objData.data.icon.offsetX, objData.data.icon.offsetY);
                                        objData.layer.add(new Graphic(
                                            new Point(objData.data.longitude, objData.data.latitude),
                                            symb,
                                            objData.data));

                                    }, {
                                        src: iconUrl,
                                        data: itm,
                                        layer: map.getLayer("lyr-custom-poi-layer")
                                    });

                                    img.src = iconUrl;
                                }

                            }
                        }));
                    }),
                    error: function (err) {
                        console.log(err);
                    }
                });
            }), 1000);
        },

        _showCustomPOIInfo: function (evt) {
            var newPT = screenUtils.toMapGeometry(map.extent, map.width, map.height, new ScreenPoint(300, 100));
            // map.infoWindow.setTitle("Title");
            // map.infoWindow.setContent("AAAAAAAAAAAAAAA");
            //map.infoWindow.resize(310, 500);
            map.infoWindow.anchor = "";

            //หลอกตำแหน่งครั้งแรกเผื่อให้Clearค้่
            map.infoWindow.setFeatures(null);
            map.infoWindow.show(newPT);
            map.infoWindow.hide();


            var tempHandler = on(map.infoWindow, "show", lang.hitch(this, function () {
                //console.log("map.infoWindow",map.infoWindow);
                tempHandler.remove();
                map.infoWindow.hide();
                map.infoWindow.show(newPT);
            }));

            //map.infoWindow.show(newPT);
            map.infoWindow.setFeatures(evt.graphics);
            //evt.stopPropagation();

        },

        disableCustomPoi: function () {
            ApplicationConfig.CustomPOIMainState = false;
            ApplicationConfig.CustomPOITextState = false;

            map.getLayer("lyr-custom-poi-non-cluster").clear();
            map.getLayer("lyr-custom-poi-non-cluster").hide();

            map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").clear();
            map.getLayer("lyr-custom-poi-non-cluster-single-labael-layer").hide();

            map.getLayer("lyr-custom-poi-layer").clear();
            map.getLayer("lyr-custom-poi-layer").hide();

            map.getLayer("lyr-draw-command").clear();
            map.getLayer("lyr-draw-command").hide();
            if (customPoiHandle != null) {
                customPoiHandle.remove();
            }

            map.infoWindow.hide();
            //console.log("extend handler", customPoiHandle);
        },

        customArea: function () {
            console.log("customarea")
            var def = $.Deferred();

            areaLabelFlg = true;

            map.getLayer("lyr-custom-area-layer").show();
            map.getLayer("lyr-custom-area-label-layer").clear();

            ApplicationConfig.CustomAreaMainState = true;
            ApplicationConfig.CustomAreaTextState = true;

            //console.log("map",map);
            //this._customPoi(map);
            var levelZoom = map.__LOD.level;
            var minLatLon = webMercatorUtils.xyToLngLat(map.extent.xmin, map.extent.ymin);
            var maxLatLon = webMercatorUtils.xyToLngLat(map.extent.xmax, map.extent.ymax);
            //console.log("etry", minLatLon);
            //var ptData = { "latMin": minLatLon[1], "lonMin": minLatLon[0], "latMax": maxLatLon[1], "lonMax": maxLatLon[0], "zoomLevel": levelZoom };
            var ptData = {
                //CompanyId: ApplicationConfig.userInfo["GISC-CompanyId"]
                CompanyId: ApplicationConfig.userInfo.info._userSession.currentCompanyId
            };



            wgWebApiCaller.requestPOST({
                url: ApplicationConfig.webAPIConfig.getCustomArea,
                data: ptData,
                success: lang.hitch(this, function (data) {
                    //console.log("return", data);
                    var SymbolMarker;




                    //var regexLonLat = new RegExp(/^[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?),\s*[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)$/);

                    //var regxLatLon = new RegExp(/^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/);


                    for (var i = 0; i < data.items.length; i++) {

                        let fillColor = Utility.convertRGBA2Hex(data.items[i].symbolFillColor);
                        let borderColor = Utility.convertRGBA2Hex(data.items[i].symbolBorderColor);
                        //console.log(fillColor, borderColor);
                        var colorRandom = this._randomColor();

                        var stringRing = data.items[i].points
                        var repalceSpace = stringRing.replace(/\s/g, '');
                        //console.log(data.items[i].id, repalceSpace);
                        var ring = JSON.parse(repalceSpace);
                        var polygon = new Polygon(new SpatialReference({
                            wkid: 4326
                        }));
                        for (var iRing = 0; iRing < ring.length; iRing++) {
                            polygon.addRing(ring[iRing]);
                        }

                        /* Calculate Area */
                        //
                        var mctPolygon = webMercatorUtils.geographicToWebMercator(polygon);
                        var areaValue = geometryEngine.geodesicArea(mctPolygon, "square-meters");
                        var displayAreaValue = Utility.toAreaUnitCompanyFormat(areaValue);

                        var areaTextPologon = new TextSymbol();
                        areaTextPologon.setText(displayAreaValue);
                        areaTextPologon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                        areaTextPologon.setHaloColor(new Color(ApplicationConfig.haloColor));
                        areaTextPologon.setHaloSize(ApplicationConfig.haloSize);
                        areaTextPologon.setOffset(0, -18);

                        var centerOfPolygon = polygon.getCentroid();
                        var textPologon = new TextSymbol();
                        textPologon.setText(data.items[i].name);
                        textPologon.setFont(new Font("14pt", Font.STYLE_NORMAL, Font.VARIANT_NORMAL, Font.WEIGHT_NORMAL, "DB-Regular"));
                        textPologon.setHaloColor(new Color(ApplicationConfig.haloColor));
                        textPologon.setHaloSize(ApplicationConfig.haloSize);

                        var symbolMarker = new SimpleFillSymbol();
                        symbolMarker.setColor(new Color(fillColor.hex || colorRandom));
                        symbolMarker.setOutline(new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID, new Color(borderColor.hex), 1));
                        //console.log("mm", symbolMarker);
                        symbolMarker.color.a = 0.5;

                        var graphic = new Graphic(polygon, symbolMarker);
                        map.getLayer("lyr-custom-area-layer").add(graphic);
                        var graphicText = new Graphic(centerOfPolygon, textPologon);
                        map.getLayer("lyr-custom-area-label-layer").add(graphicText);
                        var graphicAreaText = new Graphic(centerOfPolygon, areaTextPologon);
                        map.getLayer("lyr-custom-area-label-layer").add(graphicAreaText);
                    }

                    def.resolve();
                }),
                error: function (err) {
                    console.log(err);
                    def.reject();
                }
            });

            return def;
        },

        _randomColor: function () {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        },

        _clearHilightPoint: function () {
            console.log("_clearHilightPoint")
            var layer = this.getLayer(ApplicationConfig.commandOptions["draw-one-point-zoom"].layer);
            layer.clear();
        },

        disableCustomArea: function () {

            map.getLayer("lyr-custom-area-layer").clear();
            map.getLayer("lyr-custom-area-layer").hide();

            map.getLayer("lyr-custom-area-label-layer").clear();
            map.getLayer("lyr-custom-area-label-layer").hide();

            ApplicationConfig.CustomAreaMainState = false;
            ApplicationConfig.CustomAreaTextState = false;

            if (customAreaHandle != null) {
                customAreaHandle.remove();
            }
            //console.log("extend handler", customPoiHandle);
        },

        _onCurrentError: function (e) {
            Utility.debugLog("Get Current Location Error : ", e);
        },

        onTriggerFire: function (obj) {
            /*attach event*/
        },

        getMapExtent: function () {
            var currentExtent = webMercatorUtils.webMercatorToGeographic(map.extent);

            return currentExtent.xmin + "," + currentExtent.ymin + "," + currentExtent.xmax + "," + currentExtent.ymax;
        },


        /* Export Map Function */
        getTransferMapData: function () {

            var allVisibleLayer = jsonQuery("[=id]", basemapGrp[currentBasemapActive]);
            var lstDyLayerVisible = jsonQuery("[?mainStateVisible=true][=id]", lstLayer);

            array.forEach(basemapGrp[currentBasemapActive], lang.hitch(this, function (itm) {
                lstDyLayerVisible = lstDyLayerVisible.concat(itm.dependLayer);
            }));

            var exportMapParam = {
                //f: "json",
                Web_Map_as_JSON: {
                    mapOptions: {
                        //showAttribution: (view.ui.components.indexOf("attribution") > -1 ? true : false),
                        extent: map.extent.toJson(),
                        spatialReference: map.spatialReference.toJson(),
                        scale: map.getScale(),
                        currentBasemapActive: allVisibleLayer.concat(lstDyLayerVisible),
                    },
                    operationalLayers: [] //,
                    //exportOptions: { outputSize: [window.innerWidth, window.innerHeight]},
                    //dpi: 96
                } //,
                //Format: "PNG8",
                //Layout_Template: "MAP_ONLY"
            };

            //create operation layer
            var lyrList = map.getLayersVisibleAtScale(map.getScale());
            lyrList.forEach(function (lyr) {
                var objLayerDef = {
                    id: lyr.id,
                    //title: lyr.id,
                    opacity: lyr.opacity,
                    minScale: lyr.minScale,
                    maxScale: lyr.maxScale,
                    declaredClass: lyr.declaredClass
                };

                if (lyr.url)
                    objLayerDef.url = lyr.url;

                var gpCollection = lyr.graphics;
                if (gpCollection) {

                    var ftCollection = {
                        layers: []
                    };
                    if (lyr.visible) {
                        var featureType = Utility.getUniqueItem(jsonQuery("[=geometry.type]", gpCollection));

                        featureType.forEach(function (gpType) {
                            var type;

                            switch (gpType.toLowerCase()) {
                                case "point":
                                    type = "esriGeometryPoint";
                                    break;
                                case "polyline":
                                    type = "esriGeometryPolyline";
                                    break;
                                case "polygon":
                                    type = "esriGeometryPolygon";
                                    break;
                                case "multipoint":
                                    type = "esriGeometryMultiPoint";
                                    break;
                            }

                            var layers = {
                                layerDefinition: {
                                    name: gpType + "Layer",
                                    geometryType: type
                                },
                                featureSet: {
                                    geometryType: type,
                                    features: []
                                }
                            };

                            var gpListInType = jsonQuery("[?type=" + gpType + "]", gpCollection);
                            gpListInType.forEach(function (gpItem) {
                                layers.featureSet.features.push(gpItem.toJson());
                            });

                            ftCollection.layers.push(layers);
                        });
                    }

                    objLayerDef.featureCollection = ftCollection;
                }

                exportMapParam.Web_Map_as_JSON.operationalLayers.push(objLayerDef);

            });

            return exportMapParam;
        }
    });
});